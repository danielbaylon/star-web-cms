package com.enovax.star.cms.commons.jcrrepository.system.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PartnerExclProdMapping;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.util.jcr.publishing.PublishingUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lavanya on 22/9/16.
 */
@Repository
public class JcrPartnerExclProdMappingRepository implements IPartnerExclProdMappingRepository {

    private void updateExclusiveProductbyPartnerId(String channel, Integer partnerId, String exclProdIds) throws  RepositoryException,Exception{

            //String query = "//element(*, mgnl:content)[@partnerId = '" + partnerId + "']";
            //I/terable<Node> tierPartnerMappingNodes = JcrRepository.query(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), JcrWorkspace.PartnerExclusiveProductMapping.getNodeType(), query);
            Node partnerNode = null;
            boolean nodeExists = JcrRepository.nodeExists(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), "/"+channel+"/"+ partnerId);
            if(!nodeExists) {
                partnerNode = JcrRepository.createNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(),"/"+channel,partnerId.toString(),JcrWorkspace.PartnerExclusiveProductMapping.getNodeType());
            }else {
                partnerNode = JcrRepository.getParentNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(),"/"+channel+"/"+ partnerId);
            }
            partnerNode.setProperty("exclProdIds",exclProdIds==null?"":exclProdIds);
            JcrRepository.updateNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(),partnerNode);
        PublishingUtil.publishNodes(partnerNode.getIdentifier(), JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName());

    }

    @Override
    public List<String> getExclProdbyPartner(String channel, Integer partnerId) throws  RepositoryException{
        Node partnerNode = getPartnerNodebyPartnerId(channel,partnerId);
        List<String> exclProdList = null;
        if(partnerNode == null)
            return null;
        Property exclProdsProperty = partnerNode.getProperty("exclProdIds");
        if(exclProdsProperty != null) {
            String exclProds = exclProdsProperty.getValue().getString();
            if(!exclProds.equals("")) {
                 exclProdList = Arrays.asList(exclProds.split("\\s*,\\s*"));
            }
        }
        return  exclProdList;
    }

    @Override
    public PartnerExclProdMapping getPartnerExclProdMappingbyPartner(String channel, Integer partnerId) throws  RepositoryException{
        PartnerExclProdMapping partnerExclProdMapping = new PartnerExclProdMapping(partnerId);
        List<String> exclProdList = getExclProdbyPartner(channel, partnerId);
        if(exclProdList != null)
        partnerExclProdMapping.setExclProdIds(exclProdList);
        return  partnerExclProdMapping;
    }

    @Override
    public List<String> getPartnerByExclProd(String channel, String productId) throws RepositoryException {
        Iterable<Node> partnersIterable = JcrRepository.query(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), JcrWorkspace.PartnerExclusiveProductMapping.getNodeType(),
                "/jcr:root/" + channel + "//element(*, mgnl:partner-excl-prod-mapping)[jcr:like(@exclProdIds, '%" + productId + "%')]");
        Iterator<Node> partnersIterator = partnersIterable.iterator();
        List<String> partners = new ArrayList<>();

        while(partnersIterator.hasNext()) {
            Node partnerNode = partnersIterator.next();
            partners.add(partnerNode.getName());
        }

        return partners;
    }

    private Node getPartnerNodebyPartnerId(String channel, Integer partnerId) throws RepositoryException{
        boolean nodeExists = JcrRepository.nodeExists(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), "/"+channel+"/"+ partnerId);
        Node partnerNode = null;
        if(nodeExists)
         partnerNode = JcrRepository.getParentNode(JcrWorkspace.PartnerExclusiveProductMapping.getWorkspaceName(), "/"+channel+"/"+ partnerId);
        return partnerNode;
    }

    @Override
    public PartnerExclProdMapping savePartnerExclProdMapping(String channel, PartnerExclProdMapping partnerExclProdMapping) throws Exception{
        String exclProdIds = StringUtils.join(partnerExclProdMapping.getExclProdIds(), ',');
        updateExclusiveProductbyPartnerId(channel,partnerExclProdMapping.getPartnerId(),exclProdIds);
        return partnerExclProdMapping;
    }

    @Override
    public PartnerExclProdMapping saveExclProdbyPartner(String channel, Integer partnerId, List<String> exclProdIdList) throws Exception{
        String exclProdIds = StringUtils.join(exclProdIdList, ',');
        updateExclusiveProductbyPartnerId(channel,partnerId,exclProdIds);
        PartnerExclProdMapping partnerExclProdMapping = new PartnerExclProdMapping(partnerId);
        partnerExclProdMapping.setExclProdIds(exclProdIdList);
        return partnerExclProdMapping;
    }

}
