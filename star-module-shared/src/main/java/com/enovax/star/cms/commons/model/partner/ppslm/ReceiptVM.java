package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.OfflinePaymentType;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class ReceiptVM {
    private Logger log = LoggerFactory.getLogger(getClass());


    private String baseUrl;
    private String printedDtTimeStr;
    private String transDtStr;
    private String originalMsg;
    private String reprintedMsg;
    private String accountCode;
    private String orgName;
    private String address;
    private String licenseNum;

    private String totalStr;
    private String gstStr;
    private String exclGstStr;
    private String dollarsInwords;
    private String contactPerson;
    private String preDefinedBody;

    private List<ReceiptProdVM> prods;
    private List<ReceiptItemVM> receiptitems;

    private String recepitNum;
    private String status;
    private String tmStatus;
    private Integer extendRevalMonth;
    private String normalRevFeeStr;
    private String topupRevFeeStr;

    private String totalRevalFeeStr;
    private String validateStartDateStr;
    private String validityEndDateStr;
    private String newvalidateStartDateStr;
    private String newvalidityEndDateStr;

    private Integer transId;

    private String revalRecepitNum;
    private String revalidatedBy;
    private String revalPaymentType;
    private String revalidateDtTmStr;
    private String revalGSTStr;
    private String revalExclGSTStr;

    private String revalGSTRateStr;
    private String gstRateStr;

    private List<TncVM> tncs = new ArrayList<>();
    private boolean hasTnc = false;

    private String currency;
    private String shortCurrency;

    private String ownAddress;
    private String ownContact;

    private boolean depositBalanceAvaialble = false;
    private String depositBalanceText;

    private boolean offlinePaymentTxn = false;
    private String paymentModeText = "";


    public ReceiptVM(){}


    /**
     * This constructor is for preview revalidate receipt
     *
     * @param trans
     * @param normalRevalidatFee
     * @param topupRevalidatFee
     * @param extendRevalMonth
     */
    public ReceiptVM(PPSLMInventoryTransaction trans, BigDecimal gstRate, BigDecimal normalRevalidatFee,
                     BigDecimal topupRevalidatFee, Integer extendRevalMonth) {
        log.info("Construct revalidate receipt-");
        this.transId = trans.getId();
        this.recepitNum = trans.getReceiptNum();
        this.status = trans.getStatus();
        this.tmStatus = trans.getTmStatus();
        this.accountCode = trans.getMainAccount().getProfile().getAccountCode();
        this.orgName = trans.getMainAccount().getProfile().getOrgName();
        this.address = trans.getMainAccount().getProfile().getAddress();
        this.validateStartDateStr = NvxDateUtils.formatDateForDisplay(
                trans.getValidityStartDate(), false);
        this.validityEndDateStr = NvxDateUtils.formatDateForDisplay(
                trans.getValidityEndDate(), false);
        BigDecimal totalRevalFee = TransactionUtil.getTransRevalidateFee(trans,
                normalRevalidatFee, topupRevalidatFee);
        this.totalRevalFeeStr = TransactionUtil.priceWithDecimal(totalRevalFee,
                "");
        Date newValiEndDate = TransactionUtil.getNewValidateEndDate(
                trans.getValidityEndDate(), extendRevalMonth);
        this.newvalidityEndDateStr = NvxDateUtils.formatDateForDisplay(
                newValiEndDate, false);
        this.newvalidateStartDateStr = NvxDateUtils.formatDateForDisplay(
                trans.getValidityEndDate(), false);
        this.receiptitems = new ArrayList<ReceiptItemVM>();

        this.extendRevalMonth = extendRevalMonth;
        this.normalRevFeeStr = TransactionUtil.priceWithDecimal(
                normalRevalidatFee, "");
        this.topupRevFeeStr = TransactionUtil.priceWithDecimal(
                topupRevalidatFee, "");

        this.revalGSTStr = TransactionUtil.gstWithDecimal(totalRevalFee,gstRate, "");
        this.revalExclGSTStr = TransactionUtil.getExclGSTCostWithDecimal(
                totalRevalFee, gstRate,"");
        this.revalGSTRateStr = TransactionUtil.getDisplayGSTRate(gstRate);

        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            ReceiptItemVM receiptItem = new ReceiptItemVM(item, gstRate,
                    normalRevalidatFee, topupRevalidatFee);
            receiptitems.add(receiptItem);
        }
        Collections.sort(receiptitems);
        currency = TransactionUtil.DEFAULT_CURRENCY;
    }

    /**
     * User for view revalidate receipt
     *
     * @param trans
     * @param revalTrans
     */
    public ReceiptVM(PPSLMInventoryTransaction trans,
                     PPSLMRevalidationTransaction revalTrans, String serverUrl, String ownAddress,String ownContact) {
        this.transId = trans.getId();
        this.accountCode = trans.getMainAccount().getProfile().getAccountCode();
        this.orgName = trans.getMainAccount().getProfile().getOrgName();
        this.address = trans.getMainAccount().getProfile().getAddress();
        this.revalidatedBy = revalTrans.getUsername();
        this.revalRecepitNum = revalTrans.getReceiptNum();
        this.revalPaymentType = revalTrans.getPaymentType();
        this.tmStatus = revalTrans.getTmStatus();
        this.validateStartDateStr = NvxDateUtils.formatDateForDisplay(
                trans.getValidityStartDate(), false);
        this.validityEndDateStr = NvxDateUtils.formatDateForDisplay(
                revalTrans.getNewValidityEndDate(), false);
        this.revalidateDtTmStr = (revalTrans.getTmStatusDate() == null) ? ""
                : NvxDateUtils.formatDateForDisplay(revalTrans.getTmStatusDate(),
                false);
        this.printedDtTimeStr = NvxDateUtils.formatDate(new Date(),
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);

        BigDecimal normalRelPrice = new BigDecimal(
                revalTrans.getRevalFeeInCents()).divide(new BigDecimal(100));
        BigDecimal topupRelPrice = new BigDecimal(
                revalTrans.getRevalTopupFeeInCents())
                .divide(new BigDecimal(100));
        BigDecimal totalRevalFee = TransactionUtil.getTransRevalidateFee(revalTrans);
        this.totalRevalFeeStr = TransactionUtil.priceWithDecimal(totalRevalFee,
                "");
        this.normalRevFeeStr = TransactionUtil.priceWithDecimal(normalRelPrice,
                "");
        this.topupRevFeeStr = TransactionUtil.priceWithDecimal(topupRelPrice,
                "");
        List<PPSLMInventoryTransactionItem> revalItems = null;
        if(revalTrans.getRevalDetail() == null){
            revalItems = trans.getInventoryItems();
        }else{
            revalItems = revalTrans.getRevalItems();
        }
        this.revalGSTStr = TransactionUtil.gstWithDecimal(totalRevalFee,
                revalTrans.getGstRate(), "");
        this.revalExclGSTStr = TransactionUtil.getExclGSTCostWithDecimal(
                totalRevalFee, revalTrans.getGstRate(), "");
        this.revalGSTRateStr = TransactionUtil.getDisplayGSTRate(revalTrans
                .getGstRate());

        this.contactPerson = trans.getMainAccount().getProfile().getContactPerson();
        this.licenseNum = trans.getMainAccount().getProfile().getLicenseNum();
        // prods is used in email
        prods = new ArrayList<ReceiptProdVM>();
        for (PPSLMInventoryTransactionItem item : revalItems) {

            ReceiptProdVM ipord = new ReceiptProdVM(item.getProductId(),item.getProductName());
            if (!prods.contains(ipord)) {
                ipord.addProdItem(revalTrans);
                prods.add(ipord);
            }
        }
        // receiptitems is used in PDF

        this.baseUrl = serverUrl;
        int reprint = revalTrans.getReprintCount();
        this.originalMsg = reprint == 0 ? "ORIGINAL" : "DUPLICATE";
        this.reprintedMsg = reprint == 0 ? "" : ("Reprint Count: " + reprint);

        receiptitems = new ArrayList<ReceiptItemVM>();
        for (PPSLMInventoryTransactionItem item : revalItems) {
            ReceiptItemVM receiptItem = new ReceiptItemVM(item,
                    revalTrans.getGstRate(), normalRelPrice, topupRelPrice);
            receiptitems.add(receiptItem);
        }
        Collections.sort(receiptitems);

        this.dollarsInwords = TransactionUtil
                .convertToWordsWithDecimal(totalRevalFee);
        currency = TransactionUtil.DEFAULT_CURRENCY;
        this.ownAddress = ownAddress;
        this.ownContact = ownContact;
    }

    /**
     * this constructor is for normal receipt(not revalidate receipt)
     *
     * @param trans
     */
    public ReceiptVM(PPSLMInventoryTransaction trans,String serverUrl, String ownAddress,String ownContact) {

        this.baseUrl = serverUrl;
        this.printedDtTimeStr = NvxDateUtils.formatDate(new Date(),
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        if(trans.getTmStatusDate() != null){
            this.transDtStr = NvxDateUtils.formatDateForDisplay(trans.getTmStatusDate(),false);
        }
        int reprint = trans.getReprintCount();
        this.originalMsg = reprint == 0 ? "ORIGINAL" : "DUPLICATE";
        this.reprintedMsg = reprint == 0 ? "" : ("Reprint Count: " + reprint);
        this.accountCode = trans.getMainAccount().getProfile().getAccountCode();
        this.orgName = trans.getMainAccount().getProfile().getOrgName();
        this.address = trans.getMainAccount().getProfile().getAddress();
        this.contactPerson = trans.getMainAccount().getProfile().getContactPerson();
        this.licenseNum = trans.getMainAccount().getProfile().getLicenseNum();
        this.recepitNum = trans.getReceiptNum();
        this.totalStr = TransactionUtil.priceWithDecimal(trans.getTotalAmount(), "");
        this.gstStr = TransactionUtil.gstWithDecimal(trans.getTotalAmount(), trans.getGstRate(), "");
        this.exclGstStr = TransactionUtil.getExclGSTCostWithDecimal(trans.getTotalAmount(), trans.getGstRate(), "");
        this.gstRateStr = TransactionUtil.getDisplayGSTRate(trans.getGstRate());
        this.dollarsInwords = TransactionUtil.convertToWordsWithDecimal(trans.getTotalAmount());

        prods = new ArrayList<ReceiptProdVM>();
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            ReceiptProdVM ipord = new ReceiptProdVM(item.getProductId(),item.getProductName());
            if (!prods.contains(ipord)) {
                ipord.addProdItem(trans);
                prods.add(ipord);
            }
        }

        receiptitems = new ArrayList<ReceiptItemVM>();

        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            ReceiptItemVM receiptItem = new ReceiptItemVM(item,trans.getGstRate());
            receiptitems.add(receiptItem);
        }
        Collections.sort(receiptitems);
        currency = TransactionUtil.DEFAULT_CURRENCY;
        this.ownAddress = ownAddress;
        this.ownContact = ownContact;

        if (PartnerPortalConst.OfflinePayment.equals(trans.getPaymentType())) {
            this.offlinePaymentTxn = true;
            if (OfflinePaymentType.CHEQUE.code.equalsIgnoreCase(trans.getPaymentType())) {
                this.paymentModeText = OfflinePaymentType.CHEQUE.name;
            } else if (OfflinePaymentType.BANK_TRANSFER.code
                    .equalsIgnoreCase(trans.getPaymentType())) {
                this.paymentModeText = OfflinePaymentType.BANK_TRANSFER.name;
            } else {
                this.paymentModeText = "";
            }
        }

    }

    public ReceiptVM(PPSLMInventoryTransaction trans, PPSLMOfflinePaymentRequest req,String serverUrl, String ownAddress,String ownContact) {
        this.baseUrl = serverUrl;
        this.printedDtTimeStr = NvxDateUtils.formatDate(new Date(),
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.transDtStr = NvxDateUtils.formatDateForDisplay(req.getApproveDate(),false);
        int reprint = trans.getReprintCount();
        this.originalMsg = reprint == 0 ? "ORIGINAL" : "DUPLICATE";
        this.reprintedMsg = reprint == 0 ? "" : ("Reprint Count: " + reprint);
        this.accountCode = trans.getMainAccount().getProfile().getAccountCode();
        this.orgName = trans.getMainAccount().getProfile().getOrgName();
        this.address = trans.getMainAccount().getProfile().getAddress();
        this.contactPerson = trans.getMainAccount().getProfile().getContactPerson();
        this.licenseNum = trans.getMainAccount().getProfile().getLicenseNum();
        this.recepitNum = trans.getReceiptNum();
        this.totalStr = TransactionUtil.priceWithDecimal(trans.getTotalAmount(), "");
        this.gstStr = TransactionUtil.gstWithDecimal(trans.getTotalAmount(),trans.getGstRate(), "");
        this.exclGstStr = TransactionUtil.getExclGSTCostWithDecimal(trans.getTotalAmount(),trans.getGstRate(), "");
        this.gstRateStr = TransactionUtil.getDisplayGSTRate(trans.getGstRate());
        this.dollarsInwords = TransactionUtil.convertToWordsWithDecimal(trans.getTotalAmount());

        if (req != null && req.getStatus() != null
                && req.getStatus().trim().length() > 0) {
            this.offlinePaymentTxn = true;
            if (OfflinePaymentType.CHEQUE.code.equalsIgnoreCase(req
                    .getPaymentType())) {
                this.paymentModeText = OfflinePaymentType.CHEQUE.name;
            } else if (OfflinePaymentType.BANK_TRANSFER.code
                    .equalsIgnoreCase(req.getPaymentType())) {
                this.paymentModeText = OfflinePaymentType.BANK_TRANSFER.name;
            } else {
                this.paymentModeText = "";
            }
        }


        prods = new ArrayList<ReceiptProdVM>();
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            ReceiptProdVM ipord = new ReceiptProdVM(item.getProductId(),item.getProductName());
            if (!prods.contains(ipord)) {
                ipord.addProdItem(trans);
                prods.add(ipord);
            }
        }

        receiptitems = new ArrayList<ReceiptItemVM>();

        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            ReceiptItemVM receiptItem = new ReceiptItemVM(item,trans.getGstRate());
            receiptitems.add(receiptItem);
        }
        Collections.sort(receiptitems);
        currency = TransactionUtil.DEFAULT_CURRENCY;
        this.ownAddress = ownAddress;
        this.ownContact = ownContact;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getOriginalMsg() {
        return originalMsg;
    }

    public void setOriginalMsg(String originalMsg) {
        this.originalMsg = originalMsg;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getPrintedDtTimeStr() {
        return printedDtTimeStr;
    }

    public void setPrintedDtTimeStr(String printedDtTimeStr) {
        this.printedDtTimeStr = printedDtTimeStr;
    }

    public String getTransDtStr() {
        return transDtStr;
    }

    public void setTransDtStr(String transDtStr) {
        this.transDtStr = transDtStr;
    }

    public String getReprintedMsg() {
        return reprintedMsg;
    }

    public void setReprintedMsg(String reprintedMsg) {
        this.reprintedMsg = reprintedMsg;
    }

    public List<ReceiptItemVM> getReceiptitems() {
        return receiptitems;
    }

    public void setReceiptitems(List<ReceiptItemVM> receiptitems) {
        this.receiptitems = receiptitems;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLicenseNum() {
        return licenseNum;
    }

    public void setLicenseNum(String licenseNum) {
        this.licenseNum = licenseNum;
    }

    public String getTotalStr() {
        return totalStr;
    }

    public void setTotalStr(String totalStr) {
        this.totalStr = totalStr;
    }

    public String getGstStr() {
        return gstStr;
    }

    public void setGstStr(String gstStr) {
        this.gstStr = gstStr;
    }

    public List<ReceiptProdVM> getProds() {
        return prods;
    }

    public void setProds(List<ReceiptProdVM> prods) {
        this.prods = prods;
    }

    public String getDollarsInwords() {
        return dollarsInwords;
    }

    public void setDollarsInwords(String dollarsInwords) {
        this.dollarsInwords = dollarsInwords;
    }

    public String getPreDefinedBody() {
        return preDefinedBody;
    }

    public void setPreDefinedBody(String preDefinedBody) {
        this.preDefinedBody = preDefinedBody;
    }

    public String getTotalRevalFeeStr() {
        return totalRevalFeeStr;
    }

    public void setTotalRevalFeeStr(String totalRevalFeeStr) {
        this.totalRevalFeeStr = totalRevalFeeStr;
    }

    public String getValidityEndDateStr() {
        return validityEndDateStr;
    }

    public void setValidityEndDateStr(String validityEndDateStr) {
        this.validityEndDateStr = validityEndDateStr;
    }

    public String getNewvalidityEndDateStr() {
        return newvalidityEndDateStr;
    }

    public void setNewvalidityEndDateStr(String newvalidityEndDateStr) {
        this.newvalidityEndDateStr = newvalidityEndDateStr;
    }

    public String getValidateStartDateStr() {
        return validateStartDateStr;
    }

    public void setValidateStartDateStr(String validateStartDateStr) {
        this.validateStartDateStr = validateStartDateStr;
    }

    public String getNewvalidateStartDateStr() {
        return newvalidateStartDateStr;
    }

    public void setNewvalidateStartDateStr(String newvalidateStartDateStr) {
        this.newvalidateStartDateStr = newvalidateStartDateStr;
    }

    public Integer getExtendRevalMonth() {
        return extendRevalMonth;
    }

    public void setExtendRevalMonth(Integer extendRevalMonth) {
        this.extendRevalMonth = extendRevalMonth;
    }

    public String getNormalRevFeeStr() {
        return normalRevFeeStr;
    }

    public void setNormalRevFeeStr(String normalRevFeeStr) {
        this.normalRevFeeStr = normalRevFeeStr;
    }

    public String getTopupRevFeeStr() {
        return topupRevFeeStr;
    }

    public void setTopupRevFeeStr(String topupRevFeeStr) {
        this.topupRevFeeStr = topupRevFeeStr;
    }

    public String getRecepitNum() {
        return recepitNum;
    }

    public void setRecepitNum(String recepitNum) {
        this.recepitNum = recepitNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public String getRevalRecepitNum() {
        return revalRecepitNum;
    }

    public void setRevalRecepitNum(String revalRecepitNum) {
        this.revalRecepitNum = revalRecepitNum;
    }

    public String getRevalidatedBy() {
        return revalidatedBy;
    }

    public void setRevalidatedBy(String revalidatedBy) {
        this.revalidatedBy = revalidatedBy;
    }

    public String getRevalPaymentType() {
        return revalPaymentType;
    }

    public void setRevalPaymentType(String revalPaymentType) {
        this.revalPaymentType = revalPaymentType;
    }

    public String getRevalidateDtTmStr() {
        return revalidateDtTmStr;
    }

    public void setRevalidateDtTmStr(String revalidateDtTmStr) {
        this.revalidateDtTmStr = revalidateDtTmStr;
    }

    public String getExclGstStr() {
        return exclGstStr;
    }

    public void setExclGstStr(String exclGstStr) {
        this.exclGstStr = exclGstStr;
    }

    public String getRevalGSTStr() {
        return revalGSTStr;
    }

    public void setRevalGSTStr(String revalGSTStr) {
        this.revalGSTStr = revalGSTStr;
    }

    public String getRevalExclGSTStr() {
        return revalExclGSTStr;
    }

    public void setRevalExclGSTStr(String revalExclGSTStr) {
        this.revalExclGSTStr = revalExclGSTStr;
    }

    public List<TncVM> getTncs() {
        return tncs;
    }

    public void setTncs(List<TncVM> tncs) {
        this.tncs = tncs;
    }

    public boolean getHasTnc() {
        return hasTnc;
    }

    public void setHasTnc(boolean hasTnc) {
        this.hasTnc = hasTnc;
    }

    public String getRevalGSTRateStr() {
        return revalGSTRateStr;
    }

    public void setRevalGSTRateStr(String revalGSTRateStr) {
        this.revalGSTRateStr = revalGSTRateStr;
    }

    public String getGstRateStr() {
        return gstRateStr;
    }

    public void setGstRateStr(String gstRateStr) {
        this.gstRateStr = gstRateStr;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOwnAddress() {
        return ownAddress;
    }

    public void setOwnAddress(String ownAddress) {
        this.ownAddress = ownAddress;
    }

    public String getOwnContact() {
        return ownContact;
    }

    public void setOwnContact(String ownContact) {
        this.ownContact = ownContact;
    }

    public String getDepositBalanceText() {
        return depositBalanceText;
    }

    public void setDepositBalanceText(String depositBalanceText) {
        this.depositBalanceText = depositBalanceText;
    }

    public boolean isDepositBalanceAvaialble() {
        return depositBalanceAvaialble;
    }

    public void setDepositBalanceAvaialble(boolean depositBalanceAvaialble) {
        this.depositBalanceAvaialble = depositBalanceAvaialble;
    }

    public String getShortCurrency() {
        return shortCurrency;
    }

    public void setShortCurrency(String shortCurrency) {
        this.shortCurrency = shortCurrency;
    }

    public String getPaymentModeText() {
        return paymentModeText;
    }

    public void setPaymentModeText(String paymentModeText) {
        this.paymentModeText = paymentModeText;
    }

    public boolean isOfflinePaymentTxn() {
        return offlinePaymentTxn;
    }

    public void setOfflinePaymentTxn(boolean offlinePaymentTxn) {
        this.offlinePaymentTxn = offlinePaymentTxn;
    }
}
