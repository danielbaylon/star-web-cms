package com.enovax.star.cms.commons.repository.b2cslm;

import com.enovax.star.cms.commons.datamodel.b2cslm.B2CSLMStoreTransactionExtension;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jensen on 15/11/16.
 */
public interface B2CSLMStoreTransactionExtensionRepository extends JpaRepository<B2CSLMStoreTransactionExtension, Integer> {

    B2CSLMStoreTransactionExtension findByReceiptNumber(String receiptNumber);

}
