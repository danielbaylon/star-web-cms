package com.enovax.star.cms.commons.mgnl.form.field.validator.definition;

import com.enovax.star.cms.commons.mgnl.form.field.validator.factory.FieldLengthValidatorFactory;
import info.magnolia.ui.form.validator.definition.ConfiguredFieldValidatorDefinition;

/**
 * Created by jennylynsze on 12/30/16.
 */
public class FieldLengthValidatorDefinition extends ConfiguredFieldValidatorDefinition {
    private int maxlength;
    private String errorMessage;

    public FieldLengthValidatorDefinition() {
        this.setFactoryClass(FieldLengthValidatorFactory.class);
    }

    public int getMaxlength() {
        return maxlength;
    }

    public void setMaxlength(int maxlength) {
        this.maxlength = maxlength;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
