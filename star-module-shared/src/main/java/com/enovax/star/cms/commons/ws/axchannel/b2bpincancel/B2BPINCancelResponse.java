
package com.enovax.star.cms.commons.ws.axchannel.b2bpincancel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="B2BPINCancelResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINCancelResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "b2BPINCancelResult"
})
@XmlRootElement(name = "B2BPINCancelResponse", namespace = "http://tempuri.org/")
public class B2BPINCancelResponse {

    @XmlElementRef(name = "B2BPINCancelResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINCancelResponse> b2BPINCancelResult;

    /**
     * Gets the value of the b2BPINCancelResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINCancelResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINCancelResponse> getB2BPINCancelResult() {
        return b2BPINCancelResult;
    }

    /**
     * Sets the value of the b2BPINCancelResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINCancelResponse }{@code >}
     *     
     */
    public void setB2BPINCancelResult(JAXBElement<SDCB2BPINCancelResponse> value) {
        this.b2BPINCancelResult = value;
    }

}
