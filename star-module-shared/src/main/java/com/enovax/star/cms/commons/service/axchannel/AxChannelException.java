package com.enovax.star.cms.commons.service.axchannel;

public class AxChannelException extends Exception {

    public AxChannelException() {
    }

    public AxChannelException(String message) {
        super(message);
    }

    public AxChannelException(Throwable t) {
        super(t);
    }

    public AxChannelException(String message, Throwable t) {
        super(message, t);
    }

}
