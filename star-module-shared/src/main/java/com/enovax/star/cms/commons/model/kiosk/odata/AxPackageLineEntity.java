package com.enovax.star.cms.commons.model.kiosk.odata;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Justin
 * @since 15 SEP 16
 *
 */
public class AxPackageLineEntity {

    private String absoluteCapacityId;
    private String allowRevisit;
    private String cost;
    private String createdDate;
    private String dataAreaId;
    private String description;
    private String entryWeight;
    private String itemId;
    private String lineGroup;
    private String lineNumber;
    private String modifiedDate;
    private String packageId;
    private String printingBehavior;
    private String recId;
    private String retailVariantId;
    private String revenue;

    public String getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    public void setAbsoluteCapacityId(String absoluteCapacityId) {
        this.absoluteCapacityId = absoluteCapacityId;
    }

    public String getAllowRevisit() {
        return allowRevisit;
    }

    public void setAllowRevisit(String allowRevisit) {
        this.allowRevisit = allowRevisit;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntryWeight() {
        return entryWeight;
    }

    public void setEntryWeight(String entryWeight) {
        this.entryWeight = entryWeight;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getLineGroup() {
        return lineGroup;
    }

    public void setLineGroup(String lineGroup) {
        this.lineGroup = lineGroup;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getPrintingBehavior() {
        return printingBehavior;
    }

    public void setPrintingBehavior(String printingBehavior) {
        this.printingBehavior = printingBehavior;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

}
