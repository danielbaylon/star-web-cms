package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

/**
 * Created by houtao on 4/8/16.
 */
public class AxStarRichMediaLocations {

    private List<AxStarRichMediaLocation> items  = null;

    public List<AxStarRichMediaLocation> getItems() {
        return items;
    }

    public void setItems(List<AxStarRichMediaLocation> items) {
        this.items = items;
    }
}
