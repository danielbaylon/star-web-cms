package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGGSTRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by lavanya on 26/10/16.
 */
@Repository
public interface PPMFLGGSTRateRepository extends JpaRepository<PPMFLGGSTRate, Integer> {
    @Query("from PPMFLGGSTRate where startDate <= ?1 " +
            "and (endDate is null or endDate >= ?1)" +
            "and status = 'A' order by startDate desc")
    PPMFLGGSTRate getGSTRateByDate(Date idate);

    @Query("from PPMFLGGSTRate where status in (?1) order by startDate desc")
    List<PPMFLGGSTRate> listGSTRate(List<String> statuses);

    @Query("from PPMFLGGSTRate where (endDate is null or endDate > ?2) " +
            "and (status = 'A')" +
            "and (?1 is null or id!=?1)")
    List<PPMFLGGSTRate> getOverlappedGstWOEndDate(Integer id, Date startDate);

    @Query("from PPMFLGGSTRate where ((endDate is null and startDate < ?3) or " +
            "(endDate > ?2 and startDate < ?3))" +
            "and (?1 is null or id!=?1)" +
            "and (status = 'A')")
    List<PPMFLGGSTRate> getOverlappedGst(Integer id, Date startDate, Date endDate);
 }
