package com.enovax.star.cms.commons.model.axchannel.pin;

/**
 * Created by houtao on 23/8/16.
 */
public enum AxTransactionItemType {
    NONE("None"),
    KIT("Kit"),
    KIT_COMPONENT("KitComponent");

    private final String value;

    AxTransactionItemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AxTransactionItemType fromValue(String v) {
        for (AxTransactionItemType c: AxTransactionItemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
