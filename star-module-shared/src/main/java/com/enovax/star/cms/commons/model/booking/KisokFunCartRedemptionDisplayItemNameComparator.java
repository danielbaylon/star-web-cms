package com.enovax.star.cms.commons.model.booking;

import java.util.Comparator;

public class KisokFunCartRedemptionDisplayItemNameComparator implements Comparator<KioskFunCartRedemptionDisplayItem> {

    @Override
    public int compare(KioskFunCartRedemptionDisplayItem o1, KioskFunCartRedemptionDisplayItem o2) {
        final int nameCompare = o1.getName().compareTo(o2.getName());
        if (nameCompare != 0) {
            return nameCompare;
        }

        return o1.getLineId().compareTo(o2.getLineId());
    }
}
