package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMReason;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMReasonRepository extends JpaRepository<PPSLMReason, Integer> {

    @Query("from PPSLMReason where status = ?1 ")
    List<PPSLMReason> findAllReasonByStatus(String code);

    @Query("from PPSLMReason where id = ?1 ")
    PPSLMReason findById(Integer id);

    @Query(value="select r from PPSLMReason r where status = 'A' or status='I'")
    Page getReasonByPage(Pageable pageable);

    @Query(value = "select count(r) from PPSLMReason r where status = 'A' or status='I'")
    int getReasonSize();
}
