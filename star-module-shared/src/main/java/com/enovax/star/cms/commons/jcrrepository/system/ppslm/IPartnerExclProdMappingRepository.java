package com.enovax.star.cms.commons.jcrrepository.system.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PartnerExclProdMapping;

import javax.jcr.RepositoryException;
import java.util.List;

/**
 * Created by lavanya on 22/9/16.
 */
public interface IPartnerExclProdMappingRepository {
    public PartnerExclProdMapping savePartnerExclProdMapping(String channel,PartnerExclProdMapping partnerExclProdMapping) throws Exception;

    public PartnerExclProdMapping saveExclProdbyPartner(String channel,Integer partnerId, List<String> exclProdIdList) throws Exception;

    public List<String> getExclProdbyPartner(String channel,Integer partnerId) throws RepositoryException;

    public PartnerExclProdMapping getPartnerExclProdMappingbyPartner(String channel,Integer partnerId) throws  RepositoryException;

    List<String> getPartnerByExclProd(String channel, String productId) throws RepositoryException;

}
