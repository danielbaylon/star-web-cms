
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;


/**
 * <p>Java class for SDC_UpCrossSellLineTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_UpCrossSellLineTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="INVENTDIMID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORIGITEMID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QTY" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="UNITID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPCROSSSELLITEM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPCROSSSELLTABLERECID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="UPSELLTYPE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="VARIANTID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_UpCrossSellLineTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "inventdimid",
    "origitemid",
    "qty",
    "unitid",
    "upcrosssellitem",
    "upcrossselltablerecid",
    "upselltype",
    "variantid"
})
public class SDCUpCrossSellLineTable {

    @XmlElementRef(name = "INVENTDIMID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> inventdimid;
    @XmlElementRef(name = "ORIGITEMID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> origitemid;
    @XmlElement(name = "QTY")
    protected BigDecimal qty;
    @XmlElementRef(name = "UNITID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unitid;
    @XmlElementRef(name = "UPCROSSSELLITEM", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> upcrosssellitem;
    @XmlElement(name = "UPCROSSSELLTABLERECID")
    protected Long upcrossselltablerecid;
    @XmlElement(name = "UPSELLTYPE")
    protected Integer upselltype;
    @XmlElementRef(name = "VARIANTID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> variantid;

    /**
     * Gets the value of the inventdimid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getINVENTDIMID() {
        return inventdimid;
    }

    /**
     * Sets the value of the inventdimid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setINVENTDIMID(JAXBElement<String> value) {
        this.inventdimid = value;
    }

    /**
     * Gets the value of the origitemid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getORIGITEMID() {
        return origitemid;
    }

    /**
     * Sets the value of the origitemid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setORIGITEMID(JAXBElement<String> value) {
        this.origitemid = value;
    }

    /**
     * Gets the value of the qty property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTY() {
        return qty;
    }

    /**
     * Sets the value of the qty property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTY(BigDecimal value) {
        this.qty = value;
    }

    /**
     * Gets the value of the unitid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUNITID() {
        return unitid;
    }

    /**
     * Sets the value of the unitid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUNITID(JAXBElement<String> value) {
        this.unitid = value;
    }

    /**
     * Gets the value of the upcrosssellitem property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUPCROSSSELLITEM() {
        return upcrosssellitem;
    }

    /**
     * Sets the value of the upcrosssellitem property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUPCROSSSELLITEM(JAXBElement<String> value) {
        this.upcrosssellitem = value;
    }

    /**
     * Gets the value of the upcrossselltablerecid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUPCROSSSELLTABLERECID() {
        return upcrossselltablerecid;
    }

    /**
     * Sets the value of the upcrossselltablerecid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUPCROSSSELLTABLERECID(Long value) {
        this.upcrossselltablerecid = value;
    }

    /**
     * Gets the value of the upselltype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUPSELLTYPE() {
        return upselltype;
    }

    /**
     * Sets the value of the upselltype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUPSELLTYPE(Integer value) {
        this.upselltype = value;
    }

    /**
     * Gets the value of the variantid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVARIANTID() {
        return variantid;
    }

    /**
     * Sets the value of the variantid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVARIANTID(JAXBElement<String> value) {
        this.variantid = value;
    }

}
