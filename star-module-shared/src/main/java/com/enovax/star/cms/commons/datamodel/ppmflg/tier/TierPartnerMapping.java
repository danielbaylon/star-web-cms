package com.enovax.star.cms.commons.datamodel.ppmflg.tier;


import com.enovax.star.cms.commons.model.partner.ppmflg.PartnerVM;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 5/26/16.
 */
public class TierPartnerMapping {
    private int id;
    private Integer tierId;
    private ProductTier productTier;
    private String partnerId;
    private PartnerVM partner;
    private Node mappingNode;

    public TierPartnerMapping() {
    }

    public TierPartnerMapping(Node mappingNode) {
        try {
            this.id = Integer.parseInt(mappingNode.getName());
            this.tierId = Integer.parseInt(mappingNode.getProperty("tierId").getString());
            this.partnerId = mappingNode.getProperty("partnerId").getString();
            this.mappingNode = mappingNode;
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getTierId() {
        return tierId;
    }

    public void setTierId(Integer tierId) {
        this.tierId = tierId;
    }

    public ProductTier getProductTier() {
        return productTier;
    }

    public void setProductTier(ProductTier productTier) {
        this.productTier = productTier;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public PartnerVM getPartner() {
        return partner;
    }

    public void setPartner(PartnerVM partner) {
        this.partner = partner;
    }

    public Node getMappingNode() {
        return mappingNode;
    }

    public void setMappingNode(Node mappingNode) {
        this.mappingNode = mappingNode;
    }
}
