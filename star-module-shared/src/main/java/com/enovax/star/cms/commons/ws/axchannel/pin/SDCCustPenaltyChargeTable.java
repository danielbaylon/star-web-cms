
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import java.math.BigDecimal;


/**
 * <p>Java class for SDC_CustPenaltyChargeTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_CustPenaltyChargeTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACCOUNTCODE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ACCOUNTNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATAAREAID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EVENTGROUPID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EVENTLINEID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MARKUPCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MISCCHARGEAMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PENALTYID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RECID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_CustPenaltyChargeTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "accountcode",
    "accountnum",
    "dataareaid",
    "description",
    "eventgroupid",
    "eventlineid",
    "markupcode",
    "miscchargeamount",
    "penaltyid",
    "recid"
})
public class SDCCustPenaltyChargeTable {

    @XmlElement(name = "ACCOUNTCODE")
    protected Integer accountcode;
    @XmlElementRef(name = "ACCOUNTNUM", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountnum;
    @XmlElementRef(name = "DATAAREAID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataareaid;
    @XmlElementRef(name = "DESCRIPTION", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "EVENTGROUPID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventgroupid;
    @XmlElementRef(name = "EVENTLINEID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventlineid;
    @XmlElementRef(name = "MARKUPCODE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> markupcode;
    @XmlElement(name = "MISCCHARGEAMOUNT")
    protected BigDecimal miscchargeamount;
    @XmlElementRef(name = "PENALTYID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> penaltyid;
    @XmlElement(name = "RECID")
    protected Long recid;

    /**
     * Gets the value of the accountcode property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getACCOUNTCODE() {
        return accountcode;
    }

    /**
     * Sets the value of the accountcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setACCOUNTCODE(Integer value) {
        this.accountcode = value;
    }

    /**
     * Gets the value of the accountnum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getACCOUNTNUM() {
        return accountnum;
    }

    /**
     * Sets the value of the accountnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setACCOUNTNUM(JAXBElement<String> value) {
        this.accountnum = value;
    }

    /**
     * Gets the value of the dataareaid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDATAAREAID() {
        return dataareaid;
    }

    /**
     * Sets the value of the dataareaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDATAAREAID(JAXBElement<String> value) {
        this.dataareaid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDESCRIPTION() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDESCRIPTION(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the eventgroupid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEVENTGROUPID() {
        return eventgroupid;
    }

    /**
     * Sets the value of the eventgroupid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEVENTGROUPID(JAXBElement<String> value) {
        this.eventgroupid = value;
    }

    /**
     * Gets the value of the eventlineid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEVENTLINEID() {
        return eventlineid;
    }

    /**
     * Sets the value of the eventlineid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEVENTLINEID(JAXBElement<String> value) {
        this.eventlineid = value;
    }

    /**
     * Gets the value of the markupcode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMARKUPCODE() {
        return markupcode;
    }

    /**
     * Sets the value of the markupcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMARKUPCODE(JAXBElement<String> value) {
        this.markupcode = value;
    }

    /**
     * Gets the value of the miscchargeamount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMISCCHARGEAMOUNT() {
        return miscchargeamount;
    }

    /**
     * Sets the value of the miscchargeamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMISCCHARGEAMOUNT(BigDecimal value) {
        this.miscchargeamount = value;
    }

    /**
     * Gets the value of the penaltyid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPENALTYID() {
        return penaltyid;
    }

    /**
     * Sets the value of the penaltyid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPENALTYID(JAXBElement<String> value) {
        this.penaltyid = value;
    }

    /**
     * Gets the value of the recid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRECID() {
        return recid;
    }

    /**
     * Sets the value of the recid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRECID(Long value) {
        this.recid = value;
    }

}
