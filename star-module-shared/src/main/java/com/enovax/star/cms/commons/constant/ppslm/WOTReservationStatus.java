package com.enovax.star.cms.commons.constant.ppslm;

/**
 * Created by jennylynsze on 6/25/16.
 */
public enum WOTReservationStatus {

    Pending("Pending"), /* pending for save wot order */
    Discarded("Discarded"), /* user cancelled split */
    Failed("Failed"), /* save wot order failed */
    Reserved("Reserved"), /* admin reserved ticket will be become to purchased once partner has purchased any no of tickets from the reservation */
    PartiallyPurchased("Reserved"), /* admin reserved ticket will be become to purchased once partner has purchased any no of tickets from the reservation */
    FullyPurchased("Reserved"), /* admin reserved ticket will be become to purchased once partner has purchased any no of tickets from the reservation */
    Released("Released"), /* admin reserved ticket will be become released X days before the show time */
    Confirmed("Confirmed"), /* default status of purchased successfully */
    Cancelled("Cancelled"), /* partner cancelled reservation or admin cancelled backend reservation */
    PartiallyRedeemed("Partially Redeemed"), /* ticket partially redeemed */
    FullyRedeemed("Fully Redeemed"); /* ticket fully redeemed */

    public String displayName;

    WOTReservationStatus(String displayName) {
        this.displayName = displayName;
    }

}
