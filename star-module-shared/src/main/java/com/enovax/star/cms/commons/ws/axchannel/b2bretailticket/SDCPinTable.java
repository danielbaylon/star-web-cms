
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for SDC_PinTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_PinTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ALLOWPARTIALREDEMPTION" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CHANNELID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CREDITCARDDIGITS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CUSTACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATAAREAID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENDDATETIME" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="GUESTNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISGROUPTICKET" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LEGALENTITYDATAAREAID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MEDIATYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PACKAGENAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PINTYPE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="QTYPERPRODUCT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="REFERENCEID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REPLICATIONCOUNTERFROMORIGIN" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SDC_PinLineCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_PinLine" minOccurs="0"/>
 *         &lt;element name="SOURCETYPE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="STARTDATETIME" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TRANSACTIONID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_PinTable", propOrder = {
    "allowpartialredemption",
    "channelid",
    "creditcarddigits",
    "custaccount",
    "dataareaid",
    "description",
    "enddatetime",
    "guestname",
    "isgroupticket",
    "legalentitydataareaid",
    "mediatype",
    "packagename",
    "pincode",
    "pintype",
    "qtyperproduct",
    "referenceid",
    "replicationcounterfromorigin",
    "sdcPinLineCollection",
    "sourcetype",
    "startdatetime",
    "transactionid"
})
public class SDCPinTable {

    @XmlElement(name = "ALLOWPARTIALREDEMPTION")
    protected Integer allowpartialredemption;
    @XmlElement(name = "CHANNELID")
    protected Long channelid;
    @XmlElementRef(name = "CREDITCARDDIGITS", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditcarddigits;
    @XmlElementRef(name = "CUSTACCOUNT", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custaccount;
    @XmlElementRef(name = "DATAAREAID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataareaid;
    @XmlElementRef(name = "DESCRIPTION", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElement(name = "ENDDATETIME")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enddatetime;
    @XmlElementRef(name = "GUESTNAME", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> guestname;
    @XmlElement(name = "ISGROUPTICKET")
    protected Integer isgroupticket;
    @XmlElementRef(name = "LEGALENTITYDATAAREAID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> legalentitydataareaid;
    @XmlElementRef(name = "MEDIATYPE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mediatype;
    @XmlElementRef(name = "PACKAGENAME", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> packagename;
    @XmlElementRef(name = "PINCODE", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pincode;
    @XmlElement(name = "PINTYPE")
    protected Integer pintype;
    @XmlElement(name = "QTYPERPRODUCT")
    protected BigDecimal qtyperproduct;
    @XmlElementRef(name = "REFERENCEID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> referenceid;
    @XmlElement(name = "REPLICATIONCOUNTERFROMORIGIN")
    protected Integer replicationcounterfromorigin;
    @XmlElementRef(name = "SDC_PinLineCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCPinLine> sdcPinLineCollection;
    @XmlElement(name = "SOURCETYPE")
    protected Integer sourcetype;
    @XmlElement(name = "STARTDATETIME")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startdatetime;
    @XmlElementRef(name = "TRANSACTIONID", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionid;

    /**
     * Gets the value of the allowpartialredemption property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getALLOWPARTIALREDEMPTION() {
        return allowpartialredemption;
    }

    /**
     * Sets the value of the allowpartialredemption property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setALLOWPARTIALREDEMPTION(Integer value) {
        this.allowpartialredemption = value;
    }

    /**
     * Gets the value of the channelid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCHANNELID() {
        return channelid;
    }

    /**
     * Sets the value of the channelid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCHANNELID(Long value) {
        this.channelid = value;
    }

    /**
     * Gets the value of the creditcarddigits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCREDITCARDDIGITS() {
        return creditcarddigits;
    }

    /**
     * Sets the value of the creditcarddigits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCREDITCARDDIGITS(JAXBElement<String> value) {
        this.creditcarddigits = value;
    }

    /**
     * Gets the value of the custaccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCUSTACCOUNT() {
        return custaccount;
    }

    /**
     * Sets the value of the custaccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCUSTACCOUNT(JAXBElement<String> value) {
        this.custaccount = value;
    }

    /**
     * Gets the value of the dataareaid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDATAAREAID() {
        return dataareaid;
    }

    /**
     * Sets the value of the dataareaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDATAAREAID(JAXBElement<String> value) {
        this.dataareaid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDESCRIPTION() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDESCRIPTION(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the enddatetime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENDDATETIME() {
        return enddatetime;
    }

    /**
     * Sets the value of the enddatetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENDDATETIME(XMLGregorianCalendar value) {
        this.enddatetime = value;
    }

    /**
     * Gets the value of the guestname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGUESTNAME() {
        return guestname;
    }

    /**
     * Sets the value of the guestname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGUESTNAME(JAXBElement<String> value) {
        this.guestname = value;
    }

    /**
     * Gets the value of the isgroupticket property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getISGROUPTICKET() {
        return isgroupticket;
    }

    /**
     * Sets the value of the isgroupticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setISGROUPTICKET(Integer value) {
        this.isgroupticket = value;
    }

    /**
     * Gets the value of the legalentitydataareaid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLEGALENTITYDATAAREAID() {
        return legalentitydataareaid;
    }

    /**
     * Sets the value of the legalentitydataareaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLEGALENTITYDATAAREAID(JAXBElement<String> value) {
        this.legalentitydataareaid = value;
    }

    /**
     * Gets the value of the mediatype property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMEDIATYPE() {
        return mediatype;
    }

    /**
     * Sets the value of the mediatype property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMEDIATYPE(JAXBElement<String> value) {
        this.mediatype = value;
    }

    /**
     * Gets the value of the packagename property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPACKAGENAME() {
        return packagename;
    }

    /**
     * Sets the value of the packagename property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPACKAGENAME(JAXBElement<String> value) {
        this.packagename = value;
    }

    /**
     * Gets the value of the pincode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPINCODE() {
        return pincode;
    }

    /**
     * Sets the value of the pincode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPINCODE(JAXBElement<String> value) {
        this.pincode = value;
    }

    /**
     * Gets the value of the pintype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPINTYPE() {
        return pintype;
    }

    /**
     * Sets the value of the pintype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPINTYPE(Integer value) {
        this.pintype = value;
    }

    /**
     * Gets the value of the qtyperproduct property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQTYPERPRODUCT() {
        return qtyperproduct;
    }

    /**
     * Sets the value of the qtyperproduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQTYPERPRODUCT(BigDecimal value) {
        this.qtyperproduct = value;
    }

    /**
     * Gets the value of the referenceid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getREFERENCEID() {
        return referenceid;
    }

    /**
     * Sets the value of the referenceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setREFERENCEID(JAXBElement<String> value) {
        this.referenceid = value;
    }

    /**
     * Gets the value of the replicationcounterfromorigin property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getREPLICATIONCOUNTERFROMORIGIN() {
        return replicationcounterfromorigin;
    }

    /**
     * Sets the value of the replicationcounterfromorigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setREPLICATIONCOUNTERFROMORIGIN(Integer value) {
        this.replicationcounterfromorigin = value;
    }

    /**
     * Gets the value of the sdcPinLineCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCPinLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCPinLine> getSDCPinLineCollection() {
        return sdcPinLineCollection;
    }

    /**
     * Sets the value of the sdcPinLineCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCPinLine }{@code >}
     *     
     */
    public void setSDCPinLineCollection(JAXBElement<ArrayOfSDCPinLine> value) {
        this.sdcPinLineCollection = value;
    }

    /**
     * Gets the value of the sourcetype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSOURCETYPE() {
        return sourcetype;
    }

    /**
     * Sets the value of the sourcetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSOURCETYPE(Integer value) {
        this.sourcetype = value;
    }

    /**
     * Gets the value of the startdatetime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTARTDATETIME() {
        return startdatetime;
    }

    /**
     * Sets the value of the startdatetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTARTDATETIME(XMLGregorianCalendar value) {
        this.startdatetime = value;
    }

    /**
     * Gets the value of the transactionid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTRANSACTIONID() {
        return transactionid;
    }

    /**
     * Sets the value of the transactionid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTRANSACTIONID(JAXBElement<String> value) {
        this.transactionid = value;
    }

}
