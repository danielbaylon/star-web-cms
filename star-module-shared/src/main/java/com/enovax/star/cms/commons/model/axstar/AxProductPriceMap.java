package com.enovax.star.cms.commons.model.axstar;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by houtao on 5/9/16.
 */
public class AxProductPriceMap implements Serializable {

    //Key = listingId
    //Value = Ax Product Price
    private Map<String, List<AxProductPrice>> productPriceMap = new HashMap<String, List<AxProductPrice>>();

    public Map<String, List<AxProductPrice>> getProductPriceMap() {
        return productPriceMap;
    }

    public void setProductPriceMap(Map<String, List<AxProductPrice>> productPriceMap) {
        this.productPriceMap = productPriceMap;
    }
}
