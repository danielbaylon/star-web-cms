package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AxStarCartLine {

    //Unused variables

//    private String DeliveryMode; // private String private String ,
//    private String DeliveryModeChargeAmount; // null,
//    private String RequestedDeliveryDate; // null,
//    private String QuantityOrdered; // null,
//    private String QuantityInvoiced; // null,
//    private String OriginalPrice; // null,
//    private String PromotionLines; // [],
//    private String ReasonCodeLines; // [],
//    private String ChargeLines; // [],
//    private String ExtensionProperties; // []

    private String lineId;
    private String taxOverrideCode = ""; //Nullable
    private String itemId;
    private String barcode;
    private String description;
    private String inventoryDimensionId;
    private String comment;
    private Long productId;
    private String warehouseId;
    private int quantity;
    private BigDecimal price;
    private BigDecimal extendedPrice;
    private BigDecimal taxAmount;
    private String itemTaxGroupId;
    private BigDecimal totalAmount;
    private BigDecimal netAmountWithoutTax;
    private BigDecimal discountAmount;
    private BigDecimal lineDiscount;
    private BigDecimal linePercentageDiscount;
    private BigDecimal lineManualDiscountPercentage;
    private BigDecimal lineManualDiscountAmount;
    private String unitOfMeasureSymbol;
    private AxStarAddress shippingAddress; //Nullable
    private String returnTransactionId;
    private Long returnLineNumber;
    private String returnInventTransId;
    private boolean isVoided;
    private boolean isGiftCardLine;
    private int salesStatusValue;
    private String storeNumber;
    private String serialNumber;
    private boolean isPriceOverridden;
    private boolean isInvoiceLine;
    private String invoiceId;
    private BigDecimal invoiceAmount;
    private BigDecimal taxRatePercent;
    private boolean isCustomerAccountDeposit;
    private List<AxStartCartLineDiscountLine> discountLines = new ArrayList<>();
    private Map<String, String> stringExtensionProperties;
    
    /*
    [Sample JSON]
    {
        "LineId": "532606c6a2504b91870cfb63ff2a6b80",
        "TaxOverrideCode": null,
        "ItemId": "1010100001",
        "Barcode": "",
        "Description": "",
        "InventoryDimensionId": "",
        "Comment": "",
        "ProductId": 5637149107,
        "WarehouseId": "SLMB2C",
        "Quantity": 5,
        "Price": 4,
        "ExtendedPrice": 20,
        "TaxAmount": 0,
        "ItemTaxGroupId": "",
        "TotalAmount": 20,
        "NetAmountWithoutTax": 20,
        "DiscountAmount": 0,
        "LineDiscount": 0,
        "LinePercentageDiscount": 0,
        "LineManualDiscountPercentage": 0,
        "LineManualDiscountAmount": 0,
        "UnitOfMeasureSymbol": "Ea",
        "ShippingAddress": null,
        "DeliveryMode": "",
        "DeliveryModeChargeAmount": null,
        "RequestedDeliveryDate": null,
        "ReturnTransactionId": "",
        "ReturnLineNumber": 0,
        "ReturnInventTransId": "",
        "IsVoided": false,
        "IsGiftCardLine": false,
        "SalesStatusValue": 0,
        "QuantityOrdered": null,
        "QuantityInvoiced": null,
        "StoreNumber": "",
        "SerialNumber": "",
        "IsPriceOverridden": false,
        "OriginalPrice": null,
        "IsInvoiceLine": false,
        "InvoiceId": "",
        "InvoiceAmount": 0,
        "PromotionLines": [],
        "DiscountLines": [],
        "ReasonCodeLines": [],
        "ChargeLines": [],
        "TaxRatePercent": 0,
        "IsCustomerAccountDeposit": false,
        "ExtensionProperties": []
      }
     */

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInventoryDimensionId() {
        return inventoryDimensionId;
    }

    public void setInventoryDimensionId(String inventoryDimensionId) {
        this.inventoryDimensionId = inventoryDimensionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(BigDecimal extendedPrice) {
        this.extendedPrice = extendedPrice;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getItemTaxGroupId() {
        return itemTaxGroupId;
    }

    public void setItemTaxGroupId(String itemTaxGroupId) {
        this.itemTaxGroupId = itemTaxGroupId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getNetAmountWithoutTax() {
        return netAmountWithoutTax;
    }

    public void setNetAmountWithoutTax(BigDecimal netAmountWithoutTax) {
        this.netAmountWithoutTax = netAmountWithoutTax;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(BigDecimal lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public BigDecimal getLinePercentageDiscount() {
        return linePercentageDiscount;
    }

    public void setLinePercentageDiscount(BigDecimal linePercentageDiscount) {
        this.linePercentageDiscount = linePercentageDiscount;
    }

    public BigDecimal getLineManualDiscountPercentage() {
        return lineManualDiscountPercentage;
    }

    public void setLineManualDiscountPercentage(BigDecimal lineManualDiscountPercentage) {
        this.lineManualDiscountPercentage = lineManualDiscountPercentage;
    }

    public BigDecimal getLineManualDiscountAmount() {
        return lineManualDiscountAmount;
    }

    public void setLineManualDiscountAmount(BigDecimal lineManualDiscountAmount) {
        this.lineManualDiscountAmount = lineManualDiscountAmount;
    }

    public String getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public void setUnitOfMeasureSymbol(String unitOfMeasureSymbol) {
        this.unitOfMeasureSymbol = unitOfMeasureSymbol;
    }

    public AxStarAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AxStarAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getReturnTransactionId() {
        return returnTransactionId;
    }

    public void setReturnTransactionId(String returnTransactionId) {
        this.returnTransactionId = returnTransactionId;
    }

    public Long getReturnLineNumber() {
        return returnLineNumber;
    }

    public void setReturnLineNumber(Long returnLineNumber) {
        this.returnLineNumber = returnLineNumber;
    }

    public String getReturnInventTransId() {
        return returnInventTransId;
    }

    public void setReturnInventTransId(String returnInventTransId) {
        this.returnInventTransId = returnInventTransId;
    }

    public boolean isVoided() {
        return isVoided;
    }

    public void setVoided(boolean voided) {
        isVoided = voided;
    }

    public boolean isGiftCardLine() {
        return isGiftCardLine;
    }

    public void setGiftCardLine(boolean giftCardLine) {
        isGiftCardLine = giftCardLine;
    }

    public int getSalesStatusValue() {
        return salesStatusValue;
    }

    public void setSalesStatusValue(int salesStatusValue) {
        this.salesStatusValue = salesStatusValue;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public boolean isPriceOverridden() {
        return isPriceOverridden;
    }

    public void setPriceOverridden(boolean priceOverridden) {
        isPriceOverridden = priceOverridden;
    }

    public boolean isInvoiceLine() {
        return isInvoiceLine;
    }

    public void setInvoiceLine(boolean invoiceLine) {
        isInvoiceLine = invoiceLine;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public BigDecimal getTaxRatePercent() {
        return taxRatePercent;
    }

    public void setTaxRatePercent(BigDecimal taxRatePercent) {
        this.taxRatePercent = taxRatePercent;
    }

    public boolean isCustomerAccountDeposit() {
        return isCustomerAccountDeposit;
    }

    public void setCustomerAccountDeposit(boolean customerAccountDeposit) {
        isCustomerAccountDeposit = customerAccountDeposit;
    }

    public List<AxStartCartLineDiscountLine> getDiscountLines() {
        return discountLines;
    }

    public void setDiscountLines(List<AxStartCartLineDiscountLine> discountLines) {
        this.discountLines = discountLines;
    }

    public Map<String, String> getStringExtensionProperties() {
        return stringExtensionProperties;
    }

    public void setStringExtensionProperties(Map<String, String> stringExtensionProperties) {
        this.stringExtensionProperties = stringExtensionProperties;
    }
}
