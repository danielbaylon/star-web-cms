package com.enovax.star.cms.commons.repository.ppmflg;


import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGReasonLogMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGPPMFLGReasonLogMapRepository extends JpaRepository<PPMFLGReasonLogMap, Integer> {

    @Query("from PPMFLGReasonLogMap where logId = ?1")
    List<PPMFLGReasonLogMap> findByLogId(Integer id);
}
