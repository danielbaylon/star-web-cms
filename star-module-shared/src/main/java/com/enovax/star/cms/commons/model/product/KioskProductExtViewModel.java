package com.enovax.star.cms.commons.model.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.enovax.star.cms.commons.model.kiosk.odata.AxEventAllocationEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroupLineEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxEventGroupTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtEntity;
import com.enovax.star.cms.commons.util.NvxDateUtils;

public class KioskProductExtViewModel extends ProductExtViewModel {

    public static KioskProductExtViewModel fromExtData(AxInventTableExtEntity data) {
        final KioskProductExtViewModel model = new KioskProductExtViewModel();
        model.setPackage(data.isPackage());

        model.setTicketing(data.isTicketing());
        model.setTransport(data.isTransport());
        model.setItemId(data.getItemId());
        model.setProductId(data.getProductId());
        model.setMediaTypeId(data.getMediaTypeId());
        model.setOpenValidityEndDate(data.getOpenEndDateTime(NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        model.setOpenValidityStartDate(data.getOpenStartDateTime(NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
        model.setPrinting(data.getPrinting());
        model.setPrinterType(data.getPrinterType());
        model.setNoOfPax(StringUtils.isBlank(data.getNOOFPAX()) ? 0 : new Integer(data.getNOOFPAX()));
        model.setTemplateName(data.getTemplateName());
        model.setProductId(data.getProduct());
        final String eventGroupId = data.getEventGroupId();
        model.setOpenDate("1".equals(data.getDefineOpenDate()) ? Boolean.TRUE : Boolean.FALSE);
        model.setCapacity("1".equals(data.getIsCapacity()) ? Boolean.TRUE : Boolean.FALSE);
        model.setPackage("1".equals(data.getIsPackage()) ? Boolean.TRUE : Boolean.FALSE);
        if (model.isCapacity()) {
            model.setEvent(Boolean.TRUE);
        } else {
            model.setEvent(Boolean.FALSE);
        }
        if (StringUtils.isNotBlank(data.getEventGroupId()) && model.isPackage()) {
            model.setEvent(Boolean.TRUE);
            model.setCapacity(Boolean.TRUE);
        }

        for (AxEventGroupTableEntity eventGroup : data.getEventGroups()) {
            model.setEventGroupId(eventGroupId);
            model.setSingleEvent(eventGroup.isSingleEvent());
            if (model.isPackage() && eventGroup.isSingleEvent() && StringUtils.isNotBlank(eventGroup.getEventGroupId())) {
                model.setCapacity(Boolean.TRUE);
                model.setEvent(Boolean.TRUE);
            } else if (!model.isPackage() && model.isCapacity() && eventGroup.isSingleEvent()) {
                model.setCapacity(Boolean.TRUE);
                model.setEvent(Boolean.TRUE);
            }
            model.setDefaultEventLineId(data.getDefaultEventLineId());

            final List<ProductExtViewEventLine> eventLines = new ArrayList<>();
            for (AxEventGroupLineEntity groupLine : eventGroup.getEventGroupLines()) {
                final ProductExtViewEventLine line = new ProductExtViewEventLine();
                line.setEventLineId(groupLine.getEventLineId());
                line.setEventName(groupLine.getEventName());
                // TODO to check the return value as it is defined as
                // Integer not sure seconds or mini seconds
                line.setEventStartTime(new Integer(groupLine.getEventStartTime()));
                line.setEventEndTime(new Integer(groupLine.getEventEndTime()));
                line.setEventCapacityId(groupLine.getEventCapacityId());

                final List<ProductExtViewEventAllocation> eventDates = new ArrayList<>();
                for (AxEventAllocationEntity alloc : groupLine.getEventAllocations()) {
                    final ProductExtViewEventAllocation eventDate = new ProductExtViewEventAllocation();
                    eventDate.setEventLineId(alloc.getEventLineId());
                    eventDate.setEventGroupId(alloc.getEventGroupId());
                    eventDate.setRawDate(alloc.getEventDate(NvxDateUtils.AX_DEFAULT_DATE_TIME_FORMAT));
                    eventDate.setDate(NvxDateUtils.formatDate(eventDate.getRawDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));

                    eventDates.add(eventDate);
                }
                line.setEventDates(eventDates);

                eventLines.add(line);
            }
            model.setEventLines(eventLines);

        }

        return model;
    }

}
