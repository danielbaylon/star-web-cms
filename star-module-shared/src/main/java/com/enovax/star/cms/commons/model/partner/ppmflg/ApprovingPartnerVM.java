package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.util.List;

public class ApprovingPartnerVM {

    private Boolean canApprove = true;
    private Boolean showRemarks = true;
    private ApprovalLogVM appLogVM;
    private PartnerVM partnerVM;
    private List<ReasonVM> reasonVM;
    private List<ReasonVM> appLogReasonVM;
    private PartnerAccountVM partnerAccountVM;

    public Boolean getCanApprove() {
        return canApprove;
    }

    public void setCanApprove(Boolean canApprove) {
        this.canApprove = canApprove;
    }

    public Boolean getShowRemarks() {
        return showRemarks;
    }

    public void setShowRemarks(Boolean showRemarks) {
        this.showRemarks = showRemarks;
    }

    public ApprovalLogVM getAppLogVM() {
        return appLogVM;
    }

    public void setAppLogVM(ApprovalLogVM appLogVM) {
        this.appLogVM = appLogVM;
    }

    public PartnerVM getPartnerVM() {
        return partnerVM;
    }

    public void setPartnerVM(PartnerVM partnerVM) {
        this.partnerVM = partnerVM;
    }

    public List<ReasonVM> getReasonVM() {
        return reasonVM;
    }

    public void setReasonVM(List<ReasonVM> reasonVM) {
        this.reasonVM = reasonVM;
    }

    public List<ReasonVM> getAppLogReasonVM() {
        return appLogReasonVM;
    }

    public void setAppLogReasonVM(List<ReasonVM> appLogReasonVM) {
        this.appLogReasonVM = appLogReasonVM;
    }

    public PartnerAccountVM getPartnerAccountVM() {
        return partnerAccountVM;
    }

    public void setPartnerAccountVM(PartnerAccountVM partnerAccountVM) {
        this.partnerAccountVM = partnerAccountVM;
    }
}
