package com.enovax.star.cms.commons.model.axchannel.customerext;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 26/9/16.
 */
public class AxCustomerTable {

    private String accountMgrCMS;
    private String accountNum;
    private Integer allowOnAccount;
    private String contactPersonStr;
    private String designation;
    private String documentLinkDelimited;
    private List<AxDocumentLink> documentLinks;
    private String fax;
    private String faxExtension;
    private String marketDistributionDelimited;
    private List<AxMarketDistribution> marketDistributions;
    private Date registeredDate;
    private Integer reservationAmendment;
    private String uenNumber;
    private Date taExpiryDate;
    private String taLicenseNumber;
    private String branchName;
    private String capacityGroupId;
    private String countriesInterestedIn;
    private Double dailyTransCapValue;
    private Integer enablePartnerPortal;
    private String languageId;
    private String mainAccLoginName;
    private String officeNo;
    private String revalidationFee;
    private Integer revalidationPeriod;
    private String lineOfBusiness;
    private String countryCode;


    public String getAccountMgrCMS() {
        return accountMgrCMS;
    }

    public void setAccountMgrCMS(String accountMgrCMS) {
        this.accountMgrCMS = accountMgrCMS;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public Integer getAllowOnAccount() {
        return allowOnAccount;
    }

    public void setAllowOnAccount(Integer allowOnAccount) {
        this.allowOnAccount = allowOnAccount;
    }

    public String getContactPersonStr() {
        return contactPersonStr;
    }

    public void setContactPersonStr(String contactPersonStr) {
        this.contactPersonStr = contactPersonStr;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDocumentLinkDelimited() {
        return documentLinkDelimited;
    }

    public void setDocumentLinkDelimited(String documentLinkDelimited) {
        this.documentLinkDelimited = documentLinkDelimited;
    }

    public List<AxDocumentLink> getDocumentLinks() {
        return documentLinks;
    }

    public void setDocumentLinks(List<AxDocumentLink> documentLinks) {
        this.documentLinks = documentLinks;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFaxExtension() {
        return faxExtension;
    }

    public void setFaxExtension(String faxExtension) {
        this.faxExtension = faxExtension;
    }

    public String getMarketDistributionDelimited() {
        return marketDistributionDelimited;
    }

    public void setMarketDistributionDelimited(String marketDistributionDelimited) {
        this.marketDistributionDelimited = marketDistributionDelimited;
    }

    public List<AxMarketDistribution> getMarketDistributions() {
        return marketDistributions;
    }

    public void setMarketDistributions(List<AxMarketDistribution> marketDistributions) {
        this.marketDistributions = marketDistributions;
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }

    public Integer getReservationAmendment() {
        return reservationAmendment;
    }

    public void setReservationAmendment(Integer reservationAmendment) {
        this.reservationAmendment = reservationAmendment;
    }

    public String getUenNumber() {
        return uenNumber;
    }

    public void setUenNumber(String uenNumber) {
        this.uenNumber = uenNumber;
    }

    public Date getTaExpiryDate() {
        return taExpiryDate;
    }

    public void setTaExpiryDate(Date taExpiryDate) {
        this.taExpiryDate = taExpiryDate;
    }

    public String getTaLicenseNumber() {
        return taLicenseNumber;
    }

    public void setTaLicenseNumber(String taLicenseNumber) {
        this.taLicenseNumber = taLicenseNumber;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCapacityGroupId() {
        return capacityGroupId;
    }

    public void setCapacityGroupId(String capacityGroupId) {
        this.capacityGroupId = capacityGroupId;
    }

    public String getCountriesInterestedIn() {
        return countriesInterestedIn;
    }

    public void setCountriesInterestedIn(String countriesInterestedIn) {
        this.countriesInterestedIn = countriesInterestedIn;
    }

    public Double getDailyTransCapValue() {
        return dailyTransCapValue;
    }

    public void setDailyTransCapValue(Double dailyTransCapValue) {
        this.dailyTransCapValue = dailyTransCapValue;
    }

    public Integer getEnablePartnerPortal() {
        return enablePartnerPortal;
    }

    public void setEnablePartnerPortal(Integer enablePartnerPortal) {
        this.enablePartnerPortal = enablePartnerPortal;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getMainAccLoginName() {
        return mainAccLoginName;
    }

    public void setMainAccLoginName(String mainAccLoginName) {
        this.mainAccLoginName = mainAccLoginName;
    }

    public String getOfficeNo() {
        return officeNo;
    }

    public void setOfficeNo(String officeNo) {
        this.officeNo = officeNo;
    }

    public String getRevalidationFee() {
        return revalidationFee;
    }

    public void setRevalidationFee(String revalidationFee) {
        this.revalidationFee = revalidationFee;
    }

    public Integer getRevalidationPeriod() {
        return revalidationPeriod;
    }

    public void setRevalidationPeriod(Integer revalidationPeriod) {
        this.revalidationPeriod = revalidationPeriod;
    }

    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    public void setLineOfBusiness(String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
