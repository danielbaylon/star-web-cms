
package com.enovax.star.cms.commons.ws.axretail;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axretail package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _SDCCartRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketTable");
    private final static QName _SDCCartTicketListCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CartTicketListCriteria");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _SDCCartRetailTicketItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketItem");
    private final static QName _ArrayOfSDCCartTicketListCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CartTicketListCriteria");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_RetailTicketTable");
    private final static QName _ArrayOfSDCCartRetailTicketItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketItem");
    private final static QName _ArrayOfSDCRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_RetailTicketTable");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _SDCCartRetailTicketTableResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_CartRetailTicketTableResponse");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _SDCRetailTicketTableResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_RetailTicketTableResponse");
    private final static QName _CartCheckoutCompleteTransactionId_QNAME = new QName("http://tempuri.org/", "transactionId");
    private final static QName _SDCCartRetailTicketTableCartRetailTicketTableEntityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CartRetailTicketTableEntityCollection");
    private final static QName _SDCCartRetailTicketTableErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ErrorMessage");
    private final static QName _SDCCartRetailTicketTableTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TransactionId");
    private final static QName _SDCRetailTicketTableResponseRetailTicketTableCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RetailTicketTableCollection");
    private final static QName _CartValidateAndReserveQuantityCartTicketListCriteria_QNAME = new QName("http://tempuri.org/", "CartTicketListCriteria");
    private final static QName _SDCCartTicketListCriteriaEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventGroupId");
    private final static QName _SDCCartTicketListCriteriaItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCCartTicketListCriteriaTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionId");
    private final static QName _SDCCartTicketListCriteriaRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RetailVariantId");
    private final static QName _SDCCartTicketListCriteriaEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventLineId");
    private final static QName _SDCCartTicketListCriteriaLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineId");
    private final static QName _SDCCartTicketListCriteriaDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DataAreaId");
    private final static QName _SDCCartTicketListCriteriaAccountNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "AccountNum");
    private final static QName _CartValidateQuantityResponseCartValidateQuantityResult_QNAME = new QName("http://tempuri.org/", "CartValidateQuantityResult");
    private final static QName _RetailTicketSearchResponseRetailTicketSearchResult_QNAME = new QName("http://tempuri.org/", "RetailTicketSearchResult");
    private final static QName _CartValidateAndReserveQuantityResponseCartValidateAndReserveQuantityResult_QNAME = new QName("http://tempuri.org/", "CartValidateAndReserveQuantityResult");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _SDCRetailTicketTableUsageValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "UsageValidityId");
    private final static QName _SDCRetailTicketTableTicketTableId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketTableId");
    private final static QName _SDCRetailTicketTableEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventLineId");
    private final static QName _SDCRetailTicketTableProductName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductName");
    private final static QName _SDCRetailTicketTableDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DataAreaId");
    private final static QName _SDCRetailTicketTableEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupId");
    private final static QName _SDCRetailTicketTableOrigTicketCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OrigTicketCode");
    private final static QName _SDCRetailTicketTableItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ItemId");
    private final static QName _SDCRetailTicketTablePinCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PinCode");
    private final static QName _SDCRetailTicketTableTicketCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketCode");
    private final static QName _SDCRetailTicketTableOpenValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OpenValidityId");
    private final static QName _CartCheckoutCompleteResponseCartCheckoutCompleteResult_QNAME = new QName("http://tempuri.org/", "CartCheckoutCompleteResult");
    private final static QName _CartCheckoutCancelResponseCartCheckoutCancelResult_QNAME = new QName("http://tempuri.org/", "CartCheckoutCancelResult");
    private final static QName _SDCCartRetailTicketTableResponseCartRetailTicketCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "CartRetailTicketCollection");
    private final static QName _CartCheckoutStartOnlineResponseCartCheckoutStartOnlineResult_QNAME = new QName("http://tempuri.org/", "CartCheckoutStartOnlineResult");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axretail
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link SDCRetailTicketTableResponse }
     * 
     */
    public SDCRetailTicketTableResponse createSDCRetailTicketTableResponse() {
        return new SDCRetailTicketTableResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketTableResponse }
     * 
     */
    public SDCCartRetailTicketTableResponse createSDCCartRetailTicketTableResponse() {
        return new SDCCartRetailTicketTableResponse();
    }

    /**
     * Create an instance of {@link CartValidateAndReserveQuantity }
     * 
     */
    public CartValidateAndReserveQuantity createCartValidateAndReserveQuantity() {
        return new CartValidateAndReserveQuantity();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartTicketListCriteria }
     * 
     */
    public ArrayOfSDCCartTicketListCriteria createArrayOfSDCCartTicketListCriteria() {
        return new ArrayOfSDCCartTicketListCriteria();
    }

    /**
     * Create an instance of {@link CartValidateQuantity }
     * 
     */
    public CartValidateQuantity createCartValidateQuantity() {
        return new CartValidateQuantity();
    }

    /**
     * Create an instance of {@link CartCheckoutCancel }
     * 
     */
    public CartCheckoutCancel createCartCheckoutCancel() {
        return new CartCheckoutCancel();
    }

    /**
     * Create an instance of {@link CartCheckoutCancelResponse }
     * 
     */
    public CartCheckoutCancelResponse createCartCheckoutCancelResponse() {
        return new CartCheckoutCancelResponse();
    }

    /**
     * Create an instance of {@link CartValidateAndReserveQuantityResponse }
     * 
     */
    public CartValidateAndReserveQuantityResponse createCartValidateAndReserveQuantityResponse() {
        return new CartValidateAndReserveQuantityResponse();
    }

    /**
     * Create an instance of {@link CartValidateQuantityResponse }
     * 
     */
    public CartValidateQuantityResponse createCartValidateQuantityResponse() {
        return new CartValidateQuantityResponse();
    }

    /**
     * Create an instance of {@link CartCheckoutStartOnlineResponse }
     * 
     */
    public CartCheckoutStartOnlineResponse createCartCheckoutStartOnlineResponse() {
        return new CartCheckoutStartOnlineResponse();
    }

    /**
     * Create an instance of {@link RetailTicketSearchResponse }
     * 
     */
    public RetailTicketSearchResponse createRetailTicketSearchResponse() {
        return new RetailTicketSearchResponse();
    }

    /**
     * Create an instance of {@link CartCheckoutStartOnline }
     * 
     */
    public CartCheckoutStartOnline createCartCheckoutStartOnline() {
        return new CartCheckoutStartOnline();
    }

    /**
     * Create an instance of {@link RetailTicketSearch }
     * 
     */
    public RetailTicketSearch createRetailTicketSearch() {
        return new RetailTicketSearch();
    }

    /**
     * Create an instance of {@link CartCheckoutComplete }
     * 
     */
    public CartCheckoutComplete createCartCheckoutComplete() {
        return new CartCheckoutComplete();
    }

    /**
     * Create an instance of {@link CartCheckoutCompleteResponse }
     * 
     */
    public CartCheckoutCompleteResponse createCartCheckoutCompleteResponse() {
        return new CartCheckoutCompleteResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSDCRetailTicketTable }
     * 
     */
    public ArrayOfSDCRetailTicketTable createArrayOfSDCRetailTicketTable() {
        return new ArrayOfSDCRetailTicketTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketTable }
     * 
     */
    public SDCCartRetailTicketTable createSDCCartRetailTicketTable() {
        return new SDCCartRetailTicketTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketItem }
     * 
     */
    public SDCCartRetailTicketItem createSDCCartRetailTicketItem() {
        return new SDCCartRetailTicketItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketItem createArrayOfSDCCartRetailTicketItem() {
        return new ArrayOfSDCCartRetailTicketItem();
    }

    /**
     * Create an instance of {@link SDCRetailTicketTable }
     * 
     */
    public SDCRetailTicketTable createSDCRetailTicketTable() {
        return new SDCRetailTicketTable();
    }

    /**
     * Create an instance of {@link SDCCartTicketListCriteria }
     * 
     */
    public SDCCartTicketListCriteria createSDCCartTicketListCriteria() {
        return new SDCCartTicketListCriteria();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketTable")
    public JAXBElement<SDCCartRetailTicketTable> createSDCCartRetailTicketTable(SDCCartRetailTicketTable value) {
        return new JAXBElement<SDCCartRetailTicketTable>(_SDCCartRetailTicketTable_QNAME, SDCCartRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CartTicketListCriteria")
    public JAXBElement<SDCCartTicketListCriteria> createSDCCartTicketListCriteria(SDCCartTicketListCriteria value) {
        return new JAXBElement<SDCCartTicketListCriteria>(_SDCCartTicketListCriteria_QNAME, SDCCartTicketListCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketItem")
    public JAXBElement<SDCCartRetailTicketItem> createSDCCartRetailTicketItem(SDCCartRetailTicketItem value) {
        return new JAXBElement<SDCCartRetailTicketItem>(_SDCCartRetailTicketItem_QNAME, SDCCartRetailTicketItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CartTicketListCriteria")
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createArrayOfSDCCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_ArrayOfSDCCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_RetailTicketTable")
    public JAXBElement<SDCRetailTicketTable> createSDCRetailTicketTable(SDCRetailTicketTable value) {
        return new JAXBElement<SDCRetailTicketTable>(_SDCRetailTicketTable_QNAME, SDCRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> createArrayOfSDCCartRetailTicketItem(ArrayOfSDCCartRetailTicketItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketItem>(_ArrayOfSDCCartRetailTicketItem_QNAME, ArrayOfSDCCartRetailTicketItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_RetailTicketTable")
    public JAXBElement<ArrayOfSDCRetailTicketTable> createArrayOfSDCRetailTicketTable(ArrayOfSDCRetailTicketTable value) {
        return new JAXBElement<ArrayOfSDCRetailTicketTable>(_ArrayOfSDCRetailTicketTable_QNAME, ArrayOfSDCRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_CartRetailTicketTableResponse")
    public JAXBElement<SDCCartRetailTicketTableResponse> createSDCCartRetailTicketTableResponse(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_SDCCartRetailTicketTableResponse_QNAME, SDCCartRetailTicketTableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_RetailTicketTableResponse")
    public JAXBElement<SDCRetailTicketTableResponse> createSDCRetailTicketTableResponse(SDCRetailTicketTableResponse value) {
        return new JAXBElement<SDCRetailTicketTableResponse>(_SDCRetailTicketTableResponse_QNAME, SDCRetailTicketTableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = CartCheckoutComplete.class)
    public JAXBElement<String> createCartCheckoutCompleteTransactionId(String value) {
        return new JAXBElement<String>(_CartCheckoutCompleteTransactionId_QNAME, String.class, CartCheckoutComplete.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = CartCheckoutCancel.class)
    public JAXBElement<String> createCartCheckoutCancelTransactionId(String value) {
        return new JAXBElement<String>(_CartCheckoutCompleteTransactionId_QNAME, String.class, CartCheckoutCancel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CartRetailTicketTableEntityCollection", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> createSDCCartRetailTicketTableCartRetailTicketTableEntityCollection(ArrayOfSDCCartRetailTicketItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketItem>(_SDCCartRetailTicketTableCartRetailTicketTableEntityCollection_QNAME, ArrayOfSDCCartRetailTicketItem.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ErrorMessage", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTableErrorMessage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableErrorMessage_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableTransactionId_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RetailTicketTableCollection", scope = SDCRetailTicketTableResponse.class)
    public JAXBElement<ArrayOfSDCRetailTicketTable> createSDCRetailTicketTableResponseRetailTicketTableCollection(ArrayOfSDCRetailTicketTable value) {
        return new JAXBElement<ArrayOfSDCRetailTicketTable>(_SDCRetailTicketTableResponseRetailTicketTableCollection_QNAME, ArrayOfSDCRetailTicketTable.class, SDCRetailTicketTableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartValidateAndReserveQuantity.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartValidateAndReserveQuantityCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartValidateAndReserveQuantityCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartValidateAndReserveQuantity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventGroupId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaItemId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaItemId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaTransactionId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaRetailVariantId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventLineId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaLineId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DataAreaId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaDataAreaId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountNum", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaAccountNum(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaAccountNum_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartValidateQuantityResult", scope = CartValidateQuantityResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartValidateQuantityResponseCartValidateQuantityResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartValidateQuantityResponseCartValidateQuantityResult_QNAME, SDCCartRetailTicketTableResponse.class, CartValidateQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "RetailTicketSearchResult", scope = RetailTicketSearchResponse.class)
    public JAXBElement<SDCRetailTicketTableResponse> createRetailTicketSearchResponseRetailTicketSearchResult(SDCRetailTicketTableResponse value) {
        return new JAXBElement<SDCRetailTicketTableResponse>(_RetailTicketSearchResponseRetailTicketSearchResult_QNAME, SDCRetailTicketTableResponse.class, RetailTicketSearchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartValidateAndReserveQuantityResult", scope = CartValidateAndReserveQuantityResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartValidateAndReserveQuantityResponseCartValidateAndReserveQuantityResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartValidateAndReserveQuantityResponseCartValidateAndReserveQuantityResult_QNAME, SDCCartRetailTicketTableResponse.class, CartValidateAndReserveQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = RetailTicketSearch.class)
    public JAXBElement<String> createRetailTicketSearchTransactionId(String value) {
        return new JAXBElement<String>(_CartCheckoutCompleteTransactionId_QNAME, String.class, RetailTicketSearch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableUsageValidityId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTicketTableId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableTicketTableId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableEventLineId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableEventLineId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableProductName(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableProductName_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableDataAreaId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableDataAreaId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableEventGroupId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableEventGroupId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OrigTicketCode", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableOrigTicketCode(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableOrigTicketCode_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableItemId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableItemId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PinCode", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTablePinCode(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTablePinCode_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCode", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTicketCode(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableTicketCode_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableTransactionId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableOpenValidityId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartCheckoutCompleteResult", scope = CartCheckoutCompleteResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartCheckoutCompleteResponseCartCheckoutCompleteResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartCheckoutCompleteResponseCartCheckoutCompleteResult_QNAME, SDCCartRetailTicketTableResponse.class, CartCheckoutCompleteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableUsageValidityId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableTicketTableId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableEventLineId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemProductName(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableProductName_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableDataAreaId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableEventGroupId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OrigTicketCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemOrigTicketCode(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableOrigTicketCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemItemId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableItemId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PinCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemPinCode(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTablePinCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketCode(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableTicketCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableTransactionId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCRetailTicketTableOpenValidityId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartCheckoutCancelResult", scope = CartCheckoutCancelResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartCheckoutCancelResponseCartCheckoutCancelResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartCheckoutCancelResponseCartCheckoutCancelResult_QNAME, SDCCartRetailTicketTableResponse.class, CartCheckoutCancelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "CartRetailTicketCollection", scope = SDCCartRetailTicketTableResponse.class)
    public JAXBElement<SDCCartRetailTicketTable> createSDCCartRetailTicketTableResponseCartRetailTicketCollection(SDCCartRetailTicketTable value) {
        return new JAXBElement<SDCCartRetailTicketTable>(_SDCCartRetailTicketTableResponseCartRetailTicketCollection_QNAME, SDCCartRetailTicketTable.class, SDCCartRetailTicketTableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartCheckoutStartOnlineResult", scope = CartCheckoutStartOnlineResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartCheckoutStartOnlineResponseCartCheckoutStartOnlineResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartCheckoutStartOnlineResponseCartCheckoutStartOnlineResult_QNAME, SDCCartRetailTicketTableResponse.class, CartCheckoutStartOnlineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartCheckoutStartOnline.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartCheckoutStartOnlineCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartValidateAndReserveQuantityCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartCheckoutStartOnline.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartValidateQuantity.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartValidateQuantityCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartValidateAndReserveQuantityCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartValidateQuantity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

}
