package com.enovax.star.cms.commons.model.booking.partner;

/**
 * Created by jennylynsze on 6/29/16.
 */
public class PartnerFunCartEmailDisplay extends PartnerFunCartDisplay {

    private Object partner;

    private String gstRateStr = "";

    private String totalNoGstText = "";

    private String gstText = "";

    public String getGstRateStr() {
        return gstRateStr;
    }

    public void setGstRateStr(String gstRateStr) {
        this.gstRateStr = gstRateStr;
    }

    public String getTotalNoGstText() {
        return totalNoGstText;
    }

    public void setTotalNoGstText(String totalNoGstText) {
        this.totalNoGstText = totalNoGstText;
    }

    public String getGstText() {
        return gstText;
    }

    public void setGstText(String gstText) {
        this.gstText = gstText;
    }

    public Object getPartner() {
        return partner;
    }

    public void setPartner(Object partner) {
        this.partner = partner;
    }
}
