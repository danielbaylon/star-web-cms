package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.List;

public class PartnerRegistrationVM {

    private List<CountryVM> countries;

    private List<PartnerTypeVM> partnerTypes;

    private List<DropDownVM> partnerDocTypes;

    public List<CountryVM> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryVM> countries) {
        this.countries = countries;
    }

    public List<PartnerTypeVM> getPartnerTypes() {
        return partnerTypes;
    }

    public void setPartnerTypes(List<PartnerTypeVM> partnerTypes) {
        this.partnerTypes = partnerTypes;
    }

    public List<DropDownVM> getPartnerDocTypes() {
        return partnerDocTypes;
    }

    public void setPartnerDocTypes(List<DropDownVM> partnerDocTypes) {
        this.partnerDocTypes = partnerDocTypes;
    }
}
