package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * Collection(SDC_TicketingExtension.Entity.TemplateXMLEntity
 * 
 * 
 * @author Justin
 * @since 9 SEP 16
 */
public class AXTemplateXMLEntityRequest {

	@JsonProperty("TemplateName")
	private String templateName = "";

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

}
