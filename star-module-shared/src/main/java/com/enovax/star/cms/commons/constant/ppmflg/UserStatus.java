package com.enovax.star.cms.commons.constant.ppmflg;

import java.util.ArrayList;
import java.util.List;

public enum UserStatus {
    Active,
    Inactive,
    Locked,
    Deleted;
    
    private static List<String> list = null;
    public static List<String> toList() {
        if (list == null) {
            list = new ArrayList<String>();
            for (UserStatus stat : values()) {
                list.add(stat.toString());
            }  
        }
        return list;
    }
}
