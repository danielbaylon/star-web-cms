
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_RetailTicketTableResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_RetailTicketTableResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="RetailTicketTableCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_RetailTicketTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_RetailTicketTableResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "retailTicketTableCollection"
})
public class SDCRetailTicketTableResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "RetailTicketTableCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCRetailTicketTable> retailTicketTableCollection;

    /**
     * Gets the value of the retailTicketTableCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCRetailTicketTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCRetailTicketTable> getRetailTicketTableCollection() {
        return retailTicketTableCollection;
    }

    /**
     * Sets the value of the retailTicketTableCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCRetailTicketTable }{@code >}
     *     
     */
    public void setRetailTicketTableCollection(JAXBElement<ArrayOfSDCRetailTicketTable> value) {
        this.retailTicketTableCollection = value;
    }

}
