package com.enovax.star.cms.commons.model.axchannel.customerext;

/**
 * Created by houtao on 26/9/16.
 */
public class AxMarketDistribution {

    private String countryRegion;
    private String percent;
    private String salesManager;

    public String getCountryRegion() {
        return countryRegion;
    }

    public void setCountryRegion(String countryRegion) {
        this.countryRegion = countryRegion;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }
}
