package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author Justin
 *
 */
public class KioskGetReceiptsRequest {
    
    @JsonIgnore
    private String transactionId;

    private String isCopy = "false";

    private String receiptTypeId = "18";

    private String isRemoteOrder = "false";

    private String shiftId;

    private String isPreview = "false";

    private String queryBySalesId = "false";

    private String salesLineNumbers;

    public String getIsCopy() {
        return isCopy;
    }

    public void setIsCopy(String isCopy) {
        this.isCopy = isCopy;
    }

    public String getReceiptTypeId() {
        return receiptTypeId;
    }

    public void setReceiptTypeId(String receiptTypeId) {
        this.receiptTypeId = receiptTypeId;
    }

    public String getIsRemoteOrder() {
        return isRemoteOrder;
    }

    public void setIsRemoteOrder(String isRemoteOrder) {
        this.isRemoteOrder = isRemoteOrder;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getIsPreview() {
        return isPreview;
    }

    public void setIsPreview(String isPreview) {
        this.isPreview = isPreview;
    }

    public String getQueryBySalesId() {
        return queryBySalesId;
    }

    public void setQueryBySalesId(String queryBySalesId) {
        this.queryBySalesId = queryBySalesId;
    }

    public String getSalesLineNumbers() {
        return salesLineNumbers;
    }

    public void setSalesLineNumbers(String salesLineNumbers) {
        this.salesLineNumbers = salesLineNumbers;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    

}
