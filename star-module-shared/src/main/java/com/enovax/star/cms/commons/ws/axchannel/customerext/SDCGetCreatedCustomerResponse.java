
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_GetCreatedCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_GetCreatedCustomerResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="customerEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_CustomerTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_GetCreatedCustomerResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "customerEntity"
})
public class SDCGetCreatedCustomerResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "customerEntity", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCCustomerTable> customerEntity;

    /**
     * Gets the value of the customerEntity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCCustomerTable> getCustomerEntity() {
        return customerEntity;
    }

    /**
     * Sets the value of the customerEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCCustomerTable }{@code >}
     *     
     */
    public void setCustomerEntity(JAXBElement<ArrayOfSDCCustomerTable> value) {
        this.customerEntity = value;
    }

}
