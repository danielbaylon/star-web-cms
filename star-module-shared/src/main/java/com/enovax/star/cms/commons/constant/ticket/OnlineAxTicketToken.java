package com.enovax.star.cms.commons.constant.ticket;

/**
 * Created by jennylynsze on 12/19/16.
 */
public enum OnlineAxTicketToken {

    //TODO what is Price Type ah??

    PaxPerTicket("PaxPerTicket", "{{Pax per Ticket}}"),

    TicketType("TicketType", "{{Ticket Type}}"),

    PublishPrice("PublishPrice", "{{Publish Price}}"),

    UsageValidity("UsageValidity", "{{Usage Validity}}"),

    TicketValidity("TicketValidity", "{{Ticket Validity}}"),

    ValidityStartDate("ValidityStartDate", "{{Valid From}}"),

    ValidityEndDate("ValidityEndDate", "{{Valid To}}"),

    LegalEntity("LegalEntity", "{{Legal Entity}}"),

    TicketNumber("TicketNumber", "{{Ticket Number}}"),

    TicketCode("TicketCode", "{{Ticket Code}}"),

    CustomerName("CustomerName", "{{Customer Name}}"),

    ProductImage("ProductImage", "{{Product Image - CMS}}"),

    ItemNumber("ItemNumber", "{{Item Number}}"),

    DisplayName("DisplayName", "{{Product Name}}"),

    ProductOwner("ProductOwner", "{{ProductOwner}}"), //TODO: why AX returns this???

    TicketLineDesc("TicketLineDesc", "{{Ticket Line Description}}"), //TODO check if AX returnes this

    PrintLabel("PrintLabel", "{{Print Label}}"),

    ProductSearchName("ProductSearchName", "{{Product Search Name}}"),

    Description("Description", "{{Product Ticket Description}}"),

    ProductVarConfig("ProductVarConfig", "{{Product Variant (configuration)}}"),

    ProductVarSize("ProductVarSize", "{{Product Variant (Size)}}"),

    ProductVarColor("ProductVarColor", "{{Product Variant (Color)}}"),

    ProductVarStyle("ProductVarStyle", "{{Product Variant (Style)}}"),

    PackageItem("PackageItem", "{{Package ItemId}}"),

    PackageItemName("PackageItemName", "{{Package Item Name}}"),

    PackagePrintLabel("PackagePrintLabel", "{{Package Print Label}}"), //TODO check if AX returnes this

    MixMatchPackageName("MixMatchPackageName", "{{Mix &amp; Match Package Name}}"),

    MixMatchDescription("MixMatchDescription", "{{Mix &amp; Match Description}}"),

    EventDate("EventDate", "{{Event date}}"),

    EventName("EventName", "{{Event Name}}"),

    Facilities("Facilities", "{{Facility}}"),

    FacilityAction("FacilityAction", "{{Facility Action}}"),

    OperationIds("OperationIds", "{{Facility Operation}}"),

    DiscountApplied("DiscountApplied", "{{Discount Applied}}"),

    BarcodePLU("BarcodePLU", "{{BarcodePLU}}"), //TODO the hell this one is not in the xls

    BarcodeStartDate("BarcodeStartDate", "{{BarcodeStartDate}}"), //TODO the hell this one is not in the xls

    BarcodeEndDate("BarcodeEndDate", "{{BarcodeEndDate}}"), //TODO the hell this one is not in the xls

    TermAndCondition("TAC", "{{Terms and Condition}}"),

    Disclaimer("Disclaimer", "{{Disclaimer}}");

    private final String token;

    private final String replacement;

    private OnlineAxTicketToken(String token, String replacement) {
        this.token = token;
        this.replacement = replacement;
    }

    public String getToken() {
        return this.token;
    }

    public String getReplacement() {
        return this.replacement;
    }

    public static OnlineAxTicketToken toEnum(String token) {
        OnlineAxTicketToken ticketToken = null;
        if (PrintLabel.getToken().equals(token)) {
            ticketToken = PrintLabel;
        } else if (PublishPrice.getToken().equals(token)) {
            ticketToken = PublishPrice;
        } else if (TicketType.getToken().equals(token)) {
            ticketToken = TicketType;
        } else if (ProductOwner.getToken().equals(token)) {
            ticketToken = ProductOwner;
        } else if (PaxPerTicket.getToken().equals(token)) {
            ticketToken = PaxPerTicket;
        } else if (UsageValidity.getToken().equals(token)) {
            ticketToken = UsageValidity;
        } else if (TicketValidity.getToken().equals(token)) {
            ticketToken = TicketValidity;
        } else if (ValidityStartDate.getToken().equals(token)) {
            ticketToken = ValidityStartDate;
        } else if (ValidityEndDate.getToken().equals(token)) {
            ticketToken = ValidityEndDate;
        } else if (LegalEntity.getToken().equals(token)) {
            ticketToken = LegalEntity;
        } else if (TicketNumber.getToken().equals(token)) {
            ticketToken = TicketNumber;
        } else if (ProductImage.getToken().equals(token)) {
            ticketToken = ProductImage;
        } else if (ItemNumber.getToken().equals(token)) {
            ticketToken = ItemNumber;
        } else if (CustomerName.getToken().equals(token)) {
            ticketToken = CustomerName;
        } else if (DisplayName.getToken().equals(token)) {
            ticketToken = DisplayName;
        } else if (Description.getToken().equals(token)) {
            ticketToken = Description;
        } else if (ProductSearchName.getToken().equals(token)) {
            ticketToken = ProductSearchName;
        } else if (ProductVarConfig.getToken().equals(token)) {
            ticketToken = ProductVarConfig;
        } else if (ProductVarSize.getToken().equals(token)) {
            ticketToken = ProductVarSize;
        } else if (ProductVarColor.getToken().equals(token)) {
            ticketToken = ProductVarColor;
        } else if (ProductVarStyle.getToken().equals(token)) {
            ticketToken = ProductVarStyle;
        } else if (PackageItem.getToken().equals(token)) {
            ticketToken = PackageItem;
        } else if (PackageItemName.getToken().equals(token)) {
            ticketToken = PackageItemName;
        } else if (MixMatchPackageName.getToken().equals(token)) {
            ticketToken = MixMatchPackageName;
        } else if (MixMatchDescription.getToken().equals(token)) {
            ticketToken = MixMatchDescription;
        } else if (Facilities.getToken().equals(token)) {
            ticketToken = Facilities;
        } else if (FacilityAction.getToken().equals(token)) {
            ticketToken = FacilityAction;
        } else if (OperationIds.getToken().equals(token)) {
            ticketToken = OperationIds;
        } else if (DiscountApplied.getToken().equals(token)) {
            ticketToken = DiscountApplied;
        } else if (BarcodePLU.getToken().equals(token)) {
            ticketToken = BarcodePLU;
        } else if (BarcodeStartDate.getToken().equals(token)) {
            ticketToken = BarcodeStartDate;
        } else if (BarcodeEndDate.getToken().equals(token)) {
            ticketToken = BarcodeEndDate;
        } else if (TermAndCondition.getToken().equals(token)) {
            ticketToken = TermAndCondition;
        } else if (Disclaimer.getToken().equals(token)) {
            ticketToken = Disclaimer;
        } else if(TicketLineDesc.getToken().equals(token)) {
            ticketToken = TicketLineDesc;
        } else if (PackagePrintLabel.getToken().equals(token)) {
            ticketToken = PackagePrintLabel;
        } else if (TicketCode.getToken().equals(token)) {
            ticketToken = TicketCode;
        } else if (EventDate.getToken().equals(token)) {
            ticketToken = EventDate;
        } else if(EventName.getToken().equals(token)) {
            ticketToken = EventName;
        }
        return ticketToken;
    }
}
