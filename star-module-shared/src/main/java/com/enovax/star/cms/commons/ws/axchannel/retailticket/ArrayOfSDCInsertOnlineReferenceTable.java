
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_InsertOnlineReferenceTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_InsertOnlineReferenceTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_InsertOnlineReferenceTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_InsertOnlineReferenceTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_InsertOnlineReferenceTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcInsertOnlineReferenceTable"
})
public class ArrayOfSDCInsertOnlineReferenceTable {

    @XmlElement(name = "SDC_InsertOnlineReferenceTable", nillable = true)
    protected List<SDCInsertOnlineReferenceTable> sdcInsertOnlineReferenceTable;

    /**
     * Gets the value of the sdcInsertOnlineReferenceTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcInsertOnlineReferenceTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCInsertOnlineReferenceTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCInsertOnlineReferenceTable }
     * 
     * 
     */
    public List<SDCInsertOnlineReferenceTable> getSDCInsertOnlineReferenceTable() {
        if (sdcInsertOnlineReferenceTable == null) {
            sdcInsertOnlineReferenceTable = new ArrayList<SDCInsertOnlineReferenceTable>();
        }
        return this.sdcInsertOnlineReferenceTable;
    }

}
