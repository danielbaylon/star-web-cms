package com.enovax.star.cms.commons.datamodel.ppmflg;


import javax.persistence.*;

/**
 * Created by jennylynsze on 1/3/17.
 */
@Entity
@Table(name = "PPMFLGMixMatchPackagePinLine")
public class PPMFLGMixMatchPackagePinLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "packageItemId", nullable = false)
    private Integer packageItemId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "packageItemId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPMFLGMixMatchPackageItem mmPkgItem;

    @Column(name = "lineNum")
    private String lineNum;

    @Column(name = "receiptNum")
    private String receiptNum;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getPackageItemId() {
        return packageItemId;
    }

    public void setPackageItemId(Integer packageItemId) {
        this.packageItemId = packageItemId;
    }

    public PPMFLGMixMatchPackageItem getMmPkgItem() {
        return mmPkgItem;
    }

    public void setMmPkgItem(PPMFLGMixMatchPackageItem mmPkgItem) {
        this.mmPkgItem = mmPkgItem;
    }

    public String getLineNum() {
        return lineNum;
    }

    public void setLineNum(String lineNum) {
        this.lineNum = lineNum;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
}
