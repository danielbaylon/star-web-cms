package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 5/16/16.
 */
public class InventoryTransactionVM implements Serializable {

    public static final String PURCHASE = "Purchase";
    public static final String REVALIDATE = "Revalidate";

    private Integer id;
    private String transType;
    private String receiptNumber;
    private Integer mainAccountId;
    private String username;
    private Boolean subAccountTrans;
    private BigDecimal totalAmount;
    private String currency;
    private String tmMerchantId;
    private String status;
    private String tmStatus;
    private Date tmStatusDate;
    private String tmStatusDateStr;
    private String tmApprovalCode;
    private String tmErrorCode;
    private String tmErrorMessage;
    private String paymentType;
    private Date createdDate;
    private String createdDateStr;
    private Date modifiedDate;
    private String ip;
    private String maskedCc;
    private Integer reprintCount;
    private Date validityEndDate;
    private boolean revalidated;
    private boolean ticketGenerated;
    private BigDecimal gstRate;
    private String gstRateStr;
    private Integer transQty = 0;
    private Date validityStartDate;
    private Integer revalidatePeriod = 3;
    private Integer allowRevalPeriod = 2;
    private Integer expiringAlertPeriod = 27;
    private String displayStatus;
    private String gstStr;
    private String exclGstStr;
    private Boolean canRevalidateByAcc = null;
    private boolean allowRevalidation;

    private String validityStartDateStr;
    private String validityEndDateStr;
    private String expiringStatus;

    private List<InventoryTransactionItemVM> items;
    private List<FunCartDisplayCmsProduct> productList;

    private PartnerVM partner;
    private String offlinePaymentApprovalStatus;
    private String offlinePaymentApprovalDateText;

    private RevalTransactionVM revalTransaction;

    public static InventoryTransactionVM fromDataModel(PPSLMInventoryTransaction trans) {

        InventoryTransactionVM transVm = new InventoryTransactionVM();
        transVm.setOfflinePaymentApprovalStatus("");
        transVm.setOfflinePaymentApprovalDateText("");
        transVm.transType = PURCHASE;
        transVm.setId(trans.getId());
        transVm.setGstRate(trans.getGstRate());
        transVm.setReceiptNumber(trans.getReceiptNum());
        transVm.setStatus(TransactionUtil.getAdminStatus(trans.getStatus()));
        transVm.setTmStatus(trans.getTmStatus());
        transVm.setTicketGenerated(trans.isTicketGenerated());
        transVm.setDisplayStatus(trans.getDisplayStatus());
        transVm.setTmErrorCode(trans.getTmErrorCode());
        transVm.setTmErrorMessage(trans.getTmErrorMessage());
        if (trans.getTmStatusDate() != null) {
            transVm.setTmStatusDate(trans.getTmStatusDate());
            transVm.setTmStatusDateStr(NvxDateUtils.formatDate(trans.getTmStatusDate(),
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        transVm.setCreatedDate(trans.getCreatedDate());
        transVm.setCreatedDateStr(NvxDateUtils.formatDate(trans.getCreatedDate(),
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        if (trans.getValidityStartDate() != null) {
            transVm.setValidityStartDate(trans.getValidityStartDate());
            transVm.setValidityStartDateStr(NvxDateUtils.formatDate(trans.getValidityStartDate(),
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        if (trans.getValidityEndDate() != null) {
            transVm.setValidityEndDate(trans.getValidityStartDate());
            transVm.setValidityEndDateStr(NvxDateUtils.formatDate(trans.getValidityEndDate(),
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        transVm.setPaymentType(trans.getPaymentType());
        transVm.setUsername(trans.getUsername());
        //TODO TODO offline payment
      //  transVm.setExpiringStatus(transVm.getExpiringStatus());
        return transVm;
    }

    public static InventoryTransactionVM fromDataModel(PPSLMInventoryTransaction trans, PPSLMRevalidationTransaction revalTrans) {
        InventoryTransactionVM transVm = new InventoryTransactionVM();
        transVm.setOfflinePaymentApprovalStatus("");
        transVm.setOfflinePaymentApprovalDateText("");
        transVm.setTransType(REVALIDATE);
        transVm.setId(revalTrans.getId());
        transVm.setGstRate(revalTrans.getGstRate());
        transVm.setReceiptNumber(revalTrans.getReceiptNum());
        transVm.setStatus(TransactionUtil.getAdminStatus(revalTrans.getStatus()));
        transVm.setTmErrorCode(revalTrans.getTmErrorCode());
        transVm.setTmErrorMessage(revalTrans.getTmErrorMessage());
        if (trans.getTmStatusDate() != null) {
            transVm.setTmStatusDate(revalTrans.getTmStatusDate());
            transVm.setTmStatusDateStr(NvxDateUtils.formatDate(revalTrans.getTmStatusDate(),
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        transVm.setCreatedDate(revalTrans.getCreatedDate());
        transVm.setCreatedDateStr(NvxDateUtils.formatDate(revalTrans.getCreatedDate(),
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        if (EnovaxTmSystemStatus.Success.toString().equals(trans.getTmStatus())) {
            transVm.setValidityStartDate(trans.getValidityStartDate());
            transVm.setValidityStartDateStr(NvxDateUtils.formatDate(
                    trans.getValidityStartDate(),
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            transVm.setValidityEndDate(trans.getValidityEndDate());
            transVm.setValidityEndDateStr(NvxDateUtils.formatDate(
                    trans.getValidityEndDate(),
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        transVm.setPaymentType(revalTrans.getPaymentType());
        transVm.setUsername(revalTrans.getUsername());
        return transVm;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean isSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(Boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public Date getTmStatusDate() {
        return tmStatusDate;
    }

    public void setTmStatusDate(Date tmStatusDate) {
        this.tmStatusDate = tmStatusDate;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public String getTmErrorCode() {
        return tmErrorCode;
    }

    public void setTmErrorCode(String tmErrorCode) {
        this.tmErrorCode = tmErrorCode;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMaskedCc() {
        return maskedCc;
    }

    public void setMaskedCc(String maskedCc) {
        this.maskedCc = maskedCc;
    }

    public Integer getReprintCount() {
        return reprintCount;
    }

    public void setReprintCount(Integer reprintCount) {
        this.reprintCount = reprintCount;
    }

    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public boolean isRevalidated() {
        return revalidated;
    }

    public void setRevalidated(boolean revalidated) {
        this.revalidated = revalidated;
    }

    public boolean isTicketGenerated() {
        return ticketGenerated;
    }

    public void setTicketGenerated(boolean ticketGenerated) {
        this.ticketGenerated = ticketGenerated;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public String getGstRateStr() {
        return gstRateStr;
    }

    public void setGstRateStr(String gstRateStr) {
        this.gstRateStr = gstRateStr;
    }

    public Integer getTransQty() {
        return transQty;
    }

    public void setTransQty(Integer transQty) {
        this.transQty = transQty;
    }

    public Date getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(Date validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    public Integer getRevalidatePeriod() {
        return revalidatePeriod;
    }

    public void setRevalidatePeriod(Integer revalidatePeriod) {
        this.revalidatePeriod = revalidatePeriod;
    }

    public Integer getAllowRevalPeriod() {
        return allowRevalPeriod;
    }

    public void setAllowRevalPeriod(Integer allowRevalPeriod) {
        this.allowRevalPeriod = allowRevalPeriod;
    }

    public Integer getExpiringAlertPeriod() {
        return expiringAlertPeriod;
    }

    public void setExpiringAlertPeriod(Integer expiringAlertPeriod) {
        this.expiringAlertPeriod = expiringAlertPeriod;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public String getGstStr() {
        return gstStr;
    }

    public void setGstStr(String gstStr) {
        this.gstStr = gstStr;
    }

    public String getExclGstStr() {
        return exclGstStr;
    }

    public void setExclGstStr(String exclGstStr) {
        this.exclGstStr = exclGstStr;
    }

    public Boolean isCanRevalidateByAcc() {
        return canRevalidateByAcc;
    }

    public void setCanRevalidateByAcc(Boolean canRevalidateByAcc) {
        this.canRevalidateByAcc = canRevalidateByAcc;
    }

    public List<InventoryTransactionItemVM> getItems() {
        return items;
    }

    public void setItems(List<InventoryTransactionItemVM> items) {
        this.items = items;
    }

    public PartnerVM getPartner() {
        return partner;
    }

    public void setPartner(PartnerVM partner) {
        this.partner = partner;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public String getValidityStartDateStr() {
        return validityStartDateStr;
    }

    public void setValidityStartDateStr(String validityStartDateStr) {
        this.validityStartDateStr = validityStartDateStr;
    }

    public String getValidityEndDateStr() {
        return validityEndDateStr;
    }

    public void setValidityEndDateStr(String validityEndDateStr) {
        this.validityEndDateStr = validityEndDateStr;
    }

//    public String getExpiringStatus(){
//        long weeks = TransactionUtil.getExpiringInWeeks(this);
//        if (TicketStatus.Available.toString().equals(this.getStatus())
//                && weeks <= TransactionUtil.EXPIRING_ALERT_PERIOD && weeks > 0) {
//            displayStatus = TransactionUtil.getExpiringStatus(this);
//        } else  if(weeks <= 0) {
//            displayStatus = TicketStatus.Expired.toString();
//        } else {
//            displayStatus = this.status;
//        }
//        return displayStatus;
//    }

    public void setExpiringStatus(String expiringStatus) {
        this.expiringStatus = expiringStatus;
    }

    public List<FunCartDisplayCmsProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<FunCartDisplayCmsProduct> productList) {
        this.productList = productList;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Boolean getSubAccountTrans() {
        return subAccountTrans;
    }

    public String getTmStatusDateStr() {
        return tmStatusDateStr;
    }

    public void setTmStatusDateStr(String tmStatusDateStr) {
        this.tmStatusDateStr = tmStatusDateStr;
    }

    public Boolean getCanRevalidateByAcc() {
        return canRevalidateByAcc;
    }

    public String getExpiringStatus() {
        return expiringStatus;
    }

    public String getOfflinePaymentApprovalStatus() {
        return offlinePaymentApprovalStatus;
    }

    public void setOfflinePaymentApprovalStatus(String offlinePaymentApprovalStatus) {
        this.offlinePaymentApprovalStatus = offlinePaymentApprovalStatus;
    }

    public String getOfflinePaymentApprovalDateText() {
        return offlinePaymentApprovalDateText;
    }

    public void setOfflinePaymentApprovalDateText(String offlinePaymentApprovalDateText) {
        this.offlinePaymentApprovalDateText = offlinePaymentApprovalDateText;
    }

    public boolean isAllowRevalidation() {
        return allowRevalidation;
    }

    public void setAllowRevalidation(boolean allowRevalidation) {
        this.allowRevalidation = allowRevalidation;
    }

    public RevalTransactionVM getRevalTransaction() {
        return revalTransaction;
    }

    public void setRevalTransaction(RevalTransactionVM revalTransaction) {
        this.revalTransaction = revalTransaction;
    }
}
