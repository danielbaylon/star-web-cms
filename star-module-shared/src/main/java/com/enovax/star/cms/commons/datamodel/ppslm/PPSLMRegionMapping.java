package com.enovax.star.cms.commons.datamodel.ppslm;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = PPSLMRegionMapping.TABLE_NAME)
public class PPSLMRegionMapping {

    public static final String TABLE_NAME = "PPSLMRegionMapping";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @JoinColumn(name = "regionId",nullable = false)
    private Integer regionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "regionId", referencedColumnName = "id",updatable =false, insertable= false)
    private PPSLMRegion region;

    @JoinColumn(name = "ctyId",nullable = false)
    private Integer ctyId;

    @Column(name="status")
    private String status;

    @Column(name="createdBy", nullable=false, length=100)
    private String createdBy;

    @Column(name="createdDate", nullable=false, length=23)
    private Date createdDate;

    @Column(name="modifiedBy", nullable=false, length=100)
    private String modifiedBy;

    @Column(name="modifiedDate", nullable=false, length=23)
    private Date modifiedDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public PPSLMRegion getRegion() {
        return region;
    }

    public void setRegion(PPSLMRegion region) {
        this.region = region;
    }

    public Integer getCtyId() {
        return ctyId;
    }

    public void setCtyId(Integer ctyId) {
        this.ctyId = ctyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
