
package com.enovax.star.cms.commons.ws.axchannel.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SDC_InventTableExt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_InventTableExt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AbsoluteCapacityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAreaId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefaultEventLineId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DefineOpenDate" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EventGroupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventGroupTableCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfEventGroupTableEntity" minOccurs="0"/>
 *         &lt;element name="FacilityId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InventTableVariantExtCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfInventTableVariantExt" minOccurs="0"/>
 *         &lt;element name="IsCapacity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsPackage" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsTicketing" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsTransport" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ItemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MediaTypeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MediaTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NOOFPAX" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Nec_IsMember" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Nec_MembershipTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NonReturnable" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OpenEndDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OpenStartDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="OpenValidityRuleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnAttraction" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PackageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackageLinesCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfPackageLine" minOccurs="0"/>
 *         &lt;element name="PrinterType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Printing" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Product" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ProductName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Remarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TemplateDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TemplateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UpCrossSellTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}ArrayOfSDC_UpCrossSellTable" minOccurs="0"/>
 *         &lt;element name="UsageValidityRuleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_InventTableExt", propOrder = {
    "absoluteCapacityId",
    "accessId",
    "dataAreaId",
    "defaultEventLineId",
    "defineOpenDate",
    "eventGroupId",
    "eventGroupTableCollection",
    "facilityId",
    "inventTableVariantExtCollection",
    "isCapacity",
    "isPackage",
    "isTicketing",
    "isTransport",
    "itemId",
    "mediaTypeDescription",
    "mediaTypeId",
    "noofpax",
    "necIsMember",
    "necMembershipTypeCode",
    "nonReturnable",
    "openEndDateTime",
    "openStartDateTime",
    "openValidityRuleId",
    "operationId",
    "ownAttraction",
    "packageId",
    "packageLinesCollection",
    "printerType",
    "printing",
    "product",
    "productName",
    "recId",
    "remarks",
    "templateDescription",
    "templateName",
    "upCrossSellTable",
    "usageValidityRuleId"
})
public class SDCInventTableExt {

    @XmlElementRef(name = "AbsoluteCapacityId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> absoluteCapacityId;
    @XmlElementRef(name = "AccessId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accessId;
    @XmlElementRef(name = "DataAreaId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataAreaId;
    @XmlElementRef(name = "DefaultEventLineId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultEventLineId;
    @XmlElement(name = "DefineOpenDate")
    protected Integer defineOpenDate;
    @XmlElementRef(name = "EventGroupId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventGroupId;
    @XmlElementRef(name = "EventGroupTableCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEventGroupTableEntity> eventGroupTableCollection;
    @XmlElementRef(name = "FacilityId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> facilityId;
    @XmlElementRef(name = "InventTableVariantExtCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfInventTableVariantExt> inventTableVariantExtCollection;
    @XmlElement(name = "IsCapacity")
    protected Integer isCapacity;
    @XmlElement(name = "IsPackage")
    protected Integer isPackage;
    @XmlElement(name = "IsTicketing")
    protected Integer isTicketing;
    @XmlElement(name = "IsTransport")
    protected Integer isTransport;
    @XmlElementRef(name = "ItemId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> itemId;
    @XmlElementRef(name = "MediaTypeDescription", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mediaTypeDescription;
    @XmlElementRef(name = "MediaTypeId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mediaTypeId;
    @XmlElement(name = "NOOFPAX")
    protected Integer noofpax;
    @XmlElement(name = "Nec_IsMember")
    protected Integer necIsMember;
    @XmlElementRef(name = "Nec_MembershipTypeCode", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> necMembershipTypeCode;
    @XmlElement(name = "NonReturnable")
    protected Integer nonReturnable;
    @XmlElement(name = "OpenEndDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar openEndDateTime;
    @XmlElement(name = "OpenStartDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar openStartDateTime;
    @XmlElementRef(name = "OpenValidityRuleId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> openValidityRuleId;
    @XmlElementRef(name = "OperationId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> operationId;
    @XmlElement(name = "OwnAttraction")
    protected Integer ownAttraction;
    @XmlElementRef(name = "PackageId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> packageId;
    @XmlElementRef(name = "PackageLinesCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPackageLine> packageLinesCollection;
    @XmlElement(name = "PrinterType")
    protected Integer printerType;
    @XmlElement(name = "Printing")
    protected Integer printing;
    @XmlElement(name = "Product")
    protected Long product;
    @XmlElementRef(name = "ProductName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productName;
    @XmlElement(name = "RecId")
    protected Long recId;
    @XmlElementRef(name = "Remarks", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> remarks;
    @XmlElementRef(name = "TemplateDescription", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> templateDescription;
    @XmlElementRef(name = "TemplateName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> templateName;
    @XmlElementRef(name = "UpCrossSellTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCUpCrossSellTable> upCrossSellTable;
    @XmlElementRef(name = "UsageValidityRuleId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> usageValidityRuleId;

    /**
     * Gets the value of the absoluteCapacityId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAbsoluteCapacityId() {
        return absoluteCapacityId;
    }

    /**
     * Sets the value of the absoluteCapacityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAbsoluteCapacityId(JAXBElement<String> value) {
        this.absoluteCapacityId = value;
    }

    /**
     * Gets the value of the accessId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccessId() {
        return accessId;
    }

    /**
     * Sets the value of the accessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccessId(JAXBElement<String> value) {
        this.accessId = value;
    }

    /**
     * Gets the value of the dataAreaId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataAreaId() {
        return dataAreaId;
    }

    /**
     * Sets the value of the dataAreaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataAreaId(JAXBElement<String> value) {
        this.dataAreaId = value;
    }

    /**
     * Gets the value of the defaultEventLineId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultEventLineId() {
        return defaultEventLineId;
    }

    /**
     * Sets the value of the defaultEventLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultEventLineId(JAXBElement<String> value) {
        this.defaultEventLineId = value;
    }

    /**
     * Gets the value of the defineOpenDate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDefineOpenDate() {
        return defineOpenDate;
    }

    /**
     * Sets the value of the defineOpenDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDefineOpenDate(Integer value) {
        this.defineOpenDate = value;
    }

    /**
     * Gets the value of the eventGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventGroupId() {
        return eventGroupId;
    }

    /**
     * Sets the value of the eventGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventGroupId(JAXBElement<String> value) {
        this.eventGroupId = value;
    }

    /**
     * Gets the value of the eventGroupTableCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEventGroupTableEntity }{@code >}
     *     
     */
    public JAXBElement<ArrayOfEventGroupTableEntity> getEventGroupTableCollection() {
        return eventGroupTableCollection;
    }

    /**
     * Sets the value of the eventGroupTableCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEventGroupTableEntity }{@code >}
     *     
     */
    public void setEventGroupTableCollection(JAXBElement<ArrayOfEventGroupTableEntity> value) {
        this.eventGroupTableCollection = value;
    }

    /**
     * Gets the value of the facilityId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFacilityId() {
        return facilityId;
    }

    /**
     * Sets the value of the facilityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFacilityId(JAXBElement<String> value) {
        this.facilityId = value;
    }

    /**
     * Gets the value of the inventTableVariantExtCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfInventTableVariantExt }{@code >}
     *     
     */
    public JAXBElement<ArrayOfInventTableVariantExt> getInventTableVariantExtCollection() {
        return inventTableVariantExtCollection;
    }

    /**
     * Sets the value of the inventTableVariantExtCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfInventTableVariantExt }{@code >}
     *     
     */
    public void setInventTableVariantExtCollection(JAXBElement<ArrayOfInventTableVariantExt> value) {
        this.inventTableVariantExtCollection = value;
    }

    /**
     * Gets the value of the isCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsCapacity() {
        return isCapacity;
    }

    /**
     * Sets the value of the isCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsCapacity(Integer value) {
        this.isCapacity = value;
    }

    /**
     * Gets the value of the isPackage property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsPackage() {
        return isPackage;
    }

    /**
     * Sets the value of the isPackage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsPackage(Integer value) {
        this.isPackage = value;
    }

    /**
     * Gets the value of the isTicketing property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsTicketing() {
        return isTicketing;
    }

    /**
     * Sets the value of the isTicketing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsTicketing(Integer value) {
        this.isTicketing = value;
    }

    /**
     * Gets the value of the isTransport property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIsTransport() {
        return isTransport;
    }

    /**
     * Sets the value of the isTransport property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIsTransport(Integer value) {
        this.isTransport = value;
    }

    /**
     * Gets the value of the itemId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getItemId() {
        return itemId;
    }

    /**
     * Sets the value of the itemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setItemId(JAXBElement<String> value) {
        this.itemId = value;
    }

    /**
     * Gets the value of the mediaTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMediaTypeDescription() {
        return mediaTypeDescription;
    }

    /**
     * Sets the value of the mediaTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMediaTypeDescription(JAXBElement<String> value) {
        this.mediaTypeDescription = value;
    }

    /**
     * Gets the value of the mediaTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMediaTypeId() {
        return mediaTypeId;
    }

    /**
     * Sets the value of the mediaTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMediaTypeId(JAXBElement<String> value) {
        this.mediaTypeId = value;
    }

    /**
     * Gets the value of the noofpax property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNOOFPAX() {
        return noofpax;
    }

    /**
     * Sets the value of the noofpax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNOOFPAX(Integer value) {
        this.noofpax = value;
    }

    /**
     * Gets the value of the necIsMember property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNecIsMember() {
        return necIsMember;
    }

    /**
     * Sets the value of the necIsMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNecIsMember(Integer value) {
        this.necIsMember = value;
    }

    /**
     * Gets the value of the necMembershipTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNecMembershipTypeCode() {
        return necMembershipTypeCode;
    }

    /**
     * Sets the value of the necMembershipTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNecMembershipTypeCode(JAXBElement<String> value) {
        this.necMembershipTypeCode = value;
    }

    /**
     * Gets the value of the nonReturnable property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNonReturnable() {
        return nonReturnable;
    }

    /**
     * Sets the value of the nonReturnable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNonReturnable(Integer value) {
        this.nonReturnable = value;
    }

    /**
     * Gets the value of the openEndDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpenEndDateTime() {
        return openEndDateTime;
    }

    /**
     * Sets the value of the openEndDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpenEndDateTime(XMLGregorianCalendar value) {
        this.openEndDateTime = value;
    }

    /**
     * Gets the value of the openStartDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpenStartDateTime() {
        return openStartDateTime;
    }

    /**
     * Sets the value of the openStartDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpenStartDateTime(XMLGregorianCalendar value) {
        this.openStartDateTime = value;
    }

    /**
     * Gets the value of the openValidityRuleId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpenValidityRuleId() {
        return openValidityRuleId;
    }

    /**
     * Sets the value of the openValidityRuleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpenValidityRuleId(JAXBElement<String> value) {
        this.openValidityRuleId = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperationId(JAXBElement<String> value) {
        this.operationId = value;
    }

    /**
     * Gets the value of the ownAttraction property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOwnAttraction() {
        return ownAttraction;
    }

    /**
     * Sets the value of the ownAttraction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOwnAttraction(Integer value) {
        this.ownAttraction = value;
    }

    /**
     * Gets the value of the packageId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPackageId() {
        return packageId;
    }

    /**
     * Sets the value of the packageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPackageId(JAXBElement<String> value) {
        this.packageId = value;
    }

    /**
     * Gets the value of the packageLinesCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPackageLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPackageLine> getPackageLinesCollection() {
        return packageLinesCollection;
    }

    /**
     * Sets the value of the packageLinesCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPackageLine }{@code >}
     *     
     */
    public void setPackageLinesCollection(JAXBElement<ArrayOfPackageLine> value) {
        this.packageLinesCollection = value;
    }

    /**
     * Gets the value of the printerType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrinterType() {
        return printerType;
    }

    /**
     * Sets the value of the printerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrinterType(Integer value) {
        this.printerType = value;
    }

    /**
     * Gets the value of the printing property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPrinting() {
        return printing;
    }

    /**
     * Sets the value of the printing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPrinting(Integer value) {
        this.printing = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setProduct(Long value) {
        this.product = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductName(JAXBElement<String> value) {
        this.productName = value;
    }

    /**
     * Gets the value of the recId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecId() {
        return recId;
    }

    /**
     * Sets the value of the recId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecId(Long value) {
        this.recId = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRemarks() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRemarks(JAXBElement<String> value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the templateDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTemplateDescription() {
        return templateDescription;
    }

    /**
     * Sets the value of the templateDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTemplateDescription(JAXBElement<String> value) {
        this.templateDescription = value;
    }

    /**
     * Gets the value of the templateName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTemplateName() {
        return templateName;
    }

    /**
     * Sets the value of the templateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTemplateName(JAXBElement<String> value) {
        this.templateName = value;
    }

    /**
     * Gets the value of the upCrossSellTable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCUpCrossSellTable> getUpCrossSellTable() {
        return upCrossSellTable;
    }

    /**
     * Sets the value of the upCrossSellTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}
     *     
     */
    public void setUpCrossSellTable(JAXBElement<ArrayOfSDCUpCrossSellTable> value) {
        this.upCrossSellTable = value;
    }

    /**
     * Gets the value of the usageValidityRuleId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUsageValidityRuleId() {
        return usageValidityRuleId;
    }

    /**
     * Sets the value of the usageValidityRuleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUsageValidityRuleId(JAXBElement<String> value) {
        this.usageValidityRuleId = value;
    }

}
