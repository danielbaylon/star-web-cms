package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="PPSLMSequenceLog")
public class PPSLMSequenceLog implements Serializable {

    @Id
    @Column(name = "seqId")
    private String seqId;

    @Column(name = "modifiedDate", nullable = false)
    private Date modifiedDate;

    public String getSeqId() {
        return seqId;
    }
    public void setSeqId(String seqId) {
        this.seqId = seqId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    
}
