package com.enovax.star.cms.commons.util.barcode;

import com.enovax.star.cms.commons.util.Base64;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BarcodeGenerator {

    private static final String charset = "UTF-8";
    private static final Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<>();

    public static byte[] toByteArray(String barCodeData, int width, int height) throws WriterException, IOException {
        /* ErrorCorrectionLevel
		 * H = ~30% correction
		 * L = ~7% correction
		 * M = ~15% correction
		 * Q = ~25% correction
		*/
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        BitMatrix matrix = createCode128(barCodeData, charset, hintMap, width, height);
        ByteArrayOutputStream stream = null;
        try {
            stream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(matrix, "PNG", stream);
            byte[] barBytes = stream.toByteArray();
            return barBytes;
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

    public static boolean toBase64(String barCodeData, int width, int height, String outputFilePath) throws IOException, WriterException {
        /* ErrorCorrectionLevel
		 * H = ~30% correction
		 * L = ~7% correction
		 * M = ~15% correction
		 * Q = ~25% correction
		*/
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        BitMatrix matrix = createCode128(barCodeData, charset, hintMap, width, height);
        File file = null;
        FileOutputStream stream = null;
        try {
            file = new File(outputFilePath);
            stream = new FileOutputStream(file);
            MatrixToImageWriter.writeToStream(matrix, "PNG", stream);
            stream.flush();
            return true;
        } finally {
            if (stream != null) {
                try{
                    stream.close();
                }catch (Exception ex){
                    ex.printStackTrace();
                }catch (Throwable ex){
                    ex.printStackTrace();
                }
            }
        }
    }

    public static String toBase64(String barCodeData, int width, int height) throws WriterException, IOException {
        byte[] barCodeBytes = toByteArray(barCodeData, width, height);
        String barCodeBase64 = Base64.encodeToString(barCodeBytes, false);
        return barCodeBase64;
    }

    public static BitMatrix createCode128(String barCodeData, String charset,
                                          Map<EncodeHintType, ErrorCorrectionLevel> hintMap, int barCodeWidth, int barCodeHeight)
            throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(new String(barCodeData.getBytes(charset), charset),
                BarcodeFormat.CODE_128, barCodeWidth, barCodeHeight, hintMap);
        return matrix;
    }

}
