package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.TenderLine
 * 
 * @author Justin
 *
 */
public class AxTenderLine {

    private String cardToken;
    private String authorization;
    private String transactionStatusValue;
    private String incomeExpenseAccountTypeValue;
    private String maskedCardNumber;
    private String tenderDate;
    private String isPreProcessed;
    private String tenderLineId;
    private String amount;
    private String cashBackAmount;
    private String amountInTenderedCurrency;
    private String amountInCompanyCurrency;
    private String currency;
    private String exchangeRate;
    private String companyCurrencyExchangeRate;
    private String tenderTypeId;
    private String signatureData;

    private String lineNumber;
    private String giftCardId;
    private String creditMemoId;
    private String customerId;
    private String loyaltyCardId;
    private String cardTypeId;
    private String isChangeLine;
    private String isHistorical;
    private String isVoidable;
    private String statusValue;

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getTransactionStatusValue() {
        return transactionStatusValue;
    }

    public void setTransactionStatusValue(String transactionStatusValue) {
        this.transactionStatusValue = transactionStatusValue;
    }

    public String getIncomeExpenseAccountTypeValue() {
        return incomeExpenseAccountTypeValue;
    }

    public void setIncomeExpenseAccountTypeValue(String incomeExpenseAccountTypeValue) {
        this.incomeExpenseAccountTypeValue = incomeExpenseAccountTypeValue;
    }

    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    public String getTenderDate() {
        return tenderDate;
    }

    public void setTenderDate(String tenderDate) {
        this.tenderDate = tenderDate;
    }

    public String getIsPreProcessed() {
        return isPreProcessed;
    }

    public void setIsPreProcessed(String isPreProcessed) {
        this.isPreProcessed = isPreProcessed;
    }

    public String getTenderLineId() {
        return tenderLineId;
    }

    public void setTenderLineId(String tenderLineId) {
        this.tenderLineId = tenderLineId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCashBackAmount() {
        return cashBackAmount;
    }

    public void setCashBackAmount(String cashBackAmount) {
        this.cashBackAmount = cashBackAmount;
    }

    public String getAmountInTenderedCurrency() {
        return amountInTenderedCurrency;
    }

    public void setAmountInTenderedCurrency(String amountInTenderedCurrency) {
        this.amountInTenderedCurrency = amountInTenderedCurrency;
    }

    public String getAmountInCompanyCurrency() {
        return amountInCompanyCurrency;
    }

    public void setAmountInCompanyCurrency(String amountInCompanyCurrency) {
        this.amountInCompanyCurrency = amountInCompanyCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCompanyCurrencyExchangeRate() {
        return companyCurrencyExchangeRate;
    }

    public void setCompanyCurrencyExchangeRate(String companyCurrencyExchangeRate) {
        this.companyCurrencyExchangeRate = companyCurrencyExchangeRate;
    }

    public String getTenderTypeId() {
        return tenderTypeId;
    }

    public void setTenderTypeId(String tenderTypeId) {
        this.tenderTypeId = tenderTypeId;
    }

    public String getSignatureData() {
        return signatureData;
    }

    public void setSignatureData(String signatureData) {
        this.signatureData = signatureData;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getGiftCardId() {
        return giftCardId;
    }

    public void setGiftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
    }

    public String getCreditMemoId() {
        return creditMemoId;
    }

    public void setCreditMemoId(String creditMemoId) {
        this.creditMemoId = creditMemoId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getLoyaltyCardId() {
        return loyaltyCardId;
    }

    public void setLoyaltyCardId(String loyaltyCardId) {
        this.loyaltyCardId = loyaltyCardId;
    }

    public String getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(String cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    public String getIsChangeLine() {
        return isChangeLine;
    }

    public void setIsChangeLine(String isChangeLine) {
        this.isChangeLine = isChangeLine;
    }

    public String getIsHistorical() {
        return isHistorical;
    }

    public void setIsHistorical(String isHistorical) {
        this.isHistorical = isHistorical;
    }

    public String getIsVoidable() {
        return isVoidable;
    }

    public void setIsVoidable(String isVoidable) {
        this.isVoidable = isVoidable;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

}
