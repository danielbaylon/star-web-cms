package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGTAMainAccountRepository extends JpaRepository<PPMFLGTAMainAccount, Integer> {

    public List<PPMFLGTAMainAccount> findByUsername(String username);

    public PPMFLGTAMainAccount findFirstByUsername(String username);

    PPMFLGTAMainAccount findById(Integer id);

    int countByIdAndStatus(Integer id, String name);
}
