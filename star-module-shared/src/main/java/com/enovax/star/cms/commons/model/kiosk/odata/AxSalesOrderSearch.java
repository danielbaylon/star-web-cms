package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.SalesOrder
 * 
 * @author Justin
 *
 */
public class AxSalesOrderSearch {

    @JsonProperty("Id")
    private String transactionId;

    private String currencyCode;
    private String documentStatusValue;
    private String recordId;
    private String statusValue;
    private String orderPlacedDate;
    private String customerAccountDepositAmount;
    // AffiliationLoyaltyTierLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.SalesAffiliationLoyaltyTier)"
    // Nullable="false"/>
    private String isRequiredAmountPaid;
    private String isDiscountFullyCalculated;
    private String amountDue;
    private String estimatedShippingAmount;
    private String amountPaid;
    // AttributeValues"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.AttributeValueBase)"
    // Nullable="false"/>
    private String availableDepositAmount;
    private String beginDateTime;
    private String createdDateTime;
    private String businessDate;
    private String calculatedDepositAmount;
    private String cancellationCharge;
    private String channelId;
    private String channelReferenceId;
    private String chargeAmount;
    // ChargeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ChargeLine)"
    // Nullable="false"/>
    private String comment;
    private String invoiceComment;
    // ContactInformationCollection"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ContactInformation)"
    // Nullable="false"/>
    private String customerId;
    private String customerOrderModeValue;
    private String customerOrderTypeValue;
    private String deliveryMode;
    private String deliveryModeChargeAmount;
    private String discountAmount;
    //private List<> discountCodes;
    private String entryStatusValue;
    private String grossAmount;
    private String hasLoyaltyPayment;

    // IncomeExpenseLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.IncomeExpenseLine)"
    // Nullable="false"/>
    private String incomeExpenseTotalAmount;
    private String inventoryLocationId;
    private String isCreatedOffline;
    private String isReturnByReceipt;
    private String isSuspended;
    private String isTaxIncludedInPrice;
    private String lineDiscount;
    private String lineDiscountCalculationTypeValue;
    private String loyaltyCardId;
    private String loyaltyDiscountAmount;
    private String loyaltyManualDiscountAmount;
    // LoyaltyRewardPointLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.LoyaltyRewardPointLine)"
    // Nullable="false"/>
    private String modifiedDateTime;
    private String name;
    private String storeName;
    private String storeAddress;
    private String netAmount;
    private String netAmountWithNoTax;
    private String netAmountWithTax;
    private String numberOfItems;
    private String overriddenDepositAmount;
    private String periodicDiscountAmount;
    private String prepaymentAmountAppliedOnPickup;
    private String prepaymentAmountInvoiced;
    private String prepaymentAmountPaid;
    private String quotationExpiryDate;
    // ReasonCodeLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.ReasonCodeLine)"
    // Nullable="false"/>
    private String receiptEmail;
    private String receiptId;
    private String requestedDeliveryDate;
    private String requiredDepositAmount;
    private String returnTransactionHasLoyaltyPayment;
    
    @JsonIgnore
    @JsonProperty("SalesId")
    private String salesId;
    private String salesPaymentDifference;

    @JsonProperty("SalesLines")
    List<AxSalesLine> salesLines;
    private String shiftId;
    private String shiftTerminalId;
    // ShippingAddress"
    // Type="Microsoft.Dynamics.Commerce.Runtime.DataModel.Address"/>
    private String staffId;
    private String statementCode;
    private String storeId;
    private String subtotalAmount;
    private String subtotalAmountWithoutTax;
    private String taxAmount;
    private String taxAmountExclusive;
    private String taxAmountInclusive;
    // TaxLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.TaxLine)"
    // Nullable="false"/>
    private String taxOnCancellationCharge;
    private String taxOverrideCode;

    // <Property Name="TenderLines"
    // Type="Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.TenderLine)"
    // Nullable="false"/>
    private String terminalId;
    private String totalAmount;
    private String totalDiscount;
    private String totalManualDiscountAmount;
    private String totalManualDiscountPercentage;
    private String transactionTypeValue;
    private String applyDiscountsToPriceOverrides;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDocumentStatusValue() {
        return documentStatusValue;
    }

    public void setDocumentStatusValue(String documentStatusValue) {
        this.documentStatusValue = documentStatusValue;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getOrderPlacedDate() {
        return orderPlacedDate;
    }

    public void setOrderPlacedDate(String orderPlacedDate) {
        this.orderPlacedDate = orderPlacedDate;
    }

    public String getCustomerAccountDepositAmount() {
        return customerAccountDepositAmount;
    }

    public void setCustomerAccountDepositAmount(String customerAccountDepositAmount) {
        this.customerAccountDepositAmount = customerAccountDepositAmount;
    }

    public String getIsRequiredAmountPaid() {
        return isRequiredAmountPaid;
    }

    public void setIsRequiredAmountPaid(String isRequiredAmountPaid) {
        this.isRequiredAmountPaid = isRequiredAmountPaid;
    }

    public String getIsDiscountFullyCalculated() {
        return isDiscountFullyCalculated;
    }

    public void setIsDiscountFullyCalculated(String isDiscountFullyCalculated) {
        this.isDiscountFullyCalculated = isDiscountFullyCalculated;
    }

    public String getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(String amountDue) {
        this.amountDue = amountDue;
    }

    public String getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(String estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getAvailableDepositAmount() {
        return availableDepositAmount;
    }

    public void setAvailableDepositAmount(String availableDepositAmount) {
        this.availableDepositAmount = availableDepositAmount;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public String getCalculatedDepositAmount() {
        return calculatedDepositAmount;
    }

    public void setCalculatedDepositAmount(String calculatedDepositAmount) {
        this.calculatedDepositAmount = calculatedDepositAmount;
    }

    public String getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(String cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelReferenceId() {
        return channelReferenceId;
    }

    public void setChannelReferenceId(String channelReferenceId) {
        this.channelReferenceId = channelReferenceId;
    }

    public String getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInvoiceComment() {
        return invoiceComment;
    }

    public void setInvoiceComment(String invoiceComment) {
        this.invoiceComment = invoiceComment;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerOrderModeValue() {
        return customerOrderModeValue;
    }

    public void setCustomerOrderModeValue(String customerOrderModeValue) {
        this.customerOrderModeValue = customerOrderModeValue;
    }

    public String getCustomerOrderTypeValue() {
        return customerOrderTypeValue;
    }

    public void setCustomerOrderTypeValue(String customerOrderTypeValue) {
        this.customerOrderTypeValue = customerOrderTypeValue;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    public void setDeliveryModeChargeAmount(String deliveryModeChargeAmount) {
        this.deliveryModeChargeAmount = deliveryModeChargeAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getEntryStatusValue() {
        return entryStatusValue;
    }

    public void setEntryStatusValue(String entryStatusValue) {
        this.entryStatusValue = entryStatusValue;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getHasLoyaltyPayment() {
        return hasLoyaltyPayment;
    }

    public void setHasLoyaltyPayment(String hasLoyaltyPayment) {
        this.hasLoyaltyPayment = hasLoyaltyPayment;
    }

    public String getIncomeExpenseTotalAmount() {
        return incomeExpenseTotalAmount;
    }

    public void setIncomeExpenseTotalAmount(String incomeExpenseTotalAmount) {
        this.incomeExpenseTotalAmount = incomeExpenseTotalAmount;
    }

    public String getInventoryLocationId() {
        return inventoryLocationId;
    }

    public void setInventoryLocationId(String inventoryLocationId) {
        this.inventoryLocationId = inventoryLocationId;
    }

    public String getIsCreatedOffline() {
        return isCreatedOffline;
    }

    public void setIsCreatedOffline(String isCreatedOffline) {
        this.isCreatedOffline = isCreatedOffline;
    }

    public String getIsReturnByReceipt() {
        return isReturnByReceipt;
    }

    public void setIsReturnByReceipt(String isReturnByReceipt) {
        this.isReturnByReceipt = isReturnByReceipt;
    }

    public String getIsSuspended() {
        return isSuspended;
    }

    public void setIsSuspended(String isSuspended) {
        this.isSuspended = isSuspended;
    }

    public String getIsTaxIncludedInPrice() {
        return isTaxIncludedInPrice;
    }

    public void setIsTaxIncludedInPrice(String isTaxIncludedInPrice) {
        this.isTaxIncludedInPrice = isTaxIncludedInPrice;
    }

    public String getLineDiscount() {
        return lineDiscount;
    }

    public void setLineDiscount(String lineDiscount) {
        this.lineDiscount = lineDiscount;
    }

    public String getLineDiscountCalculationTypeValue() {
        return lineDiscountCalculationTypeValue;
    }

    public void setLineDiscountCalculationTypeValue(String lineDiscountCalculationTypeValue) {
        this.lineDiscountCalculationTypeValue = lineDiscountCalculationTypeValue;
    }

    public String getLoyaltyCardId() {
        return loyaltyCardId;
    }

    public void setLoyaltyCardId(String loyaltyCardId) {
        this.loyaltyCardId = loyaltyCardId;
    }

    public String getLoyaltyDiscountAmount() {
        return loyaltyDiscountAmount;
    }

    public void setLoyaltyDiscountAmount(String loyaltyDiscountAmount) {
        this.loyaltyDiscountAmount = loyaltyDiscountAmount;
    }

    public String getLoyaltyManualDiscountAmount() {
        return loyaltyManualDiscountAmount;
    }

    public void setLoyaltyManualDiscountAmount(String loyaltyManualDiscountAmount) {
        this.loyaltyManualDiscountAmount = loyaltyManualDiscountAmount;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getNetAmountWithNoTax() {
        return netAmountWithNoTax;
    }

    public void setNetAmountWithNoTax(String netAmountWithNoTax) {
        this.netAmountWithNoTax = netAmountWithNoTax;
    }

    public String getNetAmountWithTax() {
        return netAmountWithTax;
    }

    public void setNetAmountWithTax(String netAmountWithTax) {
        this.netAmountWithTax = netAmountWithTax;
    }

    public String getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(String numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public String getOverriddenDepositAmount() {
        return overriddenDepositAmount;
    }

    public void setOverriddenDepositAmount(String overriddenDepositAmount) {
        this.overriddenDepositAmount = overriddenDepositAmount;
    }

    public String getPeriodicDiscountAmount() {
        return periodicDiscountAmount;
    }

    public void setPeriodicDiscountAmount(String periodicDiscountAmount) {
        this.periodicDiscountAmount = periodicDiscountAmount;
    }

    public String getPrepaymentAmountAppliedOnPickup() {
        return prepaymentAmountAppliedOnPickup;
    }

    public void setPrepaymentAmountAppliedOnPickup(String prepaymentAmountAppliedOnPickup) {
        this.prepaymentAmountAppliedOnPickup = prepaymentAmountAppliedOnPickup;
    }

    public String getPrepaymentAmountInvoiced() {
        return prepaymentAmountInvoiced;
    }

    public void setPrepaymentAmountInvoiced(String prepaymentAmountInvoiced) {
        this.prepaymentAmountInvoiced = prepaymentAmountInvoiced;
    }

    public String getPrepaymentAmountPaid() {
        return prepaymentAmountPaid;
    }

    public void setPrepaymentAmountPaid(String prepaymentAmountPaid) {
        this.prepaymentAmountPaid = prepaymentAmountPaid;
    }

    public String getQuotationExpiryDate() {
        return quotationExpiryDate;
    }

    public void setQuotationExpiryDate(String quotationExpiryDate) {
        this.quotationExpiryDate = quotationExpiryDate;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public String getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(String receiptId) {
        this.receiptId = receiptId;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getRequiredDepositAmount() {
        return requiredDepositAmount;
    }

    public void setRequiredDepositAmount(String requiredDepositAmount) {
        this.requiredDepositAmount = requiredDepositAmount;
    }

    public String getReturnTransactionHasLoyaltyPayment() {
        return returnTransactionHasLoyaltyPayment;
    }

    public void setReturnTransactionHasLoyaltyPayment(String returnTransactionHasLoyaltyPayment) {
        this.returnTransactionHasLoyaltyPayment = returnTransactionHasLoyaltyPayment;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public String getSalesPaymentDifference() {
        return salesPaymentDifference;
    }

    public void setSalesPaymentDifference(String salesPaymentDifference) {
        this.salesPaymentDifference = salesPaymentDifference;
    }

    
    public List<AxSalesLine> getSalesLines() {
		return salesLines;
	}
    
    public void setSalesLines(List<AxSalesLine> salesLines) {
		this.salesLines = salesLines;
	}
    
    

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getShiftTerminalId() {
        return shiftTerminalId;
    }

    public void setShiftTerminalId(String shiftTerminalId) {
        this.shiftTerminalId = shiftTerminalId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStatementCode() {
        return statementCode;
    }

    public void setStatementCode(String statementCode) {
        this.statementCode = statementCode;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getSubtotalAmount() {
        return subtotalAmount;
    }

    public void setSubtotalAmount(String subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public String getSubtotalAmountWithoutTax() {
        return subtotalAmountWithoutTax;
    }

    public void setSubtotalAmountWithoutTax(String subtotalAmountWithoutTax) {
        this.subtotalAmountWithoutTax = subtotalAmountWithoutTax;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getTaxAmountExclusive() {
        return taxAmountExclusive;
    }

    public void setTaxAmountExclusive(String taxAmountExclusive) {
        this.taxAmountExclusive = taxAmountExclusive;
    }

    public String getTaxAmountInclusive() {
        return taxAmountInclusive;
    }

    public void setTaxAmountInclusive(String taxAmountInclusive) {
        this.taxAmountInclusive = taxAmountInclusive;
    }

    public String getTaxOnCancellationCharge() {
        return taxOnCancellationCharge;
    }

    public void setTaxOnCancellationCharge(String taxOnCancellationCharge) {
        this.taxOnCancellationCharge = taxOnCancellationCharge;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getTotalManualDiscountAmount() {
        return totalManualDiscountAmount;
    }

    public void setTotalManualDiscountAmount(String totalManualDiscountAmount) {
        this.totalManualDiscountAmount = totalManualDiscountAmount;
    }

    public String getTotalManualDiscountPercentage() {
        return totalManualDiscountPercentage;
    }

    public void setTotalManualDiscountPercentage(String totalManualDiscountPercentage) {
        this.totalManualDiscountPercentage = totalManualDiscountPercentage;
    }

    public String getTransactionTypeValue() {
        return transactionTypeValue;
    }

    public void setTransactionTypeValue(String transactionTypeValue) {
        this.transactionTypeValue = transactionTypeValue;
    }

    public String getApplyDiscountsToPriceOverrides() {
        return applyDiscountsToPriceOverrides;
    }

    public void setApplyDiscountsToPriceOverrides(String applyDiscountsToPriceOverrides) {
        this.applyDiscountsToPriceOverrides = applyDiscountsToPriceOverrides;
    }

}
