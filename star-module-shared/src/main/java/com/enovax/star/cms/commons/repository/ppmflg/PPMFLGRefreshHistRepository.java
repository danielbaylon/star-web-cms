package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRefreshHist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 20/8/16.
 */
@Repository
public interface PPMFLGRefreshHistRepository extends JpaRepository<PPMFLGRefreshHist, Integer> {

    @Query("from PPMFLGRefreshHist where key1 = ?1 and key1 is not null and key2 is null and key3 is null")
    PPMFLGRefreshHist findByKey1(String key1);

    @Query("from PPMFLGRefreshHist where key1 = ?1 and key2 = ?2 and key1 is not null and key2 is not null and key3 is null")
    PPMFLGRefreshHist findByKey1AndKey2(String key1, String key2);

    @Query("from PPMFLGRefreshHist where key1 = ?1 and key2 = ?2 and key3 = ?3 and key1 is not null and key2 is not null and key3 is not null")
    PPMFLGRefreshHist findByKey1AndKey2AndKey3(String key1, String key2, String key3);
}
