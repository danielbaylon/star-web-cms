package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGOfflinePaymentRequest;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 30/9/16.
 */
public class PPMFLGOfflinePaymentRequestSpecification extends BaseSpecification<PPMFLGOfflinePaymentRequest> {
}
