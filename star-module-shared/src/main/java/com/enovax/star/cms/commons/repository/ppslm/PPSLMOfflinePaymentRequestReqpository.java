package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by houtao on 29/9/16.
 */
@Repository
public interface PPSLMOfflinePaymentRequestReqpository extends JpaRepository<PPSLMOfflinePaymentRequest, Integer>, JpaSpecificationExecutor<PPSLMOfflinePaymentRequest> {

    @Query("select u from PPSLMOfflinePaymentRequest u where u.id = ?1 and u.mainAccountId = ?2 ")
    List<PPSLMOfflinePaymentRequest> findRequestByIdAndMainAccountId(Integer reqId, Integer mainAccountId);

    PPSLMOfflinePaymentRequest findById(Integer id);

    PPSLMOfflinePaymentRequest findFirstByTransactionId(Integer txnId);
}
