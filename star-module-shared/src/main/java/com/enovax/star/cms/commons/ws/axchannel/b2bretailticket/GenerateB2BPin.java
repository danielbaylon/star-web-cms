
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="B2BPINCriteria" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_B2BPINCriteria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "b2BPINCriteria"
})
@XmlRootElement(name = "GenerateB2BPin", namespace = "http://tempuri.org/")
public class GenerateB2BPin {

    @XmlElementRef(name = "B2BPINCriteria", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINCriteria> b2BPINCriteria;

    /**
     * Gets the value of the b2BPINCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINCriteria }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINCriteria> getB2BPINCriteria() {
        return b2BPINCriteria;
    }

    /**
     * Sets the value of the b2BPINCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINCriteria }{@code >}
     *     
     */
    public void setB2BPINCriteria(JAXBElement<SDCB2BPINCriteria> value) {
        this.b2BPINCriteria = value;
    }

}
