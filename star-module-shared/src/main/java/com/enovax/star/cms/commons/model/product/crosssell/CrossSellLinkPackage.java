package com.enovax.star.cms.commons.model.product.crosssell;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;

import java.util.Map;

public class CrossSellLinkPackage {

    private boolean success;
    private String message;
    private StoreApiChannels channel;
    private Map<String, CrossSellLinkMainProduct> mainProductMap; //Key is the itemId (productCode)
    private String axRetailUpCrossSellProductResultJson;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, CrossSellLinkMainProduct> getMainProductMap() {
        return mainProductMap;
    }

    public void setMainProductMap(Map<String, CrossSellLinkMainProduct> mainProductMap) {
        this.mainProductMap = mainProductMap;
    }

    public StoreApiChannels getChannel() {
        return channel;
    }

    public void setChannel(StoreApiChannels channel) {
        this.channel = channel;
    }

    public String getAxRetailUpCrossSellProductResultJson() {
        return axRetailUpCrossSellProductResultJson;
    }

    public void setAxRetailUpCrossSellProductResultJson(String axRetailUpCrossSellProductResultJson) {
        this.axRetailUpCrossSellProductResultJson = axRetailUpCrossSellProductResultJson;
    }
}
