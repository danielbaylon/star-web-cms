package com.enovax.star.cms.commons.model.booking;

public class CustomerDetails {

    private String email;
    private String name;
    private String mobile;
    private String dob;
    private String referSource;
    private String nationality;
    private String referSourceLabel;
    private String nationalityLabel;
    private String paymentType;
    private String paymentTypeLabel;
    private boolean subscribed;
    private String trafficSource;
    private String ip;

    private boolean isErr = false;
    private String errMsg;

    private String idType;
    private String idNo;
    private String salutation;
    private String companyName;

    private String selectedMerchant;
    private String merchantId;

    private String idText;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getReferSource() {
        return referSource;
    }

    public void setReferSource(String referSource) {
        this.referSource = referSource;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReferSourceLabel() {
        return referSourceLabel;
    }

    public void setReferSourceLabel(String referSourceLabel) {
        this.referSourceLabel = referSourceLabel;
    }

    public String getNationalityLabel() {
        return nationalityLabel;
    }

    public void setNationalityLabel(String nationalityLabel) {
        this.nationalityLabel = nationalityLabel;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeLabel() {
        return paymentTypeLabel;
    }

    public void setPaymentTypeLabel(String paymentTypeLabel) {
        this.paymentTypeLabel = paymentTypeLabel;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getTrafficSource() {
        return trafficSource;
    }

    public void setTrafficSource(String trafficSource) {
        this.trafficSource = trafficSource;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isErr() {
        return isErr;
    }

    public void setErr(boolean err) {
        isErr = err;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public void setIsErr(boolean isErr) {
        this.isErr = isErr;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNumber) {
        this.idNo = idNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSelectedMerchant() {
        return selectedMerchant;
    }

    public void setSelectedMerchant(String selectedMerchant) {
        this.selectedMerchant = selectedMerchant;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getIdText() {
        return idText;
    }

    public void setIdText(String idText) {
        this.idText = idText;
    }
}
