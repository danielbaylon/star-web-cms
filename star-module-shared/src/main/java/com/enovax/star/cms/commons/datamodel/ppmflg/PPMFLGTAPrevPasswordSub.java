package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PPMFLGTAPrevPasswordSub")
public class PPMFLGTAPrevPasswordSub extends PPMFLGPrevPassword implements Serializable{
    
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subAccountId",nullable = false)
    private PPMFLGTASubAccount user;
    
    
    
    public PPMFLGTASubAccount getUser() {
        return user;
    }
    public void setUser(PPMFLGTASubAccount user) {
        this.user = user;
    }
    
    
        
    
}
