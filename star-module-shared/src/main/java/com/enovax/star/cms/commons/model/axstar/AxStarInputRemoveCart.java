package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

public class AxStarInputRemoveCart {

    private String cartId;
    private String customerId;

    private List<String> lineIds;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<String> getLineIds() {
        return lineIds;
    }

    public void setLineIds(List<String> lineIds) {
        this.lineIds = lineIds;
    }
}
