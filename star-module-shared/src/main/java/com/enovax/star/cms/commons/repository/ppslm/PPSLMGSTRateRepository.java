package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMGSTRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by lavanya on 26/10/16.
 */
@Repository
public interface PPSLMGSTRateRepository extends JpaRepository<PPSLMGSTRate, Integer> {
    @Query("from PPSLMGSTRate where startDate <= ?1 " +
            "and (endDate is null or endDate >= ?1)" +
            "and status = 'A' order by startDate desc")
    PPSLMGSTRate getGSTRateByDate(Date idate);

    @Query("from PPSLMGSTRate where status in (?1) order by startDate desc")
    List<PPSLMGSTRate> listGSTRate(List<String> statuses);

    @Query("from PPSLMGSTRate where (endDate is null or endDate > ?2) " +
            "and (status = 'A')" +
            "and (?1 is null or id!=?1)")
    List<PPSLMGSTRate> getOverlappedGstWOEndDate(Integer id, Date startDate);

    @Query("from PPSLMGSTRate where ((endDate is null and startDate < ?3) or " +
            "(endDate > ?2 and startDate < ?3))" +
            "and (?1 is null or id!=?1)" +
            "and (status = 'A')")
    List<PPSLMGSTRate> getOverlappedGst(Integer id, Date startDate, Date endDate);
 }
