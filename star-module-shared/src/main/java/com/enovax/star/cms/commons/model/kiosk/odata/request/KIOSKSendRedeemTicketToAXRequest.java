package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSendRedeemTiketToAXCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KIOSKSendRedeemTicketToAXRequest {

	/**
	 * Collection(SDC_TicketingExtension.Entity.CartRetailTicketEntity)
	 * 
	 */
	@JsonProperty("bindingParameter")
	private List<AxCartRetailTicketEntity> axCartRetailTicketEntities;

	@JsonProperty("SendRedeemTicketCriteria")
	private AxSendRedeemTiketToAXCriteria axSendRedeemTiketToAXCriteria;

	public List<AxCartRetailTicketEntity> getAxCartRetailTicketEntities() {
		return axCartRetailTicketEntities;
	}

	public void setAxCartRetailTicketEntities(List<AxCartRetailTicketEntity> axCartRetailTicketEntities) {
		this.axCartRetailTicketEntities = axCartRetailTicketEntities;
	}

	public AxSendRedeemTiketToAXCriteria getAxSendRedeemTiketToAXCriteria() {
		return axSendRedeemTiketToAXCriteria;
	}

	public void setAxSendRedeemTiketToAXCriteria(AxSendRedeemTiketToAXCriteria axSendRedeemTiketToAXCriteria) {
		this.axSendRedeemTiketToAXCriteria = axSendRedeemTiketToAXCriteria;
	}

}
