package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGOfflinePaymentRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by houtao on 29/9/16.
 */
@Repository
public interface PPMFLGOfflinePaymentRequestReqpository extends JpaRepository<PPMFLGOfflinePaymentRequest, Integer>, JpaSpecificationExecutor<PPMFLGOfflinePaymentRequest> {

    @Query("select u from PPMFLGOfflinePaymentRequest u where u.id = ?1 and u.mainAccountId = ?2 ")
    List<PPMFLGOfflinePaymentRequest> findRequestByIdAndMainAccountId(Integer reqId, Integer mainAccountId);

    PPMFLGOfflinePaymentRequest findById(Integer id);

    PPMFLGOfflinePaymentRequest findFirstByTransactionId(Integer txnId);
}
