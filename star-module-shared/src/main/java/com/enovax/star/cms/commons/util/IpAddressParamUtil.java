package com.enovax.star.cms.commons.util;

import org.apache.commons.lang3.StringUtils;

public class IpAddressParamUtil {
	
	public static final String COMMA = ",";
	public static final String SEMICOLON = ":";

	public static String getValidCustIp(String ip) {
		if(ip != null && ip.trim().length() > 0){
			ip = ip.trim();
			String[] ips = null;
			if(ip.contains(COMMA)){
				ips = ip.split(COMMA);
			}else{
				ips = new String[]{ip};
			}
			if(ips != null && ips.length > 0){
				int ipCount = ips.length;
				for(int i = 0 ; i < ipCount ; i++){
					String _ip = ips[i];
					if(_ip != null && _ip.trim().length() > 0){
						_ip = _ip.trim();
						if(_ip.contains(SEMICOLON)){
							_ip = getValidCustIpPart(_ip);
						}
					}else{
						_ip = null;
					}
					ips[i] = _ip;
				}
			}
			if(ips.length > 0){
				for(int i = 0  ; i < ips.length ; i++){
					if(StringUtils.isNotEmpty(ips[i])){
						return ips[i];
					}
				}
			}
		}
		return null;
	}

	private static String getValidCustIpPart(String ip) {
		if(ip != null && ip.trim().length() > 0){
			String[] parts = ip.split(SEMICOLON);
			if(parts != null && parts.length > 0){
				String actualIp = parts[0];
				if(actualIp != null && actualIp.trim().length() > 0){
					return  actualIp.trim();
				}
			}
		}
		return null;
	}
	
}
