
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DiscountLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiscountLine">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntity">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ConcurrencyModeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CustomerDiscountTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DealPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountApplicationGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscountLineTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EffectiveAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="IsCompoundable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsDiscountCodeRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ManualDiscountTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OfferId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfferName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Percentage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PeriodicDiscountTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PricingPriorityNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SaleLineNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ThresholdAmountRequired" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscountLine", propOrder = {
    "amount",
    "concurrencyModeValue",
    "customerDiscountTypeValue",
    "dealPrice",
    "discountApplicationGroup",
    "discountCode",
    "discountLineTypeValue",
    "effectiveAmount",
    "isCompoundable",
    "isDiscountCodeRequired",
    "manualDiscountTypeValue",
    "offerId",
    "offerName",
    "percentage",
    "periodicDiscountTypeValue",
    "pricingPriorityNumber",
    "saleLineNumber",
    "thresholdAmountRequired"
})
public class DiscountLine
    extends CommerceEntity
{

    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "ConcurrencyModeValue")
    protected Integer concurrencyModeValue;
    @XmlElement(name = "CustomerDiscountTypeValue")
    protected Integer customerDiscountTypeValue;
    @XmlElement(name = "DealPrice")
    protected BigDecimal dealPrice;
    @XmlElementRef(name = "DiscountApplicationGroup", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> discountApplicationGroup;
    @XmlElementRef(name = "DiscountCode", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> discountCode;
    @XmlElement(name = "DiscountLineTypeValue")
    protected Integer discountLineTypeValue;
    @XmlElement(name = "EffectiveAmount")
    protected BigDecimal effectiveAmount;
    @XmlElement(name = "IsCompoundable")
    protected Boolean isCompoundable;
    @XmlElement(name = "IsDiscountCodeRequired")
    protected Boolean isDiscountCodeRequired;
    @XmlElement(name = "ManualDiscountTypeValue")
    protected Integer manualDiscountTypeValue;
    @XmlElementRef(name = "OfferId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> offerId;
    @XmlElementRef(name = "OfferName", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> offerName;
    @XmlElement(name = "Percentage")
    protected BigDecimal percentage;
    @XmlElement(name = "PeriodicDiscountTypeValue")
    protected Integer periodicDiscountTypeValue;
    @XmlElement(name = "PricingPriorityNumber")
    protected Integer pricingPriorityNumber;
    @XmlElement(name = "SaleLineNumber")
    protected BigDecimal saleLineNumber;
    @XmlElement(name = "ThresholdAmountRequired")
    protected BigDecimal thresholdAmountRequired;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the concurrencyModeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConcurrencyModeValue() {
        return concurrencyModeValue;
    }

    /**
     * Sets the value of the concurrencyModeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConcurrencyModeValue(Integer value) {
        this.concurrencyModeValue = value;
    }

    /**
     * Gets the value of the customerDiscountTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustomerDiscountTypeValue() {
        return customerDiscountTypeValue;
    }

    /**
     * Sets the value of the customerDiscountTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustomerDiscountTypeValue(Integer value) {
        this.customerDiscountTypeValue = value;
    }

    /**
     * Gets the value of the dealPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDealPrice() {
        return dealPrice;
    }

    /**
     * Sets the value of the dealPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDealPrice(BigDecimal value) {
        this.dealPrice = value;
    }

    /**
     * Gets the value of the discountApplicationGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiscountApplicationGroup() {
        return discountApplicationGroup;
    }

    /**
     * Sets the value of the discountApplicationGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiscountApplicationGroup(JAXBElement<String> value) {
        this.discountApplicationGroup = value;
    }

    /**
     * Gets the value of the discountCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiscountCode() {
        return discountCode;
    }

    /**
     * Sets the value of the discountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiscountCode(JAXBElement<String> value) {
        this.discountCode = value;
    }

    /**
     * Gets the value of the discountLineTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDiscountLineTypeValue() {
        return discountLineTypeValue;
    }

    /**
     * Sets the value of the discountLineTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDiscountLineTypeValue(Integer value) {
        this.discountLineTypeValue = value;
    }

    /**
     * Gets the value of the effectiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEffectiveAmount() {
        return effectiveAmount;
    }

    /**
     * Sets the value of the effectiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEffectiveAmount(BigDecimal value) {
        this.effectiveAmount = value;
    }

    /**
     * Gets the value of the isCompoundable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCompoundable() {
        return isCompoundable;
    }

    /**
     * Sets the value of the isCompoundable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCompoundable(Boolean value) {
        this.isCompoundable = value;
    }

    /**
     * Gets the value of the isDiscountCodeRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDiscountCodeRequired() {
        return isDiscountCodeRequired;
    }

    /**
     * Sets the value of the isDiscountCodeRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDiscountCodeRequired(Boolean value) {
        this.isDiscountCodeRequired = value;
    }

    /**
     * Gets the value of the manualDiscountTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getManualDiscountTypeValue() {
        return manualDiscountTypeValue;
    }

    /**
     * Sets the value of the manualDiscountTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setManualDiscountTypeValue(Integer value) {
        this.manualDiscountTypeValue = value;
    }

    /**
     * Gets the value of the offerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOfferId() {
        return offerId;
    }

    /**
     * Sets the value of the offerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOfferId(JAXBElement<String> value) {
        this.offerId = value;
    }

    /**
     * Gets the value of the offerName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOfferName() {
        return offerName;
    }

    /**
     * Sets the value of the offerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOfferName(JAXBElement<String> value) {
        this.offerName = value;
    }

    /**
     * Gets the value of the percentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentage() {
        return percentage;
    }

    /**
     * Sets the value of the percentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentage(BigDecimal value) {
        this.percentage = value;
    }

    /**
     * Gets the value of the periodicDiscountTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPeriodicDiscountTypeValue() {
        return periodicDiscountTypeValue;
    }

    /**
     * Sets the value of the periodicDiscountTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPeriodicDiscountTypeValue(Integer value) {
        this.periodicDiscountTypeValue = value;
    }

    /**
     * Gets the value of the pricingPriorityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPricingPriorityNumber() {
        return pricingPriorityNumber;
    }

    /**
     * Sets the value of the pricingPriorityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPricingPriorityNumber(Integer value) {
        this.pricingPriorityNumber = value;
    }

    /**
     * Gets the value of the saleLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaleLineNumber() {
        return saleLineNumber;
    }

    /**
     * Sets the value of the saleLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaleLineNumber(BigDecimal value) {
        this.saleLineNumber = value;
    }

    /**
     * Gets the value of the thresholdAmountRequired property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getThresholdAmountRequired() {
        return thresholdAmountRequired;
    }

    /**
     * Sets the value of the thresholdAmountRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setThresholdAmountRequired(BigDecimal value) {
        this.thresholdAmountRequired = value;
    }

}
