package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 * @since 24 SEP 16
 */
public class KioskVoidCartsRequest {

    @JsonIgnore
    private String cartId;

    @JsonProperty("reasonCodeLines")
    private List<?> reasonCodeLines = new ArrayList<>();

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public List<?> getReasonCodeLines() {
        return reasonCodeLines;
    }

    public void setReasonCodeLines(List<?> reasonCodeLines) {
        this.reasonCodeLines = reasonCodeLines;
    }

}
