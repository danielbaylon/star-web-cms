
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_InventTableResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_InventTableResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="InventTableExtCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_InventTableExt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_InventTableResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "inventTableExtCollection"
})
public class SDCInventTableResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "InventTableExtCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCInventTableExt> inventTableExtCollection;

    /**
     * Gets the value of the inventTableExtCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCInventTableExt }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCInventTableExt> getInventTableExtCollection() {
        return inventTableExtCollection;
    }

    /**
     * Sets the value of the inventTableExtCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCInventTableExt }{@code >}
     *     
     */
    public void setInventTableExtCollection(JAXBElement<ArrayOfSDCInventTableExt> value) {
        this.inventTableExtCollection = value;
    }

}
