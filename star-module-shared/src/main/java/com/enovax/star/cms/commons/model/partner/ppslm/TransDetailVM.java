package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.payment.tm.constant.EnovaxTmSystemStatus;
import com.enovax.star.cms.commons.constant.ppslm.PartnerPortalConst;
import com.enovax.star.cms.commons.datamodel.ppslm.*;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.util.ArrayList;
import java.util.List;

public class TransDetailVM {
    public static final String PURCHASE = "Purchase";
    public static final String REVALIDATE = "Revalidate";


    private Integer id;
    private String email;
    private String status;
    private String receiptNum;
    private String tmErrorCode;
    private String tmErrorMessage;
    private String tmStatusDateStr;
    private String createdDateStr;
    private String accountCode;
    private String orgName;
    private String address;
    private String validityStartDateStr;
    private String validityEndDateStr;
    private String username;
    private String paymentType;

    private List<ReceiptProdVM> prods;
    private String gstStr;
    private String exclGstStr;
    private String totalAmountStr;

    private String revalDtTmStr;
    private String transType;
    private String gstRateStr;
    private String displayStatus;
    private String tmStatus;
    private Boolean pinGenerated;
    private String offlinePaymentApprovalStatus;
    private String offlinePaymentApprovalDateText;

    private OfflinePaymentRequestVM offlinePaymentRequestVM;

    public TransDetailVM(){}


    public void populatePurchaseTransactionDetails(PPSLMInventoryTransaction trans, List<PPSLMInventoryTransactionItem> items, PPSLMPartner pa, PPSLMTAMainAccount mainAcc, PPSLMTASubAccount subAcc, PPSLMOfflinePaymentRequest offlinePayReq) {
        this.setOfflinePaymentApprovalStatus("");
        this.setOfflinePaymentApprovalDateText("");
        this.transType = PURCHASE;
        this.id = trans.getId();
        this.receiptNum = trans.getReceiptNum();
        this.status = TransactionUtil.getAdminStatus(trans.getStatus());
        this.tmStatus = trans.getTmStatus();
        this.pinGenerated = trans.isTicketGenerated();
        this.displayStatus = trans.getDisplayStatus();
        this.tmErrorCode = trans.getTmErrorCode();
        this.tmErrorMessage = trans.getTmErrorMessage();
        if (trans.getTmStatusDate() != null) {
            this.tmStatusDateStr = NvxDateUtils.formatDate(trans.getTmStatusDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }
        this.createdDateStr = NvxDateUtils.formatDate(trans.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        this.accountCode = pa.getAccountCode();
        this.orgName = pa.getOrgName();
        this.address = pa.getAddress();
        if (trans.getValidityStartDate()!=null) {
            this.validityStartDateStr = NvxDateUtils.formatDate(trans.getValidityStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }
        if (trans.getValidityEndDate()!=null) {
            this.validityEndDateStr = NvxDateUtils.formatDate(trans.getValidityEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }
        this.paymentType = trans.getPaymentType();
        this.username = trans.getUsername();
        this.totalAmountStr = TransactionUtil.priceWithDecimal(trans.getTotalAmount(), null);
        this.exclGstStr = TransactionUtil.getExclGSTCostWithDecimal(trans.getTotalAmount(), trans.getGstRate(), null);

        this.prods = new ArrayList<ReceiptProdVM>();
        if(items != null && items.size() > 0){
            for (PPSLMInventoryTransactionItem item : items) {
                ReceiptProdVM ipord = new ReceiptProdVM(item.getProductId(), item.getProductName());
                if (!prods.contains(ipord)) {
                    ipord.addProdItem(trans);
                    prods.add(ipord);
                }
            }
        }
        this.gstStr = TransactionUtil.gstWithDecimal(trans.getTotalAmount(), trans.getGstRate(), null);
        this.gstRateStr = TransactionUtil.getDisplayGSTRate(trans.getGstRate());

        if(PartnerPortalConst.OfflinePayment.equalsIgnoreCase(paymentType) && offlinePayReq != null){
            offlinePaymentApprovalStatus = offlinePayReq.getStatus();
            if(offlinePayReq.getApproveDate() != null){
                this.setOfflinePaymentApprovalDateText(NvxDateUtils.formatDate(offlinePayReq.getApproveDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
            }
        }
    }

    public void populateRevalidationTransactionDetails(PPSLMRevalidationTransaction revalTrans, PPSLMInventoryTransaction trans, List<PPSLMInventoryTransactionItem> items, PPSLMPartner pa, PPSLMTAMainAccount mainAcc, PPSLMTASubAccount subAcc) {
        this.setOfflinePaymentApprovalStatus("");
        this.setOfflinePaymentApprovalDateText("");
        this.transType = this.REVALIDATE;
        this.id = revalTrans.getId();
        this.receiptNum = revalTrans.getReceiptNum();
        this.status = TransactionUtil.getAdminStatus(revalTrans.getStatus());
        this.tmErrorCode = revalTrans.getTmErrorCode();
        this.tmErrorMessage = revalTrans.getTmErrorMessage();
        if (revalTrans.getTmStatusDate() != null) {
            this.tmStatusDateStr = NvxDateUtils.formatDate(revalTrans.getTmStatusDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }
        this.createdDateStr = NvxDateUtils.formatDate(revalTrans.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        this.accountCode = pa.getAccountCode();
        this.orgName = pa.getOrgName();
        this.address = pa.getAddress();
        if (EnovaxTmSystemStatus.Success.toString().equals(trans.getTmStatus())) {
            this.validityStartDateStr = NvxDateUtils.formatDate(trans.getValidityStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            this.validityEndDateStr = NvxDateUtils.formatDate(trans.getValidityEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }
        this.paymentType = revalTrans.getPaymentType();
        this.username = revalTrans.getUsername();
        this.totalAmountStr = TransactionUtil.priceWithDecimal(revalTrans.getTotal(), null);

        this.prods = new ArrayList<ReceiptProdVM>();
        for (PPSLMInventoryTransactionItem item : items) {
            ReceiptProdVM ipord = new ReceiptProdVM(item.getProductId(), item.getProductName());
            if (!prods.contains(ipord)) {
                ipord.addProdItem(revalTrans);
                prods.add(ipord);
            }
        }
        this.gstStr = TransactionUtil.gstWithDecimal(revalTrans.getTotal(), revalTrans.getGstRate(), null);
        this.exclGstStr = TransactionUtil.getExclGSTCostWithDecimal(revalTrans.getTotal(), revalTrans.getGstRate(), null);
        this.revalDtTmStr = NvxDateUtils.formatDate(revalTrans.getCreatedDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        this.gstRateStr = TransactionUtil.getDisplayGSTRate(revalTrans.getGstRate());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ReceiptProdVM> getProds() {
        return prods;
    }

    public void setProds(List<ReceiptProdVM> prods) {
        this.prods = prods;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTmErrorCode() {
        return tmErrorCode;
    }

    public void setTmErrorCode(String tmErrorCode) {
        this.tmErrorCode = tmErrorCode;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getTmStatusDateStr() {
        return tmStatusDateStr;
    }

    public void setTmStatusDateStr(String tmStatusDateStr) {
        this.tmStatusDateStr = tmStatusDateStr;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getValidityStartDateStr() {
        return validityStartDateStr;
    }

    public void setValidityStartDateStr(String validityStartDateStr) {
        this.validityStartDateStr = validityStartDateStr;
    }

    public String getValidityEndDateStr() {
        return validityEndDateStr;
    }

    public void setValidityEndDateStr(String validityEndDateStr) {
        this.validityEndDateStr = validityEndDateStr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTotalAmountStr() {
        return totalAmountStr;
    }

    public void setTotalAmountStr(String totalAmountStr) {
        this.totalAmountStr = totalAmountStr;
    }

    public String getGstStr() {
        return gstStr;
    }

    public void setGstStr(String gstStr) {
        this.gstStr = gstStr;
    }

    public String getRevalDtTmStr() {
        return revalDtTmStr;
    }

    public void setRevalDtTmStr(String revalDtTmStr) {
        this.revalDtTmStr = revalDtTmStr;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getExclGstStr() {
        return exclGstStr;
    }

    public void setExclGstStr(String exclGstStr) {
        this.exclGstStr = exclGstStr;
    }

    public String getCreatedDateStr() {
        return createdDateStr;
    }

    public void setCreatedDateStr(String createdDateStr) {
        this.createdDateStr = createdDateStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGstRateStr() {
        return gstRateStr;
    }

    public void setGstRateStr(String gstRateStr) {
        this.gstRateStr = gstRateStr;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public Boolean getPinGenerated() {
        return pinGenerated;
    }

    public void setPinGenerated(Boolean pinGenerated) {
        this.pinGenerated = pinGenerated;
    }

    public String getOfflinePaymentApprovalDateText() {
        return offlinePaymentApprovalDateText;
    }

    public void setOfflinePaymentApprovalDateText(String offlinePaymentApprovalDateText) {
        this.offlinePaymentApprovalDateText = offlinePaymentApprovalDateText;
    }

    public String getOfflinePaymentApprovalStatus() {
        return offlinePaymentApprovalStatus;
    }

    public void setOfflinePaymentApprovalStatus(String offlinePaymentApprovalStatus) {
        this.offlinePaymentApprovalStatus = offlinePaymentApprovalStatus;
    }

    public OfflinePaymentRequestVM getOfflinePaymentRequestVM() {
        return offlinePaymentRequestVM;
    }

    public void setOfflinePaymentRequestVM(OfflinePaymentRequestVM offlinePaymentRequestVM) {
        this.offlinePaymentRequestVM = offlinePaymentRequestVM;
    }
}
