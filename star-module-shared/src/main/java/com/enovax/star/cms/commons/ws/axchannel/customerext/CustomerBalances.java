
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerBalances complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerBalances">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Balance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CreditLimit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoiceAccountBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoiceAccountCreditLimit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="InvoiceAccountPendingBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PendingBalance" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerBalances", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", propOrder = {
    "balance",
    "creditLimit",
    "invoiceAccountBalance",
    "invoiceAccountCreditLimit",
    "invoiceAccountPendingBalance",
    "pendingBalance"
})
public class CustomerBalances {

    @XmlElement(name = "Balance")
    protected BigDecimal balance;
    @XmlElement(name = "CreditLimit")
    protected BigDecimal creditLimit;
    @XmlElement(name = "InvoiceAccountBalance")
    protected BigDecimal invoiceAccountBalance;
    @XmlElement(name = "InvoiceAccountCreditLimit")
    protected BigDecimal invoiceAccountCreditLimit;
    @XmlElement(name = "InvoiceAccountPendingBalance")
    protected BigDecimal invoiceAccountPendingBalance;
    @XmlElement(name = "PendingBalance")
    protected BigDecimal pendingBalance;

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditLimit(BigDecimal value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the invoiceAccountBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInvoiceAccountBalance() {
        return invoiceAccountBalance;
    }

    /**
     * Sets the value of the invoiceAccountBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInvoiceAccountBalance(BigDecimal value) {
        this.invoiceAccountBalance = value;
    }

    /**
     * Gets the value of the invoiceAccountCreditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInvoiceAccountCreditLimit() {
        return invoiceAccountCreditLimit;
    }

    /**
     * Sets the value of the invoiceAccountCreditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInvoiceAccountCreditLimit(BigDecimal value) {
        this.invoiceAccountCreditLimit = value;
    }

    /**
     * Gets the value of the invoiceAccountPendingBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInvoiceAccountPendingBalance() {
        return invoiceAccountPendingBalance;
    }

    /**
     * Sets the value of the invoiceAccountPendingBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInvoiceAccountPendingBalance(BigDecimal value) {
        this.invoiceAccountPendingBalance = value;
    }

    /**
     * Gets the value of the pendingBalance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPendingBalance() {
        return pendingBalance;
    }

    /**
     * Sets the value of the pendingBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPendingBalance(BigDecimal value) {
        this.pendingBalance = value;
    }

}
