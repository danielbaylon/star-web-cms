package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.util.NvxDateUtils;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by lavanya on 8/11/16.
 */
public class PackageReportModel {
    private String orgName;
    private String pinCode;
    private String ticketMedia;
    private Date ticketGeneratedDate;
    private Date expiryDate;
    private String packageName;
    private String packageDesc;
    private Integer pinCodeQty;
    private Integer qtyRedeemed;
    private Date lastRedemptionDate;
    private Integer pkgId;
    private String indivItems;
    private String indivReceipts;
    private String itemQty;
    private String itemQtyRedeemed;

    @Transient
    private String ticketGeneratedDateText;
    @Transient
    private String expiryDateText;
    @Transient
    private String lastRedemptionDateText;

    public void initReportValues() {
        this.ticketGeneratedDateText = ticketGeneratedDate == null ? "" : NvxDateUtils.formatDateForDisplay(ticketGeneratedDate, true);
        this.expiryDateText = expiryDate == null ? "" : NvxDateUtils.formatDateForDisplay(expiryDate, false);
        this.lastRedemptionDateText = lastRedemptionDate == null ? "" : NvxDateUtils.formatDateForDisplay(lastRedemptionDate, true);
        //itemQtyArray = Arrays.asList(itemQty.split(","));
        if(!this.ticketMedia.equalsIgnoreCase("Pincode")) {
            this.itemQtyRedeemed = "NA";
        }
    }

    /*
     * GET + SET
     */

    public String getExpiryDateText() {
        return expiryDateText;
    }
    public void setExpiryDateText(String expiryDateText) {
        this.expiryDateText = expiryDateText;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Date getTicketGeneratedDate() {
        return ticketGeneratedDate;
    }

    public void setTicketGeneratedDate(Date ticketGeneratedDate) {
        this.ticketGeneratedDate = ticketGeneratedDate;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getPinCodeQty() {
        return pinCodeQty;
    }

    public void setPinCodeQty(Integer pinCodeQty) {
        this.pinCodeQty = pinCodeQty;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public Date getLastRedemptionDate() {
        return lastRedemptionDate;
    }

    public void setLastRedemptionDate(Date lastRedemptionDate) {
        this.lastRedemptionDate = lastRedemptionDate;
    }

    public Integer getPkgId() {
        return pkgId;
    }

    public void setPkgId(Integer pkgId) {
        this.pkgId = pkgId;
    }

    public String getIndivItems() {
        return indivItems;
    }

    public void setIndivItems(String indivItems) {
        this.indivItems = indivItems;
    }

    public String getIndivReceipts() {
        return indivReceipts;
    }

    public void setIndivReceipts(String indivReceipts) {
        this.indivReceipts = indivReceipts;
    }

    public String getTicketGeneratedDateText() {
        return ticketGeneratedDateText;
    }

    public void setTicketGeneratedDateText(String ticketGeneratedDateText) {
        this.ticketGeneratedDateText = ticketGeneratedDateText;
    }

    public String getLastRedemptionDateText() {
        return lastRedemptionDateText;
    }

    public void setLastRedemptionDateText(String lastRedemptionDateText) {
        this.lastRedemptionDateText = lastRedemptionDateText;
    }

    public String getTicketMedia() {
        return ticketMedia;
    }

    public void setTicketMedia(String ticketMedia) {
        this.ticketMedia = ticketMedia;
    }

    public String getPackageDesc() {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc) {
        this.packageDesc = packageDesc;
    }

    public String getItemQty() {
        return itemQty;
    }

    public void setItemQty(String itemQty) {
        this.itemQty = itemQty;
    }

    public String getItemQtyRedeemed() {
        return itemQtyRedeemed;
    }

    public void setItemQtyRedeemed(String itemQtyRedeemed) {
        this.itemQtyRedeemed = itemQtyRedeemed;
    }

}
