package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMApprovalLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMApprovalLogRepository extends JpaRepository<PPSLMApprovalLog, Integer>, JpaSpecificationExecutor<PPSLMApprovalLog>{

    @Query("from PPSLMApprovalLog where approverType = ?1 and status = ?2 ")
    List<PPSLMApprovalLog> findAllByApproverTypeAndStatus(String type, String status);

    PPSLMApprovalLog findById(Integer id);

    List<PPSLMApprovalLog> findByCategoryAndRelatedKeyOrderByCreatedDateDesc(String category, String relatedKey);

    @Query("from PPSLMApprovalLog where relatedKey = ?1 and category = ?2 and status='Pending' order by createdDate")
    List<PPSLMApprovalLog> findPendingRequest(String relatedKey, String category);

    @Query("from PPSLMApprovalLog where relatedKey = ?1 and category = 'Partner' and actionType = 'Resubmit' and status is null order by createdDate desc")
    List<PPSLMApprovalLog> findPartnerLatestApproverLog(String id);


    @Query(value = "select count(*) from PPSLMApprovalLog where status = 'Pending' and " +
            " ((currentValue  not like '%<excluProdIds>' + :productId + '</excluProdIds>%' and " +
            "   previousValue like '%<excluProdIds>' + :productId + '</excluProdIds>%') or " +
            "  (previousValue not like '%<excluProdIds>' + :productId + '</excluProdIds>%' and " +
            "   currentValue like '%<excluProdIds>' + :productId + '</excluProdIds>%'))", nativeQuery = true)
    int getPendingRequestForProdCnt(@Param("productId") String productId);
}
