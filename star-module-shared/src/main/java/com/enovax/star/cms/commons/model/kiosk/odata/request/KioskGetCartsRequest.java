package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskGetCartsRequest {

    @JsonProperty("Id")
    private String cartId;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

}
