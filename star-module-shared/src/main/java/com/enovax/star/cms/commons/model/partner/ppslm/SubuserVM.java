package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.List;

/**
 * Created by lavanya on 26/8/16.
 */
public class SubuserVM {
    private List<PartnerAccountVM> paAccountVMs;
    private List<PPSLMTAAccessGroupVM> paAccessGroupVMs;
    int total;

    public SubuserVM() {

    }

    public SubuserVM(List<PartnerAccountVM> paAccountVMs, List<PPSLMTAAccessGroupVM> paAccessGroupVMs, int total) {
        this.paAccountVMs = paAccountVMs;
        this.paAccessGroupVMs = paAccessGroupVMs;
        this.total = total;
    }

    public List<PartnerAccountVM> getPaAccountVMs() {
        return paAccountVMs;
    }

    public void setPaAccountVMs(List<PartnerAccountVM> paAccountVMs) {
        this.paAccountVMs = paAccountVMs;
    }

    public List<PPSLMTAAccessGroupVM> getPaAccessGroupVMs() {
        return paAccessGroupVMs;
    }

    public void setPaAccessGroupVMs(List<PPSLMTAAccessGroupVM> paAccessGroupVMs) {
        this.paAccessGroupVMs = paAccessGroupVMs;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
