package com.enovax.star.cms.commons.model.booking.skydining;

import com.enovax.star.cms.commons.model.booking.FunCartDisplayCmsProduct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningFunCartDisplayProduct extends FunCartDisplayCmsProduct {

  private Integer serviceCharge;
  private Integer gst;

  private BigDecimal serviceChargeTotal;
  private BigDecimal gstTotal;
  private BigDecimal topUpPrice;

  private String serviceChargeTotalText;
  private String gstTotalText;
  private String topUpPriceText;

  private List<SkyDiningFunCartDisplayItem> skyDiningItems = new ArrayList<>();

  public List<SkyDiningFunCartDisplayItem> getSkyDiningItems() {
    return skyDiningItems;
  }

  public void setSkyDiningItems(List<SkyDiningFunCartDisplayItem> skyDiningItems) {
    this.skyDiningItems = skyDiningItems;
  }

  public Integer getServiceCharge() {
    return serviceCharge;
  }

  public void setServiceCharge(Integer serviceCharge) {
    this.serviceCharge = serviceCharge;
  }

  public Integer getGst() {
    return gst;
  }

  public void setGst(Integer gst) {
    this.gst = gst;
  }

  public BigDecimal getServiceChargeTotal() {
    return serviceChargeTotal;
  }

  public void setServiceChargeTotal(BigDecimal serviceChargeTotal) {
    this.serviceChargeTotal = serviceChargeTotal;
  }

  public BigDecimal getGstTotal() {
    return gstTotal;
  }

  public void setGstTotal(BigDecimal gstTotal) {
    this.gstTotal = gstTotal;
  }

  public BigDecimal getTopUpPrice() {
    return topUpPrice;
  }

  public void setTopUpPrice(BigDecimal topUpPrice) {
    this.topUpPrice = topUpPrice;
  }

  public String getServiceChargeTotalText() {
    return serviceChargeTotalText;
  }

  public void setServiceChargeTotalText(String serviceChargeTotalText) {
    this.serviceChargeTotalText = serviceChargeTotalText;
  }

  public String getGstTotalText() {
    return gstTotalText;
  }

  public void setGstTotalText(String gstTotalText) {
    this.gstTotalText = gstTotalText;
  }

  public String getTopUpPriceText() {
    return topUpPriceText;
  }

  public void setTopUpPriceText(String topUpPriceText) {
    this.topUpPriceText = topUpPriceText;
  }
}
