package com.enovax.star.cms.commons.model.kiosk.odata.request;

public class KioskOpenShiftRequest {

    private String cashDrawer = "";

    private String isShared = Boolean.FALSE.toString();

    private String shiftId;

    public String getCashDrawer() {
        return cashDrawer;
    }

    public void setCashDrawer(String cashDrawer) {
        this.cashDrawer = cashDrawer;
    }

    public String getIsShared() {
        return isShared;
    }

    public void setIsShared(String isShared) {
        this.isShared = isShared;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

}
