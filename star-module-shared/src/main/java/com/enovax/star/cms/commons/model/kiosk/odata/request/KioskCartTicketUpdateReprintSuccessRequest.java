package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdateReprintSuccessCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartTicketUpdateReprintSuccessRequest {

	@JsonProperty("ReprintSuccessCriteria")
	private AxCartTicketUpdateReprintSuccessCriteria axCartTicketUpdateReprintSuccessCriteria;

	public void setAxCartTicketUpdateReprintSuccessCriteria(AxCartTicketUpdateReprintSuccessCriteria axCartTicketUpdateReprintSuccessCriteria) {
		this.axCartTicketUpdateReprintSuccessCriteria = axCartTicketUpdateReprintSuccessCriteria;
	}

	public AxCartTicketUpdateReprintSuccessCriteria getAxCartTicketUpdateReprintSuccessCriteria() {
		return axCartTicketUpdateReprintSuccessCriteria;
	}
}
