package com.enovax.star.cms.commons.mgnl.definition;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Created by jennylynsze on 6/20/16.
 */
public enum DataType {
    Date,
    Money,
    Boolean;

    public static String getFormattedDisplay(Object data, String type) {

        if(data == null) {
            return null;
        }

        if(DataType.Date.name().equals(type)) {
            return new SimpleDateFormat("dd/MM/yyyy").format(data);
        }else if (DataType.Money.name().equals(type)) {
            return new DecimalFormat("0.00").format(data);
        }else if (DataType.Boolean.name().equals(type)) {
            return (Boolean)data?"Yes":"No";
        }
        return data.toString();
    }
}
