package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PPMFLGTASubAccount")
public class PPMFLGTASubAccount extends PPMFLGTAAccount implements Serializable {

    @ManyToOne
    @JoinColumn(name = "mainAccountId", nullable = false)
    private PPMFLGTAMainAccount mainUser;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", targetEntity = PPMFLGTAPrevPasswordSub.class)
    private List<PPMFLGPrevPassword> prevPws = new ArrayList<PPMFLGPrevPassword>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "subUser")
    private List<PPMFLGTARightsMapping> rightsMapping = new ArrayList<PPMFLGTARightsMapping>();

    @Transient
    private List<String> rightsMappingId;

    @Column(name = "title", nullable = false)
    private String title;

    public PPMFLGTAMainAccount getTAMainAccount() {
        return mainUser;
    }

    public void setTAMainAccount(PPMFLGTAMainAccount user) {
        this.mainUser = user;
    }

    public List<PPMFLGPrevPassword> getPrevPws() {
        return prevPws;
    }

    public void setPrevPws(List<PPMFLGPrevPassword> prevPws) {
        this.prevPws = prevPws;
    }

    public PPMFLGTAMainAccount getMainUser() {
        return mainUser;
    }

    public void setMainUser(PPMFLGTAMainAccount mainUser) {
        this.mainUser = mainUser;
    }

    public List<PPMFLGTARightsMapping> getRightsMapping() {
        return rightsMapping;
    }

    public void setRightsMapping(List<PPMFLGTARightsMapping> rightsMapping) {
        this.rightsMapping = rightsMapping;
    }

    public List<String> getRightsMappingId() {
        return rightsMappingId;
    }

    public void setRightsMappingId(List<String> rightsMappingId) {
        this.rightsMappingId = rightsMappingId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public boolean hasPermission(Integer rightsId) {
        for (PPMFLGTARightsMapping rightmap : getRightsMapping()) {
            if (rightsId != null
                    && rightsId.equals(rightmap.getAccessRightsGroup().getId())) {
                return true;
            }
        }
        return false;
    }

}
