package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.model.axstar.AxStarCart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FunCart {

    private String cartId;
    private String channelCode;
    private List<FunCartItem> items = new ArrayList<>();

    private AxStarCart axCart;
    private String axCustomerId = "";

    private Map<String, FunCartItem> axLineCommentMap = new HashMap<>();

    private FunCartAffiliation bankAffiliation;
    private FunCartAffiliation paymentModeAffiliation;
    /**
     * Key is promo code
     * Value is offer ID
     */
    private Map<String, String> appliedPromoCodes = new HashMap<>();

    private List<String> productListingIdList = new ArrayList<>();

    private CustomerDetails savedCustomerDeets = new CustomerDetails();

    public List<FunCartItem> getItems() {
        return items;
    }

    public void setItems(List<FunCartItem> items) {
        this.items = items;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public AxStarCart getAxCart() {
        return axCart;
    }

    public void setAxCart(AxStarCart axCart) {
        this.axCart = axCart;
    }

    public String getAxCustomerId() {
        return axCustomerId;
    }

    public void setAxCustomerId(String axCustomerId) {
        this.axCustomerId = axCustomerId;
    }

    public Map<String, FunCartItem> getAxLineCommentMap() {
        return axLineCommentMap;
    }

    public void setAxLineCommentMap(Map<String, FunCartItem> axLineCommentMap) {
        this.axLineCommentMap = axLineCommentMap;
    }

    public List<String> getProductListingIdList() {
        return productListingIdList;
    }

    public void setProductListingIdList(List<String> productListingIdList) {
        this.productListingIdList = productListingIdList;
    }

    public FunCartAffiliation getBankAffiliation() {
        return bankAffiliation;
    }

    public void setBankAffiliation(FunCartAffiliation bankAffiliation) {
        this.bankAffiliation = bankAffiliation;
    }

    public FunCartAffiliation getPaymentModeAffiliation() {
        return paymentModeAffiliation;
    }

    public void setPaymentModeAffiliation(FunCartAffiliation paymentModeAffiliation) {
        this.paymentModeAffiliation = paymentModeAffiliation;
    }

    public CustomerDetails getSavedCustomerDeets() {
        return savedCustomerDeets;
    }

    public void setSavedCustomerDeets(CustomerDetails savedCustomerDeets) {
        this.savedCustomerDeets = savedCustomerDeets;
    }

    public Map<String, String> getAppliedPromoCodes() {
        return appliedPromoCodes;
    }

    public void setAppliedPromoCodes(Map<String, String> appliedPromoCodes) {
        this.appliedPromoCodes = appliedPromoCodes;
    }
}
