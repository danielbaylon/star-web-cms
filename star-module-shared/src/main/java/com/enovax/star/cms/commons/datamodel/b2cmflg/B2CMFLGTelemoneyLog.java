package com.enovax.star.cms.commons.datamodel.b2cmflg;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = B2CMFLGTelemoneyLog.TABLE_NAME)
public class B2CMFLGTelemoneyLog {

    public static final String TABLE_NAME = "B2CMFLGTelemoneyLog";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;
    @Column(name="tmMerchantId", length=20)
    private String tmMerchantId;
    @Column(name="tmRefNo", length=40)
    private String tmRefNo;
    @Column(name="tmCurrency", length=10)
    private String tmCurrency;
    @Column(name="tmDebitAmt", scale=4)
    private BigDecimal tmDebitAmt;
    @Column(name="tmStatus", length=10)
    private String tmStatus;
    @Column(name="tmErrorMessage")
    private String tmErrorMessage;
    @Column(name="tmError", length=20)
    private String tmError;
    @Column(name="tmPaymentType", length=10)
    private String tmPaymentType;
    @Column(name="tmApprovalCode", length=10)
    private String tmApprovalCode;
    @Column(name="tmBankRespCode", length=10)
    private String tmBankRespCode;
    @Column(name="tmUserField1", length=20)
    private String tmUserField1;
    @Column(name="tmUserField2", length=20)
    private String tmUserField2;
    @Column(name="tmUserField3", length=20)
    private String tmUserField3;
    @Column(name="tmUserField4", length=20)
    private String tmUserField4;
    @Column(name="tmUserField5", length=20)
    private String tmUserField5;
    @Column(name="tmTransType", length=10)
    private String tmTransType;
    @Column(name="tmSubTransType", length=10)
    private String tmSubTransType;
    @Column(name="tmCcLast4Digits")
    private String tmCcLast4Digits;
    @Column(name="tmExpiryDate", length=10)
    private String tmExpiryDate;
    @Column(name="tmRecurrentId", length=20)
    private String tmRecurrentId;
    @Column(name="tmSubsequentMid", length=20)
    private String tmSubsequentMid;
    @Column(name="isQuery", nullable=false)
    private boolean isQuery;
    @Column(name="tmCcNum")
    private String tmCcNum;
    @Column(name="tmEci")
    private String tmEci;
    @Column(name="tmCavv")
    private String tmCavv;
    @Column(name="tmXid")
    private String tmXid;

    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTmMerchantId() {
        return this.tmMerchantId;
    }
    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }
    public String getTmRefNo() {
        return this.tmRefNo;
    }
    public void setTmRefNo(String tmRefNo) {
        this.tmRefNo = tmRefNo;
    }
    public String getTmCurrency() {
        return this.tmCurrency;
    }
    public void setTmCurrency(String tmCurrency) {
        this.tmCurrency = tmCurrency;
    }
    public BigDecimal getTmDebitAmt() {
        return this.tmDebitAmt;
    }
    public void setTmDebitAmt(BigDecimal tmDebitAmt) {
        this.tmDebitAmt = tmDebitAmt;
    }
    public String getTmStatus() {
        return this.tmStatus;
    }
    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }
    public String getTmErrorMessage() {
        return this.tmErrorMessage;
    }
    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }
    public String getTmError() {
        return this.tmError;
    }
    public void setTmError(String tmError) {
        this.tmError = tmError;
    }
    public String getTmPaymentType() {
        return this.tmPaymentType;
    }
    public void setTmPaymentType(String tmPaymentType) {
        this.tmPaymentType = tmPaymentType;
    }
    public String getTmApprovalCode() {
        return this.tmApprovalCode;
    }
    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }
    public String getTmBankRespCode() {
        return this.tmBankRespCode;
    }
    public void setTmBankRespCode(String tmBankRespCode) {
        this.tmBankRespCode = tmBankRespCode;
    }
    public String getTmUserField1() {
        return this.tmUserField1;
    }
    public void setTmUserField1(String tmUserField1) {
        this.tmUserField1 = tmUserField1;
    }
    public String getTmUserField2() {
        return this.tmUserField2;
    }
    public void setTmUserField2(String tmUserField2) {
        this.tmUserField2 = tmUserField2;
    }
    public String getTmUserField3() {
        return this.tmUserField3;
    }
    public void setTmUserField3(String tmUserField3) {
        this.tmUserField3 = tmUserField3;
    }
    public String getTmUserField4() {
        return this.tmUserField4;
    }
    public void setTmUserField4(String tmUserField4) {
        this.tmUserField4 = tmUserField4;
    }
    public String getTmUserField5() {
        return this.tmUserField5;
    }
    public void setTmUserField5(String tmUserField5) {
        this.tmUserField5 = tmUserField5;
    }
    public String getTmTransType() {
        return this.tmTransType;
    }
    public void setTmTransType(String tmTransType) {
        this.tmTransType = tmTransType;
    }
    public String getTmSubTransType() {
        return this.tmSubTransType;
    }
    public void setTmSubTransType(String tmSubTransType) {
        this.tmSubTransType = tmSubTransType;
    }
    public String getTmCcLast4Digits() {
        return this.tmCcLast4Digits;
    }
    public void setTmCcLast4Digits(String tmCcLast4Digits) {
        this.tmCcLast4Digits = tmCcLast4Digits;
    }
    public String getTmExpiryDate() {
        return this.tmExpiryDate;
    }
    public void setTmExpiryDate(String tmExpiryDate) {
        this.tmExpiryDate = tmExpiryDate;
    }
    public String getTmRecurrentId() {
        return this.tmRecurrentId;
    }
    public void setTmRecurrentId(String tmRecurrentId) {
        this.tmRecurrentId = tmRecurrentId;
    }
    public String getTmSubsequentMid() {
        return this.tmSubsequentMid;
    }
    public void setTmSubsequentMid(String tmSubsequentMid) {
        this.tmSubsequentMid = tmSubsequentMid;
    }
    public boolean isIsQuery() {
        return this.isQuery;
    }
    public void setIsQuery(boolean isQuery) {
        this.isQuery = isQuery;
    }
    public String getTmCcNum() {
        return tmCcNum;
    }
    public void setTmCcNum(String tmCcNum) {
        this.tmCcNum = tmCcNum;
    }
    public String getTmEci() {
        return tmEci;
    }
    public void setTmEci(String tmEci) {
        this.tmEci = tmEci;
    }
    public String getTmCavv() {
        return tmCavv;
    }
    public void setTmCavv(String tmCavv) {
        this.tmCavv = tmCavv;
    }
    public String getTmXid() {
        return tmXid;
    }
    public void setTmXid(String tmXid) {
        this.tmXid = tmXid;
    }
}
