
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TenderLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TenderLine">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}TenderLineBase">
 *       &lt;sequence>
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncomeExpenseAccountTypeValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="IsPreProcessed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MaskedCardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TenderDate" type="{http://schemas.datacontract.org/2004/07/System}DateTimeOffset" minOccurs="0"/>
 *         &lt;element name="TransactionStatusValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderLine", propOrder = {
    "authorization",
    "cardToken",
    "incomeExpenseAccountTypeValue",
    "isPreProcessed",
    "maskedCardNumber",
    "tenderDate",
    "transactionStatusValue"
})
public class TenderLine
    extends TenderLineBase
{

    @XmlElementRef(name = "Authorization", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> authorization;
    @XmlElementRef(name = "CardToken", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardToken;
    @XmlElement(name = "IncomeExpenseAccountTypeValue")
    protected Integer incomeExpenseAccountTypeValue;
    @XmlElement(name = "IsPreProcessed")
    protected Boolean isPreProcessed;
    @XmlElementRef(name = "MaskedCardNumber", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maskedCardNumber;
    @XmlElementRef(name = "TenderDate", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<DateTimeOffset> tenderDate;
    @XmlElement(name = "TransactionStatusValue")
    protected Integer transactionStatusValue;

    /**
     * Gets the value of the authorization property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAuthorization() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAuthorization(JAXBElement<String> value) {
        this.authorization = value;
    }

    /**
     * Gets the value of the cardToken property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardToken() {
        return cardToken;
    }

    /**
     * Sets the value of the cardToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardToken(JAXBElement<String> value) {
        this.cardToken = value;
    }

    /**
     * Gets the value of the incomeExpenseAccountTypeValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIncomeExpenseAccountTypeValue() {
        return incomeExpenseAccountTypeValue;
    }

    /**
     * Sets the value of the incomeExpenseAccountTypeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIncomeExpenseAccountTypeValue(Integer value) {
        this.incomeExpenseAccountTypeValue = value;
    }

    /**
     * Gets the value of the isPreProcessed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPreProcessed() {
        return isPreProcessed;
    }

    /**
     * Sets the value of the isPreProcessed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPreProcessed(Boolean value) {
        this.isPreProcessed = value;
    }

    /**
     * Gets the value of the maskedCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * Sets the value of the maskedCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaskedCardNumber(JAXBElement<String> value) {
        this.maskedCardNumber = value;
    }

    /**
     * Gets the value of the tenderDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public JAXBElement<DateTimeOffset> getTenderDate() {
        return tenderDate;
    }

    /**
     * Sets the value of the tenderDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}
     *     
     */
    public void setTenderDate(JAXBElement<DateTimeOffset> value) {
        this.tenderDate = value;
    }

    /**
     * Gets the value of the transactionStatusValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTransactionStatusValue() {
        return transactionStatusValue;
    }

    /**
     * Sets the value of the transactionStatusValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTransactionStatusValue(Integer value) {
        this.transactionStatusValue = value;
    }

}
