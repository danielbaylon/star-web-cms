package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by jennylynsze on 5/17/16.
 */
public enum PkgStatus {
    Reserved,
    Available,
    Expired,
    Redeemed,
    Deactivated,
    Failed;

    public static String getSTPkgStatus(String code) {
        switch (code) {
            case "0": return Available.toString();//A
            case "1": return Deactivated.toString();//D
            case "2": return Redeemed.toString();//F
            case "5": return Available.toString();//P
            default: return null;
        }
    }
}
