package com.enovax.star.cms.commons.model.axchannel.customerext;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houtao on 18/8/16.
 */
public class AxCustomerTransactionDetails {

    private BigDecimal amountCur;
    private String currencyCode;
    private String documentNum;
    private String invoice;
    private String orderAccount;
    private String paymentReference;
    private Date   transDate;
    private String transType;
    private String voucher;
    private String salesPoolId;
    private String type;



    public BigDecimal getAmountCur() {
        return amountCur;
    }

    public void setAmountCur(BigDecimal amountCur) {
        this.amountCur = amountCur;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDocumentNum() {
        return documentNum;
    }

    public void setDocumentNum(String documentNum) {
        this.documentNum = documentNum;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getOrderAccount() {
        return orderAccount;
    }

    public void setOrderAccount(String orderAccount) {
        this.orderAccount = orderAccount;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getSalesPoolId() {
        return salesPoolId;
    }

    public void setSalesPoolId(String salesPoolId) {
        this.salesPoolId = salesPoolId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
