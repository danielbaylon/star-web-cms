package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
public abstract class PPSLMPrevPassword implements Serializable, Comparable<PPSLMPrevPassword> {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "salt", nullable = false)
    private String salt;
	
	
	@Column(name = "createdDate", nullable = false, length = 23)
    private Date createdDate;
    
    
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    
    public String getSalt() {
        return salt;
    }
    public void setSalt(String salt) {
        this.salt = salt;
    }
    
    
    public Date getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    
    /**
     * Sort by created date
     */
    @Override
    public int compareTo(PPSLMPrevPassword pp2) {
        return this.createdDate.compareTo(pp2.getCreatedDate());
    }
    

}
