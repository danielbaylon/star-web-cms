package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartner;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

public class PPMFLGPartnerSpecification extends BaseSpecification<PPMFLGPartner> {

}
