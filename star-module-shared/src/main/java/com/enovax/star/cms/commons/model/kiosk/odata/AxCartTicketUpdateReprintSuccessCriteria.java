package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AxCartTicketUpdateReprintSuccessCriteria {
    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("DataLevelValue")
    private int dataLevelValue = 4;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setDataLevelValue(int dataLevelValue) {
        this.dataLevelValue = dataLevelValue;
    }

    public int getDataLevelValue() {
        return dataLevelValue;
    }

}
