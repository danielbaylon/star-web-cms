
package com.enovax.star.cms.commons.ws.axchannel.b2bpinview;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSDC_B2BPINViewTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_B2BPINViewTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_B2BPINViewTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_B2BPINViewTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_B2BPINViewTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcb2BPINViewTable"
})
public class ArrayOfSDCB2BPINViewTable {

    @XmlElement(name = "SDC_B2BPINViewTable", nillable = true)
    protected List<SDCB2BPINViewTable> sdcb2BPINViewTable;

    /**
     * Gets the value of the sdcb2BPINViewTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcb2BPINViewTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCB2BPINViewTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCB2BPINViewTable }
     * 
     * 
     */
    public List<SDCB2BPINViewTable> getSDCB2BPINViewTable() {
        if (sdcb2BPINViewTable == null) {
            sdcb2BPINViewTable = new ArrayList<SDCB2BPINViewTable>();
        }
        return this.sdcb2BPINViewTable;
    }

}
