package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxJournalTransaction;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskSearchJournalTransactionsResponse {

	@JsonProperty("value")
	private List<AxJournalTransaction> axJournalTransactionList;

	public void setAxJournalTransactionList(List<AxJournalTransaction> axJournalTransactionList) {
		this.axJournalTransactionList = axJournalTransactionList;
	}

	public List<AxJournalTransaction> getAxJournalTransactionList() {
		return axJournalTransactionList;
	}

}
