
package com.enovax.star.cms.commons.ws.axchannel.b2bpinview;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.b2bpinview package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCB2BPINViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINViewLineTable");
    private final static QName _SDCB2BPINTableInquiryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINTableInquiryResponse");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _SDCB2BPINViewTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINViewTable");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _SDCB2BPINViewResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINViewResponse");
    private final static QName _ArrayOfSDCB2BPINViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_B2BPINViewLineTable");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ArrayOfSDCB2BPINViewTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_B2BPINViewTable");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _GetB2BPINViewCustAccount_QNAME = new QName("http://tempuri.org/", "custAccount");
    private final static QName _GetB2BPINTableInquiryResponseGetB2BPINTableInquiryResult_QNAME = new QName("http://tempuri.org/", "GetB2BPINTableInquiryResult");
    private final static QName _SDCB2BPINViewTablePINBlockedDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINBlockedDateTime");
    private final static QName _SDCB2BPINViewTablePINCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINCode");
    private final static QName _SDCB2BPINViewTableReasonCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReasonCode");
    private final static QName _SDCB2BPINViewTablePINViewLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINViewLineCollection");
    private final static QName _SDCB2BPINViewTableLastRedeemDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LastRedeemDateTime");
    private final static QName _SDCB2BPINViewTableDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Description");
    private final static QName _SDCB2BPINViewTablePackageName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PackageName");
    private final static QName _SDCB2BPINViewTableCustAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CustAccount");
    private final static QName _SDCB2BPINViewTableMediaType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MediaType");
    private final static QName _SDCB2BPINViewTablePINBlockedBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINBlockedBy");
    private final static QName _SDCB2BPINViewTableStartDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "StartDateTime");
    private final static QName _SDCB2BPINViewTableEndDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EndDateTime");
    private final static QName _SDCB2BPINViewTableReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReferenceId");
    private final static QName _SDCB2BPINViewTablePINStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINStatus");
    private final static QName _SDCB2BPINViewLineTableEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventGroupId");
    private final static QName _SDCB2BPINViewLineTableSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SalesId");
    private final static QName _SDCB2BPINViewLineTableInventTransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InventTransId");
    private final static QName _SDCB2BPINViewLineTableItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCB2BPINViewLineTableInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InvoiceId");
    private final static QName _SDCB2BPINViewLineTableOpenValidityRule_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "OpenValidityRule");
    private final static QName _SDCB2BPINViewLineTableTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionId");
    private final static QName _SDCB2BPINViewLineTableTransactionLineNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionLineNum");
    private final static QName _SDCB2BPINViewLineTableEventDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventDate");
    private final static QName _SDCB2BPINViewLineTableDefineTicketDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DefineTicketDate");
    private final static QName _SDCB2BPINViewLineTableLineNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineNum");
    private final static QName _SDCB2BPINViewLineTableRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RetailVariantId");
    private final static QName _SDCB2BPINViewLineTableEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventLineId");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _SDCB2BPINTableInquiryResponseB2BPinViewTableInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "b2bPinViewTableInquiry");
    private final static QName _GetB2BPINTableInquiryPinCode_QNAME = new QName("http://tempuri.org/", "pinCode");
    private final static QName _SDCB2BPINViewResponseB2BPinViewTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "b2bPinViewTable");
    private final static QName _GetB2BPINViewResponseGetB2BPINViewResult_QNAME = new QName("http://tempuri.org/", "GetB2BPINViewResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.b2bpinview
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetB2BPINTableInquiry }
     * 
     */
    public GetB2BPINTableInquiry createGetB2BPINTableInquiry() {
        return new GetB2BPINTableInquiry();
    }

    /**
     * Create an instance of {@link GetB2BPINTableInquiryResponse }
     * 
     */
    public GetB2BPINTableInquiryResponse createGetB2BPINTableInquiryResponse() {
        return new GetB2BPINTableInquiryResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINTableInquiryResponse }
     * 
     */
    public SDCB2BPINTableInquiryResponse createSDCB2BPINTableInquiryResponse() {
        return new SDCB2BPINTableInquiryResponse();
    }

    /**
     * Create an instance of {@link GetB2BPINView }
     * 
     */
    public GetB2BPINView createGetB2BPINView() {
        return new GetB2BPINView();
    }

    /**
     * Create an instance of {@link GetB2BPINViewResponse }
     * 
     */
    public GetB2BPINViewResponse createGetB2BPINViewResponse() {
        return new GetB2BPINViewResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewResponse }
     * 
     */
    public SDCB2BPINViewResponse createSDCB2BPINViewResponse() {
        return new SDCB2BPINViewResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewLineTable }
     * 
     */
    public SDCB2BPINViewLineTable createSDCB2BPINViewLineTable() {
        return new SDCB2BPINViewLineTable();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewTable }
     * 
     */
    public SDCB2BPINViewTable createSDCB2BPINViewTable() {
        return new SDCB2BPINViewTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCB2BPINViewLineTable }
     * 
     */
    public ArrayOfSDCB2BPINViewLineTable createArrayOfSDCB2BPINViewLineTable() {
        return new ArrayOfSDCB2BPINViewLineTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCB2BPINViewTable }
     * 
     */
    public ArrayOfSDCB2BPINViewTable createArrayOfSDCB2BPINViewTable() {
        return new ArrayOfSDCB2BPINViewTable();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINViewLineTable")
    public JAXBElement<SDCB2BPINViewLineTable> createSDCB2BPINViewLineTable(SDCB2BPINViewLineTable value) {
        return new JAXBElement<SDCB2BPINViewLineTable>(_SDCB2BPINViewLineTable_QNAME, SDCB2BPINViewLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINTableInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINTableInquiryResponse")
    public JAXBElement<SDCB2BPINTableInquiryResponse> createSDCB2BPINTableInquiryResponse(SDCB2BPINTableInquiryResponse value) {
        return new JAXBElement<SDCB2BPINTableInquiryResponse>(_SDCB2BPINTableInquiryResponse_QNAME, SDCB2BPINTableInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINViewTable")
    public JAXBElement<SDCB2BPINViewTable> createSDCB2BPINViewTable(SDCB2BPINViewTable value) {
        return new JAXBElement<SDCB2BPINViewTable>(_SDCB2BPINViewTable_QNAME, SDCB2BPINViewTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINViewResponse")
    public JAXBElement<SDCB2BPINViewResponse> createSDCB2BPINViewResponse(SDCB2BPINViewResponse value) {
        return new JAXBElement<SDCB2BPINViewResponse>(_SDCB2BPINViewResponse_QNAME, SDCB2BPINViewResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_B2BPINViewLineTable")
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createArrayOfSDCB2BPINViewLineTable(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_ArrayOfSDCB2BPINViewLineTable_QNAME, ArrayOfSDCB2BPINViewLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_B2BPINViewTable")
    public JAXBElement<ArrayOfSDCB2BPINViewTable> createArrayOfSDCB2BPINViewTable(ArrayOfSDCB2BPINViewTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewTable>(_ArrayOfSDCB2BPINViewTable_QNAME, ArrayOfSDCB2BPINViewTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custAccount", scope = GetB2BPINView.class)
    public JAXBElement<String> createGetB2BPINViewCustAccount(String value) {
        return new JAXBElement<String>(_GetB2BPINViewCustAccount_QNAME, String.class, GetB2BPINView.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINTableInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BPINTableInquiryResult", scope = GetB2BPINTableInquiryResponse.class)
    public JAXBElement<SDCB2BPINTableInquiryResponse> createGetB2BPINTableInquiryResponseGetB2BPINTableInquiryResult(SDCB2BPINTableInquiryResponse value) {
        return new JAXBElement<SDCB2BPINTableInquiryResponse>(_GetB2BPINTableInquiryResponseGetB2BPINTableInquiryResult_QNAME, SDCB2BPINTableInquiryResponse.class, GetB2BPINTableInquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINBlockedDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINBlockedDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINBlockedDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINCode", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINCode(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINCode_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReasonCode", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableReasonCode(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReasonCode_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINViewLineCollection", scope = SDCB2BPINViewTable.class)
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createSDCB2BPINViewTablePINViewLineCollection(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_SDCB2BPINViewTablePINViewLineCollection_QNAME, ArrayOfSDCB2BPINViewLineTable.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LastRedeemDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableLastRedeemDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableLastRedeemDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableDescription(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableDescription_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PackageName", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePackageName(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePackageName_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CustAccount", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableCustAccount(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableCustAccount_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaType", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableMediaType(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableMediaType_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINBlockedBy", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINBlockedBy(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINBlockedBy_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "StartDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableStartDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableStartDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EndDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableEndDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableEndDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReferenceId_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINStatus", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINStatus(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINStatus_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventGroupId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventGroupId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SalesId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableSalesId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableSalesId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InventTransId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableInventTransId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInventTransId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableItemId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableItemId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InvoiceId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableInvoiceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInvoiceId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "OpenValidityRule", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableOpenValidityRule(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableOpenValidityRule_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableTransactionId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionLineNum", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableTransactionLineNum(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableTransactionLineNum_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventDate", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventDate(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventDate_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DefineTicketDate", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableDefineTicketDate(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableDefineTicketDate_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaType", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableMediaType(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableMediaType_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineNum", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableLineNum(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableLineNum_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableRetailVariantId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "StartDateTime", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableStartDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableStartDateTime_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EndDateTime", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEndDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableEndDateTime_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventLineId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventLineId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReferenceId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "b2bPinViewTableInquiry", scope = SDCB2BPINTableInquiryResponse.class)
    public JAXBElement<SDCB2BPINViewTable> createSDCB2BPINTableInquiryResponseB2BPinViewTableInquiry(SDCB2BPINViewTable value) {
        return new JAXBElement<SDCB2BPINViewTable>(_SDCB2BPINTableInquiryResponseB2BPinViewTableInquiry_QNAME, SDCB2BPINViewTable.class, SDCB2BPINTableInquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pinCode", scope = GetB2BPINTableInquiry.class)
    public JAXBElement<String> createGetB2BPINTableInquiryPinCode(String value) {
        return new JAXBElement<String>(_GetB2BPINTableInquiryPinCode_QNAME, String.class, GetB2BPINTableInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "b2bPinViewTable", scope = SDCB2BPINViewResponse.class)
    public JAXBElement<ArrayOfSDCB2BPINViewTable> createSDCB2BPINViewResponseB2BPinViewTable(ArrayOfSDCB2BPINViewTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewTable>(_SDCB2BPINViewResponseB2BPinViewTable_QNAME, ArrayOfSDCB2BPINViewTable.class, SDCB2BPINViewResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BPINViewResult", scope = GetB2BPINViewResponse.class)
    public JAXBElement<SDCB2BPINViewResponse> createGetB2BPINViewResponseGetB2BPINViewResult(SDCB2BPINViewResponse value) {
        return new JAXBElement<SDCB2BPINViewResponse>(_GetB2BPINViewResponseGetB2BPINViewResult_QNAME, SDCB2BPINViewResponse.class, GetB2BPINViewResponse.class, value);
    }

}
