package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * 
 * SDC_TicketingExtension.Entity.TemplateXMLEntity
 * 
 * @author Justin
 *
 */
public class AxTemplateXMLEntityResponse {

	private String printerType;

	private String templateDescription;

	private String templateName;

	private String xmlDocument;

	public String getPrinterType() {
		return printerType;
	}

	public void setPrinterType(String printerType) {
		this.printerType = printerType;
	}

	public String getTemplateDescription() {
		return templateDescription;
	}

	public void setTemplateDescription(String templateDescription) {
		this.templateDescription = templateDescription;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getXmlDocument() {
		return xmlDocument;
	}

	public void setXmlDocument(String xmlDocument) {
		this.xmlDocument = xmlDocument;
	}

}
