package com.enovax.star.cms.commons.model.axstar;

import java.io.Serializable;

/**
 * Created by jensen on 24/6/16.
 */
public class AxStarExtensionProperty implements Serializable {

    private String key;
    private AxStarExtensionPropertyValue value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public AxStarExtensionPropertyValue getValue() {
        return value;
    }

    public void setValue(AxStarExtensionPropertyValue value) {
        this.value = value;
    }
}
