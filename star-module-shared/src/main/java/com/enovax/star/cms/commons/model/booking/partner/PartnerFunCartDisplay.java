package com.enovax.star.cms.commons.model.booking.partner;

import com.enovax.star.cms.commons.model.booking.FunCartDisplay;
import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;
import com.enovax.star.cms.commons.model.partner.ppslm.PartnerVM;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 6/29/16.
 */
public class PartnerFunCartDisplay extends FunCartDisplay {

    private Map<String, Map<String,FunCartDisplayItem>> cartTopupMap = new HashMap<>();
    private Map<String, Map<String,FunCartDisplayItem>> additionalTopups = new HashMap<>();


    public Map<String, Map<String, FunCartDisplayItem>> getCartTopupMap() {
        return cartTopupMap;
    }

    public void setCartTopupMap(Map<String, Map<String, FunCartDisplayItem>> cartTopupMap) {
        this.cartTopupMap = cartTopupMap;
    }

    public Map<String, Map<String, FunCartDisplayItem>> getAdditionalTopups() {
        return additionalTopups;
    }

    public void setAdditionalTopups(Map<String, Map<String, FunCartDisplayItem>> additionalTopups) {
        this.additionalTopups = additionalTopups;
    }
}
