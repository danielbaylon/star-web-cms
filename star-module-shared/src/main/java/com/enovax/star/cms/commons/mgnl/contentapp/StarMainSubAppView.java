package com.enovax.star.cms.commons.mgnl.contentapp;

import info.magnolia.ui.api.view.View;

/**
 * Created by jennylynsze on 8/4/16.
 */
public interface StarMainSubAppView extends View {
    void setDisplay(String display);
}
