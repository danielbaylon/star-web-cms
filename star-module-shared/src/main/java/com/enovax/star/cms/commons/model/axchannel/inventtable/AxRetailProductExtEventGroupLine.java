package com.enovax.star.cms.commons.model.axchannel.inventtable;

import java.util.List;

public class AxRetailProductExtEventGroupLine {

    private String dataAreaId;
    private List<AxRetailProductExtEventAllocation> eventAllocations;
    private String eventCapacityId;
    private int eventEndTime;
    private String eventGroupId;
    private String eventLineId;
    private String eventName;
    private int eventStartTime;
    private String recId;
    private String recVersion;
    private int eventTime;

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public List<AxRetailProductExtEventAllocation> getEventAllocations() {
        return eventAllocations;
    }

    public void setEventAllocations(List<AxRetailProductExtEventAllocation> eventAllocations) {
        this.eventAllocations = eventAllocations;
    }

    public String getEventCapacityId() {
        return eventCapacityId;
    }

    public void setEventCapacityId(String eventCapacityId) {
        this.eventCapacityId = eventCapacityId;
    }

    public int getEventEndTime() {
        return eventEndTime;
    }

    public void setEventEndTime(int eventEndTime) {
        this.eventEndTime = eventEndTime;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public int getEventStartTime() {
        return eventStartTime;
    }

    public void setEventStartTime(int eventStartTime) {
        this.eventStartTime = eventStartTime;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getRecVersion() {
        return recVersion;
    }

    public void setRecVersion(String recVersion) {
        this.recVersion = recVersion;
    }

    public int getEventTime() {
        return eventTime;
    }

    public void setEventTime(int eventTime) {
        this.eventTime = eventTime;
    }
}
