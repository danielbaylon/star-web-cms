package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;
import java.util.List;

public class AxStarRetailDiscountMultibuyQuantityTier {

    private Long recordId;
    private String offerId;
    private Integer minimumQuantity;
    private BigDecimal discountPriceOrPercent;

    private List<AxStarExtensionProperty> extensionProperties;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public Integer getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(Integer minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public BigDecimal getDiscountPriceOrPercent() {
        return discountPriceOrPercent;
    }

    public void setDiscountPriceOrPercent(BigDecimal discountPriceOrPercent) {
        this.discountPriceOrPercent = discountPriceOrPercent;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
