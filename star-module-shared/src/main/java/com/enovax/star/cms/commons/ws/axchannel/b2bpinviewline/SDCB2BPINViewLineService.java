
package com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SDC_B2BPINViewLineService", targetNamespace = "http://tempuri.org/", wsdlLocation = "http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_B2BPINViewLineService.svc?wsdl")
public class SDCB2BPINViewLineService
    extends Service
{

    private final static URL SDCB2BPINVIEWLINESERVICE_WSDL_LOCATION;
    private final static WebServiceException SDCB2BPINVIEWLINESERVICE_EXCEPTION;
    private final static QName SDCB2BPINVIEWLINESERVICE_QNAME = new QName("http://tempuri.org/", "SDC_B2BPINViewLineService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://ax2012r3-devtest-sentosadev2-d2-3c6ea49c3059cf73.cloudapp.net:50000/Services/SDC_B2BPINViewLineService.svc?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SDCB2BPINVIEWLINESERVICE_WSDL_LOCATION = url;
        SDCB2BPINVIEWLINESERVICE_EXCEPTION = e;
    }

    public SDCB2BPINViewLineService() {
        super(__getWsdlLocation(), SDCB2BPINVIEWLINESERVICE_QNAME);
    }

    public SDCB2BPINViewLineService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SDCB2BPINVIEWLINESERVICE_QNAME, features);
    }

    public SDCB2BPINViewLineService(URL wsdlLocation) {
        super(wsdlLocation, SDCB2BPINVIEWLINESERVICE_QNAME);
    }

    public SDCB2BPINViewLineService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SDCB2BPINVIEWLINESERVICE_QNAME, features);
    }

    public SDCB2BPINViewLineService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SDCB2BPINViewLineService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ISDCB2BPINViewLineService
     */
    @WebEndpoint(name = "BasicHttpBinding_ISDC_B2BPINViewLineService")
    public ISDCB2BPINViewLineService getBasicHttpBindingISDCB2BPINViewLineService() {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_ISDC_B2BPINViewLineService"), ISDCB2BPINViewLineService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ISDCB2BPINViewLineService
     */
    @WebEndpoint(name = "BasicHttpBinding_ISDC_B2BPINViewLineService")
    public ISDCB2BPINViewLineService getBasicHttpBindingISDCB2BPINViewLineService(WebServiceFeature... features) {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_ISDC_B2BPINViewLineService"), ISDCB2BPINViewLineService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SDCB2BPINVIEWLINESERVICE_EXCEPTION!= null) {
            throw SDCB2BPINVIEWLINESERVICE_EXCEPTION;
        }
        return SDCB2BPINVIEWLINESERVICE_WSDL_LOCATION;
    }

}
