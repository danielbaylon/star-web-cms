package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lavanya on 16/9/16.
 */
public interface PPSLMRevalidationTransactionRepository extends JpaRepository<PPSLMRevalidationTransaction, Integer> {
    PPSLMRevalidationTransaction findById(Integer id);
    PPSLMRevalidationTransaction findByReceiptNum(String receiptNum);
}
