package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGDepositTransaction;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 18/8/16.
 */
public class PPMFLGDepositTransactionSpecification extends BaseSpecification<PPMFLGDepositTransaction> {
}
