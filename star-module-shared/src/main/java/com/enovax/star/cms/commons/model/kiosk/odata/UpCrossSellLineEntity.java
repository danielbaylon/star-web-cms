package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * 
 * @author Justin
 * @since 6 OCT 16
 */
public class UpCrossSellLineEntity {

    private String uPSELLTYPE;
    
    private String uPCROSSSELLITEM;
    
    private String vARIANTID;
    
    private String iNVENTDIMID;
    
    private String oRIGITEMID;
    
    private String qTY;
    
    private String uPCROSSSELLTABLERECID;
    
    private String uNITID;

    public String getuPSELLTYPE() {
        return uPSELLTYPE;
    }

    public void setuPSELLTYPE(String uPSELLTYPE) {
        this.uPSELLTYPE = uPSELLTYPE;
    }

    public String getuPCROSSSELLITEM() {
        return uPCROSSSELLITEM;
    }

    public void setuPCROSSSELLITEM(String uPCROSSSELLITEM) {
        this.uPCROSSSELLITEM = uPCROSSSELLITEM;
    }

    public String getvARIANTID() {
        return vARIANTID;
    }

    public void setvARIANTID(String vARIANTID) {
        this.vARIANTID = vARIANTID;
    }

    public String getiNVENTDIMID() {
        return iNVENTDIMID;
    }

    public void setiNVENTDIMID(String iNVENTDIMID) {
        this.iNVENTDIMID = iNVENTDIMID;
    }

    public String getoRIGITEMID() {
        return oRIGITEMID;
    }

    public void setoRIGITEMID(String oRIGITEMID) {
        this.oRIGITEMID = oRIGITEMID;
    }

    public String getqTY() {
        return qTY;
    }

    public void setqTY(String qTY) {
        this.qTY = qTY;
    }

    public String getuPCROSSSELLTABLERECID() {
        return uPCROSSSELLTABLERECID;
    }

    public void setuPCROSSSELLTABLERECID(String uPCROSSSELLTABLERECID) {
        this.uPCROSSSELLTABLERECID = uPCROSSSELLTABLERECID;
    }

    public String getuNITID() {
        return uNITID;
    }

    public void setuNITID(String uNITID) {
        this.uNITID = uNITID;
    }

}
