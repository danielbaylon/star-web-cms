package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

public class AxStarRetailDiscountPriceGroup {

    //Unused properties

//    private List<String> extensionProperties;

    private String offerId;
    private Long recordId;
    private Long priceGroupId;
    private String groupId;

    private List<AxStarExtensionProperty> extensionProperties;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public Long getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(Long priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    /*
    [SAMPLE JSON]
    {
        "OfferId": "MFLG-00002",
        "RecordId": 5637151326,
        "PriceGroupId": 5637149076,
        "GroupId": "POS",
        "ExtensionProperties": [
          {
            "Key": "DATAAREAID",
            "Value": {
              "BooleanValue": null,
              "ByteValue": null,
              "DecimalValue": null,
              "DateTimeOffsetValue": null,
              "IntegerValue": null,
              "LongValue": null,
              "StringValue": "mflg"
            }
          }
        ]
      }
     */
}
