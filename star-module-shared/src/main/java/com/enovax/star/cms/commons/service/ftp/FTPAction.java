package com.enovax.star.cms.commons.service.ftp;

import com.enovax.star.cms.commons.exception.BizValidationException;
import org.apache.commons.net.ftp.FTPClient;

import java.io.FileNotFoundException;

public interface FTPAction {

    public void storeFile(FTPClient ftpClient,String localFilePath, String remoteFilePath, String trackingId) throws FileNotFoundException, BizValidationException;

    public void downloadFile(FTPClient ftpClient,String localFilePath, String remoteFilePath, String trackingId) throws FileNotFoundException, BizValidationException;

}
