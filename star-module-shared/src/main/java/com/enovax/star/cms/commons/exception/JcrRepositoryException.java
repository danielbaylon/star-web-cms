package com.enovax.star.cms.commons.exception;

/**
 * Created by jensen on 10/11/16.
 */
public class JcrRepositoryException extends Exception {

    public JcrRepositoryException() {
    }

    public JcrRepositoryException(String message) {
        super(message);
    }

    public JcrRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public JcrRepositoryException(Throwable cause) {
        super(cause);
    }

    public JcrRepositoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
