package com.enovax.star.cms.commons.model.axstar;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jensen on 24/6/16.
 */
public class AxStarExtensionPropertyValue implements Serializable{
    private Integer integerValue = null;
    private String stringValue = null;
    private Boolean booleanValue = null;
    private Byte byteValue = null;
    private BigDecimal decimalValue = null;
    private Date dateTimeOffsetValue = null;
    private Long longValue = null;

    public Integer getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(Integer integerValue) {
        this.integerValue = integerValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public Boolean getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(Boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public Byte getByteValue() {
        return byteValue;
    }

    public void setByteValue(Byte byteValue) {
        this.byteValue = byteValue;
    }

    public BigDecimal getDecimalValue() {
        return decimalValue;
    }

    public void setDecimalValue(BigDecimal decimalValue) {
        this.decimalValue = decimalValue;
    }

    public Date getDateTimeOffsetValue() {
        return dateTimeOffsetValue;
    }

    public void setDateTimeOffsetValue(Date dateTimeOffsetValue) {
        this.dateTimeOffsetValue = dateTimeOffsetValue;
    }

    public Long getLongValue() {
        return longValue;
    }

    public void setLongValue(Long longValue) {
        this.longValue = longValue;
    }
}
