package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class PkgTopupItemVM implements java.io.Serializable {
    private Integer itemId;
    private static final long serialVersionUID = 1L;
    private Integer transId;
    private String productCode;
    private String listingId;
    private String displayName;

    public PkgTopupItemVM(PPSLMInventoryTransactionItem item) {
        this.itemId = item.getId();
        this.productCode = item.getItemProductCode();
        this.listingId = item.getItemListingId();
        this.transId = item.getInventoryTrans().getId();
        this.displayName = item.getDisplayName();
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof PkgTopupItemVM))
            return false;
        PkgTopupItemVM otherMyClass = (PkgTopupItemVM) other;
        if (this.productCode.equals(otherMyClass.getProductCode()) &&
                this.listingId.equals(otherMyClass.getListingId())) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = (int) serialVersionUID + itemId;
        return result;
    }
}

