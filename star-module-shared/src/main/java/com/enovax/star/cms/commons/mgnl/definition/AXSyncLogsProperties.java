package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 7/21/16.
 */
public enum AXSyncLogsProperties {
    AxProductJson("axProductJson"),
    PromotionJson("axProductPromotionJson"),
    CrossSellJson("axProductCrossSellJson"),
    AffiliationJson("axProductAffiliationJson"),
    AxProductExtDataJson("axProductExtDataJson"),
    SyncDate("axProductSyncDate"),
    SyncLog("axProductSyncLog");

    private String propertyName;

    AXSyncLogsProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
