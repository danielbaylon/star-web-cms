package com.enovax.star.cms.commons.repository.b2cmflg;

import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransactionItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface B2CMFLGStoreTransactionItemRepository extends JpaRepository<B2CMFLGStoreTransactionItem, Integer> {
}
