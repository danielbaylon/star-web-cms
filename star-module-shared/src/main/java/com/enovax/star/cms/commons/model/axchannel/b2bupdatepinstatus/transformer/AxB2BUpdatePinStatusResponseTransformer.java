package com.enovax.star.cms.commons.model.axchannel.b2bupdatepinstatus.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.ws.axchannel.pin.ArrayOfResponseError;
import com.enovax.star.cms.commons.ws.axchannel.pin.ResponseError;
import com.enovax.star.cms.commons.ws.axchannel.pin.SDCB2BUpdatePINStatusResponse;
import com.enovax.star.cms.commons.ws.axchannel.pin.SDCB2BUpdatePINStatusTable;

import javax.xml.bind.JAXBElement;
import java.util.List;

/**
 * Created by jennylynsze on 10/7/16.
 */
public class AxB2BUpdatePinStatusResponseTransformer {

    public static ApiResult<String> fromWsB2BUpdatePINStatus(SDCB2BUpdatePINStatusResponse response) {
        if(response == null){
            return new ApiResult<>(false, "", "no return response", null);
        }
        ApiResult<String> result = new ApiResult<String>();
        result.setSuccess(false);
        JAXBElement<SDCB2BUpdatePINStatusTable> xmlUpdateTable = response.getTable();
        if(xmlUpdateTable != null && xmlUpdateTable.getValue() != null){
            SDCB2BUpdatePINStatusTable updateTable = xmlUpdateTable.getValue();
            result.setSuccess(updateTable.isReturnStatus() != null ? updateTable.isReturnStatus().booleanValue() : false);
            result.setData(updateTable.getReturnMessage() != null ? updateTable.getReturnMessage().getValue() : null);
        }
        return getCommonResponseMessage(result, response);
    }

    public static ApiResult getCommonResponseMessage(ApiResult<String>  axResponse, SDCB2BUpdatePINStatusResponse response) {
        JAXBElement<ArrayOfResponseError> errorArrayResponse = response.getErrors();

        if (errorArrayResponse != null && (!errorArrayResponse.isNil())) {
            ArrayOfResponseError errorArray = errorArrayResponse.getValue();
            if (errorArray != null) {
                List<ResponseError> responseErrorList = errorArray.getResponseError();
                if (responseErrorList != null && responseErrorList.size() > 0) {
                    StringBuilder sbErrCodes = new StringBuilder("");
                    StringBuilder sbErrMessages = new StringBuilder("");
                    for (ResponseError rr : responseErrorList) {
                        if(rr == null){
                            continue;
                        }
                        if(rr.getErrorCode() != null && rr.getErrorCode().getValue() != null && rr.getErrorCode().getValue().length() > 0){
                            if(sbErrCodes.length() > 0){
                                sbErrCodes.append(" | ");
                            }
                            sbErrCodes.append(rr.getErrorCode().getValue());
                        }
                        if(rr.getErrorMessage() != null && rr.getErrorMessage().getValue() != null && rr.getErrorMessage().getValue().length() > 0){
                            if(sbErrMessages.length() > 0){
                                sbErrMessages.append(" | ");
                            }
                            sbErrMessages.append(rr.getErrorMessage().getValue());
                        }
                    }
                    if (sbErrCodes.length() > 0 || sbErrMessages.length() > 0) {
                        axResponse.setErrorCode(sbErrCodes.toString());
                        axResponse.setMessage(sbErrMessages.toString());
                    }
                }
            }
        }
        return axResponse;
    }

}
