
package com.enovax.star.cms.commons.ws.axchannel.b2bpinviewline;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetB2BPINViewLineResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINViewLineResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getB2BPINViewLineResult"
})
@XmlRootElement(name = "GetB2BPINViewLineResponse", namespace = "http://tempuri.org/")
public class GetB2BPINViewLineResponse {

    @XmlElementRef(name = "GetB2BPINViewLineResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINViewLineResponse> getB2BPINViewLineResult;

    /**
     * Gets the value of the getB2BPINViewLineResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINViewLineResponse> getGetB2BPINViewLineResult() {
        return getB2BPINViewLineResult;
    }

    /**
     * Sets the value of the getB2BPINViewLineResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineResponse }{@code >}
     *     
     */
    public void setGetB2BPINViewLineResult(JAXBElement<SDCB2BPINViewLineResponse> value) {
        this.getB2BPINViewLineResult = value;
    }

}
