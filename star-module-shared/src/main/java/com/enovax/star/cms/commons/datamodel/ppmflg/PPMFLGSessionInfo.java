package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="PPMFLGSessionInfo")
public class PPMFLGSessionInfo implements Serializable {

    private Integer id;
    private String sid;
    private String request;
    private String referrer;
    private String userAgent;
    private String ip;
    private Date firstAccessDate;
    private String transactionId;
    private String rcmdViews;

    private List<PPMFLGSessionTrace> traces = new ArrayList<>(0);
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="sid")
    public String getSid() {
        return sid;
    }
    public void setSid(String sid) {
        this.sid = sid;
    }

    @Column(name="request")
    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }

    @Column(name="referrer")
    public String getReferrer() {
        return referrer;
    }
    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    @Column(name="userAgent")
    public String getUserAgent() {
        return userAgent;
    }
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Column(name="ip")
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }

    @Column(name="firstAccessDate")
    public Date getFirstAccessDate() {
        return firstAccessDate;
    }
    public void setFirstAccessDate(Date firstAccessDate) {
        this.firstAccessDate = firstAccessDate;
    }

    @Column(name="transactionId")
    public String getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    @OneToMany(fetch= FetchType.LAZY, orphanRemoval=true)
    @JoinColumn(name="sessionInfoId", updatable=false)
    public List<PPMFLGSessionTrace> getTraces() {
        return traces;
    }
    public void setTraces(List<PPMFLGSessionTrace> traces) {
        this.traces = traces;
    }
    
    @Column(name="rcmdViews")
    public String getRcmdViews() {
        return rcmdViews;
    }
    public void setRcmdViews(String rcmdViews) {
        this.rcmdViews = rcmdViews;
    }
}
