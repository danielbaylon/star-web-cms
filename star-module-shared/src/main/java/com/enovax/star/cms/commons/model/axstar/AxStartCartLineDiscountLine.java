package com.enovax.star.cms.commons.model.axstar;

import java.math.BigDecimal;

public class AxStartCartLineDiscountLine {

    //Unused
//    private String extensionProperties; // []

    private Long saleLineNumber;
    private String offerId;
    private String offerName;
    private BigDecimal amount;
    private BigDecimal effectiveAmount;
    private BigDecimal percentage;
    private BigDecimal dealPrice;
    private int discountLineTypeValue;
    private int manualDiscountTypeValue;
    private int customerDiscountTypeValue;
    private int periodicDiscountTypeValue;
    private String discountApplicationGroup;
    private int concurrencyModeValue;
    private boolean isCompoundable;
    private String discountCode;
    private boolean isDiscountCodeRequired;
    private BigDecimal thresholdAmountRequired;
    
    /*
    [Sample JSON]
    {
            "SaleLineNumber": 1,
            "OfferId": "SLM-000002",
            "OfferName": "Buy 5 get 20% off",
            "Amount": 0,
            "EffectiveAmount": 27,
            "Percentage": 20,
            "DealPrice": 0,
            "DiscountLineTypeValue": 2,
            "ManualDiscountTypeValue": 0,
            "CustomerDiscountTypeValue": 0,
            "PeriodicDiscountTypeValue": 0,
            "DiscountApplicationGroup": "1030200001",
            "ConcurrencyModeValue": 0,
            "IsCompoundable": false,
            "DiscountCode": "",
            "IsDiscountCodeRequired": false,
            "ThresholdAmountRequired": 0,
            "ExtensionProperties": []
          }
     */

    public Long getSaleLineNumber() {
        return saleLineNumber;
    }

    public void setSaleLineNumber(Long saleLineNumber) {
        this.saleLineNumber = saleLineNumber;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getEffectiveAmount() {
        return effectiveAmount;
    }

    public void setEffectiveAmount(BigDecimal effectiveAmount) {
        this.effectiveAmount = effectiveAmount;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getDealPrice() {
        return dealPrice;
    }

    public void setDealPrice(BigDecimal dealPrice) {
        this.dealPrice = dealPrice;
    }

    public int getDiscountLineTypeValue() {
        return discountLineTypeValue;
    }

    public void setDiscountLineTypeValue(int discountLineTypeValue) {
        this.discountLineTypeValue = discountLineTypeValue;
    }

    public int getManualDiscountTypeValue() {
        return manualDiscountTypeValue;
    }

    public void setManualDiscountTypeValue(int manualDiscountTypeValue) {
        this.manualDiscountTypeValue = manualDiscountTypeValue;
    }

    public int getCustomerDiscountTypeValue() {
        return customerDiscountTypeValue;
    }

    public void setCustomerDiscountTypeValue(int customerDiscountTypeValue) {
        this.customerDiscountTypeValue = customerDiscountTypeValue;
    }

    public int getPeriodicDiscountTypeValue() {
        return periodicDiscountTypeValue;
    }

    public void setPeriodicDiscountTypeValue(int periodicDiscountTypeValue) {
        this.periodicDiscountTypeValue = periodicDiscountTypeValue;
    }

    public String getDiscountApplicationGroup() {
        return discountApplicationGroup;
    }

    public void setDiscountApplicationGroup(String discountApplicationGroup) {
        this.discountApplicationGroup = discountApplicationGroup;
    }

    public int getConcurrencyModeValue() {
        return concurrencyModeValue;
    }

    public void setConcurrencyModeValue(int concurrencyModeValue) {
        this.concurrencyModeValue = concurrencyModeValue;
    }

    public boolean isCompoundable() {
        return isCompoundable;
    }

    public void setCompoundable(boolean compoundable) {
        isCompoundable = compoundable;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public boolean isDiscountCodeRequired() {
        return isDiscountCodeRequired;
    }

    public void setDiscountCodeRequired(boolean discountCodeRequired) {
        isDiscountCodeRequired = discountCodeRequired;
    }

    public BigDecimal getThresholdAmountRequired() {
        return thresholdAmountRequired;
    }

    public void setThresholdAmountRequired(BigDecimal thresholdAmountRequired) {
        this.thresholdAmountRequired = thresholdAmountRequired;
    }
}
