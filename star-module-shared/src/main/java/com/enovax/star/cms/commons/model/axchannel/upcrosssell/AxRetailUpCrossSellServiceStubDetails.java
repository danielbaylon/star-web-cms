package com.enovax.star.cms.commons.model.axchannel.upcrosssell;

import com.enovax.star.cms.commons.ws.axretail.crosssell.ISDCUpCrossSellService;
import com.enovax.star.cms.commons.ws.axretail.crosssell.SDCUpCrossSellService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

/**
 * Created by jensen on 24/6/16.
 */
public class AxRetailUpCrossSellServiceStubDetails {

    private Logger log = LoggerFactory.getLogger(getClass());

    private SDCUpCrossSellService ws;
    private ISDCUpCrossSellService stub;
    private String apiUrl;
    private boolean init = false;

    public AxRetailUpCrossSellServiceStubDetails(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public boolean initialise() {
        try {

            ws = new SDCUpCrossSellService(new URL(apiUrl));
            stub = ws.getBasicHttpBindingISDCUpCrossSellService();

            log.info("Initialised AX Retail Up-Cross-Sell Service Client for " + this.apiUrl);
            this.init = true;
        } catch (Exception e) {
            log.error("Error initialising AX Retail Up-Cross-Sell Service Client for " + this.apiUrl, e);
            this.init = false;
        }

        return this.init;
    }

    public SDCUpCrossSellService getWs() {
        return ws;
    }

    public ISDCUpCrossSellService getStub() {
        return stub;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public boolean isInit() {
        return init;
    }
}
