package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * SDC_TicketingExtension.DataModel.SendRedeemTicketCriteria
 *
 *
 * @author dbaylon
 *
 */
@JsonIgnoreProperties
public class AxSendRedeemTiketToAXCriteria {

    @JsonProperty("PinCode")
    private String pinCode;

    @JsonProperty("TransactionId")
    private String transactionId;

    @JsonProperty("SourceTable")
    private String sourceTable;

    @JsonProperty("ReferenceId")
    private String referenceId;

    @JsonProperty("UserId")
    private String userId;

    @JsonProperty("SessionId")
    private String sessionId;

    @JsonProperty("LastRedemptionLoc")
    private String lastRedemptionLoc = "Beach";

    @JsonProperty("LastRedemptionDate")
    private String lastRedemptionDate;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getLastRedemptionLoc() {
        return lastRedemptionLoc;
    }

    public void setLastRedemptionLoc(String lastRedemptionLoc) {
        this.lastRedemptionLoc = lastRedemptionLoc;
    }

    public String getLastRedemptionDate() {
        return lastRedemptionDate;
    }

    public void setLastRedemptionDate(String lastRedemptionDate) {
        this.lastRedemptionDate = lastRedemptionDate;
    }

}
