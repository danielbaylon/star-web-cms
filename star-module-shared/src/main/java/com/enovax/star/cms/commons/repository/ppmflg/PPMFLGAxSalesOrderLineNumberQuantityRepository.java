package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGAxSalesOrderLineNumberQuantity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by jennylynsze on 8/24/16.
 */
public interface PPMFLGAxSalesOrderLineNumberQuantityRepository extends JpaRepository<PPMFLGAxSalesOrderLineNumberQuantity, Integer> {
    List<PPMFLGAxSalesOrderLineNumberQuantity> findBySalesOrderId(Integer salesOrderId);
    List<PPMFLGAxSalesOrderLineNumberQuantity> findBySalesOrderIdAndLineNumber(Integer salesOrderId, Integer lineNumber);
}
