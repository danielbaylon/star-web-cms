package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMWingsOfTimeReservation;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 27/8/16.
 */
public class PPSLMWingsOfTimeReservationSpecification extends BaseSpecification<PPSLMWingsOfTimeReservation> {
}
