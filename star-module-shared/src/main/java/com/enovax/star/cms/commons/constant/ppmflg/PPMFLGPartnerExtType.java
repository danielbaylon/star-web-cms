package com.enovax.star.cms.commons.constant.ppmflg;

public enum PPMFLGPartnerExtType {

    WingsOfTimePurchaseEnabled("wotReservationEnabled");

    public final String code;

    private PPMFLGPartnerExtType(String code) {
        this.code = code;
    }
}
