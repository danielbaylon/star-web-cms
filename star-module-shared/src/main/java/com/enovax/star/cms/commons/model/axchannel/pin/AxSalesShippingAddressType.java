package com.enovax.star.cms.commons.model.axchannel.pin;

/**
 * Created by houtao on 23/8/16.
 */
public enum AxSalesShippingAddressType {

    NONE("None"),
    DELIVERY("Delivery"),
    PAYMENT("Payment");

    private String value;

    AxSalesShippingAddressType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AxSalesShippingAddressType fromValue(String v) {
        for (AxSalesShippingAddressType c: AxSalesShippingAddressType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
