package com.enovax.star.cms.commons.model.booking;

import javax.jcr.Node;
import java.util.List;

/**
 * Created by cornelius on 28/6/16.
 */
public class CrossSellCart {

    private List<CrossSellCartItem> crossSellCartItems;

    public List<CrossSellCartItem> getCrossSellCartItems() {
        return crossSellCartItems;
    }

    public void setCrossSellCartItems(List<CrossSellCartItem> crossSellCartItems) {
        this.crossSellCartItems = crossSellCartItems;
    }
}