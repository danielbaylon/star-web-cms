package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;

/**
 * Created by jennylynsze on 8/24/16.
 */
@Entity
@Table(name = "PPMFLGAxSalesOrderLineNumberQuantity")
public class PPMFLGAxSalesOrderLineNumberQuantity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "salesOrderId", nullable = false)
    private Integer salesOrderId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "salesOrderId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPMFLGAxSalesOrder salesOrder;

    @Column(name = "lineId")
    private String lineId;

    @Column(name = "lineNumber")
    private Integer lineNumber;

    @Column(name = "qty")
    private Integer qty;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(Integer salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public PPMFLGAxSalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(PPMFLGAxSalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
