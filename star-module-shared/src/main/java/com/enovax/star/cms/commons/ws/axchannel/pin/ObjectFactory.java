
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.pin package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SDCUpdateCartExtensionFieldsResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_UpdateCartExtensionFieldsResponse");
    private final static QName _TaxableItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxableItem");
    private final static QName _DiscountLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DiscountLine");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCB2BPINViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINViewLineTable");
    private final static QName _CommercePropertyBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommerceProperty");
    private final static QName _TaxViewLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxViewLine");
    private final static QName _SDCB2BPINTableInquiryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINTableInquiryResponse");
    private final static QName _ArrayOfSDCPINRedemptionLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_PINRedemptionLineTable");
    private final static QName _SDCCustPenaltyChargeTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CustPenaltyChargeTable");
    private final static QName _RetailAffiliationType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "RetailAffiliationType");
    private final static QName _ArrayOfstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _ArrayOfAttributeValueBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfAttributeValueBase");
    private final static QName _CartLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CartLine");
    private final static QName _ArrayOfDiscountLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfDiscountLine");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _CommercePropertyValueBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommercePropertyValue");
    private final static QName _AddressType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "AddressType");
    private final static QName _ArrayOfTaxLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfTaxLine");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _SDCPINRedemptionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_PINRedemptionTable");
    private final static QName _DateTimeOffset_QNAME = new QName("http://schemas.datacontract.org/2004/07/System", "DateTimeOffset");
    private final static QName _IncomeExpenseLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "IncomeExpenseLine");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _SDCB2BPINViewResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINViewResponse");
    private final static QName _ArrayOfSDCB2BPINViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_B2BPINViewLineTable");
    private final static QName _Cart_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Cart");
    private final static QName _SDCB2BPINCancelResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINCancelResponse");
    private final static QName _SDCB2BUpdatePINStatusResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BUpdatePINStatusResponse");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _TaxLineIndia_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxLineIndia");
    private final static QName _SDCGetBlockPINRedemptionResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetBlockPINRedemptionResponse");
    private final static QName _ArrayOfChargeLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfChargeLine");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _SDCB2BUpdatePINStatusTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BUpdatePINStatusTable");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _TaxLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxLine");
    private final static QName _AffiliationLoyaltyTier_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "AffiliationLoyaltyTier");
    private final static QName _SDCSaveWOTOrderResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_SaveWOTOrderResponse");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _ArrayOfSDCCustPenaltyChargeTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CustPenaltyChargeTable");
    private final static QName _AttributeValueBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "AttributeValueBase");
    private final static QName _SDCB2BPINUpdateCancelTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINUpdateCancelTable");
    private final static QName _SDCOnLineParameter_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_OnLineParameter");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _ArrayOfSDCOnLineParameter_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_OnLineParameter");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _ArrayOfTenderLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfTenderLine");
    private final static QName _TenderLineBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TenderLineBase");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfAffiliationLoyaltyTier_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfAffiliationLoyaltyTier");
    private final static QName _TransactionItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TransactionItem");
    private final static QName _ChargeLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ChargeLine");
    private final static QName _SDCB2BPINViewLineResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINViewLineResponse");
    private final static QName _SDCPINRedemptionLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_PINRedemptionLineTable");
    private final static QName _SDCCustPenaltyChargeResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_CustPenaltyChargeResponse");
    private final static QName _ArrayOfCommerceProperty_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfCommerceProperty");
    private final static QName _SDCB2BPINViewTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_B2BPINViewTable");
    private final static QName _Transaction_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Transaction");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _ReasonCodeLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ReasonCodeLine");
    private final static QName _ArrayOfCartLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfCartLine");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _CartType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CartType");
    private final static QName _ArrayOfIncomeExpenseLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfIncomeExpenseLine");
    private final static QName _SDCOnLineParametersResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_OnLineParametersResponse");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _ArrayOfReasonCodeLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfReasonCodeLine");
    private final static QName _Address_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Address");
    private final static QName _ArrayOfTransactionItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfTransactionItem");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _ArrayOfTaxViewLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ArrayOfTaxViewLine");
    private final static QName _TransactionItemType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TransactionItemType");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _SDCB2BPINUpdateResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_B2BPINUpdateResponse");
    private final static QName _ArrayOfSDCPINRedemptionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_PINRedemptionTable");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _ArrayOfSDCB2BPINViewTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_B2BPINViewTable");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _CommerceEntityBase_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommerceEntity");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _ImageInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ImageInfo");
    private final static QName _SalesOrder_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SalesOrder");
    private final static QName _TenderLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TenderLine");
    private final static QName _ShippingAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Address");
    private final static QName _B2BPINCancelPinCode_QNAME = new QName("http://tempuri.org/", "_pinCode");
    private final static QName _B2BPINCancelMarkUpCode_QNAME = new QName("http://tempuri.org/", "_markUpCode");
    private final static QName _GetB2BPINViewLinePinRefId_QNAME = new QName("http://tempuri.org/", "pinRefId");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _SDCB2BPINViewTablePINBlockedDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINBlockedDateTime");
    private final static QName _SDCB2BPINViewTablePINCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINCode");
    private final static QName _SDCB2BPINViewTableReasonCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReasonCode");
    private final static QName _SDCB2BPINViewTablePINViewLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINViewLineCollection");
    private final static QName _SDCB2BPINViewTableLastRedeemDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LastRedeemDateTime");
    private final static QName _SDCB2BPINViewTableDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Description");
    private final static QName _SDCB2BPINViewTablePackageName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PackageName");
    private final static QName _SDCB2BPINViewTableCustAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "CustAccount");
    private final static QName _SDCB2BPINViewTableMediaType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MediaType");
    private final static QName _SDCB2BPINViewTablePINBlockedBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINBlockedBy");
    private final static QName _SDCB2BPINViewTableStartDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "StartDateTime");
    private final static QName _SDCB2BPINViewTableEndDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EndDateTime");
    private final static QName _SDCB2BPINViewTableReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReferenceId");
    private final static QName _SDCB2BPINViewTablePINStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINStatus");
    private final static QName _GetPINRedemptionResponseGetPINRedemptionResult_QNAME = new QName("http://tempuri.org/", "GetPINRedemptionResult");
    private final static QName _B2BUpdatePINStatusProcessedBy_QNAME = new QName("http://tempuri.org/", "_processedBy");
    private final static QName _B2BUpdatePINStatusReasonCode_QNAME = new QName("http://tempuri.org/", "_reasonCode");
    private final static QName _GetB2BPINViewLineResponseGetB2BPINViewLineResult_QNAME = new QName("http://tempuri.org/", "GetB2BPINViewLineResult");
    private final static QName _B2BPINUpdateResponseB2BPINUpdateResult_QNAME = new QName("http://tempuri.org/", "B2BPINUpdateResult");
    private final static QName _SaveWOTOrderResponseSaveWOTOrderResult_QNAME = new QName("http://tempuri.org/", "SaveWOTOrderResult");
    private final static QName _GetCustPenaltyChargeResponseGetCustPenaltyChargeResult_QNAME = new QName("http://tempuri.org/", "GetCustPenaltyChargeResult");
    private final static QName _CommercePropertyValueLongValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "LongValue");
    private final static QName _CommercePropertyValueStringValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StringValue");
    private final static QName _CommercePropertyValueByteValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ByteValue");
    private final static QName _CommercePropertyValueBooleanValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "BooleanValue");
    private final static QName _CommercePropertyValueIntegerValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "IntegerValue");
    private final static QName _CommercePropertyValueDateTimeOffsetValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DateTimeOffsetValue");
    private final static QName _CommercePropertyValueDecimalValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DecimalValue");
    private final static QName _AttributeValueBaseName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Name");
    private final static QName _SalesOrderStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Status");
    private final static QName _SalesOrderConfirmationId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ConfirmationId");
    private final static QName _SalesOrderOrderPlacedDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OrderPlacedDate");
    private final static QName _SalesOrderRequestedDeliveryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RequestedDeliveryDate");
    private final static QName _SalesOrderId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Id");
    private final static QName _SalesOrderSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SalesId");
    private final static QName _SalesOrderItems_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Items");
    private final static QName _TransactionSubtotalWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SubtotalWithCurrency");
    private final static QName _TransactionDiscountCodes_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DiscountCodes");
    private final static QName _TransactionLoyaltyCardId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "LoyaltyCardId");
    private final static QName _TransactionTaxAmountWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TaxAmountWithCurrency");
    private final static QName _TransactionDiscountAmountWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DiscountAmountWithCurrency");
    private final static QName _TransactionChargeAmountWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ChargeAmountWithCurrency");
    private final static QName _TransactionShippingAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ShippingAddress");
    private final static QName _TransactionDeliveryModeDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DeliveryModeDescription");
    private final static QName _TransactionTotalAmountWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TotalAmountWithCurrency");
    private final static QName _TransactionDeliveryModeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DeliveryModeId");
    private final static QName _GetCustPenaltyChargeCustAccount_QNAME = new QName("http://tempuri.org/", "CustAccount");
    private final static QName _SDCCustPenaltyChargeTableMARKUPCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MARKUPCODE");
    private final static QName _SDCCustPenaltyChargeTableEVENTLINEID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EVENTLINEID");
    private final static QName _SDCCustPenaltyChargeTableDATAAREAID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DATAAREAID");
    private final static QName _SDCCustPenaltyChargeTablePENALTYID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PENALTYID");
    private final static QName _SDCCustPenaltyChargeTableACCOUNTNUM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ACCOUNTNUM");
    private final static QName _SDCCustPenaltyChargeTableDESCRIPTION_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DESCRIPTION");
    private final static QName _SDCCustPenaltyChargeTableEVENTGROUPID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EVENTGROUPID");
    private final static QName _SDCB2BPINCancelResponseTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "table");
    private final static QName _B2BPINCancelResponseB2BPINCancelResult_QNAME = new QName("http://tempuri.org/", "B2BPINCancelResult");
    private final static QName _ChargeLineTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TransactionId");
    private final static QName _ChargeLineDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Description");
    private final static QName _ChargeLineChargeCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ChargeCode");
    private final static QName _ChargeLineCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CurrencyCode");
    private final static QName _TenderLineBaseCustomerId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CustomerId");
    private final static QName _TenderLineBaseReasonCodeLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ReasonCodeLines");
    private final static QName _TenderLineBaseTenderTypeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TenderTypeId");
    private final static QName _TenderLineBaseCreditMemoId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CreditMemoId");
    private final static QName _TenderLineBaseLoyaltyCardId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "LoyaltyCardId");
    private final static QName _TenderLineBaseGiftCardId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "GiftCardId");
    private final static QName _TenderLineBaseSignatureData_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SignatureData");
    private final static QName _TenderLineBaseCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Currency");
    private final static QName _TenderLineBaseCardTypeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CardTypeId");
    private final static QName _TenderLineBaseTenderLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TenderLineId");
    private final static QName _TaxableItemSalesTaxGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SalesTaxGroupId");
    private final static QName _TaxableItemSalesOrderUnitOfMeasure_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SalesOrderUnitOfMeasure");
    private final static QName _TaxableItemItemTaxGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ItemTaxGroupId");
    private final static QName _TaxableItemItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ItemId");
    private final static QName _TaxableItemTaxLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxLines");
    private final static QName _SDCB2BPINViewLineTableEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventGroupId");
    private final static QName _SDCB2BPINViewLineTableSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SalesId");
    private final static QName _SDCB2BPINViewLineTableInventTransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InventTransId");
    private final static QName _SDCB2BPINViewLineTableItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCB2BPINViewLineTableInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "InvoiceId");
    private final static QName _SDCB2BPINViewLineTableOpenValidityRule_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "OpenValidityRule");
    private final static QName _SDCB2BPINViewLineTableTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionId");
    private final static QName _SDCB2BPINViewLineTableTransactionLineNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionLineNum");
    private final static QName _SDCB2BPINViewLineTableEventDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventDate");
    private final static QName _SDCB2BPINViewLineTableDefineTicketDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DefineTicketDate");
    private final static QName _SDCB2BPINViewLineTableLineNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineNum");
    private final static QName _SDCB2BPINViewLineTableRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RetailVariantId");
    private final static QName _SDCB2BPINViewLineTableEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventLineId");
    private final static QName _SDCPINRedemptionTableReasonDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ReasonDesc");
    private final static QName _SDCPINRedemptionTableSessionNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SessionNo");
    private final static QName _SDCPINRedemptionTablePINRedemptionLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "PINRedemptionLines");
    private final static QName _SDCPINRedemptionTableMediaTypeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "MediaTypeId");
    private final static QName _SDCPINRedemptionTableUserId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UserId");
    private final static QName _CommercePropertyValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Value");
    private final static QName _CommercePropertyKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Key");
    private final static QName _CartLineDeliveryMode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DeliveryMode");
    private final static QName _CartLineRequestedDeliveryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "RequestedDeliveryDate");
    private final static QName _CartLineQuantityOrdered_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "QuantityOrdered");
    private final static QName _CartLineDiscountLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DiscountLines");
    private final static QName _CartLineInventoryDimensionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "InventoryDimensionId");
    private final static QName _CartLineShippingAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ShippingAddress");
    private final static QName _CartLineBarcode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Barcode");
    private final static QName _CartLineReturnTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ReturnTransactionId");
    private final static QName _CartLineSerialNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SerialNumber");
    private final static QName _CartLineWarehouseId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "WarehouseId");
    private final static QName _CartLineTaxOverrideCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxOverrideCode");
    private final static QName _CartLineDeliveryModeChargeAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DeliveryModeChargeAmount");
    private final static QName _CartLineComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Comment");
    private final static QName _CartLineLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "LineId");
    private final static QName _CartLineUnitOfMeasureSymbol_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "UnitOfMeasureSymbol");
    private final static QName _CartLineStoreNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StoreNumber");
    private final static QName _CartLineQuantityInvoiced_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "QuantityInvoiced");
    private final static QName _CartLineReturnInventTransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ReturnInventTransId");
    private final static QName _CartLinePromotionLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "PromotionLines");
    private final static QName _CartLineChargeLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ChargeLines");
    private final static QName _CartLineInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "InvoiceId");
    private final static QName _CartLineOriginalPrice_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "OriginalPrice");
    private final static QName _SDCPINRedemptionLineTableOpenValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "OpenValidityId");
    private final static QName _SDCOnLineParametersResponseOnLineParameters_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "OnLineParameters");
    private final static QName _AddressEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Email");
    private final static QName _AddressZipCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ZipCode");
    private final static QName _AddressEmailContent_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EmailContent");
    private final static QName _AddressCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "City");
    private final static QName _AddressCounty_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "County");
    private final static QName _AddressName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Name");
    private final static QName _AddressDistrictName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DistrictName");
    private final static QName _AddressAttentionTo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "AttentionTo");
    private final static QName _AddressPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Phone");
    private final static QName _AddressState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "State");
    private final static QName _AddressCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Country");
    private final static QName _AddressStreet_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Street");
    private final static QName _AddressStreetNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "StreetNumber");
    private final static QName _AddressAddressFriendlyName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "AddressFriendlyName");
    private final static QName _SDCOnLineParameterOnLineSalesDirect_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OnLineSalesDirect");
    private final static QName _SDCOnLineParameterOnLineSalesReservation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OnLineSalesReservation");
    private final static QName _SDCB2BUpdatePINStatusTableReturnMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "returnMessage");
    private final static QName _B2BPINUpdateItemId_QNAME = new QName("http://tempuri.org/", "_itemId");
    private final static QName _B2BPINUpdateMarkupCode_QNAME = new QName("http://tempuri.org/", "_markupCode");
    private final static QName _B2BPINUpdateEventLineId_QNAME = new QName("http://tempuri.org/", "_eventLineId");
    private final static QName _B2BPINUpdateInventTransId_QNAME = new QName("http://tempuri.org/", "_inventTransId");
    private final static QName _DiscountLineDiscountApplicationGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DiscountApplicationGroup");
    private final static QName _DiscountLineDiscountCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DiscountCode");
    private final static QName _DiscountLineOfferName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "OfferName");
    private final static QName _DiscountLineOfferId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "OfferId");
    private final static QName _TaxLineIndiaTaxItemGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxItemGroup");
    private final static QName _TaxLineIndiaTaxComponent_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxComponent");
    private final static QName _TaxLineIndiaTaxCodesInFormula_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxCodesInFormula");
    private final static QName _TaxLineIndiaTaxFormula_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxFormula");
    private final static QName _SDCCustPenaltyChargeResponseCustPenaltyChargetTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "custPenaltyChargetTable");
    private final static QName _GetB2BPINViewCustAccount_QNAME = new QName("http://tempuri.org/", "custAccount");
    private final static QName _GetPINRedemptionPinCode_QNAME = new QName("http://tempuri.org/", "pinCode");
    private final static QName _ShippingAddressBuildingCompliment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "BuildingCompliment");
    private final static QName _ShippingAddressPhoneExt_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "PhoneExt");
    private final static QName _ShippingAddressPartyNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "PartyNumber");
    private final static QName _ShippingAddressDistrictName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DistrictName");
    private final static QName _ShippingAddressEmailLogisticsLocationId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "EmailLogisticsLocationId");
    private final static QName _ShippingAddressState_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "State");
    private final static QName _ShippingAddressTaxGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxGroup");
    private final static QName _ShippingAddressLogisticsLocationId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "LogisticsLocationId");
    private final static QName _ShippingAddressCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "City");
    private final static QName _ShippingAddressCountyName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CountyName");
    private final static QName _ShippingAddressPostbox_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Postbox");
    private final static QName _ShippingAddressZipCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ZipCode");
    private final static QName _ShippingAddressStreet_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Street");
    private final static QName _ShippingAddressStreetNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StreetNumber");
    private final static QName _ShippingAddressPhoneLogisticsLocationId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "PhoneLogisticsLocationId");
    private final static QName _ShippingAddressTwoLetterISORegionName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TwoLetterISORegionName");
    private final static QName _ShippingAddressPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Phone");
    private final static QName _ShippingAddressUrlLogisticsLocationId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "UrlLogisticsLocationId");
    private final static QName _ShippingAddressAttentionTo_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "AttentionTo");
    private final static QName _ShippingAddressEmailContent_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "EmailContent");
    private final static QName _ShippingAddressUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Url");
    private final static QName _ShippingAddressCounty_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "County");
    private final static QName _ShippingAddressEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Email");
    private final static QName _ShippingAddressFullAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "FullAddress");
    private final static QName _ShippingAddressStateName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StateName");
    private final static QName _ShippingAddressThreeLetterISORegionName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ThreeLetterISORegionName");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _ReasonCodeLineSourceCode2_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SourceCode2");
    private final static QName _ReasonCodeLineSourceCode3_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SourceCode3");
    private final static QName _ReasonCodeLineStatementCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StatementCode");
    private final static QName _ReasonCodeLineReasonCodeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ReasonCodeId");
    private final static QName _ReasonCodeLineInformation_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Information");
    private final static QName _ReasonCodeLineItemTender_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ItemTender");
    private final static QName _ReasonCodeLineSourceCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SourceCode");
    private final static QName _ReasonCodeLineSubReasonCodeId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SubReasonCodeId");
    private final static QName _ReasonCodeLineDisplayString_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DisplayString");
    private final static QName _ReasonCodeLineParentLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ParentLineId");
    private final static QName _SDCB2BPINViewResponseB2BPinViewTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "b2bPinViewTable");
    private final static QName _GetB2BPINTableInquiryResponseGetB2BPINTableInquiryResult_QNAME = new QName("http://tempuri.org/", "GetB2BPINTableInquiryResult");
    private final static QName _CommerceEntityExtensionProperties_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ExtensionProperties");
    private final static QName _SDCSaveWOTOrderResponseErrMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrMessage");
    private final static QName _SDCSaveWOTOrderResponseGeneratedPINId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "GeneratedPINId");
    private final static QName _SDCSaveWOTOrderResponseOrder_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Order");
    private final static QName _ImageInfoAltText_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "AltText");
    private final static QName _ImageInfoUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Url");
    private final static QName _TaxViewLineTaxId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxId");
    private final static QName _IncomeExpenseLineTerminal_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Terminal");
    private final static QName _IncomeExpenseLineIncomeExpenseAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "IncomeExpenseAccount");
    private final static QName _IncomeExpenseLineShift_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Shift");
    private final static QName _CartId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Id");
    private final static QName _CartOverriddenDepositAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "OverriddenDepositAmount");
    private final static QName _CartTaxViewLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxViewLines");
    private final static QName _CartInvoiceComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "InvoiceComment");
    private final static QName _CartAttributeValues_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "AttributeValues");
    private final static QName _CartTenderLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TenderLines");
    private final static QName _CartCancellationChargeAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CancellationChargeAmount");
    private final static QName _CartSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "SalesId");
    private final static QName _CartReceiptEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ReceiptEmail");
    private final static QName _CartQuotationExpiryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "QuotationExpiryDate");
    private final static QName _CartDiscountCodes_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DiscountCodes");
    private final static QName _CartStaffId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StaffId");
    private final static QName _CartAffiliationLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "AffiliationLines");
    private final static QName _CartBeginDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "BeginDateTime");
    private final static QName _CartTerminalId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TerminalId");
    private final static QName _CartEstimatedShippingAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "EstimatedShippingAmount");
    private final static QName _CartIncomeExpenseLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "IncomeExpenseLines");
    private final static QName _CartOrderNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "OrderNumber");
    private final static QName _CartCartLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CartLines");
    private final static QName _CartModifiedDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "ModifiedDateTime");
    private final static QName _CartBusinessDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "BusinessDate");
    private final static QName _GetOnLineParametersResponseGetOnLineParametersResult_QNAME = new QName("http://tempuri.org/", "GetOnLineParametersResult");
    private final static QName _SaveWOTOrderCartId_QNAME = new QName("http://tempuri.org/", "cartId");
    private final static QName _SaveWOTOrderCustomerAccountId_QNAME = new QName("http://tempuri.org/", "CustomerAccountId");
    private final static QName _SaveWOTOrderReceiptEmail_QNAME = new QName("http://tempuri.org/", "receiptEmail");
    private final static QName _SaveWOTOrderOnlineSalesPool_QNAME = new QName("http://tempuri.org/", "onlineSalesPool");
    private final static QName _TransactionItemComment_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Comment");
    private final static QName _TransactionItemLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "LineId");
    private final static QName _TransactionItemColor_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Color");
    private final static QName _TransactionItemPriceWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PriceWithCurrency");
    private final static QName _TransactionItemKitComponents_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "KitComponents");
    private final static QName _TransactionItemProductNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductNumber");
    private final static QName _TransactionItemNetAmountWithCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "NetAmountWithCurrency");
    private final static QName _TransactionItemProductUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductUrl");
    private final static QName _TransactionItemDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Description");
    private final static QName _TransactionItemConfiguration_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Configuration");
    private final static QName _TransactionItemProductName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductName");
    private final static QName _TransactionItemSize_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Size");
    private final static QName _TransactionItemItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ItemId");
    private final static QName _TransactionItemImage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Image");
    private final static QName _TransactionItemPromotionLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PromotionLines");
    private final static QName _TransactionItemOfferNames_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OfferNames");
    private final static QName _TransactionItemElectronicDeliveryEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ElectronicDeliveryEmail");
    private final static QName _TransactionItemImageData_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ImageData");
    private final static QName _TransactionItemProductDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductDetails");
    private final static QName _TransactionItemVariantInventoryDimensionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "VariantInventoryDimensionId");
    private final static QName _TransactionItemStyle_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Style");
    private final static QName _SDCGetBlockPINRedemptionResponsePinRedemptionTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "pinRedemptionTable");
    private final static QName _SDCB2BPINTableInquiryResponseB2BPinViewTableInquiry_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "b2bPinViewTableInquiry");
    private final static QName _GetB2BPINViewResponseGetB2BPINViewResult_QNAME = new QName("http://tempuri.org/", "GetB2BPINViewResult");
    private final static QName _B2BUpdatePINStatusResponseB2BUpdatePINStatusResult_QNAME = new QName("http://tempuri.org/", "B2BUpdatePINStatusResult");
    private final static QName _TenderLineTenderDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TenderDate");
    private final static QName _TenderLineMaskedCardNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "MaskedCardNumber");
    private final static QName _TenderLineAuthorization_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "Authorization");
    private final static QName _TenderLineCardToken_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CardToken");
    private final static QName _UpdateCartExtensionFieldsResponseUpdateCartExtensionFieldsResult_QNAME = new QName("http://tempuri.org/", "UpdateCartExtensionFieldsResult");
    private final static QName _SDCB2BPINViewLineResponseB2BPinViewLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "b2bPinViewLineTable");
    private final static QName _SDCUpdateCartExtensionFieldsResponseErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "errorMessage");
    private final static QName _TaxLineDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "DataAreaId");
    private final static QName _TaxLineStoreId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "StoreId");
    private final static QName _TaxLineTaxCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "TaxCode");
    private final static QName _UpdateCartExtensionFieldsChannelRefId_QNAME = new QName("http://tempuri.org/", "channelRefId");
    private final static QName _UpdateCartExtensionFieldsCart_QNAME = new QName("http://tempuri.org/", "cart");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.pin
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link B2BPINUpdate }
     * 
     */
    public B2BPINUpdate createB2BPINUpdate() {
        return new B2BPINUpdate();
    }

    /**
     * Create an instance of {@link B2BUpdatePINStatusResponse }
     * 
     */
    public B2BUpdatePINStatusResponse createB2BUpdatePINStatusResponse() {
        return new B2BUpdatePINStatusResponse();
    }

    /**
     * Create an instance of {@link SDCB2BUpdatePINStatusResponse }
     * 
     */
    public SDCB2BUpdatePINStatusResponse createSDCB2BUpdatePINStatusResponse() {
        return new SDCB2BUpdatePINStatusResponse();
    }

    /**
     * Create an instance of {@link GetB2BPINViewLineResponse }
     * 
     */
    public GetB2BPINViewLineResponse createGetB2BPINViewLineResponse() {
        return new GetB2BPINViewLineResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewLineResponse }
     * 
     */
    public SDCB2BPINViewLineResponse createSDCB2BPINViewLineResponse() {
        return new SDCB2BPINViewLineResponse();
    }

    /**
     * Create an instance of {@link UpdateCartExtensionFields }
     * 
     */
    public UpdateCartExtensionFields createUpdateCartExtensionFields() {
        return new UpdateCartExtensionFields();
    }

    /**
     * Create an instance of {@link Cart }
     * 
     */
    public Cart createCart() {
        return new Cart();
    }

    /**
     * Create an instance of {@link GetB2BPINTableInquiryResponse }
     * 
     */
    public GetB2BPINTableInquiryResponse createGetB2BPINTableInquiryResponse() {
        return new GetB2BPINTableInquiryResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINTableInquiryResponse }
     * 
     */
    public SDCB2BPINTableInquiryResponse createSDCB2BPINTableInquiryResponse() {
        return new SDCB2BPINTableInquiryResponse();
    }

    /**
     * Create an instance of {@link SaveWOTOrderResponse }
     * 
     */
    public SaveWOTOrderResponse createSaveWOTOrderResponse() {
        return new SaveWOTOrderResponse();
    }

    /**
     * Create an instance of {@link SDCSaveWOTOrderResponse }
     * 
     */
    public SDCSaveWOTOrderResponse createSDCSaveWOTOrderResponse() {
        return new SDCSaveWOTOrderResponse();
    }

    /**
     * Create an instance of {@link B2BPINUpdateResponse }
     * 
     */
    public B2BPINUpdateResponse createB2BPINUpdateResponse() {
        return new B2BPINUpdateResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINUpdateResponse }
     * 
     */
    public SDCB2BPINUpdateResponse createSDCB2BPINUpdateResponse() {
        return new SDCB2BPINUpdateResponse();
    }

    /**
     * Create an instance of {@link GetB2BPINViewResponse }
     * 
     */
    public GetB2BPINViewResponse createGetB2BPINViewResponse() {
        return new GetB2BPINViewResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewResponse }
     * 
     */
    public SDCB2BPINViewResponse createSDCB2BPINViewResponse() {
        return new SDCB2BPINViewResponse();
    }

    /**
     * Create an instance of {@link GetCustPenaltyCharge }
     * 
     */
    public GetCustPenaltyCharge createGetCustPenaltyCharge() {
        return new GetCustPenaltyCharge();
    }

    /**
     * Create an instance of {@link GetOnLineParameters }
     * 
     */
    public GetOnLineParameters createGetOnLineParameters() {
        return new GetOnLineParameters();
    }

    /**
     * Create an instance of {@link GetPINRedemptionResponse }
     * 
     */
    public GetPINRedemptionResponse createGetPINRedemptionResponse() {
        return new GetPINRedemptionResponse();
    }

    /**
     * Create an instance of {@link SDCGetBlockPINRedemptionResponse }
     * 
     */
    public SDCGetBlockPINRedemptionResponse createSDCGetBlockPINRedemptionResponse() {
        return new SDCGetBlockPINRedemptionResponse();
    }

    /**
     * Create an instance of {@link GetB2BPINViewLine }
     * 
     */
    public GetB2BPINViewLine createGetB2BPINViewLine() {
        return new GetB2BPINViewLine();
    }

    /**
     * Create an instance of {@link GetPINRedemption }
     * 
     */
    public GetPINRedemption createGetPINRedemption() {
        return new GetPINRedemption();
    }

    /**
     * Create an instance of {@link UpdateCartExtensionFieldsResponse }
     * 
     */
    public UpdateCartExtensionFieldsResponse createUpdateCartExtensionFieldsResponse() {
        return new UpdateCartExtensionFieldsResponse();
    }

    /**
     * Create an instance of {@link SDCUpdateCartExtensionFieldsResponse }
     * 
     */
    public SDCUpdateCartExtensionFieldsResponse createSDCUpdateCartExtensionFieldsResponse() {
        return new SDCUpdateCartExtensionFieldsResponse();
    }

    /**
     * Create an instance of {@link GetOnLineParametersResponse }
     * 
     */
    public GetOnLineParametersResponse createGetOnLineParametersResponse() {
        return new GetOnLineParametersResponse();
    }

    /**
     * Create an instance of {@link SDCOnLineParametersResponse }
     * 
     */
    public SDCOnLineParametersResponse createSDCOnLineParametersResponse() {
        return new SDCOnLineParametersResponse();
    }

    /**
     * Create an instance of {@link GetB2BPINTableInquiry }
     * 
     */
    public GetB2BPINTableInquiry createGetB2BPINTableInquiry() {
        return new GetB2BPINTableInquiry();
    }

    /**
     * Create an instance of {@link B2BPINCancelResponse }
     * 
     */
    public B2BPINCancelResponse createB2BPINCancelResponse() {
        return new B2BPINCancelResponse();
    }

    /**
     * Create an instance of {@link SDCB2BPINCancelResponse }
     * 
     */
    public SDCB2BPINCancelResponse createSDCB2BPINCancelResponse() {
        return new SDCB2BPINCancelResponse();
    }

    /**
     * Create an instance of {@link B2BPINCancel }
     * 
     */
    public B2BPINCancel createB2BPINCancel() {
        return new B2BPINCancel();
    }

    /**
     * Create an instance of {@link GetCustPenaltyChargeResponse }
     * 
     */
    public GetCustPenaltyChargeResponse createGetCustPenaltyChargeResponse() {
        return new GetCustPenaltyChargeResponse();
    }

    /**
     * Create an instance of {@link SDCCustPenaltyChargeResponse }
     * 
     */
    public SDCCustPenaltyChargeResponse createSDCCustPenaltyChargeResponse() {
        return new SDCCustPenaltyChargeResponse();
    }

    /**
     * Create an instance of {@link SaveWOTOrder }
     * 
     */
    public SaveWOTOrder createSaveWOTOrder() {
        return new SaveWOTOrder();
    }

    /**
     * Create an instance of {@link B2BUpdatePINStatus }
     * 
     */
    public B2BUpdatePINStatus createB2BUpdatePINStatus() {
        return new B2BUpdatePINStatus();
    }

    /**
     * Create an instance of {@link GetB2BPINView }
     * 
     */
    public GetB2BPINView createGetB2BPINView() {
        return new GetB2BPINView();
    }

    /**
     * Create an instance of {@link SDCPINRedemptionLineTable }
     * 
     */
    public SDCPINRedemptionLineTable createSDCPINRedemptionLineTable() {
        return new SDCPINRedemptionLineTable();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewLineTable }
     * 
     */
    public SDCB2BPINViewLineTable createSDCB2BPINViewLineTable() {
        return new SDCB2BPINViewLineTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCustPenaltyChargeTable }
     * 
     */
    public ArrayOfSDCCustPenaltyChargeTable createArrayOfSDCCustPenaltyChargeTable() {
        return new ArrayOfSDCCustPenaltyChargeTable();
    }

    /**
     * Create an instance of {@link SDCPINRedemptionTable }
     * 
     */
    public SDCPINRedemptionTable createSDCPINRedemptionTable() {
        return new SDCPINRedemptionTable();
    }

    /**
     * Create an instance of {@link SDCB2BPINUpdateCancelTable }
     * 
     */
    public SDCB2BPINUpdateCancelTable createSDCB2BPINUpdateCancelTable() {
        return new SDCB2BPINUpdateCancelTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPINRedemptionLineTable }
     * 
     */
    public ArrayOfSDCPINRedemptionLineTable createArrayOfSDCPINRedemptionLineTable() {
        return new ArrayOfSDCPINRedemptionLineTable();
    }

    /**
     * Create an instance of {@link SDCB2BUpdatePINStatusTable }
     * 
     */
    public SDCB2BUpdatePINStatusTable createSDCB2BUpdatePINStatusTable() {
        return new SDCB2BUpdatePINStatusTable();
    }

    /**
     * Create an instance of {@link SDCCustPenaltyChargeTable }
     * 
     */
    public SDCCustPenaltyChargeTable createSDCCustPenaltyChargeTable() {
        return new SDCCustPenaltyChargeTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPINRedemptionTable }
     * 
     */
    public ArrayOfSDCPINRedemptionTable createArrayOfSDCPINRedemptionTable() {
        return new ArrayOfSDCPINRedemptionTable();
    }

    /**
     * Create an instance of {@link SDCB2BPINViewTable }
     * 
     */
    public SDCB2BPINViewTable createSDCB2BPINViewTable() {
        return new SDCB2BPINViewTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCB2BPINViewLineTable }
     * 
     */
    public ArrayOfSDCB2BPINViewLineTable createArrayOfSDCB2BPINViewLineTable() {
        return new ArrayOfSDCB2BPINViewLineTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCB2BPINViewTable }
     * 
     */
    public ArrayOfSDCB2BPINViewTable createArrayOfSDCB2BPINViewTable() {
        return new ArrayOfSDCB2BPINViewTable();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link SDCOnLineParameter }
     * 
     */
    public SDCOnLineParameter createSDCOnLineParameter() {
        return new SDCOnLineParameter();
    }

    /**
     * Create an instance of {@link ArrayOfSDCOnLineParameter }
     * 
     */
    public ArrayOfSDCOnLineParameter createArrayOfSDCOnLineParameter() {
        return new ArrayOfSDCOnLineParameter();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link ArrayOfTransactionItem }
     * 
     */
    public ArrayOfTransactionItem createArrayOfTransactionItem() {
        return new ArrayOfTransactionItem();
    }

    /**
     * Create an instance of {@link TransactionItem }
     * 
     */
    public TransactionItem createTransactionItem() {
        return new TransactionItem();
    }

    /**
     * Create an instance of {@link ImageInfo }
     * 
     */
    public ImageInfo createImageInfo() {
        return new ImageInfo();
    }

    /**
     * Create an instance of {@link SalesOrder }
     * 
     */
    public SalesOrder createSalesOrder() {
        return new SalesOrder();
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link CommerceEntity }
     * 
     */
    public CommerceEntity createCommerceEntity() {
        return new CommerceEntity();
    }

    /**
     * Create an instance of {@link AttributeValueBase }
     * 
     */
    public AttributeValueBase createAttributeValueBase() {
        return new AttributeValueBase();
    }

    /**
     * Create an instance of {@link ShippingAddress }
     * 
     */
    public ShippingAddress createShippingAddress() {
        return new ShippingAddress();
    }

    /**
     * Create an instance of {@link TenderLine }
     * 
     */
    public TenderLine createTenderLine() {
        return new TenderLine();
    }

    /**
     * Create an instance of {@link AffiliationLoyaltyTier }
     * 
     */
    public AffiliationLoyaltyTier createAffiliationLoyaltyTier() {
        return new AffiliationLoyaltyTier();
    }

    /**
     * Create an instance of {@link TaxLine }
     * 
     */
    public TaxLine createTaxLine() {
        return new TaxLine();
    }

    /**
     * Create an instance of {@link ArrayOfChargeLine }
     * 
     */
    public ArrayOfChargeLine createArrayOfChargeLine() {
        return new ArrayOfChargeLine();
    }

    /**
     * Create an instance of {@link TaxLineIndia }
     * 
     */
    public TaxLineIndia createTaxLineIndia() {
        return new TaxLineIndia();
    }

    /**
     * Create an instance of {@link ArrayOfTaxViewLine }
     * 
     */
    public ArrayOfTaxViewLine createArrayOfTaxViewLine() {
        return new ArrayOfTaxViewLine();
    }

    /**
     * Create an instance of {@link ArrayOfIncomeExpenseLine }
     * 
     */
    public ArrayOfIncomeExpenseLine createArrayOfIncomeExpenseLine() {
        return new ArrayOfIncomeExpenseLine();
    }

    /**
     * Create an instance of {@link IncomeExpenseLine }
     * 
     */
    public IncomeExpenseLine createIncomeExpenseLine() {
        return new IncomeExpenseLine();
    }

    /**
     * Create an instance of {@link ArrayOfReasonCodeLine }
     * 
     */
    public ArrayOfReasonCodeLine createArrayOfReasonCodeLine() {
        return new ArrayOfReasonCodeLine();
    }

    /**
     * Create an instance of {@link ArrayOfDiscountLine }
     * 
     */
    public ArrayOfDiscountLine createArrayOfDiscountLine() {
        return new ArrayOfDiscountLine();
    }

    /**
     * Create an instance of {@link ArrayOfTaxLine }
     * 
     */
    public ArrayOfTaxLine createArrayOfTaxLine() {
        return new ArrayOfTaxLine();
    }

    /**
     * Create an instance of {@link CommercePropertyValue }
     * 
     */
    public CommercePropertyValue createCommercePropertyValue() {
        return new CommercePropertyValue();
    }

    /**
     * Create an instance of {@link ReasonCodeLine }
     * 
     */
    public ReasonCodeLine createReasonCodeLine() {
        return new ReasonCodeLine();
    }

    /**
     * Create an instance of {@link ArrayOfCartLine }
     * 
     */
    public ArrayOfCartLine createArrayOfCartLine() {
        return new ArrayOfCartLine();
    }

    /**
     * Create an instance of {@link CartLine }
     * 
     */
    public CartLine createCartLine() {
        return new CartLine();
    }

    /**
     * Create an instance of {@link ArrayOfAttributeValueBase }
     * 
     */
    public ArrayOfAttributeValueBase createArrayOfAttributeValueBase() {
        return new ArrayOfAttributeValueBase();
    }

    /**
     * Create an instance of {@link TaxViewLine }
     * 
     */
    public TaxViewLine createTaxViewLine() {
        return new TaxViewLine();
    }

    /**
     * Create an instance of {@link ArrayOfCommerceProperty }
     * 
     */
    public ArrayOfCommerceProperty createArrayOfCommerceProperty() {
        return new ArrayOfCommerceProperty();
    }

    /**
     * Create an instance of {@link CommerceProperty }
     * 
     */
    public CommerceProperty createCommerceProperty() {
        return new CommerceProperty();
    }

    /**
     * Create an instance of {@link TenderLineBase }
     * 
     */
    public TenderLineBase createTenderLineBase() {
        return new TenderLineBase();
    }

    /**
     * Create an instance of {@link ArrayOfTenderLine }
     * 
     */
    public ArrayOfTenderLine createArrayOfTenderLine() {
        return new ArrayOfTenderLine();
    }

    /**
     * Create an instance of {@link ChargeLine }
     * 
     */
    public ChargeLine createChargeLine() {
        return new ChargeLine();
    }

    /**
     * Create an instance of {@link TaxableItem }
     * 
     */
    public TaxableItem createTaxableItem() {
        return new TaxableItem();
    }

    /**
     * Create an instance of {@link DiscountLine }
     * 
     */
    public DiscountLine createDiscountLine() {
        return new DiscountLine();
    }

    /**
     * Create an instance of {@link ArrayOfAffiliationLoyaltyTier }
     * 
     */
    public ArrayOfAffiliationLoyaltyTier createArrayOfAffiliationLoyaltyTier() {
        return new ArrayOfAffiliationLoyaltyTier();
    }

    /**
     * Create an instance of {@link DateTimeOffset }
     * 
     */
    public DateTimeOffset createDateTimeOffset() {
        return new DateTimeOffset();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpdateCartExtensionFieldsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_UpdateCartExtensionFieldsResponse")
    public JAXBElement<SDCUpdateCartExtensionFieldsResponse> createSDCUpdateCartExtensionFieldsResponse(SDCUpdateCartExtensionFieldsResponse value) {
        return new JAXBElement<SDCUpdateCartExtensionFieldsResponse>(_SDCUpdateCartExtensionFieldsResponse_QNAME, SDCUpdateCartExtensionFieldsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxableItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxableItem")
    public JAXBElement<TaxableItem> createTaxableItem(TaxableItem value) {
        return new JAXBElement<TaxableItem>(_TaxableItem_QNAME, TaxableItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DiscountLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DiscountLine")
    public JAXBElement<DiscountLine> createDiscountLine(DiscountLine value) {
        return new JAXBElement<DiscountLine>(_DiscountLine_QNAME, DiscountLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINViewLineTable")
    public JAXBElement<SDCB2BPINViewLineTable> createSDCB2BPINViewLineTable(SDCB2BPINViewLineTable value) {
        return new JAXBElement<SDCB2BPINViewLineTable>(_SDCB2BPINViewLineTable_QNAME, SDCB2BPINViewLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommerceProperty")
    public JAXBElement<CommerceProperty> createCommercePropertyBase(CommerceProperty value) {
        return new JAXBElement<CommerceProperty>(_CommercePropertyBase_QNAME, CommerceProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxViewLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxViewLine")
    public JAXBElement<TaxViewLine> createTaxViewLine(TaxViewLine value) {
        return new JAXBElement<TaxViewLine>(_TaxViewLine_QNAME, TaxViewLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINTableInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINTableInquiryResponse")
    public JAXBElement<SDCB2BPINTableInquiryResponse> createSDCB2BPINTableInquiryResponse(SDCB2BPINTableInquiryResponse value) {
        return new JAXBElement<SDCB2BPINTableInquiryResponse>(_SDCB2BPINTableInquiryResponse_QNAME, SDCB2BPINTableInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPINRedemptionLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_PINRedemptionLineTable")
    public JAXBElement<ArrayOfSDCPINRedemptionLineTable> createArrayOfSDCPINRedemptionLineTable(ArrayOfSDCPINRedemptionLineTable value) {
        return new JAXBElement<ArrayOfSDCPINRedemptionLineTable>(_ArrayOfSDCPINRedemptionLineTable_QNAME, ArrayOfSDCPINRedemptionLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustPenaltyChargeTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CustPenaltyChargeTable")
    public JAXBElement<SDCCustPenaltyChargeTable> createSDCCustPenaltyChargeTable(SDCCustPenaltyChargeTable value) {
        return new JAXBElement<SDCCustPenaltyChargeTable>(_SDCCustPenaltyChargeTable_QNAME, SDCCustPenaltyChargeTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetailAffiliationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "RetailAffiliationType")
    public JAXBElement<RetailAffiliationType> createRetailAffiliationType(RetailAffiliationType value) {
        return new JAXBElement<RetailAffiliationType>(_RetailAffiliationType_QNAME, RetailAffiliationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAttributeValueBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfAttributeValueBase")
    public JAXBElement<ArrayOfAttributeValueBase> createArrayOfAttributeValueBase(ArrayOfAttributeValueBase value) {
        return new JAXBElement<ArrayOfAttributeValueBase>(_ArrayOfAttributeValueBase_QNAME, ArrayOfAttributeValueBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CartLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CartLine")
    public JAXBElement<CartLine> createCartLine(CartLine value) {
        return new JAXBElement<CartLine>(_CartLine_QNAME, CartLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDiscountLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfDiscountLine")
    public JAXBElement<ArrayOfDiscountLine> createArrayOfDiscountLine(ArrayOfDiscountLine value) {
        return new JAXBElement<ArrayOfDiscountLine>(_ArrayOfDiscountLine_QNAME, ArrayOfDiscountLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommercePropertyValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommercePropertyValue")
    public JAXBElement<CommercePropertyValue> createCommercePropertyValueBase(CommercePropertyValue value) {
        return new JAXBElement<CommercePropertyValue>(_CommercePropertyValueBase_QNAME, CommercePropertyValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AddressType")
    public JAXBElement<AddressType> createAddressType(AddressType value) {
        return new JAXBElement<AddressType>(_AddressType_QNAME, AddressType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTaxLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfTaxLine")
    public JAXBElement<ArrayOfTaxLine> createArrayOfTaxLine(ArrayOfTaxLine value) {
        return new JAXBElement<ArrayOfTaxLine>(_ArrayOfTaxLine_QNAME, ArrayOfTaxLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPINRedemptionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_PINRedemptionTable")
    public JAXBElement<SDCPINRedemptionTable> createSDCPINRedemptionTable(SDCPINRedemptionTable value) {
        return new JAXBElement<SDCPINRedemptionTable>(_SDCPINRedemptionTable_QNAME, SDCPINRedemptionTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/System", name = "DateTimeOffset")
    public JAXBElement<DateTimeOffset> createDateTimeOffset(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_DateTimeOffset_QNAME, DateTimeOffset.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncomeExpenseLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "IncomeExpenseLine")
    public JAXBElement<IncomeExpenseLine> createIncomeExpenseLine(IncomeExpenseLine value) {
        return new JAXBElement<IncomeExpenseLine>(_IncomeExpenseLine_QNAME, IncomeExpenseLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINViewResponse")
    public JAXBElement<SDCB2BPINViewResponse> createSDCB2BPINViewResponse(SDCB2BPINViewResponse value) {
        return new JAXBElement<SDCB2BPINViewResponse>(_SDCB2BPINViewResponse_QNAME, SDCB2BPINViewResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_B2BPINViewLineTable")
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createArrayOfSDCB2BPINViewLineTable(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_ArrayOfSDCB2BPINViewLineTable_QNAME, ArrayOfSDCB2BPINViewLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Cart")
    public JAXBElement<Cart> createCart(Cart value) {
        return new JAXBElement<Cart>(_Cart_QNAME, Cart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINCancelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINCancelResponse")
    public JAXBElement<SDCB2BPINCancelResponse> createSDCB2BPINCancelResponse(SDCB2BPINCancelResponse value) {
        return new JAXBElement<SDCB2BPINCancelResponse>(_SDCB2BPINCancelResponse_QNAME, SDCB2BPINCancelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BUpdatePINStatusResponse")
    public JAXBElement<SDCB2BUpdatePINStatusResponse> createSDCB2BUpdatePINStatusResponse(SDCB2BUpdatePINStatusResponse value) {
        return new JAXBElement<SDCB2BUpdatePINStatusResponse>(_SDCB2BUpdatePINStatusResponse_QNAME, SDCB2BUpdatePINStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxLineIndia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxLineIndia")
    public JAXBElement<TaxLineIndia> createTaxLineIndia(TaxLineIndia value) {
        return new JAXBElement<TaxLineIndia>(_TaxLineIndia_QNAME, TaxLineIndia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetBlockPINRedemptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetBlockPINRedemptionResponse")
    public JAXBElement<SDCGetBlockPINRedemptionResponse> createSDCGetBlockPINRedemptionResponse(SDCGetBlockPINRedemptionResponse value) {
        return new JAXBElement<SDCGetBlockPINRedemptionResponse>(_SDCGetBlockPINRedemptionResponse_QNAME, SDCGetBlockPINRedemptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChargeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfChargeLine")
    public JAXBElement<ArrayOfChargeLine> createArrayOfChargeLine(ArrayOfChargeLine value) {
        return new JAXBElement<ArrayOfChargeLine>(_ArrayOfChargeLine_QNAME, ArrayOfChargeLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BUpdatePINStatusTable")
    public JAXBElement<SDCB2BUpdatePINStatusTable> createSDCB2BUpdatePINStatusTable(SDCB2BUpdatePINStatusTable value) {
        return new JAXBElement<SDCB2BUpdatePINStatusTable>(_SDCB2BUpdatePINStatusTable_QNAME, SDCB2BUpdatePINStatusTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxLine")
    public JAXBElement<TaxLine> createTaxLine(TaxLine value) {
        return new JAXBElement<TaxLine>(_TaxLine_QNAME, TaxLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AffiliationLoyaltyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "AffiliationLoyaltyTier")
    public JAXBElement<AffiliationLoyaltyTier> createAffiliationLoyaltyTier(AffiliationLoyaltyTier value) {
        return new JAXBElement<AffiliationLoyaltyTier>(_AffiliationLoyaltyTier_QNAME, AffiliationLoyaltyTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCSaveWOTOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_SaveWOTOrderResponse")
    public JAXBElement<SDCSaveWOTOrderResponse> createSDCSaveWOTOrderResponse(SDCSaveWOTOrderResponse value) {
        return new JAXBElement<SDCSaveWOTOrderResponse>(_SDCSaveWOTOrderResponse_QNAME, SDCSaveWOTOrderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCustPenaltyChargeTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CustPenaltyChargeTable")
    public JAXBElement<ArrayOfSDCCustPenaltyChargeTable> createArrayOfSDCCustPenaltyChargeTable(ArrayOfSDCCustPenaltyChargeTable value) {
        return new JAXBElement<ArrayOfSDCCustPenaltyChargeTable>(_ArrayOfSDCCustPenaltyChargeTable_QNAME, ArrayOfSDCCustPenaltyChargeTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeValueBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "AttributeValueBase")
    public JAXBElement<AttributeValueBase> createAttributeValueBase(AttributeValueBase value) {
        return new JAXBElement<AttributeValueBase>(_AttributeValueBase_QNAME, AttributeValueBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateCancelTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINUpdateCancelTable")
    public JAXBElement<SDCB2BPINUpdateCancelTable> createSDCB2BPINUpdateCancelTable(SDCB2BPINUpdateCancelTable value) {
        return new JAXBElement<SDCB2BPINUpdateCancelTable>(_SDCB2BPINUpdateCancelTable_QNAME, SDCB2BPINUpdateCancelTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCOnLineParameter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_OnLineParameter")
    public JAXBElement<SDCOnLineParameter> createSDCOnLineParameter(SDCOnLineParameter value) {
        return new JAXBElement<SDCOnLineParameter>(_SDCOnLineParameter_QNAME, SDCOnLineParameter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCOnLineParameter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_OnLineParameter")
    public JAXBElement<ArrayOfSDCOnLineParameter> createArrayOfSDCOnLineParameter(ArrayOfSDCOnLineParameter value) {
        return new JAXBElement<ArrayOfSDCOnLineParameter>(_ArrayOfSDCOnLineParameter_QNAME, ArrayOfSDCOnLineParameter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTenderLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfTenderLine")
    public JAXBElement<ArrayOfTenderLine> createArrayOfTenderLine(ArrayOfTenderLine value) {
        return new JAXBElement<ArrayOfTenderLine>(_ArrayOfTenderLine_QNAME, ArrayOfTenderLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TenderLineBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TenderLineBase")
    public JAXBElement<TenderLineBase> createTenderLineBase(TenderLineBase value) {
        return new JAXBElement<TenderLineBase>(_TenderLineBase_QNAME, TenderLineBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAffiliationLoyaltyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfAffiliationLoyaltyTier")
    public JAXBElement<ArrayOfAffiliationLoyaltyTier> createArrayOfAffiliationLoyaltyTier(ArrayOfAffiliationLoyaltyTier value) {
        return new JAXBElement<ArrayOfAffiliationLoyaltyTier>(_ArrayOfAffiliationLoyaltyTier_QNAME, ArrayOfAffiliationLoyaltyTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionItem")
    public JAXBElement<TransactionItem> createTransactionItem(TransactionItem value) {
        return new JAXBElement<TransactionItem>(_TransactionItem_QNAME, TransactionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChargeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ChargeLine")
    public JAXBElement<ChargeLine> createChargeLine(ChargeLine value) {
        return new JAXBElement<ChargeLine>(_ChargeLine_QNAME, ChargeLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINViewLineResponse")
    public JAXBElement<SDCB2BPINViewLineResponse> createSDCB2BPINViewLineResponse(SDCB2BPINViewLineResponse value) {
        return new JAXBElement<SDCB2BPINViewLineResponse>(_SDCB2BPINViewLineResponse_QNAME, SDCB2BPINViewLineResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPINRedemptionLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_PINRedemptionLineTable")
    public JAXBElement<SDCPINRedemptionLineTable> createSDCPINRedemptionLineTable(SDCPINRedemptionLineTable value) {
        return new JAXBElement<SDCPINRedemptionLineTable>(_SDCPINRedemptionLineTable_QNAME, SDCPINRedemptionLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustPenaltyChargeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_CustPenaltyChargeResponse")
    public JAXBElement<SDCCustPenaltyChargeResponse> createSDCCustPenaltyChargeResponse(SDCCustPenaltyChargeResponse value) {
        return new JAXBElement<SDCCustPenaltyChargeResponse>(_SDCCustPenaltyChargeResponse_QNAME, SDCCustPenaltyChargeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCommerceProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfCommerceProperty")
    public JAXBElement<ArrayOfCommerceProperty> createArrayOfCommerceProperty(ArrayOfCommerceProperty value) {
        return new JAXBElement<ArrayOfCommerceProperty>(_ArrayOfCommerceProperty_QNAME, ArrayOfCommerceProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_B2BPINViewTable")
    public JAXBElement<SDCB2BPINViewTable> createSDCB2BPINViewTable(SDCB2BPINViewTable value) {
        return new JAXBElement<SDCB2BPINViewTable>(_SDCB2BPINViewTable_QNAME, SDCB2BPINViewTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Transaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Transaction")
    public JAXBElement<Transaction> createTransaction(Transaction value) {
        return new JAXBElement<Transaction>(_Transaction_QNAME, Transaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReasonCodeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReasonCodeLine")
    public JAXBElement<ReasonCodeLine> createReasonCodeLine(ReasonCodeLine value) {
        return new JAXBElement<ReasonCodeLine>(_ReasonCodeLine_QNAME, ReasonCodeLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCartLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfCartLine")
    public JAXBElement<ArrayOfCartLine> createArrayOfCartLine(ArrayOfCartLine value) {
        return new JAXBElement<ArrayOfCartLine>(_ArrayOfCartLine_QNAME, ArrayOfCartLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CartType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CartType")
    public JAXBElement<CartType> createCartType(CartType value) {
        return new JAXBElement<CartType>(_CartType_QNAME, CartType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIncomeExpenseLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfIncomeExpenseLine")
    public JAXBElement<ArrayOfIncomeExpenseLine> createArrayOfIncomeExpenseLine(ArrayOfIncomeExpenseLine value) {
        return new JAXBElement<ArrayOfIncomeExpenseLine>(_ArrayOfIncomeExpenseLine_QNAME, ArrayOfIncomeExpenseLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCOnLineParametersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_OnLineParametersResponse")
    public JAXBElement<SDCOnLineParametersResponse> createSDCOnLineParametersResponse(SDCOnLineParametersResponse value) {
        return new JAXBElement<SDCOnLineParametersResponse>(_SDCOnLineParametersResponse_QNAME, SDCOnLineParametersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfReasonCodeLine")
    public JAXBElement<ArrayOfReasonCodeLine> createArrayOfReasonCodeLine(ArrayOfReasonCodeLine value) {
        return new JAXBElement<ArrayOfReasonCodeLine>(_ArrayOfReasonCodeLine_QNAME, ArrayOfReasonCodeLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfTransactionItem")
    public JAXBElement<ArrayOfTransactionItem> createArrayOfTransactionItem(ArrayOfTransactionItem value) {
        return new JAXBElement<ArrayOfTransactionItem>(_ArrayOfTransactionItem_QNAME, ArrayOfTransactionItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTaxViewLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ArrayOfTaxViewLine")
    public JAXBElement<ArrayOfTaxViewLine> createArrayOfTaxViewLine(ArrayOfTaxViewLine value) {
        return new JAXBElement<ArrayOfTaxViewLine>(_ArrayOfTaxViewLine_QNAME, ArrayOfTaxViewLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionItemType")
    public JAXBElement<TransactionItemType> createTransactionItemType(TransactionItemType value) {
        return new JAXBElement<TransactionItemType>(_TransactionItemType_QNAME, TransactionItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_B2BPINUpdateResponse")
    public JAXBElement<SDCB2BPINUpdateResponse> createSDCB2BPINUpdateResponse(SDCB2BPINUpdateResponse value) {
        return new JAXBElement<SDCB2BPINUpdateResponse>(_SDCB2BPINUpdateResponse_QNAME, SDCB2BPINUpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPINRedemptionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_PINRedemptionTable")
    public JAXBElement<ArrayOfSDCPINRedemptionTable> createArrayOfSDCPINRedemptionTable(ArrayOfSDCPINRedemptionTable value) {
        return new JAXBElement<ArrayOfSDCPINRedemptionTable>(_ArrayOfSDCPINRedemptionTable_QNAME, ArrayOfSDCPINRedemptionTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_B2BPINViewTable")
    public JAXBElement<ArrayOfSDCB2BPINViewTable> createArrayOfSDCB2BPINViewTable(ArrayOfSDCB2BPINViewTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewTable>(_ArrayOfSDCB2BPINViewTable_QNAME, ArrayOfSDCB2BPINViewTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceEntity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommerceEntity")
    public JAXBElement<CommerceEntity> createCommerceEntityBase(CommerceEntity value) {
        return new JAXBElement<CommerceEntity>(_CommerceEntityBase_QNAME, CommerceEntity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImageInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ImageInfo")
    public JAXBElement<ImageInfo> createImageInfo(ImageInfo value) {
        return new JAXBElement<ImageInfo>(_ImageInfo_QNAME, ImageInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SalesOrder")
    public JAXBElement<SalesOrder> createSalesOrder(SalesOrder value) {
        return new JAXBElement<SalesOrder>(_SalesOrder_QNAME, SalesOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TenderLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TenderLine")
    public JAXBElement<TenderLine> createTenderLine(TenderLine value) {
        return new JAXBElement<TenderLine>(_TenderLine_QNAME, TenderLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShippingAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Address")
    public JAXBElement<ShippingAddress> createShippingAddress(ShippingAddress value) {
        return new JAXBElement<ShippingAddress>(_ShippingAddress_QNAME, ShippingAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_pinCode", scope = B2BPINCancel.class)
    public JAXBElement<String> createB2BPINCancelPinCode(String value) {
        return new JAXBElement<String>(_B2BPINCancelPinCode_QNAME, String.class, B2BPINCancel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_markUpCode", scope = B2BPINCancel.class)
    public JAXBElement<String> createB2BPINCancelMarkUpCode(String value) {
        return new JAXBElement<String>(_B2BPINCancelMarkUpCode_QNAME, String.class, B2BPINCancel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pinRefId", scope = GetB2BPINViewLine.class)
    public JAXBElement<String> createGetB2BPINViewLinePinRefId(String value) {
        return new JAXBElement<String>(_GetB2BPINViewLinePinRefId_QNAME, String.class, GetB2BPINViewLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINBlockedDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINBlockedDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINBlockedDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINCode", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINCode(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINCode_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReasonCode", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableReasonCode(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReasonCode_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINViewLineCollection", scope = SDCB2BPINViewTable.class)
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createSDCB2BPINViewTablePINViewLineCollection(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_SDCB2BPINViewTablePINViewLineCollection_QNAME, ArrayOfSDCB2BPINViewLineTable.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LastRedeemDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableLastRedeemDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableLastRedeemDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableDescription(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableDescription_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PackageName", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePackageName(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePackageName_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CustAccount", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableCustAccount(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableCustAccount_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaType", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableMediaType(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableMediaType_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINBlockedBy", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINBlockedBy(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINBlockedBy_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "StartDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableStartDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableStartDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EndDateTime", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableEndDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableEndDateTime_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReferenceId_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINStatus", scope = SDCB2BPINViewTable.class)
    public JAXBElement<String> createSDCB2BPINViewTablePINStatus(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINStatus_QNAME, String.class, SDCB2BPINViewTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetBlockPINRedemptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetPINRedemptionResult", scope = GetPINRedemptionResponse.class)
    public JAXBElement<SDCGetBlockPINRedemptionResponse> createGetPINRedemptionResponseGetPINRedemptionResult(SDCGetBlockPINRedemptionResponse value) {
        return new JAXBElement<SDCGetBlockPINRedemptionResponse>(_GetPINRedemptionResponseGetPINRedemptionResult_QNAME, SDCGetBlockPINRedemptionResponse.class, GetPINRedemptionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_processedBy", scope = B2BUpdatePINStatus.class)
    public JAXBElement<String> createB2BUpdatePINStatusProcessedBy(String value) {
        return new JAXBElement<String>(_B2BUpdatePINStatusProcessedBy_QNAME, String.class, B2BUpdatePINStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_pinCode", scope = B2BUpdatePINStatus.class)
    public JAXBElement<String> createB2BUpdatePINStatusPinCode(String value) {
        return new JAXBElement<String>(_B2BPINCancelPinCode_QNAME, String.class, B2BUpdatePINStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_reasonCode", scope = B2BUpdatePINStatus.class)
    public JAXBElement<String> createB2BUpdatePINStatusReasonCode(String value) {
        return new JAXBElement<String>(_B2BUpdatePINStatusReasonCode_QNAME, String.class, B2BUpdatePINStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewLineResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BPINViewLineResult", scope = GetB2BPINViewLineResponse.class)
    public JAXBElement<SDCB2BPINViewLineResponse> createGetB2BPINViewLineResponseGetB2BPINViewLineResult(SDCB2BPINViewLineResponse value) {
        return new JAXBElement<SDCB2BPINViewLineResponse>(_GetB2BPINViewLineResponseGetB2BPINViewLineResult_QNAME, SDCB2BPINViewLineResponse.class, GetB2BPINViewLineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BPINUpdateResult", scope = B2BPINUpdateResponse.class)
    public JAXBElement<SDCB2BPINUpdateResponse> createB2BPINUpdateResponseB2BPINUpdateResult(SDCB2BPINUpdateResponse value) {
        return new JAXBElement<SDCB2BPINUpdateResponse>(_B2BPINUpdateResponseB2BPINUpdateResult_QNAME, SDCB2BPINUpdateResponse.class, B2BPINUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCSaveWOTOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "SaveWOTOrderResult", scope = SaveWOTOrderResponse.class)
    public JAXBElement<SDCSaveWOTOrderResponse> createSaveWOTOrderResponseSaveWOTOrderResult(SDCSaveWOTOrderResponse value) {
        return new JAXBElement<SDCSaveWOTOrderResponse>(_SaveWOTOrderResponseSaveWOTOrderResult_QNAME, SDCSaveWOTOrderResponse.class, SaveWOTOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCustPenaltyChargeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetCustPenaltyChargeResult", scope = GetCustPenaltyChargeResponse.class)
    public JAXBElement<SDCCustPenaltyChargeResponse> createGetCustPenaltyChargeResponseGetCustPenaltyChargeResult(SDCCustPenaltyChargeResponse value) {
        return new JAXBElement<SDCCustPenaltyChargeResponse>(_GetCustPenaltyChargeResponseGetCustPenaltyChargeResult_QNAME, SDCCustPenaltyChargeResponse.class, GetCustPenaltyChargeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LongValue", scope = CommercePropertyValue.class)
    public JAXBElement<Long> createCommercePropertyValueLongValue(Long value) {
        return new JAXBElement<Long>(_CommercePropertyValueLongValue_QNAME, Long.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StringValue", scope = CommercePropertyValue.class)
    public JAXBElement<String> createCommercePropertyValueStringValue(String value) {
        return new JAXBElement<String>(_CommercePropertyValueStringValue_QNAME, String.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ByteValue", scope = CommercePropertyValue.class)
    public JAXBElement<Short> createCommercePropertyValueByteValue(Short value) {
        return new JAXBElement<Short>(_CommercePropertyValueByteValue_QNAME, Short.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "BooleanValue", scope = CommercePropertyValue.class)
    public JAXBElement<Boolean> createCommercePropertyValueBooleanValue(Boolean value) {
        return new JAXBElement<Boolean>(_CommercePropertyValueBooleanValue_QNAME, Boolean.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "IntegerValue", scope = CommercePropertyValue.class)
    public JAXBElement<Integer> createCommercePropertyValueIntegerValue(Integer value) {
        return new JAXBElement<Integer>(_CommercePropertyValueIntegerValue_QNAME, Integer.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DateTimeOffsetValue", scope = CommercePropertyValue.class)
    public JAXBElement<DateTimeOffset> createCommercePropertyValueDateTimeOffsetValue(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CommercePropertyValueDateTimeOffsetValue_QNAME, DateTimeOffset.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DecimalValue", scope = CommercePropertyValue.class)
    public JAXBElement<BigDecimal> createCommercePropertyValueDecimalValue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CommercePropertyValueDecimalValue_QNAME, BigDecimal.class, CommercePropertyValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Name", scope = AttributeValueBase.class)
    public JAXBElement<String> createAttributeValueBaseName(String value) {
        return new JAXBElement<String>(_AttributeValueBaseName_QNAME, String.class, AttributeValueBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Status", scope = SalesOrder.class)
    public JAXBElement<String> createSalesOrderStatus(String value) {
        return new JAXBElement<String>(_SalesOrderStatus_QNAME, String.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ConfirmationId", scope = SalesOrder.class)
    public JAXBElement<String> createSalesOrderConfirmationId(String value) {
        return new JAXBElement<String>(_SalesOrderConfirmationId_QNAME, String.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OrderPlacedDate", scope = SalesOrder.class)
    public JAXBElement<String> createSalesOrderOrderPlacedDate(String value) {
        return new JAXBElement<String>(_SalesOrderOrderPlacedDate_QNAME, String.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RequestedDeliveryDate", scope = SalesOrder.class)
    public JAXBElement<String> createSalesOrderRequestedDeliveryDate(String value) {
        return new JAXBElement<String>(_SalesOrderRequestedDeliveryDate_QNAME, String.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Id", scope = SalesOrder.class)
    public JAXBElement<String> createSalesOrderId(String value) {
        return new JAXBElement<String>(_SalesOrderId_QNAME, String.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SalesId", scope = SalesOrder.class)
    public JAXBElement<String> createSalesOrderSalesId(String value) {
        return new JAXBElement<String>(_SalesOrderSalesId_QNAME, String.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Items", scope = SalesOrder.class)
    public JAXBElement<ArrayOfTransactionItem> createSalesOrderItems(ArrayOfTransactionItem value) {
        return new JAXBElement<ArrayOfTransactionItem>(_SalesOrderItems_QNAME, ArrayOfTransactionItem.class, SalesOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SubtotalWithCurrency", scope = Transaction.class)
    public JAXBElement<String> createTransactionSubtotalWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionSubtotalWithCurrency_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DiscountCodes", scope = Transaction.class)
    public JAXBElement<ArrayOfstring> createTransactionDiscountCodes(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_TransactionDiscountCodes_QNAME, ArrayOfstring.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "LoyaltyCardId", scope = Transaction.class)
    public JAXBElement<String> createTransactionLoyaltyCardId(String value) {
        return new JAXBElement<String>(_TransactionLoyaltyCardId_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TaxAmountWithCurrency", scope = Transaction.class)
    public JAXBElement<String> createTransactionTaxAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionTaxAmountWithCurrency_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DiscountAmountWithCurrency", scope = Transaction.class)
    public JAXBElement<String> createTransactionDiscountAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionDiscountAmountWithCurrency_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ChargeAmountWithCurrency", scope = Transaction.class)
    public JAXBElement<String> createTransactionChargeAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionChargeAmountWithCurrency_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ShippingAddress", scope = Transaction.class)
    public JAXBElement<Address> createTransactionShippingAddress(Address value) {
        return new JAXBElement<Address>(_TransactionShippingAddress_QNAME, Address.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DeliveryModeDescription", scope = Transaction.class)
    public JAXBElement<String> createTransactionDeliveryModeDescription(String value) {
        return new JAXBElement<String>(_TransactionDeliveryModeDescription_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TotalAmountWithCurrency", scope = Transaction.class)
    public JAXBElement<String> createTransactionTotalAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionTotalAmountWithCurrency_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DeliveryModeId", scope = Transaction.class)
    public JAXBElement<String> createTransactionDeliveryModeId(String value) {
        return new JAXBElement<String>(_TransactionDeliveryModeId_QNAME, String.class, Transaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CustAccount", scope = GetCustPenaltyCharge.class)
    public JAXBElement<String> createGetCustPenaltyChargeCustAccount(String value) {
        return new JAXBElement<String>(_GetCustPenaltyChargeCustAccount_QNAME, String.class, GetCustPenaltyCharge.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MARKUPCODE", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTableMARKUPCODE(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTableMARKUPCODE_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EVENTLINEID", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTableEVENTLINEID(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTableEVENTLINEID_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DATAAREAID", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTableDATAAREAID(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTableDATAAREAID_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PENALTYID", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTablePENALTYID(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTablePENALTYID_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ACCOUNTNUM", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTableACCOUNTNUM(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTableACCOUNTNUM_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DESCRIPTION", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTableDESCRIPTION(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTableDESCRIPTION_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EVENTGROUPID", scope = SDCCustPenaltyChargeTable.class)
    public JAXBElement<String> createSDCCustPenaltyChargeTableEVENTGROUPID(String value) {
        return new JAXBElement<String>(_SDCCustPenaltyChargeTableEVENTGROUPID_QNAME, String.class, SDCCustPenaltyChargeTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateCancelTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "table", scope = SDCB2BPINCancelResponse.class)
    public JAXBElement<SDCB2BPINUpdateCancelTable> createSDCB2BPINCancelResponseTable(SDCB2BPINUpdateCancelTable value) {
        return new JAXBElement<SDCB2BPINUpdateCancelTable>(_SDCB2BPINCancelResponseTable_QNAME, SDCB2BPINUpdateCancelTable.class, SDCB2BPINCancelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINCancelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BPINCancelResult", scope = B2BPINCancelResponse.class)
    public JAXBElement<SDCB2BPINCancelResponse> createB2BPINCancelResponseB2BPINCancelResult(SDCB2BPINCancelResponse value) {
        return new JAXBElement<SDCB2BPINCancelResponse>(_B2BPINCancelResponseB2BPINCancelResult_QNAME, SDCB2BPINCancelResponse.class, B2BPINCancelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TransactionId", scope = ChargeLine.class)
    public JAXBElement<String> createChargeLineTransactionId(String value) {
        return new JAXBElement<String>(_ChargeLineTransactionId_QNAME, String.class, ChargeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Description", scope = ChargeLine.class)
    public JAXBElement<String> createChargeLineDescription(String value) {
        return new JAXBElement<String>(_ChargeLineDescription_QNAME, String.class, ChargeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ChargeCode", scope = ChargeLine.class)
    public JAXBElement<String> createChargeLineChargeCode(String value) {
        return new JAXBElement<String>(_ChargeLineChargeCode_QNAME, String.class, ChargeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CurrencyCode", scope = ChargeLine.class)
    public JAXBElement<String> createChargeLineCurrencyCode(String value) {
        return new JAXBElement<String>(_ChargeLineCurrencyCode_QNAME, String.class, ChargeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CustomerId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseCustomerId(String value) {
        return new JAXBElement<String>(_TenderLineBaseCustomerId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReasonCodeLines", scope = TenderLineBase.class)
    public JAXBElement<ArrayOfReasonCodeLine> createTenderLineBaseReasonCodeLines(ArrayOfReasonCodeLine value) {
        return new JAXBElement<ArrayOfReasonCodeLine>(_TenderLineBaseReasonCodeLines_QNAME, ArrayOfReasonCodeLine.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TenderTypeId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseTenderTypeId(String value) {
        return new JAXBElement<String>(_TenderLineBaseTenderTypeId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CreditMemoId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseCreditMemoId(String value) {
        return new JAXBElement<String>(_TenderLineBaseCreditMemoId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LoyaltyCardId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseLoyaltyCardId(String value) {
        return new JAXBElement<String>(_TenderLineBaseLoyaltyCardId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "GiftCardId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseGiftCardId(String value) {
        return new JAXBElement<String>(_TenderLineBaseGiftCardId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SignatureData", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseSignatureData(String value) {
        return new JAXBElement<String>(_TenderLineBaseSignatureData_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Currency", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseCurrency(String value) {
        return new JAXBElement<String>(_TenderLineBaseCurrency_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CardTypeId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseCardTypeId(String value) {
        return new JAXBElement<String>(_TenderLineBaseCardTypeId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TenderLineId", scope = TenderLineBase.class)
    public JAXBElement<String> createTenderLineBaseTenderLineId(String value) {
        return new JAXBElement<String>(_TenderLineBaseTenderLineId_QNAME, String.class, TenderLineBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SalesTaxGroupId", scope = TaxableItem.class)
    public JAXBElement<String> createTaxableItemSalesTaxGroupId(String value) {
        return new JAXBElement<String>(_TaxableItemSalesTaxGroupId_QNAME, String.class, TaxableItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SalesOrderUnitOfMeasure", scope = TaxableItem.class)
    public JAXBElement<String> createTaxableItemSalesOrderUnitOfMeasure(String value) {
        return new JAXBElement<String>(_TaxableItemSalesOrderUnitOfMeasure_QNAME, String.class, TaxableItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ItemTaxGroupId", scope = TaxableItem.class)
    public JAXBElement<String> createTaxableItemItemTaxGroupId(String value) {
        return new JAXBElement<String>(_TaxableItemItemTaxGroupId_QNAME, String.class, TaxableItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ItemId", scope = TaxableItem.class)
    public JAXBElement<String> createTaxableItemItemId(String value) {
        return new JAXBElement<String>(_TaxableItemItemId_QNAME, String.class, TaxableItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTaxLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxLines", scope = TaxableItem.class)
    public JAXBElement<ArrayOfTaxLine> createTaxableItemTaxLines(ArrayOfTaxLine value) {
        return new JAXBElement<ArrayOfTaxLine>(_TaxableItemTaxLines_QNAME, ArrayOfTaxLine.class, TaxableItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventGroupId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventGroupId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SalesId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableSalesId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableSalesId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InventTransId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableInventTransId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInventTransId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableItemId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableItemId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InvoiceId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableInvoiceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInvoiceId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "OpenValidityRule", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableOpenValidityRule(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableOpenValidityRule_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableTransactionId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionLineNum", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableTransactionLineNum(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableTransactionLineNum_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventDate", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventDate(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventDate_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DefineTicketDate", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableDefineTicketDate(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableDefineTicketDate_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaType", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableMediaType(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableMediaType_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineNum", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableLineNum(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableLineNum_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableRetailVariantId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "StartDateTime", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableStartDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableStartDateTime_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EndDateTime", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEndDateTime(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableEndDateTime_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableEventLineId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventLineId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCB2BPINViewLineTable.class)
    public JAXBElement<String> createSDCB2BPINViewLineTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReferenceId_QNAME, String.class, SDCB2BPINViewLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReasonDesc", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableReasonDesc(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionTableReasonDesc_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SessionNo", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableSessionNo(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionTableSessionNo_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINCode", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTablePINCode(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePINCode_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReasonCode", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableReasonCode(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReasonCode_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPINRedemptionLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PINRedemptionLines", scope = SDCPINRedemptionTable.class)
    public JAXBElement<ArrayOfSDCPINRedemptionLineTable> createSDCPINRedemptionTablePINRedemptionLines(ArrayOfSDCPINRedemptionLineTable value) {
        return new JAXBElement<ArrayOfSDCPINRedemptionLineTable>(_SDCPINRedemptionTablePINRedemptionLines_QNAME, ArrayOfSDCPINRedemptionLineTable.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Description", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableDescription(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableDescription_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaTypeId", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableMediaTypeId(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionTableMediaTypeId_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "PackageName", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTablePackageName(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTablePackageName_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReferenceId_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "CustAccount", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableCustAccount(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableCustAccount_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UserId", scope = SDCPINRedemptionTable.class)
    public JAXBElement<String> createSDCPINRedemptionTableUserId(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionTableUserId_QNAME, String.class, SDCPINRedemptionTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CustomerId", scope = AffiliationLoyaltyTier.class)
    public JAXBElement<String> createAffiliationLoyaltyTierCustomerId(String value) {
        return new JAXBElement<String>(_TenderLineBaseCustomerId_QNAME, String.class, AffiliationLoyaltyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReasonCodeLines", scope = AffiliationLoyaltyTier.class)
    public JAXBElement<ArrayOfReasonCodeLine> createAffiliationLoyaltyTierReasonCodeLines(ArrayOfReasonCodeLine value) {
        return new JAXBElement<ArrayOfReasonCodeLine>(_TenderLineBaseReasonCodeLines_QNAME, ArrayOfReasonCodeLine.class, AffiliationLoyaltyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommercePropertyValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Value", scope = CommerceProperty.class)
    public JAXBElement<CommercePropertyValue> createCommercePropertyValue(CommercePropertyValue value) {
        return new JAXBElement<CommercePropertyValue>(_CommercePropertyValue_QNAME, CommercePropertyValue.class, CommerceProperty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Key", scope = CommerceProperty.class)
    public JAXBElement<String> createCommercePropertyKey(String value) {
        return new JAXBElement<String>(_CommercePropertyKey_QNAME, String.class, CommerceProperty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "table", scope = SDCB2BUpdatePINStatusResponse.class)
    public JAXBElement<SDCB2BUpdatePINStatusTable> createSDCB2BUpdatePINStatusResponseTable(SDCB2BUpdatePINStatusTable value) {
        return new JAXBElement<SDCB2BUpdatePINStatusTable>(_SDCB2BPINCancelResponseTable_QNAME, SDCB2BUpdatePINStatusTable.class, SDCB2BUpdatePINStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DeliveryMode", scope = CartLine.class)
    public JAXBElement<String> createCartLineDeliveryMode(String value) {
        return new JAXBElement<String>(_CartLineDeliveryMode_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "RequestedDeliveryDate", scope = CartLine.class)
    public JAXBElement<DateTimeOffset> createCartLineRequestedDeliveryDate(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CartLineRequestedDeliveryDate_QNAME, DateTimeOffset.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "QuantityOrdered", scope = CartLine.class)
    public JAXBElement<BigDecimal> createCartLineQuantityOrdered(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartLineQuantityOrdered_QNAME, BigDecimal.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ItemTaxGroupId", scope = CartLine.class)
    public JAXBElement<String> createCartLineItemTaxGroupId(String value) {
        return new JAXBElement<String>(_TaxableItemItemTaxGroupId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDiscountLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DiscountLines", scope = CartLine.class)
    public JAXBElement<ArrayOfDiscountLine> createCartLineDiscountLines(ArrayOfDiscountLine value) {
        return new JAXBElement<ArrayOfDiscountLine>(_CartLineDiscountLines_QNAME, ArrayOfDiscountLine.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "InventoryDimensionId", scope = CartLine.class)
    public JAXBElement<String> createCartLineInventoryDimensionId(String value) {
        return new JAXBElement<String>(_CartLineInventoryDimensionId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShippingAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ShippingAddress", scope = CartLine.class)
    public JAXBElement<ShippingAddress> createCartLineShippingAddress(ShippingAddress value) {
        return new JAXBElement<ShippingAddress>(_CartLineShippingAddress_QNAME, ShippingAddress.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Barcode", scope = CartLine.class)
    public JAXBElement<String> createCartLineBarcode(String value) {
        return new JAXBElement<String>(_CartLineBarcode_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReturnTransactionId", scope = CartLine.class)
    public JAXBElement<String> createCartLineReturnTransactionId(String value) {
        return new JAXBElement<String>(_CartLineReturnTransactionId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SerialNumber", scope = CartLine.class)
    public JAXBElement<String> createCartLineSerialNumber(String value) {
        return new JAXBElement<String>(_CartLineSerialNumber_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "WarehouseId", scope = CartLine.class)
    public JAXBElement<String> createCartLineWarehouseId(String value) {
        return new JAXBElement<String>(_CartLineWarehouseId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReasonCodeLines", scope = CartLine.class)
    public JAXBElement<ArrayOfReasonCodeLine> createCartLineReasonCodeLines(ArrayOfReasonCodeLine value) {
        return new JAXBElement<ArrayOfReasonCodeLine>(_TenderLineBaseReasonCodeLines_QNAME, ArrayOfReasonCodeLine.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxOverrideCode", scope = CartLine.class)
    public JAXBElement<String> createCartLineTaxOverrideCode(String value) {
        return new JAXBElement<String>(_CartLineTaxOverrideCode_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DeliveryModeChargeAmount", scope = CartLine.class)
    public JAXBElement<BigDecimal> createCartLineDeliveryModeChargeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartLineDeliveryModeChargeAmount_QNAME, BigDecimal.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Comment", scope = CartLine.class)
    public JAXBElement<String> createCartLineComment(String value) {
        return new JAXBElement<String>(_CartLineComment_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LineId", scope = CartLine.class)
    public JAXBElement<String> createCartLineLineId(String value) {
        return new JAXBElement<String>(_CartLineLineId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "UnitOfMeasureSymbol", scope = CartLine.class)
    public JAXBElement<String> createCartLineUnitOfMeasureSymbol(String value) {
        return new JAXBElement<String>(_CartLineUnitOfMeasureSymbol_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StoreNumber", scope = CartLine.class)
    public JAXBElement<String> createCartLineStoreNumber(String value) {
        return new JAXBElement<String>(_CartLineStoreNumber_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "QuantityInvoiced", scope = CartLine.class)
    public JAXBElement<BigDecimal> createCartLineQuantityInvoiced(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartLineQuantityInvoiced_QNAME, BigDecimal.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReturnInventTransId", scope = CartLine.class)
    public JAXBElement<String> createCartLineReturnInventTransId(String value) {
        return new JAXBElement<String>(_CartLineReturnInventTransId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "PromotionLines", scope = CartLine.class)
    public JAXBElement<ArrayOfstring> createCartLinePromotionLines(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_CartLinePromotionLines_QNAME, ArrayOfstring.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChargeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ChargeLines", scope = CartLine.class)
    public JAXBElement<ArrayOfChargeLine> createCartLineChargeLines(ArrayOfChargeLine value) {
        return new JAXBElement<ArrayOfChargeLine>(_CartLineChargeLines_QNAME, ArrayOfChargeLine.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ItemId", scope = CartLine.class)
    public JAXBElement<String> createCartLineItemId(String value) {
        return new JAXBElement<String>(_TaxableItemItemId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "InvoiceId", scope = CartLine.class)
    public JAXBElement<String> createCartLineInvoiceId(String value) {
        return new JAXBElement<String>(_CartLineInvoiceId_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Description", scope = CartLine.class)
    public JAXBElement<String> createCartLineDescription(String value) {
        return new JAXBElement<String>(_ChargeLineDescription_QNAME, String.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "OriginalPrice", scope = CartLine.class)
    public JAXBElement<BigDecimal> createCartLineOriginalPrice(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartLineOriginalPrice_QNAME, BigDecimal.class, CartLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableEventGroupId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventGroupId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SalesId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableSalesId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableSalesId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InventTransId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableInventTransId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInventTransId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableItemId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableItemId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "InvoiceId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableInvoiceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableInvoiceId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "OpenValidityId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionLineTableOpenValidityId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventDate", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<XMLGregorianCalendar> createSDCPINRedemptionLineTableEventDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_SDCB2BPINViewLineTableEventDate_QNAME, XMLGregorianCalendar.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "MediaTypeId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableMediaTypeId(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionTableMediaTypeId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableEventLineId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewLineTableEventLineId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ReferenceId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableReferenceId(String value) {
        return new JAXBElement<String>(_SDCB2BPINViewTableReferenceId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UserId", scope = SDCPINRedemptionLineTable.class)
    public JAXBElement<String> createSDCPINRedemptionLineTableUserId(String value) {
        return new JAXBElement<String>(_SDCPINRedemptionTableUserId_QNAME, String.class, SDCPINRedemptionLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCOnLineParameter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "OnLineParameters", scope = SDCOnLineParametersResponse.class)
    public JAXBElement<ArrayOfSDCOnLineParameter> createSDCOnLineParametersResponseOnLineParameters(ArrayOfSDCOnLineParameter value) {
        return new JAXBElement<ArrayOfSDCOnLineParameter>(_SDCOnLineParametersResponseOnLineParameters_QNAME, ArrayOfSDCOnLineParameter.class, SDCOnLineParametersResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Email", scope = Address.class)
    public JAXBElement<String> createAddressEmail(String value) {
        return new JAXBElement<String>(_AddressEmail_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ZipCode", scope = Address.class)
    public JAXBElement<String> createAddressZipCode(String value) {
        return new JAXBElement<String>(_AddressZipCode_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EmailContent", scope = Address.class)
    public JAXBElement<String> createAddressEmailContent(String value) {
        return new JAXBElement<String>(_AddressEmailContent_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "City", scope = Address.class)
    public JAXBElement<String> createAddressCity(String value) {
        return new JAXBElement<String>(_AddressCity_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "County", scope = Address.class)
    public JAXBElement<String> createAddressCounty(String value) {
        return new JAXBElement<String>(_AddressCounty_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Name", scope = Address.class)
    public JAXBElement<String> createAddressName(String value) {
        return new JAXBElement<String>(_AddressName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DistrictName", scope = Address.class)
    public JAXBElement<String> createAddressDistrictName(String value) {
        return new JAXBElement<String>(_AddressDistrictName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AttentionTo", scope = Address.class)
    public JAXBElement<String> createAddressAttentionTo(String value) {
        return new JAXBElement<String>(_AddressAttentionTo_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Phone", scope = Address.class)
    public JAXBElement<String> createAddressPhone(String value) {
        return new JAXBElement<String>(_AddressPhone_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "State", scope = Address.class)
    public JAXBElement<String> createAddressState(String value) {
        return new JAXBElement<String>(_AddressState_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Country", scope = Address.class)
    public JAXBElement<String> createAddressCountry(String value) {
        return new JAXBElement<String>(_AddressCountry_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Street", scope = Address.class)
    public JAXBElement<String> createAddressStreet(String value) {
        return new JAXBElement<String>(_AddressStreet_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "StreetNumber", scope = Address.class)
    public JAXBElement<String> createAddressStreetNumber(String value) {
        return new JAXBElement<String>(_AddressStreetNumber_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AddressFriendlyName", scope = Address.class)
    public JAXBElement<String> createAddressAddressFriendlyName(String value) {
        return new JAXBElement<String>(_AddressAddressFriendlyName_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OnLineSalesDirect", scope = SDCOnLineParameter.class)
    public JAXBElement<String> createSDCOnLineParameterOnLineSalesDirect(String value) {
        return new JAXBElement<String>(_SDCOnLineParameterOnLineSalesDirect_QNAME, String.class, SDCOnLineParameter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OnLineSalesReservation", scope = SDCOnLineParameter.class)
    public JAXBElement<String> createSDCOnLineParameterOnLineSalesReservation(String value) {
        return new JAXBElement<String>(_SDCOnLineParameterOnLineSalesReservation_QNAME, String.class, SDCOnLineParameter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "returnMessage", scope = SDCB2BUpdatePINStatusTable.class)
    public JAXBElement<String> createSDCB2BUpdatePINStatusTableReturnMessage(String value) {
        return new JAXBElement<String>(_SDCB2BUpdatePINStatusTableReturnMessage_QNAME, String.class, SDCB2BUpdatePINStatusTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_itemId", scope = B2BPINUpdate.class)
    public JAXBElement<String> createB2BPINUpdateItemId(String value) {
        return new JAXBElement<String>(_B2BPINUpdateItemId_QNAME, String.class, B2BPINUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_pinCode", scope = B2BPINUpdate.class)
    public JAXBElement<String> createB2BPINUpdatePinCode(String value) {
        return new JAXBElement<String>(_B2BPINCancelPinCode_QNAME, String.class, B2BPINUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_markupCode", scope = B2BPINUpdate.class)
    public JAXBElement<String> createB2BPINUpdateMarkupCode(String value) {
        return new JAXBElement<String>(_B2BPINUpdateMarkupCode_QNAME, String.class, B2BPINUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_eventLineId", scope = B2BPINUpdate.class)
    public JAXBElement<String> createB2BPINUpdateEventLineId(String value) {
        return new JAXBElement<String>(_B2BPINUpdateEventLineId_QNAME, String.class, B2BPINUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "_inventTransId", scope = B2BPINUpdate.class)
    public JAXBElement<String> createB2BPINUpdateInventTransId(String value) {
        return new JAXBElement<String>(_B2BPINUpdateInventTransId_QNAME, String.class, B2BPINUpdate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DiscountApplicationGroup", scope = DiscountLine.class)
    public JAXBElement<String> createDiscountLineDiscountApplicationGroup(String value) {
        return new JAXBElement<String>(_DiscountLineDiscountApplicationGroup_QNAME, String.class, DiscountLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DiscountCode", scope = DiscountLine.class)
    public JAXBElement<String> createDiscountLineDiscountCode(String value) {
        return new JAXBElement<String>(_DiscountLineDiscountCode_QNAME, String.class, DiscountLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "OfferName", scope = DiscountLine.class)
    public JAXBElement<String> createDiscountLineOfferName(String value) {
        return new JAXBElement<String>(_DiscountLineOfferName_QNAME, String.class, DiscountLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "OfferId", scope = DiscountLine.class)
    public JAXBElement<String> createDiscountLineOfferId(String value) {
        return new JAXBElement<String>(_DiscountLineOfferId_QNAME, String.class, DiscountLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxItemGroup", scope = TaxLineIndia.class)
    public JAXBElement<String> createTaxLineIndiaTaxItemGroup(String value) {
        return new JAXBElement<String>(_TaxLineIndiaTaxItemGroup_QNAME, String.class, TaxLineIndia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxComponent", scope = TaxLineIndia.class)
    public JAXBElement<String> createTaxLineIndiaTaxComponent(String value) {
        return new JAXBElement<String>(_TaxLineIndiaTaxComponent_QNAME, String.class, TaxLineIndia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxCodesInFormula", scope = TaxLineIndia.class)
    public JAXBElement<ArrayOfstring> createTaxLineIndiaTaxCodesInFormula(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_TaxLineIndiaTaxCodesInFormula_QNAME, ArrayOfstring.class, TaxLineIndia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxFormula", scope = TaxLineIndia.class)
    public JAXBElement<String> createTaxLineIndiaTaxFormula(String value) {
        return new JAXBElement<String>(_TaxLineIndiaTaxFormula_QNAME, String.class, TaxLineIndia.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCustPenaltyChargeTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "custPenaltyChargetTable", scope = SDCCustPenaltyChargeResponse.class)
    public JAXBElement<ArrayOfSDCCustPenaltyChargeTable> createSDCCustPenaltyChargeResponseCustPenaltyChargetTable(ArrayOfSDCCustPenaltyChargeTable value) {
        return new JAXBElement<ArrayOfSDCCustPenaltyChargeTable>(_SDCCustPenaltyChargeResponseCustPenaltyChargetTable_QNAME, ArrayOfSDCCustPenaltyChargeTable.class, SDCCustPenaltyChargeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "custAccount", scope = GetB2BPINView.class)
    public JAXBElement<String> createGetB2BPINViewCustAccount(String value) {
        return new JAXBElement<String>(_GetB2BPINViewCustAccount_QNAME, String.class, GetB2BPINView.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pinCode", scope = GetPINRedemption.class)
    public JAXBElement<String> createGetPINRedemptionPinCode(String value) {
        return new JAXBElement<String>(_GetPINRedemptionPinCode_QNAME, String.class, GetPINRedemption.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "BuildingCompliment", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressBuildingCompliment(String value) {
        return new JAXBElement<String>(_ShippingAddressBuildingCompliment_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "PhoneExt", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressPhoneExt(String value) {
        return new JAXBElement<String>(_ShippingAddressPhoneExt_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "PartyNumber", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressPartyNumber(String value) {
        return new JAXBElement<String>(_ShippingAddressPartyNumber_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DistrictName", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressDistrictName(String value) {
        return new JAXBElement<String>(_ShippingAddressDistrictName_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "EmailLogisticsLocationId", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressEmailLogisticsLocationId(String value) {
        return new JAXBElement<String>(_ShippingAddressEmailLogisticsLocationId_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "State", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressState(String value) {
        return new JAXBElement<String>(_ShippingAddressState_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxGroup", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressTaxGroup(String value) {
        return new JAXBElement<String>(_ShippingAddressTaxGroup_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LogisticsLocationId", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressLogisticsLocationId(String value) {
        return new JAXBElement<String>(_ShippingAddressLogisticsLocationId_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "City", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressCity(String value) {
        return new JAXBElement<String>(_ShippingAddressCity_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CountyName", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressCountyName(String value) {
        return new JAXBElement<String>(_ShippingAddressCountyName_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Postbox", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressPostbox(String value) {
        return new JAXBElement<String>(_ShippingAddressPostbox_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ZipCode", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressZipCode(String value) {
        return new JAXBElement<String>(_ShippingAddressZipCode_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Street", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressStreet(String value) {
        return new JAXBElement<String>(_ShippingAddressStreet_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StreetNumber", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressStreetNumber(String value) {
        return new JAXBElement<String>(_ShippingAddressStreetNumber_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "PhoneLogisticsLocationId", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressPhoneLogisticsLocationId(String value) {
        return new JAXBElement<String>(_ShippingAddressPhoneLogisticsLocationId_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TwoLetterISORegionName", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressTwoLetterISORegionName(String value) {
        return new JAXBElement<String>(_ShippingAddressTwoLetterISORegionName_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Phone", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressPhone(String value) {
        return new JAXBElement<String>(_ShippingAddressPhone_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "UrlLogisticsLocationId", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressUrlLogisticsLocationId(String value) {
        return new JAXBElement<String>(_ShippingAddressUrlLogisticsLocationId_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "AttentionTo", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressAttentionTo(String value) {
        return new JAXBElement<String>(_ShippingAddressAttentionTo_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "EmailContent", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressEmailContent(String value) {
        return new JAXBElement<String>(_ShippingAddressEmailContent_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Name", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressName(String value) {
        return new JAXBElement<String>(_AttributeValueBaseName_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Url", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressUrl(String value) {
        return new JAXBElement<String>(_ShippingAddressUrl_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "County", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressCounty(String value) {
        return new JAXBElement<String>(_ShippingAddressCounty_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Email", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressEmail(String value) {
        return new JAXBElement<String>(_ShippingAddressEmail_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "FullAddress", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressFullAddress(String value) {
        return new JAXBElement<String>(_ShippingAddressFullAddress_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StateName", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressStateName(String value) {
        return new JAXBElement<String>(_ShippingAddressStateName_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ThreeLetterISORegionName", scope = ShippingAddress.class)
    public JAXBElement<String> createShippingAddressThreeLetterISORegionName(String value) {
        return new JAXBElement<String>(_ShippingAddressThreeLetterISORegionName_QNAME, String.class, ShippingAddress.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SourceCode2", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineSourceCode2(String value) {
        return new JAXBElement<String>(_ReasonCodeLineSourceCode2_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SourceCode3", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineSourceCode3(String value) {
        return new JAXBElement<String>(_ReasonCodeLineSourceCode3_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StatementCode", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineStatementCode(String value) {
        return new JAXBElement<String>(_ReasonCodeLineStatementCode_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TransactionId", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineTransactionId(String value) {
        return new JAXBElement<String>(_ChargeLineTransactionId_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReasonCodeId", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineReasonCodeId(String value) {
        return new JAXBElement<String>(_ReasonCodeLineReasonCodeId_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Information", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineInformation(String value) {
        return new JAXBElement<String>(_ReasonCodeLineInformation_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ItemTender", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineItemTender(String value) {
        return new JAXBElement<String>(_ReasonCodeLineItemTender_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SourceCode", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineSourceCode(String value) {
        return new JAXBElement<String>(_ReasonCodeLineSourceCode_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SubReasonCodeId", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineSubReasonCodeId(String value) {
        return new JAXBElement<String>(_ReasonCodeLineSubReasonCodeId_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DisplayString", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineDisplayString(String value) {
        return new JAXBElement<String>(_ReasonCodeLineDisplayString_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ParentLineId", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineParentLineId(String value) {
        return new JAXBElement<String>(_ReasonCodeLineParentLineId_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LineId", scope = ReasonCodeLine.class)
    public JAXBElement<String> createReasonCodeLineLineId(String value) {
        return new JAXBElement<String>(_CartLineLineId_QNAME, String.class, ReasonCodeLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "b2bPinViewTable", scope = SDCB2BPINViewResponse.class)
    public JAXBElement<ArrayOfSDCB2BPINViewTable> createSDCB2BPINViewResponseB2BPinViewTable(ArrayOfSDCB2BPINViewTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewTable>(_SDCB2BPINViewResponseB2BPinViewTable_QNAME, ArrayOfSDCB2BPINViewTable.class, SDCB2BPINViewResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINTableInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BPINTableInquiryResult", scope = GetB2BPINTableInquiryResponse.class)
    public JAXBElement<SDCB2BPINTableInquiryResponse> createGetB2BPINTableInquiryResponseGetB2BPINTableInquiryResult(SDCB2BPINTableInquiryResponse value) {
        return new JAXBElement<SDCB2BPINTableInquiryResponse>(_GetB2BPINTableInquiryResponseGetB2BPINTableInquiryResult_QNAME, SDCB2BPINTableInquiryResponse.class, GetB2BPINTableInquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCommerceProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ExtensionProperties", scope = CommerceEntity.class)
    public JAXBElement<ArrayOfCommerceProperty> createCommerceEntityExtensionProperties(ArrayOfCommerceProperty value) {
        return new JAXBElement<ArrayOfCommerceProperty>(_CommerceEntityExtensionProperties_QNAME, ArrayOfCommerceProperty.class, CommerceEntity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrMessage", scope = SDCSaveWOTOrderResponse.class)
    public JAXBElement<String> createSDCSaveWOTOrderResponseErrMessage(String value) {
        return new JAXBElement<String>(_SDCSaveWOTOrderResponseErrMessage_QNAME, String.class, SDCSaveWOTOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "GeneratedPINId", scope = SDCSaveWOTOrderResponse.class)
    public JAXBElement<String> createSDCSaveWOTOrderResponseGeneratedPINId(String value) {
        return new JAXBElement<String>(_SDCSaveWOTOrderResponseGeneratedPINId_QNAME, String.class, SDCSaveWOTOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Order", scope = SDCSaveWOTOrderResponse.class)
    public JAXBElement<SalesOrder> createSDCSaveWOTOrderResponseOrder(SalesOrder value) {
        return new JAXBElement<SalesOrder>(_SDCSaveWOTOrderResponseOrder_QNAME, SalesOrder.class, SDCSaveWOTOrderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "AltText", scope = ImageInfo.class)
    public JAXBElement<String> createImageInfoAltText(String value) {
        return new JAXBElement<String>(_ImageInfoAltText_QNAME, String.class, ImageInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Url", scope = ImageInfo.class)
    public JAXBElement<String> createImageInfoUrl(String value) {
        return new JAXBElement<String>(_ImageInfoUrl_QNAME, String.class, ImageInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxId", scope = TaxViewLine.class)
    public JAXBElement<String> createTaxViewLineTaxId(String value) {
        return new JAXBElement<String>(_TaxViewLineTaxId_QNAME, String.class, TaxViewLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StoreNumber", scope = IncomeExpenseLine.class)
    public JAXBElement<String> createIncomeExpenseLineStoreNumber(String value) {
        return new JAXBElement<String>(_CartLineStoreNumber_QNAME, String.class, IncomeExpenseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Terminal", scope = IncomeExpenseLine.class)
    public JAXBElement<String> createIncomeExpenseLineTerminal(String value) {
        return new JAXBElement<String>(_IncomeExpenseLineTerminal_QNAME, String.class, IncomeExpenseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "IncomeExpenseAccount", scope = IncomeExpenseLine.class)
    public JAXBElement<String> createIncomeExpenseLineIncomeExpenseAccount(String value) {
        return new JAXBElement<String>(_IncomeExpenseLineIncomeExpenseAccount_QNAME, String.class, IncomeExpenseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Shift", scope = IncomeExpenseLine.class)
    public JAXBElement<String> createIncomeExpenseLineShift(String value) {
        return new JAXBElement<String>(_IncomeExpenseLineShift_QNAME, String.class, IncomeExpenseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Id", scope = Cart.class)
    public JAXBElement<String> createCartId(String value) {
        return new JAXBElement<String>(_CartId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DeliveryMode", scope = Cart.class)
    public JAXBElement<String> createCartDeliveryMode(String value) {
        return new JAXBElement<String>(_CartLineDeliveryMode_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "RequestedDeliveryDate", scope = Cart.class)
    public JAXBElement<DateTimeOffset> createCartRequestedDeliveryDate(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CartLineRequestedDeliveryDate_QNAME, DateTimeOffset.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "OverriddenDepositAmount", scope = Cart.class)
    public JAXBElement<BigDecimal> createCartOverriddenDepositAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartOverriddenDepositAmount_QNAME, BigDecimal.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTaxViewLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxViewLines", scope = Cart.class)
    public JAXBElement<ArrayOfTaxViewLine> createCartTaxViewLines(ArrayOfTaxViewLine value) {
        return new JAXBElement<ArrayOfTaxViewLine>(_CartTaxViewLines_QNAME, ArrayOfTaxViewLine.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "InvoiceComment", scope = Cart.class)
    public JAXBElement<String> createCartInvoiceComment(String value) {
        return new JAXBElement<String>(_CartInvoiceComment_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAttributeValueBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "AttributeValues", scope = Cart.class)
    public JAXBElement<ArrayOfAttributeValueBase> createCartAttributeValues(ArrayOfAttributeValueBase value) {
        return new JAXBElement<ArrayOfAttributeValueBase>(_CartAttributeValues_QNAME, ArrayOfAttributeValueBase.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShippingAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ShippingAddress", scope = Cart.class)
    public JAXBElement<ShippingAddress> createCartShippingAddress(ShippingAddress value) {
        return new JAXBElement<ShippingAddress>(_CartLineShippingAddress_QNAME, ShippingAddress.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTenderLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TenderLines", scope = Cart.class)
    public JAXBElement<ArrayOfTenderLine> createCartTenderLines(ArrayOfTenderLine value) {
        return new JAXBElement<ArrayOfTenderLine>(_CartTenderLines_QNAME, ArrayOfTenderLine.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CancellationChargeAmount", scope = Cart.class)
    public JAXBElement<BigDecimal> createCartCancellationChargeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartCancellationChargeAmount_QNAME, BigDecimal.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CustomerId", scope = Cart.class)
    public JAXBElement<String> createCartCustomerId(String value) {
        return new JAXBElement<String>(_TenderLineBaseCustomerId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "SalesId", scope = Cart.class)
    public JAXBElement<String> createCartSalesId(String value) {
        return new JAXBElement<String>(_CartSalesId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "WarehouseId", scope = Cart.class)
    public JAXBElement<String> createCartWarehouseId(String value) {
        return new JAXBElement<String>(_CartLineWarehouseId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReceiptEmail", scope = Cart.class)
    public JAXBElement<String> createCartReceiptEmail(String value) {
        return new JAXBElement<String>(_CartReceiptEmail_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "QuotationExpiryDate", scope = Cart.class)
    public JAXBElement<DateTimeOffset> createCartQuotationExpiryDate(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CartQuotationExpiryDate_QNAME, DateTimeOffset.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ReasonCodeLines", scope = Cart.class)
    public JAXBElement<ArrayOfReasonCodeLine> createCartReasonCodeLines(ArrayOfReasonCodeLine value) {
        return new JAXBElement<ArrayOfReasonCodeLine>(_TenderLineBaseReasonCodeLines_QNAME, ArrayOfReasonCodeLine.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxOverrideCode", scope = Cart.class)
    public JAXBElement<String> createCartTaxOverrideCode(String value) {
        return new JAXBElement<String>(_CartLineTaxOverrideCode_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DeliveryModeChargeAmount", scope = Cart.class)
    public JAXBElement<BigDecimal> createCartDeliveryModeChargeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartLineDeliveryModeChargeAmount_QNAME, BigDecimal.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DiscountCodes", scope = Cart.class)
    public JAXBElement<ArrayOfstring> createCartDiscountCodes(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_CartDiscountCodes_QNAME, ArrayOfstring.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "LoyaltyCardId", scope = Cart.class)
    public JAXBElement<String> createCartLoyaltyCardId(String value) {
        return new JAXBElement<String>(_TenderLineBaseLoyaltyCardId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Comment", scope = Cart.class)
    public JAXBElement<String> createCartComment(String value) {
        return new JAXBElement<String>(_CartLineComment_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StaffId", scope = Cart.class)
    public JAXBElement<String> createCartStaffId(String value) {
        return new JAXBElement<String>(_CartStaffId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAffiliationLoyaltyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "AffiliationLines", scope = Cart.class)
    public JAXBElement<ArrayOfAffiliationLoyaltyTier> createCartAffiliationLines(ArrayOfAffiliationLoyaltyTier value) {
        return new JAXBElement<ArrayOfAffiliationLoyaltyTier>(_CartAffiliationLines_QNAME, ArrayOfAffiliationLoyaltyTier.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "BeginDateTime", scope = Cart.class)
    public JAXBElement<DateTimeOffset> createCartBeginDateTime(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CartBeginDateTime_QNAME, DateTimeOffset.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TerminalId", scope = Cart.class)
    public JAXBElement<String> createCartTerminalId(String value) {
        return new JAXBElement<String>(_CartTerminalId_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "EstimatedShippingAmount", scope = Cart.class)
    public JAXBElement<BigDecimal> createCartEstimatedShippingAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CartEstimatedShippingAmount_QNAME, BigDecimal.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIncomeExpenseLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "IncomeExpenseLines", scope = Cart.class)
    public JAXBElement<ArrayOfIncomeExpenseLine> createCartIncomeExpenseLines(ArrayOfIncomeExpenseLine value) {
        return new JAXBElement<ArrayOfIncomeExpenseLine>(_CartIncomeExpenseLines_QNAME, ArrayOfIncomeExpenseLine.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "PromotionLines", scope = Cart.class)
    public JAXBElement<ArrayOfstring> createCartPromotionLines(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_CartLinePromotionLines_QNAME, ArrayOfstring.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChargeLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ChargeLines", scope = Cart.class)
    public JAXBElement<ArrayOfChargeLine> createCartChargeLines(ArrayOfChargeLine value) {
        return new JAXBElement<ArrayOfChargeLine>(_CartLineChargeLines_QNAME, ArrayOfChargeLine.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "OrderNumber", scope = Cart.class)
    public JAXBElement<String> createCartOrderNumber(String value) {
        return new JAXBElement<String>(_CartOrderNumber_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Name", scope = Cart.class)
    public JAXBElement<String> createCartName(String value) {
        return new JAXBElement<String>(_AttributeValueBaseName_QNAME, String.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCartLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CartLines", scope = Cart.class)
    public JAXBElement<ArrayOfCartLine> createCartCartLines(ArrayOfCartLine value) {
        return new JAXBElement<ArrayOfCartLine>(_CartCartLines_QNAME, ArrayOfCartLine.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "ModifiedDateTime", scope = Cart.class)
    public JAXBElement<DateTimeOffset> createCartModifiedDateTime(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CartModifiedDateTime_QNAME, DateTimeOffset.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "BusinessDate", scope = Cart.class)
    public JAXBElement<DateTimeOffset> createCartBusinessDate(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_CartBusinessDate_QNAME, DateTimeOffset.class, Cart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCOnLineParametersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetOnLineParametersResult", scope = GetOnLineParametersResponse.class)
    public JAXBElement<SDCOnLineParametersResponse> createGetOnLineParametersResponseGetOnLineParametersResult(SDCOnLineParametersResponse value) {
        return new JAXBElement<SDCOnLineParametersResponse>(_GetOnLineParametersResponseGetOnLineParametersResult_QNAME, SDCOnLineParametersResponse.class, GetOnLineParametersResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "cartId", scope = SaveWOTOrder.class)
    public JAXBElement<String> createSaveWOTOrderCartId(String value) {
        return new JAXBElement<String>(_SaveWOTOrderCartId_QNAME, String.class, SaveWOTOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CustomerAccountId", scope = SaveWOTOrder.class)
    public JAXBElement<String> createSaveWOTOrderCustomerAccountId(String value) {
        return new JAXBElement<String>(_SaveWOTOrderCustomerAccountId_QNAME, String.class, SaveWOTOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "receiptEmail", scope = SaveWOTOrder.class)
    public JAXBElement<String> createSaveWOTOrderReceiptEmail(String value) {
        return new JAXBElement<String>(_SaveWOTOrderReceiptEmail_QNAME, String.class, SaveWOTOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "onlineSalesPool", scope = SaveWOTOrder.class)
    public JAXBElement<String> createSaveWOTOrderOnlineSalesPool(String value) {
        return new JAXBElement<String>(_SaveWOTOrderOnlineSalesPool_QNAME, String.class, SaveWOTOrder.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Comment", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemComment(String value) {
        return new JAXBElement<String>(_TransactionItemComment_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "LineId", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemLineId(String value) {
        return new JAXBElement<String>(_TransactionItemLineId_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Color", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemColor(String value) {
        return new JAXBElement<String>(_TransactionItemColor_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PriceWithCurrency", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemPriceWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionItemPriceWithCurrency_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransactionItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "KitComponents", scope = TransactionItem.class)
    public JAXBElement<ArrayOfTransactionItem> createTransactionItemKitComponents(ArrayOfTransactionItem value) {
        return new JAXBElement<ArrayOfTransactionItem>(_TransactionItemKitComponents_QNAME, ArrayOfTransactionItem.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductNumber", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemProductNumber(String value) {
        return new JAXBElement<String>(_TransactionItemProductNumber_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "NetAmountWithCurrency", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemNetAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionItemNetAmountWithCurrency_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DeliveryModeId", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemDeliveryModeId(String value) {
        return new JAXBElement<String>(_TransactionDeliveryModeId_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductUrl", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemProductUrl(String value) {
        return new JAXBElement<String>(_TransactionItemProductUrl_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DiscountAmountWithCurrency", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemDiscountAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionDiscountAmountWithCurrency_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ShippingAddress", scope = TransactionItem.class)
    public JAXBElement<Address> createTransactionItemShippingAddress(Address value) {
        return new JAXBElement<Address>(_TransactionShippingAddress_QNAME, Address.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DeliveryModeDescription", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemDeliveryModeDescription(String value) {
        return new JAXBElement<String>(_TransactionDeliveryModeDescription_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Description", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemDescription(String value) {
        return new JAXBElement<String>(_TransactionItemDescription_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TaxAmountWithCurrency", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemTaxAmountWithCurrency(String value) {
        return new JAXBElement<String>(_TransactionTaxAmountWithCurrency_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Configuration", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemConfiguration(String value) {
        return new JAXBElement<String>(_TransactionItemConfiguration_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemProductName(String value) {
        return new JAXBElement<String>(_TransactionItemProductName_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Size", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemSize(String value) {
        return new JAXBElement<String>(_TransactionItemSize_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemItemId(String value) {
        return new JAXBElement<String>(_TransactionItemItemId_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImageInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Image", scope = TransactionItem.class)
    public JAXBElement<ImageInfo> createTransactionItemImage(ImageInfo value) {
        return new JAXBElement<ImageInfo>(_TransactionItemImage_QNAME, ImageInfo.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PromotionLines", scope = TransactionItem.class)
    public JAXBElement<ArrayOfstring> createTransactionItemPromotionLines(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_TransactionItemPromotionLines_QNAME, ArrayOfstring.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OfferNames", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemOfferNames(String value) {
        return new JAXBElement<String>(_TransactionItemOfferNames_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ElectronicDeliveryEmail", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemElectronicDeliveryEmail(String value) {
        return new JAXBElement<String>(_TransactionItemElectronicDeliveryEmail_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ImageData", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemImageData(String value) {
        return new JAXBElement<String>(_TransactionItemImageData_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductDetails", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemProductDetails(String value) {
        return new JAXBElement<String>(_TransactionItemProductDetails_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "VariantInventoryDimensionId", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemVariantInventoryDimensionId(String value) {
        return new JAXBElement<String>(_TransactionItemVariantInventoryDimensionId_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Style", scope = TransactionItem.class)
    public JAXBElement<String> createTransactionItemStyle(String value) {
        return new JAXBElement<String>(_TransactionItemStyle_QNAME, String.class, TransactionItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPINRedemptionTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "pinRedemptionTable", scope = SDCGetBlockPINRedemptionResponse.class)
    public JAXBElement<ArrayOfSDCPINRedemptionTable> createSDCGetBlockPINRedemptionResponsePinRedemptionTable(ArrayOfSDCPINRedemptionTable value) {
        return new JAXBElement<ArrayOfSDCPINRedemptionTable>(_SDCGetBlockPINRedemptionResponsePinRedemptionTable_QNAME, ArrayOfSDCPINRedemptionTable.class, SDCGetBlockPINRedemptionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "b2bPinViewTableInquiry", scope = SDCB2BPINTableInquiryResponse.class)
    public JAXBElement<SDCB2BPINViewTable> createSDCB2BPINTableInquiryResponseB2BPinViewTableInquiry(SDCB2BPINViewTable value) {
        return new JAXBElement<SDCB2BPINViewTable>(_SDCB2BPINTableInquiryResponseB2BPinViewTableInquiry_QNAME, SDCB2BPINViewTable.class, SDCB2BPINTableInquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINViewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetB2BPINViewResult", scope = GetB2BPINViewResponse.class)
    public JAXBElement<SDCB2BPINViewResponse> createGetB2BPINViewResponseGetB2BPINViewResult(SDCB2BPINViewResponse value) {
        return new JAXBElement<SDCB2BPINViewResponse>(_GetB2BPINViewResponseGetB2BPINViewResult_QNAME, SDCB2BPINViewResponse.class, GetB2BPINViewResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "returnMessage", scope = SDCB2BPINUpdateCancelTable.class)
    public JAXBElement<String> createSDCB2BPINUpdateCancelTableReturnMessage(String value) {
        return new JAXBElement<String>(_SDCB2BUpdatePINStatusTableReturnMessage_QNAME, String.class, SDCB2BPINUpdateCancelTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateCancelTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "table", scope = SDCB2BPINUpdateResponse.class)
    public JAXBElement<SDCB2BPINUpdateCancelTable> createSDCB2BPINUpdateResponseTable(SDCB2BPINUpdateCancelTable value) {
        return new JAXBElement<SDCB2BPINUpdateCancelTable>(_SDCB2BPINCancelResponseTable_QNAME, SDCB2BPINUpdateCancelTable.class, SDCB2BPINUpdateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCB2BUpdatePINStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "B2BUpdatePINStatusResult", scope = B2BUpdatePINStatusResponse.class)
    public JAXBElement<SDCB2BUpdatePINStatusResponse> createB2BUpdatePINStatusResponseB2BUpdatePINStatusResult(SDCB2BUpdatePINStatusResponse value) {
        return new JAXBElement<SDCB2BUpdatePINStatusResponse>(_B2BUpdatePINStatusResponseB2BUpdatePINStatusResult_QNAME, SDCB2BUpdatePINStatusResponse.class, B2BUpdatePINStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateTimeOffset }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TenderDate", scope = TenderLine.class)
    public JAXBElement<DateTimeOffset> createTenderLineTenderDate(DateTimeOffset value) {
        return new JAXBElement<DateTimeOffset>(_TenderLineTenderDate_QNAME, DateTimeOffset.class, TenderLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "MaskedCardNumber", scope = TenderLine.class)
    public JAXBElement<String> createTenderLineMaskedCardNumber(String value) {
        return new JAXBElement<String>(_TenderLineMaskedCardNumber_QNAME, String.class, TenderLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "Authorization", scope = TenderLine.class)
    public JAXBElement<String> createTenderLineAuthorization(String value) {
        return new JAXBElement<String>(_TenderLineAuthorization_QNAME, String.class, TenderLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CardToken", scope = TenderLine.class)
    public JAXBElement<String> createTenderLineCardToken(String value) {
        return new JAXBElement<String>(_TenderLineCardToken_QNAME, String.class, TenderLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpdateCartExtensionFieldsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "UpdateCartExtensionFieldsResult", scope = UpdateCartExtensionFieldsResponse.class)
    public JAXBElement<SDCUpdateCartExtensionFieldsResponse> createUpdateCartExtensionFieldsResponseUpdateCartExtensionFieldsResult(SDCUpdateCartExtensionFieldsResponse value) {
        return new JAXBElement<SDCUpdateCartExtensionFieldsResponse>(_UpdateCartExtensionFieldsResponseUpdateCartExtensionFieldsResult_QNAME, SDCUpdateCartExtensionFieldsResponse.class, UpdateCartExtensionFieldsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "pinCode", scope = GetB2BPINTableInquiry.class)
    public JAXBElement<String> createGetB2BPINTableInquiryPinCode(String value) {
        return new JAXBElement<String>(_GetPINRedemptionPinCode_QNAME, String.class, GetB2BPINTableInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCB2BPINViewLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "b2bPinViewLineTable", scope = SDCB2BPINViewLineResponse.class)
    public JAXBElement<ArrayOfSDCB2BPINViewLineTable> createSDCB2BPINViewLineResponseB2BPinViewLineTable(ArrayOfSDCB2BPINViewLineTable value) {
        return new JAXBElement<ArrayOfSDCB2BPINViewLineTable>(_SDCB2BPINViewLineResponseB2BPinViewLineTable_QNAME, ArrayOfSDCB2BPINViewLineTable.class, SDCB2BPINViewLineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "errorMessage", scope = SDCUpdateCartExtensionFieldsResponse.class)
    public JAXBElement<String> createSDCUpdateCartExtensionFieldsResponseErrorMessage(String value) {
        return new JAXBElement<String>(_SDCUpdateCartExtensionFieldsResponseErrorMessage_QNAME, String.class, SDCUpdateCartExtensionFieldsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TerminalId", scope = TaxLine.class)
    public JAXBElement<String> createTaxLineTerminalId(String value) {
        return new JAXBElement<String>(_CartTerminalId_QNAME, String.class, TaxLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TransactionId", scope = TaxLine.class)
    public JAXBElement<String> createTaxLineTransactionId(String value) {
        return new JAXBElement<String>(_ChargeLineTransactionId_QNAME, String.class, TaxLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "DataAreaId", scope = TaxLine.class)
    public JAXBElement<String> createTaxLineDataAreaId(String value) {
        return new JAXBElement<String>(_TaxLineDataAreaId_QNAME, String.class, TaxLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxGroup", scope = TaxLine.class)
    public JAXBElement<String> createTaxLineTaxGroup(String value) {
        return new JAXBElement<String>(_ShippingAddressTaxGroup_QNAME, String.class, TaxLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "StoreId", scope = TaxLine.class)
    public JAXBElement<String> createTaxLineStoreId(String value) {
        return new JAXBElement<String>(_TaxLineStoreId_QNAME, String.class, TaxLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "TaxCode", scope = TaxLine.class)
    public JAXBElement<String> createTaxLineTaxCode(String value) {
        return new JAXBElement<String>(_TaxLineTaxCode_QNAME, String.class, TaxLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "channelRefId", scope = UpdateCartExtensionFields.class)
    public JAXBElement<String> createUpdateCartExtensionFieldsChannelRefId(String value) {
        return new JAXBElement<String>(_UpdateCartExtensionFieldsChannelRefId_QNAME, String.class, UpdateCartExtensionFields.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "cart", scope = UpdateCartExtensionFields.class)
    public JAXBElement<Cart> createUpdateCartExtensionFieldsCart(Cart value) {
        return new JAXBElement<Cart>(_UpdateCartExtensionFieldsCart_QNAME, Cart.class, UpdateCartExtensionFields.class, value);
    }

}
