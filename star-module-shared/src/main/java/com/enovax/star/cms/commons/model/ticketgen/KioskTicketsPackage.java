package com.enovax.star.cms.commons.model.ticketgen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class KioskTicketsPackage implements Serializable {

    private List<TicketXmlRecord> ticketXmls = new ArrayList<>();
    private String receiptXml;

    public List<TicketXmlRecord> getTicketXmls() {
        return ticketXmls;
    }

    public void setTicketXmls(List<TicketXmlRecord> ticketXmls) {
        this.ticketXmls = ticketXmls;
    }

    public String getReceiptXml() {
        return receiptXml;
    }

    public void setReceiptXml(String receiptXml) {
        this.receiptXml = receiptXml;
    }
}
