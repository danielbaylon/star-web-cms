package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketPINTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;

/**
 * 
 * @author Justin
 *
 */
public class KioskCartCheckoutCompleteResponse {

    private List<AxCartRetailTicketPINTableEntity> axCartRetailTicketPINTableEntityList = new ArrayList<>();

    private List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList = new ArrayList<>();

    private String errorStatus;

    public List<AxCartRetailTicketPINTableEntity> getAxCartRetailTicketPINTableEntityList() {
        return axCartRetailTicketPINTableEntityList;
    }

    public void setAxCartRetailTicketPINTableEntityList(List<AxCartRetailTicketPINTableEntity> axCartRetailTicketPINTableEntityList) {
        this.axCartRetailTicketPINTableEntityList = axCartRetailTicketPINTableEntityList;
    }

    public List<AxCartRetailTicketTableEntity> getAxCartRetailTicketTableEntityList() {
        return axCartRetailTicketTableEntityList;
    }

    public void setAxCartRetailTicketTableEntityList(List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntityList) {
        this.axCartRetailTicketTableEntityList = axCartRetailTicketTableEntityList;
    }

    public String getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.errorStatus = errorStatus;
    }

}
