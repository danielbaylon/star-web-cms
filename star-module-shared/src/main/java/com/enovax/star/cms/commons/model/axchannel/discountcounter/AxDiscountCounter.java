package com.enovax.star.cms.commons.model.axchannel.discountcounter;

/**
 * Created by jensen on 27/9/16.
 */
public class AxDiscountCounter {

    private String offerId;
    private Integer maxUse;
    private Integer quantity;

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public Integer getMaxUse() {
        return maxUse;
    }

    public void setMaxUse(Integer maxUse) {
        this.maxUse = maxUse;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
