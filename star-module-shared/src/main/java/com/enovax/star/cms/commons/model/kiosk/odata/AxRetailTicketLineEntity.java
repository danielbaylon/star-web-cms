package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * SDC_TicketingExtension.Entity.RetailTicketLineEntity
 * 
 * @author Justin
 *
 */
public class AxRetailTicketLineEntity {

    private String sourceRecId;

    private String itemId;

    private String lineNum;

    private String lineId;

    private String description;

    public String getSourceRecId() {
        return sourceRecId;
    }

    public void setSourceRecId(String sourceRecId) {
        this.sourceRecId = sourceRecId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getLineNum() {
        return lineNum;
    }

    public void setLineNum(String lineNum) {
        this.lineNum = lineNum;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    

}
