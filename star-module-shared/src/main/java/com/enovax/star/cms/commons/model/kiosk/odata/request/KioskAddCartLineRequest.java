package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxAddCartLine;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskAddCartLineRequest {

    @JsonIgnore
    private String cartId;

    @JsonProperty("cartLines")
    private List<AxAddCartLine> axCartLineList;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public List<AxAddCartLine> getAxCartLineList() {
        return axCartLineList;
    }

    public void setAxCartLineList(List<AxAddCartLine> axCartLineList) {
        this.axCartLineList = axCartLineList;
    }

}
