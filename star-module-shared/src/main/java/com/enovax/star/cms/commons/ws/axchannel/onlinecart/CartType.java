
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CartType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CartType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Shopping"/>
 *     &lt;enumeration value="Checkout"/>
 *     &lt;enumeration value="CustomerOrder"/>
 *     &lt;enumeration value="IncomeExpense"/>
 *     &lt;enumeration value="AccountDeposit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CartType")
@XmlEnum
public enum CartType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Shopping")
    SHOPPING("Shopping"),
    @XmlEnumValue("Checkout")
    CHECKOUT("Checkout"),
    @XmlEnumValue("CustomerOrder")
    CUSTOMER_ORDER("CustomerOrder"),
    @XmlEnumValue("IncomeExpense")
    INCOME_EXPENSE("IncomeExpense"),
    @XmlEnumValue("AccountDeposit")
    ACCOUNT_DEPOSIT("AccountDeposit");
    private final String value;

    CartType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CartType fromValue(String v) {
        for (CartType c: CartType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
