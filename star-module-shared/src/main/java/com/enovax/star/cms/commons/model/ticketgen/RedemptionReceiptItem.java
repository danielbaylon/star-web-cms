package com.enovax.star.cms.commons.model.ticketgen;

public class RedemptionReceiptItem extends ReceiptItem {

	private String originalQty;
	private String redeemedQty;
	private String remainingQty;
	
	public String getOriginalQty() {
		return originalQty;
	}

	public void setOriginalQty(String originalQty) {
		this.originalQty = originalQty;
	}

	public String getRedeemedQty() {
		return redeemedQty;
	}
	
	public void setRedeemedQty(String redeemedQty) {
		this.redeemedQty = redeemedQty;
	}
	
	public String getRemainingQty() {
		return remainingQty;
	}
	
	public void setRemainingQty(String remainingQty) {
		this.remainingQty = remainingQty;
	}

}
