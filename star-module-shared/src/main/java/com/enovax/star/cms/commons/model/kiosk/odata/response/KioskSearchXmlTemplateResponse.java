package com.enovax.star.cms.commons.model.kiosk.odata.response;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxTemplateXMLEntityResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskSearchXmlTemplateResponse {

	@JsonProperty("value")
	private List<AxTemplateXMLEntityResponse> entityResponse;

	public List<AxTemplateXMLEntityResponse> getEntityResponse() {
		return entityResponse;
	}

	public void setEntityResponse(List<AxTemplateXMLEntityResponse> entityResponse) {
		this.entityResponse = entityResponse;
	}

}
