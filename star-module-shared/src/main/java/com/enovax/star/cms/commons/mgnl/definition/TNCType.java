package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 7/18/16.
 */
public enum TNCType {
    General,
    Product
}
