package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="PPSLMAuditTrail")
public class PPSLMAuditTrail implements Serializable {
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
	
	@Column(name = "action", nullable = false)
    private String action;
	
	@Column(name = "details", nullable = false)
    private String details;
	
	@Column(name = "relatedEntities")
	private String relatedEntities;
	
	@Column(name = "relatedEntityKeys")
    private String relatedEntityKeys;
    
	@Column(name = "performedBy", nullable = false)
    private String performedBy;
	
	@Column(name = "createdDate", nullable = false)
    private Date createdDate;
    
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

    
    public String getDetails() {
        return details;
    }
    public void setDetails(String details) {
        this.details = details;
    }

    
    public String getRelatedEntities() {
        return relatedEntities;
    }
    public void setRelatedEntities(String relatedEntities) {
        this.relatedEntities = relatedEntities;
    }

    
    public String getRelatedEntityKeys() {
        return relatedEntityKeys;
    }
    public void setRelatedEntityKeys(String relatedEntityKeys) {
        this.relatedEntityKeys = relatedEntityKeys;
    }

    
    public String getPerformedBy() {
        return performedBy;
    }
    public void setPerformedBy(String performedBy) {
        this.performedBy = performedBy;
    }
    
    
    public Date getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
