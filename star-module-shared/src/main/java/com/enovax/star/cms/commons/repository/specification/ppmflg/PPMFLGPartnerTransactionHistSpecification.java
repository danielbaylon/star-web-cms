package com.enovax.star.cms.commons.repository.specification.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPartnerTransactionHist;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 21/8/16.
 */
public class PPMFLGPartnerTransactionHistSpecification extends BaseSpecification<PPMFLGPartnerTransactionHist> {
}
