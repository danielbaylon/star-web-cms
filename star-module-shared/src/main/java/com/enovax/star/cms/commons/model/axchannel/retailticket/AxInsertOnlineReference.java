package com.enovax.star.cms.commons.model.axchannel.retailticket;

import java.util.Date;

/**
 * Created by jennylynsze on 1/10/17.
 */
public class AxInsertOnlineReference {
    private String transactionId;
    private Date transDate;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }
}
