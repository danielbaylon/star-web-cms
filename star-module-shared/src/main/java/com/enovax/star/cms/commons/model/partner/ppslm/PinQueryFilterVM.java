package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.text.ParseException;
import java.util.Date;

public class PinQueryFilterVM {
    private String fromDtStr;
    private String toDtStr;
    private String receiptNum;
    private String orgName;
    private String accountCode;
    private String pinCode;
    private Date fromDate;
    private Date toDate;
    private Integer[] paIds;
    private String ticketMediaType;
    
    public String getFromDtStr() {
        return fromDtStr;
    }

    public void setFromDtStr(String fromDtStr) {
        this.fromDtStr = fromDtStr;
    }

    public String getToDtStr() {
        return toDtStr;
    }

    public void setToDtStr(String toDtStr) {
        this.toDtStr = toDtStr;
    }

    public String getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Date getFromDate() {
        if (this.fromDtStr != null && !"".equals(fromDtStr)) {
            try {
                fromDate = NvxDateUtils.parseDate(fromDtStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        if (toDtStr != null && !"".equals(toDtStr)) {
            try {
                toDate = NvxDateUtils.parseDate(toDtStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                toDate = TransactionUtil.addDaysToDate(toDate, 1);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer[] getPaIds() {
        return paIds;
    }

    public void setPaIds(Integer[] paIds) {
        this.paIds = paIds;
    }

    public String getTicketMediaType() {
        return ticketMediaType;
    }

    public void setTicketMediaType(String ticketMediaType) {
        this.ticketMediaType = ticketMediaType;
    }
}
