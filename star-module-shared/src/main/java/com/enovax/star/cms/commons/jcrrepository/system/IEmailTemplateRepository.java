package com.enovax.star.cms.commons.jcrrepository.system;


import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;

public interface IEmailTemplateRepository {

    public SysEmailTemplateVM getSystemParamByKey(String appKey, String paramKey);

    boolean hasSystemParamByKey(String appKey, String paramKey);
}
