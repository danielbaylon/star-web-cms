package com.enovax.star.cms.commons.model.partner.ppslm;

import java.util.List;

/**
 * Created by lavanya on 7/11/16.
 */
public class TransactionReportInitVM {
    private List<PartnerVM> allPartnerVMs;
    private int recsPerPage;
    private List<FilterProductTopLevelVM> itemsList;

    public TransactionReportInitVM() {
    }

    public TransactionReportInitVM(List<PartnerVM> allPartnerVMs, int recsPerPage, List<FilterProductTopLevelVM> itemsList) {
        this.allPartnerVMs = allPartnerVMs;
        this.recsPerPage = recsPerPage;
        this.itemsList = itemsList;
    }

    public List<PartnerVM> getAllPartnerVMs() {
        return allPartnerVMs;
    }

    public void setAllPartnerVMs(List<PartnerVM> allPartnerVMs) {
        this.allPartnerVMs = allPartnerVMs;
    }

    public int getRecsPerPage() {
        return recsPerPage;
    }

    public void setRecsPerPage(int recsPerPage) {
        this.recsPerPage = recsPerPage;
    }

    public List<FilterProductTopLevelVM> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<FilterProductTopLevelVM> itemsList) {
        this.itemsList = itemsList;
    }
}
