
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ArrayOfSDC_UseDiscountCounterTable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_UseDiscountCounterTable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_UseDiscountCounterTable" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_UseDiscountCounterTable" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_UseDiscountCounterTable", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", propOrder = {
    "sdcUseDiscountCounterTable"
})
public class ArrayOfSDCUseDiscountCounterTable {

    @XmlElement(name = "SDC_UseDiscountCounterTable", nillable = true)
    protected List<SDCUseDiscountCounterTable> sdcUseDiscountCounterTable;

    /**
     * Gets the value of the sdcUseDiscountCounterTable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcUseDiscountCounterTable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCUseDiscountCounterTable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCUseDiscountCounterTable }
     * 
     * 
     */
    public List<SDCUseDiscountCounterTable> getSDCUseDiscountCounterTable() {
        if (sdcUseDiscountCounterTable == null) {
            sdcUseDiscountCounterTable = new ArrayList<SDCUseDiscountCounterTable>();
        }
        return this.sdcUseDiscountCounterTable;
    }

}
