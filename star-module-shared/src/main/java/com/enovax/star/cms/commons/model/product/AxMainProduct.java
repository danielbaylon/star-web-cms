package com.enovax.star.cms.commons.model.product;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jennylynsze on 7/20/16.
 */
public class AxMainProduct {
    private Long recordId; //listingId
    private String itemId; //productCode
    private String productName;
    private String description;
    private BigDecimal price;
    private String searchName;
    private String displayNumber;
    private String mediaTypeDescription;
    private String mediaTypeId;
    private boolean isCapacity;
    private boolean ownAttraction;
    private String templateName;
    private int printing;
    private int noOfPax;
    private boolean isPackage;
    private Date validFrom;
    private Date validTo;
    private String ticketType;
    private BigDecimal publishedPrice;


    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }

    public String getDisplayNumber() {
        return displayNumber;
    }

    public void setDisplayNumber(String displayNumber) {
        this.displayNumber = displayNumber;
    }

    public String getMediaTypeDescription() {
        return mediaTypeDescription;
    }

    public void setMediaTypeDescription(String mediaTypeDescription) {
        this.mediaTypeDescription = mediaTypeDescription;
    }

    public String getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public boolean isCapacity() {
        return isCapacity;
    }

    public void setIsCapacity(boolean isCapacity) {
        this.isCapacity = isCapacity;
    }

    public boolean isOwnAttraction() {
        return ownAttraction;
    }

    public void setOwnAttraction(boolean ownAttraction) {
        this.ownAttraction = ownAttraction;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public int getPrinting() {
        return printing;
    }

    public void setPrinting(int printing) {
        this.printing = printing;
    }

    public int getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(int noOfPax) {
        this.noOfPax = noOfPax;
    }

    public boolean isPackage() {
        return isPackage;
    }

    public void setIsPackage(boolean isPackage) {
        this.isPackage = isPackage;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public BigDecimal getPublishedPrice() {
        return publishedPrice;
    }

    public void setPublishedPrice(BigDecimal publishedPrice) {
        this.publishedPrice = publishedPrice;
    }
}
