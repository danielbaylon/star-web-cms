package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMTAMainAccountRepository extends JpaRepository<PPSLMTAMainAccount, Integer> {

    public List<PPSLMTAMainAccount> findByUsername(String username);

    public PPSLMTAMainAccount findFirstByUsername(String username);

    PPSLMTAMainAccount findById(Integer id);

    int countByIdAndStatus(Integer id, String name);
}
