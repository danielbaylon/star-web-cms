package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.util.Date;

/**
 * Created by houtao on 30/8/16.
 */
public class WOTVerifyProdQtyVM {

    private boolean isNewReservation;

    private String custAccountNo;
    private Date eventDate;
    private String eventGroupId;
    private String eventLineId;
    private String itemId;
    private Long productId;
    private Integer qty;
    /**
     * New Request : 0
     * Update existing Reservation : 1
     */
    private boolean isUpdateCapacity;
    private String lineId;
    private String transactionId;


    public static WOTVerifyProdQtyVM getNewReservationVerifyObject(WOTReservationVM reservation) throws Exception {
        WOTVerifyProdQtyVM vm = new WOTVerifyProdQtyVM();
        vm.setNewReservation(true);
        vm.setCustAccountNo(reservation.getAxCustomerNumber());
        if(reservation.getShowDate() != null && reservation.getShowDate().trim().length() > 0) {
            vm.setEventDate(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        vm.setEventGroupId(reservation.getEventGroupId());
        vm.setEventLineId(reservation.getEventLineId());
        vm.setItemId(reservation.getItemId());
        vm.setProductId(Long.parseLong(reservation.getProductId().trim()));
        vm.setQty(reservation.getQty());
        vm.setUpdateCapacity(false);
        return vm;
    }

    public static WOTVerifyProdQtyVM getUpdateReservationVerifyObject(WOTReservationVM reservation) throws Exception {
        WOTVerifyProdQtyVM vm = new WOTVerifyProdQtyVM();
        vm.setNewReservation(false);
        vm.setCustAccountNo(reservation.getAxCustomerNumber());
        if(reservation.getShowDate() != null && reservation.getShowDate().trim().length() > 0){
            vm.setEventDate(NvxDateUtils.parseDate(reservation.getShowDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY));
        }
        vm.setEventGroupId(reservation.getEventGroupId());
        vm.setEventLineId(reservation.getEventLineId());
        vm.setItemId(reservation.getItemId());
        vm.setProductId(Long.parseLong(reservation.getProductId().trim()));
        vm.setQty(reservation.getQty());
        vm.setUpdateCapacity(true);
        vm.setLineId(reservation.getLineId());
        vm.setTransactionId(reservation.getTransactionId());
        return vm;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }


    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public boolean isUpdateCapacity() {
        return isUpdateCapacity;
    }

    public void setUpdateCapacity(boolean updateCapacity) {
        isUpdateCapacity = updateCapacity;
    }

    public String getCustAccountNo() {
        return custAccountNo;
    }

    public void setCustAccountNo(String custAccountNo) {
        this.custAccountNo = custAccountNo;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    public boolean isNewReservation() {
        return isNewReservation;
    }

    public void setNewReservation(boolean newReservation) {
        isNewReservation = newReservation;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }
}
