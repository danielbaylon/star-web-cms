
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchXmlTemplateResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_TemplateXMLSearchResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchXmlTemplateResult"
})
@XmlRootElement(name = "SearchXmlTemplateResponse", namespace = "http://tempuri.org/")
public class SearchXmlTemplateResponse {

    @XmlElementRef(name = "SearchXmlTemplateResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCTemplateXMLSearchResponse> searchXmlTemplateResult;

    /**
     * Gets the value of the searchXmlTemplateResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCTemplateXMLSearchResponse }{@code >}
     *     
     */
    public JAXBElement<SDCTemplateXMLSearchResponse> getSearchXmlTemplateResult() {
        return searchXmlTemplateResult;
    }

    /**
     * Sets the value of the searchXmlTemplateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCTemplateXMLSearchResponse }{@code >}
     *     
     */
    public void setSearchXmlTemplateResult(JAXBElement<SDCTemplateXMLSearchResponse> value) {
        this.searchXmlTemplateResult = value;
    }

}
