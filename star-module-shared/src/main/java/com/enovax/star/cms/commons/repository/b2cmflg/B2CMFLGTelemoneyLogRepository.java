package com.enovax.star.cms.commons.repository.b2cmflg;

import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGTelemoneyLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jonathan on 7/12/16.
 */
public interface B2CMFLGTelemoneyLogRepository extends JpaRepository<B2CMFLGTelemoneyLog, Integer> {
}
