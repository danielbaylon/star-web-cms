package com.enovax.star.cms.commons.model.ticketgen;

public class ReceiptCompanyDetails {

    private String logo;
    
    private String companyName;
    
    private String address;
    
    private String postalCode;
    
    private String contactDetails;
    
    private String gstDetails;

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setContactDetails(String contactDetails) {
		this.contactDetails = contactDetails;
	}
	
	public String getContactDetails() {
		return contactDetails;
	}

	public String getGstDetails() {
		return gstDetails;
	}

	public void setGstDetails(String gstDetails) {
		this.gstDetails = gstDetails;
	}
    
    
}
