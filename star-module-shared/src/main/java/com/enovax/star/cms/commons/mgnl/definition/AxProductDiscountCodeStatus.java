package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jace on 9/9/16.
 */
public enum AxProductDiscountCodeStatus {
  Active,
  Inactive
}

