package com.enovax.star.cms.commons.model.partner.ppmflg;

/**
 * Created by houtao on 12/11/16.
 */
public class TicketMediaTypeVM {

    private String name;
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
