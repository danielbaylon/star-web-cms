package com.enovax.star.cms.commons.model.booking;

import java.math.BigDecimal;

public class ReceiptDisplay extends FunCartDisplay {

    private CustomerDetails customer;

    private String dateOfPurchase;
    private String receiptNumber;
    private String paymentType;
    private String paymentTypeLabel;

    private String transStatus;
    private boolean isError;
    private String errorCode;
    private String errorMessage;
    private String message;

    private BigDecimal totalTotal;
    private String totalTotalText;
    private String ccDigits;

    private boolean hasPinCode = false;

    private String pinCode = "";

    private String baseUrl = "";
    private String barcodeImage = "";

    public CustomerDetails getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDetails customer) {
        this.customer = customer;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeLabel() {
        return paymentTypeLabel;
    }

    public void setPaymentTypeLabel(String paymentTypeLabel) {
        this.paymentTypeLabel = paymentTypeLabel;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getTotalTotal() {
        return totalTotal;
    }

    public void setTotalTotal(BigDecimal totalTotal) {
        this.totalTotal = totalTotal;
    }

    public String getTotalTotalText() {
        return totalTotalText;
    }

    public void setTotalTotalText(String totalTotalText) {
        this.totalTotalText = totalTotalText;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBarcodeImage() {
        return barcodeImage;
    }

    public void setBarcodeImage(String barcodeImage) {
        this.barcodeImage = barcodeImage;
    }

    public boolean isHasPinCode() {
        return hasPinCode;
    }

    public void setHasPinCode(boolean hasPinCode) {
        this.hasPinCode = hasPinCode;
    }

    public String getCcDigits() {
        return ccDigits;
    }

    public void setCcDigits(String ccDigits) {
        this.ccDigits = ccDigits;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }
}
