
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCustomerTransactionDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomerTransactionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerTransactionDetails" type="{http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels}CustomerTransactionDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomerTransactionDetails", namespace = "http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels", propOrder = {
    "customerTransactionDetails"
})
public class ArrayOfCustomerTransactionDetails {

    @XmlElement(name = "CustomerTransactionDetails", nillable = true)
    protected List<CustomerTransactionDetails> customerTransactionDetails;

    /**
     * Gets the value of the customerTransactionDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerTransactionDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerTransactionDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerTransactionDetails }
     * 
     * 
     */
    public List<CustomerTransactionDetails> getCustomerTransactionDetails() {
        if (customerTransactionDetails == null) {
            customerTransactionDetails = new ArrayList<CustomerTransactionDetails>();
        }
        return this.customerTransactionDetails;
    }

}
