
package com.enovax.star.cms.commons.ws.axchannel.inventtable;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSDC_InventTableExt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSDC_InventTableExt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_InventTableExt" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SDC_InventTableExt" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSDC_InventTableExt", propOrder = {
    "sdcInventTableExt"
})
public class ArrayOfSDCInventTableExt {

    @XmlElement(name = "SDC_InventTableExt", nillable = true)
    protected List<SDCInventTableExt> sdcInventTableExt;

    /**
     * Gets the value of the sdcInventTableExt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sdcInventTableExt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSDCInventTableExt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SDCInventTableExt }
     * 
     * 
     */
    public List<SDCInventTableExt> getSDCInventTableExt() {
        if (sdcInventTableExt == null) {
            sdcInventTableExt = new ArrayList<SDCInventTableExt>();
        }
        return this.sdcInventTableExt;
    }

}
