package com.enovax.star.cms.commons.jcrrepository.system;

import com.enovax.payment.tm.constant.TmRequestType;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.exception.JcrRepositoryException;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.booking.TelemoneyParamPackage;
import com.enovax.star.cms.commons.util.JsonUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

@Service
public class DefaultJcrTmParamRepository implements ITmParamRepository {

    private static final Logger log = LoggerFactory.getLogger(DefaultJcrTmParamRepository.class);

    public static final String TM_PARAM_FOLDER = "payment-gateway";

    public static final String TM_PARAM_RETURN_URL = "tmReturnUrl";
    public static final String TM_PARAM_RETURN_URL_SKY_DINING = "tmReturnUrlSkyDining";
    public static final String TM_PARAM_STATUS_URL = "tmStatusUrl";
    public static final String TM_PARAM_TRANS_EMPTY_LOWER_BOUND_MILLIS = "transEmptyLowerBoundMillis";
    public static final String TM_PARAM_TRANS_EMPTY_UPPER_BOUND_MILLIS = "transEmptyUpperBoundMillis";
    public static final String TM_PARAM_TRANS_TIMEOUT_LOWER_BOUND_MILLIS = "transTimeoutLowerBoundMillis";
    public static final String TM_PARAM_TRANS_TIMEOUT_UPPER_BOUND_MILLIS = "transTimeoutUpperBoundMillis";

    public static final String TM_PARAM_DEPOSIT_TOPUP_RETURN_URL = "tmDepositTopupReturnUrl";

    //TODO Implement caching

    @Override
    public TelemoneyParamPackage getTmParams(StoreApiChannels channel) throws JcrRepositoryException {
        return getTmParams(channel, null);
    }

    @Override
    public TelemoneyParamPackage getTmParams(StoreApiChannels channel, TmRequestType requestType) throws JcrRepositoryException {
        if (channel == null) {
            throw new JcrRepositoryException("Channel passed cannot be null.");
        }

        final String rootPath = "/" + TM_PARAM_FOLDER + "/" + channel.code;
        final Node rootNode;
        try {

            rootNode = JcrRepository.getParentNode(JcrWorkspace.AppConfigs.getWorkspaceName(), rootPath);

            if (rootNode == null) {
                final String errMsg = "Telemoney parameter retrieval failed. Unable to find node at " + rootPath;
                log.error(errMsg);
                throw new JcrRepositoryException(errMsg);
            }

            final TelemoneyParamPackage pkg = new TelemoneyParamPackage();

            if (requestType == null) {
                pkg.setTmReturnUrl(rootNode.getProperty(TM_PARAM_RETURN_URL).getString());
            } else {
                String returnUrl = PropertyUtil.getString(rootNode, requestType.tmReturnUrl);
                if (StringUtils.isEmpty(returnUrl)) {
                    pkg.setTmReturnUrl(rootNode.getProperty(TM_PARAM_RETURN_URL).getString());
                    log.error("The return URL expected for channel " + channel.code + " property " + requestType.tmReturnUrl + " is not found");
                } else {
                    pkg.setTmReturnUrl(returnUrl);
                }
            }

            pkg.setTmReturnUrlSkyDining(PropertyUtil.getString(rootNode, TM_PARAM_RETURN_URL_SKY_DINING));
            pkg.setTmStatusUrl(rootNode.getProperty(TM_PARAM_STATUS_URL).getString());
            pkg.setTransEmptyLowerBoundMillis((int) rootNode.getProperty(TM_PARAM_TRANS_EMPTY_LOWER_BOUND_MILLIS).getLong());
            pkg.setTransEmptyUpperBoundMillis((int) rootNode.getProperty(TM_PARAM_TRANS_EMPTY_UPPER_BOUND_MILLIS).getLong());
            pkg.setTransTimeoutLowerBoundMillis((int) rootNode.getProperty(TM_PARAM_TRANS_TIMEOUT_LOWER_BOUND_MILLIS).getLong());
            pkg.setTransTimeoutUpperBoundMillis((int) rootNode.getProperty(TM_PARAM_TRANS_TIMEOUT_UPPER_BOUND_MILLIS).getLong());

            log.info("Retrieved the following TM parameters from JCR: " + JsonUtil.jsonify(pkg));

            return pkg;
        } catch (RepositoryException e) {
            log.error("Error retrieving Telemoney parameters from JCR: " + rootPath, e);
            throw new JcrRepositoryException(e);
        }
    }
}
