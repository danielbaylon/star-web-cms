package com.enovax.star.cms.commons.model.axstar;

import java.util.Map;

public class AxStarInputAddCartItem {

    private Long productId;
    private int quantity;
    private String comment;
    private Map<String, String> stringExtensionProperties;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Map<String, String> getStringExtensionProperties() {
        return stringExtensionProperties;
    }

    public void setStringExtensionProperties(Map<String, String> stringExtensionProperties) {
        this.stringExtensionProperties = stringExtensionProperties;
    }
}
