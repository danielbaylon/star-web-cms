package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAAccessRightsGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMTAAccessRightsGroupRepository extends JpaRepository<PPSLMTAAccessRightsGroup, Integer> {

    PPSLMTAAccessRightsGroup findById(int id);

    List<PPSLMTAAccessRightsGroup> findByType(String type);

}
