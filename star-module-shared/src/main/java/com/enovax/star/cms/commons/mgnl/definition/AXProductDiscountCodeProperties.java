package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 7/21/16.
 */
public enum AXProductDiscountCodeProperties {
    RecordId("recordId"),
    Code("code"),
    SystemName("systemName"),
    ValidFrom("validFrom"),
    ValidTo("validTo"),
    Barcode("barcode"),
    RelatedOfferId("relatedOfferId"),
    SystemStatus("systemStatus");

    private String propertyName;

    AXProductDiscountCodeProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
