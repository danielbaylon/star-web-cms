package com.enovax.star.cms.commons.model.product;

import java.util.List;

public class ProductItemCriteriaWrapper {

    private List<ProductItemCriteria> products;

    public List<ProductItemCriteria> getProducts() {
        return products;
    }

    public void setProducts(List<ProductItemCriteria> products) {
        this.products = products;
    }
}
