package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.CartCheckoutCancelCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartCheckoutCancelRequest {

    @JsonProperty("CartCheckoutCancelCriteria")
    private CartCheckoutCancelCriteria criteria = new CartCheckoutCancelCriteria();

    public CartCheckoutCancelCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(CartCheckoutCancelCriteria criteria) {
        this.criteria = criteria;
    }

}
