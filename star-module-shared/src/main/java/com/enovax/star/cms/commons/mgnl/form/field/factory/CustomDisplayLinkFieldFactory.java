package com.enovax.star.cms.commons.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.form.field.CustomDisplayLinkField;
import com.enovax.star.cms.commons.mgnl.form.field.definition.CustomDisplayLinkFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.app.AppController;
import info.magnolia.ui.api.app.ChooseDialogCallback;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.LinkFieldDefinition;
import info.magnolia.ui.form.field.factory.AbstractFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class CustomDisplayLinkFieldFactory<T extends CustomDisplayLinkFieldDefinition>  extends AbstractFieldFactory<LinkFieldDefinition, String> {

    private static final Logger log = LoggerFactory.getLogger(CustomDisplayLinkFieldFactory.class);
    private CustomDisplayLinkField linkField;
    private ComponentProvider componentProvider;

    private final AppController appController;
    private final UiContext uiContext;
    private final CustomDisplayLinkFieldDefinition definition;

    @Inject
    public CustomDisplayLinkFieldFactory(CustomDisplayLinkFieldDefinition definition, Item relatedFieldItem, AppController appController, UiContext uiContext, ComponentProvider componentProvider, I18NAuthoringSupport i18NAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18NAuthoringSupport);
        this.componentProvider = componentProvider;
        this.appController = appController;
        this.uiContext = uiContext;
        this.definition = definition;
    }

    @Override
    public void setComponentProvider(ComponentProvider componentProvider) {
        super.setComponentProvider(componentProvider);
        this.componentProvider = componentProvider;
    }

    @Override
    protected Field<String> createFieldComponent() {

        linkField = getNewLinkField();
        // Set Caption
        linkField.setButtonCaptionNew("Select New");
        linkField.setButtonCaptionOther("Select Another");
        // Add a callback listener on the select button
        linkField.getSelectButton().addClickListener(createButtonClickListener());
        return linkField;
    }


    protected CustomDisplayLinkField getNewLinkField() {
        return new CustomDisplayLinkField(definition, componentProvider);
    }

    /**
     * Create the Button click Listener. On click: Create a Dialog and
     * Initialize callback handling.
     */
    protected Button.ClickListener createButtonClickListener() {
        return new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                ChooseDialogCallback callback = createChooseDialogCallback();
                String uuid = linkField.getValue();
                String value = "";
                if(StringUtils.isNotBlank(uuid)) {
                    try {
                        value = NodeUtil.getNodeByIdentifier(definition.getTargetWorkspace(), uuid).getPath();
                    } catch (RepositoryException e) {
                        e.printStackTrace();
                    }
                }

                if (StringUtils.isNotBlank(definition.getTargetTreeRootPath())) {
                    appController.openChooseDialog(definition.getAppName(), uiContext, definition.getTargetTreeRootPath(), value, callback);
                } else {
                    appController.openChooseDialog(definition.getAppName(), uiContext, value, callback);
                }
            }
        };
    }

    /**
     * @return specific {@link ChooseDialogCallback} implementation used to process the selected value.
     */
    protected ChooseDialogCallback createChooseDialogCallback() {
        return new ChooseDialogCallback() {

            private String path;
            private String uuid;

            @Override
            public void onItemChosen(String actionName, final Object chosenValue) {
                String newValue = null;
                if (chosenValue instanceof JcrItemId) {
                    String propertyName = definition.getTargetPropertyToPopulate();
                    try {
                        javax.jcr.Item jcrItem = JcrItemUtil.getJcrItem((JcrItemId) chosenValue);
                        if (jcrItem.isNode()) {
                            final Node selected = (Node) jcrItem;
                            boolean isPropertyExisting = StringUtils.isNotBlank(propertyName) && selected.hasProperty(propertyName);
                            newValue = isPropertyExisting ? selected.getProperty(propertyName).getString() : selected.getPath();
                            linkField.setDisplay(getLinkFieldDisplay(selected, newValue));
                        }
                    } catch (RepositoryException e) {
                        log.error("Not able to access the configured property. Value will not be set.", e);
                    }
                } else {
                    newValue = String.valueOf(chosenValue);
                }
                linkField.setValue(newValue);
            }

            @Override
            public void onCancel() {
            }
        };
    }

    protected String getLinkFieldDisplay(final Node selected, String defaultValue) throws RepositoryException{
        if (StringUtils.isNotEmpty(definition.getTargetPropertyToDisplay()) && selected.hasProperty(definition.getTargetPropertyToDisplay())) {

            String text;
            if(CustomDisplayLinkFieldDefinition.NODE_NAME.equals(definition.getTargetPropertyToDisplay())) {
                text =  selected.getName();
            }else {
                text = selected.getProperty(definition.getTargetPropertyToDisplay()).getString();
            }

            if (StringUtils.isNotEmpty(text)) {
                return text;
            }
        }
        return defaultValue; //show the path by default
    }


}
