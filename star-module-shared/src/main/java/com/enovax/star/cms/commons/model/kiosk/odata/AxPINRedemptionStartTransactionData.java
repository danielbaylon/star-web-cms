package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * SDC_NonBindableCRTExtension.Entity.PINRedemptionStartTransEntity
 * 
 * @author dbaylon
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxPINRedemptionStartTransactionData {

    @JsonProperty("LineID")
    private String lineID;

    @JsonProperty("ExpiryDate")
    private String expiryDate;

    @JsonProperty("InventTransId")
    private String inventTransId;

    @JsonProperty("InvoiceId")
    private String invoiceId;

    @JsonProperty("IsCombined")
    private String isCombined;

    @JsonProperty("ItemId")
    private String itemId;

    @JsonProperty("MediaTypeId")
    private String mediaTypeId;

    @JsonProperty("OpenValidityId")
    private String openValidityId;

    @JsonProperty("OriginalQty")
    private String originalQty;

    @JsonProperty("Printing")
    private String printing;

    @JsonProperty("Qty")
    private String qty;

    @JsonProperty("QtyRedeemed")
    private String qtyRedeemed;

    @JsonProperty("ReferenceId")
    private String referenceId;

    @JsonProperty("RefRecId")
    private String refRecId;

    @JsonProperty("SalesId")
    private String salesId;

    @JsonProperty("SessionNo")
    private String sessionNo;

    @SerializedName("sourceRecId")
    private String sourceRecId;

    @JsonProperty("StartDateTime")
    private String startDateTime;

    @JsonProperty("TicketDate")
    private String ticketDate;

    @JsonProperty("TicketDateParm")
    private String ticketDateParm;

    @JsonProperty("UserId")
    private String userId;

    // @JsonProperty("EventDate")
    @JsonIgnore
    private String eventDate;

    @JsonProperty("EventGroupId")
    private String eventGroupId;

    @JsonProperty("EventLineId")
    private String eventLineId;

    @JsonProperty("EndDateTime")
    private String endDateTime;

    @JsonProperty("NoOfPax")
    private String noOfPax;

    @JsonProperty("DefineTicketDate")
    private String defineTicketDate;

    @JsonProperty("ItemName")
    private String itemName;

    @JsonProperty("TicketStartDateTime")
    private String ticketStartDateTime;

    @JsonProperty("TicketEndDateTime")
    private String ticketEndDateTime;

    @JsonProperty("PrintingBehavior")
    private String printingBehavior;

    @JsonProperty("EventGroupList")
    private AxEventGroupList axEventGroupList;

    /**
     * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
     * 
     * 
     * @JsonProperty("ExtensionProperties") private AxCommercePropertyList
     * axCommercePropertyList = new AxCommercePropertyList();
     */
    @JsonProperty("IsCapacity")
    private String isCapacity;

    @JsonProperty("AllowChangeEventDate")
    private String allowChangeEventDate;

    public String getLineID() {
        return lineID;
    }

    public void setLineID(String lineID) {
        this.lineID = lineID;
    }

    public String getExpiryDate() {

        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {

        this.expiryDate = expiryDate;
    }

    public String getInventTransId() {

        return inventTransId;
    }

    public void setInventTransId(String inventTransId) {

        this.inventTransId = inventTransId;
    }

    public String getInvoiceId() {

        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {

        this.invoiceId = invoiceId;
    }

    public String getIsCombined() {

        return isCombined;
    }

    public void setIsCombined(String isCombined) {

        this.isCombined = isCombined;
    }

    public String getItemId() {

        return itemId;
    }

    public void setItemId(String itemId) {

        this.itemId = itemId;
    }

    public String getMediaTypeId() {

        return mediaTypeId;
    }

    public void setMediaTypeId(String mediaTypeId) {

        this.mediaTypeId = mediaTypeId;
    }

    public String getOpenValidityId() {

        return openValidityId;
    }

    public void setOpenValidityId(String openValidityId) {

        this.openValidityId = openValidityId;
    }

    public String getOriginalQty() {

        return originalQty;
    }

    public void setOriginalQty(String originalQty) {

        this.originalQty = originalQty;
    }

    public String getPrinting() {

        return printing;
    }

    public void setPrinting(String printing) {

        this.printing = printing;
    }

    public String getQty() {

        return qty;
    }

    public void setQty(String qty) {

        this.qty = qty;
    }

    public String getQtyRedeemed() {

        return qtyRedeemed;
    }

    public void setQtyRedeemed(String qtyRedeemed) {

        this.qtyRedeemed = qtyRedeemed;
    }

    public String getReferenceId() {

        return referenceId;
    }

    public void setReferenceId(String referenceId) {

        this.referenceId = referenceId;
    }

    public String getRefRecId() {

        return refRecId;
    }

    public void setRefRecId(String refRecId) {

        this.refRecId = refRecId;
    }

    public String getSalesId() {

        return salesId;
    }

    public void setSalesId(String salesId) {

        this.salesId = salesId;
    }

    public String getSessionNo() {

        return sessionNo;
    }

    public void setSessionNo(String sessionNo) {

        this.sessionNo = sessionNo;
    }

    public String getSourceRecId() {
        return sourceRecId;
    }

    public void setSourceRecId(String sourceRecId) {
        this.sourceRecId = sourceRecId;
    }

    public String getStartDateTime() {

        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {

        this.startDateTime = startDateTime;
    }

    public String getTicketDate() {

        return ticketDate;
    }

    public void setTicketDate(String ticketDate) {

        this.ticketDate = ticketDate;
    }

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {

        this.userId = userId;
    }

    public String getEventDate() {

        return eventDate;
    }

    public void setEventDate(String eventDate) {

        this.eventDate = eventDate;
    }

    public String getEventGroupId() {

        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {

        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {

        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {

        this.eventLineId = eventLineId;
    }

    public String getTicketDateParm() {
        return ticketDateParm;
    }

    public void setTicketDateParm(String ticketDateParm) {
        this.ticketDateParm = ticketDateParm;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getNoOfPax() {

        return noOfPax;
    }

    public void setNoOfPax(String noOfPax) {

        this.noOfPax = noOfPax;
    }

    public String getDefineTicketDate() {

        return defineTicketDate;
    }

    public void setDefineTicketDate(String defineTicketDate) {

        this.defineTicketDate = defineTicketDate;
    }

    public String getItemName() {

        return itemName;
    }

    public void setItemName(String itemName) {

        this.itemName = itemName;
    }

    public AxEventGroupList getAxEventGroupList() {

        return axEventGroupList;
    }

    public void setAxEventGroupList(AxEventGroupList axEventGroupList) {

        this.axEventGroupList = axEventGroupList;
    }

    public void setIsCapacity(String isCapacity) {
        this.isCapacity = isCapacity;
    }

    public String getIsCapacity() {
        return isCapacity;
    }

    public void setAllowChangeEventDate(String allowChangeEventDate) {
        this.allowChangeEventDate = allowChangeEventDate;
    }

    public String getAllowChangeEventDate() {
        return allowChangeEventDate;
    }

    public String getTicketStartDateTime() {
        return ticketStartDateTime;
    }

    public void setTicketStartDateTime(String ticketStartDateTime) {
        this.ticketStartDateTime = ticketStartDateTime;
    }

    public String getTicketEndDateTime() {
        return ticketEndDateTime;
    }

    public void setTicketEndDateTime(String ticketEndDateTime) {
        this.ticketEndDateTime = ticketEndDateTime;
    }

    public String getPrintingBehavior() {
        return printingBehavior;
    }

    public void setPrintingBehavior(String printingBehavior) {
        this.printingBehavior = printingBehavior;
    }

}
