package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.model.axstar.AxStarSalesOrder;

public class StoreTransactionExtensionData {

    private int id;
    private int transactionId;
    private String receiptNumber;
    private StoreTransaction txn;
    private FunCart cart;
    private AxStarSalesOrder axSalesOrder;
    private String ticketsJson;

    public StoreTransaction getTxn() {
        return txn;
    }

    public void setTxn(StoreTransaction txn) {
        this.txn = txn;
    }

    public FunCart getCart() {
        return cart;
    }

    public void setCart(FunCart cart) {
        this.cart = cart;
    }

    public AxStarSalesOrder getAxSalesOrder() {
        return axSalesOrder;
    }

    public void setAxSalesOrder(AxStarSalesOrder axSalesOrder) {
        this.axSalesOrder = axSalesOrder;
    }

    public String getTicketsJson() {
        return ticketsJson;
    }

    public void setTicketsJson(String ticketsJson) {
        this.ticketsJson = ticketsJson;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }
}
