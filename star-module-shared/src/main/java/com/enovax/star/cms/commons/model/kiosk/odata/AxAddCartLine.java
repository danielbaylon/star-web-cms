package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.CartLine
 * 
 * @author Justin
 *
 */
public class AxAddCartLine {

    @JsonProperty("Description")
    private String description;

    @JsonProperty("ItemId")
    private String itemId;

    @JsonProperty("ProductId")
    private String productId;

    @JsonProperty("Quantity")
    private String quantity;

    @JsonIgnore
    private String shippingAddress;

    @JsonProperty("Comment")
    private String comment = "";

    @JsonProperty("UnitOfMeasureSymbol")
    private String unitOfMeasureSymbol;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public void setUnitOfMeasureSymbol(String unitOfMeasureSymbol) {
        this.unitOfMeasureSymbol = unitOfMeasureSymbol;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
