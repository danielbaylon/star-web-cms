package com.enovax.star.cms.commons.util;

import com.enovax.star.cms.commons.mgnl.definition.*;
import info.magnolia.commands.CommandsManager;
import info.magnolia.commands.chain.Command;
import info.magnolia.context.Context;
import info.magnolia.context.SimpleContext;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jennylynsze on 4/14/16.
 */
public class PublishingUtil {

    private static final Logger log = LoggerFactory.getLogger(PublishingUtil.class);

    private static final String CHINESE_SUFFIX = "_zh_CN";

    public static void publishNodes(String uuid, String workspaceName) throws Exception {
        String cmdName = "activate";
        Command cmd = ((CommandsManager) Components.getComponent(CommandsManager.class)).getCommand("", cmdName);
        Context ctx = new SimpleContext();
        Map params = new HashMap<>();
        params.put("repository", workspaceName);
        params.put("uuid", uuid);
        params.put("recursive", true);

        ctx.putAll(params);
        cmd.execute(ctx);
    }

    public static void deleteNodeAndPublish(String path, String workspaceName) throws Exception {
        String cmdName = "delete";

        Command cmd = ((CommandsManager) Components.getComponent(CommandsManager.class)).getCommand("", cmdName);
        Context ctx = new SimpleContext();
        Map params = new HashMap<>();
        params.put("repository", workspaceName);
        params.put("path", path);

        ctx.putAll(params);
        cmd.execute(ctx);
    }

    public static void deleteNodeAndPublish(Node node, String workspaceName) throws Exception {
        deleteNodeAndPublish(node.getPath(), workspaceName);
    }


    public static void publishAXProductRelatedNodes(Node axProductNode) throws Exception {
        publishPromotions(axProductNode);
        publishCrossSellsImages(axProductNode);
    }


    private static void publishPromotions(Node axProductNode) throws Exception {
        if(axProductNode.hasNode(AXProductProperties.Promotion.getPropertyName())) {
            Node promotionNode = axProductNode.getNode(AXProductProperties.Promotion.getPropertyName());
            List<Node> promotionList =  NodeUtil.asList(NodeUtil.getNodes(promotionNode));
            for(Node promotion: promotionList) {
                if(promotion.hasProperty(AXProductProperties.RelatedPromotionUUID.getPropertyName())) {
                    String uuid = PropertyUtil.getString(promotion, AXProductProperties.RelatedPromotionUUID.getPropertyName());
                    Node promotionMainNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Promotions.getWorkspaceName(), uuid);
                    PublishingUtil.publishNodes(uuid, JcrWorkspace.Promotions.getWorkspaceName());
                    publishAffiliatons(promotionMainNode);
                    publishDiscountCodes(promotionMainNode);
                    publishPromotionsImages(promotionMainNode);
                }
            }
        }
    }

    public static void publishPromotionsRelatedNodes(Node promotionMainNode) throws Exception {
        publishAffiliatons(promotionMainNode);
        publishDiscountCodes(promotionMainNode);
        publishPromotionsImages(promotionMainNode);
    }

    private static void publishDiscountCodes(Node promotionNode) throws Exception {
        if(promotionNode.hasNode(AXProductPromotionProperties.DiscountCode.getPropertyName())) {
            Node dcNode = promotionNode.getNode(AXProductPromotionProperties.DiscountCode.getPropertyName());
            PublishingUtil.publishNodes(dcNode.getIdentifier(), JcrWorkspace.Promotions.getWorkspaceName());
        }
    }

    private static void publishAffiliatons(Node promotionNode) throws Exception {
        if(promotionNode.hasNode(AXProductPromotionProperties.Affiliation.getPropertyName())) {
            Node affiliationNode = promotionNode.getNode(AXProductPromotionProperties.Affiliation.getPropertyName());
            List<Node> affiliationList = NodeUtil.asList(NodeUtil.getNodes(affiliationNode));
            for(Node affiliation: affiliationList) {
                if(affiliation.hasProperty(AXProductPromotionProperties.RelatedAffUUID.getPropertyName())) {
                    String uuid = PropertyUtil.getString(affiliation, AXProductPromotionProperties.RelatedAffUUID.getPropertyName());
                    Node affMainNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Affiliations.getWorkspaceName(), uuid);
                    PublishingUtil.publishNodes(uuid, JcrWorkspace.Affiliations.getWorkspaceName());
                    PublishingUtil.publishAffiliatonsRelatedNode(affMainNode);
                }
            }
        }
    }

    private static void publishAffiliatonsRelatedNode(Node affNode) throws Exception {
        if(affNode.hasProperty(AXProductAffiliationProperties.DiscountTypeCreditCard.getPropertyName())) {
           String uuid = PropertyUtil.getString(affNode, AXProductAffiliationProperties.DiscountTypeCreditCard.getPropertyName());
           if(StringUtils.isNotEmpty(uuid))  {
                PublishingUtil.publishNodes(uuid, JcrWorkspace.Merchant.getWorkspaceName());
            }
        }
    }

    private static void publishPromotionsImages(Node promotion) throws Exception {
        //check the promotions
        if(promotion.hasProperty(AXProductProperties.PromotionIcon.getPropertyName())) {
            String uuid = promotion.getProperty(AXProductProperties.PromotionIcon.getPropertyName()).getString();
            publishImages(uuid);
        }
    }

    private static void publishCrossSellsImages(Node axProductNode) throws Exception {
        //check the promotions
        if(axProductNode.hasNode(AXProductProperties.CrossSell.getPropertyName())) {
            Node crossSellNode = axProductNode.getNode(AXProductProperties.CrossSell.getPropertyName());
            List<Node> crossSellList =  NodeUtil.asList(NodeUtil.getNodes(crossSellNode));
            for(Node crossSell: crossSellList) {
                //check the "crossSellItemImage" both english and chinese
                if(crossSell.hasProperty(AXProductProperties.CrossSellImage.getPropertyName())) {
                    String uuid = crossSell.getProperty(AXProductProperties.CrossSellImage.getPropertyName()).getString();
                    publishImages(uuid);
                }

                if(crossSell.hasProperty(AXProductProperties.CrossSellImage.getPropertyName() + CHINESE_SUFFIX)) {
                    String uuid = crossSell.getProperty(AXProductProperties.CrossSellImage.getPropertyName() + CHINESE_SUFFIX).getString();
                    publishImages(uuid);
                }
            }
        }
    }


    public static void publishImages(String uuid) throws Exception {
        if(StringUtils.isNotBlank(uuid)) {
            if(uuid.startsWith("jcr:")) {
                uuid = uuid.substring(4);
            }

            Node imageNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Dam.getWorkspaceName(), uuid);
            if (!PublishingUtil.isPublish(imageNode)) {
                try {
                    //traverse all the parents && published it first
                    Node parentNode = imageNode.getParent();
                    while(parentNode != null && !"/".equals(parentNode.getPath())) {
                        if(!PublishingUtil.isPublish(parentNode)) {
                            PublishingUtil.publishNodes(parentNode.getIdentifier(), JcrWorkspace.Dam.getWorkspaceName());
                        }
                        try {
                            parentNode = parentNode.getParent();
                        }catch(Exception e) {
                            parentNode = null;
                        }
                    }
                    PublishingUtil.publishNodes(uuid, JcrWorkspace.Dam.getWorkspaceName());

                }catch(Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public static void publishProductRelatedNodes(Node productNode) throws Exception {
        if(productNode.hasNode(CMSProductProperties.ProductImages.getPropertyName())) {
            publishProductRelatedImages(productNode);
        }

        if(productNode.hasProperty(CMSProductProperties.TNC.getPropertyName()))  {
            publishProductRelatedTnc(productNode);
        }
    }

    private static void publishProductRelatedTnc(Node productNode) throws Exception {
        String uuid = productNode.getProperty(CMSProductProperties.TNC.getPropertyName()).getString();
        Node tncNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.TNC.getWorkspaceName(), uuid);
        if(!PublishingUtil.isPublish(tncNode)) {
            PublishingUtil.publishNodes(uuid, JcrWorkspace.TNC.getWorkspaceName());
        }
    }

    private static void publishProductRelatedImages(Node productNode) throws Exception{
        Node productImagesNode =  productNode.getNode(CMSProductProperties.ProductImages.getPropertyName());
        List<Node> relatedProductImagesNodes =  NodeUtil.asList(NodeUtil.getNodes(productImagesNode));
        for(Node relatedProductImageNode : relatedProductImagesNodes) {
            if (relatedProductImageNode.hasProperty(CMSProductProperties.ProductImages.getPropertyName())) {
                String uuid = relatedProductImageNode.getProperty(CMSProductProperties.ProductImages.getPropertyName()).getString();

                if(StringUtils.isNotBlank(uuid) && uuid.startsWith("jcr:")) {
                    uuid = uuid.substring(4);
                }

                Node imageNode = NodeUtil.getNodeByIdentifier(JcrWorkspace.Dam.getWorkspaceName(), uuid);
                if (!PublishingUtil.isPublish(imageNode)) {
                    try {
                        //traverse all the parents && published it first
                        Node parentNode = imageNode.getParent();
                        while(parentNode != null && !"/".equals(parentNode.getPath())) {
                            if(!PublishingUtil.isPublish(parentNode)) {
                                PublishingUtil.publishNodes(parentNode.getIdentifier(), JcrWorkspace.Dam.getWorkspaceName());
                            }
                            try {
                                parentNode = parentNode.getParent();
                            }catch(Exception e) {
                                parentNode = null;
                            }
                        }
                        PublishingUtil.publishNodes(uuid, JcrWorkspace.Dam.getWorkspaceName());

                    }catch(Exception e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
        }
    }

    public static boolean isPromotionPublish(Node node) {
        //TODO something here
        return false;
    }

    public static boolean isPublish(Node node) {
//        try {
//            return node.hasProperty("mgnl:activationStatus")
//                    && "true".equals(node.getProperty("mgnl:activationStatus").getString());
//        } catch (RepositoryException e) {
//            e.printStackTrace();
//        }
        Integer e;
        try {
            e = Integer.valueOf(NodeTypes.Activatable.getActivationStatus(node));
        } catch (RepositoryException var13) {
            e = Integer.valueOf(0);
        }

        switch(e.intValue()) {
            case 1:
                return false;
            case 2:
                return true;
            default:
                return false;
        }
    }
}
