package com.enovax.star.cms.commons.model.product;

import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class ProductExtDataCacheObject {

    private LocalDateTime cacheTs = null;
    private Map<String, AxRetailProductExtData> dataMap = new HashMap<>();
    private Map<String, ProductExtViewModel> viewModelMap = new HashMap<>();
    private boolean initLoad = false;

    public LocalDateTime getCacheTs() {
        return cacheTs;
    }

    public void setCacheTs(LocalDateTime cacheTs) {
        this.cacheTs = cacheTs;
    }

    public Map<String, AxRetailProductExtData> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, AxRetailProductExtData> dataMap) {
        this.dataMap = dataMap;
    }

    public Map<String, ProductExtViewModel> getViewModelMap() {
        return viewModelMap;
    }

    public void setViewModelMap(Map<String, ProductExtViewModel> viewModelMap) {
        this.viewModelMap = viewModelMap;
    }

    public boolean isInitLoad() {
        return initLoad;
    }

    public void setInitLoad(boolean initLoad) {
        this.initLoad = initLoad;
    }
}
