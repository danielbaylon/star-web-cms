package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdatePrintStatusCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskUpdatePrintStatusRequest {

    @JsonProperty("CartTicketUpdatePrintStatusCriteria")
    private AxCartTicketUpdatePrintStatusCriteria axCartTicketUpdatePrintStatusCriteria;

    public AxCartTicketUpdatePrintStatusCriteria getAxCartTicketUpdatePrintStatusCriteria() {
        return axCartTicketUpdatePrintStatusCriteria;
    }

    public void setAxCartTicketUpdatePrintStatusCriteria(AxCartTicketUpdatePrintStatusCriteria axCartTicketUpdatePrintStatusCriteria) {
        this.axCartTicketUpdatePrintStatusCriteria = axCartTicketUpdatePrintStatusCriteria;
    }

}
