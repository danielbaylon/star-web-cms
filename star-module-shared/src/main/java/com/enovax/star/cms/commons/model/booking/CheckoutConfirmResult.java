package com.enovax.star.cms.commons.model.booking;

public class CheckoutConfirmResult {
    private boolean tmRedirect = false;
    private String redirectUrl;

    public boolean isTmRedirect() {
        return tmRedirect;
    }

    public void setTmRedirect(boolean tmRedirect) {
        this.tmRedirect = tmRedirect;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
