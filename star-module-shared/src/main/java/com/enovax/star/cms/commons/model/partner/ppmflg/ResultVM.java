package com.enovax.star.cms.commons.model.partner.ppmflg;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class ResultVM {
    public final static String Break = "\n";
    Boolean status = true;
    String message;

    int total = 0;
    Object viewModel;

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Object getViewModel() {
        return viewModel;
    }

    public void setViewModel(Object viewModel) {
        this.viewModel = viewModel;
    }

    public static ResultVM processResult(ResultVM... resultVMs) {
        ResultVM result = new ResultVM();
        if (resultVMs == null) {
            return new ResultVM();
        } else {
            for (ResultVM iresult : resultVMs) {
                if (iresult != null && (!iresult.isStatus())) {
                    result.setStatus(false);
                    result.setMessage((result.getMessage() == null ? ""
                            : result.getMessage())
                            + iresult.getMessage()
                            + Break);
                }
            }
        }
        return result;
    }
}
