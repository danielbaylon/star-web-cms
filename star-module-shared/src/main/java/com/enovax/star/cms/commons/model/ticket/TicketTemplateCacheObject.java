package com.enovax.star.cms.commons.model.ticket;

import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailTemplateXML;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 12/15/16.
 */
public class TicketTemplateCacheObject {
    private Map<String, AxRetailTemplateXML> templateXMLMap = new HashMap<>();

    public Map<String, AxRetailTemplateXML> getTemplateXMLMap() {
        return templateXMLMap;
    }

    public void setTemplateXMLMap(Map<String, AxRetailTemplateXML> templateXMLMap) {
        this.templateXMLMap = templateXMLMap;
    }
}
