package com.enovax.star.cms.commons.service.axcrt;

import com.enovax.star.cms.commons.service.config.ISysCfgService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by houtao on 10/8/16.
 */
@Service
public class AxStarRestRequestHandler {

    private static final Logger log = LoggerFactory.getLogger(AxStarRestRequestHandler.class);

    @Autowired
    private ISysCfgService cfgSrv;
    private Gson gson;

    @PostConstruct
    private void init(){
        String axDateTimeFormat = null;
        try{
            axDateTimeFormat = cfgSrv.getAxDateTimeFormat();
        }catch (Exception ex){
            log.warn("retrieving AX REST service api date/time format config failed, default ax date time format will not initialized", ex);
        }
        if(axDateTimeFormat == null || axDateTimeFormat.trim().length() == 0){
            log.warn("default ax date time format is empty and will not initialized");
        }
        JsonUtil util = new JsonUtil();
        gson = util.getGson(true, axDateTimeFormat, FieldNamingPolicy.UPPER_CAMEL_CASE);
        util = null;
    }


    public String parseObjectToString(Object t) {
        return gson.toJson(t);
    }
}
