package com.enovax.star.cms.commons.service.ftp;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.jcrrepository.system.ISystemParamRepository;
import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;

/**
 * Created by houtao on 19/10/16.
 */
public interface FTPService {

    public void init(ISystemParamRepository paramRepo, String channel, String ftpCfg);

    public FTPClient connect() throws BizValidationException, IOException;

    public void login(FTPClient client) throws BizValidationException, IOException;

    public void logout(FTPClient client) throws BizValidationException, IOException;

}
