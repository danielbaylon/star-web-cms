package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartCheckoutCompleteCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 * @since 10 SEP 16
 */
public class KioskCartCheckoutCompleteRequest {

    @JsonProperty("CartCheckoutCompleteCriteria")
    private AxCartCheckoutCompleteCriteria axCartCheckoutCompleteCriteria = new AxCartCheckoutCompleteCriteria();

    public AxCartCheckoutCompleteCriteria getAxCartCheckoutCompleteCriteria() {
        return axCartCheckoutCompleteCriteria;
    }

    public void setAxCartCheckoutCompleteCriteria(AxCartCheckoutCompleteCriteria axCartCheckoutCompleteCriteria) {
        this.axCartCheckoutCompleteCriteria = axCartCheckoutCompleteCriteria;
    }

}
