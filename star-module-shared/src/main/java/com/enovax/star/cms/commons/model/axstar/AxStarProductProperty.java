package com.enovax.star.cms.commons.model.axstar;

/**
 * Created by jennylynsze on 10/13/16.
 */
public class AxStarProductProperty {

    private Integer propertyTypeValue;
    private String keyName;
    private String friendlyName;
    private Long recordId;
    private String valueString;

    public Integer getPropertyTypeValue() {
        return propertyTypeValue;
    }

    public void setPropertyTypeValue(Integer propertyTypeValue) {
        this.propertyTypeValue = propertyTypeValue;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }
}
