package com.enovax.star.cms.commons.mgnl.form.field.factory;

import com.enovax.star.cms.commons.mgnl.form.field.FixMultiValueField;
import com.enovax.star.cms.commons.mgnl.form.field.definition.FixMultiValueFieldDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.ui.Field;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.factory.FieldFactoryFactory;
import info.magnolia.ui.form.field.factory.MultiValueFieldFactory;

import javax.inject.Inject;

/**
 * Created by jennylynsze on 6/14/16.
 */
public class FixMultiValueFieldFactory<T extends FixMultiValueFieldDefinition> extends MultiValueFieldFactory<T>  {

    private FieldFactoryFactory fieldFactoryFactory;
    private ComponentProvider componentProvider;
    private I18NAuthoringSupport i18nAuthoringSupport;
    private FixMultiValueFieldDefinition definition;

    @Inject
    public FixMultiValueFieldFactory(FixMultiValueFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport, FieldFactoryFactory fieldFactoryFactory, ComponentProvider componentProvider)  {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport, fieldFactoryFactory, componentProvider);

        this.fieldFactoryFactory = fieldFactoryFactory;
        this.componentProvider = componentProvider;
        this.i18nAuthoringSupport = i18nAuthoringSupport;
        this.definition = definition;
    }

    @Override
    protected Field<PropertysetItem> createFieldComponent() {

        FixMultiValueField field = new FixMultiValueField(definition, fieldFactoryFactory, componentProvider, item, i18nAuthoringSupport);

        return field;
    }
}
