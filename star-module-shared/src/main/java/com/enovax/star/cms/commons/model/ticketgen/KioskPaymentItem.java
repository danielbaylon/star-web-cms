package com.enovax.star.cms.commons.model.ticketgen;

public class KioskPaymentItem {
    private String terminalId;
    private String merchantId;
    private String batchNumber;
    private String invoiceNumber;
    private String approvalCode;
    private String retrivalReferenceData;
    private String merchantName;
    private String entryMode;
    private String terminalVerificationResponse;
    private String applicationIdentifier;
    private String appLabel;
    private String transactionCertificate;
    private String paymentMode;
    private String cardLabel;
    private String cardNumber;

    private String signatureRequired;
    private String signature;
    private String noSignature;
    private String cardHolderName;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getRetrivalReferenceData() {
        return retrivalReferenceData;
    }

    public void setRetrivalReferenceData(String retrivalReferenceData) {
        this.retrivalReferenceData = retrivalReferenceData;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getEntryMode() {
        return entryMode;
    }

    public void setEntryMode(String entryMode) {
        this.entryMode = entryMode;
    }

    public String getTerminalVerificationResponse() {
        return terminalVerificationResponse;
    }

    public void setTerminalVerificationResponse(String terminalVerificationResponse) {
        this.terminalVerificationResponse = terminalVerificationResponse;
    }

    public String getApplicationIdentifier() {
        return applicationIdentifier;
    }

    public void setApplicationIdentifier(String applicationIdentifier) {
        this.applicationIdentifier = applicationIdentifier;
    }

    public String getAppLabel() {
        return appLabel;
    }

    public void setAppLabel(String appLabel) {
        this.appLabel = appLabel;
    }

    public String getTransactionCertificate() {
        return transactionCertificate;
    }

    public void setTransactionCertificate(String transactionCertificate) {
        this.transactionCertificate = transactionCertificate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCardLabel() {
        return cardLabel;
    }

    public void setCardLabel(String cardLabel) {
        this.cardLabel = cardLabel;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getSignatureRequired() {
        return signatureRequired;
    }

    public void setSignatureRequired(String signatureRequired) {
        this.signatureRequired = signatureRequired;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getNoSignature() {
        return noSignature;
    }

    public void setNoSignature(String noSignature) {
        this.noSignature = noSignature;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

  
}
