package com.enovax.star.cms.commons.service.email;

public interface MailCallback {
    
    void complete(String trackingId, boolean success);
}
