package com.enovax.star.cms.commons.model.booking.skydining;

import com.enovax.star.cms.commons.model.booking.FunCartDisplayItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 17/6/16.
 */
public class SkyDiningFunCartDisplayItem extends FunCartDisplayItem {
    private Boolean hasRemarks = false;
    private String remarks;

    private String themeId;
    private String themeName;

    private Boolean hasMainCourse = false;
    private String mainCourseText;
    private List<SkyDiningFunCartDisplayMainCourse> mainCourses = new ArrayList<>();

    private Boolean hasDiscount = false;
    private String discountName;

    public List<SkyDiningFunCartDisplayMainCourse> getMainCourses() {
        return mainCourses;
    }

    public void setMainCourses(List<SkyDiningFunCartDisplayMainCourse> mainCourses) {
        this.mainCourses = mainCourses;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getThemeId() {
        return themeId;
    }

    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getMainCourseText() {
        return mainCourseText;
    }

    public void setMainCourseText(String mainCourseText) {
        this.mainCourseText = mainCourseText;
    }

    public Boolean getHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(Boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public Boolean getHasMainCourse() {
        return hasMainCourse;
    }

    public void setHasMainCourse(Boolean hasMainCourse) {
        this.hasMainCourse = hasMainCourse;
    }

    public Boolean getHasRemarks() {
        return hasRemarks;
    }

    public void setHasRemarks(Boolean hasRemarks) {
        this.hasRemarks = hasRemarks;
    }
}
