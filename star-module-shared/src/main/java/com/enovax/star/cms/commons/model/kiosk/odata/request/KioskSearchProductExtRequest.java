package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxInventTableExtCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 *
 */
public class KioskSearchProductExtRequest {

    @JsonProperty("InventTableExtCriteria")
    private AxInventTableExtCriteria axInventTableExtCriteria = new AxInventTableExtCriteria();

    public AxInventTableExtCriteria getAxInventTableExtCriteria() {
        return axInventTableExtCriteria;
    }

    public void setAxInventTableExtCriteria(AxInventTableExtCriteria axInventTableExtCriteria) {
        this.axInventTableExtCriteria = axInventTableExtCriteria;
    }

}
