package com.enovax.star.cms.commons.constant.axchannel;

/**
 * Created by jensen on 24/8/16.
 */
public enum AxPinType {
    AX(0),
    B2B(1),
    B2C(2),
    Complimentary(3);

    public final Integer code;

    AxPinType(final Integer code) {
        this.code = code;
    }
}
