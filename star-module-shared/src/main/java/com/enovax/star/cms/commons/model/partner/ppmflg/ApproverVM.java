package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.GeneralStatus;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprover;
import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by lavanya on 28/9/16.
 */
public class ApproverVM {
    private Integer id;
    private String adminId;
    private String adminNm;
    private String startDateStr;
    private String endDateStr;
    private Date endDate;
    private Date startDate;
    private String status;
    private String statusLabel;

    public ApproverVM() {
    }

    public ApproverVM(PPMFLGApprover approver) {
        this.id = approver.getId();
        this.adminId = approver.getAdminId();
        this.adminNm = approver.getAdminId();
        this.startDateStr = approver.getStartDate() == null ? "" : NvxDateUtils.formatDate(approver.getStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.endDateStr = approver.getEndDate() == null ? "" : NvxDateUtils.formatDate(approver.getEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
        this.status = approver.getStatus();
        this.statusLabel = GeneralStatus.getLabel(approver.getStatus());
    }

    public ApproverVM(AdminAccountVM admin) {
        this.adminId = admin.getId();
        this.adminNm = admin.getUsername();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminNm() {
        return adminNm;
    }

    public void setAdminNm(String adminNm) {
        this.adminNm = adminNm;
    }

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr() {
        return endDateStr;
    }

    public void setEndDateStr(String endDateStr) {
        this.endDateStr = endDateStr;
    }

    public Date getEndDate() {
        try {
            if(endDateStr!=null && !"".equalsIgnoreCase(endDateStr)) {
                endDate = NvxDateUtils.parseDate(endDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        try {
            if(startDateStr!=null && !"".equalsIgnoreCase(startDateStr)) {
                startDate = NvxDateUtils.parseDate(startDateStr,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_HHMM);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }
}
