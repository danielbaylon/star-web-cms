package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGApprover;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by houtao on 24/9/16.
 */
@Repository
public interface PPMFLGApproverRepository extends JpaRepository<PPMFLGApprover, Integer> {
    @Query("from PPMFLGApprover where category in ?1 and status = 'A' and (endDate > ?2 or endDate is null) and ( startDate < ?2 or startDate is null ) order by id desc ")
    List<PPMFLGApprover> findApproverByType(List<String> approverTypes, Date date);

    @Query("from PPMFLGApprover where category=?1 and status = 'A' order by id desc ")
    PPMFLGApprover findCurrentApprover(String approverType);

    @Query(value="select a from PPMFLGApprover a where a.category='G_B' and (a.status='A' or a.status='I')")
    Page getBackupApprover(Pageable pageable);

    @Query(value="select count(a) from PPMFLGApprover a where a.category='G_B' and (a.status='A' or a.status='I')")
    int countBackupApprover();

    @Query("from PPMFLGApprover where adminId=?1 and status='A'")
    List<PPMFLGApprover> findByAdminId(String adminId);

    @Query("from PPMFLGApprover where adminId=?1 and status='A' and endDate > ?2 and startDate < ?3 and (?4 is null or id<>?4)")
    List<PPMFLGApprover> getOverlappedBackupOfficers(String adminId, Date startDate, Date endDate,Integer id);

    PPMFLGApprover findById(Integer id);


    @Query(value="select count(a) from PPMFLGApprover a where a.category='G' and a.status='A' and a.adminId = ?1 and (endDate > ?2 or endDate is null) and ( startDate < ?2 or startDate is null ) ")
    int countGeneralApproverByAdminId(String adminId, Date date);

    @Query(value="select count(a) from PPMFLGApprover a where a.category='G_B' and a.status='A' and a.adminId = ?1 and (endDate > ?2 or endDate is null) and ( startDate < ?2 or startDate is null ) ")
    int countGeneralBackupApproverByAdminId(String adminId, Date date);


}
