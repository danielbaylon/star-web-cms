package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AxCommercePropertyValue {

    @JsonProperty("BooleanValue")
    private String booleanValue;

    @JsonProperty("ByteValue")
    private String byteValue;
    
    @JsonProperty("DateTimeOffserValue")
    private String dateTimeOffsetValue;
    
    @JsonProperty("DecimalValue")
    private String decimalValue;
    
    @JsonProperty("IntegerValue")
    private String integerValue;
    
    @JsonProperty("LongValue")
    private String longValue;
    
    @JsonProperty("StringValue")
    private String stringValue;

    public String getBooleanValue() {
        return booleanValue;
    }

    public void setBooleanValue(String booleanValue) {
        this.booleanValue = booleanValue;
    }

    public String getByteValue() {
        return byteValue;
    }

    public void setByteValue(String byteValue) {
        this.byteValue = byteValue;
    }

    public String getDateTimeOffsetValue() {
        return dateTimeOffsetValue;
    }

    public void setDateTimeOffsetValue(String dateTimeOffsetValue) {
        this.dateTimeOffsetValue = dateTimeOffsetValue;
    }

    public String getDecimalValue() {
        return decimalValue;
    }

    public void setDecimalValue(String decimalValue) {
        this.decimalValue = decimalValue;
    }

    public String getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(String integerValue) {
        this.integerValue = integerValue;
    }

    public String getLongValue() {
        return longValue;
    }

    public void setLongValue(String longValue) {
        this.longValue = longValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

}
