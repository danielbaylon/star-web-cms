package com.enovax.star.cms.commons.mgnl.form.field.factory;

import com.vaadin.data.Item;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.api.i18n.I18NAuthoringSupport;
import info.magnolia.ui.form.field.definition.StaticFieldDefinition;
import info.magnolia.ui.form.field.factory.StaticFieldFactory;
import info.magnolia.ui.vaadin.integration.jcr.JcrNewNodeAdapter;
import info.magnolia.ui.vaadin.integration.jcr.JcrNodeAdapter;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

/**
 * Created by jennylynsze on 10/18/16.
 */
public class PublishStatusStaticFieldFactory extends StaticFieldFactory {

    private Item relatedFieldItem;
    public static final String ACTIVATED = "Published";
    public static final String NOT_ACTIVATED = "Not Published";
    public static final String MODIFIED = "Published (Modified)";

    @Inject
    public PublishStatusStaticFieldFactory(StaticFieldDefinition definition, Item relatedFieldItem, UiContext uiContext, I18NAuthoringSupport i18nAuthoringSupport) {
        super(definition, relatedFieldItem, uiContext, i18nAuthoringSupport);
        this.relatedFieldItem = relatedFieldItem;
    }

    public String createFieldValue() {

        if(this.relatedFieldItem instanceof JcrNewNodeAdapter) {
            return NOT_ACTIVATED;
        }

        Node relatedNode = ((JcrNodeAdapter) this.relatedFieldItem).getJcrItem();

        Integer publishVal;
        try {
            publishVal= Integer.valueOf(NodeTypes.Activatable.getActivationStatus(relatedNode));
        } catch (RepositoryException var13) {
            publishVal = Integer.valueOf(0);
        }

        switch(publishVal.intValue()) {
            case 1:
                return MODIFIED;
            case 2:
                return ACTIVATED;
            default:
                return NOT_ACTIVATED;
        }
    }
}
