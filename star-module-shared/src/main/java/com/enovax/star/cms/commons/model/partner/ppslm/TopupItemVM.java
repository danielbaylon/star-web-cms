package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class TopupItemVM {
    private Integer id;
    private String displayName;
    private String status;
    private String ticketType;
    private Date validateStartDate;
    private String validateStartDateStr;
    private Date validityEndDate;
    private String validityEndDateStr;
    private Integer qty;
    private Integer unpackagedQty;
    private BigDecimal subTotal;
    private String username;

    public TopupItemVM(PPSLMInventoryTransactionItem item) {
        this.id = item.getId();
        this.displayName = item.getDisplayName();
        this.status = item.getInventoryTrans().getStatus();
        this.ticketType = item.getTicketType();
        this.validateStartDate = item.getInventoryTrans()
                .getValidityStartDate();
        this.validityEndDate = item.getInventoryTrans().getValidityEndDate();
        this.unpackagedQty = item.getUnpackagedQty();
        this.qty = item.getQty();
        this.subTotal = item.getSubTotal();
        this.username = item.getInventoryTrans().getUsername();

        if (validateStartDate != null) {
            validateStartDateStr = NvxDateUtils.formatDate(validateStartDate,
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }

        if (validityEndDate != null) {
            validityEndDateStr = NvxDateUtils.formatDate(validityEndDate,
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Date getValidateStartDate() {
        return validateStartDate;
    }

    public void setValidateStartDate(Date validateStartDate) {
        this.validateStartDate = validateStartDate;
    }

    public String getValidateStartDateStr() {
        return validateStartDateStr;
    }

    public void setValidateStartDateStr(String validateStartDateStr) {
        this.validateStartDateStr = validateStartDateStr;
    }

    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    public String getValidityEndDateStr() {
        return validityEndDateStr;
    }

    public void setValidityEndDateStr(String validityEndDateStr) {
        this.validityEndDateStr = validityEndDateStr;
    }

    public Integer getUnpackagedQty() {
        return unpackagedQty;
    }

    public void setUnpackagedQty(Integer unpackagedQty) {
        this.unpackagedQty = unpackagedQty;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
