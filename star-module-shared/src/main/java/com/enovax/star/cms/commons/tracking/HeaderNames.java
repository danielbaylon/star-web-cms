package com.enovax.star.cms.commons.tracking;

public enum HeaderNames {
    AcceptLanguage("Accept-Language"),
    Host("Host"),
    Referrer("Referer"),
    UserAgent("User-Agent"),
    Cookie("cookie");

    public final String code;
    HeaderNames(String code) {
        this.code = code;
    }
}
