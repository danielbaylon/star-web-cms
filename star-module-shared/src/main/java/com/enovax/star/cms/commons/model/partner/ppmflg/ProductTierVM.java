package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.tier.ProductTier;
import com.enovax.star.cms.commons.datamodel.ppmflg.tier.TierProductMapping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class ProductTierVM implements Serializable {
    private String priceGroupId;
    private String name;
    private int isB2B;
    private List<PartnerProductVM> prodVms;

    public ProductTierVM() {
    }

    public ProductTierVM(ProductTier productTier) {
        //TODO change this one to not using the NODE
            this.priceGroupId = productTier.getPriceGroupId();
            this.name = productTier.getName();
            this.isB2B = productTier.getIsB2B();

    }

    public ProductTierVM(ProductTier productTier, List<TierProductMapping> tierProdMaps) {
        this(productTier);


        prodVms = new ArrayList<PartnerProductVM>();

        if(tierProdMaps != null) {
            for(TierProductMapping tpMap:tierProdMaps){
                prodVms.add(tpMap.getProductVM());
            }
        }

    }

    public String getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(String priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsB2B() {
        return isB2B;
    }

    public void setIsB2B(int isB2B) {
        this.isB2B = isB2B;
    }

    public List<PartnerProductVM> getProdVms() {
        return prodVms;
    }

    public void setProdVms(List<PartnerProductVM> prodVms) {
        this.prodVms = prodVms;
    }
}
