package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxProceedWithPrintedCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskProceedPrintedRequest {

	/**
	 * Collection(SDC_TicketingExtension.Entity.CartRetailTicketEntity)
	 * 
	 */
	@JsonProperty("bindingParameter")
	private List<AxCartRetailTicketEntity> axCartRetailTicketEntities;

	@JsonProperty("ProceedWithPrintedCriteria")
	private AxProceedWithPrintedCriteria axProceedWithPrintedCriteria;

	public List<AxCartRetailTicketEntity> getAxCartRetailTicketEntities() {
		return axCartRetailTicketEntities;
	}

	public void setAxCartRetailTicketEntities(List<AxCartRetailTicketEntity> axCartRetailTicketEntities) {
		this.axCartRetailTicketEntities = axCartRetailTicketEntities;
	}

}
