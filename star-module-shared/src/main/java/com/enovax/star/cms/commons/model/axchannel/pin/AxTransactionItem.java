package com.enovax.star.cms.commons.model.axchannel.pin;

import java.math.BigDecimal;

/**
 * Created by houtao on 26/8/16.
 */
public class AxTransactionItem {

    private BigDecimal discountAmount;
    private String discountAmountWithCurrency;
    private String itemId;
    private AxTransactionItemType itemType;
    private String lineId;
    private String netAmountWithCurrency;
    private String priceWithCurrency;
    private Long productId;
    private Integer quantity;
    private String taxAmountWithCurrency;
    private String variantInventoryDimensionId;


    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmountWithCurrency() {
        return discountAmountWithCurrency;
    }

    public void setDiscountAmountWithCurrency(String discountAmountWithCurrency) {
        this.discountAmountWithCurrency = discountAmountWithCurrency;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public AxTransactionItemType getItemType() {
        return itemType;
    }

    public void setItemType(AxTransactionItemType itemType) {
        this.itemType = itemType;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getNetAmountWithCurrency() {
        return netAmountWithCurrency;
    }

    public void setNetAmountWithCurrency(String netAmountWithCurrency) {
        this.netAmountWithCurrency = netAmountWithCurrency;
    }

    public String getPriceWithCurrency() {
        return priceWithCurrency;
    }

    public void setPriceWithCurrency(String priceWithCurrency) {
        this.priceWithCurrency = priceWithCurrency;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getTaxAmountWithCurrency() {
        return taxAmountWithCurrency;
    }

    public void setTaxAmountWithCurrency(String taxAmountWithCurrency) {
        this.taxAmountWithCurrency = taxAmountWithCurrency;
    }

    public String getVariantInventoryDimensionId() {
        return variantInventoryDimensionId;
    }

    public void setVariantInventoryDimensionId(String variantInventoryDimensionId) {
        this.variantInventoryDimensionId = variantInventoryDimensionId;
    }
}
