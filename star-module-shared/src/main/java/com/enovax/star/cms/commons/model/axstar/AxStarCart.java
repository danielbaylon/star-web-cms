package com.enovax.star.cms.commons.model.axstar;

import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailCartTicket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AxStarCart {

    //Unused variables

//    private String AffiliationLines; // [],
//    private String AttributeValues; // [],
//    private String CancellationChargeAmount; // null,
//    private String EstimatedShippingAmount; // null,
//    private String ChargeAmount; // 0;
//    private String ChargeLines; // [];
//    private String TaxViewLines; // [];
//    private String CustomerOrderModeValue; // 0;
//    private String DeliveryMode; // null;
//    private String DeliveryModeChargeAmount; // null;
//    private String DiscountCodes; // [];
//    private String TransactionTypeValue; // -1;
//    private String IncomeExpenseLines; // [];
//    private String IncomeExpenseTotalAmount; // 0;
//    private String PromotionLines; // []; TODO This will be used VERY SOON
//    private String QuotationExpiryDate; // null;
//    private String ReasonCodeLines; // [];
//    private String ReceiptEmail; // null;
//    private String RequestedDeliveryDate; // null;
//    private String TenderLines; // []; TODO This might be used soon
//    private String ExtensionProperties; // []

    private String id;

    private List<AxStarCartLine> cartLines = new ArrayList<>();

    private boolean isRequiredAmountPaid;
    private BigDecimal customerAccountDepositAmount;
    private boolean isDiscountFullyCalculated;
    private BigDecimal amountDue;
    private BigDecimal amountPaid;
    private String beginDateTime;
    private String businessDate = ""; //Nullable
    private int cartType; //Seems like 1 for Shopping Cart, 2 for Checkout Cart
    private int cartTypeValue; //Seems like 1 for Shopping Cart, 2 for Checkout Cart
    private Long channelId;
    private String comment;
    private String invoiceComment;
    private String customerId;
    private BigDecimal discountAmount;
    private boolean isReturnByReceipt;
    private boolean returnTransactionHasLoyaltyPayment;
    private boolean isFavorite;
    private boolean isRecurring;
    private boolean isSuspended;
    private String loyaltyCardId;
    private String modifiedDateTime;
    private String name = ""; //Nullable
    private String orderNumber = ""; //Nullable
    private BigDecimal availableDepositAmount;
    private BigDecimal overriddenDepositAmount = BigDecimal.ZERO; //Nullable
    private BigDecimal prepaymentAmountPaid;
    private BigDecimal prepaymentAppliedOnPickup;
    private BigDecimal requiredDepositAmount;
    private String salesId = ""; //Nullable
    private AxStarAddress shippingAddress; //Nullable
    private String staffId = ""; //Nullable
    private BigDecimal subtotalAmount;
    private BigDecimal subtotalAmountWithoutTax;
    private BigDecimal taxAmount;
    private BigDecimal taxAmountExclusive;
    private BigDecimal taxAmountInclusive;
    private BigDecimal taxOnCancellationCharge;
    private String taxOverrideCode = ""; //Nullable
    private String terminalId;
    private BigDecimal totalAmount;
    private BigDecimal totalManualDiscountAmount;
    private BigDecimal totalManualDiscountPercentage;
    private String warehouseId;
    private boolean isCreatedOffline;
    private int CartStatusValue;
    private int ReceiptTransactionTypeValue;
    private String receiptEmail;

    private AxStarCart checkoutCart = null;
    private List<AxRetailCartTicket> reservedTickets = new ArrayList<>();

    /*
    [Sample JSON]
    {
    "AffiliationLines": [],
    "IsRequiredAmountPaid": false,
    "CustomerAccountDepositAmount": 0,
    "IsDiscountFullyCalculated": true,
    "AmountDue": 0,
    "AmountPaid": 0,
    "AttributeValues": [],
    "BeginDateTime": "2016-04-24T17:32:13.0237947+08:00",
    "BusinessDate": null,
    "CancellationChargeAmount": null,
    "EstimatedShippingAmount": null,
    "CartLines": [
      {
        "LineId": "31b545357c354adb95219319594eaaef",
        "TaxOverrideCode": null,
        "ItemId": "1010100001",
        "Barcode": "",
        "Description": "",
        "InventoryDimensionId": "",
        "Comment": "",
        "ProductId": 5637149107,
        "WarehouseId": "SLMB2C",
        "Quantity": 5,
        "Price": 4,
        "ExtendedPrice": 20,
        "TaxAmount": 0,
        "ItemTaxGroupId": "",
        "TotalAmount": 20,
        "NetAmountWithoutTax": 20,
        "DiscountAmount": 0,
        "LineDiscount": 0,
        "LinePercentageDiscount": 0,
        "LineManualDiscountPercentage": 0,
        "LineManualDiscountAmount": 0,
        "UnitOfMeasureSymbol": "Ea",
        "ShippingAddress": null,
        "DeliveryMode": "",
        "DeliveryModeChargeAmount": null,
        "RequestedDeliveryDate": null,
        "ReturnTransactionId": "",
        "ReturnLineNumber": 0,
        "ReturnInventTransId": "",
        "IsVoided": false,
        "IsGiftCardLine": false,
        "SalesStatusValue": 0,
        "QuantityOrdered": null,
        "QuantityInvoiced": null,
        "StoreNumber": "",
        "SerialNumber": "",
        "IsPriceOverridden": false,
        "OriginalPrice": null,
        "IsInvoiceLine": false,
        "InvoiceId": "",
        "InvoiceAmount": 0,
        "PromotionLines": [],
        "DiscountLines": [],
        "ReasonCodeLines": [],
        "ChargeLines": [],
        "TaxRatePercent": 0,
        "IsCustomerAccountDeposit": false,
        "ExtensionProperties": []
      }
    ],
    "CartType": 1,
    "CartTypeValue": 1,
    "ChannelId": 5637149827,
    "ChargeAmount": 0,
    "ChargeLines": [],
    "TaxViewLines": [],
    "Comment": "",
    "InvoiceComment": "",
    "CustomerId": "",
    "CustomerOrderModeValue": 0,
    "DeliveryMode": null,
    "DeliveryModeChargeAmount": null,
    "DiscountAmount": 0,
    "DiscountCodes": [],
    "Id": "SC568D87458FA34F8C97689DEFBBDA3508",
    "TransactionTypeValue": -1,
    "IncomeExpenseLines": [],
    "IncomeExpenseTotalAmount": 0,
    "IsReturnByReceipt": false,
    "ReturnTransactionHasLoyaltyPayment": false,
    "IsFavorite": false,
    "IsRecurring": false,
    "IsSuspended": false,
    "LoyaltyCardId": "",
    "ModifiedDateTime": "2016-04-24T17:32:13.027+08:00",
    "Name": null,
    "OrderNumber": null,
    "AvailableDepositAmount": 0,
    "OverriddenDepositAmount": null,
    "PrepaymentAmountPaid": 0,
    "PrepaymentAppliedOnPickup": 0,
    "PromotionLines": [],
    "QuotationExpiryDate": null,
    "ReasonCodeLines": [],
    "ReceiptEmail": null,
    "RequestedDeliveryDate": null,
    "RequiredDepositAmount": 0,
    "SalesId": null,
    "ShippingAddress": null,
    "StaffId": null,
    "SubtotalAmount": 20,
    "SubtotalAmountWithoutTax": 20,
    "TaxAmount": 0,
    "TaxAmountExclusive": 0,
    "TaxAmountInclusive": 0,
    "TaxOnCancellationCharge": 0,
    "TaxOverrideCode": null,
    "TenderLines": [],
    "TerminalId": "",
    "TotalAmount": 20,
    "TotalManualDiscountAmount": 0,
    "TotalManualDiscountPercentage": 0,
    "WarehouseId": "SLMB2C",
    "IsCreatedOffline": false,
    "CartStatusValue": 1,
    "ReceiptTransactionTypeValue": 1,
    "ExtensionProperties": []
  }
     */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRequiredAmountPaid() {
        return isRequiredAmountPaid;
    }

    public void setRequiredAmountPaid(boolean requiredAmountPaid) {
        isRequiredAmountPaid = requiredAmountPaid;
    }

    public BigDecimal getCustomerAccountDepositAmount() {
        return customerAccountDepositAmount;
    }

    public void setCustomerAccountDepositAmount(BigDecimal customerAccountDepositAmount) {
        this.customerAccountDepositAmount = customerAccountDepositAmount;
    }

    public boolean isDiscountFullyCalculated() {
        return isDiscountFullyCalculated;
    }

    public void setDiscountFullyCalculated(boolean discountFullyCalculated) {
        isDiscountFullyCalculated = discountFullyCalculated;
    }

    public BigDecimal getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(BigDecimal amountDue) {
        this.amountDue = amountDue;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public int getCartType() {
        return cartType;
    }

    public void setCartType(int cartType) {
        this.cartType = cartType;
    }

    public int getCartTypeValue() {
        return cartTypeValue;
    }

    public void setCartTypeValue(int cartTypeValue) {
        this.cartTypeValue = cartTypeValue;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInvoiceComment() {
        return invoiceComment;
    }

    public void setInvoiceComment(String invoiceComment) {
        this.invoiceComment = invoiceComment;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public boolean isReturnByReceipt() {
        return isReturnByReceipt;
    }

    public void setReturnByReceipt(boolean returnByReceipt) {
        isReturnByReceipt = returnByReceipt;
    }

    public boolean isReturnTransactionHasLoyaltyPayment() {
        return returnTransactionHasLoyaltyPayment;
    }

    public void setReturnTransactionHasLoyaltyPayment(boolean returnTransactionHasLoyaltyPayment) {
        this.returnTransactionHasLoyaltyPayment = returnTransactionHasLoyaltyPayment;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    public boolean isSuspended() {
        return isSuspended;
    }

    public void setSuspended(boolean suspended) {
        isSuspended = suspended;
    }

    public String getLoyaltyCardId() {
        return loyaltyCardId;
    }

    public void setLoyaltyCardId(String loyaltyCardId) {
        this.loyaltyCardId = loyaltyCardId;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getAvailableDepositAmount() {
        return availableDepositAmount;
    }

    public void setAvailableDepositAmount(BigDecimal availableDepositAmount) {
        this.availableDepositAmount = availableDepositAmount;
    }

    public BigDecimal getOverriddenDepositAmount() {
        return overriddenDepositAmount;
    }

    public void setOverriddenDepositAmount(BigDecimal overriddenDepositAmount) {
        this.overriddenDepositAmount = overriddenDepositAmount;
    }

    public BigDecimal getPrepaymentAmountPaid() {
        return prepaymentAmountPaid;
    }

    public void setPrepaymentAmountPaid(BigDecimal prepaymentAmountPaid) {
        this.prepaymentAmountPaid = prepaymentAmountPaid;
    }

    public BigDecimal getPrepaymentAppliedOnPickup() {
        return prepaymentAppliedOnPickup;
    }

    public void setPrepaymentAppliedOnPickup(BigDecimal prepaymentAppliedOnPickup) {
        this.prepaymentAppliedOnPickup = prepaymentAppliedOnPickup;
    }

    public BigDecimal getRequiredDepositAmount() {
        return requiredDepositAmount;
    }

    public void setRequiredDepositAmount(BigDecimal requiredDepositAmount) {
        this.requiredDepositAmount = requiredDepositAmount;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public AxStarAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AxStarAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public BigDecimal getSubtotalAmount() {
        return subtotalAmount;
    }

    public void setSubtotalAmount(BigDecimal subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public BigDecimal getSubtotalAmountWithoutTax() {
        return subtotalAmountWithoutTax;
    }

    public void setSubtotalAmountWithoutTax(BigDecimal subtotalAmountWithoutTax) {
        this.subtotalAmountWithoutTax = subtotalAmountWithoutTax;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTaxAmountExclusive() {
        return taxAmountExclusive;
    }

    public void setTaxAmountExclusive(BigDecimal taxAmountExclusive) {
        this.taxAmountExclusive = taxAmountExclusive;
    }

    public BigDecimal getTaxAmountInclusive() {
        return taxAmountInclusive;
    }

    public void setTaxAmountInclusive(BigDecimal taxAmountInclusive) {
        this.taxAmountInclusive = taxAmountInclusive;
    }

    public BigDecimal getTaxOnCancellationCharge() {
        return taxOnCancellationCharge;
    }

    public void setTaxOnCancellationCharge(BigDecimal taxOnCancellationCharge) {
        this.taxOnCancellationCharge = taxOnCancellationCharge;
    }

    public String getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(String taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalManualDiscountAmount() {
        return totalManualDiscountAmount;
    }

    public void setTotalManualDiscountAmount(BigDecimal totalManualDiscountAmount) {
        this.totalManualDiscountAmount = totalManualDiscountAmount;
    }

    public BigDecimal getTotalManualDiscountPercentage() {
        return totalManualDiscountPercentage;
    }

    public void setTotalManualDiscountPercentage(BigDecimal totalManualDiscountPercentage) {
        this.totalManualDiscountPercentage = totalManualDiscountPercentage;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public boolean isCreatedOffline() {
        return isCreatedOffline;
    }

    public void setCreatedOffline(boolean createdOffline) {
        isCreatedOffline = createdOffline;
    }

    public int getCartStatusValue() {
        return CartStatusValue;
    }

    public void setCartStatusValue(int cartStatusValue) {
        CartStatusValue = cartStatusValue;
    }

    public int getReceiptTransactionTypeValue() {
        return ReceiptTransactionTypeValue;
    }

    public void setReceiptTransactionTypeValue(int receiptTransactionTypeValue) {
        ReceiptTransactionTypeValue = receiptTransactionTypeValue;
    }

    public List<AxStarCartLine> getCartLines() {
        return cartLines;
    }

    public void setCartLines(List<AxStarCartLine> cartLines) {
        this.cartLines = cartLines;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public AxStarCart getCheckoutCart() {
        return checkoutCart;
    }

    public void setCheckoutCart(AxStarCart checkoutCart) {
        this.checkoutCart = checkoutCart;
    }

    public List<AxRetailCartTicket> getReservedTickets() {
        return reservedTickets;
    }

    public void setReservedTickets(List<AxRetailCartTicket> reservedTickets) {
        this.reservedTickets = reservedTickets;
    }
}
