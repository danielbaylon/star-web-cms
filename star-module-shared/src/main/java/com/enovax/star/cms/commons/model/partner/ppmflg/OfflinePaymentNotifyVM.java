package com.enovax.star.cms.commons.model.partner.ppmflg;

public class OfflinePaymentNotifyVM extends OfflinePaymentResultVM {
	
	private String msg;
    private String baseUrl;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}
