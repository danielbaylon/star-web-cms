package com.enovax.star.cms.commons.util;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * Created by jensen on 28/3/16.
 */
public class NvxNumberUtils {

    public static final RoundingMode DEFAULT_ROUND_MODE = RoundingMode.HALF_UP;


    public static final String DEFAULT_SYSPARAM_DECIMAL_FORMAT = "##0.00";
    public static final String DEFAULT_CURRENCY_FORMAT = "#,##0.00";

    public static Integer moneyToCents(BigDecimal money) {
        return money.multiply(BigDecimal.valueOf(100)).intValue();
    }

    public static BigDecimal centsToMoney(int priceInCents) {
        return BigDecimal.valueOf(priceInCents, 2);
    }

    public static BigDecimal centsToMoney(long priceInCents) {
        return BigDecimal.valueOf(priceInCents, 2);
    }

    public static String formatToCurrency(BigDecimal number) {
        return formatToCurrency(number, "", DEFAULT_ROUND_MODE);
    }

    public static String formatToCurrency(BigDecimal number, String currencySymbol) {
        return formatToCurrency(number, currencySymbol, DEFAULT_ROUND_MODE);
    }

    public static String formatToCurrency(BigDecimal number, String currencySymbol, RoundingMode mode) {
        final DecimalFormat format = new DecimalFormat(DEFAULT_CURRENCY_FORMAT);
        format.setRoundingMode(mode);

        return (StringUtils.isEmpty(currencySymbol) ? "" : currencySymbol) + format.format(number);
    }

    public static String formatToBigDecimalString(BigDecimal number) {
        return formatToBigDecimalString(number, "", DEFAULT_ROUND_MODE);
    }

    public static String formatToBigDecimalString(BigDecimal number, String currencySymbol, RoundingMode mode) {
        final DecimalFormat format = new DecimalFormat(DEFAULT_SYSPARAM_DECIMAL_FORMAT);
        format.setRoundingMode(mode);

        return (StringUtils.isEmpty(currencySymbol) ? "" : currencySymbol) + format.format(number);
    }

    public static BigDecimal parseBigDecimal(String value, String format) throws ParseException {
        final DecimalFormat df = new DecimalFormat(format);
        df.setParseBigDecimal(true);
        return (BigDecimal)df.parse(value);
    }

    public static BigDecimal percentOff(Integer priceInCents, Integer percent) {
        BigDecimal value = null;
        if ((priceInCents != null) && (percent != null)) {
            value = new BigDecimal(priceInCents).divide(new BigDecimal(100))
              .subtract(
                new BigDecimal(priceInCents * percent)
                  .divide(new BigDecimal(10000)));
        }
        return value;
    }

    public static Integer percentOffInCents(Integer priceInCents,
                                            Integer percent) {
        return percentOff(priceInCents, percent).multiply(new BigDecimal(100))
          .intValue();
    }

    public static Long percentOffInCents(Long priceInCents,
                                         Integer percent) {
        return percentOff(priceInCents, percent).multiply(new BigDecimal(100))
            .longValue();
    }

    public static BigDecimal percentOff(Long priceInCents, Integer percent) {
        BigDecimal value = null;
        if ((priceInCents != null) && (percent != null)) {
            value = new BigDecimal(priceInCents).divide(new BigDecimal(100))
                .subtract(
                    new BigDecimal(priceInCents * percent)
                        .divide(new BigDecimal(10000)));
        }
        return value;
    }

}
