package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.model.partner.ppslm.InventoryReportModel;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportFilterVM;
import com.enovax.star.cms.commons.model.partner.ppslm.ReportResult;
import com.enovax.star.cms.commons.model.partner.ppslm.TransReportModel;

/**
 * Created by lavanya on 8/11/16.
 */
public interface InventoryReportDao {
    public ReportResult<InventoryReportModel> generateReport(ReportFilterVM filter, Integer skip, Integer take,
                                                             String orderBy, boolean asc, boolean isCount) throws Exception;
}
