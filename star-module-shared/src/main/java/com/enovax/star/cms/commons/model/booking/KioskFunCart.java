package com.enovax.star.cms.commons.model.booking;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCart;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketTableEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxPINRedemptionStartData;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrder;
import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrderSearch;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * AX Cart connect is not the same as FunCart
 * 
 * @author Justin
 *
 */
public class KioskFunCart extends FunCart {

    @JsonIgnore
    private AxCart kioskAxCart;

    @JsonIgnore
    private AxPINRedemptionStartData axPINRedemptionStartData;
    
    @JsonIgnore
    private AxSalesOrderSearch axSalesOrderSearch;
    
    @JsonIgnore
    private List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntities;
    

    public AxCart getKioskAxCart() {
        return kioskAxCart;
    }

    public void setKioskAxCart(AxCart kioskAxCart) {
        this.kioskAxCart = kioskAxCart;
    }

    public AxPINRedemptionStartData getAxPINRedemptionStartData() {
        return axPINRedemptionStartData;
    }

    public void setAxPINRedemptionStartData(AxPINRedemptionStartData axPINRedemptionStartData) {
        this.axPINRedemptionStartData = axPINRedemptionStartData;
    }

	public void setAxSalesOrderSearch(AxSalesOrderSearch axSalesOrderSearch) {
		this.axSalesOrderSearch = axSalesOrderSearch;
	}
	
	public AxSalesOrderSearch getAxSalesOrderSearch() {
		return axSalesOrderSearch;
	}
	
	public void setAxCartRetailTicketTableEntities(
			List<AxCartRetailTicketTableEntity> axCartRetailTicketTableEntities) {
		this.axCartRetailTicketTableEntities = axCartRetailTicketTableEntities;
	}
	
	public List<AxCartRetailTicketTableEntity> getAxCartRetailTicketTableEntities() {
		return axCartRetailTicketTableEntities;
	}
	
}
