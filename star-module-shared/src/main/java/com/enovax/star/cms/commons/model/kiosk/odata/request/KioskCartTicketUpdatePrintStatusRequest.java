package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartRetailTicketEntity;
import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTicketUpdatePrintStatusCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskCartTicketUpdatePrintStatusRequest {

	/**
	 * Collection(SDC_TicketingExtension.Entity.CartRetailTicketEntity)
	 * 
	 */
	@JsonProperty("bindingParameter")
	private List<AxCartRetailTicketEntity> axCartRetailTicketEntities;

	@JsonProperty("CartTicketUpdatePrintStatusCriteria")
	private AxCartTicketUpdatePrintStatusCriteria axCartTicketUpdatePrintStatusCriteria;

	public List<AxCartRetailTicketEntity> getAxCartRetailTicketEntities() {
		return axCartRetailTicketEntities;
	}

	public void setAxCartRetailTicketEntities(List<AxCartRetailTicketEntity> axCartRetailTicketEntities) {
		this.axCartRetailTicketEntities = axCartRetailTicketEntities;
	}

	public AxCartTicketUpdatePrintStatusCriteria getAxCartTicketUpdatePrintStatusCriteria() {
		return axCartTicketUpdatePrintStatusCriteria;
	}

	public void setAxCartTicketUpdatePrintStatusCriteria(
			AxCartTicketUpdatePrintStatusCriteria axCartTicketUpdatePrintStatusCriteria) {
		this.axCartTicketUpdatePrintStatusCriteria = axCartTicketUpdatePrintStatusCriteria;
	}

}
