package com.enovax.star.cms.commons.constant.ppslm;

/**
 * Created by jennylynsze on 1/11/17.
 */
public enum TAAccessRight {
    attractions,
    reports,
    accMgmt,
    purchaseTickets,
    revalidateTickets,
    redemptionStatus,
    viewInventory,
    generateBundle,
    packages,
    events,
    subAccMgmt,
    reserveWOT,
    viewWOTReceipts,
    manageDeposits,
    topupDeposits,
    offlinePayment
}
