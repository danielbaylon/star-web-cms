package com.enovax.star.cms.commons.model.partner.ppmflg;

import com.enovax.star.cms.commons.model.axchannel.customerext.AxCapacityGroup;
import com.enovax.star.cms.commons.model.axstar.AxCustomerGroup;

import java.util.List;

/**
 * Created by lavanya on 24/8/16.
 */
public class PartnerMaintenanceVM {
    private PartnerVM partnerVM;
    private List<PartnerTypeVM> partnerTypeVMs;
    private List<AdminAccountVM> adminAccountVms;
    private List<RevalFeeItemVM> revalFeeItemVMs;
    private List<CountryVM> ctyVMs;
    private Integer ticketRevalidatePeriod;
    private boolean isPending;
    private List<DropDownVM> docTypeVMs;
    private List<ProductTierVM> tierVMs;
    private List<AxCustomerGroup> customerGroupList;
    private List<AxCapacityGroup> capacityGroupList;

    public PartnerMaintenanceVM() {
    }

    public PartnerMaintenanceVM(PartnerVM partnerVM, List<PartnerTypeVM> partnerTypeVMs, List<AdminAccountVM> adminAccountVms,
                                List<RevalFeeItemVM> revalFeeItemVMs, List<CountryVM> ctyVMs, Integer ticketRevalidatePeriod,
                                boolean isPending, List<DropDownVM> docType, List<ProductTierVM> tierVMs, List<AxCustomerGroup> axCustomerGroupList,
                                List<AxCapacityGroup> axCapacityGroupList) {
        this.partnerVM = partnerVM;
        this.partnerTypeVMs = partnerTypeVMs;
        this.adminAccountVms = adminAccountVms;
        this.revalFeeItemVMs = revalFeeItemVMs;
        this.ctyVMs = ctyVMs;
        this.ticketRevalidatePeriod = ticketRevalidatePeriod;
        this.isPending = isPending;
        this.docTypeVMs = docType;
        this.tierVMs = tierVMs;
        this.customerGroupList = axCustomerGroupList;
        this.capacityGroupList = axCapacityGroupList;
    }

    public PartnerVM getPartnerVM() {
        return partnerVM;
    }

    public void setPartnerVM(PartnerVM partnerVM) {
        this.partnerVM = partnerVM;
    }

    public List<ProductTierVM> getTierVMs() {
        return tierVMs;
    }

    public void setTierVMs(List<ProductTierVM> tierVMs) {
        this.tierVMs = tierVMs;
    }

    public List<DropDownVM> getDocTypeVMs() {
        return docTypeVMs;
    }

    public void setDocTypeVMs(List<DropDownVM> docTypeVMs) {
        this.docTypeVMs = docTypeVMs;
    }

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public Integer getTicketRevalidatePeriod() {
        return ticketRevalidatePeriod;
    }

    public void setTicketRevalidatePeriod(Integer ticketRevalidatePeriod) {
        this.ticketRevalidatePeriod = ticketRevalidatePeriod;
    }

    public List<CountryVM> getCtyVMs() {
        return ctyVMs;
    }

    public void setCtyVMs(List<CountryVM> ctyVMs) {
        this.ctyVMs = ctyVMs;
    }

    public List<RevalFeeItemVM> getRevalFeeItemVMs() {
        return revalFeeItemVMs;
    }

    public void setRevalFeeItemVMs(List<RevalFeeItemVM> revalFeeItemVMs) {
        this.revalFeeItemVMs = revalFeeItemVMs;
    }

    public List<AdminAccountVM> getAdminAccountVms() {
        return adminAccountVms;
    }

    public void setAdminAccountVms(List<AdminAccountVM> adminAccountVms) {
        this.adminAccountVms = adminAccountVms;
    }

    public List<PartnerTypeVM> getPartnerTypeVMs() {
        return partnerTypeVMs;
    }

    public void setPartnerTypeVMs(List<PartnerTypeVM> partnerTypeVMs) {
        this.partnerTypeVMs = partnerTypeVMs;
    }

    public List<AxCustomerGroup> getCustomerGroupList() {
        return customerGroupList;
    }

    public void setCustomerGroupList(List<AxCustomerGroup> customerGroupList) {
        this.customerGroupList = customerGroupList;
    }

    public List<AxCapacityGroup> getCapacityGroupList() {
        return capacityGroupList;
    }

    public void setCapacityGroupList(List<AxCapacityGroup> capacityGroupList) {
        this.capacityGroupList = capacityGroupList;
    }
}
