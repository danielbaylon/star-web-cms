package com.enovax.star.cms.commons.constant.ppslm;

/**
 * Created by jennylynsze on 7/15/16.
 */
public enum TransItemType {
    Standard,
    Topup
}
