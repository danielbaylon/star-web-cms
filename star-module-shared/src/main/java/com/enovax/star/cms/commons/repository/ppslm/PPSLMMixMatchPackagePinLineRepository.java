package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackagePinLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 1/3/17.
 */
@Repository
public interface PPSLMMixMatchPackagePinLineRepository extends JpaRepository<PPSLMMixMatchPackagePinLine, Integer> {
    List<PPSLMMixMatchPackagePinLine> findByPackageItemId(Integer packageItemId);
}
