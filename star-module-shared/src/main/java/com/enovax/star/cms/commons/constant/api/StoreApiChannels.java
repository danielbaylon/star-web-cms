package com.enovax.star.cms.commons.constant.api;

public enum StoreApiChannels {

    B2C_SLM("b2c-slm", "SDC"),
    B2C_MFLG("b2c-mflg", "MF"),
    PARTNER_PORTAL_SLM("partner-portal-slm", "STAR"),
    PARTNER_PORTAL_MFLG("partner-portal-mflg", "STAR"),
    KIOSK_SLM("kiosk-slm", "STAR"),
    KIOSK_MFLG("kiosk-mflg", "STAR")
    ;

    public final String code;
    public final String receiptPrefix;

    StoreApiChannels(String code, String receiptPrefix) {
        this.code = code;
        this.receiptPrefix = receiptPrefix;
    }

    public static StoreApiChannels fromCode(String code) {
        if (B2C_SLM.code.equals(code)) {
            return B2C_SLM;
        }
        if (B2C_MFLG.code.equals(code)) {
            return B2C_MFLG;
        }
        if (PARTNER_PORTAL_SLM.code.equals(code)) {
            return PARTNER_PORTAL_SLM;
        }
        if (PARTNER_PORTAL_MFLG.code.equals(code)) {
            return PARTNER_PORTAL_MFLG;
        }
        if (KIOSK_SLM.code.equals(code)) {
            return KIOSK_SLM;
        }
        if (KIOSK_MFLG.code.equals(code)) {
            return KIOSK_MFLG;
        }

        throw new IllegalArgumentException("Channel not found for input.");
    }
}