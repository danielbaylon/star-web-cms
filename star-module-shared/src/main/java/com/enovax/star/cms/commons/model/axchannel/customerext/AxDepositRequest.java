package com.enovax.star.cms.commons.model.axchannel.customerext;


import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by houtao on 17/8/16.
 */
public class AxDepositRequest {

    // basic details
    private String accountNum;
    private BigDecimal amount;
    private String currencyCode;
    private Date documentDate;
    private String documentNum;
    private Date dueDate;
    private Date postingDate;
    private String txt;
    private String tenderTypeId;


    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Date getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentNum() {
        return documentNum;
    }

    public void setDocumentNum(String documentNum) {
        this.documentNum = documentNum;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public String getTenderTypeId() {
        return tenderTypeId;
    }

    public void setTenderTypeId(String tenderTypeId) {
        this.tenderTypeId = tenderTypeId;
    }
}
