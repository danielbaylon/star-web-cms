package com.enovax.star.cms.commons.model.product.promotions;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jennylynsze on 9/12/16.
 */
public class PromotionLinkMainPromotion {
    private ProductRelatedPromotion promotion;
    private Map<String, PromotionRelatedProduct> relatedItems = new HashMap<>();

    public ProductRelatedPromotion getPromotion() {
        return promotion;
    }

    public void setPromotion(ProductRelatedPromotion promotion) {
        this.promotion = promotion;
    }

    public Map<String, PromotionRelatedProduct> getRelatedItems() {
        return relatedItems;
    }

    public void setRelatedItems(Map<String, PromotionRelatedProduct> relatedItems) {
        this.relatedItems = relatedItems;
    }
}
