package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AxJournalTransaction {

	@JsonProperty("Id")
	private String id;

	@JsonProperty("StoreId")
	private String storeId;

	@JsonProperty("TerminalId")
	private String terminalId;

	@JsonProperty("StaffId")
	private String staffId;

	@JsonProperty("ShiftId")
	private String shiftId;

	@JsonProperty("ShiftTerminalId")
	private String shiftTerminal;

	@JsonProperty("Description")
	private String description;

	@JsonProperty("ReceiptId")
	private String receiptId;

	@JsonProperty("GrossAmount")
	private String grossAmount;

	@JsonProperty("TotalAmount")
	private String totalAmount;

	@JsonProperty("CreatedDateTime")
	private String createdDateTime;

	@JsonProperty("TransactionTypeValue")
	private String transactionTypeValue;

	@JsonProperty("TransactionStatusValue")
	private String transactionStatusValue;

	/**
	 * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
	 * 
	 */
	@JsonProperty("ExtensionProperties")
	private List<AxCommerceProperty> axExtensionProperties;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getShiftId() {
		return shiftId;
	}

	public void setShiftId(String shiftId) {
		this.shiftId = shiftId;
	}

	public String getShiftTerminal() {
		return shiftTerminal;
	}

	public void setShiftTerminal(String shiftTerminal) {
		this.shiftTerminal = shiftTerminal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(String grossAmount) {
		this.grossAmount = grossAmount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getTransactionTypeValue() {
		return transactionTypeValue;
	}

	public void setTransactionTypeValue(String transactionTypeValue) {
		this.transactionTypeValue = transactionTypeValue;
	}

	public String getTransactionStatusValue() {
		return transactionStatusValue;
	}

	public void setTransactionStatusValue(String transactionStatusValue) {
		this.transactionStatusValue = transactionStatusValue;
	}

	public List<AxCommerceProperty> getAxExtensionProperties() {
		return axExtensionProperties;
	}

	public void setAxExtensionProperties(List<AxCommerceProperty> axExtensionProperties) {
		this.axExtensionProperties = axExtensionProperties;
	}

}
