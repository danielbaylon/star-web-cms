package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;

/**
 * Created by jensen on 3/9/16.
 */
public class TelemoneyProcessingData {
    private StoreApiChannels channel;
    private String receiptNumber;
    private String sid;

    public StoreApiChannels getChannel() {
        return channel;
    }

    public void setChannel(StoreApiChannels channel) {
        this.channel = channel;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
