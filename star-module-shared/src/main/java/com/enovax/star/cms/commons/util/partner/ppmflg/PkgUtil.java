package com.enovax.star.cms.commons.util.partner.ppmflg;

import com.enovax.star.cms.commons.constant.ppmflg.PkgStatus;
import com.enovax.star.cms.commons.constant.ppmflg.TicketMediaType;
import com.enovax.star.cms.commons.constant.ppmflg.TicketType;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransactionItem;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackage;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGMixMatchPackageItem;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by jennylynsze on 5/17/16.
 */
public class PkgUtil {
    /**
     * @param itransItems
     * @param ipkgQtyPerProd
     * @param itemCode
     * @return return original list when ipkgQtyPerProd is null, return empty
     *         list when quantity can not match with user defined quantity.
     *         return correct item list when match with user defined quantity.
     */
    public static List<PPMFLGInventoryTransactionItem> loadItemsToBundledItem(
            List<PPMFLGInventoryTransactionItem> itransItems, Integer ipkgQtyPerProd,
            String ipkgTktType, String itemCode, boolean finalSubmit) {
        List<PPMFLGInventoryTransactionItem> oItems = filterByStandardItemCode(
                itransItems, itemCode);
        oItems = filterByTktType(oItems, ipkgTktType);
        if (ipkgQtyPerProd != null && ipkgQtyPerProd > 0) {
  //          Collections.sort(oItems); //TODO sorting
            int usedQty = 0;
            for (PPMFLGInventoryTransactionItem item : oItems) {
                if (item.getUnpackagedQty() > 0 && (usedQty < ipkgQtyPerProd)) {
                    if ((!ipkgQtyPerProd.equals(usedQty))
                            && (item.getUnpackagedQty() + usedQty) <= ipkgQtyPerProd) {
                        item.setPkgQty(item.getUnpackagedQty());
                        usedQty = usedQty + item.getUnpackagedQty();
                    } else if ((!ipkgQtyPerProd.equals(usedQty))
                            && (item.getUnpackagedQty() + usedQty) > ipkgQtyPerProd) {
                        item.setPkgQty(ipkgQtyPerProd - usedQty);
                        usedQty = ipkgQtyPerProd;
                    }
                }
            }
            if (!ipkgQtyPerProd.equals(usedQty) && finalSubmit) {
                // Return empty list when can not meet user defined quantity
                return new ArrayList<PPMFLGInventoryTransactionItem>();
            }
            ArrayList<PPMFLGInventoryTransactionItem> oItemsWithpkgQty = new ArrayList<PPMFLGInventoryTransactionItem>();
            for (int i = 0; i < oItems.size(); i++) {
                if (oItems.get(i).getPkgQty() != null) {
                    oItemsWithpkgQty.add(oItems.get(i));
                }
            }
            return oItemsWithpkgQty;
        } else {
            return oItems;
        }
    }

    public static List<PPMFLGInventoryTransactionItem> loadItemsToBundledItem(
            List<PPMFLGInventoryTransactionItem> itransItems, Integer ipkgQtyPerProd,
            String ipkgTktType, String itemCode) {
        return PkgUtil.loadItemsToBundledItem(itransItems, ipkgQtyPerProd,
                ipkgTktType, itemCode, true);
    }
    public static Date getLatestExpiringDate(
            List<PPMFLGInventoryTransactionItem> itransItems) {
        Date latestExpireDt = null;
        for (PPMFLGInventoryTransactionItem item : itransItems) {
            Date valiDate = item.getValidityEndDate();
            if (latestExpireDt == null || latestExpireDt.after(valiDate)) {
                latestExpireDt = valiDate;
            }
        }
        return latestExpireDt;
    }

    /**
     * @param itransItems
     *            all the items submit by user
     * @return all the itemcode include item can not put into the package
     */
    public static Set<String> getAllItemCode(
            List<PPMFLGInventoryTransactionItem> itransItems) {
        Set<String> codeSet = new TreeSet<String>();
        for (PPMFLGInventoryTransactionItem item : itransItems) {
            codeSet.add(item.getItemProductCode()); //TODO evaluate if product code is good enought
        }
        return codeSet;
    }

//    public static Set<Integer> getAllItemCodeByPkgItem(
//            List<MixMatchPackageItemVM> mmItems) {
//        Set<Integer> codeSet = new TreeSet<Integer>();
//        for (MixMatchPackageItem item : mmItems) {
//            codeSet.add(item.getItemCode());
//        }
//        return codeSet;
//    }

    /**
     * @param itransItems
     *            all the items submit by user
     * @param ipkgQtyPerProd
     * @param ipkgTktType
     * @return when ipkgQtyPerProd,ipkgTktType not null, will return the item
     *         can add into the pkg
     */
    public static List<PPMFLGInventoryTransactionItem> getAvailableListUnderPkg(
            List<PPMFLGInventoryTransactionItem> itransItems, Integer ipkgQtyPerProd,
            String ipkgTktType, boolean finalSubmit) {
        Set<String> codeSet = getAllItemCode(itransItems);
        List<PPMFLGInventoryTransactionItem> oItems = new ArrayList<PPMFLGInventoryTransactionItem>();
        for (String code : codeSet) {
            List<PPMFLGInventoryTransactionItem> tempList = loadItemsToBundledItem(
                    itransItems, ipkgQtyPerProd, ipkgTktType, code, finalSubmit);
            oItems.addAll(tempList);
        }
        return oItems;
    }

    public static List<PPMFLGInventoryTransactionItem> getAvailableListUnderPkg(
            List<PPMFLGInventoryTransactionItem> itransItems, Integer ipkgQtyPerProd,
            String ipkgTktType) {
        return PkgUtil.getAvailableListUnderPkg(itransItems, ipkgQtyPerProd,
                ipkgTktType, true);
    }

    public static String generatePkgName(String partnerCode) {
        String dateStr = NvxDateUtils.formatDate(new Date(), "ddMMyyyy");
        String surfix = RandomStringUtils.randomAlphanumeric(4).toUpperCase();
        String pkgName = partnerCode + "_" + dateStr + "_" + surfix;
        return pkgName;
    }

    private static List<PPMFLGInventoryTransactionItem> filterByTktType(
            List<PPMFLGInventoryTransactionItem> itransItems, String ipkgTktType) {
        if (ipkgTktType != null) {
            ArrayList<PPMFLGInventoryTransactionItem> oItems = new ArrayList<PPMFLGInventoryTransactionItem>();
            for (int i = 0; i < itransItems.size(); i++) {
                String otype = itransItems.get(i).getTicketType();
                if (otype.equals(ipkgTktType)
                        || (TicketType.isStandard(otype) && TicketType
                        .isStandard(ipkgTktType))) {
                    oItems.add(itransItems.get(i));
                }
            }
            return oItems;
        }
        return itransItems;
    }

    private static List<PPMFLGInventoryTransactionItem> filterByStandardItemCode(
            List<PPMFLGInventoryTransactionItem> itransItems, String itemCode) {
        if (itemCode != null) {
            ArrayList<PPMFLGInventoryTransactionItem> oItems = new ArrayList<>();
            for (int i = 0; i < itransItems.size(); i++) {
                if (itransItems.get(i).getItemProductCode().equals(itemCode)) { //TODO dunno if correct
                    //TODO always true that is Standard
                    oItems.add(itransItems.get(i));
                }
            }
            return oItems;
        }
        return itransItems;
    }

    public static List<Integer> getTransIdsFromStr(String transItemIdsStr) {
        String[] transItemIds = transItemIdsStr.split(",");
        List<Integer> tranIdsInt = new ArrayList<>();
        for (int i = 0; i < transItemIds.length; i++) {
            tranIdsInt.add(Integer.parseInt(transItemIds[i]));
        }
        return tranIdsInt;
    }

    public static Boolean verifyItemWithPackageType(
            PPMFLGInventoryTransactionItem transItem, String ticketType) {
        String otype = transItem.getTicketType();
        if (otype.equals(ticketType)
                || (TicketType.isStandard(otype) && TicketType
                .isStandard(ticketType))) {
            return true;
        }
        return false;
    }

    public static String[] getAllDisplayStatus() {
        return new String[] { PkgStatus.Available.toString(),
                PkgStatus.Expired.toString(), PkgStatus.Redeemed.toString(),
                PkgStatus.Deactivated.toString()};
    }

    public static int getQtyRedeemedFromSt(PPMFLGMixMatchPackage pkg,
                                           Integer remainQty) {
        int allQty = 0;
        for (PPMFLGMixMatchPackageItem item : pkg.getPkgItems()) {
            allQty = allQty + item.getQty();
        }
        int qty = pkg.getQty() - remainQty
                / (allQty / pkg.getQty());
        return qty;
    }

    public static String getTicketMediaTypeAllowed(List<ProductExtViewModel> productExtViewModelList) {
        return getTicketMediaTypeAllowed(productExtViewModelList, null);
    }

    public static String getTicketMediaTypeAllowed(List<ProductExtViewModel> productExtViewModelList, List<String> listingIdsToExclude) {
        int noOfPax = 0;
        boolean isBarcode = false;
        String barcodeItemId = "";
        boolean isCapacity = false;
        String capacityItemId = "";

        for(ProductExtViewModel extViewModel: productExtViewModelList) {

            if(listingIdsToExclude != null && listingIdsToExclude.contains(extViewModel.getProductId())) {
                continue;
            }

            //check media type, if one is pin, the rest should all be pin
            if("PIN".equalsIgnoreCase(extViewModel.getMediaTypeId())) {
                return TicketMediaType.Pincode.name();
            }

            //check no of pax
            if(noOfPax == 0) {
                noOfPax = extViewModel.getNoOfPax();
            }else if(noOfPax != extViewModel.getNoOfPax()) {
                return TicketMediaType.Pincode.name();
            }

            //isbarcode
            if(extViewModel.getPrinting() == 3) {
                isBarcode = true;
                if(StringUtils.isEmpty(barcodeItemId)) {
                    barcodeItemId = extViewModel.getItemId();
                }else if(!barcodeItemId.equals(extViewModel.getItemId())) {
                    return TicketMediaType.Pincode.name();
                }
            }else if(isBarcode) {
                return TicketMediaType.Pincode.name();
            }

            //check capacity / or just check if is event since the package is complicated one.... aiyo...
            if(extViewModel.isEvent()) {
                isCapacity = true;
                if(StringUtils.isEmpty(capacityItemId)) {
                    capacityItemId = extViewModel.getItemId();
                }else if(!capacityItemId.equals(extViewModel.getItemId())) {
                    return TicketMediaType.Pincode.name();
                }
            }else if(isCapacity) {
                return TicketMediaType.Pincode.name();
            }
        }

        return ""; //no restriction
    }

}
