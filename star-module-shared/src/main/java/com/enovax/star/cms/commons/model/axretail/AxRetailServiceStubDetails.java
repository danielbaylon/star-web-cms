package com.enovax.star.cms.commons.model.axretail;

import com.enovax.star.cms.commons.ws.axretail.ISDCRetailTicketTableService;
import com.enovax.star.cms.commons.ws.axretail.SDCRetailTicketTableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class AxRetailServiceStubDetails {

    private Logger log = LoggerFactory.getLogger(getClass());

    private SDCRetailTicketTableService ws;
    private ISDCRetailTicketTableService stub;
    private String apiUrl;
    private boolean init = false;

    public AxRetailServiceStubDetails(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public boolean initialise() {
        try {

            ws = new SDCRetailTicketTableService(new URL(apiUrl));
            stub = ws.getBasicHttpBindingISDCRetailTicketTableService();

            log.info("Initialised AX Retail Service Client for " + this.apiUrl);
            this.init = true;
        } catch (Exception e) {
            log.error("Error initialising AX Retail Service Client for " + this.apiUrl, e);
            this.init = false;
        }

        return this.init;
    }

    public SDCRetailTicketTableService getWs() {
        return ws;
    }

    public ISDCRetailTicketTableService getStub() {
        return stub;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public boolean isInit() {
        return init;
    }
}
