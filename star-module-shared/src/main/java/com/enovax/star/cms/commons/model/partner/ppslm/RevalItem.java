package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;

import java.util.Date;

/**
 * Created by jennylynsze on 8/13/16.
 */
public class RevalItem {
    private Integer id;
    private Integer transactionId;
    private String productId;
    private String productName;
    private Date dateOfVisit;
    private String eventGroupId;
    private String eventLineId;
    private String itemListingId;
    private String itemProductCode;
    private String itemType;
    private String transItemType;
    private String displayName;
    private String displayDetails;
    private String ticketType;
    private Integer qty;
    private Integer unpackagedQty;
    private String mainItemProductId;
    private String mainItemListingId;


    public RevalItem(PPSLMInventoryTransactionItem item) {
        this.id = item.getId();
        this.transactionId = item.getTransactionId();
        this.productId = item.getProductId();
        this.productName = item.getProductName();
        this.dateOfVisit = item.getDateOfVisit();
        this.eventGroupId = item.getEventGroupId();
        this.eventLineId = item.getEventLineId();
        this.itemListingId = item.getItemListingId();
        this.itemProductCode = item.getItemProductCode();
        this.itemType = item.getItemType();
        this.transItemType = item.getTransItemType();
        this.displayName = item.getDisplayName();
        this.displayDetails = item.getDisplayDetails();
        this.ticketType = item.getTicketType();
        this.qty = item.getQty();
        this.unpackagedQty = item.getUnpackagedQty();
        this.mainItemProductId = item.getMainItemProductId();
        this.mainItemListingId = item.getMainItemListingId();
    }

    public RevalItem() {
        // TODO Auto-generated constructor stub
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getItemListingId() {
        return itemListingId;
    }

    public void setItemListingId(String itemListingId) {
        this.itemListingId = itemListingId;
    }

    public String getItemProductCode() {
        return itemProductCode;
    }

    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getTransItemType() {
        return transItemType;
    }

    public void setTransItemType(String transItemType) {
        this.transItemType = transItemType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getUnpackagedQty() {
        return unpackagedQty;
    }

    public void setUnpackagedQty(Integer unpackagedQty) {
        this.unpackagedQty = unpackagedQty;
    }

    public String getMainItemProductId() {
        return mainItemProductId;
    }

    public void setMainItemProductId(String mainItemProductId) {
        this.mainItemProductId = mainItemProductId;
    }

    public String getMainItemListingId() {
        return mainItemListingId;
    }

    public void setMainItemListingId(String mainItemListingId) {
        this.mainItemListingId = mainItemListingId;
    }
}
