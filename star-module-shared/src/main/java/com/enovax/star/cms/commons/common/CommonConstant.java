package com.enovax.star.cms.commons.common;

/**
 * Created by tharaka on 1/8/16.
 */
public class CommonConstant {


    public final static String TITLE="title";
    public final static String VALID_FROM_DATE="validFrom";
    public final static String VALID_TO_DATE="validTo";
    public final static String CONTENT="content";
    public final static String RELATED_LINK="relatedLink";
    public final static String IMAGE="image";
    public final static String STATUS="status";
    public final static String ZONE="zone";
    public final static String AX_PRODUCT_CROSS_SELL="cross-sell";


    public final static String IMAGE_WIDTH ="width";
    public final static String IMAGE_HEIGHT ="height";

    public final static String PRODUCT_CATEGOTY_EVENTS="Events";
}
