package com.enovax.star.cms.commons.constant.ppmflg;

/**
 * Created by houtao on 19/7/16.
 */
public class PartnerPortalConst {

    public static final String SESSION_ATTR_SPRING_SECURITY_KEY = "SPRING_SECURITY_CONTEXT";
    public static final String SESSION_LOGIN_FAILED_ERROR_KEY = "PPMFLG_LOGIN_FAILED_ERROR";
    public static final String SESSION_LOGIN_SUSPENDED_CUSTOMER_SUPPORT_MSG = "PPMFLG_LOGIN_SUSPENDED_CUSTOMER_SUPPORT_MSG";
    public static final String AUTHORNIZED_USER = "PPMFLG_AUTHORIZED_USER";

    public static final String MODULE = "/.store";

    public static final String SIMPLE_DELIM = "|||";

    public static final String ROOT = "/ppmflg";
    public static final String ROOT_PUBLIC = "/ppmflg/publicity";
    public static final String ROOT_SECURED = "/ppmflg/secured";

    public static final String Partner_Portal_Channel = "partner-portal-mflg";
    public static final String Partner_Portal_Ax_Data = Partner_Portal_Channel;

    public static final String TA_MAIN_ACC_POSTFIX = "_ADMIN";
    public static final String SYSTEM = "system";

    public static final String PARTNER_REGISTER_LABEL =" registered in system";
    public static final String PARTNER_RESUBMIT_LABEL =" resubmitted in system";

    public static final String ORG_NAME = "ORG_NAME";
    public static final String ORG_ACCOUNT_CODE = "accountCode";
    public static final String ADDRESS_OWN = "OWN_ORG_ADDR";
    public static final String CONTACT_OWN = "OWN_CONTACT";
    public static final String BASE_URL_KEY = "BASE_URL";

    public static final String DEFAULT_SYSTEM_CURRENCY_CODE = "SGD";

    public static final String WOT_ONLINE_SALES_POOL = "Reserve";

    //for TAAccessRightsGroup
    public static final String GROUPS_FOR_SUB = "S";
    public static final String GROUPS_FOR_ADMIN = "A";

    public static final String ADMIN_ROOT = "/ppmflgadmin";

    public static final String REGISTER_LABEL =" registered in system";

    public static final String REJECT_PA_REGISTER_LABEL = "rejected the register of ";

    public static final String CACHE_WOT_PRODUCT_PRICE_CONFIG = "cache_wot_product_price_map";

    public static final String CACHE_PARTNER_PRODUCT_PRICE_MAP = "cahce_partner_product_price_map";

    public static final String SESSION_ATTR_LOGGED_IN_ACCESS_RIGHTS = "UserAccessRights";
    public static final String SESSION_ATTR_LOGGED_IN_PARTNER_ID = "PartnerId";
    public static final String SESSION_ATTR_LOGGED_IN_PARTNER_TIER_ID = "PartnerTierId";

    public static final String OfflinePayment = "OfflinePayment";

    public static final String Exclusive = "Exclusive";

    public static final String FAKE_TELEMONEY_RESPONSE = "FakeTelemoneyResponse";
}
