package com.enovax.star.cms.commons.model.axchannel.pin;

/**
 * Created by houtao on 23/8/16.
 */
public class AxImageInfo {

    private String altText;
    private String url;

    public String getAltText() {
        return altText;
    }

    public void setAltText(String altText) {
        this.altText = altText;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
