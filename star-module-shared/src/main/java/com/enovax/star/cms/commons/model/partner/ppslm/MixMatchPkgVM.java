package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackage;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMMixMatchPackageItem;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.model.ticketgen.ETicketData;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.partner.ppslm.TransactionUtil;

import java.io.Serializable;
import java.util.*;

/**
 * Created by jennylynsze on 5/10/16.
 */
public class MixMatchPkgVM implements Serializable{
    private static final long serialVersionUID = 1L;

    private String name;
    private String description;
    private Date expiryDate;
    private String expiryDateStr;
    private Integer qty;

    private Integer totalQty;
    private Integer totalQtyRedeemed;

    private List<MixMatchPkgItemVM> pkgItems;
    private List<MixMatchPkgTransItemVM> transItems;
    private Set<String> transItemsNames;

    private Integer id;
    private String pinCode;
    private Date ticketGeneratedDate;
    private String ticketGeneratedDateStr;
    private String status;
    private Integer qtyRedeemed;
    private String qtyRedeemedStr;
    private Date lastRedemptionDate;
    private String lastRedemptionDateStr;
    private String username;

    private boolean canRedeem;
    private String canRedeemDtStr;

    private String baseUrl;
    private String orgName;
    private String barCodeImage;
    private String preDefinedBody;
    private String preDefinedFooter;
    private String pkgTktType;
    private String pkgTktTypeLabel;

    private List<TncVM> tncs = new ArrayList<>();
    private boolean hasTnc = false;
    private String allReceiptNums;

    private String address;
    private String accountCode;
    private String email;
    private String pinRequestId;

    private Integer mainAccountId;

    private Date createdDate;

    private boolean subAccountTrans;

    private String pkgTktMedia;

    private String ticketBarcodeImageString;

    public MixMatchPkgVM(){}

    /**
     * For Confirm Mix&Match Package page
     * Refer: MMPkgVM
     * @param immpkg
     * @param iItems
     */
    public MixMatchPkgVM(PPSLMMixMatchPackage immpkg, List<PPSLMInventoryTransactionItem> iItems) {
        this.name = immpkg.getName();
        this.description = immpkg.getDescription();
        Date expDate = immpkg.getExpiryDate();
        this.expiryDateStr = NvxDateUtils.formatDate(expDate,
                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        this.qty = immpkg.getQty();

        transItems = new ArrayList<MixMatchPkgTransItemVM>();

        Map<Integer, Integer> qtyRedeemedMap = new HashMap<>();


        if(immpkg.getPkgItems() != null) {
            for(PPSLMMixMatchPackageItem pkgItem: immpkg.getPkgItems()) {
                qtyRedeemedMap.put(pkgItem.getTransactionItemId(), pkgItem.getQtyRedeemed());
            }
        }


        for (PPSLMInventoryTransactionItem item : iItems) {
            MixMatchPkgTransItemVM tempItem = new MixMatchPkgTransItemVM(item);
            if (!transItems.contains(tempItem)) {
                transItems.add(tempItem);
            }
            tempItem.setQtyRedeemed(qtyRedeemedMap.get(item.getId()));

        }

        if(transItems.size()>0){
            this.pkgTktTypeLabel = transItems.get(0).getTicketType();
        }


        transItemsNames = new LinkedHashSet<String>();
        for(MixMatchPkgTransItemVM tempItem:transItems){
            transItemsNames.add(tempItem.getDisplayName() + tempItem.getDisplayDetails());
            for(MixMatchPkgTopupItemVM tempTopup:tempItem.getTopupItems()){
                transItemsNames.add(tempTopup.getDisplayName() + tempTopup.getDisplayDetails());
                tempTopup.setQtyRedeemed(qtyRedeemedMap.get(tempTopup.getItemId()));
            }
        }

        // add below to reuse in pkg popup page
        this.id = immpkg.getId();
        this.name = immpkg.getName();
        Date genDate = immpkg.getTicketGeneratedDate();
        Date lastRdmDate = immpkg.getLastRedemptionDate();
        this.pinCode = immpkg.getPinCode();
        this.pinRequestId = immpkg.getPinRequestId();
        this.ticketGeneratedDateStr = (genDate == null) ? "" : NvxDateUtils.formatDate(
                genDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        this.status = immpkg.getStatus();
        this.qtyRedeemedStr = immpkg.getQtyRedeemed() + "/"
                + immpkg.getQty();
        this.lastRedemptionDateStr = (lastRdmDate == null) ? "" : NvxDateUtils
                .formatDate(lastRdmDate,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        this.username = immpkg.getUsername();

        this.pkgTktMedia = immpkg.getTicketMedia();
        this.totalQty = immpkg.getTotalQty();
        this.totalQtyRedeemed = immpkg.getTotalQtyRedeemed();
    }

    public void populatePartnerDetails(String baseURL, PPSLMPartner pa){
        this.baseUrl = baseURL;
        if(pa != null){
            this.orgName = pa.getOrgName();
            this.address = pa.getAddress();
            this.accountCode = pa.getAccountCode();
        }
    }



    /**
     * For view package page
     *
     * @param immpkg
     */
    public MixMatchPkgVM(PPSLMMixMatchPackage immpkg) {
        this.id = immpkg.getId();
        this.name = immpkg.getName();
        this.description = immpkg.getDescription();
        this.pkgTktMedia = immpkg.getTicketMedia();

        Date expDate = immpkg.getExpiryDate();
        Date genDate = immpkg.getTicketGeneratedDate();
        Date lastRdmDate = immpkg.getLastRedemptionDate();
        this.expiryDateStr = (expDate == null) ? "" : NvxDateUtils.formatDate(
                expDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
        this.qty = immpkg.getQty();
        this.pinCode = immpkg.getPinCode();
        this.pinRequestId = immpkg.getPinRequestId();
        this.ticketGeneratedDateStr = (genDate == null) ? "" : NvxDateUtils.formatDate(
                genDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        this.status = immpkg.getStatus();
        this.qtyRedeemedStr = immpkg.getQtyRedeemed() + "/"
                + immpkg.getQty();
        this.lastRedemptionDateStr = (lastRdmDate == null) ? "" : NvxDateUtils
                .formatDate(lastRdmDate,
                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        this.username = immpkg.getUsername();
        if ((immpkg.getQtyRedeemed() < immpkg.getQty())
                && (immpkg.getLastCheckDate() == null || NvxDateUtils
                .hoursAgo(immpkg.getLastCheckDate()) > 2)) {
            this.canRedeem = true;
        }
        if (immpkg.getLastCheckDate() != null) {
            Date canRdmDt = TransactionUtil.addToDate(
                    immpkg.getLastCheckDate(), Calendar.HOUR, 2);
            this.canRedeemDtStr = NvxDateUtils.formatDate(canRdmDt,
                    NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
        }
        //TODO evaluate the use of baseUrl
//        final String serverUrl = ProjectEngine.projProps().getProperty(SysConst.KEY_SERVER_URL);
//        this.baseUrl = serverUrl;
        //below is add for admin query

        //TODO reevaluate the partner
//        if(immpkg.getPartner()!=null){
//            this.orgName = immpkg.getPartner().getOrgName();
//        }
        Set<String> receiptNums = new LinkedHashSet<String>();
        for(PPSLMMixMatchPackageItem pkgItem:immpkg.getPkgItems()){
            receiptNums.add(pkgItem.getReceiptNum());
        }
        allReceiptNums = null;
        for (String temStr : receiptNums) {
            if (allReceiptNums == null) {
                allReceiptNums = temStr;
            } else {
                allReceiptNums = allReceiptNums + "<BR/>" + temStr;
            }
        }

        this.totalQty = immpkg.getTotalQty();
        this.totalQtyRedeemed = immpkg.getTotalQtyRedeemed();
    }


    private List<ETicketData> etickets;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryDateStr() {
        return expiryDateStr;
    }

    public void setExpiryDateStr(String expiryDateStr) {
        this.expiryDateStr = expiryDateStr;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public List<MixMatchPkgTransItemVM> getTransItems() {
        return transItems;
    }

    public void setTransItems(List<MixMatchPkgTransItemVM> transItems) {
        this.transItems = transItems;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTicketGeneratedDateStr() {
        return ticketGeneratedDateStr;
    }

    public void setTicketGeneratedDateStr(String ticketGeneratedDateStr) {
        this.ticketGeneratedDateStr = ticketGeneratedDateStr;
    }

    public Integer getQtyRedeemed() {
        return qtyRedeemed;
    }

    public void setQtyRedeemed(Integer qtyRedeemed) {
        this.qtyRedeemed = qtyRedeemed;
    }

    public String getQtyRedeemedStr() {
        return qtyRedeemedStr;
    }

    public void setQtyRedeemedStr(String qtyRedeemedStr) {
        this.qtyRedeemedStr = qtyRedeemedStr;
    }

    public String getLastRedemptionDateStr() {
        return lastRedemptionDateStr;
    }

    public void setLastRedemptionDateStr(String lastRedemptionDateStr) {
        this.lastRedemptionDateStr = lastRedemptionDateStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isCanRedeem() {
        return canRedeem;
    }

    public void setCanRedeem(boolean canRedeem) {
        this.canRedeem = canRedeem;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getBarCodeImage() {
        return barCodeImage;
    }

    public void setBarCodeImage(String barCodeImage) {
        this.barCodeImage = barCodeImage;
    }

    public String getPreDefinedBody() {
        return preDefinedBody;
    }

    public void setPreDefinedBody(String preDefinedBody) {
        this.preDefinedBody = preDefinedBody;
    }

    public String getPkgTktType() {
        return pkgTktType;
    }

    public void setPkgTktType(String pkgTktType) {
        this.pkgTktType = pkgTktType;
    }

    public String getPreDefinedFooter() {
        return preDefinedFooter;
    }

    public void setPreDefinedFooter(String preDefinedFooter) {
        this.preDefinedFooter = preDefinedFooter;
    }

    public List<TncVM> getTncs() {
        return tncs;
    }

    public void setTncs(List<TncVM> tncs) {
        this.tncs = tncs;
    }

    public boolean getHasTnc() {
        return hasTnc;
    }

    public void setHasTnc(boolean hasTnc) {
        this.hasTnc = hasTnc;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getCanRedeemDtStr() {
        return canRedeemDtStr;
    }

    public void setCanRedeemDtStr(String canRedeemDtStr) {
        this.canRedeemDtStr = canRedeemDtStr;
    }

    public Set<String> getTransItemsNames() {
        return transItemsNames;
    }

    public void setTransItemsNames(Set<String> transItemsNames) {
        this.transItemsNames = transItemsNames;
    }

    public String getAllReceiptNums() {
        return allReceiptNums;
    }

    public void setAllReceiptNums(String allReceiptNums) {
        this.allReceiptNums = allReceiptNums;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPinRequestId() {
        return pinRequestId;
    }

    public void setPinRequestId(String pinRequestId) {
        this.pinRequestId = pinRequestId;
    }

    public String getPkgTktTypeLabel() {
        return pkgTktTypeLabel;
    }

    public void setPkgTktTypeLabel(String pkgTktTypeLabel) {
        this.pkgTktTypeLabel = pkgTktTypeLabel;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isSubAccountTrans() {
        return subAccountTrans;
    }

    public void setSubAccountTrans(boolean subAccountTrans) {
        this.subAccountTrans = subAccountTrans;
    }

//    public MixMatchPkgVM showWithItems(List<InventoryTransactionItemVM> items) {
//        this.expiryDateStr = NvxDateUtils.formatDate(this.expiryDate,
//                NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
//        transItems = new ArrayList<>();
//        for (InventoryTransactionItemVM item : items) {
//            MixMatchPkgTransItemVM tempItem = new MixMatchPkgTransItemVM(item);
//            if (!transItems.contains(tempItem)) {
//                transItems.add(tempItem);
//            }
//        }
//
//        if(transItems.size()>0){
//            this.pkgTktTypeLabel = transItems.get(0).getTicketType();
//        }
//
//        //TODO add more
//
////        Date genDate = getTicketGeneratedDate();
////        Date lastRdmDate = getLastRedemptionDate();
////        this.pinGeneratedDateStr = (genDate == null) ? "" : NvxDateUtils.formatDate(
////                genDate, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
////        this.qtyRedeemedStr = getQtyRedeemed() + "/"
////                +  getPinCodeQty();
////        this.lastRedemptionDateStr = (lastRdmDate == null) ? "" : NvxUtil
////                .formatDate(lastRdmDate,
////                        NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME);
//
////        if (getPartner() != null) {
////            this.orgName = immpkg.getPartner().getOrgName();
////            this.address = immpkg.getPartner().getAddress();
////            this.accountCode = immpkg.getPartner().getAccountCode();
////        }
//        return this;
//    }


    public List<ETicketData> getEtickets() {
        return etickets;
    }

    public void setEtickets(List<ETicketData> etickets) {
        this.etickets = etickets;
    }

    public String getPkgTktMedia() {
        return pkgTktMedia;
    }

    public void setPkgTktMedia(String pkgTktMedia) {
        this.pkgTktMedia = pkgTktMedia;
    }

    public Integer getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    public Integer getTotalQtyRedeemed() {
        return totalQtyRedeemed;
    }

    public void setTotalQtyRedeemed(Integer totalQtyRedeemed) {
        this.totalQtyRedeemed = totalQtyRedeemed;
    }

    public List<MixMatchPkgItemVM> getPkgItems() {
        return pkgItems;
    }

    public void setPkgItems(List<MixMatchPkgItemVM> pkgItems) {
        this.pkgItems = pkgItems;
    }

    public String getTicketBarcodeImageString() {
        return ticketBarcodeImageString;
    }

    public void setTicketBarcodeImageString(String ticketBarcodeImageString) {
        this.ticketBarcodeImageString = ticketBarcodeImageString;
    }

    public Date getTicketGeneratedDate() {
        return ticketGeneratedDate;
    }

    public void setTicketGeneratedDate(Date ticketGeneratedDate) {
        this.ticketGeneratedDate = ticketGeneratedDate;
    }

    public Date getLastRedemptionDate() {
        return lastRedemptionDate;
    }

    public void setLastRedemptionDate(Date lastRedemptionDate) {
        this.lastRedemptionDate = lastRedemptionDate;
    }
}
