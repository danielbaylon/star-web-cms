
package com.enovax.star.cms.commons.model.kiosk.odata;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.PINRedemptionUnlockEntity
 * 
 * @author dbaylon
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxPINRedemptionUnlockEntity {

	@JsonProperty("IsSucceed")
	private String isSuceed;

	@JsonProperty("ErrorMessage")
	private String errorMessage;

	/**
	 * Collection(Microsoft.Dynamics.Commerce.Runtime.DataModel.CommerceProperty)
	 */
	@JsonProperty("ExtensionProperties")
	private List<AxCommerceProperty> axExtensionPropertiesDataList;

	public String getIsSuceed() {

		return isSuceed;
	}

	public void setIsSuceed(String isSuceed) {

		this.isSuceed = isSuceed;
	}

	public String getErrorMessage() {

		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {

		this.errorMessage = errorMessage;
	}

	public List<AxCommerceProperty> getAxExtensionPropertiesDataList() {

		return axExtensionPropertiesDataList;
	}

	public void setAxExtensionPropertiesDataList(
		List<AxCommerceProperty> axExtensionPropertiesDataList) {

		this.axExtensionPropertiesDataList = axExtensionPropertiesDataList;
	}

}
