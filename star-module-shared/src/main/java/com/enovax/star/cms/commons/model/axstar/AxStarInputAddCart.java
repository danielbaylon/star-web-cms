package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

public class AxStarInputAddCart {

    private String cartId;
    private String customerId;
    private String salesOrderNumber;
    private List<AxStarInputAddCartItem> items;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<AxStarInputAddCartItem> getItems() {
        return items;
    }

    public void setItems(List<AxStarInputAddCartItem> items) {
        this.items = items;
    }

    public String getSalesOrderNumber() {
        return salesOrderNumber;
    }

    public void setSalesOrderNumber(String salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }
}
