
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for CommerceEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommerceEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExtensionProperties" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfCommerceProperty" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommerceEntity", propOrder = {
    "extensionProperties"
})
@XmlSeeAlso({
    Cart.class,
    AttributeValueBase.class,
    ShippingAddress.class,
    AffiliationLoyaltyTier.class,
    TaxLine.class,
    IncomeExpenseLine.class,
    ReasonCodeLine.class,
    CartLine.class,
    TenderLineBase.class,
    TaxableItem.class,
    DiscountLine.class
})
public class CommerceEntity {

    @XmlElementRef(name = "ExtensionProperties", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCommerceProperty> extensionProperties;

    /**
     * Gets the value of the extensionProperties property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCommerceProperty }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCommerceProperty> getExtensionProperties() {
        return extensionProperties;
    }

    /**
     * Sets the value of the extensionProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCommerceProperty }{@code >}
     *     
     */
    public void setExtensionProperties(JAXBElement<ArrayOfCommerceProperty> value) {
        this.extensionProperties = value;
    }

}
