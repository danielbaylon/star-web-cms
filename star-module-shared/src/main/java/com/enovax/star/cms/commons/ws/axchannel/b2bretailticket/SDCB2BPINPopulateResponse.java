
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_B2BPINPopulateResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_B2BPINPopulateResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="PinTableCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}ArrayOfSDC_PinTable" minOccurs="0"/>
 *         &lt;element name="RequestReferenceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_B2BPINPopulateResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "pinTableCollection",
    "requestReferenceId"
})
public class SDCB2BPINPopulateResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "PinTableCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSDCPinTable> pinTableCollection;
    @XmlElementRef(name = "RequestReferenceId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<String> requestReferenceId;

    /**
     * Gets the value of the pinTableCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCPinTable }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSDCPinTable> getPinTableCollection() {
        return pinTableCollection;
    }

    /**
     * Sets the value of the pinTableCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSDCPinTable }{@code >}
     *     
     */
    public void setPinTableCollection(JAXBElement<ArrayOfSDCPinTable> value) {
        this.pinTableCollection = value;
    }

    /**
     * Gets the value of the requestReferenceId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRequestReferenceId() {
        return requestReferenceId;
    }

    /**
     * Sets the value of the requestReferenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRequestReferenceId(JAXBElement<String> value) {
        this.requestReferenceId = value;
    }

}
