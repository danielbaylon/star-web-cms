package com.enovax.star.cms.commons.model.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.OfflinePaymentStatus;
import com.enovax.star.cms.commons.constant.ppslm.OfflinePaymentType;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPartner;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public class OfflinePaymentRequestVM {
	private Integer id;
	private String currency;
    private String receiptNum;
	private String paymentType;
	private String paymentTypeText;
    private String transferChargeStr;
    private String action;
    private String remarks;
    private String remarksContent;
    private String approverRemarks;
    private String status;
    private String totalAmountStr;
    private String createDtStr;
    private String submitDtStr;
    private String submitBy;
    private String username;
    private Integer transId;
	private List<ChatHistVM> chatHists;
	private List<ChatHistVM> finalApprovalHists;
	private String referenceNum;
    private String bankName;
    private String bankCountry;
    private String accountCode;
    private String orgName;
    private Boolean isApprover = Boolean.FALSE;
	private String approvedBy;
    private Boolean offlinePaymentApproved = Boolean.FALSE;
	private String approvalDateText;
	private String paymentDateStr;
	private String finalApprovalRemarks;
	private String finalCancelRemarks;

	private String txnPaymentType;
	private String validityStartDateStr;
	private String validityEndDateStr;
	private String displayStatus;
	private boolean submitted = false;


	private List<HashMap<String,String>> paymentTypes = new ArrayList<HashMap<String,String>>();

	private List<CountryVM> countries = new ArrayList<CountryVM>();

	public OfflinePaymentRequestVM() {
		// TODO Auto-generated constructor stub
	}

	public static OfflinePaymentRequestVM buildPaymentRequestBaisc(PPSLMOfflinePaymentRequest opr, PPSLMInventoryTransaction txn, String orgName, String accountCode){
		return constructPaymentRequest(opr, txn, orgName, accountCode, false);
	}
	public static OfflinePaymentRequestVM buildPaymentRequest(PPSLMOfflinePaymentRequest opr, PPSLMInventoryTransaction txn, String orgName, String accountCode){
		return constructPaymentRequest(opr, txn, orgName, accountCode, true);
	}
	
	public static OfflinePaymentRequestVM constructPaymentRequest(PPSLMOfflinePaymentRequest opr, PPSLMInventoryTransaction txn, String orgName, String accountCode, boolean requiredFUL){
		Logger log = LoggerFactory.getLogger(OfflinePaymentRequestVM.class);
		OfflinePaymentRequestVM oprVM = new OfflinePaymentRequestVM();
		oprVM.setId(opr.getId());
		oprVM.setStatus(opr.getStatus());
		oprVM.setPaymentType(opr.getPaymentType());
		if(OfflinePaymentType.BANK_TRANSFER.code.equalsIgnoreCase(opr.getPaymentType())){
			oprVM.setPaymentTypeText(OfflinePaymentType.BANK_TRANSFER.name);
		}else if(OfflinePaymentType.CHEQUE.code.equalsIgnoreCase(opr.getPaymentType())){
			oprVM.setPaymentTypeText(OfflinePaymentType.CHEQUE.name);
		}else{
			oprVM.setPaymentTypeText("");
		}
		if(opr.getCreatedDate() != null) {
			oprVM.setCreateDtStr(NvxDateUtils.formatDateForDisplay(opr.getCreatedDate(), false));
		}else{
			oprVM.setCreateDtStr("");
		}
		oprVM.setSubmitBy(opr.getSubmitBy());
		if(opr.getSubmitDate() != null){
			oprVM.setSubmitDtStr(NvxDateUtils.formatDateForDisplay(opr.getSubmitDate(), false));
		}else{
			oprVM.setSubmitDtStr("");
		}
		if(!OfflinePaymentStatus.Pending_Submit.code.equalsIgnoreCase(opr.getStatus())){
			oprVM.setSubmitted(true);
		}
		oprVM.setUsername(opr.getCreatedBy());
		oprVM.setRemarks(opr.getRemarks());
		oprVM.setBankName(opr.getBankName());
		oprVM.setBankCountry(opr.getBankCountry());
		oprVM.setReferenceNum(opr.getReferenceNum());
		oprVM.setRemarksContent(getOfflinePaymentRequestRemarks(opr.getRemarks()));
		oprVM.setOfflinePaymentApproved(opr.getApproveDate() != null);
		oprVM.setApprovedBy(opr.getApprover() != null && opr.getApprover().trim().length() > 0 ? opr.getApprover().trim() : "");
		oprVM.setApprovalDateText(opr.getApproveDate() != null ? NvxDateUtils.formatDate(opr.getApproveDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY) : "");
		oprVM.setPaymentDateStr(opr.getPaymentDate() != null ? NvxDateUtils.formatDate(opr.getPaymentDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY) : "" );
		oprVM.setFinalApprovalRemarks(opr.getFinalApprovalRemarks());
		oprVM.setTransId(opr.getTransactionId());
		if(txn!=null){
			oprVM.setCurrency(txn.getCurrency());
			oprVM.setReceiptNum(txn.getReceiptNum());
			oprVM.setTotalAmountStr(NvxNumberUtils.formatToCurrency(txn.getTotalAmount()));
			oprVM.setAccountCode(accountCode);
			oprVM.setOrgName(orgName);
			oprVM.setDisplayStatus(txn.getStatus());
			oprVM.setValidityEndDateStr(txn.getValidityStartDate() != null ? NvxDateUtils.formatDateForDisplay(txn.getValidityStartDate(), false) : "");
			oprVM.setValidityStartDateStr(txn.getValidityEndDate() != null ? NvxDateUtils.formatDateForDisplay(txn.getValidityEndDate(), false) : "");
			oprVM.setTxnPaymentType(txn.getPaymentType());
		}
		if(requiredFUL){
			if(!"Pending_Submit".equals(opr.getStatus())){
				ArrayList<ChatHistVM> tmpHists = new ArrayList<ChatHistVM>();
				try {
					List<ChatHistVM> adminChatHists = Arrays.asList(JsonUtil.fromJson(opr.getApproverRemarks(), ChatHistVM[].class));
					if(adminChatHists != null && adminChatHists.size() > 0) {
						for (ChatHistVM ah : adminChatHists) {
							ah.setUsertype("ADMIN");
							tmpHists.add(ah);
						}
					}
				} catch (Exception e) {
					log.info("Invalid Json in OfflinePaymentRequest");
				}
				ArrayList<ChatHistVM> finalApprovalHist = new ArrayList<ChatHistVM>();
				if(opr.getFinalApprovalRemarks() != null && opr.getFinalApprovalRemarks().trim().length() > 0){
					try {
						List<ChatHistVM> tempHist = Arrays.asList(JsonUtil.fromJson(opr.getFinalApprovalRemarks(), ChatHistVM[].class));
						if(tempHist != null && tempHist.size() > 0) {
							for (ChatHistVM ah : tempHist) {
								ah.setUsertype("ADMIN");
								finalApprovalHist.add(ah);
							}
						}
					} catch (Exception e) {
						log.info("Invalid Json in OfflinePaymentRequest");
					}
				}
				oprVM.setFinalApprovalHists(finalApprovalHist);
				try {
					List<ChatHistVM> userChatHists = Arrays.asList(JsonUtil.fromJson(opr.getRemarks(), ChatHistVM[].class));
					if(userChatHists != null && userChatHists.size() > 0){
						for(ChatHistVM uh:userChatHists) {
							uh.setUsertype("USER");
							tmpHists.add(uh);
						}
					}
				} catch (Exception e) {
					log.info("Invalid Json in OfflinePaymentRequest");
				}
				Collections.sort(tmpHists);
				oprVM.setChatHists(tmpHists);
			}
		}

		return oprVM;
	}
	
	private static String getOfflinePaymentRequestRemarks(String remarks) {
		if(remarks == null || remarks.trim().length() == 0){
			return null;
		}
		ChatHistVM[] chatList = JsonUtil.fromJson(remarks.trim(), ChatHistVM[].class);
		if(chatList == null || chatList.length == 0){
			return null;
		}
		ChatHistVM vm = chatList[chatList.length -1];
		if(vm == null){
			return null;
		}
		return vm.getContent();
	}

	public static void constructOfflinePaymentFormParameters(OfflinePaymentRequestVM requestVM){
		for (final OfflinePaymentType tmpObj : OfflinePaymentType.values()) {
			HashMap map = new HashMap<String, String>();
			map.put("code", tmpObj.code);
			map.put("name", tmpObj.name);
			requestVM.getPaymentTypes().add(map);
		}
	}


	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTransferChargeStr() {
		return transferChargeStr;
	}

	public void setTransferChargeStr(String transferChargeStr) {
		this.transferChargeStr = transferChargeStr;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTotalAmountStr() {
		return totalAmountStr;
	}
	public void setTotalAmountStr(String totalAmountStr) {
		this.totalAmountStr = totalAmountStr;
	}
	public String getCreateDtStr() {
		return createDtStr;
	}
	public void setCreateDtStr(String createDtStr) {
		this.createDtStr = createDtStr;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getTransId() {
		return transId;
	}
	public void setTransId(Integer transId) {
		this.transId = transId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getApproverRemarks() {
		return approverRemarks;
	}
	public void setApproverRemarks(String approverRemarks) {
		this.approverRemarks = approverRemarks;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<ChatHistVM> getChatHists() {
		return chatHists;
	}
	public void setChatHists(List<ChatHistVM> chatHists) {
		this.chatHists = chatHists;
	}
	public String getReferenceNum() {
		return referenceNum;
	}
	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankCountry() {
		return bankCountry;
	}
	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Boolean getIsApprover() {
		return isApprover;
	}
	public void setIsApprover(Boolean isApprover) {
		this.isApprover = isApprover;
	}

	public String getSubmitDtStr() {
		return submitDtStr;
	}

	public void setSubmitDtStr(String submitDtStr) {
		this.submitDtStr = submitDtStr;
	}

	public String getSubmitBy() {
		return submitBy;
	}

	public void setSubmitBy(String submitBy) {
		this.submitBy = submitBy;
	}

	public String getRemarksContent() {
		return remarksContent;
	}

	public void setRemarksContent(String remarksContent) {
		this.remarksContent = remarksContent;
	}

	public String getApprovalDateText() {
		return approvalDateText;
	}

	public void setApprovalDateText(String approvalDateText) {
		this.approvalDateText = approvalDateText;
	}

    public Boolean getOfflinePaymentApproved() {
        return offlinePaymentApproved;
    }

    public void setOfflinePaymentApproved(Boolean offlinePaymentApproved) {
        this.offlinePaymentApproved = offlinePaymentApproved;
    }

	public Boolean getApprover() {
		return isApprover;
	}

	public void setApprover(Boolean approver) {
		isApprover = approver;
	}

	public String getPaymentDateStr() {
		return paymentDateStr;
	}

	public void setPaymentDateStr(String paymentDateStr) {
		this.paymentDateStr = paymentDateStr;
	}

	public String getFinalApprovalRemarks() {
		return finalApprovalRemarks;
	}

	public void setFinalApprovalRemarks(String finalApprovalRemarks) {
		this.finalApprovalRemarks = finalApprovalRemarks;
	}

	public List<ChatHistVM> getFinalApprovalHists() {
		return finalApprovalHists;
	}

	public void setFinalApprovalHists(List<ChatHistVM> finalApprovalHists) {
		this.finalApprovalHists = finalApprovalHists;
	}

	public String getFinalCancelRemarks() {
		return finalCancelRemarks;
	}

	public void setFinalCancelRemarks(String finalCancelRemarks) {
		this.finalCancelRemarks = finalCancelRemarks;
	}


	public List<HashMap<String, String>> getPaymentTypes() {
		return paymentTypes;
	}

	public void setPaymentTypes(List<HashMap<String, String>> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	public List<CountryVM> getCountries() {
		return countries;
	}

	public void setCountries(List<CountryVM> countries) {
		this.countries = countries;
	}

	public String getTxnPaymentType() {
		return txnPaymentType;
	}

	public void setTxnPaymentType(String txnPaymentType) {
		this.txnPaymentType = txnPaymentType;
	}

	public String getValidityStartDateStr() {
		return validityStartDateStr;
	}

	public void setValidityStartDateStr(String validityStartDateStr) {
		this.validityStartDateStr = validityStartDateStr;
	}

	public String getValidityEndDateStr() {
		return validityEndDateStr;
	}

	public void setValidityEndDateStr(String validityEndDateStr) {
		this.validityEndDateStr = validityEndDateStr;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	public String getPaymentTypeText() {
		return paymentTypeText;
	}

	public void setPaymentTypeText(String paymentTypeText) {
		this.paymentTypeText = paymentTypeText;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}
}
