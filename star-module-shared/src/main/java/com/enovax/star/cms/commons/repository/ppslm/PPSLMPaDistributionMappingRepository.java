package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPaDistributionMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMPaDistributionMappingRepository extends JpaRepository<PPSLMPaDistributionMapping, Integer> {

    List<PPSLMPaDistributionMapping> findByPartnerId(Integer id);

    PPSLMPaDistributionMapping findFirstByAdminIdAndPartnerIdAndCountryId(String adminId, Integer partnerId, String countryId);

    @Query("from PPSLMPaDistributionMapping where partnerId in ?1")
    List<PPSLMPaDistributionMapping> findMappingbyPartnerIds(@Param("partnerIds") List<Integer> partnerIds);
}
