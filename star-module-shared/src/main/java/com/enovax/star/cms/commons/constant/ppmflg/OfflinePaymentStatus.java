package com.enovax.star.cms.commons.constant.ppmflg;

public enum OfflinePaymentStatus {
    Pending_Approval("Pending_Approval","Pending Approval"),
    Pending_Submit("Pending_Submit","Pending Submit"),
    Cancelled("Cancelled","Cancelled"),
	Approved("Approved","Approved"),
	Rejected("Rejected","Rejected");
    public String code;
    public String name;

    private OfflinePaymentStatus(String code, String name){
        this.code = code;
        this.name = name;       
    }
}
