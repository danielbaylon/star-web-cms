package com.enovax.star.cms.commons.repository.ppmflg;


import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransactionItem;
import com.enovax.star.cms.commons.model.partner.ppmflg.FilterProductItemRowVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PPMFLGInventoryTransactionItemRepository extends JpaRepository<PPMFLGInventoryTransactionItem, Integer>, JpaSpecificationExecutor<PPMFLGInventoryTransactionItem> {

    List<PPMFLGInventoryTransactionItem> findByTransactionId(Integer transactionId);

    /*
          if(showAvailable){
            criteria.add(Restrictions.in("itrans.status", TransactionUtil.getAvailableStatus()));
            criteria.add(Restrictions.gt("unpackagedQty", 0));
        }else{
            criteria.add(Restrictions.in("itrans.status", TransactionUtil.getAllDisplayStatus()));
     */

    @Query(value = "select i from PPMFLGInventoryTransactionItem i " +
            " where (:toDate is null or i.inventoryTrans.validityStartDate <= :toDate) " +
            " and (:fromDate is null or i.inventoryTrans.validityEndDate >= :fromDate) " +
            " and (:prodNm is null or i.displayName like :prodNm)" +
            " and (i.transItemType = 'Standard')" +
            " and (i.inventoryTrans.mainAccountId = :adminId) "+
            " and ((:showAvailable = true and i.unpackagedQty > 0 and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring')) " +
            " or (:showAvailable = false and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Expired'" +
            " or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring' or i.inventoryTrans.status = 'Refunded'" +
            " or i.inventoryTrans.status = 'Forfeited')))")
    Page getInventoryTransactionsItem(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("showAvailable") boolean showAvailable,
            @Param("prodNm") String prodNm,
            Pageable pageable);

    @Query(value = "select i from PPMFLGInventoryTransactionItem i " +
            " where (:toDate is null or i.inventoryTrans.validityStartDate <= :toDate) " +
            " and (:fromDate is null or i.inventoryTrans.validityEndDate >= :fromDate) " +
            " and (:prodNm is null or i.displayName like :prodNm)" +
            " and (i.transItemType = 'Standard')" +
            " and (i.inventoryTrans.mainAccountId = :adminId) "+
            " and ((:showAvailable = true and i.unpackagedQty > 0 and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring')) " +
            " or (:showAvailable = false and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Expired'" +
            " or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring' or i.inventoryTrans.status = 'Refunded'" +
            " or i.inventoryTrans.status = 'Forfeited')))" +
            " order by i.id DESC")
   List<PPMFLGInventoryTransactionItem> getInventoryTransactionsItem(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("showAvailable") boolean showAvailable,
            @Param("prodNm") String prodNm);

    @Query(value = "select count(i) from PPMFLGInventoryTransactionItem i " +
            " where (:toDate is null or i.inventoryTrans.validityStartDate <= :toDate) " +
            " and (:fromDate is null or i.inventoryTrans.validityEndDate >= :fromDate) " +
            " and (:prodNm is null or i.displayName like :prodNm)" +
            " and (i.transItemType = 'Standard')" +
            " and (i.inventoryTrans.mainAccountId = :adminId) "+
            " and ((:showAvailable = true and i.unpackagedQty > 0 and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring')) " +
            " or (:showAvailable = false and (i.inventoryTrans.status = 'Available' or i.inventoryTrans.status = 'Expired'" +
            " or i.inventoryTrans.status = 'Revalidated' or i.inventoryTrans.status = 'Expiring' or i.inventoryTrans.status = 'Refunded'" +
            " or i.inventoryTrans.status = 'Forfeited')))")
    Long countInventoryTransactionsItem(
            @Param("adminId") Integer adminId,
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("showAvailable") boolean showAvailable,
            @Param("prodNm") String prodNm
           );


    List<PPMFLGInventoryTransactionItem> findByIdIn(List<Integer> idList);


    @Query(value = "select i from PPMFLGInventoryTransactionItem i" +
            " where i.transactionId = :transactionId " +
            " and i.productId = :productId " +
            " and i.unpackagedQty = :unpackagedQty " +
            " and i.mainItemProductId = :mainItemProductId " +
            " and i.mainItemListingId = :mainItemListingId " +
            " and i.transItemType = 'Topup' " +
            " and (:eventGroupId is null or i.eventGroupId = :eventGroupId) " +
            " and (:eventLineId is null or i.eventLineId = :eventLineId)" +
            " and (:dateOfVisit is null or i.dateOfVisit = :dateOfVisit) ")
    List<PPMFLGInventoryTransactionItem> getTaggedTopItem
    (@Param("transactionId") Integer transactionId,
     @Param("productId") String productId,
     @Param("unpackagedQty") Integer unpackedQty,
     @Param("mainItemProductId") String mainItemProductId,
     @Param("mainItemListingId") String mainItemListingId,
     @Param("eventGroupId") String eventGroupId,
     @Param("eventLineId") String eventLineId,
     @Param("dateOfVisit") Date dateOfVisit);


    @Query(value="select distinct new com.enovax.star.cms.commons.model.partner.ppmflg.FilterProductItemRowVM(i.productId, i.productName, i.displayName) from PPMFLGInventoryTransactionItem i")
    List<FilterProductItemRowVM> getItemsFilter();

    @Query(value="select distinct new com.enovax.star.cms.commons.model.partner.ppmflg.FilterProductItemRowVM(i.productId, i.productName, i.displayName) " +
            "from PPMFLGInventoryTransactionItem i " +
            "where i.inventoryTrans.mainAccountId = :mainAccountId")
    List<FilterProductItemRowVM> getItemsFilterByMainAccount(@Param("mainAccountId") Integer mainAccountId);
}
