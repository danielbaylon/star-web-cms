package com.enovax.star.cms.commons.model.tnc;

/**
 * Created by jennylynsze on 9/15/16.
 */
public class TncVM {
    private String title;
    private String content;
    private String type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
