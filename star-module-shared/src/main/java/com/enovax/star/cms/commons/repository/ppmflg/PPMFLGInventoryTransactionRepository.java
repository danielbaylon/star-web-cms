package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.model.partner.ppmflg.MktSharePartnerVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PPMFLGInventoryTransactionRepository extends JpaRepository<PPMFLGInventoryTransaction, Integer> {
    PPMFLGInventoryTransaction findByReceiptNum(String receiptNum);

    PPMFLGInventoryTransaction findById(Integer transactionId);

    List<PPMFLGInventoryTransaction> findByStatusInAndValidityEndDateLessThan(List<String> status, Date currDate);

    List<PPMFLGInventoryTransaction> findByStatusAndRevalidated(String status, boolean revalidated);

    @Query(value = "select t.* " +
            " from PPMFLGInventoryTransaction t, PPMFLGPartner p, PPMFLGInventoryTransactionItem i " +
            " where t.mainAccountId = p.adminId " +
            " and ((p.revalPeriodMonths is null and t.validityEndDate < :forfeitDate) or  " +
            "      (p.revalPeriodMonths is not null and DATEADD(month,p.revalPeriodMonths,t.validityEndDate) < CONVERT(date, getdate()))) " +
            " and t.status = 'Expired' " +
            " and t.revalidated = 0 " +
            " and t.id = i.transactionId " +
            " and i.unpackagedQty > 0 ", nativeQuery = true)
    List<PPMFLGInventoryTransaction> retrieveForfeitedForNoRevalidatedTrans(@Param("forfeitDate") Date forfeitDt);

    @Query(value= "select new com.enovax.star.cms.commons.model.partner.ppmflg.MktSharePartnerVM(t.mainAccount.profile.id , t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status, sum(t.totalAmount)) " +
            " from PPMFLGInventoryTransaction t" +
            " where (((t.createdDate > :fromDate) " +
            " and (t.createdDate - 1 < :toDate)) " +
            " and ((:orgName is null) or (t.mainAccount.profile.orgName like :orgName))" +
            " and (t.tmStatus ='Success'))" +
            " group by t.mainAccount.profile.id, t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status")
    Page getCountryMktShareDetailsWithoutID(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orgName") String orgName, Pageable pageable);

    @Query(value= "select new com.enovax.star.cms.commons.model.partner.ppmflg.MktSharePartnerVM(t.mainAccount.profile.id , t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status, sum(t.totalAmount)) " +
            " from PPMFLGInventoryTransaction t" +
            " where (((t.createdDate > :fromDate) " +
            " and (t.createdDate - 1 < :toDate)) " +
            " and ((:orgName is null) or (t.mainAccount.profile.orgName like :orgName))" +
            " and (t.tmStatus ='Success')" +
            " and (t.mainAccount.profile.id in (:ids)))" +
            " group by t.mainAccount.profile.id, t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status")
    Page getCountryMktShareDetailsWithID(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orgName") String orgName, @Param("ids")List<Integer> paIdList,Pageable pageable);

    @Query(value= "select new com.enovax.star.cms.commons.model.partner.ppmflg.MktSharePartnerVM(t.mainAccount.profile.id , t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status, sum(t.totalAmount)) " +
            " from PPMFLGInventoryTransaction t" +
            " where (((t.createdDate > :fromDate) " +
            " and (t.createdDate - 1 < :toDate)) " +
            " and ((:orgName is null) or (t.mainAccount.profile.orgName like :orgName))" +
            " and (t.tmStatus ='Success'))" +
            " group by t.mainAccount.profile.id, t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status")
    List<MktSharePartnerVM> getCountryMktShareDetailsWithoutID(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orgName") String orgName);

    @Query(value= "select new com.enovax.star.cms.commons.model.partner.ppmflg.MktSharePartnerVM(t.mainAccount.profile.id , t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status, sum(t.totalAmount)) " +
            " from PPMFLGInventoryTransaction t" +
            " where (((t.createdDate > :fromDate) " +
            " and (t.createdDate - 1 < :toDate)) " +
            " and ((:orgName is null) or (t.mainAccount.profile.orgName like :orgName))" +
            " and (t.tmStatus ='Success')" +
            " and (t.mainAccount.profile.id in (:ids)))" +
            " group by t.mainAccount.profile.id, t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status")
    List<MktSharePartnerVM> getCountryMktShareDetailsWithID(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orgName") String orgName, @Param("ids")List<Integer> paIdList);

    @Query(value= "select count(distinct i.mainAccount.profile.id) from PPMFLGInventoryTransaction i where i.mainAccount.profile.id in (select t.mainAccount.profile.id " +
            " from PPMFLGInventoryTransaction t" +
            " where (((t.createdDate > :fromDate) " +
            " and (t.createdDate - 1 < :toDate)) " +
            " and ((:orgName is null) or (t.mainAccount.profile.orgName like :orgName))" +
            " and (t.tmStatus ='Success'))" +
            " group by t.mainAccount.profile.id, t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status)")
    int getCountryMktShareSizeWithoutID(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orgName") String orgName);

    @Query(value= "select count(distinct i.mainAccountId) from PPMFLGInventoryTransaction i where i.mainAccount.profile.id in (select t.mainAccount.profile.id " +
            " from PPMFLGInventoryTransaction t" +
            " where (((t.createdDate > :fromDate) " +
            " and (t.createdDate - 1 < :toDate)) " +
            " and ((:orgName is null) or (t.mainAccount.profile.orgName like :orgName))" +
            " and (t.tmStatus ='Success')" +
            " and (t.mainAccount.profile.id in (:ids)))" +
            " group by t.mainAccount.profile.id, t.mainAccount.profile.orgName, t.mainAccount.profile.uen, t.mainAccount.profile.status)")
    int getCountryMktShareSizeWithID(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("orgName") String orgName, @Param("ids")List<Integer> paIdList);


    @Query(value= "select * from PPMFLGInventoryTransaction trans " +
            " where (trans.tmStatus =  '' " +
            "                 or trans.tmStatus is null" +
            "                 or trans.tmStatus in ('RedirectedToTm', 'TmQuerySent')) " +
            "            and trans.status in ('Reserved') " +
            "            and trans.createdDate < :timeoutLowerBoundMins and trans.createdDate > :timeoutUpperBoundMins", nativeQuery = true)
    List<PPMFLGInventoryTransaction> getTransactionsForStatusUpdate(@Param("timeoutLowerBoundMins") Date timeoutLowerBoundMins, @Param("timeoutUpperBoundMins") Date timeoutUpperBoundMins);
}
