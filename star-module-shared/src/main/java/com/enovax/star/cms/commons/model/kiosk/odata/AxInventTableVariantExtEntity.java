package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * SDC_TicketingExtension.Entity.InventTableVariantExtEntity
 * 
 * @author Justin
 *
 */
public class AxInventTableVariantExtEntity {

    private String itemId;
    private String packageId;
    private String version;
    private String retailVariantId;
    private String description;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRetailVariantId() {
        return retailVariantId;
    }

    public void setRetailVariantId(String retailVariantId) {
        this.retailVariantId = retailVariantId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
