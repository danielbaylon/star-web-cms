
package com.enovax.star.cms.commons.ws.axchannel.b2bretailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="B2BCartCheckoutCancelResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}SDC_CartRetailTicketTableResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "b2BCartCheckoutCancelResult"
})
@XmlRootElement(name = "B2BCartCheckoutCancelResponse", namespace = "http://tempuri.org/")
public class B2BCartCheckoutCancelResponse {

    @XmlElementRef(name = "B2BCartCheckoutCancelResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCCartRetailTicketTableResponse> b2BCartCheckoutCancelResult;

    /**
     * Gets the value of the b2BCartCheckoutCancelResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}
     *     
     */
    public JAXBElement<SDCCartRetailTicketTableResponse> getB2BCartCheckoutCancelResult() {
        return b2BCartCheckoutCancelResult;
    }

    /**
     * Sets the value of the b2BCartCheckoutCancelResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}
     *     
     */
    public void setB2BCartCheckoutCancelResult(JAXBElement<SDCCartRetailTicketTableResponse> value) {
        this.b2BCartCheckoutCancelResult = value;
    }

}
