package com.enovax.star.cms.commons.mgnl.form.field.validator.factory;

import com.enovax.star.cms.commons.mgnl.form.field.validator.ImageSizeValidator;
import com.enovax.star.cms.commons.mgnl.form.field.validator.definition.ImageSizeValidatorDefinition;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import info.magnolia.ui.form.validator.factory.AbstractFieldValidatorFactory;

/**
 * Created by jennylynsze on 12/30/16.
 */
public class ImageSizeValidatorFactory extends AbstractFieldValidatorFactory<ImageSizeValidatorDefinition> {

    private Item item;
    private ImageSizeValidatorDefinition definition;

    public ImageSizeValidatorFactory(ImageSizeValidatorDefinition definition, Item item) {
        super(definition);
        this.item = item;
        this.definition=definition;

    }
    @Override
    public Validator createValidator() {
        return new ImageSizeValidator(item, getI18nErrorMessage(),definition);
    }
}
