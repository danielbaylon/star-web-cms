package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGTASubAccountRepository extends JpaRepository<PPMFLGTASubAccount, Integer> {

    List<PPMFLGTASubAccount> findByUsername(String username);

    PPMFLGTASubAccount  findFirstByUsername(String username);

    PPMFLGTASubAccount findById(Integer id);

    List<PPMFLGTASubAccount> findByMainUser(PPMFLGTAMainAccount mainAccount);

    @Query("SELECT p FROM PPMFLGTASubAccount p WHERE p.mainUser=:mainAccount AND p.id!=:userId ")
    List<PPMFLGTASubAccount> find(@Param("mainAccount") PPMFLGTAMainAccount mainAccount,@Param("userId") int userId);

    int countByIdAndStatus(Integer id, String name);
//    @Query("from PPMFLGTASubAccount where status = 'A' ")
 //   int countByMainUser(PPMFLGTAMainAccount mainAccount);
}
