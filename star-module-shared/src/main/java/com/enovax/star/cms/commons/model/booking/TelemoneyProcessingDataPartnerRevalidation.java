package com.enovax.star.cms.commons.model.booking;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGRevalidationTransaction;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGTASubAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAMainAccount;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTASubAccount;

/**
 * Created by jennylynsze on 11/15/16.
 */
public class TelemoneyProcessingDataPartnerRevalidation extends TelemoneyProcessingData {
    private boolean slm;
    private PPSLMRevalidationTransaction revalTrans;
    private PPSLMInventoryTransaction invTrans;
    private PPSLMTAMainAccount mainAccount;
    private PPSLMTASubAccount subAccount;


    private PPMFLGRevalidationTransaction revalTransMflg;
    private PPMFLGInventoryTransaction invTransMflg;
    private PPMFLGTAMainAccount mainAccountMflg;
    private PPMFLGTASubAccount subAccountMflg;

    private boolean isSubAccount;
    private String email;

    public boolean isSlm() {
        return slm;
    }

    public void setSlm(boolean slm) {
        this.slm = slm;
    }

    public PPSLMRevalidationTransaction getRevalTrans() {
        return revalTrans;
    }

    public void setRevalTrans(PPSLMRevalidationTransaction revalTrans) {
        this.revalTrans = revalTrans;
    }

    public PPSLMInventoryTransaction getInvTrans() {
        return invTrans;
    }

    public void setInvTrans(PPSLMInventoryTransaction invTrans) {
        this.invTrans = invTrans;
    }

    public PPSLMTAMainAccount getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(PPSLMTAMainAccount mainAccount) {
        this.mainAccount = mainAccount;
    }

    public PPSLMTASubAccount getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(PPSLMTASubAccount subAccount) {
        this.subAccount = subAccount;
    }

    public boolean isSubAccount() {
        return isSubAccount;
    }

    public void setIsSubAccount(boolean isSubAccount) {
        this.isSubAccount = isSubAccount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PPMFLGRevalidationTransaction getRevalTransMflg() {
        return revalTransMflg;
    }

    public void setRevalTransMflg(PPMFLGRevalidationTransaction revalTransMflg) {
        this.revalTransMflg = revalTransMflg;
    }

    public PPMFLGInventoryTransaction getInvTransMflg() {
        return invTransMflg;
    }

    public void setInvTransMflg(PPMFLGInventoryTransaction invTransMflg) {
        this.invTransMflg = invTransMflg;
    }

    public PPMFLGTAMainAccount getMainAccountMflg() {
        return mainAccountMflg;
    }

    public void setMainAccountMflg(PPMFLGTAMainAccount mainAccountMflg) {
        this.mainAccountMflg = mainAccountMflg;
    }

    public PPMFLGTASubAccount getSubAccountMflg() {
        return subAccountMflg;
    }

    public void setSubAccountMflg(PPMFLGTASubAccount subAccountMflg) {
        this.subAccountMflg = subAccountMflg;
    }
}
