package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskGetRedemptionPinStartRequest {

	@JsonProperty("PINCode")
	private String pinCode;

	@JsonProperty("UserId")
	private String userId;

	@JsonProperty("SessionId")
	private String sessionId;

	@JsonProperty("RetailStoreId")
	private String retailStoreId;

	@JsonProperty("RetailTerminalId")
	private String retailTerminalId;

	@JsonProperty("IsError")
	private boolean error;

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getRetailStoreId() {
		return retailStoreId;
	}

	public void setRetailStoreId(String retailStoreId) {
		this.retailStoreId = retailStoreId;
	}

	public String getRetailTerminalId() {
		return retailTerminalId;
	}

	public void setRetailTerminalId(String retailTerminalId) {
		this.retailTerminalId = retailTerminalId;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

}
