package com.enovax.star.cms.commons.model.axchannel.b2bretailticket.transformer;


import com.enovax.star.cms.commons.model.axchannel.retailticket.AxFacility;
import com.enovax.star.cms.commons.model.axchannel.retailticket.AxRetailTicketRecord;
import com.enovax.star.cms.commons.ws.axchannel.b2bretailticket.*;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

/**
 * Created by jennylynsze on 8/17/16.
 */
public class AxB2BRetailTicketRecordTransformer {

    public static List<AxRetailTicketRecord> fromWsCart(List<SDCCartRetailTicketItem> wsTickets) {
        final List<AxRetailTicketRecord> tickets = new ArrayList<>();

        for (SDCCartRetailTicketItem wsTicket : wsTickets) {
            final AxRetailTicketRecord ticket = new AxRetailTicketRecord();
            final XMLGregorianCalendar wsTicketCreatedDateTime = wsTicket.getCreatedDateTime();
            ticket.setCreatedDate(wsTicketCreatedDateTime == null ? new Date() : wsTicketCreatedDateTime.toGregorianCalendar().getTime());

            ticket.setDataAreaId(wsTicket.getDataAreaId().getValue());

            final XMLGregorianCalendar wsTicketEndDate = wsTicket.getEndDate();
            ticket.setEndDate(wsTicketEndDate == null ? new Date() : wsTicketEndDate.toGregorianCalendar().getTime());

            final XMLGregorianCalendar wsTicketEventDate = wsTicket.getEventDate();
            ticket.setEventDate(wsTicketEventDate == null ? new Date() : wsTicketEventDate.toGregorianCalendar().getTime());

            ticket.setEventGroupId(wsTicket.getEventGroupId().getValue());

            ticket.setEventLineId(wsTicket.getEventLineId().getValue());

            ticket.setItemId(wsTicket.getItemId().getValue());

            final XMLGregorianCalendar wsTicketModifiedDateTime = wsTicket.getModifiedDateTime();
            ticket.setModifiedDate(wsTicketModifiedDateTime == null ? new Date() : wsTicketModifiedDateTime.toGregorianCalendar().getTime());

            ticket.setNeedsActivation(wsTicket.getNeedActivation() == 1);

            ticket.setOpenValidityId(wsTicket.getOpenValidityId().getValue());

            ticket.setOriginalTicketCode(wsTicket.getOrigTicketCode().getValue());

            ticket.setPinCode(wsTicket.getPinCode().getValue());

            ticket.setProductName(wsTicket.getProductName().getValue());

            ticket.setQtyGroup(wsTicket.getQtyGroup());

            ticket.setRecId(wsTicket.getRecId());

            final XMLGregorianCalendar wsTicketStartDate = wsTicket.getStartDate();
            ticket.setStartDate(wsTicketStartDate == null ? new Date() : wsTicketStartDate.toGregorianCalendar().getTime());

            ticket.setTicketCode(wsTicket.getTicketCode().getValue());

            ticket.setTicketStatus(wsTicket.getTicketStatus());

            ticket.setTicketTableId(wsTicket.getTicketTableId().getValue());

            final XMLGregorianCalendar wsTicketTransDate = wsTicket.getTransDate();
            ticket.setTransactionDate(wsTicketTransDate == null ? new Date() : wsTicketTransDate.toGregorianCalendar().getTime());

            ticket.setTransactionId(wsTicket.getTransactionId().getValue());

            ticket.setUsageValidityId(wsTicket.getUsageValidityId().getValue());

            ticket.setTemplateName(wsTicket.getTemplateName()==null? null: wsTicket.getTemplateName().getValue());


            ArrayOfSDCFacility facility = wsTicket.getSDCFacilityCollection()==null?null:wsTicket.getSDCFacilityCollection().getValue();

            if(facility != null) {
                List<SDCFacility> facilityList = facility.getSDCFacility();
                List<AxFacility> facilityClassEntityList = new ArrayList<>();
                for(SDCFacility fac: facilityList) {
                    // fac.get
                    AxFacility facClass = new AxFacility();
                    facClass.setFacilityId(fac.getFacilityId().getValue());
                    facClass.setFacilityAction(fac.getFacilityAction());

                    ArrayOfstring operationIdsArr = fac.getOperationIds().getValue();
                    if(operationIdsArr != null) {
                        facClass.setOperationIds(operationIdsArr.getString());
                    }

                    ArrayOfint daysOfWeekUsageArr = fac.getDaysOfWeekUsage().getValue();
                    if(daysOfWeekUsageArr != null) {
                        facClass.setDaysOfWeekUsage(daysOfWeekUsageArr.getInt());
                    }

                    facilityClassEntityList.add(facClass);
                }
                ticket.setFacilityClassEntity(facilityClassEntityList);
            }


            Map<String, String> tokenTableMap = new HashMap<>();
            ArrayOfSDCPrintTokenTable tokenTableArray =  wsTicket.getSDCPrintTokenCollection()==null?null:wsTicket.getSDCPrintTokenCollection().getValue();
            if(tokenTableArray != null) {
                List<SDCPrintTokenTable> tokenTableList = tokenTableArray.getSDCPrintTokenTable();

                for(SDCPrintTokenTable tokenTable: tokenTableList) {
                    tokenTableMap.put(tokenTable.getTokenKey().getValue(), tokenTable.getValue().getValue());
                }

                ticket.setPrintTokenEntity(tokenTableMap);
            }

            tickets.add(ticket);
        }

        return tickets;
    }
}
