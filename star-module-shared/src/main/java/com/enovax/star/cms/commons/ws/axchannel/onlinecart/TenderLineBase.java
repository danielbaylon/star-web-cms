
package com.enovax.star.cms.commons.ws.axchannel.onlinecart;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TenderLineBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TenderLineBase">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}CommerceEntity">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AmountInCompanyCurrency" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AmountInTenderedCurrency" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CardTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CashBackAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CompanyCurrencyExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CreditMemoId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GiftCardId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsChangeLine" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsHistorical" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsVoidable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LoyaltyCardId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonCodeLines" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel}ArrayOfReasonCodeLine" minOccurs="0"/>
 *         &lt;element name="SignatureData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusValue" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TenderLineId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TenderTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderLineBase", propOrder = {
    "amount",
    "amountInCompanyCurrency",
    "amountInTenderedCurrency",
    "cardTypeId",
    "cashBackAmount",
    "companyCurrencyExchangeRate",
    "creditMemoId",
    "currency",
    "customerId",
    "exchangeRate",
    "giftCardId",
    "isChangeLine",
    "isHistorical",
    "isVoidable",
    "lineNumber",
    "loyaltyCardId",
    "reasonCodeLines",
    "signatureData",
    "statusValue",
    "tenderLineId",
    "tenderTypeId"
})
@XmlSeeAlso({
    TenderLine.class
})
public class TenderLineBase
    extends CommerceEntity
{

    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "AmountInCompanyCurrency")
    protected BigDecimal amountInCompanyCurrency;
    @XmlElement(name = "AmountInTenderedCurrency")
    protected BigDecimal amountInTenderedCurrency;
    @XmlElementRef(name = "CardTypeId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardTypeId;
    @XmlElement(name = "CashBackAmount")
    protected BigDecimal cashBackAmount;
    @XmlElement(name = "CompanyCurrencyExchangeRate")
    protected BigDecimal companyCurrencyExchangeRate;
    @XmlElementRef(name = "CreditMemoId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditMemoId;
    @XmlElementRef(name = "Currency", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currency;
    @XmlElementRef(name = "CustomerId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerId;
    @XmlElement(name = "ExchangeRate")
    protected BigDecimal exchangeRate;
    @XmlElementRef(name = "GiftCardId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> giftCardId;
    @XmlElement(name = "IsChangeLine")
    protected Boolean isChangeLine;
    @XmlElement(name = "IsHistorical")
    protected Boolean isHistorical;
    @XmlElement(name = "IsVoidable")
    protected Boolean isVoidable;
    @XmlElement(name = "LineNumber")
    protected BigDecimal lineNumber;
    @XmlElementRef(name = "LoyaltyCardId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loyaltyCardId;
    @XmlElementRef(name = "ReasonCodeLines", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfReasonCodeLine> reasonCodeLines;
    @XmlElementRef(name = "SignatureData", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> signatureData;
    @XmlElement(name = "StatusValue")
    protected Integer statusValue;
    @XmlElementRef(name = "TenderLineId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tenderLineId;
    @XmlElementRef(name = "TenderTypeId", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tenderTypeId;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the amountInCompanyCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountInCompanyCurrency() {
        return amountInCompanyCurrency;
    }

    /**
     * Sets the value of the amountInCompanyCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountInCompanyCurrency(BigDecimal value) {
        this.amountInCompanyCurrency = value;
    }

    /**
     * Gets the value of the amountInTenderedCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountInTenderedCurrency() {
        return amountInTenderedCurrency;
    }

    /**
     * Sets the value of the amountInTenderedCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountInTenderedCurrency(BigDecimal value) {
        this.amountInTenderedCurrency = value;
    }

    /**
     * Gets the value of the cardTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardTypeId() {
        return cardTypeId;
    }

    /**
     * Sets the value of the cardTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardTypeId(JAXBElement<String> value) {
        this.cardTypeId = value;
    }

    /**
     * Gets the value of the cashBackAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCashBackAmount() {
        return cashBackAmount;
    }

    /**
     * Sets the value of the cashBackAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCashBackAmount(BigDecimal value) {
        this.cashBackAmount = value;
    }

    /**
     * Gets the value of the companyCurrencyExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCompanyCurrencyExchangeRate() {
        return companyCurrencyExchangeRate;
    }

    /**
     * Sets the value of the companyCurrencyExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCompanyCurrencyExchangeRate(BigDecimal value) {
        this.companyCurrencyExchangeRate = value;
    }

    /**
     * Gets the value of the creditMemoId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreditMemoId() {
        return creditMemoId;
    }

    /**
     * Sets the value of the creditMemoId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreditMemoId(JAXBElement<String> value) {
        this.creditMemoId = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrency(JAXBElement<String> value) {
        this.currency = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerId(JAXBElement<String> value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the exchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Sets the value of the exchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExchangeRate(BigDecimal value) {
        this.exchangeRate = value;
    }

    /**
     * Gets the value of the giftCardId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGiftCardId() {
        return giftCardId;
    }

    /**
     * Sets the value of the giftCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGiftCardId(JAXBElement<String> value) {
        this.giftCardId = value;
    }

    /**
     * Gets the value of the isChangeLine property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsChangeLine() {
        return isChangeLine;
    }

    /**
     * Sets the value of the isChangeLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsChangeLine(Boolean value) {
        this.isChangeLine = value;
    }

    /**
     * Gets the value of the isHistorical property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsHistorical() {
        return isHistorical;
    }

    /**
     * Sets the value of the isHistorical property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsHistorical(Boolean value) {
        this.isHistorical = value;
    }

    /**
     * Gets the value of the isVoidable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsVoidable() {
        return isVoidable;
    }

    /**
     * Sets the value of the isVoidable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsVoidable(Boolean value) {
        this.isVoidable = value;
    }

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLineNumber(BigDecimal value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the loyaltyCardId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoyaltyCardId() {
        return loyaltyCardId;
    }

    /**
     * Sets the value of the loyaltyCardId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoyaltyCardId(JAXBElement<String> value) {
        this.loyaltyCardId = value;
    }

    /**
     * Gets the value of the reasonCodeLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfReasonCodeLine> getReasonCodeLines() {
        return reasonCodeLines;
    }

    /**
     * Sets the value of the reasonCodeLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfReasonCodeLine }{@code >}
     *     
     */
    public void setReasonCodeLines(JAXBElement<ArrayOfReasonCodeLine> value) {
        this.reasonCodeLines = value;
    }

    /**
     * Gets the value of the signatureData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSignatureData() {
        return signatureData;
    }

    /**
     * Sets the value of the signatureData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSignatureData(JAXBElement<String> value) {
        this.signatureData = value;
    }

    /**
     * Gets the value of the statusValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatusValue() {
        return statusValue;
    }

    /**
     * Sets the value of the statusValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatusValue(Integer value) {
        this.statusValue = value;
    }

    /**
     * Gets the value of the tenderLineId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTenderLineId() {
        return tenderLineId;
    }

    /**
     * Sets the value of the tenderLineId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTenderLineId(JAXBElement<String> value) {
        this.tenderLineId = value;
    }

    /**
     * Gets the value of the tenderTypeId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTenderTypeId() {
        return tenderTypeId;
    }

    /**
     * Sets the value of the tenderTypeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTenderTypeId(JAXBElement<String> value) {
        this.tenderTypeId = value;
    }

}
