package com.enovax.star.cms.commons.jcrrepository.system;

import com.enovax.star.cms.commons.model.system.SysParamVM;

import java.util.Map;

public interface ISystemParamRepository {

    public String getSystemParamValueByKey(String appKey, String key);

    public SysParamVM getSystemParamByKey(String appKey, String paramKey);

    Map<String,String> getSystemParamAsMapByKey(String appKey, String sysParamMailServerCfg);

    public void saveSystemParamByKey(String appKey, String paramKey, String value);
}
