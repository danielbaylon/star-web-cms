package com.enovax.star.cms.commons.service.product;

import com.enovax.star.cms.commons.constant.AxProductPropertiesKey;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.inventtable.AxRetailProductExtData;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellLine;
import com.enovax.star.cms.commons.model.axchannel.upcrosssell.AxRetailUpCrossSellProduct;
import com.enovax.star.cms.commons.model.axstar.*;
import com.enovax.star.cms.commons.model.product.*;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkMainProduct;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkRelatedProduct;
import com.enovax.star.cms.commons.model.product.promotions.*;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;
import com.enovax.star.cms.commons.service.axchannel.AxChannelProductsService;
import com.enovax.star.cms.commons.service.axcrt.AxStarService;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.google.gson.reflect.TypeToken;
import info.magnolia.jcr.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("Duplicates")
@Service
public class DefaultProductService implements IProductService {

    private Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    private AxStarService axStarService;
    @Autowired
    private AxChannelProductsService axChannelProductsService;

    public static final long CACHE_MINS = 5L;

    private Map<StoreApiChannels, ProductExtDataCacheObject> productExtDataCache = new ConcurrentHashMap<>();
    private Map<StoreApiChannels, Boolean> productExtDataUpdating = new ConcurrentHashMap<>();

    //TODO Make this update a batch job in order to lessen impact on user experience.
    //TODO Consider using a cache library, e.g. Guava

    @PostConstruct
    public void init() {
        productExtDataCache.put(StoreApiChannels.B2C_MFLG, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.B2C_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.KIOSK_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.KIOSK_MFLG, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.PARTNER_PORTAL_SLM, new ProductExtDataCacheObject());
        productExtDataCache.put(StoreApiChannels.PARTNER_PORTAL_MFLG, new ProductExtDataCacheObject());

        productExtDataUpdating.put(StoreApiChannels.B2C_MFLG, false);
        productExtDataUpdating.put(StoreApiChannels.B2C_SLM, false);
        productExtDataUpdating.put(StoreApiChannels.PARTNER_PORTAL_SLM, false);
        productExtDataUpdating.put(StoreApiChannels.PARTNER_PORTAL_MFLG, false);
    }

    @Override
    public boolean updateProductExtDataCache(StoreApiChannels channel) throws AxChannelException {

        final ProductExtDataCacheObject channelCacheObject = productExtDataCache.get(channel);
        final ApiResult<List<AxRetailProductExtData>> result = axChannelProductsService.apiSearchProductExt(channel, true, "", 0L);
        if (!result.isSuccess()) {
            log.error("Error updating product ext data cache for " + channel + ". Result: " + JsonUtil.jsonify(result));
            return false;
        }

        productExtDataUpdating.put(channel, true); //lock

        final Map<String, ProductExtViewModel> vmMap = channelCacheObject.getViewModelMap();
        vmMap.clear();
        final Map<String, AxRetailProductExtData> dataMap = channelCacheObject.getDataMap();
        dataMap.clear();
        for (AxRetailProductExtData data : result.getData()) {
            dataMap.put(data.getProductId(), data);
            vmMap.put(data.getProductId(), ProductExtViewModel.fromExtData(data));
        }

        channelCacheObject.setInitLoad(true);

        productExtDataUpdating.put(channel, false); //unlock

        log.info("Updated product ext data cache.");
        return true;

    }

    @Override
    public ApiResult<List<ProductExtViewModel>> getDataForProducts(StoreApiChannels channel, List<String> productIds) throws AxChannelException {

        while(productExtDataUpdating.get(channel)) {
        } //if in case it's updating, please wait first

        final ProductExtDataCacheObject channelCacheObject = productExtDataCache.get(channel);

        if(!channelCacheObject.isInitLoad()) {
            updateProductExtDataCache(channel);
        }

        final Map<String, ProductExtViewModel> vmMap = channelCacheObject.getViewModelMap();

        final List<ProductExtViewModel> viewModels = new ArrayList<>();
        for (String productId : productIds) {
            final ProductExtViewModel vm = vmMap.get(productId);
            if (vm != null) {
                viewModels.add(vm);
            }
        }

        return new ApiResult<>(true, "", "", viewModels);
    }

    @Override
    public ApiResult<AxProductRelatedLinkPackage> getValidRelatedAxProducts(StoreApiChannels channel) {
        AxProductRelatedLinkPackage pkg = new AxProductRelatedLinkPackage();
        pkg.setChannel(channel);

        ApiResult<AxProductLinkPackage> axProductResult = getValidAxProducts(channel);
        if(!axProductResult.isSuccess()) {
            log.error("Error in getting the Products...");
            return new ApiResult<>(false, axProductResult.getErrorCode(), axProductResult.getMessage(), null);
        }

        AxProductLinkPackage axProductData = axProductResult.getData();
        pkg.setAxProductLinkPackage(axProductData);


        Map<Long, BigDecimal> mainProductPriceMap = new HashMap<>();
        Map<String, Integer> mainProductListingIdMap = new HashMap<>();
        List<AxMainProduct> mainProductList = axProductData.getMainProductList();

        for(int i = 0; i < mainProductList.size(); i++) {
            AxMainProduct mainProduct = mainProductList.get(i);
            mainProductPriceMap.put(mainProduct.getRecordId(), mainProduct.getPrice());
            mainProductListingIdMap.put(mainProduct.getRecordId().toString(), i);
        }

        ApiResult<PromotionLinkPackage> promotionResult = getFullDiscountData(channel, mainProductPriceMap);

        if(!promotionResult.isSuccess()) {
            log.error("Error in getting the Promotions...");
            return new ApiResult<>(false, promotionResult.getErrorCode(), promotionResult.getMessage(), null);
        }

        PromotionLinkPackage promotionData = promotionResult.getData();
        pkg.setPromotionLinkPackage(promotionData);

        try {
            ApiResult<CrossSellLinkPackage> crossSellResult = getFullUpCrossSellData(channel);

            if(!crossSellResult.isSuccess()) {
                return new ApiResult<>(false, crossSellResult.getErrorCode(), crossSellResult.getMessage(), null);
            }

            pkg.setCrossSellLinkPackage(crossSellResult.getData());

        } catch (AxChannelException e) {
            log.error(e.getMessage(), e);
            return new ApiResult<>(false, "ERR_CROSS_SELL_RETRIEVAL", "Error encountered in getting the cross sell", null);
        }

        //get the ax data
        try {
            final ApiResult<List<AxRetailProductExtData>> axRetailProdExtResult = axChannelProductsService.apiSearchProductExt(channel, true, "", 0L);
            if(!axRetailProdExtResult.isSuccess()) {
                return new ApiResult<>(false,axRetailProdExtResult.getErrorCode(), axRetailProdExtResult.getMessage(), null);
            }

            List<AxRetailProductExtData> axRetailProdExtData = axRetailProdExtResult.getData();
            for(AxRetailProductExtData extData: axRetailProdExtData) {
                if( mainProductListingIdMap.containsKey(extData.getProductId())) {
                    Integer axMainProductIndex = mainProductListingIdMap.get(extData.getProductId());
                    AxMainProduct axMainProduct = mainProductList.get(axMainProductIndex);
                    axMainProduct.setMediaTypeDescription(extData.getMediaTypeDescription());
                    axMainProduct.setMediaTypeId(extData.getMediaTypeId());
                    axMainProduct.setIsCapacity(extData.isCapacity());
                    axMainProduct.setOwnAttraction(extData.isOwnAttraction());
                    axMainProduct.setTemplateName(extData.getTemplateName());
                    axMainProduct.setPrinting(extData.getPrinting());
                    axMainProduct.setNoOfPax(extData.getNoOfPax());
                    axMainProduct.setIsPackage(extData.isPackage());

                }
            }

            axProductData.setAxRetailProductExtDataResultJson(JsonUtil.jsonify(axRetailProdExtData, new TypeToken<List<AxRetailProductExtData>>() {}.getType()));

        } catch (AxChannelException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }

        pkg.setSuccess(true);
        pkg.setMessage("Successful");

        return new ApiResult<>(true, "", "", pkg);
    }

    public ApiResult<AxProductLinkPackage> getValidAxProducts(StoreApiChannels channel) {
        AxProductLinkPackage pkg = new AxProductLinkPackage();
        pkg.setChannel(channel);

        AxStarServiceResult<List<AxStarProduct>> axProducts = axStarService.apiProductsGet(channel);
        if(!axProducts.isSuccess()) {
            log.error("Error in getting the Products...");
            return new ApiResult<>(false, "ERR_PRODUCT_DATA_RETRIEVAL", "Error retrieving the product data.", null);
        }

        List<AxStarProduct> axProductsData = axProducts.getData();
        List<AxMainProduct> axMainProductList = new ArrayList<>();

        //Get the key name from CMS CONFIG
        String ticketTypeKey;
        try {
           Node ticketTypeKeyNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), AxProductPropertiesKey.TICKET_TYPE_KEY_JCR_PATH);
           ticketTypeKey = PropertyUtil.getString(ticketTypeKeyNode, "value");
        } catch (RepositoryException e) {
           ticketTypeKey = AxProductPropertiesKey.TICKET_TYPE; //default if nothing is set
           log.error("No cms config found for path: " + AxProductPropertiesKey.TICKET_TYPE_KEY_JCR_PATH + ". Will use the default key: " + AxProductPropertiesKey.TICKET_TYPE);
        }

        String publishPriceKey;
        try {
            Node publishPriceKeyNode = JcrRepository.getParentNode(JcrWorkspace.CMSConfig.getWorkspaceName(), AxProductPropertiesKey.PUBLISH_PRICE_KEY_JCR_PATH);
            publishPriceKey = PropertyUtil.getString(publishPriceKeyNode, "value");
        } catch (RepositoryException e) {
            publishPriceKey = AxProductPropertiesKey.PUBLISHED_PRICE; //default if nothing is set
            log.error("No cms config found for path: " + AxProductPropertiesKey.PUBLISH_PRICE_KEY_JCR_PATH + ". Will use the default key: " + AxProductPropertiesKey.PUBLISHED_PRICE);
        }



        for(AxStarProduct axProdData: axProductsData) {
            AxMainProduct product = new AxMainProduct();
            product.setItemId(axProdData.getItemId());
            product.setProductName(axProdData.getProductName());
            product.setRecordId(axProdData.getRecordId());
            product.setPrice(axProdData.getPrice());
            product.setDescription(axProdData.getDescription());
            product.setSearchName(axProdData.getSearchName());

            //assign the ticket type and published price
            List<AxStarProductPropertyTranslation> translations = axProdData.getProductProperties();
            for(AxStarProductPropertyTranslation trans: translations) {
                if(AxStarProductPropertyTranslation.EN.equals(trans.getTranslationLanguage())) {
                    //THIS Is what I WANT
                    List<AxStarProductProperty> properties = trans.getTranslatedProperties();

                    for(AxStarProductProperty prop: properties) {
                        if(ticketTypeKey.equals(prop.getKeyName())) {
                            String ticketType = prop.getValueString();

                            //everything that is not adult or child will automatically become Standard
                            if(!AxProductPropertiesKey.TICKET_TYPE_ADULT.equals(ticketType) && !AxProductPropertiesKey.TICKET_TYPE_CHILD.equals(ticketType) ) {
                                ticketType =AxProductPropertiesKey.TICKET_TYPE_STANDARD;
                            }

                            product.setTicketType(ticketType);
                        }else if(publishPriceKey.equals(prop.getKeyName())) {
                            product.setPublishedPrice( new BigDecimal(prop.getValueString()));
                        }
                    }
                }
            }

            Map<Long, List<AxStarProductCatalog>> productCatalogMap = axProdData.getProductCatalogMap();
            if(productCatalogMap != null) {
                List<AxStarProductCatalog> productCatalogs = productCatalogMap.get(axProdData.getRecordId());
                if(productCatalogs != null && productCatalogs.size() > 0) {
                    AxStarProductCatalog catalog =  productCatalogs.get(0); //I dont care about the rest
                    product.setValidFrom(parseValidityDate(catalog.getValidFrom()));
                    product.setValidTo(parseValidityDate(catalog.getValidTo()));
                }
            }

            axMainProductList.add(product);
        }

        pkg.setSuccess(true);
        pkg.setMainProductList(axMainProductList);
        pkg.setAxProductsResultJson(axProducts.getRawData());
        pkg.setMessage("Successful");

        return new ApiResult<>(true, "", "", pkg);
    }

    @Override
    public ApiResult<PromotionLinkPackage> getFullDiscountData(StoreApiChannels channel, Map<Long, BigDecimal> mainProductPriceMap) {

        PromotionLinkPackage pkg = new PromotionLinkPackage();
        pkg.setChannel(channel);

        AxStarServiceResult<List<AxStarRetailDiscount>> axDiscounts = axStarService.apiRetailDiscountsGet(channel);


        if(!axDiscounts.isSuccess()) {
            log.error("Error in getting the RetailDiscounts...");
            return new ApiResult<>(false, "ERR_DISCOUNT_DATA_RETRIEVAL", "Error retrieving the discount data.", null);
        }


        AxStarServiceResult<List<AxStarAffiliation>> axAffs = axStarService.apiAffiliationsGet(channel);
        if(!axAffs.isSuccess()) {
            log.error("Error in getting the Affiliations...");
            return new ApiResult<>(false, "ERR_AFFILIATION_RETRIEVAL", "Error retrieving the affiliations data.", null);
        }

        //Process the affiliations and store by PriceGroupId
        List<AxStarAffiliation> affiliations = axAffs.getData();
        Map<Long, List<PromotionRelatedAffiliation>> affPriceGroupMap = new HashMap<>(); //TODO check if one priceGroup can belong to 2 different aff
        if(affiliations != null && affiliations.size() > 0) {
            for(AxStarAffiliation aff: affiliations) {
                List<AxStarPriceGroup> priceGroupList = aff.getPriceGroups();
                for(AxStarPriceGroup priceGroup: priceGroupList) {
                    PromotionRelatedAffiliation relatedAff = new PromotionRelatedAffiliation();
                    relatedAff.setRecordId(aff.getRecordId().toString());
                    relatedAff.setSystemName(aff.getName());
                    relatedAff.setSystemDescription(aff.getDescription());

                    List<PromotionRelatedAffiliation> relatedAffList = affPriceGroupMap.get(priceGroup.getPriceGroupId());
                    if(relatedAffList == null) {
                        relatedAffList = new ArrayList<>();
                    }
                    relatedAffList.add(relatedAff); //add in the new one
                    affPriceGroupMap.put(priceGroup.getPriceGroupId(), relatedAffList);
                }
            }
        }


        //Process the discounts
        Map<Long, List<ProductRelatedPromotion>> promotionProductMap = new HashMap<>(); //map of promotion with product
        List<AxStarRetailDiscount> retailDiscount = axDiscounts.getData();

        if(retailDiscount != null && retailDiscount.size() > 0) {

            List<PromotionLinkMainPromotion> mainPromotionList = new ArrayList<>();

            for(AxStarRetailDiscount rd: retailDiscount) {

                ProductRelatedPromotion promotion;
                List<PromotionRelatedAffiliation> relatedAffiliationList = new ArrayList<>();
                List<PromotionRelatedDiscountCode> relatedDiscountCodeList = new ArrayList<>();
                Date now = NvxDateUtils.clearTime(new Date());
                ProductRelatedPromotion mainPromotion = new ProductRelatedPromotion();
                PromotionLinkMainPromotion mainPromotionLink = new PromotionLinkMainPromotion();

                mainPromotionList.add(mainPromotionLink);

                //for the MAIN PROMOTION, ideally for those without a related item....
                mainPromotion.setOfferId(rd.getOfferId());
                mainPromotion.setSystemName(rd.getName());

                Date mainPromotionValidFrom = parseValidityDate(rd.getValidFromDate());
                Date mainPromotionValidTo = parseValidityDate(rd.getValidToDate());

                if(mainPromotionValidFrom != null && mainPromotionValidTo != null && now.compareTo(mainPromotionValidFrom) >= 0 && now.compareTo(mainPromotionValidTo) <= 0) {
                    mainPromotion.setSystemActive(true); //TODO check if this is the case
                }else {
                    mainPromotion.setSystemActive(false);
                }

                mainPromotion.setValidFrom(mainPromotionValidFrom);
                mainPromotion.setValidTo(mainPromotionValidTo);
                mainPromotion.setCurrencyCode(rd.getCurrencyCode());
                mainPromotion.setEstimatedDiscountedUnitPrice(BigDecimal.ZERO);

                mainPromotionLink.setPromotion(mainPromotion);

                //TODO
                //TODO
                //TODO For now no need to check if the groupId is SLM or KIOSK, assume that the API will return to me the correct Channel
                //Related Affiliation
                List<AxStarRetailDiscountPriceGroup> priceGroupList = rd.getPriceGroups();
                for(AxStarRetailDiscountPriceGroup priceGroup: priceGroupList) {
                    //check if it has related aff
                    if(affPriceGroupMap.containsKey(priceGroup.getPriceGroupId())) {
                        relatedAffiliationList.addAll(affPriceGroupMap.get(priceGroup.getPriceGroupId())); //TODO: for now just add everything, even if there's only one aff
                    }
                }

                mainPromotion.setAffiliations(relatedAffiliationList);

                //Related DiscountCode
                List<AxStarRetailDiscountDiscountCode> discountCodeList = rd.getDiscountCodes();
                for(AxStarRetailDiscountDiscountCode discountCode: discountCodeList) {
                    PromotionRelatedDiscountCode relatedDiscountCode = new PromotionRelatedDiscountCode();
                    relatedDiscountCode.setRecordId(discountCode.getRecordId().toString());
                    relatedDiscountCode.setRelatedOfferId(discountCode.getOfferId());
                    relatedDiscountCode.setBarcode(discountCode.getBarcode());
                    relatedDiscountCode.setDiscountCode(discountCode.getCode());
                    relatedDiscountCode.setSystemName(discountCode.getName());
                    Date validFrom = parseValidityDate(discountCode.getValidFrom());
                    Date validTo = parseValidityDate(discountCode.getValidTo());

                    if(validFrom != null && validTo != null && now.compareTo(validFrom) >= 0 && now.compareTo(validTo) <= 0) {
                        relatedDiscountCode.setSystemActive(true); //TODO check if this is the case
                    }else {
                        relatedDiscountCode.setSystemActive(false);
                    }
                    relatedDiscountCode.setValidFrom(validFrom);
                    relatedDiscountCode.setValidTo(validTo);
                    relatedDiscountCodeList.add(relatedDiscountCode);
                }

                mainPromotion.setDiscountCodes(relatedDiscountCodeList);


                //For Promotion Sake

                //For Main Product Sake
                Map<String, PromotionRelatedProduct> relatedProductMap = new HashMap<>();
                mainPromotionLink.setRelatedItems(relatedProductMap);

                List<AxStarRetailDiscountLine> discountLineList = rd.getDiscountLines();
                for(AxStarRetailDiscountLine discountLine: discountLineList) {

                    //TODO if product id is 0, just ignore for now, evaluate later what needs to do
                    if(discountLine.getProductId() == 0) {
                        continue;
                    }

                    promotion = new ProductRelatedPromotion(); //new promotion for discountLine
                    promotion.setOfferId(rd.getOfferId());
                    promotion.setSystemName(rd.getName());

                    List<ProductRelatedPromotion> relatedPromotionList = promotionProductMap.get(discountLine.getProductId()); //get from the mapping for the promotion and product that we stored so far
                    if(relatedPromotionList == null) {
                        relatedPromotionList = new ArrayList<>();
                    }
                    //TODO do the computation here
                    BigDecimal relProductPrice = mainProductPriceMap.get(discountLine.getProductId());
                    promotion.setEstimatedDiscountedUnitPrice(computeEstimatedPrice(relProductPrice, rd, discountLine, rd.getMultibuyQuantityTiers())); //TODO compute this one
                    relatedPromotionList.add(promotion);

                    PromotionRelatedProduct relatedProduct = new PromotionRelatedProduct();
                    relatedProduct.setItemId(discountLine.getProductId() + "");
                    relatedProduct.setEstimatedDiscountedUnitPrice(promotion.getEstimatedDiscountedUnitPrice());
                    relatedProductMap.put(relatedProduct.getItemId(), relatedProduct);

                    promotionProductMap.put(discountLine.getProductId(), relatedPromotionList);
                }
            }

            List<PromotionLinkMainProduct> promotionMainProductList = new ArrayList<>();
            for(Long productId: promotionProductMap.keySet()) {
                PromotionLinkMainProduct mainProduct = new PromotionLinkMainProduct();
                mainProduct.setItemId(productId.toString());
                mainProduct.setRelatedPromotions(promotionProductMap.get(productId));
                promotionMainProductList.add(mainProduct);
            }

            pkg.setMainPromotionList(mainPromotionList);
            pkg.setMainProductList(promotionMainProductList);
        }


       pkg.setMessage("Success");
       pkg.setSuccess(true);

       pkg.setRetailDiscountResultJson(axDiscounts.getRawData());
       pkg.setAffiliationResultJson(axAffs.getRawData());

       return new ApiResult<>(true, "", "", pkg);
    }


    private BigDecimal computeEstimatedPrice(BigDecimal productPrice, AxStarRetailDiscount retailDiscount, AxStarRetailDiscountLine discountLine, List<AxStarRetailDiscountMultibuyQuantityTier> quantityTier) {

        if(productPrice == null) {
            return BigDecimal.ZERO;
        }

        //(1) DiscountPercent, 10% off
        if(BigDecimal.ZERO.compareTo(discountLine.getDiscountPercent()) != 0) {
            return productPrice.multiply(new BigDecimal(100).subtract(discountLine.getDiscountPercent())).divide(new BigDecimal(100));
        }

        //(2) Buy X for $Y.00, dealPrice divide by the number of items
        if(BigDecimal.ZERO.compareTo(retailDiscount.getMixAndMatchDealPrice()) != 0) {
            return retailDiscount.getMixAndMatchDealPrice().divide(new BigDecimal(discountLine.getMixAndMatchLineNumberOfItemsNeeded()));
        }

        //(3) OfferPriceIncTax > 0, this is the amount
        if(BigDecimal.ZERO.compareTo(discountLine.getOfferPriceIncludingTax()) != 0) {
            return discountLine.getOfferPriceIncludingTax();
        }

        //(4) Buy 1 Merlion with USS $60
        if(BigDecimal.ZERO.compareTo(new BigDecimal(discountLine.getDiscountLinePercentOrValue())) != 0) {
           // discount type = 4, is actually the actual value
            if(retailDiscount.getDiscountType().intValue() == 4) {
                return new BigDecimal(discountLine.getDiscountLinePercentOrValue()).divide(new BigDecimal(discountLine.getMixAndMatchLineNumberOfItemsNeeded()));
            }else {
                //else it means that probably it is percent i guess
                return productPrice.multiply(new BigDecimal(100).subtract(new BigDecimal(discountLine.getDiscountLinePercentOrValue()))).divide(new BigDecimal(100));
            }

        }

        //(5) Buy minimum 5 get 20% off and Buy minimum 3 get discounted price of $3 each
        if(quantityTier != null && quantityTier.size() > 0) {
            //TODO for now get the first one only
            AxStarRetailDiscountMultibuyQuantityTier tier = quantityTier.get(0);
            //if discount type = 1, use % off
            if(retailDiscount.getDiscountType().intValue() == 1) {
                return productPrice.multiply(new BigDecimal(100).subtract(tier.getDiscountPriceOrPercent())).divide(new BigDecimal(100));
            }else if(retailDiscount.getDiscountType().intValue() == 0) {
                return tier.getDiscountPriceOrPercent();
            }
        }

        //(6) Amount off, 5$ off
        if(BigDecimal.ZERO.compareTo(discountLine.getDiscountAmount()) != 0) {
            return productPrice.subtract(discountLine.getDiscountAmount());
        }

        return productPrice;
    }

    private Date parseValidityDate(String validity) {
        Date validDate;
        try {
            String validToDateClean = validity.substring(0, 10) + validity.substring(11, 19) + validity.substring(19, 22) + validity.substring(23);
            validDate = NvxDateUtils.parseDate(validToDateClean, "yyyy-MM-ddHH:mm:ssZ");
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
            return null;
        }
        return validDate;
    }

    @Override
    public ApiResult<CrossSellLinkPackage> getFullUpCrossSellData(StoreApiChannels channel) throws AxChannelException {
        CrossSellLinkPackage pkg = new CrossSellLinkPackage();
        pkg.setChannel(channel);

        final ApiResult<List<AxRetailUpCrossSellProduct>> axApiResult = axChannelProductsService.apiGetUpCrossSellData(channel);
        if (!axApiResult.isSuccess()) {
            return new ApiResult<>();
        }

        final List<AxRetailUpCrossSellProduct> axData = axApiResult.getData();
        if (axData.isEmpty()) {
            pkg.setSuccess(true);
            pkg.setMessage("Success");
            return new ApiResult<>(true, "", "", pkg);
        }

        Map<String, CrossSellLinkMainProduct> mainProductMap = new HashMap<>();

        for(AxRetailUpCrossSellProduct crossSelData: axData) {
            CrossSellLinkMainProduct mainProduct = new CrossSellLinkMainProduct();
            mainProduct.setItemId(crossSelData.getItemId());
            mainProduct.setUnitId(crossSelData.getUnitId());
            mainProduct.setMultipleQty(crossSelData.getMultipleQty());
            mainProduct.setMinimumQty(crossSelData.getMinimumQty());

            List<AxRetailUpCrossSellLine> relCrossSells = crossSelData.getLines();
            List<CrossSellLinkRelatedProduct> relatedProductList = new ArrayList<>();
            for(AxRetailUpCrossSellLine relCS: relCrossSells) {
                CrossSellLinkRelatedProduct relatedProduct = new CrossSellLinkRelatedProduct();
                relatedProduct.setRecordId(relCS.getTableRecId().toString());
                relatedProduct.setItemId(relCS.getItemId());
                relatedProduct.setQty(relCS.getQty());
                relatedProduct.setUnitId(relCS.getUnitId());
                relatedProductList.add(relatedProduct);
            }
            mainProduct.setRelatedProducts(relatedProductList);
            mainProductMap.put(mainProduct.getItemId(), mainProduct);
        }
        pkg.setMainProductMap(mainProductMap);
        pkg.setSuccess(true);
        pkg.setMessage("Success");
        pkg.setAxRetailUpCrossSellProductResultJson(JsonUtil.jsonify(axData, new TypeToken<List<AxRetailUpCrossSellProduct>>() {}.getType()));

        return new ApiResult<>(true, "", "", pkg); //TODO
    }


    //    {
//        "OfferId": "SLM-000001",
//            "CurrencyCode": "SGD",
//            "PriceDiscountGroup": 5637147576,
//            "Name": "10% disc",
//            "PeriodicDiscountType": 2,
//            "ConcurrencyMode": 2,
//            "IsDiscountCodeRequired": false,
//            "ValidationPeriodId": "",
//            "DateValidationType": 1,
//            "ValidFromDate": "1900-01-01T00:00:00+00:00",
//            "ValidToDate": "2154-12-31T00:00:00+08:00",
//            "DiscountType": 2,
//            "MixAndMatchDealPrice": 0,
//            "MixAndMatchDiscountPercent": 10,
//            "MixAndMatchDiscountAmount": 0,
//            "MixAndMatchNumberOfLeastExpensiveLines": 0,
//            "MixAndMatchNumberOfTimeApplicable": 0,
//            "ShouldCountNonDiscountItems": 0,
//            "DiscountLines": [
//        {
//            "OfferId": "SLM-000001",
//                "DiscountLineNumber": 1,
//                "DiscountLinePercentOrValue": 0,
//                "MixAndMatchLineGroup": "",
//                "MixAndMatchLineSpecificDiscountType": 0,
//                "MixAndMatchLineNumberOfItemsNeeded": 0,
//                "DiscountMethod": 0,
//                "DiscountAmount": 0,
//                "DiscountPercent": 10,
//                "OfferPrice": 0,
//                "OfferPriceIncludingTax": 0,
//                "UnitOfMeasureSymbol": "Ea",
//                "CategoryId": 5637150592,
//                "ProductId": 5637145331,
//                "DistinctProductVariantId": 0,
//                "ExtensionProperties": [
//            {
//                "Key": "DATAAREAID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "slm"
//            }
//            }
//            ]
//        }
//        ],
//        "PriceGroups": [
//        {
//            "OfferId": "SLM-000001",
//                "RecordId": 5637152076,
//                "PriceGroupId": 5637147576,
//                "GroupId": "B2C",
//                "ExtensionProperties": [
//            {
//                "Key": "DATAAREAID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "slm"
//            }
//            }
//            ]
//        },
//        {
//            "OfferId": "SLM-000001",
//                "RecordId": 5637153582,
//                "PriceGroupId": 5637151327,
//                "GroupId": "Kiosk",
//                "ExtensionProperties": [
//            {
//                "Key": "DATAAREAID",
//                    "Value": {
//                "BooleanValue": null,
//                        "ByteValue": null,
//                        "DecimalValue": null,
//                        "DateTimeOffsetValue": null,
//                        "IntegerValue": null,
//                        "LongValue": null,
//                        "StringValue": "slm"
//            }
//            }
//            ]
//        }
//        ],
//        "DiscountCodes": [],
//        "MultibuyQuantityTiers": [],
//        "ThresholdDiscountTiers": [],
//        "ExtensionProperties": [
//        {
//            "Key": "DATAAREAID",
//                "Value": {
//            "BooleanValue": null,
//                    "ByteValue": null,
//                    "DecimalValue": null,
//                    "DateTimeOffsetValue": null,
//                    "IntegerValue": null,
//                    "LongValue": null,
//                    "StringValue": "slm"
//        }
//        }
//        ]
//    },


}
