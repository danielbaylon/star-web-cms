package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGPADocMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPMFLGPADocMappingRepository extends JpaRepository<PPMFLGPADocMapping, Integer> {

    @Query("select a from PPMFLGPADocMapping a where a.partnerId = ?1 order by a.paDoc.createdDate desc")
    List<PPMFLGPADocMapping> findByPartnerId(Integer id);

    PPMFLGPADocMapping findByPartnerIdAndDocId(Integer paId, Integer docId);

    PPMFLGPADocMapping findByDocId(Integer docId);
}
