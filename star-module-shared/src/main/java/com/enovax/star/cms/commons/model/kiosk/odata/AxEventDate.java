package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SDC_NonBindableCRTExtension.Entity.EventDates
 * 
 * @author justin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AxEventDate {

    @JsonProperty("EventDateOption")
    private String eventDateOption;

    @JsonProperty("EventAvailOption")
    private String eventAvailOption;

    public String getEventDateOption() {
        return eventDateOption;
    }

    public void setEventDateOption(String eventDateOption) {
        this.eventDateOption = eventDateOption;
    }

    public String getEventAvailOption() {
        return eventAvailOption;
    }

    public void setEventAvailOption(String eventAvailOption) {
        this.eventAvailOption = eventAvailOption;
    }

}
