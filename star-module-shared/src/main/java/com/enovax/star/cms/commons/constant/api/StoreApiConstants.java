package com.enovax.star.cms.commons.constant.api;

public final class StoreApiConstants {

    public static final String REQUEST_HEADER_CHANNEL = "Store-Api-Channel";

    public static final String SESSION_ATTR_STORE_TXN = "StoreTransaction";

    public static final String REQUEST_HEADER_LOCALE = "Store-Api-Locale";

    public static final String SESS_ATTR_TXN_DATA = "SessionTransactionData";

    public static final String SESS_ATTR_REVAL_TXN_RECEIPT_NUM = "StoreRevalTransactionReceiptNum";

}
