package com.enovax.star.cms.commons.datamodel.ppslm;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jennylynsze on 7/13/16.
 */
@Entity
@Table(name = PPSLMWingsOfTimeReceiptLog.TABLE_NAME)
public class PPSLMWingsOfTimeReceiptLog {
    public static final String TABLE_NAME = "PPSLMWingsOfTimeReceiptLog";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "type")
    private String type;

    @Column(name = "date")
    private Date date;

    @Column(name = "count")
    private int count;

    @Column(name = "mainAccountId")
    private Integer mainAccountId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Integer getMainAccountId() {
        return mainAccountId;
    }

    public void setMainAccountId(Integer mainAccountId) {
        this.mainAccountId = mainAccountId;
    }
}
