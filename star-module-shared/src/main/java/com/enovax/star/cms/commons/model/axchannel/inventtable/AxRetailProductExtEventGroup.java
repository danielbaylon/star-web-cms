package com.enovax.star.cms.commons.model.axchannel.inventtable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AxRetailProductExtEventGroup {

    private String capacityCalendarId;
    private String dataAreaId;
    private Date eventDate;
    private String eventGroupId;
    private List<AxRetailProductExtEventGroupLine> eventGroupLines = new ArrayList<>();
    private boolean isSingleEvent;
    private String recId;
    private String recVersion;

    public String getCapacityCalendarId() {
        return capacityCalendarId;
    }

    public void setCapacityCalendarId(String capacityCalendarId) {
        this.capacityCalendarId = capacityCalendarId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public List<AxRetailProductExtEventGroupLine> getEventGroupLines() {
        return eventGroupLines;
    }

    public void setEventGroupLines(List<AxRetailProductExtEventGroupLine> eventGroupLines) {
        this.eventGroupLines = eventGroupLines;
    }

    public boolean isSingleEvent() {
        return isSingleEvent;
    }

    public void setSingleEvent(boolean singleEvent) {
        isSingleEvent = singleEvent;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public String getRecVersion() {
        return recVersion;
    }

    public void setRecVersion(String recVersion) {
        this.recVersion = recVersion;
    }
}
