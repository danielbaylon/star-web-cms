package com.enovax.star.cms.commons.repository.ppmflg;

import com.enovax.star.cms.commons.datamodel.ppmflg.PPMFLGWingsOfTimeReservationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by houtao on 26/8/16.
 */
@Repository
public interface PPMFLGWingsOfTimeReservationLogRepository extends JpaRepository<PPMFLGWingsOfTimeReservationLog, Integer> {
}
