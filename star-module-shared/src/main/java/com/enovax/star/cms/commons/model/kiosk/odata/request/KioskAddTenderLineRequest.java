package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartTenderLine;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Justin
 * @since 10 SEP 16
 */
public class KioskAddTenderLineRequest {

    @JsonIgnore
    private String cartId;

    @JsonProperty("cartTenderLine")
    private AxCartTenderLine axCartTenderLine = new AxCartTenderLine();

    public AxCartTenderLine getAxCartTenderLine() {
        return axCartTenderLine;
    }

    public void setAxCartTenderLine(AxCartTenderLine axCartTenderLine) {
        this.axCartTenderLine = axCartTenderLine;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

}
