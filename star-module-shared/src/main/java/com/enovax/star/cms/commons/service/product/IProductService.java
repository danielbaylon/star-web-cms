package com.enovax.star.cms.commons.service.product;

import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.product.AxProductLinkPackage;
import com.enovax.star.cms.commons.model.product.AxProductRelatedLinkPackage;
import com.enovax.star.cms.commons.model.product.ProductExtViewModel;
import com.enovax.star.cms.commons.model.product.crosssell.CrossSellLinkPackage;
import com.enovax.star.cms.commons.model.product.promotions.PromotionLinkPackage;
import com.enovax.star.cms.commons.service.axchannel.AxChannelException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface IProductService {

    boolean updateProductExtDataCache(StoreApiChannels channel) throws AxChannelException;

    ApiResult<List<ProductExtViewModel>> getDataForProducts(StoreApiChannels channel, List<String> productIds) throws AxChannelException;

    ApiResult<AxProductRelatedLinkPackage> getValidRelatedAxProducts(StoreApiChannels channel);

    ApiResult<AxProductLinkPackage> getValidAxProducts(StoreApiChannels channel);

    ApiResult<PromotionLinkPackage> getFullDiscountData(StoreApiChannels channel, Map<Long, BigDecimal> mainProductPriceMap);

    ApiResult<CrossSellLinkPackage> getFullUpCrossSellData(StoreApiChannels channel) throws AxChannelException;

}
