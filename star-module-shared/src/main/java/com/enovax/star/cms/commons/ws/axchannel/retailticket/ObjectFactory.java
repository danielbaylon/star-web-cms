
package com.enovax.star.cms.commons.ws.axchannel.retailticket;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.enovax.star.cms.commons.ws.axchannel.retailticket package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SDCCartRetailTicketItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketItem");
    private final static QName _SDCUpCrossSellTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_UpCrossSellTable");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _SDCCartRetailTicketPINLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketPINLineItem");
    private final static QName _ArrayOfSDCFacility_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_Facility");
    private final static QName _SDCInsertOnlineReferenceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_InsertOnlineReferenceResponse");
    private final static QName _ArrayOfSDCRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_RetailTicketTable");
    private final static QName _ArrayOfstring_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _SDCCartRetailTicketPINItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketPINItem");
    private final static QName _SDCCartRetailTicketTableResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_CartRetailTicketTableResponse");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _ArrayOfSDCUpCrossSellTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_UpCrossSellTable");
    private final static QName _SDCFacility_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_Facility");
    private final static QName _ArrayOfSDCCartRetailTicketPINItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketPINItem");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _SDCPrintTokenTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PrintTokenTable");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _SDCUseDiscountCounterResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_UseDiscountCounterResponse");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ResponseError");
    private final static QName _ArrayOfSDCPrintTokenTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_PrintTokenTable");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _ArrayOfint_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfint");
    private final static QName _SDCUpCrossSellLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_UpCrossSellLineTable");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _SDCRetailTicketTableResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "SDC_RetailTicketTableResponse");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _ArrayOfSDCInsertOnlineReferenceTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_InsertOnlineReferenceTable");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _SDCCartTicketListCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_CartTicketListCriteria");
    private final static QName _SDCCartRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketTable");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfSDCUpCrossSellLineTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_UpCrossSellLineTable");
    private final static QName _ArrayOfSDCCartTicketListCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_CartTicketListCriteria");
    private final static QName _ArrayOfSDCUseDiscountCounterTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_UseDiscountCounterTable");
    private final static QName _ArrayOfSDCCartRetailTicketLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketLineItem");
    private final static QName _ArrayOfSDCCartRetailTicketPINLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketPINLineItem");
    private final static QName _SDCInsertOnlineReferenceTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_InsertOnlineReferenceTable");
    private final static QName _SDCCartRetailTicketLineItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketLineItem");
    private final static QName _SDCRetailTicketTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_RetailTicketTable");
    private final static QName _ArrayOfSDCCartRetailTicketItem_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ArrayOfSDC_CartRetailTicketItem");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _SDCGetDiscountCounterResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetDiscountCounterResponse");
    private final static QName _SDCGetUpCrossSellResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "SDC_GetUpCrossSellResponse");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _CommerceEntitySearch_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", "CommerceEntitySearch");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _UpCrossSellTableCriteria_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", "UpCrossSellTableCriteria");
    private final static QName _SDCDiscountCounterTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_DiscountCounterTable");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _ArrayOfSDCDiscountCounterTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ArrayOfSDC_DiscountCounterTable");
    private final static QName _ArrayOfResponseError_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ArrayOfResponseError");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _SDCUseDiscountCounterTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "SDC_UseDiscountCounterTable");
    private final static QName _ServiceResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ServiceResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _UseDiscountCounterDiscountCode_QNAME = new QName("http://tempuri.org/", "discountCode");
    private final static QName _UseDiscountCounterOfferId_QNAME = new QName("http://tempuri.org/", "offerId");
    private final static QName _UseDiscountCounterTransactionId_QNAME = new QName("http://tempuri.org/", "transactionId");
    private final static QName _UseDiscountCounterInventTransId_QNAME = new QName("http://tempuri.org/", "inventTransId");
    private final static QName _SDCInsertOnlineReferenceTableTRANSACTIONID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TRANSACTIONID");
    private final static QName _SDCInsertOnlineReferenceTableMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "Message");
    private final static QName _SDCCartRetailTicketLineItemDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Description");
    private final static QName _SDCCartRetailTicketLineItemLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "LineId");
    private final static QName _SDCCartRetailTicketLineItemDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "DataAreaId");
    private final static QName _SDCCartRetailTicketLineItemEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventGroupId");
    private final static QName _SDCCartRetailTicketLineItemItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ItemId");
    private final static QName _SDCCartRetailTicketLineItemTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TransactionId");
    private final static QName _SDCCartRetailTicketLineItemUsageValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "UsageValidityId");
    private final static QName _SDCCartRetailTicketLineItemRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RetailVariantId");
    private final static QName _SDCCartRetailTicketLineItemPackageLineItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageLineItemId");
    private final static QName _SDCCartRetailTicketLineItemTicketTableId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketTableId");
    private final static QName _SDCCartRetailTicketLineItemEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "EventLineId");
    private final static QName _SDCCartRetailTicketLineItemRefTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "RefTransactionId");
    private final static QName _SDCCartRetailTicketLineItemPackageId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageId");
    private final static QName _SDCCartRetailTicketLineItemPackageLineGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PackageLineGroup");
    private final static QName _SDCCartRetailTicketLineItemOpenValidityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OpenValidityId");
    private final static QName _SDCUpCrossSellTableITEMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ITEMID");
    private final static QName _SDCUpCrossSellTableUpCrossSellLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UpCrossSellLines");
    private final static QName _SDCUpCrossSellTableUNITIDId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UNITIDId");
    private final static QName _CartCheckoutStartOnlineCartTicketListCriteria_QNAME = new QName("http://tempuri.org/", "CartTicketListCriteria");
    private final static QName _SDCPrintTokenTableTokenKey_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TokenKey");
    private final static QName _SDCPrintTokenTableValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "Value");
    private final static QName _CartValidateAndReserveQuantityResponseCartValidateAndReserveQuantityResult_QNAME = new QName("http://tempuri.org/", "CartValidateAndReserveQuantityResult");
    private final static QName _SDCCartRetailTicketItemPAXID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PAXID");
    private final static QName _SDCCartRetailTicketItemProductName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductName");
    private final static QName _SDCCartRetailTicketItemSDCFacilityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_FacilityCollection");
    private final static QName _SDCCartRetailTicketItemSDCPrintTokenCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_PrintTokenCollection");
    private final static QName _SDCCartRetailTicketItemPinCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PinCode");
    private final static QName _SDCCartRetailTicketItemTicketCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketCode");
    private final static QName _SDCCartRetailTicketItemSDCCartRetailTicketLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketLineCollection");
    private final static QName _SDCCartRetailTicketItemTemplateName_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TemplateName");
    private final static QName _SDCCartRetailTicketItemOrigTicketCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OrigTicketCode");
    private final static QName _SDCCartRetailTicketItemTicketCodePackage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "TicketCodePackage");
    private final static QName _SDCUpCrossSellLineTableORIGITEMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ORIGITEMID");
    private final static QName _SDCUpCrossSellLineTableINVENTDIMID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "INVENTDIMID");
    private final static QName _SDCUpCrossSellLineTableVARIANTID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "VARIANTID");
    private final static QName _SDCUpCrossSellLineTableUPCROSSSELLITEM_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UPCROSSSELLITEM");
    private final static QName _SDCUpCrossSellLineTableUNITID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "UNITID");
    private final static QName _SDCGetUpCrossSellResponseUpCrossSellEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "upCrossSellEntity");
    private final static QName _UpCrossSellTableCriteriaItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", "ItemId");
    private final static QName _InsertOnlineReferenceTransactionID_QNAME = new QName("http://tempuri.org/", "transactionID");
    private final static QName _UseDiscountCounterResponseUseDiscountCounterResult_QNAME = new QName("http://tempuri.org/", "UseDiscountCounterResult");
    private final static QName _SDCCartTicketListCriteriaEventGroupId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventGroupId");
    private final static QName _SDCCartTicketListCriteriaItemId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "ItemId");
    private final static QName _SDCCartTicketListCriteriaTransactionId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "TransactionId");
    private final static QName _SDCCartTicketListCriteriaLineNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineNumber");
    private final static QName _SDCCartTicketListCriteriaRetailVariantId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "RetailVariantId");
    private final static QName _SDCCartTicketListCriteriaEventLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "EventLineId");
    private final static QName _SDCCartTicketListCriteriaLineId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "LineId");
    private final static QName _SDCCartTicketListCriteriaDataAreaId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "DataAreaId");
    private final static QName _SDCCartTicketListCriteriaAccountNum_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "AccountNum");
    private final static QName _CartCheckoutCancelResponseCartCheckoutCancelResult_QNAME = new QName("http://tempuri.org/", "CartCheckoutCancelResult");
    private final static QName _CartValidateAndReserveQuantityOnlineResponseCartValidateAndReserveQuantityOnlineResult_QNAME = new QName("http://tempuri.org/", "CartValidateAndReserveQuantityOnlineResult");
    private final static QName _SDCCartRetailTicketPINItemSDCCartRetailTicketPINLineCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SDC_CartRetailTicketPINLineCollection");
    private final static QName _SDCCartRetailTicketPINItemProductPINCodePackage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ProductPINCodePackage");
    private final static QName _SDCCartRetailTicketPINItemPRODUCTPINCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PRODUCTPINCODE");
    private final static QName _SDCCartRetailTicketPINItemCustAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CustAccount");
    private final static QName _SDCCartRetailTicketPINItemPaxId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "PaxId");
    private final static QName _SDCCartRetailTicketPINItemREFERENCEID_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "REFERENCEID");
    private final static QName _ServiceResponseRedirectUrl_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RedirectUrl");
    private final static QName _ServiceResponseErrors_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Errors");
    private final static QName _SDCCartRetailTicketTableCartRetailTicketTableEntityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CartRetailTicketTableEntityCollection");
    private final static QName _SDCCartRetailTicketTableCartRetailTicketPINTableEntityCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "CartRetailTicketPINTableEntityCollection");
    private final static QName _SDCCartRetailTicketTableErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ErrorMessage");
    private final static QName _SDCRetailTicketTableResponseRetailTicketTableCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "RetailTicketTableCollection");
    private final static QName _CartValidateQuantityResponseCartValidateQuantityResult_QNAME = new QName("http://tempuri.org/", "CartValidateQuantityResult");
    private final static QName _GetUpCrossSellCriteria_QNAME = new QName("http://tempuri.org/", "criteria");
    private final static QName _SDCFacilityFacilityId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "facilityId");
    private final static QName _SDCFacilityDaysOfWeekUsage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "daysOfWeekUsage");
    private final static QName _SDCFacilityOperationIds_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "operationIds");
    private final static QName _SDCCartRetailTicketTableResponseCartRetailTicketCollection_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "CartRetailTicketCollection");
    private final static QName _CartCheckoutStartOnlineResponseCartCheckoutStartOnlineResult_QNAME = new QName("http://tempuri.org/", "CartCheckoutStartOnlineResult");
    private final static QName _GetUpCrossSellResponseGetUpCrossSellResult_QNAME = new QName("http://tempuri.org/", "GetUpCrossSellResult");
    private final static QName _ResponseErrorErrorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorCode");
    private final static QName _ResponseErrorExtendedErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ExtendedErrorMessage");
    private final static QName _ResponseErrorSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "Source");
    private final static QName _ResponseErrorErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "ErrorMessage");
    private final static QName _ResponseErrorStackTrace_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", "StackTrace");
    private final static QName _SDCGetDiscountCounterResponseDiscountCounterEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "discountCounterEntity");
    private final static QName _SDCUseDiscountCounterResponseUseDiscountCounterEntity_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "useDiscountCounterEntity");
    private final static QName _RetailTicketSearchResponseRetailTicketSearchResult_QNAME = new QName("http://tempuri.org/", "RetailTicketSearchResult");
    private final static QName _CartCheckoutCompleteResponseCartCheckoutCompleteResult_QNAME = new QName("http://tempuri.org/", "CartCheckoutCompleteResult");
    private final static QName _SDCInsertOnlineReferenceResponseInsertOnlineReferenceTable_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", "insertOnlineReferenceTable");
    private final static QName _SDCUseDiscountCounterTableOfferId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", "OfferId");
    private final static QName _SDCCartRetailTicketPINLineItemInventtransId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InventtransId");
    private final static QName _SDCCartRetailTicketPINLineItemSalesId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "SalesId");
    private final static QName _SDCCartRetailTicketPINLineItemInvoiceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "InvoiceId");
    private final static QName _SDCCartRetailTicketPINLineItemOpenValidityRule_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "OpenValidityRule");
    private final static QName _SDCCartRetailTicketPINLineItemReferenceId_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "ReferenceId");
    private final static QName _SDCCartRetailTicketPINLineItemMediaType_QNAME = new QName("http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", "MediaType");
    private final static QName _GetDiscountCounterResponseGetDiscountCounterResult_QNAME = new QName("http://tempuri.org/", "GetDiscountCounterResult");
    private final static QName _InsertOnlineReferenceResponseInsertOnlineReferenceResult_QNAME = new QName("http://tempuri.org/", "insertOnlineReferenceResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.enovax.star.cms.commons.ws.axchannel.retailticket
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UseDiscountCounter }
     * 
     */
    public UseDiscountCounter createUseDiscountCounter() {
        return new UseDiscountCounter();
    }

    /**
     * Create an instance of {@link CartValidateAndReserveQuantity }
     * 
     */
    public CartValidateAndReserveQuantity createCartValidateAndReserveQuantity() {
        return new CartValidateAndReserveQuantity();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartTicketListCriteria }
     * 
     */
    public ArrayOfSDCCartTicketListCriteria createArrayOfSDCCartTicketListCriteria() {
        return new ArrayOfSDCCartTicketListCriteria();
    }

    /**
     * Create an instance of {@link GetUpCrossSell }
     * 
     */
    public GetUpCrossSell createGetUpCrossSell() {
        return new GetUpCrossSell();
    }

    /**
     * Create an instance of {@link UpCrossSellTableCriteria }
     * 
     */
    public UpCrossSellTableCriteria createUpCrossSellTableCriteria() {
        return new UpCrossSellTableCriteria();
    }

    /**
     * Create an instance of {@link CartValidateQuantity }
     * 
     */
    public CartValidateQuantity createCartValidateQuantity() {
        return new CartValidateQuantity();
    }

    /**
     * Create an instance of {@link CartValidateAndReserveQuantityResponse }
     * 
     */
    public CartValidateAndReserveQuantityResponse createCartValidateAndReserveQuantityResponse() {
        return new CartValidateAndReserveQuantityResponse();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketTableResponse }
     * 
     */
    public SDCCartRetailTicketTableResponse createSDCCartRetailTicketTableResponse() {
        return new SDCCartRetailTicketTableResponse();
    }

    /**
     * Create an instance of {@link CartValidateAndReserveQuantityOnline }
     * 
     */
    public CartValidateAndReserveQuantityOnline createCartValidateAndReserveQuantityOnline() {
        return new CartValidateAndReserveQuantityOnline();
    }

    /**
     * Create an instance of {@link CartCheckoutStartOnlineResponse }
     * 
     */
    public CartCheckoutStartOnlineResponse createCartCheckoutStartOnlineResponse() {
        return new CartCheckoutStartOnlineResponse();
    }

    /**
     * Create an instance of {@link RetailTicketSearchResponse }
     * 
     */
    public RetailTicketSearchResponse createRetailTicketSearchResponse() {
        return new RetailTicketSearchResponse();
    }

    /**
     * Create an instance of {@link SDCRetailTicketTableResponse }
     * 
     */
    public SDCRetailTicketTableResponse createSDCRetailTicketTableResponse() {
        return new SDCRetailTicketTableResponse();
    }

    /**
     * Create an instance of {@link CartCheckoutStartOnline }
     * 
     */
    public CartCheckoutStartOnline createCartCheckoutStartOnline() {
        return new CartCheckoutStartOnline();
    }

    /**
     * Create an instance of {@link RetailTicketSearch }
     * 
     */
    public RetailTicketSearch createRetailTicketSearch() {
        return new RetailTicketSearch();
    }

    /**
     * Create an instance of {@link CartCheckoutComplete }
     * 
     */
    public CartCheckoutComplete createCartCheckoutComplete() {
        return new CartCheckoutComplete();
    }

    /**
     * Create an instance of {@link CartCheckoutCompleteResponse }
     * 
     */
    public CartCheckoutCompleteResponse createCartCheckoutCompleteResponse() {
        return new CartCheckoutCompleteResponse();
    }

    /**
     * Create an instance of {@link GetUpCrossSellResponse }
     * 
     */
    public GetUpCrossSellResponse createGetUpCrossSellResponse() {
        return new GetUpCrossSellResponse();
    }

    /**
     * Create an instance of {@link SDCGetUpCrossSellResponse }
     * 
     */
    public SDCGetUpCrossSellResponse createSDCGetUpCrossSellResponse() {
        return new SDCGetUpCrossSellResponse();
    }

    /**
     * Create an instance of {@link InsertOnlineReference }
     * 
     */
    public InsertOnlineReference createInsertOnlineReference() {
        return new InsertOnlineReference();
    }

    /**
     * Create an instance of {@link CartCheckoutCancel }
     * 
     */
    public CartCheckoutCancel createCartCheckoutCancel() {
        return new CartCheckoutCancel();
    }

    /**
     * Create an instance of {@link CartCheckoutCancelResponse }
     * 
     */
    public CartCheckoutCancelResponse createCartCheckoutCancelResponse() {
        return new CartCheckoutCancelResponse();
    }

    /**
     * Create an instance of {@link CartValidateQuantityResponse }
     * 
     */
    public CartValidateQuantityResponse createCartValidateQuantityResponse() {
        return new CartValidateQuantityResponse();
    }

    /**
     * Create an instance of {@link CartValidateAndReserveQuantityOnlineResponse }
     * 
     */
    public CartValidateAndReserveQuantityOnlineResponse createCartValidateAndReserveQuantityOnlineResponse() {
        return new CartValidateAndReserveQuantityOnlineResponse();
    }

    /**
     * Create an instance of {@link UseDiscountCounterResponse }
     * 
     */
    public UseDiscountCounterResponse createUseDiscountCounterResponse() {
        return new UseDiscountCounterResponse();
    }

    /**
     * Create an instance of {@link SDCUseDiscountCounterResponse }
     * 
     */
    public SDCUseDiscountCounterResponse createSDCUseDiscountCounterResponse() {
        return new SDCUseDiscountCounterResponse();
    }

    /**
     * Create an instance of {@link GetDiscountCounterResponse }
     * 
     */
    public GetDiscountCounterResponse createGetDiscountCounterResponse() {
        return new GetDiscountCounterResponse();
    }

    /**
     * Create an instance of {@link SDCGetDiscountCounterResponse }
     * 
     */
    public SDCGetDiscountCounterResponse createSDCGetDiscountCounterResponse() {
        return new SDCGetDiscountCounterResponse();
    }

    /**
     * Create an instance of {@link InsertOnlineReferenceResponse }
     * 
     */
    public InsertOnlineReferenceResponse createInsertOnlineReferenceResponse() {
        return new InsertOnlineReferenceResponse();
    }

    /**
     * Create an instance of {@link SDCInsertOnlineReferenceResponse }
     * 
     */
    public SDCInsertOnlineReferenceResponse createSDCInsertOnlineReferenceResponse() {
        return new SDCInsertOnlineReferenceResponse();
    }

    /**
     * Create an instance of {@link GetDiscountCounter }
     * 
     */
    public GetDiscountCounter createGetDiscountCounter() {
        return new GetDiscountCounter();
    }

    /**
     * Create an instance of {@link ArrayOfSDCUseDiscountCounterTable }
     * 
     */
    public ArrayOfSDCUseDiscountCounterTable createArrayOfSDCUseDiscountCounterTable() {
        return new ArrayOfSDCUseDiscountCounterTable();
    }

    /**
     * Create an instance of {@link SDCUpCrossSellTable }
     * 
     */
    public SDCUpCrossSellTable createSDCUpCrossSellTable() {
        return new SDCUpCrossSellTable();
    }

    /**
     * Create an instance of {@link SDCUseDiscountCounterTable }
     * 
     */
    public SDCUseDiscountCounterTable createSDCUseDiscountCounterTable() {
        return new SDCUseDiscountCounterTable();
    }

    /**
     * Create an instance of {@link SDCUpCrossSellLineTable }
     * 
     */
    public SDCUpCrossSellLineTable createSDCUpCrossSellLineTable() {
        return new SDCUpCrossSellLineTable();
    }

    /**
     * Create an instance of {@link SDCInsertOnlineReferenceTable }
     * 
     */
    public SDCInsertOnlineReferenceTable createSDCInsertOnlineReferenceTable() {
        return new SDCInsertOnlineReferenceTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCUpCrossSellTable }
     * 
     */
    public ArrayOfSDCUpCrossSellTable createArrayOfSDCUpCrossSellTable() {
        return new ArrayOfSDCUpCrossSellTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCDiscountCounterTable }
     * 
     */
    public ArrayOfSDCDiscountCounterTable createArrayOfSDCDiscountCounterTable() {
        return new ArrayOfSDCDiscountCounterTable();
    }

    /**
     * Create an instance of {@link SDCCartTicketListCriteria }
     * 
     */
    public SDCCartTicketListCriteria createSDCCartTicketListCriteria() {
        return new SDCCartTicketListCriteria();
    }

    /**
     * Create an instance of {@link ArrayOfSDCInsertOnlineReferenceTable }
     * 
     */
    public ArrayOfSDCInsertOnlineReferenceTable createArrayOfSDCInsertOnlineReferenceTable() {
        return new ArrayOfSDCInsertOnlineReferenceTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCUpCrossSellLineTable }
     * 
     */
    public ArrayOfSDCUpCrossSellLineTable createArrayOfSDCUpCrossSellLineTable() {
        return new ArrayOfSDCUpCrossSellLineTable();
    }

    /**
     * Create an instance of {@link SDCDiscountCounterTable }
     * 
     */
    public SDCDiscountCounterTable createSDCDiscountCounterTable() {
        return new SDCDiscountCounterTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketPINItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketPINItem createArrayOfSDCCartRetailTicketPINItem() {
        return new ArrayOfSDCCartRetailTicketPINItem();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketTable }
     * 
     */
    public SDCCartRetailTicketTable createSDCCartRetailTicketTable() {
        return new SDCCartRetailTicketTable();
    }

    /**
     * Create an instance of {@link SDCPrintTokenTable }
     * 
     */
    public SDCPrintTokenTable createSDCPrintTokenTable() {
        return new SDCPrintTokenTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketItem }
     * 
     */
    public SDCCartRetailTicketItem createSDCCartRetailTicketItem() {
        return new SDCCartRetailTicketItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketLineItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketLineItem createArrayOfSDCCartRetailTicketLineItem() {
        return new ArrayOfSDCCartRetailTicketLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketPINLineItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketPINLineItem createArrayOfSDCCartRetailTicketPINLineItem() {
        return new ArrayOfSDCCartRetailTicketPINLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCPrintTokenTable }
     * 
     */
    public ArrayOfSDCPrintTokenTable createArrayOfSDCPrintTokenTable() {
        return new ArrayOfSDCPrintTokenTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketPINLineItem }
     * 
     */
    public SDCCartRetailTicketPINLineItem createSDCCartRetailTicketPINLineItem() {
        return new SDCCartRetailTicketPINLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCFacility }
     * 
     */
    public ArrayOfSDCFacility createArrayOfSDCFacility() {
        return new ArrayOfSDCFacility();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketLineItem }
     * 
     */
    public SDCCartRetailTicketLineItem createSDCCartRetailTicketLineItem() {
        return new SDCCartRetailTicketLineItem();
    }

    /**
     * Create an instance of {@link ArrayOfSDCCartRetailTicketItem }
     * 
     */
    public ArrayOfSDCCartRetailTicketItem createArrayOfSDCCartRetailTicketItem() {
        return new ArrayOfSDCCartRetailTicketItem();
    }

    /**
     * Create an instance of {@link SDCRetailTicketTable }
     * 
     */
    public SDCRetailTicketTable createSDCRetailTicketTable() {
        return new SDCRetailTicketTable();
    }

    /**
     * Create an instance of {@link ArrayOfSDCRetailTicketTable }
     * 
     */
    public ArrayOfSDCRetailTicketTable createArrayOfSDCRetailTicketTable() {
        return new ArrayOfSDCRetailTicketTable();
    }

    /**
     * Create an instance of {@link SDCCartRetailTicketPINItem }
     * 
     */
    public SDCCartRetailTicketPINItem createSDCCartRetailTicketPINItem() {
        return new SDCCartRetailTicketPINItem();
    }

    /**
     * Create an instance of {@link SDCFacility }
     * 
     */
    public SDCFacility createSDCFacility() {
        return new SDCFacility();
    }

    /**
     * Create an instance of {@link ServiceResponse }
     * 
     */
    public ServiceResponse createServiceResponse() {
        return new ServiceResponse();
    }

    /**
     * Create an instance of {@link ArrayOfResponseError }
     * 
     */
    public ArrayOfResponseError createArrayOfResponseError() {
        return new ArrayOfResponseError();
    }

    /**
     * Create an instance of {@link ResponseError }
     * 
     */
    public ResponseError createResponseError() {
        return new ResponseError();
    }

    /**
     * Create an instance of {@link CommerceEntitySearch }
     * 
     */
    public CommerceEntitySearch createCommerceEntitySearch() {
        return new CommerceEntitySearch();
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link ArrayOfint }
     * 
     */
    public ArrayOfint createArrayOfint() {
        return new ArrayOfint();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketItem")
    public JAXBElement<SDCCartRetailTicketItem> createSDCCartRetailTicketItem(SDCCartRetailTicketItem value) {
        return new JAXBElement<SDCCartRetailTicketItem>(_SDCCartRetailTicketItem_QNAME, SDCCartRetailTicketItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpCrossSellTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_UpCrossSellTable")
    public JAXBElement<SDCUpCrossSellTable> createSDCUpCrossSellTable(SDCUpCrossSellTable value) {
        return new JAXBElement<SDCUpCrossSellTable>(_SDCUpCrossSellTable_QNAME, SDCUpCrossSellTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketPINLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketPINLineItem")
    public JAXBElement<SDCCartRetailTicketPINLineItem> createSDCCartRetailTicketPINLineItem(SDCCartRetailTicketPINLineItem value) {
        return new JAXBElement<SDCCartRetailTicketPINLineItem>(_SDCCartRetailTicketPINLineItem_QNAME, SDCCartRetailTicketPINLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_Facility")
    public JAXBElement<ArrayOfSDCFacility> createArrayOfSDCFacility(ArrayOfSDCFacility value) {
        return new JAXBElement<ArrayOfSDCFacility>(_ArrayOfSDCFacility_QNAME, ArrayOfSDCFacility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInsertOnlineReferenceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_InsertOnlineReferenceResponse")
    public JAXBElement<SDCInsertOnlineReferenceResponse> createSDCInsertOnlineReferenceResponse(SDCInsertOnlineReferenceResponse value) {
        return new JAXBElement<SDCInsertOnlineReferenceResponse>(_SDCInsertOnlineReferenceResponse_QNAME, SDCInsertOnlineReferenceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_RetailTicketTable")
    public JAXBElement<ArrayOfSDCRetailTicketTable> createArrayOfSDCRetailTicketTable(ArrayOfSDCRetailTicketTable value) {
        return new JAXBElement<ArrayOfSDCRetailTicketTable>(_ArrayOfSDCRetailTicketTable_QNAME, ArrayOfSDCRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketPINItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketPINItem")
    public JAXBElement<SDCCartRetailTicketPINItem> createSDCCartRetailTicketPINItem(SDCCartRetailTicketPINItem value) {
        return new JAXBElement<SDCCartRetailTicketPINItem>(_SDCCartRetailTicketPINItem_QNAME, SDCCartRetailTicketPINItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_CartRetailTicketTableResponse")
    public JAXBElement<SDCCartRetailTicketTableResponse> createSDCCartRetailTicketTableResponse(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_SDCCartRetailTicketTableResponse_QNAME, SDCCartRetailTicketTableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_UpCrossSellTable")
    public JAXBElement<ArrayOfSDCUpCrossSellTable> createArrayOfSDCUpCrossSellTable(ArrayOfSDCUpCrossSellTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellTable>(_ArrayOfSDCUpCrossSellTable_QNAME, ArrayOfSDCUpCrossSellTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_Facility")
    public JAXBElement<SDCFacility> createSDCFacility(SDCFacility value) {
        return new JAXBElement<SDCFacility>(_SDCFacility_QNAME, SDCFacility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketPINItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketPINItem> createArrayOfSDCCartRetailTicketPINItem(ArrayOfSDCCartRetailTicketPINItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINItem>(_ArrayOfSDCCartRetailTicketPINItem_QNAME, ArrayOfSDCCartRetailTicketPINItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCPrintTokenTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PrintTokenTable")
    public JAXBElement<SDCPrintTokenTable> createSDCPrintTokenTable(SDCPrintTokenTable value) {
        return new JAXBElement<SDCPrintTokenTable>(_SDCPrintTokenTable_QNAME, SDCPrintTokenTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUseDiscountCounterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_UseDiscountCounterResponse")
    public JAXBElement<SDCUseDiscountCounterResponse> createSDCUseDiscountCounterResponse(SDCUseDiscountCounterResponse value) {
        return new JAXBElement<SDCUseDiscountCounterResponse>(_SDCUseDiscountCounterResponse_QNAME, SDCUseDiscountCounterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ResponseError")
    public JAXBElement<ResponseError> createResponseError(ResponseError value) {
        return new JAXBElement<ResponseError>(_ResponseError_QNAME, ResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPrintTokenTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_PrintTokenTable")
    public JAXBElement<ArrayOfSDCPrintTokenTable> createArrayOfSDCPrintTokenTable(ArrayOfSDCPrintTokenTable value) {
        return new JAXBElement<ArrayOfSDCPrintTokenTable>(_ArrayOfSDCPrintTokenTable_QNAME, ArrayOfSDCPrintTokenTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfint")
    public JAXBElement<ArrayOfint> createArrayOfint(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_ArrayOfint_QNAME, ArrayOfint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUpCrossSellLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_UpCrossSellLineTable")
    public JAXBElement<SDCUpCrossSellLineTable> createSDCUpCrossSellLineTable(SDCUpCrossSellLineTable value) {
        return new JAXBElement<SDCUpCrossSellLineTable>(_SDCUpCrossSellLineTable_QNAME, SDCUpCrossSellLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "SDC_RetailTicketTableResponse")
    public JAXBElement<SDCRetailTicketTableResponse> createSDCRetailTicketTableResponse(SDCRetailTicketTableResponse value) {
        return new JAXBElement<SDCRetailTicketTableResponse>(_SDCRetailTicketTableResponse_QNAME, SDCRetailTicketTableResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCInsertOnlineReferenceTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_InsertOnlineReferenceTable")
    public JAXBElement<ArrayOfSDCInsertOnlineReferenceTable> createArrayOfSDCInsertOnlineReferenceTable(ArrayOfSDCInsertOnlineReferenceTable value) {
        return new JAXBElement<ArrayOfSDCInsertOnlineReferenceTable>(_ArrayOfSDCInsertOnlineReferenceTable_QNAME, ArrayOfSDCInsertOnlineReferenceTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_CartTicketListCriteria")
    public JAXBElement<SDCCartTicketListCriteria> createSDCCartTicketListCriteria(SDCCartTicketListCriteria value) {
        return new JAXBElement<SDCCartTicketListCriteria>(_SDCCartTicketListCriteria_QNAME, SDCCartTicketListCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketTable")
    public JAXBElement<SDCCartRetailTicketTable> createSDCCartRetailTicketTable(SDCCartRetailTicketTable value) {
        return new JAXBElement<SDCCartRetailTicketTable>(_SDCCartRetailTicketTable_QNAME, SDCCartRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_UpCrossSellLineTable")
    public JAXBElement<ArrayOfSDCUpCrossSellLineTable> createArrayOfSDCUpCrossSellLineTable(ArrayOfSDCUpCrossSellLineTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellLineTable>(_ArrayOfSDCUpCrossSellLineTable_QNAME, ArrayOfSDCUpCrossSellLineTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_CartTicketListCriteria")
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createArrayOfSDCCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_ArrayOfSDCCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUseDiscountCounterTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_UseDiscountCounterTable")
    public JAXBElement<ArrayOfSDCUseDiscountCounterTable> createArrayOfSDCUseDiscountCounterTable(ArrayOfSDCUseDiscountCounterTable value) {
        return new JAXBElement<ArrayOfSDCUseDiscountCounterTable>(_ArrayOfSDCUseDiscountCounterTable_QNAME, ArrayOfSDCUseDiscountCounterTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketLineItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketLineItem> createArrayOfSDCCartRetailTicketLineItem(ArrayOfSDCCartRetailTicketLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketLineItem>(_ArrayOfSDCCartRetailTicketLineItem_QNAME, ArrayOfSDCCartRetailTicketLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketPINLineItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> createArrayOfSDCCartRetailTicketPINLineItem(ArrayOfSDCCartRetailTicketPINLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem>(_ArrayOfSDCCartRetailTicketPINLineItem_QNAME, ArrayOfSDCCartRetailTicketPINLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInsertOnlineReferenceTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_InsertOnlineReferenceTable")
    public JAXBElement<SDCInsertOnlineReferenceTable> createSDCInsertOnlineReferenceTable(SDCInsertOnlineReferenceTable value) {
        return new JAXBElement<SDCInsertOnlineReferenceTable>(_SDCInsertOnlineReferenceTable_QNAME, SDCInsertOnlineReferenceTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketLineItem")
    public JAXBElement<SDCCartRetailTicketLineItem> createSDCCartRetailTicketLineItem(SDCCartRetailTicketLineItem value) {
        return new JAXBElement<SDCCartRetailTicketLineItem>(_SDCCartRetailTicketLineItem_QNAME, SDCCartRetailTicketLineItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_RetailTicketTable")
    public JAXBElement<SDCRetailTicketTable> createSDCRetailTicketTable(SDCRetailTicketTable value) {
        return new JAXBElement<SDCRetailTicketTable>(_SDCRetailTicketTable_QNAME, SDCRetailTicketTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ArrayOfSDC_CartRetailTicketItem")
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> createArrayOfSDCCartRetailTicketItem(ArrayOfSDCCartRetailTicketItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketItem>(_ArrayOfSDCCartRetailTicketItem_QNAME, ArrayOfSDCCartRetailTicketItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetDiscountCounterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetDiscountCounterResponse")
    public JAXBElement<SDCGetDiscountCounterResponse> createSDCGetDiscountCounterResponse(SDCGetDiscountCounterResponse value) {
        return new JAXBElement<SDCGetDiscountCounterResponse>(_SDCGetDiscountCounterResponse_QNAME, SDCGetDiscountCounterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetUpCrossSellResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "SDC_GetUpCrossSellResponse")
    public JAXBElement<SDCGetUpCrossSellResponse> createSDCGetUpCrossSellResponse(SDCGetUpCrossSellResponse value) {
        return new JAXBElement<SDCGetUpCrossSellResponse>(_SDCGetUpCrossSellResponse_QNAME, SDCGetUpCrossSellResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommerceEntitySearch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Commerce.Runtime.DataModel", name = "CommerceEntitySearch")
    public JAXBElement<CommerceEntitySearch> createCommerceEntitySearch(CommerceEntitySearch value) {
        return new JAXBElement<CommerceEntitySearch>(_CommerceEntitySearch_QNAME, CommerceEntitySearch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpCrossSellTableCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", name = "UpCrossSellTableCriteria")
    public JAXBElement<UpCrossSellTableCriteria> createUpCrossSellTableCriteria(UpCrossSellTableCriteria value) {
        return new JAXBElement<UpCrossSellTableCriteria>(_UpCrossSellTableCriteria_QNAME, UpCrossSellTableCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCDiscountCounterTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_DiscountCounterTable")
    public JAXBElement<SDCDiscountCounterTable> createSDCDiscountCounterTable(SDCDiscountCounterTable value) {
        return new JAXBElement<SDCDiscountCounterTable>(_SDCDiscountCounterTable_QNAME, SDCDiscountCounterTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCDiscountCounterTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ArrayOfSDC_DiscountCounterTable")
    public JAXBElement<ArrayOfSDCDiscountCounterTable> createArrayOfSDCDiscountCounterTable(ArrayOfSDCDiscountCounterTable value) {
        return new JAXBElement<ArrayOfSDCDiscountCounterTable>(_ArrayOfSDCDiscountCounterTable_QNAME, ArrayOfSDCDiscountCounterTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ArrayOfResponseError")
    public JAXBElement<ArrayOfResponseError> createArrayOfResponseError(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ArrayOfResponseError_QNAME, ArrayOfResponseError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUseDiscountCounterTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "SDC_UseDiscountCounterTable")
    public JAXBElement<SDCUseDiscountCounterTable> createSDCUseDiscountCounterTable(SDCUseDiscountCounterTable value) {
        return new JAXBElement<SDCUseDiscountCounterTable>(_SDCUseDiscountCounterTable_QNAME, SDCUseDiscountCounterTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ServiceResponse")
    public JAXBElement<ServiceResponse> createServiceResponse(ServiceResponse value) {
        return new JAXBElement<ServiceResponse>(_ServiceResponse_QNAME, ServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "discountCode", scope = UseDiscountCounter.class)
    public JAXBElement<String> createUseDiscountCounterDiscountCode(String value) {
        return new JAXBElement<String>(_UseDiscountCounterDiscountCode_QNAME, String.class, UseDiscountCounter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "offerId", scope = UseDiscountCounter.class)
    public JAXBElement<String> createUseDiscountCounterOfferId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterOfferId_QNAME, String.class, UseDiscountCounter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = UseDiscountCounter.class)
    public JAXBElement<String> createUseDiscountCounterTransactionId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterTransactionId_QNAME, String.class, UseDiscountCounter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "inventTransId", scope = UseDiscountCounter.class)
    public JAXBElement<String> createUseDiscountCounterInventTransId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterInventTransId_QNAME, String.class, UseDiscountCounter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TRANSACTIONID", scope = SDCInsertOnlineReferenceTable.class)
    public JAXBElement<String> createSDCInsertOnlineReferenceTableTRANSACTIONID(String value) {
        return new JAXBElement<String>(_SDCInsertOnlineReferenceTableTRANSACTIONID_QNAME, String.class, SDCInsertOnlineReferenceTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "Message", scope = SDCInsertOnlineReferenceTable.class)
    public JAXBElement<String> createSDCInsertOnlineReferenceTableMessage(String value) {
        return new JAXBElement<String>(_SDCInsertOnlineReferenceTableMessage_QNAME, String.class, SDCInsertOnlineReferenceTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Description", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemDescription(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDescription_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "LineId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemLineId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventGroupId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemItemId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTransactionId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemUsageValidityId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RetailVariantId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemRetailVariantId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineItemId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemPackageLineItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemPackageLineItemId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTicketTableId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventLineId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RefTransactionId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemRefTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemRefTransactionId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemPackageId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemPackageId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineGroup", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemPackageLineGroup(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemPackageLineGroup_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCCartRetailTicketLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketLineItemOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemOpenValidityId_QNAME, String.class, SDCCartRetailTicketLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = CartCheckoutCancel.class)
    public JAXBElement<String> createCartCheckoutCancelTransactionId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterTransactionId_QNAME, String.class, CartCheckoutCancel.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ITEMID", scope = SDCUpCrossSellTable.class)
    public JAXBElement<String> createSDCUpCrossSellTableITEMID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellTableITEMID_QNAME, String.class, SDCUpCrossSellTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellLineTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UpCrossSellLines", scope = SDCUpCrossSellTable.class)
    public JAXBElement<ArrayOfSDCUpCrossSellLineTable> createSDCUpCrossSellTableUpCrossSellLines(ArrayOfSDCUpCrossSellLineTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellLineTable>(_SDCUpCrossSellTableUpCrossSellLines_QNAME, ArrayOfSDCUpCrossSellLineTable.class, SDCUpCrossSellTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UNITIDId", scope = SDCUpCrossSellTable.class)
    public JAXBElement<String> createSDCUpCrossSellTableUNITIDId(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellTableUNITIDId_QNAME, String.class, SDCUpCrossSellTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartCheckoutStartOnline.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartCheckoutStartOnlineCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartCheckoutStartOnlineCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartCheckoutStartOnline.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TokenKey", scope = SDCPrintTokenTable.class)
    public JAXBElement<String> createSDCPrintTokenTableTokenKey(String value) {
        return new JAXBElement<String>(_SDCPrintTokenTableTokenKey_QNAME, String.class, SDCPrintTokenTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "Value", scope = SDCPrintTokenTable.class)
    public JAXBElement<String> createSDCPrintTokenTableValue(String value) {
        return new JAXBElement<String>(_SDCPrintTokenTableValue_QNAME, String.class, SDCPrintTokenTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartValidateAndReserveQuantityResult", scope = CartValidateAndReserveQuantityResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartValidateAndReserveQuantityResponseCartValidateAndReserveQuantityResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartValidateAndReserveQuantityResponseCartValidateAndReserveQuantityResult_QNAME, SDCCartRetailTicketTableResponse.class, CartValidateAndReserveQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PAXID", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemPAXID(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemPAXID_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemProductName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemProductName_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_FacilityCollection", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<ArrayOfSDCFacility> createSDCCartRetailTicketItemSDCFacilityCollection(ArrayOfSDCFacility value) {
        return new JAXBElement<ArrayOfSDCFacility>(_SDCCartRetailTicketItemSDCFacilityCollection_QNAME, ArrayOfSDCFacility.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCPrintTokenTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_PrintTokenCollection", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<ArrayOfSDCPrintTokenTable> createSDCCartRetailTicketItemSDCPrintTokenCollection(ArrayOfSDCPrintTokenTable value) {
        return new JAXBElement<ArrayOfSDCPrintTokenTable>(_SDCCartRetailTicketItemSDCPrintTokenCollection_QNAME, ArrayOfSDCPrintTokenTable.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventGroupId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemItemId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PinCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemPinCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemPinCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTicketCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTransactionId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemUsageValidityId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketLineCollection", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketLineItem> createSDCCartRetailTicketItemSDCCartRetailTicketLineCollection(ArrayOfSDCCartRetailTicketLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketLineItem>(_SDCCartRetailTicketItemSDCCartRetailTicketLineCollection_QNAME, ArrayOfSDCCartRetailTicketLineItem.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTicketTableId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventLineId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TemplateName", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTemplateName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTemplateName_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OrigTicketCode", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemOrigTicketCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemOrigTicketCode_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCodePackage", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemTicketCodePackage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTicketCodePackage_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCCartRetailTicketItem.class)
    public JAXBElement<String> createSDCCartRetailTicketItemOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemOpenValidityId_QNAME, String.class, SDCCartRetailTicketItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ORIGITEMID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableORIGITEMID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableORIGITEMID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "INVENTDIMID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableINVENTDIMID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableINVENTDIMID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "VARIANTID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableVARIANTID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableVARIANTID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UPCROSSSELLITEM", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableUPCROSSSELLITEM(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableUPCROSSSELLITEM_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "UNITID", scope = SDCUpCrossSellLineTable.class)
    public JAXBElement<String> createSDCUpCrossSellLineTableUNITID(String value) {
        return new JAXBElement<String>(_SDCUpCrossSellLineTableUNITID_QNAME, String.class, SDCUpCrossSellLineTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUpCrossSellTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "upCrossSellEntity", scope = SDCGetUpCrossSellResponse.class)
    public JAXBElement<ArrayOfSDCUpCrossSellTable> createSDCGetUpCrossSellResponseUpCrossSellEntity(ArrayOfSDCUpCrossSellTable value) {
        return new JAXBElement<ArrayOfSDCUpCrossSellTable>(_SDCGetUpCrossSellResponseUpCrossSellEntity_QNAME, ArrayOfSDCUpCrossSellTable.class, SDCGetUpCrossSellResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/SDC_NonBindableCRTExtension.DataModel", name = "ItemId", scope = UpCrossSellTableCriteria.class)
    public JAXBElement<String> createUpCrossSellTableCriteriaItemId(String value) {
        return new JAXBElement<String>(_UpCrossSellTableCriteriaItemId_QNAME, String.class, UpCrossSellTableCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartValidateQuantity.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartValidateQuantityCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartCheckoutStartOnlineCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartValidateQuantity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionID", scope = InsertOnlineReference.class)
    public JAXBElement<String> createInsertOnlineReferenceTransactionID(String value) {
        return new JAXBElement<String>(_InsertOnlineReferenceTransactionID_QNAME, String.class, InsertOnlineReference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCUseDiscountCounterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "UseDiscountCounterResult", scope = UseDiscountCounterResponse.class)
    public JAXBElement<SDCUseDiscountCounterResponse> createUseDiscountCounterResponseUseDiscountCounterResult(SDCUseDiscountCounterResponse value) {
        return new JAXBElement<SDCUseDiscountCounterResponse>(_UseDiscountCounterResponseUseDiscountCounterResult_QNAME, SDCUseDiscountCounterResponse.class, UseDiscountCounterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventGroupId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventGroupId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "ItemId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaItemId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaItemId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "TransactionId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaTransactionId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineNumber", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaLineNumber(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaLineNumber_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "RetailVariantId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaRetailVariantId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "EventLineId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaEventLineId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "LineId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaLineId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaLineId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "DataAreaId", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaDataAreaId_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "AccountNum", scope = SDCCartTicketListCriteria.class)
    public JAXBElement<String> createSDCCartTicketListCriteriaAccountNum(String value) {
        return new JAXBElement<String>(_SDCCartTicketListCriteriaAccountNum_QNAME, String.class, SDCCartTicketListCriteria.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = RetailTicketSearch.class)
    public JAXBElement<String> createRetailTicketSearchTransactionId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterTransactionId_QNAME, String.class, RetailTicketSearch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartCheckoutCancelResult", scope = CartCheckoutCancelResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartCheckoutCancelResponseCartCheckoutCancelResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartCheckoutCancelResponseCartCheckoutCancelResult_QNAME, SDCCartRetailTicketTableResponse.class, CartCheckoutCancelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartValidateAndReserveQuantityOnlineResult", scope = CartValidateAndReserveQuantityOnlineResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartValidateAndReserveQuantityOnlineResponseCartValidateAndReserveQuantityOnlineResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartValidateAndReserveQuantityOnlineResponseCartValidateAndReserveQuantityOnlineResult_QNAME, SDCCartRetailTicketTableResponse.class, CartValidateAndReserveQuantityOnlineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINLineItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_CartRetailTicketPINLineCollection", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem> createSDCCartRetailTicketPINItemSDCCartRetailTicketPINLineCollection(ArrayOfSDCCartRetailTicketPINLineItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINLineItem>(_SDCCartRetailTicketPINItemSDCCartRetailTicketPINLineCollection_QNAME, ArrayOfSDCCartRetailTicketPINLineItem.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductPINCodePackage", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemProductPINCodePackage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemProductPINCodePackage_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PRODUCTPINCODE", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemPRODUCTPINCODE(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemPRODUCTPINCODE_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemProductName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemProductName_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CustAccount", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemCustAccount(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemCustAccount_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PaxId", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemPaxId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemPaxId_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCFacility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SDC_FacilityCollection", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<ArrayOfSDCFacility> createSDCCartRetailTicketPINItemSDCFacilityCollection(ArrayOfSDCFacility value) {
        return new JAXBElement<ArrayOfSDCFacility>(_SDCCartRetailTicketItemSDCFacilityCollection_QNAME, ArrayOfSDCFacility.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "REFERENCEID", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemREFERENCEID(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemREFERENCEID_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketPINItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemItemId_QNAME, String.class, SDCCartRetailTicketPINItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartValidateAndReserveQuantity.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartValidateAndReserveQuantityCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartCheckoutStartOnlineCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartValidateAndReserveQuantity.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RedirectUrl", scope = ServiceResponse.class)
    public JAXBElement<String> createServiceResponseRedirectUrl(String value) {
        return new JAXBElement<String>(_ServiceResponseRedirectUrl_QNAME, String.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponseError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Errors", scope = ServiceResponse.class)
    public JAXBElement<ArrayOfResponseError> createServiceResponseErrors(ArrayOfResponseError value) {
        return new JAXBElement<ArrayOfResponseError>(_ServiceResponseErrors_QNAME, ArrayOfResponseError.class, ServiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CartRetailTicketTableEntityCollection", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketItem> createSDCCartRetailTicketTableCartRetailTicketTableEntityCollection(ArrayOfSDCCartRetailTicketItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketItem>(_SDCCartRetailTicketTableCartRetailTicketTableEntityCollection_QNAME, ArrayOfSDCCartRetailTicketItem.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PaxId", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTablePaxId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINItemPaxId_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartRetailTicketPINItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "CartRetailTicketPINTableEntityCollection", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<ArrayOfSDCCartRetailTicketPINItem> createSDCCartRetailTicketTableCartRetailTicketPINTableEntityCollection(ArrayOfSDCCartRetailTicketPINItem value) {
        return new JAXBElement<ArrayOfSDCCartRetailTicketPINItem>(_SDCCartRetailTicketTableCartRetailTicketPINTableEntityCollection_QNAME, ArrayOfSDCCartRetailTicketPINItem.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ErrorMessage", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTableErrorMessage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketTableErrorMessage_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCCartRetailTicketTable.class)
    public JAXBElement<String> createSDCCartRetailTicketTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTransactionId_QNAME, String.class, SDCCartRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "RetailTicketTableCollection", scope = SDCRetailTicketTableResponse.class)
    public JAXBElement<ArrayOfSDCRetailTicketTable> createSDCRetailTicketTableResponseRetailTicketTableCollection(ArrayOfSDCRetailTicketTable value) {
        return new JAXBElement<ArrayOfSDCRetailTicketTable>(_SDCRetailTicketTableResponseRetailTicketTableCollection_QNAME, ArrayOfSDCRetailTicketTable.class, SDCRetailTicketTableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartValidateQuantityResult", scope = CartValidateQuantityResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartValidateQuantityResponseCartValidateQuantityResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartValidateQuantityResponseCartValidateQuantityResult_QNAME, SDCCartRetailTicketTableResponse.class, CartValidateQuantityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCCartTicketListCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartTicketListCriteria", scope = CartValidateAndReserveQuantityOnline.class)
    public JAXBElement<ArrayOfSDCCartTicketListCriteria> createCartValidateAndReserveQuantityOnlineCartTicketListCriteria(ArrayOfSDCCartTicketListCriteria value) {
        return new JAXBElement<ArrayOfSDCCartTicketListCriteria>(_CartCheckoutStartOnlineCartTicketListCriteria_QNAME, ArrayOfSDCCartTicketListCriteria.class, CartValidateAndReserveQuantityOnline.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PAXID", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTablePAXID(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemPAXID_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ProductName", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableProductName(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemProductName_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDataAreaId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventGroupId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemItemId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PinCode", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTablePinCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemPinCode_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCode", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTicketCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTicketCode_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TransactionId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTransactionId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTransactionId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "UsageValidityId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableUsageValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemUsageValidityId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTicketTableId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventLineId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OrigTicketCode", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableOrigTicketCode(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemOrigTicketCode_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketCodePackage", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableTicketCodePackage(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketItemTicketCodePackage_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityId", scope = SDCRetailTicketTable.class)
    public JAXBElement<String> createSDCRetailTicketTableOpenValidityId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemOpenValidityId_QNAME, String.class, SDCRetailTicketTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpCrossSellTableCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "criteria", scope = GetUpCrossSell.class)
    public JAXBElement<UpCrossSellTableCriteria> createGetUpCrossSellCriteria(UpCrossSellTableCriteria value) {
        return new JAXBElement<UpCrossSellTableCriteria>(_GetUpCrossSellCriteria_QNAME, UpCrossSellTableCriteria.class, GetUpCrossSell.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "facilityId", scope = SDCFacility.class)
    public JAXBElement<String> createSDCFacilityFacilityId(String value) {
        return new JAXBElement<String>(_SDCFacilityFacilityId_QNAME, String.class, SDCFacility.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "daysOfWeekUsage", scope = SDCFacility.class)
    public JAXBElement<ArrayOfint> createSDCFacilityDaysOfWeekUsage(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_SDCFacilityDaysOfWeekUsage_QNAME, ArrayOfint.class, SDCFacility.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "operationIds", scope = SDCFacility.class)
    public JAXBElement<ArrayOfstring> createSDCFacilityOperationIds(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_SDCFacilityOperationIds_QNAME, ArrayOfstring.class, SDCFacility.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "CartRetailTicketCollection", scope = SDCCartRetailTicketTableResponse.class)
    public JAXBElement<SDCCartRetailTicketTable> createSDCCartRetailTicketTableResponseCartRetailTicketCollection(SDCCartRetailTicketTable value) {
        return new JAXBElement<SDCCartRetailTicketTable>(_SDCCartRetailTicketTableResponseCartRetailTicketCollection_QNAME, SDCCartRetailTicketTable.class, SDCCartRetailTicketTableResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartCheckoutStartOnlineResult", scope = CartCheckoutStartOnlineResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartCheckoutStartOnlineResponseCartCheckoutStartOnlineResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartCheckoutStartOnlineResponseCartCheckoutStartOnlineResult_QNAME, SDCCartRetailTicketTableResponse.class, CartCheckoutStartOnlineResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetUpCrossSellResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetUpCrossSellResult", scope = GetUpCrossSellResponse.class)
    public JAXBElement<SDCGetUpCrossSellResponse> createGetUpCrossSellResponseGetUpCrossSellResult(SDCGetUpCrossSellResponse value) {
        return new JAXBElement<SDCGetUpCrossSellResponse>(_GetUpCrossSellResponseGetUpCrossSellResult_QNAME, SDCGetUpCrossSellResponse.class, GetUpCrossSellResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorCode", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorCode(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorCode_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ExtendedErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorExtendedErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorExtendedErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "Source", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorSource(String value) {
        return new JAXBElement<String>(_ResponseErrorSource_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "ErrorMessage", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorErrorMessage(String value) {
        return new JAXBElement<String>(_ResponseErrorErrorMessage_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", name = "StackTrace", scope = ResponseError.class)
    public JAXBElement<String> createResponseErrorStackTrace(String value) {
        return new JAXBElement<String>(_ResponseErrorStackTrace_QNAME, String.class, ResponseError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCDiscountCounterTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "discountCounterEntity", scope = SDCGetDiscountCounterResponse.class)
    public JAXBElement<ArrayOfSDCDiscountCounterTable> createSDCGetDiscountCounterResponseDiscountCounterEntity(ArrayOfSDCDiscountCounterTable value) {
        return new JAXBElement<ArrayOfSDCDiscountCounterTable>(_SDCGetDiscountCounterResponseDiscountCounterEntity_QNAME, ArrayOfSDCDiscountCounterTable.class, SDCGetDiscountCounterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCUseDiscountCounterTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "useDiscountCounterEntity", scope = SDCUseDiscountCounterResponse.class)
    public JAXBElement<ArrayOfSDCUseDiscountCounterTable> createSDCUseDiscountCounterResponseUseDiscountCounterEntity(ArrayOfSDCUseDiscountCounterTable value) {
        return new JAXBElement<ArrayOfSDCUseDiscountCounterTable>(_SDCUseDiscountCounterResponseUseDiscountCounterEntity_QNAME, ArrayOfSDCUseDiscountCounterTable.class, SDCUseDiscountCounterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "RetailTicketSearchResult", scope = RetailTicketSearchResponse.class)
    public JAXBElement<SDCRetailTicketTableResponse> createRetailTicketSearchResponseRetailTicketSearchResult(SDCRetailTicketTableResponse value) {
        return new JAXBElement<SDCRetailTicketTableResponse>(_RetailTicketSearchResponseRetailTicketSearchResult_QNAME, SDCRetailTicketTableResponse.class, RetailTicketSearchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "CartCheckoutCompleteResult", scope = CartCheckoutCompleteResponse.class)
    public JAXBElement<SDCCartRetailTicketTableResponse> createCartCheckoutCompleteResponseCartCheckoutCompleteResult(SDCCartRetailTicketTableResponse value) {
        return new JAXBElement<SDCCartRetailTicketTableResponse>(_CartCheckoutCompleteResponseCartCheckoutCompleteResult_QNAME, SDCCartRetailTicketTableResponse.class, CartCheckoutCompleteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSDCInsertOnlineReferenceTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", name = "insertOnlineReferenceTable", scope = SDCInsertOnlineReferenceResponse.class)
    public JAXBElement<ArrayOfSDCInsertOnlineReferenceTable> createSDCInsertOnlineReferenceResponseInsertOnlineReferenceTable(ArrayOfSDCInsertOnlineReferenceTable value) {
        return new JAXBElement<ArrayOfSDCInsertOnlineReferenceTable>(_SDCInsertOnlineReferenceResponseInsertOnlineReferenceTable_QNAME, ArrayOfSDCInsertOnlineReferenceTable.class, SDCInsertOnlineReferenceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "OfferId", scope = SDCUseDiscountCounterTable.class)
    public JAXBElement<String> createSDCUseDiscountCounterTableOfferId(String value) {
        return new JAXBElement<String>(_SDCUseDiscountCounterTableOfferId_QNAME, String.class, SDCUseDiscountCounterTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "transactionId", scope = CartCheckoutComplete.class)
    public JAXBElement<String> createCartCheckoutCompleteTransactionId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterTransactionId_QNAME, String.class, CartCheckoutComplete.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InventtransId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemInventtransId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemInventtransId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "DataAreaId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemDataAreaId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemDataAreaId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventGroupId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemEventGroupId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventGroupId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "SalesId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemSalesId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemSalesId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ItemId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemItemId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "InvoiceId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemInvoiceId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemInvoiceId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "OpenValidityRule", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemOpenValidityRule(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemOpenValidityRule_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "RetailVariantId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemRetailVariantId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemRetailVariantId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineItemId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemPackageLineItemId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemPackageLineItemId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "TicketTableId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemTicketTableId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemTicketTableId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "EventLineId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemEventLineId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemEventLineId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "ReferenceId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemReferenceId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemReferenceId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "MediaType", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemMediaType(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketPINLineItemMediaType_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageId", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemPackageId(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemPackageId_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models", name = "PackageLineGroup", scope = SDCCartRetailTicketPINLineItem.class)
    public JAXBElement<String> createSDCCartRetailTicketPINLineItemPackageLineGroup(String value) {
        return new JAXBElement<String>(_SDCCartRetailTicketLineItemPackageLineGroup_QNAME, String.class, SDCCartRetailTicketPINLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCGetDiscountCounterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetDiscountCounterResult", scope = GetDiscountCounterResponse.class)
    public JAXBElement<SDCGetDiscountCounterResponse> createGetDiscountCounterResponseGetDiscountCounterResult(SDCGetDiscountCounterResponse value) {
        return new JAXBElement<SDCGetDiscountCounterResponse>(_GetDiscountCounterResponseGetDiscountCounterResult_QNAME, SDCGetDiscountCounterResponse.class, GetDiscountCounterResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SDCInsertOnlineReferenceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "insertOnlineReferenceResult", scope = InsertOnlineReferenceResponse.class)
    public JAXBElement<SDCInsertOnlineReferenceResponse> createInsertOnlineReferenceResponseInsertOnlineReferenceResult(SDCInsertOnlineReferenceResponse value) {
        return new JAXBElement<SDCInsertOnlineReferenceResponse>(_InsertOnlineReferenceResponseInsertOnlineReferenceResult_QNAME, SDCInsertOnlineReferenceResponse.class, InsertOnlineReferenceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models", name = "OfferId", scope = SDCDiscountCounterTable.class)
    public JAXBElement<String> createSDCDiscountCounterTableOfferId(String value) {
        return new JAXBElement<String>(_SDCUseDiscountCounterTableOfferId_QNAME, String.class, SDCDiscountCounterTable.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "discountCode", scope = GetDiscountCounter.class)
    public JAXBElement<String> createGetDiscountCounterDiscountCode(String value) {
        return new JAXBElement<String>(_UseDiscountCounterDiscountCode_QNAME, String.class, GetDiscountCounter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "offerId", scope = GetDiscountCounter.class)
    public JAXBElement<String> createGetDiscountCounterOfferId(String value) {
        return new JAXBElement<String>(_UseDiscountCounterOfferId_QNAME, String.class, GetDiscountCounter.class, value);
    }

}
