
package com.enovax.star.cms.commons.ws.axchannel.inventtable;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfEventGroupLineEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEventGroupLineEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventGroupLineEntity" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}EventGroupLineEntity" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEventGroupLineEntity", propOrder = {
    "eventGroupLineEntity"
})
public class ArrayOfEventGroupLineEntity {

    @XmlElement(name = "EventGroupLineEntity", nillable = true)
    protected List<EventGroupLineEntity> eventGroupLineEntity;

    /**
     * Gets the value of the eventGroupLineEntity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventGroupLineEntity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventGroupLineEntity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventGroupLineEntity }
     * 
     * 
     */
    public List<EventGroupLineEntity> getEventGroupLineEntity() {
        if (eventGroupLineEntity == null) {
            eventGroupLineEntity = new ArrayList<EventGroupLineEntity>();
        }
        return this.eventGroupLineEntity;
    }

}
