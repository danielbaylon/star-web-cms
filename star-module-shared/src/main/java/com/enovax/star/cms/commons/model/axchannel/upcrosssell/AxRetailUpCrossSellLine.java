package com.enovax.star.cms.commons.model.axchannel.upcrosssell;

/**
 * Created by jensen on 25/6/16.
 */
public class AxRetailUpCrossSellLine {

    private String inventDimId;
    private String originalItemId;
    private Integer qty;
    private String unitId;
    private String itemId;
    private Long tableRecId;
    private Integer upsellType;
    private String variantId;

    public String getInventDimId() {
        return inventDimId;
    }

    public void setInventDimId(String inventDimId) {
        this.inventDimId = inventDimId;
    }

    public String getOriginalItemId() {
        return originalItemId;
    }

    public void setOriginalItemId(String originalItemId) {
        this.originalItemId = originalItemId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Long getTableRecId() {
        return tableRecId;
    }

    public void setTableRecId(Long tableRecId) {
        this.tableRecId = tableRecId;
    }

    public Integer getUpsellType() {
        return upsellType;
    }

    public void setUpsellType(Integer upsellType) {
        this.upsellType = upsellType;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }
}
