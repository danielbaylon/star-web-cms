package com.enovax.star.cms.commons.model.booking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KioskFunCartRedemptionDisplayItem extends FunCartDisplayItem {

    private String expiryDate;

    private Boolean isCombined = Boolean.FALSE;

    private Boolean isPartial = Boolean.FALSE;

    private Boolean isOpenDated = Boolean.FALSE;

    private Boolean isExpired = Boolean.FALSE;

    private Boolean allowChangeEventDate = Boolean.FALSE;

    private List<String> eventLines = new ArrayList<>();

    private Map<String, List<String>> eventLineDates = new HashMap<>();

    private Integer remainingQty = 0;

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public Boolean getIsCombined() {
        return isCombined;
    }

    public void setIsCombined(Boolean isCombined) {
        this.isCombined = isCombined;
    }

    public Boolean getIsPartial() {
        return isPartial;
    }

    public void setIsPartial(Boolean isPartial) {
        this.isPartial = isPartial;
    }

    public Boolean getIsOpenDated() {
        return isOpenDated;
    }

    public void setIsOpenDated(Boolean isOpenDated) {
        this.isOpenDated = isOpenDated;
    }

    public Boolean getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(Boolean isExpired) {
        this.isExpired = isExpired;
    }

    public Boolean getAllowChangeEventDate() {
        return allowChangeEventDate;
    }

    public void setAllowChangeEventDate(Boolean allowChangeEventDate) {
        this.allowChangeEventDate = allowChangeEventDate;
    }

    public Integer getRemainingQty() {
        return remainingQty;
    }

    public void setRemainingQty(Integer remainingQty) {
        this.remainingQty = remainingQty;
    }

    public Map<String, List<String>> getEventLineDates() {
        return eventLineDates;
    }

    public void setEventLineDates(Map<String, List<String>> eventLineDates) {
        this.eventLineDates = eventLineDates;
    }

    public List<String> getEventLines() {
        return eventLines;
    }

    public void setEventLines(List<String> eventLines) {
        this.eventLines = eventLines;
    }

}
