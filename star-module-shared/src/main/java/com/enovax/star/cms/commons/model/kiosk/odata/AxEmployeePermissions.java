package com.enovax.star.cms.commons.model.kiosk.odata;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.EmployeePermissions
 * 
 * @author Justin
 * @since 6 SEP 16
 */
public class AxEmployeePermissions implements Serializable{

	private Integer allowPriceOverride;

	private BigDecimal maximumDiscountPercentage;

	private BigDecimal maximumLineDiscountAmount;

	private BigDecimal maximumLineReturnAmount;

	private BigDecimal maximumTotalDiscountAmount;

	private BigDecimal maximumTotalDiscountPercentage;

	private BigDecimal maxTotalReturnAmount;

	private String staffId;

	private List<String> roles;

	private Boolean allowUseSharedShift;

	private Boolean allowManageSharedShift;

	public Integer getAllowPriceOverride() {
		return allowPriceOverride;
	}

	public void setAllowPriceOverride(Integer allowPriceOverride) {
		this.allowPriceOverride = allowPriceOverride;
	}

	public BigDecimal getMaximumDiscountPercentage() {
		return maximumDiscountPercentage;
	}

	public void setMaximumDiscountPercentage(BigDecimal maximumDiscountPercentage) {
		this.maximumDiscountPercentage = maximumDiscountPercentage;
	}

	public BigDecimal getMaximumLineDiscountAmount() {
		return maximumLineDiscountAmount;
	}

	public void setMaximumLineDiscountAmount(BigDecimal maximumLineDiscountAmount) {
		this.maximumLineDiscountAmount = maximumLineDiscountAmount;
	}

	public BigDecimal getMaximumLineReturnAmount() {
		return maximumLineReturnAmount;
	}

	public void setMaximumLineReturnAmount(BigDecimal maximumLineReturnAmount) {
		this.maximumLineReturnAmount = maximumLineReturnAmount;
	}

	public BigDecimal getMaximumTotalDiscountAmount() {
		return maximumTotalDiscountAmount;
	}

	public void setMaximumTotalDiscountAmount(BigDecimal maximumTotalDiscountAmount) {
		this.maximumTotalDiscountAmount = maximumTotalDiscountAmount;
	}

	public BigDecimal getMaximumTotalDiscountPercentage() {
		return maximumTotalDiscountPercentage;
	}

	public void setMaximumTotalDiscountPercentage(BigDecimal maximumTotalDiscountPercentage) {
		this.maximumTotalDiscountPercentage = maximumTotalDiscountPercentage;
	}

	public BigDecimal getMaxTotalReturnAmount() {
		return maxTotalReturnAmount;
	}

	public void setMaxTotalReturnAmount(BigDecimal maxTotalReturnAmount) {
		this.maxTotalReturnAmount = maxTotalReturnAmount;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public Boolean getAllowUseSharedShift() {
		return allowUseSharedShift;
	}

	public void setAllowUseSharedShift(Boolean allowUseSharedShift) {
		this.allowUseSharedShift = allowUseSharedShift;
	}

	public Boolean getAllowManageSharedShift() {
		return allowManageSharedShift;
	}

	public void setAllowManageSharedShift(Boolean allowManageSharedShift) {
		this.allowManageSharedShift = allowManageSharedShift;
	}

}
