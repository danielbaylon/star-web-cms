package com.enovax.star.cms.commons.model.axchannel.discountcounter.transformer;

import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.axchannel.discountcounter.AxDiscountCounter;
import com.enovax.star.cms.commons.ws.axchannel.retailticket.*;

import java.util.List;

@SuppressWarnings("Duplicates")
public class AxDiscountCounterTransformer {

    public static ApiResult<AxDiscountCounter> fromWsGetDiscountCounter(SDCGetDiscountCounterResponse wsResponse) {
        if (wsResponse == null) {
            return new ApiResult<>();
        }

        final ArrayOfResponseError arrayOfResponseError = wsResponse.getErrors().getValue();
        if (arrayOfResponseError != null && !arrayOfResponseError.getResponseError().isEmpty()) {
            final List<ResponseError> errors = arrayOfResponseError.getResponseError();
            StringBuilder sbErrCodes = new StringBuilder("");
            StringBuilder sbErrMessages = new StringBuilder("");
            for (ResponseError error : errors) {
                sbErrCodes.append(error.getErrorCode().getValue()).append(" | ");
                sbErrMessages.append(error.getErrorMessage().getValue()).append(" | ");
            }
            return new ApiResult<>(false, sbErrCodes.toString(), sbErrMessages.toString(), null);
        }

        final ArrayOfSDCDiscountCounterTable arrayOfSDCDiscountCounterTable = wsResponse.getDiscountCounterEntity().getValue();
        if (arrayOfSDCDiscountCounterTable == null) {
            return new ApiResult<>(false, "FAILED_NOTHING_RETURNED", "Failed, nothing returned.", null);
        }

        final List<SDCDiscountCounterTable> wsDiscountCounters = arrayOfSDCDiscountCounterTable.getSDCDiscountCounterTable();
        if (wsDiscountCounters.isEmpty()) {
            return new ApiResult<>(false, "UNEXPECTED_RESULT_SIZE", "Unexpected result size.", null);
        }

        final SDCDiscountCounterTable discountCounter = wsDiscountCounters.get(0);

        final AxDiscountCounter dc = new AxDiscountCounter();
        dc.setMaxUse(discountCounter.getMaxUse());
        dc.setQuantity(discountCounter.getQty());
        dc.setOfferId(discountCounter.getOfferId().getValue());

        return new ApiResult<>(true, "", "", dc);
    }


    public static ApiResult<String> fromWsUseDiscountCounter(SDCUseDiscountCounterResponse wsResponse) {
        if (wsResponse == null) {
            return new ApiResult<>();
        }

        final ArrayOfResponseError arrayOfResponseError = wsResponse.getErrors().getValue();
        if (arrayOfResponseError != null && !arrayOfResponseError.getResponseError().isEmpty()) {
            final List<ResponseError> errors = arrayOfResponseError.getResponseError();
            StringBuilder sbErrCodes = new StringBuilder("");
            StringBuilder sbErrMessages = new StringBuilder("");
            for (ResponseError error : errors) {
                sbErrCodes.append(error.getErrorCode().getValue()).append(" | ");
                sbErrMessages.append(error.getErrorMessage().getValue()).append(" | ");
            }
            return new ApiResult<>(false, sbErrCodes.toString(), sbErrMessages.toString(), "");
        }

        final ArrayOfSDCUseDiscountCounterTable arrayOfSDCUseDiscountCounterTable = wsResponse.getUseDiscountCounterEntity().getValue();
        if (arrayOfSDCUseDiscountCounterTable == null) {
            return new ApiResult<>(false, "FAILED_NOTHING_RETURNED", "Failed, nothing returned.", "");
        }

        final List<SDCUseDiscountCounterTable> wsUseDiscountCounters = arrayOfSDCUseDiscountCounterTable.getSDCUseDiscountCounterTable();
        if (wsUseDiscountCounters.isEmpty()) {
            return new ApiResult<>(false, "UNEXPECTED_RESULT_SIZE", "Unexpected result size.", "");
        }

        final SDCUseDiscountCounterTable udc = wsUseDiscountCounters.get(0);
        return new ApiResult<>(true, "", "", udc.getOfferId().getValue());
    }
}
