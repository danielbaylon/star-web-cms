package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * SDC_TicketingExtension.DataModel.CartCheckoutCancelCriteria
 * 
 * @author Justin
 *
 */
public class CartCheckoutCancelCriteria {

    @JsonProperty("DataLevelValue")
    private String dataLevelValue = "4";

    @JsonProperty("IsPINRedemption")
    private String isPINRedemption = "0";

    @JsonProperty("TransactionId")
    private String transactionId = "";

    @JsonProperty("ShiftId")
    private String shiftId;

    @JsonProperty("StaffId")
    private String staffId;

    @JsonProperty("StoreId")
    private String storeId;

    @JsonProperty("TerminalId")
    private String terminalId;

    @JsonProperty("AccountNum")
    private String accountNum;

    @JsonProperty("SourceTable")
    private String sourceTable;

    public String getDataLevelValue() {
        return dataLevelValue;
    }

    public void setDataLevelValue(String dataLevelValue) {
        this.dataLevelValue = dataLevelValue;
    }

    public String getIsPINRedemption() {
        return isPINRedemption;
    }

    public void setIsPINRedemption(String isPINRedemption) {
        this.isPINRedemption = isPINRedemption;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

}
