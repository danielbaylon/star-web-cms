package com.enovax.star.cms.commons.model.kiosk.odata;

/**
 * 
 * @author Justin
 *
 */
public class AxProductProperty {
    
    public static final String TICKET_TYPE = "1TicketType";
    public static final String TICKET_TYPE_CHILD = "Child";
    public static final String TICKET_TYPE_ADULT = "Adult";
    public static final String TICKET_TYPE_STANDARD = "Standard";

    public static final String PUBLISHED_PRICE = "2PublishPrice"; 

    private String valueString;

    private String propertyTypeValue;

    private String keyName;

    private String friendlyName;

    private String recordId;

    private String isDimensionProperty;

    private String attributeValueId;

    private String unitText;

    private String groupId;

    private String groupTypeValue;

    private String groupName;

    private String productId;

    private String catalogId;

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public String getPropertyTypeValue() {
        return propertyTypeValue;
    }

    public void setPropertyTypeValue(String propertyTypeValue) {
        this.propertyTypeValue = propertyTypeValue;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getIsDimensionProperty() {
        return isDimensionProperty;
    }

    public void setIsDimensionProperty(String isDimensionProperty) {
        this.isDimensionProperty = isDimensionProperty;
    }

    public String getAttributeValueId() {
        return attributeValueId;
    }

    public void setAttributeValueId(String attributeValueId) {
        this.attributeValueId = attributeValueId;
    }

    public String getUnitText() {
        return unitText;
    }

    public void setUnitText(String unitText) {
        this.unitText = unitText;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupTypeValue() {
        return groupTypeValue;
    }

    public void setGroupTypeValue(String groupTypeValue) {
        this.groupTypeValue = groupTypeValue;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

}
