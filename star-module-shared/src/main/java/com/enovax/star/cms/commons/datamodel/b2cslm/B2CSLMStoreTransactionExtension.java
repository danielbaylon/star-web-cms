package com.enovax.star.cms.commons.datamodel.b2cslm;

import javax.persistence.*;

@Entity
@Table(name = "B2CSLMStoreTransactionExtension")
public class B2CSLMStoreTransactionExtension {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "transactionId", nullable = false)
    private Integer transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transactionId", referencedColumnName = "id", updatable = false, insertable = false)
    private B2CSLMStoreTransaction storeTransaction;

    @Column(name = "receiptNumber")
    private String receiptNumber;

    @Column(name = "storeTransactionJson")
    private String storeTransactionJson;

    @Column(name = "cartJson")
    private String cartJson;

    @Column(name = "salesOrderJson")
    private String salesOrderJson;

    @Column(name = "ticketsJson")
    private String ticketsJson;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public B2CSLMStoreTransaction getStoreTransaction() {
        return storeTransaction;
    }

    public void setStoreTransaction(B2CSLMStoreTransaction storeTransaction) {
        this.storeTransaction = storeTransaction;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getStoreTransactionJson() {
        return storeTransactionJson;
    }

    public void setStoreTransactionJson(String storeTransactionJson) {
        this.storeTransactionJson = storeTransactionJson;
    }

    public String getCartJson() {
        return cartJson;
    }

    public void setCartJson(String cartJson) {
        this.cartJson = cartJson;
    }

    public String getSalesOrderJson() {
        return salesOrderJson;
    }

    public void setSalesOrderJson(String salesOrderJson) {
        this.salesOrderJson = salesOrderJson;
    }

    public String getTicketsJson() {
        return ticketsJson;
    }

    public void setTicketsJson(String ticketsJson) {
        this.ticketsJson = ticketsJson;
    }
}
