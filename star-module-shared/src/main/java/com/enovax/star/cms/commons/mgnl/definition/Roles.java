package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 4/8/16.
 */
public enum Roles {
    SUPERUSER("superuser"),

    //Product Management
    B2CSLMCMSPRODUCT("b2c-slm-cms-product"),
    B2CMFLGCMSPRODUCT("b2c-mflg-cms-product"),
    KIOSKSLMCMSPRODUCT("kiosk-slm-cms-product"),
    KIOSKMFLGCMSPRODUCT("kiosk-mflg-cms-product"),
    PPSLMCMSPRODUCT("pp-slm-cms-product"),
    PPMFLGCMSPRODUCT("pp-mflg-cms-product"),
    //Category Management
    B2CSLMPRODUCTCATEGORY("b2c-slm-product-category"),
    B2CMFLGPRODUCTCATEGORY("b2c-mflg-product-category"),
    KIOSKSLMPRODUCTCATEGORY("kiosk-slm-product-category"),
    KIOSKMFLGPRODUCTCATEGORY("kiosk-mflg-product-category"),
    PPSLMPRODUCTCATEGORY("pp-slm-product-category"),
    PPMFLGPRODUCTCATEGORY("pp-mflg-product-category"),
    //Publish
    B2CSLMPUBLISHCATEGORY("b2c-slm-publish-category"),
    B2CMFLGPUBLISHCATEGORY("b2c-mflg-publish-category"),
    KIOSKSLMPUBLISHCATEGORY("kiosk-slm-publish-category"),
    KIOSKMFLGPUBLISHCATEGORY("kiosk-mflg-publish-category"),
    PPSLMPUBLISHCATEGORY("pp-slm-publish-category"),
    PPMFLGPUBLISHCATEGORY("pp-mflg-publish-category"),
    //ax sync
    B2CSLMAXSYNC("b2c-slm-ax-sync"),
    B2CMFLGAXSYNC("b2c-mflg-ax-sync"),
    KIOSKSLMAXSYNC("kiosk-slm-ax-sync"),
    KIOSKMFLGAXSYNC("kiosk-mflg-ax-sync"),
    PPSLMAXSYNC("pp-slm-ax-sync"),
    PPMFLGAXSYNC("pp-mflg-ax-sync");

    private String role;

    Roles(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
