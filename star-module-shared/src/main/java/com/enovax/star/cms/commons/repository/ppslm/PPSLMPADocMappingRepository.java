package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPADocMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMPADocMappingRepository extends JpaRepository<PPSLMPADocMapping, Integer> {

    @Query("select a from PPSLMPADocMapping a where a.partnerId = ?1 order by a.paDoc.createdDate desc")
    List<PPSLMPADocMapping> findByPartnerId(Integer id);

    PPSLMPADocMapping findByPartnerIdAndDocId(Integer paId, Integer docId);

    PPSLMPADocMapping findByDocId(Integer docId);
}
