package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 3/22/16.
 */
public enum RemoteChannels {
    B2C_SLM("b2c-slm", "B2C SLM"),
    B2C_MFLG("b2c-mflg", "B2C MFLG"),
    KIOSK_SLM("kiosk-slm", "KIOSK SLM"),
    KIOSK_MFLG("kiosk-mflg", "KIOSK MFLG"),
    PP_SLM("partner-portal-slm", "PARTNER PORTAL"),
    PP_MFLG("partner-portal-mflg", "PARTNER PORTAL MFLG");

    private String channel;
    private String displayName;

    RemoteChannels(String channel, String displayName) {
        this.channel = channel;
        this.displayName = displayName;
    }

    public String getChannel() {
        return this.channel;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static String getChannelDisplayName(String channel) {
        RemoteChannels remoteChannels[] = RemoteChannels.values();
        for(int i = 0; i < remoteChannels.length; i++) {
            if(remoteChannels[i].getChannel().equals(channel)) {
                return remoteChannels[i].getDisplayName();
            }
        }
        return channel;
    }
}
