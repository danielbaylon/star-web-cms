package com.enovax.star.cms.commons.model.booking;

/**
 * Created by cornelius on 28/6/16.
 */
public class CrossSellCartItem {

    private String crossSellProductListingId;
    private String crossSellItemImage;
    private String crossSellItemName;
    private String crossSellItemDesc;
    private String crossSellProductCode;
    private String productPrice;
    private String ticketType;
    private String imageIconPath;
    private String parentListingId;
    private String parentCmsProductId;
    private int quantity;
    private String cmsProductName;

    public String getCrossSellProductListingId() {
        return crossSellProductListingId;
    }

    public void setCrossSellProductListingId(String crossSellProductListingId) {
        this.crossSellProductListingId = crossSellProductListingId;
    }

    public String getCrossSellItemImage() {
        return crossSellItemImage;
    }

    public void setCrossSellItemImage(String crossSellItemImage) {
        this.crossSellItemImage = crossSellItemImage;
    }

    public String getCrossSellItemName() {
        return crossSellItemName;
    }

    public void setCrossSellItemName(String crossSellItemName) {
        this.crossSellItemName = crossSellItemName;
    }

    public String getCrossSellItemDesc() {
        return crossSellItemDesc;
    }

    public void setCrossSellItemDesc(String crossSellItemDesc) {
        this.crossSellItemDesc = crossSellItemDesc;
    }

    public String getCrossSellProductCode() {
        return crossSellProductCode;
    }

    public void setCrossSellProductCode(String crossSellProductCode) {
        this.crossSellProductCode = crossSellProductCode;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getImageIconPath() {
        return imageIconPath;
    }

    public void setImageIconPath(String imageIconPath) {
        this.imageIconPath = imageIconPath;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getParentListingId() {
        return parentListingId;
    }

    public void setParentListingId(String parentListingId) {
        this.parentListingId = parentListingId;
    }

    public String getParentCmsProductId() {
        return parentCmsProductId;
    }

    public void setParentCmsProductId(String parentCmsProductId) {
        this.parentCmsProductId = parentCmsProductId;
    }

    public String getCmsProductName() {
        return cmsProductName;
    }

    public void setCmsProductName(String cmsProductName) {
        this.cmsProductName = cmsProductName;
    }

}