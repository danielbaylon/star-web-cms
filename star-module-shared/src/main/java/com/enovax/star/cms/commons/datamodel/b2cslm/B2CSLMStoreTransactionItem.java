package com.enovax.star.cms.commons.datamodel.b2cslm;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jensen on 3/9/16.
 */

@Entity
@Table(name = B2CSLMStoreTransactionItem.TABLE_NAME)
public class B2CSLMStoreTransactionItem {

    public static final String TABLE_NAME = "B2CSLMStoreTransactionItem";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "transactionId", nullable = false)
    private Integer transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transactionId", referencedColumnName = "id", updatable = false, insertable = false)
    private B2CSLMStoreTransaction transaction;

    @Column(name = "cmsProductId", nullable = false)
    private String cmsProductId;

    @Column(name = "cmsProductName", nullable = false)
    private String cmsProductName;

    @Column(name = "itemListingId", nullable = false)
    private String itemListingId;

    @Column(name = "itemProductCode", nullable = false)
    private String itemProductCode;

    @Column(name = "transItemType", nullable = false)
    private String transItemType;

    @Column(name = "displayName", nullable = false)
    private String displayName;

    @Column(name = "displayDetails")
    private String displayDetails;

    @Column(name = "ticketType", nullable = false)
    private String ticketType;

    @Column(name = "baseUnitPrice", nullable = false)
    private BigDecimal baseUnitPrice;

    @Column(name = "actualUnitPrice", nullable = false)
    private BigDecimal actualUnitPrice;

    @Column(name = "baseQty", nullable = false)
    private Integer baseQty;

    @Column(name = "qty", nullable = false)
    private Integer qty;

    @Column(name = "subTotal", nullable = false)
    private BigDecimal subTotal;

    @Column(name = "cmsProductNameByLang")
    private String cmsProductNameByLang;

    @Column(name = "displayNameByLang")
    private String displayNameByLang;

    @Column(name = "displayDetailsByLang")
    private String displayDetailsByLang;

    @Column(name = "dateOfVisit")
    private Date dateOfVisit;

    @Column(name = "eventGroupId")
    private String eventGroupId;

    @Column(name = "eventLineId")
    private String eventLineId;

    @Column(name = "otherDetails")
    private String otherDetails;

    @Column(name = "mainItemCmsProductId")
    private String mainItemCmsProductId;

    @Column(name = "mainItemListingId")
    private String mainItemListingId;

    @Column(name = "promoCodes")
    private String promoCodes;

    @Column(name = "promoCodeDiscountId")
    private String promoCodeDiscountId;

    @Column(name = "affiliations")
    private String affiliations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public B2CSLMStoreTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(B2CSLMStoreTransaction transaction) {
        this.transaction = transaction;
    }

    public String getCmsProductId() {
        return cmsProductId;
    }

    public void setCmsProductId(String cmsProductId) {
        this.cmsProductId = cmsProductId;
    }

    public String getCmsProductName() {
        return cmsProductName;
    }

    public void setCmsProductName(String cmsProductName) {
        this.cmsProductName = cmsProductName;
    }

    public String getItemListingId() {
        return itemListingId;
    }

    public void setItemListingId(String itemListingId) {
        this.itemListingId = itemListingId;
    }

    public String getItemProductCode() {
        return itemProductCode;
    }

    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

    public String getTransItemType() {
        return transItemType;
    }

    public void setTransItemType(String transItemType) {
        this.transItemType = transItemType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayDetails() {
        return displayDetails;
    }

    public void setDisplayDetails(String displayDetails) {
        this.displayDetails = displayDetails;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public BigDecimal getBaseUnitPrice() {
        return baseUnitPrice;
    }

    public void setBaseUnitPrice(BigDecimal baseUnitPrice) {
        this.baseUnitPrice = baseUnitPrice;
    }

    public BigDecimal getActualUnitPrice() {
        return actualUnitPrice;
    }

    public void setActualUnitPrice(BigDecimal actualUnitPrice) {
        this.actualUnitPrice = actualUnitPrice;
    }

    public Integer getBaseQty() {
        return baseQty;
    }

    public void setBaseQty(Integer baseQty) {
        this.baseQty = baseQty;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public String getCmsProductNameByLang() {
        return cmsProductNameByLang;
    }

    public void setCmsProductNameByLang(String cmsProductNameByLang) {
        this.cmsProductNameByLang = cmsProductNameByLang;
    }

    public String getDisplayNameByLang() {
        return displayNameByLang;
    }

    public void setDisplayNameByLang(String displayNameByLang) {
        this.displayNameByLang = displayNameByLang;
    }

    public String getDisplayDetailsByLang() {
        return displayDetailsByLang;
    }

    public void setDisplayDetailsByLang(String displayDetailsByLang) {
        this.displayDetailsByLang = displayDetailsByLang;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(String eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public String getEventLineId() {
        return eventLineId;
    }

    public void setEventLineId(String eventLineId) {
        this.eventLineId = eventLineId;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getMainItemCmsProductId() {
        return mainItemCmsProductId;
    }

    public void setMainItemCmsProductId(String mainItemCmsProductId) {
        this.mainItemCmsProductId = mainItemCmsProductId;
    }

    public String getMainItemListingId() {
        return mainItemListingId;
    }

    public void setMainItemListingId(String mainItemListingId) {
        this.mainItemListingId = mainItemListingId;
    }

    public String getPromoCodes() {
        return promoCodes;
    }

    public void setPromoCodes(String promoCodes) {
        this.promoCodes = promoCodes;
    }

    public String getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(String affiliations) {
        this.affiliations = affiliations;
    }

    public String getPromoCodeDiscountId() {
        return promoCodeDiscountId;
    }

    public void setPromoCodeDiscountId(String promoCodeDiscountId) {
        this.promoCodeDiscountId = promoCodeDiscountId;
    }
}
