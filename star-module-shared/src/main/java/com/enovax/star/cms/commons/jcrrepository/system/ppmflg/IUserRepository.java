package com.enovax.star.cms.commons.jcrrepository.system.ppmflg;

import com.enovax.star.cms.commons.model.partner.ppmflg.AdminAccountVM;
import info.magnolia.cms.security.User;

import java.util.List;

/**
 * Created by lavanya on 20/9/16.
 */
public interface IUserRepository {
    List<AdminAccountVM> getAllPPMFLGUsers();
    public List<String> getAdminUserAssignedRights(String adminId);
    public User getUserById(String adminId);
    public List<String> getCountryList(String adminId);
    public List<String> getUserbyRole(String role);
    public List<AdminAccountVM> getAdminAccountVMbyRole(String role);
    public List<AdminAccountVM> getAdminAccountVMByCountry(String countryId);

    String getUserEmailById(String accMgrId);
}
