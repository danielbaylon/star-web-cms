package com.enovax.star.cms.commons.model.kiosk.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Microsoft.Dynamics.Commerce.Runtime.DataModel.CartRetailTicketPINTableEntity
 * 
 * @author Justin
 *
 */
public class AxCartRetailTicketPINTableEntity {

    @JsonProperty("PRODUCTPINCODE")
    private String productPinCode;

    private String qtyGroup;

    private String referenceId;

    private String dataAreaId;

    private String startDateTime;

    private String endDateTime;

    private String custAccount;

    private String productPinCodePackage;

    private String channelId;

    private String productName;

    private String itemId;

    private String paxId;

    public String getProductPinCode() {
        return productPinCode;
    }

    public void setProductPinCode(String productPinCode) {
        this.productPinCode = productPinCode;
    }

    public String getQtyGroup() {
        return qtyGroup;
    }

    public void setQtyGroup(String qtyGroup) {
        this.qtyGroup = qtyGroup;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getDataAreaId() {
        return dataAreaId;
    }

    public void setDataAreaId(String dataAreaId) {
        this.dataAreaId = dataAreaId;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getCustAccount() {
        return custAccount;
    }

    public void setCustAccount(String custAccount) {
        this.custAccount = custAccount;
    }

    public String getProductPinCodePackage() {
        return productPinCodePackage;
    }

    public void setProductPinCodePackage(String productPinCodePackage) {
        this.productPinCodePackage = productPinCodePackage;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getPaxId() {
        return paxId;
    }

    public void setPaxId(String paxId) {
        this.paxId = paxId;
    }

    // @JsonProperty("ExtensionProperties")
    // private List<AxCommerceProperty> commercePropertyList;
    
    

}
