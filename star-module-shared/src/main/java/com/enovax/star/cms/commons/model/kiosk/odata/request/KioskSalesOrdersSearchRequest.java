package com.enovax.star.cms.commons.model.kiosk.odata.request;

import com.enovax.star.cms.commons.model.kiosk.odata.AxSalesOrdersSearchCriteria;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author dbaylon
 *
 */
public class KioskSalesOrdersSearchRequest {

	@JsonProperty("salesOrderSearchCriteria")
	private AxSalesOrdersSearchCriteria axSalesOrdersSearchCriteria;

	public void setAxSalesOrdersSearchCriteria(AxSalesOrdersSearchCriteria axSalesOrdersSearchCriteria) {
		this.axSalesOrdersSearchCriteria = axSalesOrdersSearchCriteria;
	}

	public AxSalesOrdersSearchCriteria getAxSalesOrdersSearchCriteria() {
		return axSalesOrdersSearchCriteria;
	}

}
