package com.enovax.star.cms.commons.model.search;

import java.io.Serializable;

/**
 * Created by jonathan on 28/12/16.
 */
public class QuickSearchRequest implements Serializable {

    private String qry;

    public String getQry() {
        return qry;
    }

    public void setQry(String qry) {
        this.qry = qry;
    }

}
