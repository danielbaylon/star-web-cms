package com.enovax.star.cms.commons.util.partner.ppslm;

import com.enovax.star.cms.commons.constant.ppslm.TicketStatus;
import com.enovax.star.cms.commons.constant.ppslm.TransItemType;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransaction;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMInventoryTransactionItem;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMRevalidationTransaction;
import com.enovax.star.cms.commons.model.partner.ppslm.InventoryTransactionVM;
import com.enovax.star.cms.commons.model.partner.ppslm.RevalDetail;
import com.enovax.star.cms.commons.model.partner.ppslm.RevalItem;
import com.enovax.star.cms.commons.util.NvxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class TransactionUtil {
    private static Logger log = LoggerFactory.getLogger(TransactionUtil.class);
    public final static String DEFAULT_CURRENCY = "S$";
//    private final static double DEFAULT_GST_RATE = (7.0 / 107);

    private static final String[] tensNames = { "", " ten", " twenty",
            " thirty", " forty", " fifty", " sixty", " seventy", " eighty",
            " ninety" };

    private static final String[] numNames = { "", " one", " two", " three",
            " four", " five", " six", " seven", " eight", " nine", " ten",
            " eleven", " twelve", " thirteen", " fourteen", " fifteen",
            " sixteen", " seventeen", " eighteen", " nineteen" };

    public static Date getTransactionExpiringAlertDate(Integer weeks) {
        Calendar ca = getCalendarWithoutTime();
        ca.add(Calendar.DAY_OF_YEAR, weeks * 7);
        return ca.getTime();

    }

    public static Date getDateWithoutTime() {
        Calendar ca = getCalendarWithoutTime();
        return ca.getTime();
    }

    public static Date getValidateStartDate(Date transDate) {
        if (transDate == null) {
            return null;
        }
        Calendar ca = Calendar.getInstance();
        ca.setTime(transDate);
        //ca.add(Calendar.DATE, -1);
        return ca.getTime();
    }

    public static Date getNewValidateEndDate(Date valiEndDate, int extMonth) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(valiEndDate);
        ca.add(Calendar.MONTH, extMonth);
        return ca.getTime();
    }

    public static boolean hasExpiredRevalidateDate(Date validateEndDate,
            Integer month) {
        Calendar cca = getCalendarWithoutTime();

        Calendar eca = getCalendarWithoutTime(validateEndDate);
        eca.add(Calendar.MONTH, month);
        boolean flag = false;
        if (eca.before(cca)) {
            flag = true;
        }
        return flag;
    }

    private static Calendar getCalendarWithoutTime() {
        return getCalendarWithoutTime(null);
    }

    private static Calendar getCalendarWithoutTime(Date idate) {
        Calendar ca = Calendar.getInstance();
        if (idate == null) {
            ca.setTime(new Date());
        } else {
            ca.setTime(idate);
        }
        ca.set(Calendar.HOUR_OF_DAY, 0);
        ca.set(Calendar.MINUTE, 0);
        ca.set(Calendar.SECOND, 0);
        ca.set(Calendar.MILLISECOND, 0);
        return ca;
    }

    public static String priceWithDecimal(BigDecimal price, String currency) {
        if(price == null){
            return null;
        }
        DecimalFormat formatter = new DecimalFormat("###,###,##0.00");
        String priceStr = formatter.format(price);
        if (currency != null) {
            return currency + " " + priceStr;
        }
        return DEFAULT_CURRENCY + " " + priceStr;
    }

    public static String gstWithDecimal(BigDecimal price, BigDecimal gstRate, String currency) {
        BigDecimal gst = getGSTCost(price, gstRate);
        return priceWithDecimal(gst, currency);
    }

    public static double getGSTRateForWhole(BigDecimal gstRate){
        BigDecimal gstPart = null;
        if(gstRate == null){
//            gstPart = new BigDecimal(0);
            return 0.00;
        }else{
            gstPart = gstRate.multiply(new BigDecimal(100));
        }
        BigDecimal priceWhole = gstPart.add(new BigDecimal(100));
        double calcRate = gstPart.doubleValue()/priceWhole.doubleValue();
        log.debug("calcRate"+calcRate);
        return calcRate;
    }
    
    public static String getDisplayGSTRate(BigDecimal gstRate) {
        if (gstRate != null) {
            return gstRate.multiply(new BigDecimal(100)).intValue() + "%";
        } else {
            return "NA";
        }
    }

    public static BigDecimal getGSTCost(BigDecimal price, BigDecimal gstRate) {
        if (price == null || gstRate == null) {
            return null;
        }
        double calcRate = getGSTRateForWhole(gstRate);
        BigDecimal finalgst = price.multiply(new BigDecimal(calcRate))
                .setScale(2, RoundingMode.HALF_UP);
        log.debug("calcRate" + finalgst);
        return finalgst;
    }

    public static String getExclGSTCostWithDecimal(BigDecimal price, BigDecimal gstRate,
                                                   String currency) {
        BigDecimal gst = getExclGSTCost(price, gstRate);
        return priceWithDecimal(gst, currency);
    }

    public static BigDecimal getExclGSTCost(BigDecimal price, BigDecimal gstRate) {
        BigDecimal gst = getGSTCost(price, gstRate);
        return price.subtract(gst);
    }

    private static String convertLessThanOneThousand(int number) {
        String soFar;

        if (number % 100 < 20) {
            soFar = numNames[number % 100];
            number /= 100;
        } else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        if (number == 0)
            return soFar;
        return numNames[number] + " hundred" + soFar;
    }

    public static String convertToWords(long number) {
        // 0 to 999 999 999 999
        if (number == 0) {
            return "zero";
        }

        String snumber = Long.toString(number);

        // pad with "0"
        String mask = "000000000000";
        DecimalFormat df = new DecimalFormat(mask);
        snumber = df.format(number);

        // XXXnnnnnnnnn
        int billions = Integer.parseInt(snumber.substring(0, 3));
        // nnnXXXnnnnnn
        int millions = Integer.parseInt(snumber.substring(3, 6));
        // nnnnnnXXXnnn
        int hundredThousands = Integer.parseInt(snumber.substring(6, 9));
        // nnnnnnnnnXXX
        int thousands = Integer.parseInt(snumber.substring(9, 12));

        String tradBillions;
        switch (billions) {
        case 0:
            tradBillions = "";
            break;
        case 1:
            tradBillions = convertLessThanOneThousand(billions) + " billion ";
            break;
        default:
            tradBillions = convertLessThanOneThousand(billions) + " billion ";
        }
        String result = tradBillions;

        String tradMillions;
        switch (millions) {
        case 0:
            tradMillions = "";
            break;
        case 1:
            tradMillions = convertLessThanOneThousand(millions) + " million ";
            break;
        default:
            tradMillions = convertLessThanOneThousand(millions) + " million ";
        }
        result = result + tradMillions;

        String tradHundredThousands;
        switch (hundredThousands) {
        case 0:
            tradHundredThousands = "";
            break;
        case 1:
            tradHundredThousands = "one thousand ";
            break;
        default:
            tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                    + " thousand ";
        }
        result = result + tradHundredThousands;

        String tradThousand;
        tradThousand = convertLessThanOneThousand(thousands);
        result = result + tradThousand;

        // remove extra spaces!
        return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
    }

    public static String convertToWordsWithDecimal(BigDecimal decaml) {
        String mainPartStr = convertToWords(decaml.longValue());
        BigDecimal fracPart = decaml.remainder(BigDecimal.ONE);
        BigDecimal cents = fracPart.multiply(new BigDecimal(100));
        if (cents.compareTo(new BigDecimal(1)) >= 0) {
            String fracPartStr = convertToWords(cents.longValue());
            return mainPartStr + " DOLLARS AND " + fracPartStr + " CENTS";
        } else {
            return mainPartStr.toUpperCase() + " DOLLARS ONLY";
        }
    }
    
    public static Date addDaysToDate(Date transDate, int adddays) {
        return addToDate(transDate, Calendar.DATE, adddays);
    }

    public static Date addToDate(Date idate, int type, int unit) {
        if (idate == null) {
            return null;
        }
        Calendar ca = Calendar.getInstance();
        ca.setTime(idate);
        ca.add(type, unit);
        return ca.getTime();
    }

    public static String[] getAllDisplayStatus() {
        return new String[] { TicketStatus.Available.toString(),
                TicketStatus.Expiring.toString(),
                TicketStatus.Revalidated.toString(),
                TicketStatus.Expired.toString(),
                TicketStatus.Refunded.toString(),
                TicketStatus.Forfeited.toString()};
    }

    public static boolean isItemAvailable(PPSLMInventoryTransactionItem transItem) {
        String[] status = getAvailableStatus();
        boolean astatus = Arrays.asList(status).contains(
                transItem.getInventoryTrans().getStatus());
        if (astatus && transItem.getUnpackagedQty() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean withInRevalPeriod(PPSLMInventoryTransaction trans) {
        boolean result = false;
//        Calendar revalEndDay = getCalendarWithoutTime(trans
//                .getValidityEndDate());
        Calendar revalStartDay = getCalendarWithoutTime(trans
                .getValidityEndDate());
//        revalStartDay
//                .add(Calendar.DAY_OF_YEAR, - 7 * trans.getAllowRevalPeriod());
        revalStartDay.add(Calendar.DAY_OF_YEAR, -trans.getAllowRevalPeriod());
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        if (now.after(revalStartDay)) {
            return true;
        }
        return result;

    }

    public static List<RevalItem> getRevalDetailFromXml(String revalDetailStr) {
        RevalDetail revalDetail = NvxUtil.getObjFromXml(revalDetailStr,
                RevalDetail.class);
        if (revalDetail != null) {
            return revalDetail.getRevalItems();
        }
        return null;
    }

    public static List<PPSLMInventoryTransactionItem> getRevalItemsFromXml(String revalDetail) {
        List<RevalItem> revalItems = getRevalDetailFromXml(revalDetail);
        List<PPSLMInventoryTransactionItem> inItems = new ArrayList<PPSLMInventoryTransactionItem>();
        for(RevalItem temp:revalItems){
            inItems.add(new PPSLMInventoryTransactionItem(temp));
        }
        return inItems;
    }

    public static final int EXPIRING_ALERT_PERIOD = 27;

    public static long getExpiringInWeeks(Date endDate){
        LocalDate today = LocalDate.now();
        LocalDate expiringDate = LocalDateTime.ofInstant(endDate.toInstant(), ZoneId.systemDefault()).toLocalDate();

        if(today.isBefore(expiringDate)){
            long weeks = ChronoUnit.WEEKS.between(today, expiringDate);
            return (weeks+1);
        }
        return -1;
    }


    public static long getExpiringInWeeks(PPSLMInventoryTransaction trans){
        return getExpiringInWeeks(trans.getValidityEndDate());
    }

    public static long getExpiringInDays(Date endDate){
        LocalDate today = LocalDate.now();
        LocalDate expiringDate = LocalDateTime.ofInstant(endDate.toInstant(), ZoneId.systemDefault()).toLocalDate();
        if(today.isBefore(expiringDate)){
            long days =   ChronoUnit.DAYS.between(today, expiringDate);
            return days;
        }
        return -1;
    }

    public static long getExpiringInDays(InventoryTransactionVM trans){
        return getExpiringInDays(trans.getValidityEndDate());
    }

    public static String getExpiringStatus(PPSLMInventoryTransaction trans){
        return getExpiringStatus(trans.getValidityEndDate());
    }

    public static String getExpiringStatus(Date endDate){
        long days = getExpiringInDays(endDate);
        String displayStatus = null;
        if(days == 0){
            displayStatus = "Expiring by today";
        }else if (days == 1){
            displayStatus = "Expiring in 1 day";
        }else if ( days > 1 ){
            displayStatus = "Expiring in "+days + " days";
        } else if (days < 0) {
            displayStatus = TicketStatus.Expired.toString(); //TODO this one should be handled by batch job
        }
        return displayStatus;
    }

    public static String[] getAvailableStatus() {
        return new String[] { TicketStatus.Available.toString(),
                TicketStatus.Expiring.toString(),
                TicketStatus.Revalidated.toString() };
    }

    public static BigDecimal getTransRevalidateFee(PPSLMInventoryTransaction trans,
                                                   BigDecimal normalRevalidatFee) {
        return getTransRevalidateFee(trans, normalRevalidatFee, BigDecimal.ZERO);
    }

    public static BigDecimal getTransRevalidateFee(PPSLMInventoryTransaction trans,
                                                   BigDecimal normalRevalidatFee, BigDecimal topupRevalidatFee) {
        BigDecimal totalRevalidatFee = new BigDecimal(0);
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            BigDecimal itemRevalidatFee = getTransItemRevalidateFee(item,
                    normalRevalidatFee, topupRevalidatFee);
            totalRevalidatFee = totalRevalidatFee.add(itemRevalidatFee);
        }
        return totalRevalidatFee;
    }
    public static BigDecimal getTransRevalidateFee(PPSLMRevalidationTransaction revalTrans) {
        BigDecimal normailRevalFee = new BigDecimal(
                revalTrans.getRevalFeeInCents() / 100);
        BigDecimal topupRevalFee = new BigDecimal(
                revalTrans.getRevalTopupFeeInCents() / 100);
        BigDecimal totalRevalidatFee = new BigDecimal(0);
        for (PPSLMInventoryTransactionItem item : revalTrans.getRevalItems()) {
            BigDecimal itemRevalidatFee = getTransItemRevalidateFee(item,
                    normailRevalFee, topupRevalFee);
            totalRevalidatFee = totalRevalidatFee.add(itemRevalidatFee);
        }
        return totalRevalidatFee;
    }

    public static BigDecimal getTransItemRevalidateFee(
            PPSLMInventoryTransactionItem item, BigDecimal normalRevalidatFee,
            BigDecimal topupRevalidatFee) {
        if (item.getUnpackagedQty() == null || item.getUnpackagedQty() <= 0) {
            return BigDecimal.ZERO;
        }
        if (TransItemType.Topup.toString().equals(item.getTransItemType())) {
            if (BigDecimal.ZERO.equals(topupRevalidatFee)) {
                return BigDecimal.ZERO;
            }
            return topupRevalidatFee.multiply(new BigDecimal(item
                    .getUnpackagedQty()));
        } else {
            if (BigDecimal.ZERO.equals(normalRevalidatFee)) {
                return BigDecimal.ZERO;
            }
            return normalRevalidatFee.multiply(new BigDecimal(item
                    .getUnpackagedQty()));
        }
    }

    public static Integer getTransRevalidateQty(PPSLMInventoryTransaction trans,
                                                boolean isTopup) {
        int mainQty = 0;
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            if (TransItemType.Topup.toString().equals(item.getTransItemType())
                    && isTopup) {
                mainQty = mainQty + item.getUnpackagedQty();
            } else if (TransItemType.Standard.toString().equals(
                    item.getTransItemType())
                    && !isTopup) {
                mainQty = mainQty + item.getUnpackagedQty();
            }
        }
        return mainQty;
    }

    public static String getRevalDetailInXml(PPSLMInventoryTransaction trans) {
        List<RevalItem> revalItems = new ArrayList<RevalItem>();
        for (PPSLMInventoryTransactionItem item : trans.getInventoryItems()) {
            RevalItem temItem = new RevalItem(item);
            revalItems.add(temItem);
        }
        RevalDetail revalDetail = new RevalDetail();
        revalDetail.setRevalItems(revalItems);
        return NvxUtil.getXmlfromObj(revalDetail);
    }

    public static String getAdminStatus(String status){
        if (TicketStatus.Reserved.toString().equals(status)
                || TicketStatus.Incomplete.toString().equals(status)) {
            return TicketStatus.Incomplete.toString();
        }
        if (TicketStatus.Failed.toString().equals(status)) {
            return TicketStatus.Failed.toString();
        }
        if (TicketStatus.Available.toString().equals(status)
                || TicketStatus.Revalidated.toString().equals(status)
                || TicketStatus.Refunded.toString().equals(status)
                || TicketStatus.Expired.toString().equals(status)) {
            return "Successful";
        }
        return status;
    }

}
