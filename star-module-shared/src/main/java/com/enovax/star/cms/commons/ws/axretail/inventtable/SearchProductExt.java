
package com.enovax.star.cms.commons.ws.axretail.inventtable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SDC_InventTableExtCriteria" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront.Models}SDC_InventTableExtCriteria" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sdcInventTableExtCriteria"
})
@XmlRootElement(name = "SearchProductExt", namespace = "http://tempuri.org/")
public class SearchProductExt {

    @XmlElementRef(name = "SDC_InventTableExtCriteria", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCInventTableExtCriteria> sdcInventTableExtCriteria;

    /**
     * Gets the value of the sdcInventTableExtCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCInventTableExtCriteria }{@code >}
     *     
     */
    public JAXBElement<SDCInventTableExtCriteria> getSDCInventTableExtCriteria() {
        return sdcInventTableExtCriteria;
    }

    /**
     * Sets the value of the sdcInventTableExtCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCInventTableExtCriteria }{@code >}
     *     
     */
    public void setSDCInventTableExtCriteria(JAXBElement<SDCInventTableExtCriteria> value) {
        this.sdcInventTableExtCriteria = value;
    }

}
