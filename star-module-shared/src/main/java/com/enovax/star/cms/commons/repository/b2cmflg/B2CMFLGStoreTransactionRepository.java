package com.enovax.star.cms.commons.repository.b2cmflg;

import com.enovax.star.cms.commons.datamodel.b2cmflg.B2CMFLGStoreTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jennylynsze on 11/12/16.
 */
@Repository
public interface B2CMFLGStoreTransactionRepository extends JpaRepository<B2CMFLGStoreTransaction, Integer> {
    
    B2CMFLGStoreTransaction findByReceiptNum(String receiptNum);


    @Query(value = "select * from B2CMFLGStoreTransaction " +
            "where stage = 'Reserved' " +
            "and (tmStatus is null or tmStatus = '') " +
            "and createdDate <= DATEADD(mi, -:intervalFrom, dbo.GetLocalDate(DEFAULT))", nativeQuery = true)
    List<B2CMFLGStoreTransaction> retrieveReservedUnpaidTimedOutTransactions(@Param("intervalFrom") Integer intervalFrom);

    @Query(value = "select * from B2CMFLGStoreTransaction " +
            "where stage = 'Reserved' " +
            "and tmStatus is not null " +
            "and modifiedDate <= DATEADD(mi, -:intervalFrom, dbo.GetLocalDate(DEFAULT)) " +
            "and modifiedDate > DATEADD(mi, -:intervalTo, dbo.GetLocalDate(DEFAULT))", nativeQuery = true)
    List<B2CMFLGStoreTransaction> retrieveTransactionsWithPendingPayment(@Param("intervalFrom") Integer intervalFrom, @Param("intervalTo") Integer intervalTo);

    @Query(value = "select * from B2CMFLGStoreTransaction " +
            "where stage = 'Reserved' " +
            "and tmStatus is not null " +
            "and modifiedDate <= DATEADD(mi, -:intervalFrom, dbo.GetLocalDate(DEFAULT))", nativeQuery = true)
    List<B2CMFLGStoreTransaction> retrieveTransactionsWithPendingPayment(@Param("intervalFrom") Integer intervalFrom);
}