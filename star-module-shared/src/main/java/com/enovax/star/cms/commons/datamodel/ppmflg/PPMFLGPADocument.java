package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name="PPMFLGPADocument")
public class PPMFLGPADocument implements Serializable{

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

	@Column(name = "filePath", nullable = false)
	private String filePath;

	@Column(name = "fileName", nullable = false)
	private String fileName;
	
	@Column(name = "fileType")
	private String fileType;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "createdBy", nullable = false)
	private String createdBy;
	
	@Column(name = "createdDate", nullable = false)
	private Date createdDate;
	
	@Column(name = "modifiedBy", nullable = false)
	private String modifiedBy;
	
	@Column(name = "modifiedDate", nullable = false)
	private Date modifiedDate;

	@Column(name = "remoteFileDir")
	private String remoteFileDir;

	@Column(name = "remoteFileName")
	private String remoteFileName;

	@Column(name = "axFileViewPath")
	private String axFileViewPath;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getRemoteFileDir() {
		return remoteFileDir;
	}

	public void setRemoteFileDir(String remoteFileDir) {
		this.remoteFileDir = remoteFileDir;
	}

	public String getRemoteFileName() {
		return remoteFileName;
	}

	public void setRemoteFileName(String remoteFileName) {
		this.remoteFileName = remoteFileName;
	}

	public String getAxFileViewPath() {
		return axFileViewPath;
	}

	public void setAxFileViewPath(String axFileViewPath) {
		this.axFileViewPath = axFileViewPath;
	}
}
