
package com.enovax.star.cms.commons.ws.axchannel.pin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="B2BPINUpdateResult" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses}SDC_B2BPINUpdateResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "b2BPINUpdateResult"
})
@XmlRootElement(name = "B2BPINUpdateResponse", namespace = "http://tempuri.org/")
public class B2BPINUpdateResponse {

    @XmlElementRef(name = "B2BPINUpdateResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCB2BPINUpdateResponse> b2BPINUpdateResult;

    /**
     * Gets the value of the b2BPINUpdateResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateResponse }{@code >}
     *     
     */
    public JAXBElement<SDCB2BPINUpdateResponse> getB2BPINUpdateResult() {
        return b2BPINUpdateResult;
    }

    /**
     * Sets the value of the b2BPINUpdateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCB2BPINUpdateResponse }{@code >}
     *     
     */
    public void setB2BPINUpdateResult(JAXBElement<SDCB2BPINUpdateResponse> value) {
        this.b2BPINUpdateResult = value;
    }

}
