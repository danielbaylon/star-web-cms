package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMAxSalesOrderLineNumberQuantity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by jennylynsze on 8/24/16.
 */
public interface PPSLMAxSalesOrderLineNumberQuantityRepository extends JpaRepository<PPSLMAxSalesOrderLineNumberQuantity, Integer> {
    List<PPSLMAxSalesOrderLineNumberQuantity> findBySalesOrderId(Integer salesOrderId);
    List<PPSLMAxSalesOrderLineNumberQuantity> findBySalesOrderIdAndLineNumber(Integer salesOrderId, Integer lineNumber);
}
