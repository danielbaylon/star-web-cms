package com.enovax.star.cms.commons.model.kiosk.odata.request;

import java.util.ArrayList;
import java.util.List;

import com.enovax.star.cms.commons.model.kiosk.odata.AxCartLine;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KioskUpdateCartLineRequest {

    @JsonIgnore
    private String cartId;

    @JsonProperty("cartLines")
    private List<AxCartLine> axCartLineList = new ArrayList<>();

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public List<AxCartLine> getAxCartLineList() {
        return axCartLineList;
    }

    public void setAxCartLineList(List<AxCartLine> axCartLineList) {
        this.axCartLineList = axCartLineList;
    }

}
