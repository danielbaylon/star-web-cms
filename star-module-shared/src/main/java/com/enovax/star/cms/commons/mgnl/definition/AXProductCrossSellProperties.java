package com.enovax.star.cms.commons.mgnl.definition;

/**
 * Created by jennylynsze on 7/21/16.
 */
public enum AXProductCrossSellProperties {
    RecordId("crossSellRecordId"),
    ProductCode("crossSellProductCode"),
    ListingId("crossSellProductListingId"),
    ProductName("crossSellProductName"),
    ItemName("crossSellItemName"),
    Display("crossSellDisplay"),
    UnitId("crossSellUnitId"),
    Qty("crossSellQty"),
    SystemStatus("systemStatus");

    private String propertyName;

    AXProductCrossSellProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }
}
