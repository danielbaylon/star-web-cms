package com.enovax.star.cms.commons.model.axstar;

import java.util.List;

public class AxStarRetailDiscountDiscountCode {

    //TODO

    private Long recordId;
    private String barcode;
    private String code;
    private Integer concurrencyMode;
    private String description;
    private String disclaimer;
    private boolean isDiscountCodeRequired;
    private String name;
    private String offerId;
    private boolean isEnabled;
    private String validFrom;
    private String validTo;


    private List<AxStarExtensionProperty> extensionProperties;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getConcurrencyMode() {
        return concurrencyMode;
    }

    public void setConcurrencyMode(Integer concurrencyMode) {
        this.concurrencyMode = concurrencyMode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public boolean isDiscountCodeRequired() {
        return isDiscountCodeRequired;
    }

    public void setDiscountCodeRequired(boolean discountCodeRequired) {
        isDiscountCodeRequired = discountCodeRequired;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public List<AxStarExtensionProperty> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<AxStarExtensionProperty> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }
}
