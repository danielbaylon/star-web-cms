package com.enovax.star.cms.commons.model.partner.ppmflg;

import java.util.List;

public class AdminAccountVM {
    private String id;
    private String username;
    private String designation;
    private String email;
    private String status;
    private List<String> roleIds;
    private List<String> countryIds;
    private List<String> groupIds;

    public AdminAccountVM(){
    }

    //dminAccountVM(1001, "Admin 1", "admin1@enovax.com", GeneralStatus.Active.name(), admRoles, ctys);
    public AdminAccountVM(String id, String username, String designation, String email, List<String> roleIds, List<String> groupIds, List<String> countryIds) {
        this.id = id;
        this.username = username;
        this.designation = designation;
        this.email = email;
        this.roleIds = roleIds;
        this.groupIds = groupIds;
        this.countryIds = countryIds;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getDesignation() {
        return designation;
    }
    public void setDesignation(String designation) {
        this.designation = designation;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<String> getRoleIds() {
        return roleIds;
    }
    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }
    public List<String> getCountryIds() {
        return countryIds;
    }
    public void setCountryIds(List<String> countryIds) {
        this.countryIds = countryIds;
    }

    public List<String> getGroups() {
        return groupIds;
    }

    public void setGroups(List<String> groupIds) {
        this.groupIds = groupIds;
    }
}
