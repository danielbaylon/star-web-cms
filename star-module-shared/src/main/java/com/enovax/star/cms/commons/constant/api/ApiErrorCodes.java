package com.enovax.star.cms.commons.constant.api;

public enum ApiErrorCodes {

    //General
    AuthorInstanceNotAllowedHere("AuthorInstanceNotAllowedHere", "This function is not allowed to be done in CMS Author instance."),
    UnexpectedSystemException("UnexpectedSystemException", "Unexpected system error encountered. "),
    General("GeneralError", "A general system error occurred."),

    //AX Star API
    AxStarErrorCartValidateQuantity("AxStarErrorCartValidateQuantity", "Error encountered validating cart quantity (AX)."),
    AxStarErrorCartValidateAndReserveQuantity("AxStarErrorCartValidateAndReserveQuantity", "Error encountered validating and reserving cart quantity (AX)."),
    AxStarErrorCartAddItem("AxStarErrorCartAddItem", "Error encountered adding to cart (AX)."),
    AxStarErrorCartAddItemTopup("AxStarErrorCartAddItemTopup", "Error encountered adding to cart (AX)."),
    AxStarErrorCartRemoveItem("AxStarErrorCartRemoveItem", "Error encountered removing item from cart (AX)."),
    AxStarErrorCartAddPromoCode("AxStarErrorCartAddPromoCode", "Error encountered adding promo code to cart (AX)."),
    AxStarErrorCartCheckout("AxStarErrorCartCheckout", "Error encountered on cart checkout (AX)."),
    AxStarErrorSalesOrderCreate("AxStarErrorSalesOrderCreate", "Error encountered on creation of sales order (AX)."),
    AxStarErrorConfirmSalesOrder("AxStarErrorConfirmSalesOrder", "Error encountered on confirm of sales order (AX)."),

    //Cart
    CartErrorPromoCodeOfferId("CartErrorPromoCodeOfferId", "Unable to validate your promo code. Please try again."),
    CartErrorPromoCodeUsage("CartErrorPromoCodeUsage", "Capacity for selected promo code has been fully utilised."),
    NoCartFound("NoCartFound", "Your shopping cart is empty. Please try again or refresh the page."),
    CartTopupOverLimit("CartTopupOverLimit", "Your number of top ups cannot exceed the number of main items you added."),
    CartErrorApplyAffiliation("CartErrorApplyAffiliation", "Error encountered applying affiliation to your cart."),
    CartErrorApplyAffiliationInput("CartErrorApplyAffiliationInput", "Error encountered applying affiliation to the cart due to user input."),
    CartErrorRebuildCart("CartErrorRebuildCart", "Error encountered while trying to clean up cart. Cart has been cleared. Please try again."),

    //Sessions & Transactions
    NoTransactionFoundInSession("NoTransactionFoundInSession", "No transaction found in session."),
    NoTransactionFound("NoTransactionFound", "No transaction found."),
    TransactionNotReserved("TransactionNotReserved", "Invalid transaction status. Transaction status is not reserved."),
    TransactionSuccess("TransactionSuccessful", "Invalid transaction status. Transaction status is already successful."),
    TransactionAlreadyProceededWithPayment("TransactionAlreadyProceededWithPayment", "Transaction has already proceeded with payment."),
    PaymentFailed("PaymentFailed", "Error encountered processing your payment. Please try again."),

    //Partner Account
    LoginAuthenticationFailed("LoginAuthenticationFailed", "You have entered the wrong password. Please try again."),
    LoginUnknownAccount("LoginUnknownAccount", "The account does not exist in the system."),
    LoginInactiveAccount("LoginInactiveAccount", "Your account is inactive."),
    LoginLockedAccount("LoginLockedAccount", "Your account is locked."),
    LoginExcessConcurrentLogins("LoginExcessConcurrentLogins", "Account exceeded the maximum amount of concurrent logins."),

    NotLoggedIn("NotLoggedIn", "You are not logged in."),


    //Partner Portal
    ErrorAddingToCart("ErrorAddingToCart", "Unable to add to cart. Please refresh the page and try again."),
    DifferentEventSchedules("DifferentEventSchedules", "Your chosen item cannot be added as there is an existing item of a different event date in the cart. Please purchase this item in a separate transaction or remove the previous event items from your cart. We apologize for the inconvenience."),
    InsufficientQty("InsufficientQty", "There is insufficient capacity for this selection to fulfil your quantity requirement. Please adjust your quantity or make another selection"),

    PkgValidateEventItemCantMix("PkgValidateEventItemCantMix","Event Items Cannot Mix with other items."),
    PkgValidateInsufficientQty("PkgValidateInsufficientQty", "Please note that you have insufficient products to generate the required package. Kindly adjust your product or package quantity to proceed."),
    PkgValidateCreateErrItemsReserved("PkgValidateCreateErrItemsReserved", "Some items are reserved by another account, please select other items."),
    PkgNameAlreadyInUsed("PkgNameAlreadyInUsed", "Package Name is in use, System auto generated a new Package name for you."),
    PkgValidateQtyProductMin("PkgValidateQtyProductMin", "Quantity Per Product should be more than {0}."),
    PkgValidateQtyProductMax("PkgValidateQtyProductMax", "Maximum quantity for the package is {0}, please reduce the quantity and try again."),
    PkgDifferentEventSchedule("PkgDifferentEventSchedule", "Events with different schedule is not allowed to be added in the same package."),
    PkgValidatePincodeOnly("PkgValidatePincodeOnly", "One of the item requires PINCODE as ticket Media. Please select PINCODE as Ticket Media only."),
    DepositBalanceInsufficient("DepositBalanceInsufficient", "Your Deposit Balance is not enough to do this reservation."),
    TicketReachedMaxAllowed("TicketReachedMaxAllowed", "You are only allowed to reserve up to 200 ticket per Show Date/Time."),
    TicketCannotCancel("TicketCannotCancel", "The reservation is no longer allowed to be cancelled."),
    ReservationAlreadyCancelled("CannotUpdateReservation", "The reservation is already cancelled and cannot update."),
    PincodeDeactivateError("PincodeDeactivateError","Can not deactivate the pin code in AX."),



    PkgCreateErrMix("PkgCreateErrorMix", "=Event Items Cannot Mix with other items."),
    PkgCreateErrDifferentSchedule("PkgCreateErrDifferentSchedule", "Events with different schedule is not allowed to be added in the same package."),
    PkgCreateErrTypeMismatch("PkgCreateErrTypeMismatch", "Can't add this item, due to mismatch with package ticket type."),
    PkgCreateErrItemReserved("PkgCreateErrItemReserved", "This item is reserved."),
    PkgCreateErrMixStandalone("PkgCreateErrMixStandalone", "Standalone Items Cannot Mix with other items."),

    //Parnter Portal AddCart Result
    AddCartResultSuccess("AddCartResultSuccess", ""),
    AddCartResultSuccessWithPayMethod("AddCartResultSuccess", ""),
    AddCartResultSuccessWithRemovedTopups("AddCartResultSuccessWithPayMethod", ""),
    AddCartResultFailed("AddCartResultFailed", "Unable to add to cart. Please refresh the page and try again."),
    AddCartResultValidationFailed("AddCartResultValidationFailed", ""),
    AddCartResultInvalidJewelCard("AddCartResultInvalidJewelCard", ""),
    AddCartResultInvalidPromoCode("AddCartResultInvalidPromoCode", ""),
    AddCartResultInvalidatedCart("AddCartResultInvalidatedCart", "Some items in your cart were found to be invalid. Your cart has been reset. Please select items again."),
    AddCartResultConflictingCreditCard("AddCartResultConflictingCreditCard", "The item/s you have added conflicts with an existing credit card promotion item in your cart. Please try again."),
    AddCartResultExceededMaxItems("AddCartResultExceededMaxItems", "Your shopping cart has reached its maximum capacity. Please proceed to payment before adding more items."),
    AddCartResultInactiveItem("AddCartResultInactiveItem", "The item you're trying to add is unavailable. Please refresh the page and try again."),
    AddCartResultTopupExceedsLimit("AddCartResultTopupExceedsLimit", "The quantity of top-ups you have selected has exceeded the limit for this package. Please try again."),
    AddCartResultTopupExceedsMainTickets("AddCartResultTopupExceedsMainTickets", "Your selected top-ups must be of equal or lower quantity than the corresponding main items you have selected."),
    AddCartResultDifferentEventSchedules("AddCartResultDifferentEventSchedules", "Your chosen item cannot be added as there is an existing item of a different event date in the cart. Please purchase this item in a separate transaction or remove the previous event items from your cart. We apologize for the inconvenience."),
    AddCartResultFailedItemMinQty("AddCartResultFailedItemMinQty", "Please note that the minimum order quantity is {0}. Kindly increase your order quantity to proceed."),
    AddCartResultDateLapsed("AddCartResultDateLapsed", ""),
    AddCartResultInsufficientQty("AddCartResultInsufficientQty", "The remaining tickets for this product is insufficient to fulfill your requirements. Kindly adjust your order quantity or select a different product."),
    AddCartResultOverusedPromoCode("AddCartResultOverusedPromoCode", ""),
    AddCartResultExpiredJewelCard("AddCartResultExpiredJewelCard", ""),
    AddCartResultConflictingPayMethod("AddCartResultConflictingPayMethod", ""),
    AddCartResultNoMainItem("AddCartResultNoMainItem", ""),
    AddCartResultDifferentExiprationItem("AddCartResultDifferentExiprationItem", "Your chosen item cannot be added as there is an existing item of a different expiration date in the cart. Please purchase this item in a separate transaction or remove the other items from your cart. We apologize for the inconvenience."),

    //Partner Portal Checkout Result
    CheckoutResultSuccess("CheckoutResultSuccess", ""),
    CheckoutResultFailed("CheckoutResultFailed", ""),
    CheckoutResultCustomerDetailsFailed("CheckoutResultCustomerDetailsFailed", ""),
    CheckoutResultFailedGetBookingFee("CheckoutResultFailedGetBookingFee", ""),
    CheckoutResultFailedReceiptSequence("CheckoutResultFailedReceiptSequence", ""),
    CheckoutResultFailedReserveTicket("CheckoutResultFailedReserveTicket", ""),
    CheckoutResultFailedInsufficientTickets("CheckoutResultFailedInsufficientTicket", ""),
    CheckoutResultFailedPurchaseMinQtyPerTxn("CheckoutResultFailedPurchaseMinQtyPerTxn", ""),
    CheckoutResultFailedPurchaseMaxDailyTxnLimit("CheckoutResultFailedPurchaseMaxDailyTxnLimit", ""),
    CheckoutResultFailedPurchaseMaxAmountPerTxn("CheckoutResultFailedPurchaseMaxAmountPerTxn", ""),
    CheckoutResultFailedPurchaseMinQtySetting("CheckoutResultFailedPurchaseMinQtySetting", ""),
    CheckoutResultFailedConfirmSale("CheckoutResultFailedConfirmSale", ""),
    CheckoutResultFailedMixEventInOffline("CheckoutResultFailedMixEventInOffline", ""),
    CheckoutResultFailedMixBarcodeInOffline("CheckoutResultFailedMixBarcodeInOffline", ""),
    CheckoutResultFailedProductDetailsCheckInOffline("CheckoutResultFailedProductDetailsCheckInOffline", ""),
    CheckoutResultFailedItemExpiredInCatalog("CheckoutResultFailedItemExpiredInCatalog", ""),
    CheckoutResultFailedItemDifferentValidityCfg("CheckoutResultFailedItemDifferentValidityCfg", ""),

    //Revalidation
    RevaliationNotAllowed("RevaliationNotAllowed", "Revaliation is unavailable for this transaction"),
    ErrorInGettingRevalidationInfo("ErrorInGettingRevalidationInfo", "Error encountered when getting the revalidation info"),

    //Partner Portal Admin
    PartnerNotFound("PartnerNotFound","Partner is not found."),
    SubAccountAlreadyExists("SubaccountAlreadyExists","User creation failed. Username already exists."),
    MaxSubAccountExceeded("MaxSubaccountExceeded", "Exceeded the number of sub users allowed."),
    SubAccountNotFound("SubaccountNotFound","Subaccount is not found."),
    RefundErrorWithPin("RefundErrorWithPin","You can't refund a transaction with PIN code."),
    Success("Success","Success"),
    AxCustomerNotFound("AxCustomerNotFound","Error updating Customer (AX)."),
    RemarksNotFound("RemarksNotFound","Remarks not found."),

    //System Configuration
    ConfigNotFound("ConfigNotFound","System Configuration not found."),
    BackupAlreadyCovering("BackupAlreadyCovering","This officer is already assigned as covering officer."),
    BackupAlreadyAdmin("BackupAlreadyAdmin","This officer is already assigned as approval officer."),
    BackupSelfBackup("BackupSelfBackup","You may not set covering officer same as approval officer."),
    BackupOverlap("BackupOverlap","The covering period is overlapped with existing covering officer."),
    SysParamUpdateNoApproveSuccess("SysParamUpdateNoApproveSuccess","The System Parameter is updated successfully."),
    SysParamUpdateApproveSuccess("SysParamUpdateApproveSuccess","The System Parameter is pending approval now, it will changed after approve."),
    SysParamUpdateErrorPending("SysParamUpdateErrorPending","The System Parameter is pending approval, you can only change it after approved."),
    TicketConfigNotFound("TicketConfigNotFound","Ticket Configuration not found."),
    WotConfigNotFound("WotConfigNotFound","Wings of Time Configuration not found."),
    RevalItemNotFound("RevalItemNotFound","Invalid Product Listing ID."),
    RevalItemAlreadyExists("RevalItemAlreadyExists","Revalidation Item alreadys exists."),
    RevalItemRetrievalError("RevalItemRetrievalError","Error retrieving Revalidation Items."),
    WotResetError("WotResetError","Error during Reset Partner Reservation Cap."),
    WotProductsRetrievalError("WotProductsRetrievalError","Error retrieving Wings of Time Products."),
    WotProductNotFound("WotProductNotFound","Invalid Product Listing ID."),
    WotProductAlreadyExists("WotProductAlreadyExists","Wings of Time product alreadys exists."),
    GSTOverlapError("GSTOverlapError","The GST period is overlapped with others."),

    //Reports
    PartnerProfileRptInitError("PartnerProfileRptInitError","Error initializing Report."),

    // Kiosk
    TicketPrintError("TicketPrintError", ""),
    TicketAndReceiptPrintError("TicketAndReceiptPrintError", ""),
    NoKioskTransactionFound("NoTransactionFound", "No Transaction with the Receipt Number found.")
    ;

    public final String code;
    public final String message;

    ApiErrorCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }
}