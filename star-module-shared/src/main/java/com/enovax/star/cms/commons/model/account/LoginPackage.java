package com.enovax.star.cms.commons.model.account;

public class LoginPackage {

    private String username;
    private String password;

    public LoginPackage() {

    }

    public LoginPackage(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
