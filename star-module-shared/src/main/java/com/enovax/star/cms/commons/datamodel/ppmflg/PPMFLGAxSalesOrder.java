package com.enovax.star.cms.commons.datamodel.ppmflg;

import javax.persistence.*;

/**
 * Created by jennylynsze on 8/17/16.
 */
@Entity
@Table(name = "PPMFLGAxSalesOrder")
public class PPMFLGAxSalesOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "transactionId", nullable = false)
    private Integer transactionId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transactionId", referencedColumnName = "id", updatable = false, insertable = false)
    private PPMFLGInventoryTransaction inventoryTrans;

    @Column(name = "customerId")
    private String customerId;

    @Column(name = "salesOrderId")
    private String salesOrderId;

    @Column(name = "salesOrderJson")
    private String salesOrderJson;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public PPMFLGInventoryTransaction getInventoryTrans() {
        return inventoryTrans;
    }

    public void setInventoryTrans(PPMFLGInventoryTransaction inventoryTrans) {
        this.inventoryTrans = inventoryTrans;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(String salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public String getSalesOrderJson() {
        return salesOrderJson;
    }

    public void setSalesOrderJson(String salesOrderJson) {
        this.salesOrderJson = salesOrderJson;
    }
}
