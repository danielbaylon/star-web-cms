
package com.enovax.star.cms.commons.ws.axretail;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_CartRetailTicketTableResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_CartRetailTicketTableResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="CartRetailTicketCollection" type="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models}SDC_CartRetailTicketTable" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_CartRetailTicketTableResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", propOrder = {
    "cartRetailTicketCollection"
})
public class SDCCartRetailTicketTableResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "CartRetailTicketCollection", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services", type = JAXBElement.class, required = false)
    protected JAXBElement<SDCCartRetailTicketTable> cartRetailTicketCollection;

    /**
     * Gets the value of the cartRetailTicketCollection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}
     *     
     */
    public JAXBElement<SDCCartRetailTicketTable> getCartRetailTicketCollection() {
        return cartRetailTicketCollection;
    }

    /**
     * Sets the value of the cartRetailTicketCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SDCCartRetailTicketTable }{@code >}
     *     
     */
    public void setCartRetailTicketCollection(JAXBElement<SDCCartRetailTicketTable> value) {
        this.cartRetailTicketCollection = value;
    }

}
