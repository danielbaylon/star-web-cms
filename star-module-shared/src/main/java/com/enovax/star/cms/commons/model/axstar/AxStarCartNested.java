package com.enovax.star.cms.commons.model.axstar;

public class AxStarCartNested {
    private AxStarCart cart;

    public AxStarCart getCart() {
        return cart;
    }

    public void setCart(AxStarCart cart) {
        this.cart = cart;
    }
}
