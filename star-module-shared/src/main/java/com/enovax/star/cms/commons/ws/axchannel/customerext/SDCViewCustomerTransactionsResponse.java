
package com.enovax.star.cms.commons.ws.axchannel.customerext;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SDC_ViewCustomerTransactionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SDC_ViewCustomerTransactionsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services}ServiceResponse">
 *       &lt;sequence>
 *         &lt;element name="CustomerTransactionDetails" type="{http://schemas.datacontract.org/2004/07/SDC_OnlineCRTExtensions.DataModels}ArrayOfCustomerTransactionDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SDC_ViewCustomerTransactionsResponse", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", propOrder = {
    "customerTransactionDetails"
})
public class SDCViewCustomerTransactionsResponse
    extends ServiceResponse
{

    @XmlElementRef(name = "CustomerTransactionDetails", namespace = "http://schemas.datacontract.org/2004/07/Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomerTransactionDetails> customerTransactionDetails;

    /**
     * Gets the value of the customerTransactionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerTransactionDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomerTransactionDetails> getCustomerTransactionDetails() {
        return customerTransactionDetails;
    }

    /**
     * Sets the value of the customerTransactionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerTransactionDetails }{@code >}
     *     
     */
    public void setCustomerTransactionDetails(JAXBElement<ArrayOfCustomerTransactionDetails> value) {
        this.customerTransactionDetails = value;
    }

}
