package com.enovax.star.cms.commons.model.ticketgen;

public class TicketXmlRecord {

    private String id;
    private String templateXml;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTemplateXml() {
        return templateXml;
    }

    public void setTemplateXml(String templateXml) {
        this.templateXml = templateXml;
    }
}
