package com.enovax.star.cms.commons.model.axchannel.pin;

/**
 * Created by houtao on 18/8/16.
 */
public class AxWOTOrder {

    private String cartId;
    private String customerAccount;
    private String receiptEmail;
    private String onlineSalesPool;


    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(String receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public String getOnlineSalesPool() {
        return onlineSalesPool;
    }

    public void setOnlineSalesPool(String onlineSalesPool) {
        this.onlineSalesPool = onlineSalesPool;
    }
}
