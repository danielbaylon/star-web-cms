package com.enovax.star.cms.commons.repository.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMPrevPassword;
import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMTAPrevPasswordSub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PPSLMTAPrevPasswordSubRepository extends JpaRepository<PPSLMTAPrevPasswordSub, Integer> {

    @Query("from PPSLMTAPrevPasswordSub where subAccountId = ?1 order by createdDate desc")
    List<PPSLMPrevPassword> findHistByAccountId(Integer id);
}
