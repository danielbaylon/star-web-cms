package com.enovax.star.cms.commons.repository.specification.ppslm;

import com.enovax.star.cms.commons.datamodel.ppslm.PPSLMOfflinePaymentRequest;
import com.enovax.star.cms.commons.repository.specification.BaseSpecification;

/**
 * Created by houtao on 30/9/16.
 */
public class PPSLMOfflinePaymentRequestSpecification extends BaseSpecification<PPSLMOfflinePaymentRequest> {
}
