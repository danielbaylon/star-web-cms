package com.enovax.star.cms.commons.jcrrepository.system;

import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.model.system.SysParamVM;
import com.enovax.star.cms.commons.util.PublishingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DefaultJcrSystemParamRepository implements ISystemParamRepository {

    private static final Logger log = LoggerFactory.getLogger(DefaultJcrSystemParamRepository.class);

    public String getSystemParamValueByKey(String appKey, String key) {
        SysParamVM vm = getSystemParamByKey(appKey, key);
        if(vm == null){
            return null;
        }
        String str = vm.getValue();
        vm = null;
        if(str != null && str.trim().length() > 0){
            return str.trim();
        }
        return null;
    }

    public SysParamVM getSystemParamByKey(String appKey, String paramKey) {
        if(paramKey == null || paramKey.trim().length() == 0){
            return null;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.AppConfigs.getWorkspaceName(), "/" + appKey +"/" + paramKey.trim());
            if(node == null){
                return null;
            }
            return populateSystemParamVM(node, true, false);
        } catch (RepositoryException e) {
            log.error("App-Config [/"+appKey+"/"+paramKey+"] not found : "+e.getMessage());
        }

        return null;
    }

    @Override
    public Map<String, String> getSystemParamAsMapByKey(String appKey, String paramKey) {
        if(paramKey == null || paramKey.trim().length() == 0){
            return null;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.AppConfigs.getWorkspaceName(), "/"+ appKey +"/"+paramKey.trim());
            if(node == null){
                return null;
            }
            SysParamVM param = populateSystemParamVM(node, true, true);
            if(param != null){
                Map<String, String> map = null;
                List<SysParamVM> cfgs = param.getCfgs();
                if(cfgs != null && cfgs.size() > 0){
                    map = new HashMap<String, String>();
                    for(SysParamVM vm : cfgs){
                        map.put(vm.getAttrName(), vm.getValue());
                    }
                    cfgs = null;
                    return map;
                }
            }
        } catch (RepositoryException e) {
            log.error("App-Config [/"+appKey+"/"+paramKey+"] not found : "+e.getMessage());
        }

        return null;
    }

    @Override
    public void saveSystemParamByKey(String appKey, String paramKey, String value) {
        if(paramKey == null || paramKey.trim().length() == 0){
            return;
        }
        try {
            Node node = JcrRepository.getParentNode(JcrWorkspace.AppConfigs.getWorkspaceName(), "/" + appKey +"/" + paramKey.trim());
            if(node == null){
                return;
            }
            if(node.hasProperty("value")) {
                node.setProperty("value",value);
                //TODO check if modified by is updated
                JcrRepository.updateNode(JcrWorkspace.AppConfigs.getWorkspaceName(),node);
                PublishingUtil.publishNodes(node.getIdentifier(),JcrWorkspace.AppConfigs.getWorkspaceName());
            }
        } catch (Exception e) {
            log.error("App-Config [/"+appKey+"/"+paramKey+"] not found : "+e.getMessage());
        }
    }

    private SysParamVM populateSystemParamVM(Node node, boolean isRootElement, boolean requiredAttrName) throws RepositoryException {
        if(node == null){
            return null;
        }
        String attrName = null;
        if(requiredAttrName){
            if(!isRootElement){
                attrName = getPropertyValue(node, "attrName");
                if(attrName == null || attrName.trim().length() == 0){
                    return null;
                }
            }
        }
        SysParamVM vm = new SysParamVM();
        vm.setName(node.getName());
        vm.setValue(getPropertyValue(node, "value"));
        if(node.hasProperty("requireApprove")) {
            vm.setRequireApprove(getPropertyValue(node, "requireApprove").equalsIgnoreCase("true")?true:false);
        }
        vm.setAttrName(attrName);
        NodeIterator nodeIter = node.getNodes();
        if(nodeIter != null){
            List<SysParamVM> cfgs = new ArrayList<SysParamVM>();
            while(nodeIter.hasNext()){
                Node subNode = nodeIter.nextNode();
                SysParamVM subVM = populateSystemParamVM(subNode, false, requiredAttrName);
                if(subNode != null){
                    cfgs.add(subVM);
                }
            }
            if(cfgs.size() > 0) {
                vm.setCfgs(cfgs);
            }else{
                cfgs = null;
            }
        }
        nodeIter = null;
        return vm;
    }

    private String getPropertyValue(Node node, String key) throws RepositoryException {
        if(key == null || key.trim().length() == 0){
            return null;
        }
        if(!node.hasProperty(key.trim())){
            return null;
        }
        Property prop = node.getProperty(key.trim());
        if(prop == null){
            return null;
        }
        String str = prop.getString();
        prop = null;
        if(str.trim().length() > 0){
            return str.trim();
        }
        return null;
    }
}
