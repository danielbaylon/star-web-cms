package com.enovax.payment.tm.constant;

/**
 * Created by jensen on 10/9/15.
 */
public enum TmCurrency {
    SGD,
    USD;

    public static TmCurrency fromCode(String code) {
        if(SGD.toString().equals(code)) {
            return SGD;
        }

        if(USD.toString().equals(code)) {
            return USD;
        }

        return TmCurrency.SGD;
    }
}
