package com.enovax.payment.tm.constant;

/**
 * Created by jensen on 9/9/15.
 */
public enum EnovaxTmSystemStatus {
    //Reserved
    RedirectedToTm,
    TmQuerySent,

    //Success
    Success,

    //Failed
    Failed,
    VoidSuccess,
    VoidFailed,

    //Incomplete
    NA;
}
