package com.enovax.payment.tm.constant;

public enum TmTransType {
    Sale("sale"),
    Void("void"),
    Reversal("reversal"),
    Refund("refund");

    public final String tmCode;

    TmTransType(String tmCode) {
        this.tmCode = tmCode;
    }
}
