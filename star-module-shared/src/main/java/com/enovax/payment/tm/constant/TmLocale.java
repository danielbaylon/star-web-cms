package com.enovax.payment.tm.constant;

/**
 * Created by jensen on 10/9/15.
 */
public enum TmLocale {
    EnglishUs("en_US"),
    ChineseSimplified("zh_CN");

    public final String tmCode;

    TmLocale(String tmCode) {
        this.tmCode = tmCode;
    }
}
