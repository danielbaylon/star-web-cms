package com.enovax.payment.tm.service;

import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.constant.TmPaymentType;
import com.enovax.payment.tm.constant.TmStatus;
import com.enovax.payment.tm.constant.TmTransType;
import com.enovax.payment.tm.model.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class TmServiceProvider {

    public static final String AMT = "amt";
    public static final String REF = "ref";
    public static final String CUR = "cur";
    public static final String MID = "mid";
    public static final String TRANSTYPE = "transtype";
    public static final String SUBTRANSTYPE = "subtranstype";
    public static final String STATUSURL = "statusurl";
    public static final String RCARD = "rcard";
    public static final String LOCALE = "locale";
    public static final String RETURNURL = "returnurl";
    public static final String PAYTYPE = "paytype";
    public static final String VERSION = "version";
    public static final String SIGNATURE = "signature";
    public static final String USERFIELD1 = "userfield1";
    public static final String USERFIELD2 = "userfield2";
    public static final String USERFIELD3 = "userfield3";
    public static final String USERFIELD4 = "userfield4";
    public static final String USERFIELD5 = "userfield5";

    public static final String RCARD_64 = "64"; //Masked CC: First 6 - Last 4

    private Logger log = LoggerFactory.getLogger(TmServiceProvider.class);

    private String tmRequestUrl;
    private String tmQueryUrl;
    private String tmVoidUrl;

    private int retryLimit = 3;
    private int retryIntervalInMs = 3000;

    public static final String ENCODING = "UTF-8";

    private static DecimalFormat moneyFormatter = new DecimalFormat("##0.00");
    static {
        moneyFormatter.setRoundingMode(RoundingMode.HALF_UP);
        moneyFormatter.setParseBigDecimal(true);
    }

    public TmServiceProvider(String tmRequestUrl, String tmQueryUrl, String tmVoidUrl) {
        this.tmRequestUrl = tmRequestUrl;
        this.tmQueryUrl = tmQueryUrl;
        this.tmVoidUrl = tmVoidUrl;
    }

    public TmServiceProvider(String tmRequestUrl, String tmQueryUrl, String tmVoidUrl,
                             int retryLimit, int retryIntervalInMs) {
        this.tmRequestUrl = tmRequestUrl;
        this.tmQueryUrl = tmQueryUrl;
        this.tmVoidUrl = tmVoidUrl;
        this.retryLimit = retryLimit;
        this.retryIntervalInMs = retryIntervalInMs;
    }

    public String constructPaymentRedirectUrl(TmPaymentRedirectInput input) {
        final String currency = input.getCurrency().name();
        final String merchantId = input.getMerchantId();
        final String receiptNumber = input.getReceiptNumber();
        final String returnUrl = input.getReturnUrl();
        final String statusUrl = input.getStatusUrl();
        final String returnUrlParamString = input.getReturnUrlParamString();
        final TmPaymentType paymentTypeEnum = input.getTmPaymentType();
        final String paymentType = paymentTypeEnum == null ? "" : paymentTypeEnum.tmCode;
        final String amountText = moneyFormatter.format(input.getTotalAmount());
        final TmFraudCheckPackage fraudCheckPackage = input.getFraudCheckPackage();
        final String locale = input.getTmLocale().tmCode;
        final String userField1 = input.getUserField1();
        final String userField2 = input.getUserField2();
        final String userField3 = input.getUserField3();
        final String userField4 = input.getUserField4();
        final String userField5 = input.getUserField5();

        final TmMerchantSignatureConfig signatureConfig = input.getMerchantSignatureConfig();
        final String signature = generateSignature(signatureConfig, amountText, receiptNumber, currency, merchantId, TmTransType.Sale.tmCode);

        try {

            String url = tmRequestUrl +
                    "?" + MID +  "=" + URLEncoder.encode(merchantId, ENCODING) +
                    "&" + AMT + "=" + URLEncoder.encode(amountText, ENCODING) +
                    "&" + CUR + "=" + URLEncoder.encode(currency, ENCODING) +
                    "&" + REF + "=" + URLEncoder.encode(receiptNumber, ENCODING) +
                    "&" + STATUSURL + "=" + URLEncoder.encode(statusUrl, ENCODING) +
                    "&" + RCARD + "=" + RCARD_64 +
                    "&" + LOCALE + "="+ locale +
                    "&" + RETURNURL + "=" +
                    URLEncoder.encode(returnUrl + (StringUtils.isEmpty(returnUrlParamString) ? "" : "?" + returnUrlParamString), ENCODING) +
                    (StringUtils.isEmpty(paymentType) ? "" : "&" + PAYTYPE + "=" + URLEncoder.encode(paymentType, ENCODING)) +
                    (fraudCheckPackage == null ? "" : fraudCheckPackage.constructUrlParams()) +
                    "&" + TRANSTYPE + "=" + TmTransType.Sale.tmCode +
                    (StringUtils.isEmpty(signature)? "": "&" + VERSION + "=" + signatureConfig.getVersion() + "&" + SIGNATURE + "=" + signature) +
                    (StringUtils.isEmpty(userField1) ? "" : "&" + USERFIELD1 + "=" + URLEncoder.encode(userField1, ENCODING)) +
                    (StringUtils.isEmpty(userField2) ? "" : "&" + USERFIELD2 + "=" + URLEncoder.encode(userField2, ENCODING)) +
                    (StringUtils.isEmpty(userField3) ? "" : "&" + USERFIELD3 + "=" + URLEncoder.encode(userField3, ENCODING)) +
                    (StringUtils.isEmpty(userField4) ? "" : "&" + USERFIELD4 + "=" + URLEncoder.encode(userField4, ENCODING)) +
                    (StringUtils.isEmpty(userField5) ? "" : "&" + USERFIELD5 + "=" + URLEncoder.encode(userField5, ENCODING))
                    ;

            log.info("Constructed TM redirection URL: " + url);
            return url;
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Error encountered while constructing redirect URL.", uee);
        }
    }

    private String generateSignature(TmMerchantSignatureConfig signatureConfig,
                                     String amt, String ref, String cur, String mid, String transtype) {
        return generateSignature(signatureConfig, amt, ref, cur, mid, transtype, "", "");
    }

    /**
     *
     * @param signatureConfig
     * @param amt
     * @param ref
     * @param cur
     * @param mid
     * @param transType
     * @param tmStatus only used to verify signature sent back by Telemoney
     * @param tmError only used to verify signature sent back by Telemoney
     * @return the hashed signature, or an empty string if no signature is required or
     * an error is encountered generating a signature
     */
    private String generateSignature(TmMerchantSignatureConfig signatureConfig,
                                     String amt, String ref, String cur, String mid, String transType,
                                     String tmStatus, String tmError) {
        if (signatureConfig == null || !signatureConfig.isSignatureEnabled()) {
            log.info("Signature not generated due to it being disabled.");
            return "";
        }

        try {

            final Map<String,String> fieldValues = new HashMap<>();
            fieldValues.put(AMT, amt);
            fieldValues.put(REF, ref);
            fieldValues.put(CUR, cur);
            fieldValues.put(MID, mid);
            fieldValues.put(TRANSTYPE, transType);

            final StringBuilder sbData = new StringBuilder();
            for (String field : signatureConfig.getFieldSequence()) {
                final String fieldValue = fieldValues.get(field);
                sbData.append(fieldValue);
            }

            sbData.append(tmStatus).append(tmError).append(signatureConfig.getHashkey());

            final String data = sbData.toString();

            final MessageDigest hash = MessageDigest.getInstance("SHA-512");
            hash.update(data.getBytes());
            final byte[] result = hash.digest();
            final StringBuilder sbSignature = new StringBuilder();
            for (byte hashByte : result) {
                final String s = Integer.toHexString(hashByte);
                final int length = s.length();

                if (length >= 2) {
                    sbSignature.append(s.substring(length - 2, length));
                } else {
                    sbSignature.append("0");
                    sbSignature.append(s);
                }
            }

            log.info("Generated signature {} for data {}", sbSignature.toString(), data);

            return sbSignature.toString();
        } catch (Exception e) {
            log.error("Error encountered in generating the signature to pass to Telemoney: " + e.getMessage(), e);
        }

        return "";
    }

    public TmServiceResult sendQueryWithRetry(String merchantId, String receiptNumber, TmMerchantSignatureConfig signatureConfig) {
        int tryCount = 0;
        while (tryCount < retryLimit) {
            try {
                tryCount++;

                TmServiceResult result = sendQuery(merchantId, receiptNumber, signatureConfig);
                if (!result.isSuccess()) {
                    log.error("Send query has failed. Try count: " + tryCount);
                    waitBeforeRetrying(retryIntervalInMs);
                } else {
                    log.info("Send query success. Try count: " + tryCount);
                    return result;
                }
            } catch (Exception e) {
                log.error("An error occurred while sending query. Number of tries: " + tryCount, e);
                waitBeforeRetrying(retryIntervalInMs);
            }
        }

        final String errLog = "Sending of query has failed after " + retryLimit + " retries.";
        log.error(errLog);
        return new TmServiceResult(false, errLog, "");
    }

    public TmServiceResult sendQuery(String merchantId, String receiptNumber, TmMerchantSignatureConfig signatureConfig) {
        log.info(":: SERVICE :: TmServiceProvider . sendQuery (to TM) ::");
        try {

            log.info("Sending query to " + tmQueryUrl);
            URL url = new URL(tmQueryUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            initializeHttpConnection(connection);

            final String signature = generateSignature(signatureConfig, "", receiptNumber, "", merchantId, "sale");

            final String content = MID + "=" + URLEncoder.encode(merchantId, ENCODING) +
                    "&" + REF + "=" + URLEncoder.encode(receiptNumber, ENCODING) +
                    "&" + TRANSTYPE + "=" + TmTransType.Sale.tmCode +
                    (StringUtils.isEmpty(signature)? "": "&" + VERSION + "=" + signatureConfig.getVersion() + "&" + SIGNATURE + "=" + signature);

            log.info("Sending query with the following data: " + content);

            DataOutputStream printout = new DataOutputStream(connection.getOutputStream());
            try {
                printout.writeBytes(content);
                printout.flush();
            } finally {
                printout.close();
            }
            log.info("Successfully sent query to Telemoney.");

            try {
                int rc = connection.getResponseCode();
                log.info("Query response code: " + rc + ", message: " + connection.getResponseMessage());
                if (rc == -1 || rc >= 400) {
                    log.error("Query response returned an error code: " + rc);
                    return new TmServiceResult(false, "HTTP response code: " + rc + ", message: " + connection.getResponseMessage(), "");
                }
                return new TmServiceResult(true);
            } catch (Exception e) {
                return new TmServiceResult(false, "TmServiceProvider.sendQuery", ExceptionUtils.getStackTrace(e));
            }
        } catch (Exception e) {
            return new TmServiceResult(false, "TmServiceProvider.sendQuery", ExceptionUtils.getStackTrace(e));
        }
    }

    public TmServiceResult sendVoidWithRetry(String merchantId, TmCurrency currency, BigDecimal amount, String receiptNumber,
                                             TmMerchantSignatureConfig signatureConfig) {
        int tryCount = 0;
        while (tryCount < retryLimit) {
            try {
                tryCount++;

                TmServiceResult result = sendVoid(merchantId, currency, amount, receiptNumber, signatureConfig);
                if (!result.isSuccess() || !TmStatus.isSuccess(result.getTmResponse())) {
                    log.error("Send void has failed. Try count: " + tryCount);
                    waitBeforeRetrying(retryIntervalInMs);
                } else {
                    log.info("Send void success. Try count: " + tryCount);
                    return result;
                }
            } catch (Exception e) {
                log.error("An error occurred while sending void. Number of tries: " + tryCount, e);
                waitBeforeRetrying(retryIntervalInMs);
            }
        }

        final String errLog = "Sending of void has failed after " + retryLimit + " retries.";
        log.error(errLog);
        return new TmServiceResult(false, errLog, "");
    }

    public TmServiceResult sendVoid(String merchantId, TmCurrency currency, BigDecimal amount,
                                    String receiptNumber, TmMerchantSignatureConfig signatureConfig) {
        log.info(":: SERVICE :: TmServiceProvider . sendVoid (to TM) ::");
        try {

            //TODO Still lacking logic for handling VOID/REVERSAL/REFUND scenarios with different banks. Refer: Jen
            final String transType = TmTransType.Void.tmCode;

            final String amountText = moneyFormatter.format(amount);

            final String signature = generateSignature(signatureConfig, amountText, receiptNumber, currency.name(), merchantId, transType);

            log.info("Sending void to " + tmVoidUrl);
            URL url = new URL(tmVoidUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            initializeHttpConnection(connection);

            // Send POST output.
            String content = MID + "=" + URLEncoder.encode(merchantId, ENCODING) +
                    "&" + CUR + "=" + URLEncoder.encode(currency.name(), ENCODING) +
                    "&" + AMT + "=" + URLEncoder.encode(amountText, ENCODING) +
                    "&" + REF + "=" + URLEncoder.encode(receiptNumber, ENCODING) +
                    "&" + TRANSTYPE + "=" + transType + "&" + SUBTRANSTYPE + "=" + TmTransType.Sale.tmCode +
                    (StringUtils.isEmpty(signature)? "": "&" + VERSION + "=" + signatureConfig.getVersion() + "&" + SIGNATURE + "=" + signature);

            log.info("Sending void with the following data: " + content);

            DataOutputStream printout = new DataOutputStream(connection.getOutputStream());
            try {

                printout.writeBytes(content);
                printout.flush();
            } finally {
                printout.close();
            }

            /*
             *  Gigantic NOTE to everyone: Apparently, when you send a VOID request to Telemoney, you don't need
             *  to handle the response because TM will echo the response back along the statusURL you specified
             *  when you started this whole transaction.
             *
             *  Untested if this behavior also happens during batch job queries when much time may have already
             *  gone by since the user accessed the payment gateway.
             */

            //Retrieve results
            log.info("Retrieving results of Send Void.");
            InputStream inputStream = connection.getInputStream();
            try {

                final String voidOutput = IOUtils.toString(inputStream, ENCODING);
                final TelemoneyResponse response = TelemoneyResponse.fromUrlParams(voidOutput, false);

                return new TmServiceResult(response);
            } finally {
                inputStream.close();
            }
        } catch (Exception e) {
            return new TmServiceResult(false, "TmServiceProvider.sendVoid", ExceptionUtils.getStackTrace(e));
        }
    }

    private void initializeHttpConnection(HttpURLConnection conn) throws ProtocolException {
        conn.setDoInput (true);
        conn.setDoOutput (true);
        conn.setUseCaches (false);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    }

    private void waitBeforeRetrying(Integer retryIntervalInMs) {
        try {

            log.info("Retrying service method after " + retryIntervalInMs + " milliseconds.");
            Thread.sleep(retryIntervalInMs);
        } catch (InterruptedException ie) {
            log.warn("Retry interval for service method was interrupted. Resuming re-execution.", ie);
        }
    }

    /*
     * Getters and Setters
     */

    public String getTmRequestUrl() {
        return tmRequestUrl;
    }

    public void setTmRequestUrl(String tmRequestUrl) {
        this.tmRequestUrl = tmRequestUrl;
    }

    public String getTmQueryUrl() {
        return tmQueryUrl;
    }

    public void setTmQueryUrl(String tmQueryUrl) {
        this.tmQueryUrl = tmQueryUrl;
    }

    public String getTmVoidUrl() {
        return tmVoidUrl;
    }

    public void setTmVoidUrl(String tmVoidUrl) {
        this.tmVoidUrl = tmVoidUrl;
    }

    public int getRetryLimit() {
        return retryLimit;
    }

    public void setRetryLimit(int retryLimit) {
        this.retryLimit = retryLimit;
    }

    public int getRetryIntervalInMs() {
        return retryIntervalInMs;
    }

    public void setRetryIntervalInMs(int retryIntervalInMs) {
        this.retryIntervalInMs = retryIntervalInMs;
    }
}
