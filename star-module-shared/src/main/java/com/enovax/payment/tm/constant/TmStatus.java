package com.enovax.payment.tm.constant;

import com.enovax.payment.tm.model.TelemoneyResponse;

public enum TmStatus {
    Success("YES"),
    NotFound("NA"),
    Failed("NO"),
    InProgress("InProgress"),
    Redirected("RDR");

    public final String tmCode;

    TmStatus(String tmCode) {
        this.tmCode = tmCode;
    }

    public static boolean isSuccess(String testValue) {
        return Success.tmCode.equals(testValue);
    }

    public static boolean isSuccess(TelemoneyResponse response) {
        if (response == null) { return false; }
        return Success.tmCode.equals(response.getTmStatus());
    }
}
