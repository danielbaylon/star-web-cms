package com.enovax.payment.tm.model;

import com.enovax.payment.tm.constant.EnovaxTmConst;
import com.enovax.payment.tm.constant.TmCurrency;
import com.enovax.payment.tm.constant.TmLocale;
import com.enovax.payment.tm.constant.TmPaymentType;

import java.math.BigDecimal;

public class TmPaymentRedirectInput {

    private String returnUrlParamString;
    private String returnUrl;
    private String statusUrl;
    private String receiptNumber;
    private BigDecimal totalAmount;
    private String merchantId;
    private TmPaymentType tmPaymentType = null;
    private TmCurrency currency = EnovaxTmConst.DEFAULT_CURRENCY;
    private TmFraudCheckPackage fraudCheckPackage = null;
    private TmLocale tmLocale = TmLocale.EnglishUs;
    private TmMerchantSignatureConfig merchantSignatureConfig = null;
    private String userField1; //20 max
    private String userField2; //20
    private String userField3; //40
    private String userField4; //40
    private String userField5; //200

    public TmPaymentRedirectInput(String returnUrl, String statusUrl, String returnUrlParamString, String receiptNumber,
                                  BigDecimal totalAmount, String merchantId) {
        this.returnUrl = returnUrl;
        this.statusUrl = statusUrl;
        this.returnUrlParamString = returnUrlParamString;
        this.receiptNumber = receiptNumber;
        this.totalAmount = totalAmount;
        this.merchantId = merchantId;
    }

    public TmPaymentRedirectInput(String returnUrl, String statusUrl, String returnUrlParamString, String receiptNumber,
                                  BigDecimal totalAmount, String merchantId, TmPaymentType tmPaymentType) {
        this.returnUrl = returnUrl;
        this.statusUrl = statusUrl;
        this.returnUrlParamString = returnUrlParamString;
        this.receiptNumber = receiptNumber;
        this.totalAmount = totalAmount;
        this.merchantId = merchantId;
        this.tmPaymentType = tmPaymentType;
    }

    public TmPaymentRedirectInput(String returnUrl, String statusUrl, String returnUrlParamString, String receiptNumber,
                                  BigDecimal totalAmount, String merchantId, TmPaymentType tmPaymentType,
                                  TmCurrency currency) {
        this.returnUrl = returnUrl;
        this.statusUrl = statusUrl;
        this.returnUrlParamString = returnUrlParamString;
        this.receiptNumber = receiptNumber;
        this.totalAmount = totalAmount;
        this.merchantId = merchantId;
        this.tmPaymentType = tmPaymentType;
        this.currency = currency;
    }

    public TmPaymentRedirectInput(String returnUrl, String statusUrl, String returnUrlParamString, String receiptNumber,
                                  BigDecimal totalAmount, String merchantId, TmPaymentType tmPaymentType,
                                  TmCurrency currency, TmFraudCheckPackage fraudPkg, TmLocale locale) {
        this.returnUrl = returnUrl;
        this.statusUrl = statusUrl;
        this.returnUrlParamString = returnUrlParamString;
        this.receiptNumber = receiptNumber;
        this.totalAmount = totalAmount;
        this.merchantId = merchantId;
        this.tmPaymentType = tmPaymentType;
        this.currency = currency;
        this.fraudCheckPackage = fraudPkg;
        this.tmLocale = locale;
    }

    public TmPaymentRedirectInput(String returnUrl, String statusUrl, String returnUrlParamString,
                                  String receiptNumber, BigDecimal totalAmount,
                                  String merchantId, TmPaymentType tmPaymentType, TmCurrency currency,
                                  TmFraudCheckPackage fraudCheckPackage, TmLocale tmLocale,
                                  TmMerchantSignatureConfig merchantSignatureConfig) {
        this.returnUrl = returnUrl;
        this.statusUrl = statusUrl;
        this.returnUrlParamString = returnUrlParamString;
        this.receiptNumber = receiptNumber;
        this.totalAmount = totalAmount;
        this.merchantId = merchantId;
        this.tmPaymentType = tmPaymentType;
        this.currency = currency;
        this.fraudCheckPackage = fraudCheckPackage;
        this.tmLocale = tmLocale;
        this.merchantSignatureConfig = merchantSignatureConfig;
    }

    public String getReturnUrlParamString() {
        return returnUrlParamString;
    }

    public void setReturnUrlParamString(String returnUrlParamString) {
        this.returnUrlParamString = returnUrlParamString;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public TmPaymentType getTmPaymentType() {
        return tmPaymentType;
    }

    public void setTmPaymentType(TmPaymentType tmPaymentType) {
        this.tmPaymentType = tmPaymentType;
    }

    public TmCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(TmCurrency currency) {
        this.currency = currency;
    }

    public TmFraudCheckPackage getFraudCheckPackage() {
        return fraudCheckPackage;
    }

    public void setFraudCheckPackage(TmFraudCheckPackage fraudCheckPackage) {
        this.fraudCheckPackage = fraudCheckPackage;
    }

    public TmLocale getTmLocale() {
        return tmLocale;
    }

    public void setTmLocale(TmLocale tmLocale) {
        this.tmLocale = tmLocale;
    }

    public String getUserField5() {
        return userField5;
    }

    public void setUserField5(String userField5) {
        this.userField5 = userField5;
    }

    public String getUserField4() {
        return userField4;
    }

    public void setUserField4(String userField4) {
        this.userField4 = userField4;
    }

    public String getUserField3() {
        return userField3;
    }

    public void setUserField3(String userField3) {
        this.userField3 = userField3;
    }

    public String getUserField2() {
        return userField2;
    }

    public void setUserField2(String userField2) {
        this.userField2 = userField2;
    }

    public String getUserField1() {
        return userField1;
    }

    public void setUserField1(String userField1) {
        this.userField1 = userField1;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public TmMerchantSignatureConfig getMerchantSignatureConfig() {
        return merchantSignatureConfig;
    }

    public void setMerchantSignatureConfig(TmMerchantSignatureConfig merchantSignatureConfig) {
        this.merchantSignatureConfig = merchantSignatureConfig;
    }

    public String getStatusUrl() {
        return statusUrl;
    }

    public void setStatusUrl(String statusUrl) {
        this.statusUrl = statusUrl;
    }
}
