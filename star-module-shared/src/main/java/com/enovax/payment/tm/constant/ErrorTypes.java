package com.enovax.payment.tm.constant;

/**
 * Created by jennylynsze on 11/15/16.
 */
public enum ErrorTypes {
    General("S-GENERAL", "(MFLG) General error occurred."),

    TKT_PDF_GEN_FAILED("S-PDFFAIL", "(SIMPLE Ticketing) An error occurred during Receipt PDF Generation"),
    TKT_BARCODE_IOE("S-BCODEIOE", "(SIMPLE Ticketing) An IO error occurred during Barcode Generation"),

    TKT_INVALID_TRANS("S-NULTRANS", "(Ticketing) TMTNF No transaction was found for the reference number returned by Telemoney."),
    TKT_STATUS_SET("S-TMSTAT0", "(Ticketing) TMTSE Telemoney status has already been set for this transaction."),
    TKT_WRONG_TMMID("S-TMMID0", "(Ticketing) TMMII Transaction merchant ID was different from Telemoney-returned mID."),

    TKT_RECGEN_GENERAL_VOID1("S-RECGENV1", "(Ticketing) Error occurred while trying to generate receipt. VoidTM: YES"),
    TKT_RECGEN_GENERAL_VOID0("S-RECGENV0", "(Ticketing) Error occurred while trying to generate receipt. VoidTM: NO"),
    TKT_RECGEN_GENERAL_VOID1_SALE0("S-RECGENV1S0", "(Ticketing) Error occurred while trying to generate receipt. VoidTM: YES, VoidSale: NO"),
    TKT_RECGEN_GENERAL_VOID0_SALE0("S-RECGENV0S0", "(Ticketing) Error occurred while trying to generate receipt. VoidTM: NO, VoidSale: NO"),
    TKT_RECGEN_GENERAL_VOID1_RECUR0("S-RECGENV1R0", "(Ticketing) Error occurred while trying to generate receipt. VoidTM: YES, VoidRecur: NO"),
    TKT_RECGEN_GENERAL_VOID0_RECUR0("S-RECGENV0R0", "(Ticketing) Error occurred while trying to generate receipt. VoidTM: NO, VoidRecur: NO"),
    TKT_RESEXPIRED_VOID1("S-RESEXPV1", "(Ticketing) Unable to confirm Sales due to reservation expiry. VoidTM: YES"),
    TKT_RESEXPIRED_VOID0("S-RESEXPV0", "(Ticketing) Unable to confirm Sales due to reservation expiry. VoidTM: NO"),
    TKT_CONF0_VOID1("S-CONF0V1", "(Ticketing) Unable to confirm Sales of the transaction. VoidTM: YES"),
    TKT_CONF0_VOID0("S-CONF0V0", "(Ticketing) Unable to confirm Sales of the transaction. VoidTM: NO"),
    TKT_CONF0_REL1("S-CONF0V1", "(Ticketing) Unable to confirm Sales of the transaction. VoidTM: YES ReleaseTicket: YES"),
    TKT_CONF0_REL0("S-CONF0V0", "(Ticketing) Unable to confirm Sales of the transaction. VoidTM: YES ReleaseTicket: NO"),
    TKT_CONF0_VOID1_RECUR0("S-CONF0V1R0", "(Ticketing) Unable to confirm Sales of the transaction. VoidTM: YES, VoidRecur: NO"),
    TKT_CONF0_VOID0_RECUR0("S-CONF0V0R0", "(Ticketing) Unable to confirm Sales of the transaction. VoidTM: NO, VoidRecur: NO"),
    TKT_DBFAILED_VOID1("S-DBFAILV1", "(Ticketing) Transaction was unabled to be saved to Database. VoidTM: YES"),
    TKT_DBFAILED_VOID0("S-DBFAILV0", "(Ticketing) Transaction was unabled to be saved to Database. VoidTM: NO"),
    TM_USER_CANCEL_REL1("T-USRCANR1", "(Telemoney) User has cancelled transaction on Telemoney side. ReleaseTicket: YES"),
    TM_USER_CANCEL_REL0("T-USRCANR0", "(Telemoney) User has cancelled transaction on Telemoney side. ReleaseTicket: NO"),
    TM_INC_AMT_REL1_VOID1("T-IAR1V1", "(Telemoney) Telemoney returned incorrect amount. ReleaseTicket: YES, VoidTM: YES"),
    TM_INC_AMT_REL1_VOID0("T-IAR1V0", "(Telemoney) Telemoney returned incorrect amount. ReleaseTicket: YES, VoidTM: NO"),
    TM_INC_AMT_REL0_VOID1("T-IAR0V1", "(Telemoney) Telemoney returned incorrect amount. ReleaseTicket: NO, VoidTM: YES"),
    TM_INC_AMT_REL0_VOID0("T-IAR0V0", "(Telemoney) Telemoney returned incorrect amount. ReleaseTicket: NO, VoidTM: NO"),
    TM_STATUSFAIL_REL1("T-FAILR1", "(Telemoney) Telemoney returns NA/Failed status. ReleaseTicket: YES"),
    TM_STATUSFAIL_REL0("T-FAILR0", "(Telemoney) Telemoney returns NA/Failed status. ReleaseTicket: NO"),
    TM_VOID_ERROR("T-VOIDERR", "(Telemoney) STMVE An exception occurred during Send TM Void execution."),
    TM_QUERY_ERROR("T-QUERYERR", "(Telemoney) An exception occurred during Send TM Query execution."),
    TKT_EMAIL_ERROR("S-FAILMAIL", "(Ticketing) EMFA An error occurred while sending an email."),

    TM_INVALID_RDR("T-TMRDR", "(Telemoney) Telemoney invalid transaction TM status was RDR."),
    TM_RDR_REL0_VOID1("T-RDRR0V1", "(Telemoney) Telemoney returns RDR status. VoidTM: YES"),
    TM_RDR_REL0_VOID0("T-RDRR0V0", "(Telemoney) Telemoney returns RDR status. VoidTM: NO"),
    TM_RDR_REL1_VOID1("T-RDRR1V1", "(Telemoney) Telemoney returns RDR status. VoidTM: YES"),
    TM_RDR_REL1_VOID0("T-RDRR1V0", "(Telemoney) Telemoney returns RDR status. VoidTM: NO"),

    TKT_REV_0COST_VOID0("T-REL0COST0", "(Revalidate) With 0 charge failed. VoidST: YES"),
    TKT_REV_0COST_VOID1("T-REL0COST1", "(Revalidate) With 0 charge failed. VoidST: NO"),
    TKT_REV_0COST_ERROR("T-REL0COSTR", "(Revalidate) With 0 charge reserve failed.");

    public final String cd;
    public final String msg;
    private ErrorTypes(String cd, String msg) {
        this.cd = cd;
        this.msg = msg;
    }
}
