package com.enovax.payment.tm.model;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
public class TmFraudCheckPackage {

    private String firstName;
    private String phone;
    private String email;
    private String birthDate;
    private String birthDateInitialFormat;
    private String ip;
    private String encoding;

    public static final String BIRTHDATE_OUT_FORMAT = "yyyy-MM-dd";

    public String constructUrlParams() {
        final StringBuilder sb = new StringBuilder();
        try {

            if (StringUtils.isNotEmpty(firstName)) {
                sb.append("&firstname=").append(URLEncoder.encode(StringUtils.substring(firstName, 0, 50), encoding));
            }

            if (StringUtils.isNotEmpty(phone)) {
                sb.append("&phone=").append(URLEncoder.encode(StringUtils.substring(phone, 0, 25), encoding));
            }

            if (StringUtils.isNotEmpty(email)) {
                sb.append("&email=").append(URLEncoder.encode(StringUtils.substring(email, 0, 50), encoding));
            }

            if (StringUtils.isNotEmpty(birthDate)) {
                String dobString;
                try {

                    final SimpleDateFormat initialSdf = new SimpleDateFormat(birthDateInitialFormat);
                    final Date dob = initialSdf.parse(birthDate);
                    final SimpleDateFormat finalSdf = new SimpleDateFormat(BIRTHDATE_OUT_FORMAT);
                    dobString = "&birthDate=" + finalSdf.format(dob);
                } catch (Exception e) {
                    //Error parsing birthDate. Passing through.
                    dobString = "";
                }
                sb.append(dobString);
            }

            if (StringUtils.isNotEmpty(ip)) {
                sb.append("&customeripaddr=").append(URLEncoder.encode(StringUtils.substring(ip, 0, 16), encoding));
            }

            return sb.toString();
        } catch (UnsupportedEncodingException uee) {
            //Should never happen
            uee.printStackTrace();
            return "";
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDateInitialFormat() {
        return birthDateInitialFormat;
    }

    public void setBirthDateInitialFormat(String birthDateInitialFormat) {
        this.birthDateInitialFormat = birthDateInitialFormat;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
