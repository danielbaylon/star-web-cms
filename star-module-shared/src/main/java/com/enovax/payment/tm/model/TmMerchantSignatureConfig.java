package com.enovax.payment.tm.model;

import java.util.ArrayList;
import java.util.List;

public class TmMerchantSignatureConfig {

    private boolean signatureEnabled = false;
    private String mid;
    private List<String> fieldSequence = new ArrayList<>();
    private String hashkey;
    private String version = "2";

    public TmMerchantSignatureConfig() {
    }

    public TmMerchantSignatureConfig(String mid, List<String> fieldSequence, String hashkey) {
        this.signatureEnabled = true;
        this.mid = mid;
        this.fieldSequence = fieldSequence;
        this.hashkey = hashkey;
    }

    public TmMerchantSignatureConfig(String mid, List<String> fieldSequence, String hashkey, String version) {
        this.signatureEnabled = true;
        this.mid = mid;
        this.fieldSequence = fieldSequence;
        this.hashkey = hashkey;
        this.version = version;
    }

    public boolean isSignatureEnabled() {
        return signatureEnabled;
    }

    public void setSignatureEnabled(boolean signatureEnabled) {
        this.signatureEnabled = signatureEnabled;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public List<String> getFieldSequence() {
        return fieldSequence;
    }

    public void setFieldSequence(List<String> fieldSequence) {
        this.fieldSequence = fieldSequence;
    }

    public String getHashkey() {
        return hashkey;
    }

    public void setHashkey(String hashkey) {
        this.hashkey = hashkey;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
