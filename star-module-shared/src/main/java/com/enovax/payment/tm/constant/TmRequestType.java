package com.enovax.payment.tm.constant;

/**
 * Created by jennylynsze on 11/14/16.
 */
//Copied from TmTransType.java of b2b
public enum TmRequestType {
    InventoryPurchase("tmPurchaseReturnUrl"),
    Revalidation("tmPurchaseRevalivateReturnUrl"),
    DepositTopup("tmDepositTopupReturnUrl");

    public String tmReturnUrl;

    TmRequestType(String tmReturlUrl) {
        this.tmReturnUrl = tmReturlUrl;
    }
}
