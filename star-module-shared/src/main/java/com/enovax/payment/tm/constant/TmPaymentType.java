package com.enovax.payment.tm.constant;

/**
 * Created by jensen on 9/9/15.
 */
public enum TmPaymentType {
    Mastercard("2", "Mastercard"),
    Visa("3", "VISA"),
    ChinaUnionPay("25", "China UnionPay"),
    Amex("5", "Amex Credit Card"),
    Diners("22", "Diners Credit Card"),
    JCB("23", "JCB Credit Card");

    public final String tmCode;
    public final String tmTitle;

    TmPaymentType(String tmCode, String tmTitle) {
        this.tmCode = tmCode;
        this.tmTitle = tmTitle;
    }

    //This method is still in use but can be refactored/removed
    public static TmPaymentType fromPaymentModeString(String payMode) {
        switch (payMode) {
            case "Visa" : return TmPaymentType.Visa;
            case "Mastercard" : return TmPaymentType.Mastercard;
            case "ChinaUnionPay" : return TmPaymentType.ChinaUnionPay;
            case "Amex" : return TmPaymentType.Amex;
            case "JCB" : return TmPaymentType.JCB;
            default: return null;
        }
    }

    public static String getTitle(String code) {
        switch (code) {
            case "3": return Visa.tmTitle;
            case "2": return Mastercard.tmTitle;
            case "25": return ChinaUnionPay.tmTitle;
            case "5": return Amex.tmTitle;
            case "22": return Diners.tmTitle;
            case "23": return JCB.tmTitle;
            default: return null;
        }
    }
}
