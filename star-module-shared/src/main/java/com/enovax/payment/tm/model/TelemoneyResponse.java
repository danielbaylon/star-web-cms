package com.enovax.payment.tm.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TelemoneyResponse implements Serializable {

    public static final String TM_ApprovalCode = "TM_ApprovalCode";
    public static final String TM_BankRespCode = "TM_BankRespCode";
    public static final String TM_Currency = "TM_Currency";
    public static final String TM_DebitAmt = "TM_DebitAmt";
    public static final String TM_Error = "TM_Error";
    public static final String TM_ErrorMsg = "TM_ErrorMsg";
    public static final String TM_ExpiryDate = "TM_ExpiryDate";
    public static final String TM_MCode = "TM_MCode";
    public static final String TM_PaymentType = "TM_PaymentType";
    public static final String TM_RecurrentId = "TM_RecurrentId";
    public static final String TM_RefNo = "TM_RefNo";
    public static final String TM_Status = "TM_Status";
    public static final String TM_SubSequentMCode = "TM_SubSequentMCode";
    public static final String TM_SubTrnType = "TM_SubTrnType";
    public static final String TM_TrnType = "TM_TrnType";
    public static final String TM_UserField1 = "TM_UserField1";
    public static final String TM_UserField2 = "TM_UserField2";
    public static final String TM_UserField3 = "TM_UserField3";
    public static final String TM_UserField4 = "TM_UserField4";
    public static final String TM_UserField5 = "TM_UserField5";
    public static final String TM_CCNum = "TM_CCNum";

    public static final String TM_ECI = "TM_ECI";
    public static final String TM_CAVV = "TM_CAVV";
    public static final String TM_XID = "TM_XID";

    public static final String TM_Original_RefNo =  "TM_Original_RefNo";
    public static final String TM_Signature = "TM_Signature";
    public static final String TM_OriginalPayType = "TM_OriginalPayType";

    private String tmMerchantId;
    private String tmRefNo;
    private String tmCurrency;
    private BigDecimal tmDebitAmt;
    private String tmDebitAmtText = "";
    private String tmStatus;
    private String tmErrorMessage;
    private String tmError;
    private String tmPaymentType;
    private String tmApprovalCode;
    private String tmBankRespCode;
    private String tmUserField1;
    private String tmUserField2;
    private String tmUserField3;
    private String tmUserField4;
    private String tmUserField5;
    private String tmTransType;
    private String tmSubTransType;
    private String tmExpiryDate;
    private String tmRecurrentId;
    private String tmSubsequentMid;
    private String tmCcNum;
    private String tmEci;
    private String tmCavv;
    private String tmXid;
    private String originalRefNo;
    private String signature;
    private String originalPayType;


    private boolean isQuery = false;
    
    public static TelemoneyResponse fromUrlParams(Map<String, String> params, boolean isQuery) {
        final TelemoneyResponse response = new TelemoneyResponse();
        response.setIsQuery(isQuery);

        response.setTmApprovalCode(params.get(TM_ApprovalCode));
        response.setTmBankRespCode(params.get(TM_BankRespCode));
        response.setTmCurrency(params.get(TM_Currency));

        final String debitAmtText = params.get(TM_DebitAmt);
        try {
            if (!StringUtils.isEmpty(debitAmtText)) {
                response.setTmDebitAmt(new BigDecimal(debitAmtText));
                response.setTmDebitAmtText(debitAmtText);
            }
        } catch (Exception e) {
            response.setTmDebitAmtText("");
        }

        response.setTmError(params.get(TM_Error));
        response.setTmErrorMessage(params.get(TM_ErrorMsg));
        response.setTmExpiryDate(params.get(TM_ExpiryDate));
        response.setTmMerchantId(params.get(TM_MCode));
        response.setTmPaymentType(params.get(TM_PaymentType));
        response.setTmRecurrentId(params.get(TM_RecurrentId));
        response.setTmRefNo(params.get(TM_RefNo));
        response.setTmStatus(params.get(TM_Status));
        response.setTmSubsequentMid(params.get(TM_SubSequentMCode));
        response.setTmSubTransType(params.get(TM_SubTrnType));
        response.setTmTransType(params.get(TM_TrnType));
        response.setTmUserField1(params.get(TM_UserField1));
        response.setTmUserField2(params.get(TM_UserField2));
        response.setTmUserField3(params.get(TM_UserField3));
        response.setTmUserField4(params.get(TM_UserField4));
        response.setTmUserField5(params.get(TM_UserField5));
        response.setTmCcNum(params.get(TM_CCNum));
        response.setTmEci(params.get(TM_ECI));
        response.setTmCavv(params.get(TM_CAVV));
        response.setTmXid(params.get(TM_XID));
        response.setSignature(params.get(TM_Signature));
        response.setOriginalPayType(params.get(TM_OriginalPayType));
        response.setOriginalRefNo(params.get(TM_Original_RefNo));
        
        return response;
    }

    public static TelemoneyResponse fromUrlParams(String urlQueryString, boolean isQuery) {
        final String[] params = urlQueryString.split("&", -1);
        
        final Map<String, String> paramMap = new HashMap<>();
        
        for (String param : params) {
            String[] p = param.split("=", -1);
            String key = p[0];
            String value = p[1];
            
            paramMap.put(key, value);
        }
        
        return fromUrlParams(paramMap, isQuery);
    }


    public TelemoneyResponse() {

    }

    public TelemoneyResponse(String tmMerchantId, String tmRefNo, String tmCurrency, BigDecimal tmDebitAmt,
                             String tmStatus, String tmErrorMessage, String tmError, String tmPaymentType,
                             String tmApprovalCode, String tmBankRespCode, String tmUserField1, String tmUserField2,
                             String tmUserField3, String tmUserField4, String tmUserField5, String tmTransType,
                             String tmSubTransType, String tmExpiryDate, String tmRecurrentId, String tmSubsequentMid,
                             String tmCcNum, String tmEci, String tmCavv, String tmXid, String originalRefNo, String signature, String originalPayType, boolean isQuery) {
        this.tmMerchantId = tmMerchantId;
        this.tmRefNo = tmRefNo;
        this.tmCurrency = tmCurrency;
        this.tmDebitAmt = tmDebitAmt;
        this.tmStatus = tmStatus;
        this.tmErrorMessage = tmErrorMessage;
        this.tmError = tmError;
        this.tmPaymentType = tmPaymentType;
        this.tmApprovalCode = tmApprovalCode;
        this.tmBankRespCode = tmBankRespCode;
        this.tmUserField1 = tmUserField1;
        this.tmUserField2 = tmUserField2;
        this.tmUserField3 = tmUserField3;
        this.tmUserField4 = tmUserField4;
        this.tmUserField5 = tmUserField5;
        this.tmTransType = tmTransType;
        this.tmSubTransType = tmSubTransType;
        this.tmExpiryDate = tmExpiryDate;
        this.tmRecurrentId = tmRecurrentId;
        this.tmSubsequentMid = tmSubsequentMid;
        this.tmCcNum = tmCcNum;
        this.tmEci = tmEci;
        this.tmCavv = tmCavv;
        this.tmXid = tmXid;
        this.originalRefNo = originalRefNo;
        this.signature = signature;
        this.originalPayType = originalPayType;
        this.isQuery = isQuery;
    }

    public String getTmMerchantId() {
        return tmMerchantId;
    }

    public void setTmMerchantId(String tmMerchantId) {
        this.tmMerchantId = tmMerchantId;
    }

    public String getTmRefNo() {
        return tmRefNo;
    }

    public void setTmRefNo(String tmRefNo) {
        this.tmRefNo = tmRefNo;
    }

    public String getTmCurrency() {
        return tmCurrency;
    }

    public void setTmCurrency(String tmCurrency) {
        this.tmCurrency = tmCurrency;
    }

    public BigDecimal getTmDebitAmt() {
        return tmDebitAmt;
    }

    public void setTmDebitAmt(BigDecimal tmDebitAmt) {
        this.tmDebitAmt = tmDebitAmt;
    }

    public String getTmStatus() {
        return tmStatus;
    }

    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }

    public String getTmErrorMessage() {
        return tmErrorMessage;
    }

    public void setTmErrorMessage(String tmErrorMessage) {
        this.tmErrorMessage = tmErrorMessage;
    }

    public String getTmError() {
        return tmError;
    }

    public void setTmError(String tmError) {
        this.tmError = tmError;
    }

    public String getTmPaymentType() {
        return tmPaymentType;
    }

    public void setTmPaymentType(String tmPaymentType) {
        this.tmPaymentType = tmPaymentType;
    }

    public String getTmApprovalCode() {
        return tmApprovalCode;
    }

    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }

    public String getTmBankRespCode() {
        return tmBankRespCode;
    }

    public void setTmBankRespCode(String tmBankRespCode) {
        this.tmBankRespCode = tmBankRespCode;
    }

    public String getTmUserField1() {
        return tmUserField1;
    }

    public void setTmUserField1(String tmUserField1) {
        this.tmUserField1 = tmUserField1;
    }

    public String getTmUserField2() {
        return tmUserField2;
    }

    public void setTmUserField2(String tmUserField2) {
        this.tmUserField2 = tmUserField2;
    }

    public String getTmUserField3() {
        return tmUserField3;
    }

    public void setTmUserField3(String tmUserField3) {
        this.tmUserField3 = tmUserField3;
    }

    public String getTmUserField4() {
        return tmUserField4;
    }

    public void setTmUserField4(String tmUserField4) {
        this.tmUserField4 = tmUserField4;
    }

    public String getTmUserField5() {
        return tmUserField5;
    }

    public void setTmUserField5(String tmUserField5) {
        this.tmUserField5 = tmUserField5;
    }

    public String getTmTransType() {
        return tmTransType;
    }

    public void setTmTransType(String tmTransType) {
        this.tmTransType = tmTransType;
    }

    public String getTmSubTransType() {
        return tmSubTransType;
    }

    public void setTmSubTransType(String tmSubTransType) {
        this.tmSubTransType = tmSubTransType;
    }

    public String getTmExpiryDate() {
        return tmExpiryDate;
    }

    public void setTmExpiryDate(String tmExpiryDate) {
        this.tmExpiryDate = tmExpiryDate;
    }

    public String getTmRecurrentId() {
        return tmRecurrentId;
    }

    public void setTmRecurrentId(String tmRecurrentId) {
        this.tmRecurrentId = tmRecurrentId;
    }

    public String getTmSubsequentMid() {
        return tmSubsequentMid;
    }

    public void setTmSubsequentMid(String tmSubsequentMid) {
        this.tmSubsequentMid = tmSubsequentMid;
    }

    public String getTmCcNum() {
        return tmCcNum;
    }

    public void setTmCcNum(String tmCcNum) {
        this.tmCcNum = tmCcNum;
    }

    public String getTmEci() {
        return tmEci;
    }

    public void setTmEci(String tmEci) {
        this.tmEci = tmEci;
    }

    public String getTmCavv() {
        return tmCavv;
    }

    public void setTmCavv(String tmCavv) {
        this.tmCavv = tmCavv;
    }

    public String getTmXid() {
        return tmXid;
    }

    public void setTmXid(String tmXid) {
        this.tmXid = tmXid;
    }

    public boolean isQuery() {
        return isQuery;
    }

    public void setIsQuery(boolean isQuery) {
        this.isQuery = isQuery;
    }

    public String getTmDebitAmtText() {
        return tmDebitAmtText;
    }

    public void setTmDebitAmtText(String tmDebitAmtText) {
        this.tmDebitAmtText = tmDebitAmtText;
    }

    public String getOriginalRefNo() {
        return originalRefNo;
    }

    public void setOriginalRefNo(String originalRefNo) {
        this.originalRefNo = originalRefNo;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getOriginalPayType() {
        return originalPayType;
    }

    public void setOriginalPayType(String originalPayType) {
        this.originalPayType = originalPayType;
    }
}
