package com.enovax.payment.tm.constant;

/**
 * Created by jensen on 11/9/15.
 */
public enum TmErrorCode {
    UserCancelled("9203");

    public final String tmCode;

    TmErrorCode(String tmCode) {
        this.tmCode = tmCode;
    }
}
