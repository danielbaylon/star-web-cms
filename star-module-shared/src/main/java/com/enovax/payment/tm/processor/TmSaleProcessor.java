package com.enovax.payment.tm.processor;

import com.enovax.payment.tm.constant.TmStatus;
import com.enovax.payment.tm.constant.TmTransType;
import com.enovax.payment.tm.model.TelemoneyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class TmSaleProcessor<T> {

    protected Logger log = LoggerFactory.getLogger(getClass());

    protected abstract void onTelemoneyTransactionSuccessful(TelemoneyResponse tmResponse, T data);

    protected abstract void onTelemoneyTransactionNotFound(TelemoneyResponse tmResponse, T data);

    protected abstract void onTelemoneyTransactionFailed(TelemoneyResponse tmResponse, T data);

    protected abstract void onTelemoneyTransactionOtherState(TelemoneyResponse tmResponse, T data);

    protected void processTelemoneySalePost(TelemoneyResponse tmResponse, T data) {
        final String tmStatus = tmResponse.getTmStatus();
        if (TmStatus.Success.tmCode.equalsIgnoreCase(tmStatus)) {
            //Transaction successfully processed in Telemoney.
            onTelemoneyTransactionSuccessful(tmResponse, data);

        } else if (TmStatus.NotFound.tmCode.equalsIgnoreCase(tmStatus)) {
            //Transaction not found in Telemoney.
            onTelemoneyTransactionNotFound(tmResponse, data);

        } else if (TmStatus.Failed.tmCode.equalsIgnoreCase(tmStatus)) {
            //Transaction failed in Telemoney.
            onTelemoneyTransactionFailed(tmResponse, data);

        } else {
            //Transaction is in an in-progress state, or some totally different state. Usually no handling is required.
            onTelemoneyTransactionOtherState(tmResponse, data);
        }
    }
}
