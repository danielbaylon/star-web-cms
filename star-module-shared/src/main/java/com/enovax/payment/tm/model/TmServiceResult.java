package com.enovax.payment.tm.model;

public class TmServiceResult {
    private boolean success = false;
    private String errorMessage = "";
    private String errorStacktrace = "";
    private TelemoneyResponse tmResponse = null;

    public TmServiceResult() {

    }

    public TmServiceResult(boolean success) {
        this.success = success;
    }

    public TmServiceResult(TelemoneyResponse tmResponse) {
        this.success = true;
        this.tmResponse = tmResponse;
    }

    public TmServiceResult(boolean success, String errorMessage, String errorStacktrace) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.errorStacktrace = errorStacktrace;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStacktrace() {
        return errorStacktrace;
    }

    public void setErrorStacktrace(String errorStacktrace) {
        this.errorStacktrace = errorStacktrace;
    }

    public TelemoneyResponse getTmResponse() {
        return tmResponse;
    }

    public void setTmResponse(TelemoneyResponse tmResponse) {
        this.tmResponse = tmResponse;
    }
}
