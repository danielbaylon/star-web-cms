/*
 Navicat Premium Data Transfer

 Source Server         : STAR Staging
 Source Server Type    : SQL Server
 Source Server Version : 10501600
 Source Host           : 192.168.0.92
 Source Database       : STAR_DB
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 10501600
 File Encoding         : utf-8

 Date: 09/25/2016 22:23:30 PM
*/

-- ----------------------------
--  Table structure for B2CSLMAxCheckoutCart
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[B2CSLMAxCheckoutCart]') AND type IN ('U'))
	DROP TABLE [dbo].[B2CSLMAxCheckoutCart]
GO
CREATE TABLE [dbo].[B2CSLMAxCheckoutCart] (
	[id] int IDENTITY(1,1) NOT NULL,
	[transactionId] int NOT NULL,
	[cartJson] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for B2CSLMAxSalesOrder
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[B2CSLMAxSalesOrder]') AND type IN ('U'))
	DROP TABLE [dbo].[B2CSLMAxSalesOrder]
GO
CREATE TABLE [dbo].[B2CSLMAxSalesOrder] (
	[id] int IDENTITY(1,1) NOT NULL,
	[transactionId] int NOT NULL,
	[salesOrderId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[salesOrderJson] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for B2CSLMStoreTransaction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[B2CSLMStoreTransaction]') AND type IN ('U'))
	DROP TABLE [dbo].[B2CSLMStoreTransaction]
GO
CREATE TABLE [dbo].[B2CSLMStoreTransaction] (
	[id] int IDENTITY(1,1) NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[custName] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custEmail] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custIdType] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custIdNo] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custMobile] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custSubscribe] bit NULL DEFAULT ((0)),
	[custNationality] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custReferSource] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custDob] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custSalutation] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custCompanyName] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custJewelCard] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[totalAmount] money NOT NULL,
	[currency] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[tmMerchantId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[stage] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[tmStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorMessage] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmApprovalCode] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[statusDate] datetime NULL,
	[createdDate] datetime NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[remarks] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[trafficSource] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ip] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[maskedCc] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentType] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[langCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT ('en'),
	[ticketMedia] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[pinCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[promoCodes] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[affiliations] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for B2CSLMStoreTransactionItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[B2CSLMStoreTransactionItem]') AND type IN ('U'))
	DROP TABLE [dbo].[B2CSLMStoreTransactionItem]
GO
CREATE TABLE [dbo].[B2CSLMStoreTransactionItem] (
	[id] int IDENTITY(1,1) NOT NULL,
	[transactionId] int NOT NULL,
	[cmsProductId] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[cmsProductName] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemListingId] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemProductCode] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[transItemType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[displayName] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[displayDetails] varchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ticketType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[baseUnitPrice] money NOT NULL,
	[actualUnitPrice] money NOT NULL,
	[baseQty] int NOT NULL,
	[qty] int NOT NULL,
	[subTotal] money NOT NULL,
	[cmsProductNameByLang] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[displayNameByLang] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[displayDetailsByLang] varchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[dateOfVisit] datetime NULL,
	[eventGroupId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eventLineId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[otherDetails] varchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[mainItemCmsProductId] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[mainItemListingId] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[promoCodes] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[affiliations] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMApprovalLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMApprovalLog]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMApprovalLog]
GO
CREATE TABLE [dbo].[PPSLMApprovalLog] (
	[id] int IDENTITY(1,1) NOT NULL,
	[category] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[relatedKey] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[currentValue] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[previousValue] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[description] nvarchar(1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[approverType] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[approver] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reviewDate] datetime NULL,
	[remarks] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[actionType] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[parentLogId] int NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMApprover
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMApprover]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMApprover]
GO
CREATE TABLE [dbo].[PPSLMApprover] (
	[id] int IDENTITY(1,1) NOT NULL,
	[adminId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[category] nvarchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[startDate] datetime NULL,
	[endDate] datetime NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMAuditTrail
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMAuditTrail]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMAuditTrail]
GO
CREATE TABLE [dbo].[PPSLMAuditTrail] (
	[id] int IDENTITY(1,1) NOT NULL,
	[action] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[details] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[relatedEntities] varchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[relatedEntityKeys] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[performedBy] varchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMAxCheckoutCart
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMAxCheckoutCart]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMAxCheckoutCart]
GO
CREATE TABLE [dbo].[PPSLMAxCheckoutCart] (
	[id] int IDENTITY(1,1) NOT NULL,
	[transactionId] int NULL,
	[checkoutCartJson] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMAxSalesOrder
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMAxSalesOrder]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMAxSalesOrder]
GO
CREATE TABLE [dbo].[PPSLMAxSalesOrder] (
	[customerId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[salesOrderId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[transactionId] int NOT NULL,
	[salesOrderJson] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[id] int IDENTITY(1,1) NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMAxSalesOrderLineNumberQuantity
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMAxSalesOrderLineNumberQuantity]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMAxSalesOrderLineNumberQuantity]
GO
CREATE TABLE [dbo].[PPSLMAxSalesOrderLineNumberQuantity] (
	[id] int IDENTITY(1,1) NOT NULL,
	[salesOrderId] int NOT NULL,
	[lineNumber] int NOT NULL,
	[qty] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMCountry
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMCountry]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMCountry]
GO
CREATE TABLE [dbo].[PPSLMCountry] (
	[id] int IDENTITY(1,1) NOT NULL,
	[ctyCode] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ctyName] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdBy] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdDate] datetime NULL,
	[modifiedBy] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[modifiedDate] datetime NULL,
	[isoCode] nvarchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isoRegionCode] nvarchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMDepositTransaction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMDepositTransaction]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMDepositTransaction]
GO
CREATE TABLE [dbo].[PPSLMDepositTransaction] (
	[id] int IDENTITY(1,1) NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[totalAmount] money NOT NULL,
	[mainAccountId] int NOT NULL,
	[currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmMerchantId] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmStatus] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmStatusDate] datetime NULL,
	[tmApprovalCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorMessage] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorCode] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentType] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdDate] datetime NULL,
	[ip] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[markedCc] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[location] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[gstRate] decimal(3,2) NULL,
	[modifiedDate] datetime NULL,
	[username] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[maskedCc] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isSubAccountTrans] tinyint NULL,
	[transType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMETicket
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMETicket]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMETicket]
GO
CREATE TABLE [dbo].[PPSLMETicket] (
	[id] int IDENTITY(1,1) NOT NULL,
	[packageId] int NOT NULL,
	[ticketNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[displayName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ticketPersonType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[validityStartDate] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[validityEndDate] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[totalPrice] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[shortDisplayName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eventDate] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eventSession] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eTicketData] varchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMInventoryTransaction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMInventoryTransaction]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMInventoryTransaction]
GO
CREATE TABLE [dbo].[PPSLMInventoryTransaction] (
	[id] int IDENTITY(1,1) NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[mainAccountId] int NOT NULL,
	[username] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[isSubAccountTrans] bit NOT NULL,
	[totalAmount] money NOT NULL,
	[currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[tmMerchantId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[tmStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmStatusDate] datetime NULL,
	[tmApprovalCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorMessage] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ip] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[maskedCc] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reprintCount] int NOT NULL,
	[validityStartDate] datetime NULL,
	[validityEndDate] datetime NOT NULL,
	[revalidated] bit NOT NULL,
	[ticketGenerated] bit NOT NULL,
	[gstRate] decimal(3,2) NULL,
	[createdDate] datetime NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMInventoryTransactionItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMInventoryTransactionItem]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMInventoryTransactionItem]
GO
CREATE TABLE [dbo].[PPSLMInventoryTransactionItem] (
	[id] int IDENTITY(1,1) NOT NULL,
	[transactionId] int NOT NULL,
	[productId] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[productName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemListingId] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemProductCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[transItemType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[displayName] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[displayDetails] varchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[dateOfVisit] datetime NULL,
	[eventGroupId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eventLineId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eventName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ticketType] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemType] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[unitPrice] money NOT NULL,
	[baseQty] int NOT NULL,
	[qty] int NOT NULL,
	[subTotal] money NOT NULL,
	[otherDetails] varchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[unpackagedQty] int NOT NULL,
	[mainItemProductId] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[mainItemListingId] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[validityStartDate] datetime NULL,
	[validityEndDate] datetime NULL,
	[validityDateType] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMMixMatchPackage
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMMixMatchPackage]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMMixMatchPackage]
GO
CREATE TABLE [dbo].[PPSLMMixMatchPackage] (
	[id] int IDENTITY(1,1) NOT NULL,
	[mainAccountId] int NOT NULL,
	[username] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[isSubAccountTrans] bit NOT NULL,
	[name] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[description] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[qty] int NOT NULL,
	[ticketMedia] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[expiryDate] datetime NOT NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[qtyRedeemed] int NOT NULL,
	[lastRedemptionDate] datetime NULL,
	[lastCheckDate] datetime NULL,
	[dateOfVisit] datetime NULL,
	[createdDate] datetime NOT NULL,
	[pinCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ticketGeneratedDate] datetime NULL,
	[pinRequestId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMMixMatchPackageItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMMixMatchPackageItem]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMMixMatchPackageItem]
GO
CREATE TABLE [dbo].[PPSLMMixMatchPackageItem] (
	[id] int IDENTITY(1,1) NOT NULL,
	[packageId] int NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[transactionItemId] int NOT NULL,
	[displayName] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[displayDetails] nvarchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[dateOfVisit] datetime NULL,
	[itemListingId] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemProductCode] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[qty] int NOT NULL,
	[transItemType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ticketType] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[itemType] varchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMOfflinePaymentRequest
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMOfflinePaymentRequest]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMOfflinePaymentRequest]
GO
CREATE TABLE [dbo].[PPSLMOfflinePaymentRequest] (
	[id] int IDENTITY(1,1) NOT NULL,
	[transactionId] int NOT NULL,
	[paymentType] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[referenceNum] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[bankName] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[bankCountry] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[detail] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[approverRemarks] nvarchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[remarks] nvarchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[approver] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[approveDate] datetime NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[submitDate] date NULL,
	[submitBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPADistributionMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPADistributionMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPADistributionMapping]
GO
CREATE TABLE [dbo].[PPSLMPADistributionMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[partnerId] int NOT NULL,
	[adminId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[countryId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[percentage] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPADocMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPADocMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPADocMapping]
GO
CREATE TABLE [dbo].[PPSLMPADocMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[partnerId] int NOT NULL,
	[docId] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPADocument
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPADocument]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPADocument]
GO
CREATE TABLE [dbo].[PPSLMPADocument] (
	[id] int IDENTITY(1,1) NOT NULL,
	[filePath] nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[fileName] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[fileType] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartner
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartner]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartner]
GO
CREATE TABLE [dbo].[PPSLMPartner] (
	[id] int IDENTITY(1,1) NOT NULL,
	[orgName] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[accountCode] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[adminId] int NULL,
	[orgType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[country] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[branchName] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[uen] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[licenseNum] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[revalFeeItemId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[revalPeriodMonths] int NULL,
	[dailyTransCap] money NULL,
	[remarks] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[accountManagerId] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[registrationYear] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[contactPerson] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[contactDesignation] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[address] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[postalCode] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[city] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[telNum] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[mobileNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[faxNum] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[email] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[website] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[languagePreference] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[mainDestinations] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[offlinePaymentEnabled] bit NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[tierId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axAccountNumber] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[licenseExpDate] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartnerDocument
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartnerDocument]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartnerDocument]
GO
CREATE TABLE [dbo].[PPSLMPartnerDocument] (
	[id] int IDENTITY(1,1) NOT NULL,
	[filePath] nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[fileName] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[fileType] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartnerDocumentMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartnerDocumentMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartnerDocumentMapping]
GO
CREATE TABLE [dbo].[PPSLMPartnerDocumentMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[partnerId] int NOT NULL,
	[docId] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartnerExclusiveMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartnerExclusiveMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartnerExclusiveMapping]
GO
CREATE TABLE [dbo].[PPSLMPartnerExclusiveMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[partnerId] int NOT NULL,
	[productId] int NOT NULL,
	[isActive] bit NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartnerExt
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartnerExt]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartnerExt]
GO
CREATE TABLE [dbo].[PPSLMPartnerExt] (
	[id] int IDENTITY(1,1) NOT NULL,
	[partnerId] int NOT NULL,
	[attrName] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[attrValue] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[description] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] char(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[approvalRequired] tinyint NULL,
	[attrValueType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[attrValueFormat] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[mandatoryInd] tinyint NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartnerTransactionHist
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartnerTransactionHist]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartnerTransactionHist]
GO
CREATE TABLE [dbo].[PPSLMPartnerTransactionHist] (
	[id] int IDENTITY(1,1) NOT NULL,
	[amountCur] money NULL,
	[currencyCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[documentNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[invoice] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[orderAccount] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentReference] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[transDate] datetime NULL,
	[transType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[voucher] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMPartnerType
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMPartnerType]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMPartnerType]
GO
CREATE TABLE [dbo].[PPSLMPartnerType] (
	[id] int IDENTITY(1,1) NOT NULL,
	[label] nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[description] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[code] nvarchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMReason
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMReason]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMReason]
GO
CREATE TABLE [dbo].[PPSLMReason] (
	[id] int IDENTITY(1,1) NOT NULL,
	[title] nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[content] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMReasonLogMap
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMReasonLogMap]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMReasonLogMap]
GO
CREATE TABLE [dbo].[PPSLMReasonLogMap] (
	[id] int IDENTITY(1,1) NOT NULL,
	[reasonId] int NOT NULL,
	[logId] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMRefreshHist
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMRefreshHist]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMRefreshHist]
GO
CREATE TABLE [dbo].[PPSLMRefreshHist] (
	[id] int IDENTITY(1,1) NOT NULL,
	[key1] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[key2] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[key3] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[value] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMRegion
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMRegion]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMRegion]
GO
CREATE TABLE [dbo].[PPSLMRegion] (
	[id] int IDENTITY(1,1) NOT NULL,
	[regionName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[description] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMRegionMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMRegionMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMRegionMapping]
GO
CREATE TABLE [dbo].[PPSLMRegionMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[regionId] int NOT NULL,
	[ctyId] int NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMRevalidationFeeItem
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMRevalidationFeeItem]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMRevalidationFeeItem]
GO
CREATE TABLE [dbo].[PPSLMRevalidationFeeItem] (
	[id] int IDENTITY(1,1) NOT NULL,
	[itemCode] int NOT NULL,
	[price] money NOT NULL,
	[description] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT ('A'),
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT ('system'),
	[createdDate] datetime NOT NULL DEFAULT (getdate()),
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL DEFAULT ('system'),
	[modifiedDate] datetime NOT NULL DEFAULT (getdate())
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMRevalidationTransaction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMRevalidationTransaction]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMRevalidationTransaction]
GO
CREATE TABLE [dbo].[PPSLMRevalidationTransaction] (
	[id] int IDENTITY(1,1) NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[transactionId] int NOT NULL,
	[username] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[mainAccountId] int NOT NULL,
	[totalMainQty] int NOT NULL,
	[totalTopupQty] int NOT NULL,
	[revalItemCode] int NOT NULL,
	[revalFeeInCents] int NOT NULL,
	[revalTopupItemCode] int NOT NULL,
	[revalTopupFeeInCents] int NOT NULL,
	[total] money NOT NULL,
	[oldValidityEndDate] datetime NOT NULL,
	[newValidityEndDate] datetime NOT NULL,
	[createdDate] datetime NOT NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[tmStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmStatusDate] datetime NULL,
	[tmApprovalCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorMessage] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[sactResponse] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reprintCount] int NOT NULL DEFAULT ((0)),
	[isSubAccountTrans] bit NOT NULL,
	[tmMerchantId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[gstRate] decimal(3,2) NULL,
	[revalDetail] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMSequenceLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMSequenceLog]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMSequenceLog]
GO
CREATE TABLE [dbo].[PPSLMSequenceLog] (
	[seqId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMSessionInfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMSessionInfo]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMSessionInfo]
GO
CREATE TABLE [dbo].[PPSLMSessionInfo] (
	[id] int IDENTITY(1,1) NOT NULL,
	[sid] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[request] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[referrer] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[userAgent] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ip] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[firstAccessDate] datetime NOT NULL,
	[transactionId] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[rcmdViews] varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[fromDecide] bit NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMSessionTrace
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMSessionTrace]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMSessionTrace]
GO
CREATE TABLE [dbo].[PPSLMSessionTrace] (
	[id] int IDENTITY(1,1) NOT NULL,
	[sessionInfoId] int NOT NULL,
	[traceDate] datetime NOT NULL,
	[traceLabel] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[traceDesc] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[request] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAAccessRight
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAAccessRight]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAAccessRight]
GO
CREATE TABLE [dbo].[PPSLMTAAccessRight] (
	[id] int IDENTITY(1,1) NOT NULL,
	[topNavName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[description] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAAccessRightsGroup
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAAccessRightsGroup]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAAccessRightsGroup]
GO
CREATE TABLE [dbo].[PPSLMTAAccessRightsGroup] (
	[id] int IDENTITY(1,1) NOT NULL,
	[name] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[description] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[type] nchar(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAAccessRightsGroupMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAAccessRightsGroupMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAAccessRightsGroupMapping]
GO
CREATE TABLE [dbo].[PPSLMTAAccessRightsGroupMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[accessRightId] int NOT NULL,
	[accessRightsGroupId] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAMainAccount
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAMainAccount]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAMainAccount]
GO
CREATE TABLE [dbo].[PPSLMTAMainAccount] (
	[id] int IDENTITY(1,1) NOT NULL,
	[accountCode] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[username] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[name] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[password] varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[salt] varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[pwdLastModDt] datetime NOT NULL,
	[pwdAttempts] int NOT NULL,
	[pwdForceChange] bit NOT NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[subAccountEnabled] bit NOT NULL,
	[email] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAMainRightsMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAMainRightsMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAMainRightsMapping]
GO
CREATE TABLE [dbo].[PPSLMTAMainRightsMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[mainAccountId] int NOT NULL,
	[accessRightsGroupId] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAPrevPasswordMain
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAPrevPasswordMain]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAPrevPasswordMain]
GO
CREATE TABLE [dbo].[PPSLMTAPrevPasswordMain] (
	[id] int IDENTITY(1,1) NOT NULL,
	[mainAccountId] int NOT NULL,
	[password] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[salt] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTAPrevPasswordSub
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTAPrevPasswordSub]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTAPrevPasswordSub]
GO
CREATE TABLE [dbo].[PPSLMTAPrevPasswordSub] (
	[id] int IDENTITY(1,1) NOT NULL,
	[subAccountId] int NOT NULL,
	[password] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[salt] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTARightsMapping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTARightsMapping]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTARightsMapping]
GO
CREATE TABLE [dbo].[PPSLMTARightsMapping] (
	[id] int IDENTITY(1,1) NOT NULL,
	[subAccountId] int NOT NULL,
	[accessRightsGroupId] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTASalesManager
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTASalesManager]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTASalesManager]
GO
CREATE TABLE [dbo].[PPSLMTASalesManager] (
	[id] int IDENTITY(1,1) NOT NULL,
	[mainAccountId] int NOT NULL,
	[salesManagerId] int NOT NULL,
	[pctMarketDist] int NOT NULL,
	[countryOfDist] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMTASubAccount
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMTASubAccount]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMTASubAccount]
GO
CREATE TABLE [dbo].[PPSLMTASubAccount] (
	[id] int IDENTITY(1,1) NOT NULL,
	[mainAccountId] int NOT NULL,
	[username] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[name] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[email] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[password] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[salt] nvarchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[pwdLastModDt] datetime NOT NULL,
	[pwdAttempts] int NOT NULL,
	[pwdForceChange] bit NOT NULL,
	[title] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMUserSession
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMUserSession]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMUserSession]
GO
CREATE TABLE [dbo].[PPSLMUserSession] (
	[id] int IDENTITY(1,1) NOT NULL,
	[sid] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[lastAccessDate] datetime NOT NULL,
	[username] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMWingsOfTimeReservation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMWingsOfTimeReservation]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMWingsOfTimeReservation]
GO
CREATE TABLE [dbo].[PPSLMWingsOfTimeReservation] (
	[id] int IDENTITY(1,1) NOT NULL,
	[mainAccountId] int NOT NULL,
	[username] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[isSubAccountTrans] bit NOT NULL,
	[receiptNum] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[pinCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[remarks] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[status] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[productId] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[eventLineId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[eventName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[eventDate] datetime NOT NULL,
	[qty] int NOT NULL,
	[qtyRedeemed] int NOT NULL,
	[qtyUnredeemed] int NOT NULL,
	[ticketPrice] money NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[itemId] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[mediaTypeId] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[eventGroupId] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[openValidityEndDate] datetime NULL,
	[openValidityStartDate] datetime NULL,
	[receiptEmail] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axCartId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axLineId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axItemTaxGroupId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axPrice] money NULL,
	[axExternalPrice] money NULL,
	[axTaxAmt] money NULL,
	[axTotalAmt] money NULL,
	[axSubtotalAmt] money NULL,
	[axNetAmtWoTax] money NULL,
	[axCheckoutCartId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtConfirmationId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtSalesOrderStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtSalesId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtRequestedDeliveryDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtOrderPlacedDate] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtChargeAmountWithCurrency] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtDeliveryModeDescription] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtDeliveryModeId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtSubtotalWithCurrency] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtTaxAmountWithCurrency] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtTotalAmountWithCurrency] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtItemId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtItemType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtLineId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtNetAmountWithCurrency] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtpPriceWithCurrency] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axrtValChannelId] bigint NULL,
	[axrtProductId] bigint NULL,
	[axrtQuantity] int NULL,
	[axrtPaymentStatus] int NULL,
	[axrtDiscountAmount] money NULL,
	[axrtTotalAmount] money NULL,
	[axDiscountAmt] money NULL,
	[axrtId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[modifiedBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axCheckoutLineId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axReferenceId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[axInventTransId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[qtyReturned] int NULL,
	[srcReservationId] int NULL,
	[eventStartTime] int NULL,
	[eventEndTime] int NULL,
	[eventCapacityId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reservationType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isAdminRequest] tinyint NULL,
	[reserveNeedsActivation] tinyint NULL,
	[reserveOriginalTicketCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reserveQtyGroup] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reserveRecId] bigint NULL,
	[reserveStartDate] datetime NULL,
	[reserveEndDate] datetime NULL,
	[reserveTicketCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reserveTicketStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reserveTransactionId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[purchaseSrcId] int NULL,
	[purchaseSrcQty] int NULL,
	[qtySold] int NULL,
	[splitSrcId] int NULL,
	[splitSrcQty] int NULL,
	[releaseNotifiedDate] datetime NULL,
	[releasedDate] datetime NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for PPSLMWingsOfTimeReservationLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[PPSLMWingsOfTimeReservationLog]') AND type IN ('U'))
	DROP TABLE [dbo].[PPSLMWingsOfTimeReservationLog]
GO
CREATE TABLE [dbo].[PPSLMWingsOfTimeReservationLog] (
	[id] int IDENTITY(1,1) NOT NULL,
	[wotReserveId] int NULL,
	[channel] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[serviceName] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[requestText] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[responseText] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[createdDateTime] datetime NULL,
	[createdBy] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[success] tinyint NULL,
	[errorCode] varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[errorMsg] text COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isSubAccountTrans] tinyint NULL,
	[isAdminRequest] tinyint NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningCart
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningCart]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningCart]
GO
CREATE TABLE [dbo].[SkyDiningCart] (
	[id] int IDENTITY(1,1) NOT NULL,
	[guestName] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[contactNumber] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[emailAddress] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[date] datetime NOT NULL,
	[time] time(7) NULL,
	[pax] int NULL,
	[packageId] int NOT NULL,
	[sessionId] int NOT NULL,
	[themeId] int NULL,
	[menuId] int NOT NULL,
	[specialRequest] nvarchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[type] int NOT NULL,
	[status] int NOT NULL,
	[webSessionId] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[discountId] int NULL,
	[jewelCard] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[jewelCardExpiry] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[promoCode] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[merchantId] int NULL,
	[priceType] int NULL,
	[discountPrice] int NULL,
	[originalPrice] int NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningCartMainCourse
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningCartMainCourse]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningCartMainCourse]
GO
CREATE TABLE [dbo].[SkyDiningCartMainCourse] (
	[id] int IDENTITY(1,1) NOT NULL,
	[cartId] int NOT NULL,
	[mainCourseId] int NOT NULL,
	[quantity] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningCartTopUp
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningCartTopUp]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningCartTopUp]
GO
CREATE TABLE [dbo].[SkyDiningCartTopUp] (
	[id] int IDENTITY(1,1) NOT NULL,
	[cartId] int NOT NULL,
	[topUpId] int NOT NULL,
	[quantity] int NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningJewelCard
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningJewelCard]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningJewelCard]
GO
CREATE TABLE [dbo].[SkyDiningJewelCard] (
	[id] int IDENTITY(1,1) NOT NULL,
	[cardNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[expiryDate] datetime NOT NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningReservation
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningReservation]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningReservation]
GO
CREATE TABLE [dbo].[SkyDiningReservation] (
	[id] int IDENTITY(1,1) NOT NULL,
	[guestName] nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[contactNumber] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[emailAddress] nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[date] datetime NOT NULL,
	[time] time(7) NOT NULL,
	[pax] int NOT NULL,
	[packageId] int NOT NULL,
	[sessionId] int NOT NULL,
	[themeId] int NULL,
	[menuId] int NOT NULL,
	[specialRequest] nvarchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[type] int NOT NULL,
	[status] int NOT NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL,
	[discountId] int NULL,
	[jewelCard] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[jewelCardExpiry] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[promoCode] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[merchantId] int NULL,
	[receiptNum] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[receivedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentDate] datetime NULL,
	[paymentMode] int NULL,
	[paidAmount] money NULL,
	[priceType] int NULL,
	[discountPrice] int NULL,
	[originalPrice] int NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningResMainCourse
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningResMainCourse]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningResMainCourse]
GO
CREATE TABLE [dbo].[SkyDiningResMainCourse] (
	[id] int IDENTITY(1,1) NOT NULL,
	[reservationId] int NOT NULL,
	[mainCourseId] int NOT NULL,
	[quantity] int NOT NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningResTopUp
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningResTopUp]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningResTopUp]
GO
CREATE TABLE [dbo].[SkyDiningResTopUp] (
	[id] int IDENTITY(1,1) NOT NULL,
	[reservationId] int NOT NULL,
	[topUpId] int NOT NULL,
	[quantity] int NOT NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningSequenceLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningSequenceLog]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningSequenceLog]
GO
CREATE TABLE [dbo].[SkyDiningSequenceLog] (
	[seqId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO

-- ----------------------------
--  Table structure for SkyDiningTransaction
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID('[dbo].[SkyDiningTransaction]') AND type IN ('U'))
	DROP TABLE [dbo].[SkyDiningTransaction]
GO
CREATE TABLE [dbo].[SkyDiningTransaction] (
	[id] int IDENTITY(1,1) NOT NULL,
	[receiptNum] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[custName] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[custSalutation] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custCompany] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custEmail] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[custIdType] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[custIdNo] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[custMobile] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[custSubscribe] bit NOT NULL,
	[custJewelCard] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[paymentType] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[totalAmount] money NOT NULL,
	[currency] varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[bookFeeType] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[bookFeeCents] int NULL,
	[bookFeeItemCode] int NULL,
	[bookFeeWaived] bit NULL,
	[bookFeePromoCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmMerchantId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[statusDate] datetime NULL,
	[errorCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[stage] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[sactResId] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[sactPin] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmStatus] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmErrorMessage] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[tmApprovalCode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[reservationId] int NULL,
	[createdBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[createdDate] datetime NOT NULL,
	[modifiedBy] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[modifiedDate] datetime NOT NULL
)
ON [PRIMARY]
GO


-- ----------------------------
--  Primary key structure for table B2CSLMAxCheckoutCart
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMAxCheckoutCart] ADD
	CONSTRAINT [PK__B2CSLMAx__3213E83F00DF2177] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table B2CSLMAxSalesOrder
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMAxSalesOrder] ADD
	CONSTRAINT [PK__B2CSLMAx__3213E83F05A3D694] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table B2CSLMStoreTransaction
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMStoreTransaction] ADD
	CONSTRAINT [PK__B2CSLMSt__3213E83F76619304] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table B2CSLMStoreTransaction
-- ----------------------------
CREATE UNIQUE NONCLUSTERED INDEX [B2CSLMStoreTransaction_receiptNum_uindex]
ON [dbo].[B2CSLMStoreTransaction] ([receiptNum] ASC)
WITH (PAD_INDEX = OFF,
	IGNORE_DUP_KEY = OFF,
	STATISTICS_NORECOMPUTE = OFF,
	SORT_IN_TEMPDB = OFF,
	ONLINE = OFF,
	ALLOW_ROW_LOCKS = ON,
	ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table B2CSLMStoreTransactionItem
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMStoreTransactionItem] ADD
	CONSTRAINT [PK__B2CSLMSt__3213E83F7C1A6C5A] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMApprovalLog
-- ----------------------------
ALTER TABLE [dbo].[PPSLMApprovalLog] ADD
	CONSTRAINT [PK__PPSLMApp__3213E83F59FA5E80] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMAuditTrail
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAuditTrail] ADD
	CONSTRAINT [PK__PPSLMAud__3213E83F5EBF139D] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMAxCheckoutCart
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxCheckoutCart] ADD
	CONSTRAINT [PK__PPSLMAxC__3213E83F51300E55] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMAxSalesOrder
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxSalesOrder] ADD
	CONSTRAINT [PK__PPSLMAxS__3213E83F498EEC8D] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMAxSalesOrderLineNumberQuantity
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxSalesOrderLineNumberQuantity] ADD
	CONSTRAINT [PK__PPSLMAxS__3213E83F58D1301D] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMCountry
-- ----------------------------
ALTER TABLE [dbo].[PPSLMCountry] ADD
	CONSTRAINT [PK__PPSLMCou__3213E83F2D27B809] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMDepositTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMDepositTransaction] ADD
	CONSTRAINT [PK__PPSLMPar__3213E83F4D5F7D71] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table PPSLMDepositTransaction
-- ----------------------------
CREATE UNIQUE NONCLUSTERED INDEX [PPSLMPartnerDepositTransaction_id_uindex]
ON [dbo].[PPSLMDepositTransaction] ([id] ASC)
WITH (PAD_INDEX = OFF,
	IGNORE_DUP_KEY = OFF,
	STATISTICS_NORECOMPUTE = OFF,
	SORT_IN_TEMPDB = OFF,
	ONLINE = OFF,
	ALLOW_ROW_LOCKS = ON,
	ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table PPSLMETicket
-- ----------------------------
ALTER TABLE [dbo].[PPSLMETicket] ADD
	CONSTRAINT [PK__PPSLMETi__3213E83F40F9A68C] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMInventoryTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMInventoryTransaction] ADD
	CONSTRAINT [PK_InventoryTransaction] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMInventoryTransactionItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMInventoryTransactionItem] ADD
	CONSTRAINT [PK_InventoryTransactionItem] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMMixMatchPackage
-- ----------------------------
ALTER TABLE [dbo].[PPSLMMixMatchPackage] ADD
	CONSTRAINT [PK_MixMatchPackage] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMMixMatchPackageItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMMixMatchPackageItem] ADD
	CONSTRAINT [PK_MixMatchPackageItem] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMOfflinePaymentRequest
-- ----------------------------
ALTER TABLE [dbo].[PPSLMOfflinePaymentRequest] ADD
	CONSTRAINT [PK_PPSLMOfflinePaymentRequest] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPADocument
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPADocument] ADD
	CONSTRAINT [PK__PPSLMPAD__3213E83F68487DD7] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartner
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartner] ADD
	CONSTRAINT [PK_TB_PARTNER	] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartnerDocument
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerDocument] ADD
	CONSTRAINT [PK_TB_DOCUMENT	] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartnerDocumentMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerDocumentMapping] ADD
	CONSTRAINT [PK__PartnerD__3213E83F0CBAE877] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartnerExclusiveMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerExclusiveMapping] ADD
	CONSTRAINT [PK__PPSLMPar__3213E83F6C190EBB] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartnerExt
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerExt] ADD
	CONSTRAINT [PPSLMPartnerExt_id_pk] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartnerTransactionHist
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerTransactionHist] ADD
	CONSTRAINT [PK_PPSLMPartnerTransactionHist] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMPartnerType
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerType] ADD
	CONSTRAINT [PK__PPSLMPar__3213E83F29572725] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMRefreshHist
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRefreshHist] ADD
	CONSTRAINT [PK_PPSLMRefreshHist] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMRegion
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRegion] ADD
	CONSTRAINT [PK__PPSLMReg__3213E83F34C8D9D1] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMRegionMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRegionMapping] ADD
	CONSTRAINT [PK__PPSLMReg__3213E83F38996AB5] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMRevalidationFeeItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRevalidationFeeItem] ADD
	CONSTRAINT [PK__PPSLMRev__3213E83F2FCF1A8A] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMRevalidationTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRevalidationTransaction] ADD
	CONSTRAINT [PK_RevalidationTransaction] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMSequenceLog
-- ----------------------------
ALTER TABLE [dbo].[PPSLMSequenceLog] ADD
	CONSTRAINT [PK__PPSLMSeq__DE10A5DB2645B050] PRIMARY KEY CLUSTERED ([seqId]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMSessionInfo
-- ----------------------------
ALTER TABLE [dbo].[PPSLMSessionInfo] ADD
	CONSTRAINT [PK__PPSLMSes__3213E83F778AC167] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMSessionTrace
-- ----------------------------
ALTER TABLE [dbo].[PPSLMSessionTrace] ADD
	CONSTRAINT [PK__PPSLMSes__3213E83F7B5B524B] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAAccessRight
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAAccessRight] ADD
	CONSTRAINT [PK__PPSLMTAA__3213E83F7F2BE32F] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAAccessRightsGroup
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAAccessRightsGroup] ADD
	CONSTRAINT [PK__PPSLMTAA__3213E83F02FC7413] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAAccessRightsGroupMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAAccessRightsGroupMapping] ADD
	CONSTRAINT [PK__PPSLMTAA__3213E83F06CD04F7] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAMainAccount
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAMainAccount] ADD
	CONSTRAINT [PK__TAMainAc__3213E83F060DEAE8] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAMainRightsMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAMainRightsMapping] ADD
	CONSTRAINT [PK__PPSLMTAM__3213E83F0A9D95DB] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAPrevPasswordMain
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAPrevPasswordMain] ADD
	CONSTRAINT [PK__PPSLMTAP__3213E83F0E6E26BF] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTAPrevPasswordSub
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAPrevPasswordSub] ADD
	CONSTRAINT [PK__PPSLMTAP__3213E83F123EB7A3] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTARightsMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTARightsMapping] ADD
	CONSTRAINT [PK__PPSLMTAR__3213E83F160F4887] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTASalesManager
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTASalesManager] ADD
	CONSTRAINT [PK__PPSLMTAS__3213E83F19DFD96B] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMTASubAccount
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTASubAccount] ADD
	CONSTRAINT [PK_TASubAccount] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMUserSession
-- ----------------------------
ALTER TABLE [dbo].[PPSLMUserSession] ADD
	CONSTRAINT [PK_UserSession] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMWingsOfTimeReservation
-- ----------------------------
ALTER TABLE [dbo].[PPSLMWingsOfTimeReservation] ADD
	CONSTRAINT [PK__WingsOfT__3213E83F21B6055D] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table PPSLMWingsOfTimeReservationLog
-- ----------------------------
ALTER TABLE [dbo].[PPSLMWingsOfTimeReservationLog] ADD
	CONSTRAINT [PK__PPSLMWin__3213E83F6EC0713C] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Indexes structure for table PPSLMWingsOfTimeReservationLog
-- ----------------------------
CREATE UNIQUE NONCLUSTERED INDEX [PPSLMWingsOfTimeReservationLog_id_uindex]
ON [dbo].[PPSLMWingsOfTimeReservationLog] ([id] ASC)
WITH (PAD_INDEX = OFF,
	IGNORE_DUP_KEY = OFF,
	STATISTICS_NORECOMPUTE = OFF,
	SORT_IN_TEMPDB = OFF,
	ONLINE = OFF,
	ALLOW_ROW_LOCKS = ON,
	ALLOW_PAGE_LOCKS = ON)
ON [PRIMARY]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningCart
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCart] ADD
	CONSTRAINT [PK_SkyDiningCart] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningCartMainCourse
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCartMainCourse] ADD
	CONSTRAINT [PK_SkyDiningCartMCMapping] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningCartTopUp
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCartTopUp] ADD
	CONSTRAINT [PK_SkyDiningCartTUMapping] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningJewelCard
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningJewelCard] ADD
	CONSTRAINT [PK_SkyDiningJewelCard] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningReservation
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningReservation] ADD
	CONSTRAINT [PK_SkyDiningReservation] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningResMainCourse
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningResMainCourse] ADD
	CONSTRAINT [PK_SkyDiningResMCMapping] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningResTopUp
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningResTopUp] ADD
	CONSTRAINT [PK_SkyDiningResTUMapping] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningSequenceLog
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningSequenceLog] ADD
	CONSTRAINT [PK_SkyDiningSequenceLog] PRIMARY KEY CLUSTERED ([seqId]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Primary key structure for table SkyDiningTransaction
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningTransaction] ADD
	CONSTRAINT [PK_SkyDiningTransaction] PRIMARY KEY CLUSTERED ([id]) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [default]
GO

-- ----------------------------
--  Uniques structure for table SkyDiningTransaction
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningTransaction] ADD
	CONSTRAINT [IX_SkyDiningTransaction] UNIQUE NONCLUSTERED ([receiptNum] ASC) 
	WITH (PAD_INDEX = OFF,
		IGNORE_DUP_KEY = OFF,
		ALLOW_ROW_LOCKS = ON,
		ALLOW_PAGE_LOCKS = ON)
	ON [PRIMARY]
GO

-- ----------------------------
--  Foreign keys structure for table B2CSLMAxCheckoutCart
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMAxCheckoutCart] ADD
	CONSTRAINT [B2CSLMAxCheckoutCart_B2CSLMStoreTransaction_id_fk] FOREIGN KEY ([transactionId]) REFERENCES [dbo].[B2CSLMStoreTransaction] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table B2CSLMAxSalesOrder
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMAxSalesOrder] ADD
	CONSTRAINT [B2CSLMAxSalesOrder_B2CSLMStoreTransaction_id_fk] FOREIGN KEY ([transactionId]) REFERENCES [dbo].[B2CSLMStoreTransaction] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table B2CSLMStoreTransactionItem
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMStoreTransactionItem] ADD
	CONSTRAINT [B2CSLMStoreTransactionItem_B2CSLMStoreTransaction_id_fk] FOREIGN KEY ([transactionId]) REFERENCES [dbo].[B2CSLMStoreTransaction] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMAxSalesOrder
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxSalesOrder] ADD
	CONSTRAINT [FK_PPSLMAxSalesOrder_InventoryTransaction] FOREIGN KEY ([transactionId]) REFERENCES [dbo].[PPSLMInventoryTransaction] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMETicket
-- ----------------------------
ALTER TABLE [dbo].[PPSLMETicket] ADD
	CONSTRAINT [FK_PPSLMETicket_MixMatchPackage] FOREIGN KEY ([packageId]) REFERENCES [dbo].[PPSLMMixMatchPackage] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMInventoryTransactionItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMInventoryTransactionItem] ADD
	CONSTRAINT [FK_PPSLMInventoryTransactionItem_InventoryTransaction] FOREIGN KEY ([transactionId]) REFERENCES [dbo].[PPSLMInventoryTransaction] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMMixMatchPackageItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMMixMatchPackageItem] ADD
	CONSTRAINT [FK_PPSLMMixMatchPackageItem_MixMatchPackage] FOREIGN KEY ([packageId]) REFERENCES [dbo].[PPSLMMixMatchPackage] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT [FK_PPSLMMixMatchPackageItem_InventoryTransactionItem] FOREIGN KEY ([transactionItemId]) REFERENCES [dbo].[PPSLMInventoryTransactionItem] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMPartner
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartner] ADD
	CONSTRAINT [fk_PPSLMPartner_TAMainAccount] FOREIGN KEY ([adminId]) REFERENCES [dbo].[PPSLMTAMainAccount] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMPartnerDocumentMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerDocumentMapping] ADD
	CONSTRAINT [fk_PPSLMPartnerDocumentMapping_Partner] FOREIGN KEY ([partnerId]) REFERENCES [dbo].[PPSLMPartner] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT [fk_PPSLMPartnerDocumentMapping_Document] FOREIGN KEY ([docId]) REFERENCES [dbo].[PPSLMPartnerDocument] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMRegionMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRegionMapping] ADD
	CONSTRAINT [fk_RegionMapping_Region] FOREIGN KEY ([regionId]) REFERENCES [dbo].[PPSLMRegion] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT [fk_RegionMapping_Country] FOREIGN KEY ([ctyId]) REFERENCES [dbo].[PPSLMCountry] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMRevalidationTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRevalidationTransaction] ADD
	CONSTRAINT [FK_PPSLMRevalidationTransaction_InventoryTransaction] FOREIGN KEY ([transactionId]) REFERENCES [dbo].[PPSLMInventoryTransaction] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table PPSLMTASubAccount
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTASubAccount] ADD
	CONSTRAINT [FK_PPSLMTASubAccount_TAMainAccount] FOREIGN KEY ([mainAccountId]) REFERENCES [dbo].[PPSLMTAMainAccount] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table SkyDiningCartMainCourse
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCartMainCourse] ADD
	CONSTRAINT [FK_SkyDiningCartMainCourse_SkyDiningCart] FOREIGN KEY ([cartId]) REFERENCES [dbo].[SkyDiningCart] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table SkyDiningCartTopUp
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCartTopUp] ADD
	CONSTRAINT [FK_SkyDiningCartTopUp_SkyDiningCart] FOREIGN KEY ([cartId]) REFERENCES [dbo].[SkyDiningCart] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table SkyDiningResMainCourse
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningResMainCourse] ADD
	CONSTRAINT [FK_SkyDiningResMCMapping_SkyDiningReservation] FOREIGN KEY ([reservationId]) REFERENCES [dbo].[SkyDiningReservation] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Foreign keys structure for table SkyDiningResTopUp
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningResTopUp] ADD
	CONSTRAINT [FK_SkyDiningResTUMapping_SkyDiningReservation] FOREIGN KEY ([reservationId]) REFERENCES [dbo].[SkyDiningReservation] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
--  Options for table B2CSLMAxCheckoutCart
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMAxCheckoutCart] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[B2CSLMAxCheckoutCart]', RESEED, 1)
GO

-- ----------------------------
--  Options for table B2CSLMAxSalesOrder
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMAxSalesOrder] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[B2CSLMAxSalesOrder]', RESEED, 1)
GO

-- ----------------------------
--  Options for table B2CSLMStoreTransaction
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMStoreTransaction] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[B2CSLMStoreTransaction]', RESEED, 1)
GO

-- ----------------------------
--  Options for table B2CSLMStoreTransactionItem
-- ----------------------------
ALTER TABLE [dbo].[B2CSLMStoreTransactionItem] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[B2CSLMStoreTransactionItem]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMApprovalLog
-- ----------------------------
ALTER TABLE [dbo].[PPSLMApprovalLog] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMApprovalLog]', RESEED, 211)
GO

-- ----------------------------
--  Options for table PPSLMApprover
-- ----------------------------
ALTER TABLE [dbo].[PPSLMApprover] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMApprover]', RESEED, 3)
GO

-- ----------------------------
--  Options for table PPSLMAuditTrail
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAuditTrail] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMAuditTrail]', RESEED, 78)
GO

-- ----------------------------
--  Options for table PPSLMAxCheckoutCart
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxCheckoutCart] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMAxCheckoutCart]', RESEED, 148)
GO

-- ----------------------------
--  Options for table PPSLMAxSalesOrder
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxSalesOrder] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMAxSalesOrder]', RESEED, 100)
GO

-- ----------------------------
--  Options for table PPSLMAxSalesOrderLineNumberQuantity
-- ----------------------------
ALTER TABLE [dbo].[PPSLMAxSalesOrderLineNumberQuantity] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMAxSalesOrderLineNumberQuantity]', RESEED, 97)
GO

-- ----------------------------
--  Options for table PPSLMCountry
-- ----------------------------
ALTER TABLE [dbo].[PPSLMCountry] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMCountry]', RESEED, 15)
GO

-- ----------------------------
--  Options for table PPSLMDepositTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMDepositTransaction] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMDepositTransaction]', RESEED, 59)
GO

-- ----------------------------
--  Options for table PPSLMETicket
-- ----------------------------
ALTER TABLE [dbo].[PPSLMETicket] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMETicket]', RESEED, 108)
GO

-- ----------------------------
--  Options for table PPSLMInventoryTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMInventoryTransaction] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMInventoryTransaction]', RESEED, 784)
GO

-- ----------------------------
--  Options for table PPSLMInventoryTransactionItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMInventoryTransactionItem] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMInventoryTransactionItem]', RESEED, 826)
GO

-- ----------------------------
--  Options for table PPSLMMixMatchPackage
-- ----------------------------
ALTER TABLE [dbo].[PPSLMMixMatchPackage] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMMixMatchPackage]', RESEED, 697)
GO

-- ----------------------------
--  Options for table PPSLMMixMatchPackageItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMMixMatchPackageItem] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMMixMatchPackageItem]', RESEED, 730)
GO

-- ----------------------------
--  Options for table PPSLMOfflinePaymentRequest
-- ----------------------------
ALTER TABLE [dbo].[PPSLMOfflinePaymentRequest] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMOfflinePaymentRequest]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMPADistributionMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPADistributionMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPADistributionMapping]', RESEED, 78)
GO

-- ----------------------------
--  Options for table PPSLMPADocMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPADocMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPADocMapping]', RESEED, 117)
GO

-- ----------------------------
--  Options for table PPSLMPADocument
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPADocument] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPADocument]', RESEED, 119)
GO

-- ----------------------------
--  Options for table PPSLMPartner
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartner] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartner]', RESEED, 54)
GO

-- ----------------------------
--  Options for table PPSLMPartnerDocument
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerDocument] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartnerDocument]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMPartnerDocumentMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerDocumentMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartnerDocumentMapping]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMPartnerExclusiveMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerExclusiveMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartnerExclusiveMapping]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMPartnerExt
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerExt] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartnerExt]', RESEED, 80)
GO

-- ----------------------------
--  Options for table PPSLMPartnerTransactionHist
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerTransactionHist] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartnerTransactionHist]', RESEED, 2)
GO

-- ----------------------------
--  Options for table PPSLMPartnerType
-- ----------------------------
ALTER TABLE [dbo].[PPSLMPartnerType] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMPartnerType]', RESEED, 10)
GO

-- ----------------------------
--  Options for table PPSLMReason
-- ----------------------------
ALTER TABLE [dbo].[PPSLMReason] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMReason]', RESEED, 7)
GO

-- ----------------------------
--  Options for table PPSLMReasonLogMap
-- ----------------------------
ALTER TABLE [dbo].[PPSLMReasonLogMap] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMReasonLogMap]', RESEED, 16)
GO

-- ----------------------------
--  Options for table PPSLMRefreshHist
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRefreshHist] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMRefreshHist]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMRegion
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRegion] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMRegion]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMRegionMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRegionMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMRegionMapping]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMRevalidationFeeItem
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRevalidationFeeItem] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMRevalidationFeeItem]', RESEED, 5)
GO

-- ----------------------------
--  Options for table PPSLMRevalidationTransaction
-- ----------------------------
ALTER TABLE [dbo].[PPSLMRevalidationTransaction] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMRevalidationTransaction]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMSequenceLog
-- ----------------------------
ALTER TABLE [dbo].[PPSLMSequenceLog] SET (LOCK_ESCALATION = TABLE)
GO

-- ----------------------------
--  Options for table PPSLMSessionInfo
-- ----------------------------
ALTER TABLE [dbo].[PPSLMSessionInfo] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMSessionInfo]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMSessionTrace
-- ----------------------------
ALTER TABLE [dbo].[PPSLMSessionTrace] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMSessionTrace]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMTAAccessRight
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAAccessRight] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAAccessRight]', RESEED, 11)
GO

-- ----------------------------
--  Options for table PPSLMTAAccessRightsGroup
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAAccessRightsGroup] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAAccessRightsGroup]', RESEED, 5)
GO

-- ----------------------------
--  Options for table PPSLMTAAccessRightsGroupMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAAccessRightsGroupMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAAccessRightsGroupMapping]', RESEED, 11)
GO

-- ----------------------------
--  Options for table PPSLMTAMainAccount
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAMainAccount] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAMainAccount]', RESEED, 56)
GO

-- ----------------------------
--  Options for table PPSLMTAMainRightsMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAMainRightsMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAMainRightsMapping]', RESEED, 150)
GO

-- ----------------------------
--  Options for table PPSLMTAPrevPasswordMain
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAPrevPasswordMain] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAPrevPasswordMain]', RESEED, 15)
GO

-- ----------------------------
--  Options for table PPSLMTAPrevPasswordSub
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTAPrevPasswordSub] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTAPrevPasswordSub]', RESEED, 28)
GO

-- ----------------------------
--  Options for table PPSLMTARightsMapping
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTARightsMapping] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTARightsMapping]', RESEED, 105)
GO

-- ----------------------------
--  Options for table PPSLMTASalesManager
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTASalesManager] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTASalesManager]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMTASubAccount
-- ----------------------------
ALTER TABLE [dbo].[PPSLMTASubAccount] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMTASubAccount]', RESEED, 39)
GO

-- ----------------------------
--  Options for table PPSLMUserSession
-- ----------------------------
ALTER TABLE [dbo].[PPSLMUserSession] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMUserSession]', RESEED, 1)
GO

-- ----------------------------
--  Options for table PPSLMWingsOfTimeReservation
-- ----------------------------
ALTER TABLE [dbo].[PPSLMWingsOfTimeReservation] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMWingsOfTimeReservation]', RESEED, 81)
GO

-- ----------------------------
--  Options for table PPSLMWingsOfTimeReservationLog
-- ----------------------------
ALTER TABLE [dbo].[PPSLMWingsOfTimeReservationLog] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[PPSLMWingsOfTimeReservationLog]', RESEED, 585)
GO

-- ----------------------------
--  Options for table SkyDiningCart
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCart] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningCart]', RESEED, 20)
GO

-- ----------------------------
--  Options for table SkyDiningCartMainCourse
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCartMainCourse] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningCartMainCourse]', RESEED, 35)
GO

-- ----------------------------
--  Options for table SkyDiningCartTopUp
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningCartTopUp] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningCartTopUp]', RESEED, 3)
GO

-- ----------------------------
--  Options for table SkyDiningJewelCard
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningJewelCard] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningJewelCard]', RESEED, 1)
GO

-- ----------------------------
--  Options for table SkyDiningReservation
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningReservation] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningReservation]', RESEED, 4)
GO

-- ----------------------------
--  Options for table SkyDiningResMainCourse
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningResMainCourse] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningResMainCourse]', RESEED, 7)
GO

-- ----------------------------
--  Options for table SkyDiningResTopUp
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningResTopUp] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningResTopUp]', RESEED, 1)
GO

-- ----------------------------
--  Options for table SkyDiningSequenceLog
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningSequenceLog] SET (LOCK_ESCALATION = TABLE)
GO

-- ----------------------------
--  Options for table SkyDiningTransaction
-- ----------------------------
ALTER TABLE [dbo].[SkyDiningTransaction] SET (LOCK_ESCALATION = TABLE)
GO
DBCC CHECKIDENT ('[dbo].[SkyDiningTransaction]', RESEED, 2)
GO

