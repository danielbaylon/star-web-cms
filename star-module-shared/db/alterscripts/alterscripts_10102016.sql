---Applied TO UAT (Oct 15)

alter table PPSLMRevalidationTransaction alter column revalItemCode varchar(50) NOT NULL

alter table PPSLMRevalidationTransaction alter column revalTopupItemCode int NULL
alter table PPSLMRevalidationTransaction alter column revalTopupFeeInCents int NULL