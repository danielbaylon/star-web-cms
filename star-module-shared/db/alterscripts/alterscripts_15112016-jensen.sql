CREATE TABLE STAR_DB.dbo.B2CSLMStoreTransactionExtension
(
    id INT PRIMARY KEY NOT NULL IDENTITY,
    transactionId INT NOT NULL,
    receiptNumber VARCHAR(50) NOT NULL,
    storeTransactionJson VARCHAR(MAX),
    cartJson VARCHAR(MAX),
    salesOrderJson VARCHAR(MAX),
    ticketsJson VARCHAR(MAX),
    CONSTRAINT B2CSLMStoreTransactionExtension_B2CSLMStoreTransaction_id_fk FOREIGN KEY (transactionId) REFERENCES B2CSLMStoreTransaction (id)
);
CREATE UNIQUE INDEX B2CSLMStoreTransactionExtension_transactionId_uindex ON STAR_DB.dbo.B2CSLMStoreTransactionExtension (transactionId);
CREATE UNIQUE INDEX B2CSLMStoreTransactionExtension_receiptNumber_uindex ON STAR_DB.dbo.B2CSLMStoreTransactionExtension (receiptNumber);

CREATE TABLE STAR_DB.dbo.B2CMFLGStoreTransactionExtension
(
    id INT PRIMARY KEY NOT NULL IDENTITY,
    transactionId INT NOT NULL,
    receiptNumber VARCHAR(50) NOT NULL,
    storeTransactionJson VARCHAR(MAX),
    cartJson VARCHAR(MAX),
    salesOrderJson VARCHAR(MAX),
    ticketsJson VARCHAR(MAX),
    CONSTRAINT B2CMFLGStoreTransactionExtension_B2CMFLGStoreTransaction_id_fk FOREIGN KEY (transactionId) REFERENCES B2CMFLGStoreTransaction (id)
);
CREATE UNIQUE INDEX B2CMFLGStoreTransactionExtension_transactionId_uindex ON STAR_DB.dbo.B2CMFLGStoreTransactionExtension (transactionId);
CREATE UNIQUE INDEX B2CMFLGStoreTransactionExtension_receiptNumber_uindex ON STAR_DB.dbo.B2CMFLGStoreTransactionExtension (receiptNumber);

