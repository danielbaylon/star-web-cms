drop table PPSLMOfflinePaymentRequest;
CREATE TABLE PPSLMOfflinePaymentRequest(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[transactionId] [int] NOT NULL,
	[paymentType] [nvarchar](20)  NULL,
	[referenceNum] [nvarchar](200)  NULL,
	[bankName] [nvarchar](200)  NULL,
	[bankCountry] [nvarchar](200)  NULL,
	[status] [nvarchar](20) NOT NULL,
	[detail]  [nvarchar](max) NULL,
	[approverRemarks] [nvarchar](2000) NULL,
	[remarks] [nvarchar](2000) NULL,
	[approver] [nvarchar](100)  NULL,
	[approveDate] [datetime]  NULL,
	[createdBy] [nvarchar](100) NOT NULL,
	[createdDate] [datetime] NOT NULL,
CONSTRAINT [PK_OfflinePaymentRequest] PRIMARY KEY CLUSTERED
([id] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];



alter table PPSLMOfflinePaymentRequest add  paymentDate Date null;
alter table PPSLMOfflinePaymentRequest add  finalApprovalRemarks varchar(2000) null;
alter table PPSLMOfflinePaymentRequest add  finalCancelRemarks varchar(2000) null;
alter table PPSLMOfflinePaymentRequest alter column remarks text;
alter table PPSLMOfflinePaymentRequest alter column [approverRemarks] text;
alter table PPSLMOfflinePaymentRequest alter column [finalApprovalRemarks] text;
alter table PPSLMOfflinePaymentRequest alter column [finalCancelRemarks] text;

alter table PPSLMOfflinePaymentRequest add mainAccountId integer;


alter table PPSLMOfflinePaymentRequest alter column [detail] text;

alter table PPSLMOfflinePaymentRequest add submitDate date;

alter table PPSLMOfflinePaymentRequest add submitBy [nvarchar](100);

alter table PPSLMOfflinePaymentRequest add  bankCountryCode varchar(3) null;
