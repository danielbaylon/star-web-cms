alter table PPSLMWingsOfTimeReservation add axChargeInvoice varchar(50);
alter table PPSLMWingsOfTimeReservation add axChargeVoucher varchar(50);
alter table PPSLMWingsOfTimeReservation add axChargeDocumentNum varchar(50);
alter table PPSLMWingsOfTimeReservation add axChargeAmount MONEY;

alter table PPSLMWingsOfTimeReservation add axRefundInvoice varchar(50);
alter table PPSLMWingsOfTimeReservation add axRefundVoucher varchar(50);
alter table PPSLMWingsOfTimeReservation add axRefundDocumentNum varchar(50);
alter table PPSLMWingsOfTimeReservation add axRefundAmount MONEY;

alter table PPSLMWingsOfTimeReservation add axChargeQty int null;
alter table PPSLMWingsOfTimeReservation add axRefundQty int null;

alter table PPSLMWingsOfTimeReservation drop column	axDiscountAmt	;
alter table PPSLMWingsOfTimeReservation drop column	axExternalPrice	;
alter table PPSLMWingsOfTimeReservation drop column	axItemTaxGroupId	;
alter table PPSLMWingsOfTimeReservation drop column	axLineId	;
alter table PPSLMWingsOfTimeReservation drop column	axNetAmtWoTax	;
alter table PPSLMWingsOfTimeReservation drop column	axPrice	;
alter table PPSLMWingsOfTimeReservation drop column	axrtChargeAmountWithCurrency	;
alter table PPSLMWingsOfTimeReservation drop column	axrtDeliveryModeDescription	;
alter table PPSLMWingsOfTimeReservation drop column	axrtDeliveryModeId	;
alter table PPSLMWingsOfTimeReservation drop column	axrtDiscountAmount	;
alter table PPSLMWingsOfTimeReservation drop column	axrtId	;
alter table PPSLMWingsOfTimeReservation drop column	axrtNetAmountWithCurrency	;
alter table PPSLMWingsOfTimeReservation drop column	axrtOrderPlacedDate	;
alter table PPSLMWingsOfTimeReservation drop column	axrtPaymentStatus	;
alter table PPSLMWingsOfTimeReservation drop column	axrtpPriceWithCurrency	;
alter table PPSLMWingsOfTimeReservation drop column	axrtRequestedDeliveryDate	;
alter table PPSLMWingsOfTimeReservation drop column	axrtSubtotalWithCurrency	;
alter table PPSLMWingsOfTimeReservation drop column	axrtTaxAmountWithCurrency	;
alter table PPSLMWingsOfTimeReservation drop column	axrtTotalAmountWithCurrency	;
alter table PPSLMWingsOfTimeReservation drop column	axrtValChannelId	;
alter table PPSLMWingsOfTimeReservation drop column	axSubtotalAmt	;
alter table PPSLMWingsOfTimeReservation drop column	axTaxAmt	;
alter table PPSLMWingsOfTimeReservation drop column	axTotalAmt	;

CREATE TABLE STAR_DB.dbo.PPSLMWingsOfTimeReceiptLog
(
    id INT IDENTITY,
    type VARCHAR(20) NOT NULL,
    date DATE NOT NULL,
    count INT NOT NULL
);

alter table PPSLMWingsOfTimeReceiptLog add mainAccountId INTEGER NULL;

