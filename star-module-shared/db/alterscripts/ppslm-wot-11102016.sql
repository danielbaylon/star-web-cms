
alter table PPSLMDepositTransaction add  tenderTypeId VARCHAR(1) null;

alter table PPSLMWingsOfTimeReservation add eventTime INT null;

alter table PPSLMPartner add tierId varchar(50) null;

alter table PPSLMPADocument add remoteFileDir varchar(1000) null;
alter table PPSLMPADocument add remoteFileName varchar(1000) null;
alter table PPSLMPADocument add axFileViewPath varchar(1000) null;
