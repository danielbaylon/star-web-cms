alter table PPSLMAxSalesOrderLineNumberQuantity add lineId varchar(50)
alter table PPSLMMixMatchPackage add totalQty int;
alter table PPSLMMixMatchPackage add totalQtyRedeemed int;

alter table PPSLMMixMatchPackageItem add qtyRedeemed int;

alter table PPSLMMixMatchPackageItem add eventGroupId varchar(100), eventLineId varchar(50), eventName varchar(100);
