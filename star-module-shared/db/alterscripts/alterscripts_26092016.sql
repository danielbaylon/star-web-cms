-- Put all the alterscripts for the same day. if the following day, create a new file with the date of that day. Thanks!

-- APPLIED ---
ALTER TABLE SkyDiningCart ALTER COLUMN merchantId nvarchar(100) null;

ALTER TABLE SkyDiningReservation ALTER COLUMN merchantId nvarchar(100) null;