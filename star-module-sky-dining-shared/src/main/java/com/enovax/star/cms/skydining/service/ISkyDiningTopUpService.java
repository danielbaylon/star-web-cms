package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.star.cms.skydining.model.TopUpModel;
import com.enovax.star.cms.skydining.model.TopUpRecord;

import java.io.File;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningTopUpService {

  int countTopUpRecords(String name, Boolean active);

  List<TopUpRecord> getTopUpRecords(String name, Boolean active, int start,
                                    int pageSize, String orderBy, boolean asc);

  TopUpModel getToUp(int id);

  SaveResult saveDisplayImage(int id, File image, String fileName,
                              String userId, String timestampText);

  SaveResult saveTopUp(int id, String newName, Integer newType,
                       String newDescription, Integer newDisplayOrder,
                       String newEarlyBirdEndDate, String newStartDate, String newEndDate,
                       Integer newSalesCutOffDays, Integer newPrice, String newImageHash,
                       String userId, String timestampText);

}
