package com.enovax.star.cms.skydining.repository.db;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTelemoneyLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jonathan on 7/12/16.
 */
@Repository
public interface SkyDiningTelemoneyLogRepository extends JpaRepository<SkyDiningTelemoneyLog, Integer> {
}
