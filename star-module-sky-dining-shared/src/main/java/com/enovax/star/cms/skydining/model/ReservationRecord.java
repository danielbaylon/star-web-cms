package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class ReservationRecord implements Serializable {

  private int id;
  private String guestName;
  private String packageName;
  private String dateOfVisit;
  private String transactionDate;
  private String receiptNumber;
  private String transactionStatus;
  private String reservationStatus;
  private String reservationType;

  public ReservationRecord(SkyDiningReservation dataModel) {
    receiptNumber = "n/a";
    transactionStatus = "n/a";

    id = dataModel.getId();

    if (StringUtils.isNotBlank(dataModel.getPackageName())) {
      packageName = dataModel.getPackageName();
    } else {
      packageName = "-";
    }

    if (StringUtils.isNotEmpty(dataModel.getGuestName())) {
      guestName = dataModel.getGuestName();
    } else {
      guestName = "-";
    }

    if (dataModel.getDate() != null) {
      dateOfVisit = NvxDateUtils.formatDate(dataModel.getDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    } else {
      dateOfVisit = "-";
    }

    if (dataModel.getStatus() != null) {
      switch (dataModel.getStatus()) {
        case 0:
          reservationStatus = "Cancelled";
          break;
        case 1:
          reservationStatus = "Paid inside System";
          break;
        case 2:
          reservationStatus = "Not Paid";
          break;
        case 3:
          reservationStatus = "Paid outside System";
          break;
        default:
          reservationStatus = "-";
          break;
      }
    } else {
      reservationStatus = "-";
    }

    if (dataModel.getType() != null) {
      switch (dataModel.getType()) {
        case 0:
          reservationType = "Online";
          break;
        case 1:
          reservationType = "Admin";
          break;
        default:
          reservationType = "-";
          break;
      }
    } else {
      reservationType = "-";
    }

    transactionDate = NvxDateUtils.formatDateForDisplay(
      dataModel.getCreatedDate(), true);
  }

  public String getDateOfVisit() {
    return dateOfVisit;
  }

  public String getGuestName() {
    return guestName;
  }

  public int getId() {
    return id;
  }

  public String getPackageName() {
    return packageName;
  }

  public String getReceiptNumber() {
    return receiptNumber;
  }

  public String getReservationStatus() {
    return reservationStatus;
  }

  public String getReservationType() {
    return reservationType;
  }

  public String getTransactionDate() {
    return transactionDate;
  }

  public String getTransactionStatus() {
    return transactionStatus;
  }

  public void setDateOfVisit(String dateOfVisit) {
    this.dateOfVisit = dateOfVisit;
  }

  public void setGuestName(String guestName) {
    this.guestName = guestName;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public void setReceiptNumber(String receiptNumber) {
    this.receiptNumber = receiptNumber;
  }

  public void setReservationStatus(String reservationStatus) {
    this.reservationStatus = reservationStatus;
  }

  public void setReservationType(String reservationType) {
    this.reservationType = reservationType;
  }

  public void setTransactionDate(String transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setTransactionStatus(String transactionStatus) {
    this.transactionStatus = transactionStatus;
  }

}
