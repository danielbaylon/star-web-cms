package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTnc;
import com.enovax.util.jcr.repository.JcrRepository;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningTncRepository extends JcrRepository<SkyDiningTnc> {
}
