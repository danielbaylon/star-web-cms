package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;
import com.enovax.star.cms.skydining.util.dao.RepoUtil;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.query.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jace on 28/7/16.
 */
@Repository
public class DefaultSkyDiningTopUpRepository extends BaseJcrRepository<SkyDiningTopUp> implements ISkyDiningTopUpRepository {

  public static final String JCR_WORKSPACE = "sky-dining-admin";
  public static final String JCR_PATH = "top-ups";
  public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-top-up";

  @Override
  protected String getJcrWorkspace() {
    return JCR_WORKSPACE;
  }

  @Override
  protected String getJcrPath() {
    return JCR_PATH;
  }

  @Override
  protected SkyDiningTopUp getNewInstance() {
    return new SkyDiningTopUp();
  }

  @Override
  protected String getPrimaryNodeType() {
    return PRIMARY_NODE_TYPE;
  }

  protected static final String SQL_FIND_PAGED = "select * from [mgnl:sky-dining-top-up] as tu "
    + RepoUtil.FILTER_CLAUSE + RepoUtil.ORDER_CLAUSE;

  @Override
  public int count(String name, Boolean active, Integer packageId) {
    final String query = getSQLFindPaged(name, active, packageId, null, false);
    final Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (active != null) {
      params.put("currentDate", new Date());
    }

    List<SkyDiningTopUp> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, params, null, null);

    if ((packageId != null) && (packageId > 0)) {
      result = filterNotLinkedTo("sky-dining-admin", "packages", String.valueOf(packageId), "mgnl:sky-dining-package", "topUps", result);
    }
    return result.size();
  }

  @Override
  public List<SkyDiningTopUp> findPaged(String name, Boolean active, Integer packageId, int start, int pageSize, String orderBy, boolean asc) {
    final String query = getSQLFindPaged(name, active, packageId, null, false);
    final Map<String, Object> params = new HashMap<String, Object>();
    if (StringUtils.isNotEmpty(name)) {
      params.put("name", "%" + name + "%");
    }
    if (active != null) {
      params.put("currentDate", new Date());
    }

    List<SkyDiningTopUp> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, params, pageSize, start);

    if ((packageId != null) && (packageId > 0)) {
      result = filterNotLinkedTo("sky-dining-admin", "packages", String.valueOf(packageId), "mgnl:sky-dining-package", "topUps", result);
    }
    return result;
  }

  protected String getSQLFindPaged(String name, Boolean active,
                                   Integer packageId, String orderBy, boolean asc) {
    String query = SQL_FIND_PAGED;
    String filterClause = "";
    String orderClause = "";

    if (StringUtils.isNotEmpty(name)) {
      filterClause += "and tu.name like $name ";
    }

    if (active != null) {
      if (active) {
        filterClause += "and tu.endDate >= $currentDate and tu.startDate <= $currentDate ";
      } else {
        filterClause += "and (tu.endDate < $currentDate or tu.startDate > $currentDate) ";
      }
    }

//    if (packageId != null) {
//      if (packageId > 0) {
//        filterClause += "and tu.id not in (select tum.topUpId from SkyDiningTopUpMapping tum where tum.packageId = :packageId) ";
//      }
//    }

    if (filterClause.startsWith("and")) {
      filterClause = filterClause.substring(3);
    }
    if (StringUtils.isNotBlank(filterClause)) {
      filterClause = "where" + filterClause;
    }

    if (StringUtils.isNotEmpty(orderBy)) {
      orderClause += "order by tu." + orderBy + " ";
      if (asc) {
        orderClause += "asc";
      } else {
        orderClause += "desc";
      }
    }

    query = query.replace(RepoUtil.FILTER_CLAUSE, filterClause);
    query = query.replace(RepoUtil.ORDER_CLAUSE, orderClause);

    return query;
  }
}
