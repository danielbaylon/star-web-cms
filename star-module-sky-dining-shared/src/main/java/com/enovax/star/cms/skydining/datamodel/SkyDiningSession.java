package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-session")
public class SkyDiningSession implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-sessions", propertyName = "session-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String name;
  @JcrProperty
  private Integer sessionNumber;
  @JcrProperty
  private Integer sessionType;
  @JcrProperty
  private Date startDate;
  @JcrProperty
  private Date endDate;
  @JcrProperty
  private String time;
  @JcrProperty
  private Date salesStartDate;
  @JcrProperty
  private Date salesEndDate;
  @JcrProperty
  private Integer capacity;
  @JcrProperty
  private Boolean active;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;
  @JcrProperty
  private String effectiveDays;

  @JcrLink(linkedPath = "packages", linkedWorkspace = "sky-dining-admin", linkedNodeType = "mgnl:sky-dining-package")
  private List<SkyDiningPackage> packages = new ArrayList<>(0);

  @JcrContent
  private List<SkyDiningBlockOutDate> blockOutDates = new ArrayList<>(
    0);

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSessionNumber() {
    return sessionNumber;
  }

  public void setSessionNumber(Integer sessionNumber) {
    this.sessionNumber = sessionNumber;
  }

  public Integer getSessionType() {
    return sessionType;
  }

  public void setSessionType(Integer sessionType) {
    this.sessionType = sessionType;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public Date getSalesStartDate() {
    return salesStartDate;
  }

  public void setSalesStartDate(Date salesStartDate) {
    this.salesStartDate = salesStartDate;
  }

  public Date getSalesEndDate() {
    return salesEndDate;
  }

  public void setSalesEndDate(Date salesEndDate) {
    this.salesEndDate = salesEndDate;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getEffectiveDays() {
    return effectiveDays;
  }

  public void setEffectiveDays(String effectiveDays) {
    this.effectiveDays = effectiveDays;
  }

  public List<SkyDiningPackage> getPackages() {
    return packages;
  }

  public void setPackages(List<SkyDiningPackage> packages) {
    this.packages = packages;
  }

  public List<SkyDiningBlockOutDate> getBlockOutDates() {
    return blockOutDates;
  }

  public void setBlockOutDates(List<SkyDiningBlockOutDate> blockOutDates) {
    this.blockOutDates = blockOutDates;
  }
}
