package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 30/7/16.
 */
public class SkyDiningPackageActionParam extends BaseSkyDiningActionParam {

  protected String newName;
  protected String newEarlyBirdEndDate;
  protected String newStartDate;
  protected String newEndDate;
  protected Integer newSalesCutOffDays;
  protected Integer newCapacity;
  protected String newDescription;
  protected String newLink;
  protected Integer newMinPax;
  protected Integer newMaxPax;
  protected String newImageHash;
  protected String newNotes;
  protected Integer newSelectedTncId;
  protected String newTitle;
  protected String newTncContent;
  protected Boolean newSelected;
  protected Boolean newActive;
  protected String newType;

  protected Integer newDisplayOrder;

  protected String newMerchantId;
  protected Integer newPriceType;
  protected Integer newDiscountPrice;
  protected String nameFilter;
  protected Integer activeFilter;

  protected String newCode;
  protected Boolean newHasLimit;
  protected Integer newLimit;
  protected Integer newTimesUsed;

  protected String newValidFrom;
  protected String newValidTo;
  protected String newTnc;

  protected String menuTypeFilter;

  public String getNewName() {
    return newName;
  }

  public void setNewName(String newName) {
    this.newName = newName;
  }

  public String getNewEarlyBirdEndDate() {
    return newEarlyBirdEndDate;
  }

  public void setNewEarlyBirdEndDate(String newEarlyBirdEndDate) {
    this.newEarlyBirdEndDate = newEarlyBirdEndDate;
  }

  public String getNewStartDate() {
    return newStartDate;
  }

  public void setNewStartDate(String newStartDate) {
    this.newStartDate = newStartDate;
  }

  public String getNewEndDate() {
    return newEndDate;
  }

  public void setNewEndDate(String newEndDate) {
    this.newEndDate = newEndDate;
  }

  public Integer getNewSalesCutOffDays() {
    return newSalesCutOffDays;
  }

  public void setNewSalesCutOffDays(Integer newSalesCutOffDays) {
    this.newSalesCutOffDays = newSalesCutOffDays;
  }

  public Integer getNewCapacity() {
    return newCapacity;
  }

  public void setNewCapacity(Integer newCapacity) {
    this.newCapacity = newCapacity;
  }

  public String getNewDescription() {
    return newDescription;
  }

  public void setNewDescription(String newDescription) {
    this.newDescription = newDescription;
  }

  public String getNewLink() {
    return newLink;
  }

  public void setNewLink(String newLink) {
    this.newLink = newLink;
  }

  public Integer getNewMinPax() {
    return newMinPax;
  }

  public void setNewMinPax(Integer newMinPax) {
    this.newMinPax = newMinPax;
  }

  public Integer getNewMaxPax() {
    return newMaxPax;
  }

  public void setNewMaxPax(Integer newMaxPax) {
    this.newMaxPax = newMaxPax;
  }

  public String getNewImageHash() {
    return newImageHash;
  }

  public void setNewImageHash(String newImageHash) {
    this.newImageHash = newImageHash;
  }

  public String getNewNotes() {
    return newNotes;
  }

  public void setNewNotes(String newNotes) {
    this.newNotes = newNotes;
  }

  public Integer getNewSelectedTncId() {
    return newSelectedTncId;
  }

  public void setNewSelectedTncId(Integer newSelectedTncId) {
    this.newSelectedTncId = newSelectedTncId;
  }

  public String getNewTitle() {
    return newTitle;
  }

  public void setNewTitle(String newTitle) {
    this.newTitle = newTitle;
  }

  public String getNewTncContent() {
    return newTncContent;
  }

  public void setNewTncContent(String newTncContent) {
    this.newTncContent = newTncContent;
  }

  public Boolean getNewSelected() {
    return newSelected;
  }

  public void setNewSelected(Boolean newSelected) {
    this.newSelected = newSelected;
  }

  public Boolean getNewActive() {
    return newActive;
  }

  public void setNewActive(Boolean newActive) {
    this.newActive = newActive;
  }

  public String getNewType() {
    return newType;
  }

  public void setNewType(String newType) {
    this.newType = newType;
  }

  public Integer getNewDisplayOrder() {
    return newDisplayOrder;
  }

  public void setNewDisplayOrder(Integer newDisplayOrder) {
    this.newDisplayOrder = newDisplayOrder;
  }

  public String getNewMerchantId() {
    return newMerchantId;
  }

  public void setNewMerchantId(String newMerchantId) {
    this.newMerchantId = newMerchantId;
  }

  public Integer getNewPriceType() {
    return newPriceType;
  }

  public void setNewPriceType(Integer newPriceType) {
    this.newPriceType = newPriceType;
  }

  public Integer getNewDiscountPrice() {
    return newDiscountPrice;
  }

  public void setNewDiscountPrice(Integer newDiscountPrice) {
    this.newDiscountPrice = newDiscountPrice;
  }

  public String getNameFilter() {
    return nameFilter;
  }

  public void setNameFilter(String nameFilter) {
    this.nameFilter = nameFilter;
  }

  public Integer getActiveFilter() {
    return activeFilter;
  }

  public void setActiveFilter(Integer activeFilter) {
    this.activeFilter = activeFilter;
  }

  public String getNewCode() {
    return newCode;
  }

  public void setNewCode(String newCode) {
    this.newCode = newCode;
  }

  public Boolean getNewHasLimit() {
    return newHasLimit;
  }

  public void setNewHasLimit(Boolean newHasLimit) {
    this.newHasLimit = newHasLimit;
  }

  public Integer getNewLimit() {
    return newLimit;
  }

  public void setNewLimit(Integer newLimit) {
    this.newLimit = newLimit;
  }

  public Integer getNewTimesUsed() {
    return newTimesUsed;
  }

  public void setNewTimesUsed(Integer newTimesUsed) {
    this.newTimesUsed = newTimesUsed;
  }

  public String getNewValidFrom() {
    return newValidFrom;
  }

  public void setNewValidFrom(String newValidFrom) {
    this.newValidFrom = newValidFrom;
  }

  public String getNewValidTo() {
    return newValidTo;
  }

  public void setNewValidTo(String newValidTo) {
    this.newValidTo = newValidTo;
  }

  public String getNewTnc() {
    return newTnc;
  }

  public void setNewTnc(String newTnc) {
    this.newTnc = newTnc;
  }

  public String getMenuTypeFilter() {
    return menuTypeFilter;
  }

  public void setMenuTypeFilter(String menuTypeFilter) {
    this.menuTypeFilter = menuTypeFilter;
  }
}
