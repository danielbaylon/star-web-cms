package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.Date;

/**
 * Created by jace on 21/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-main-course")
public class SkyDiningMainCourse implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-menus", propertyName = "main-course-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String code;
  @JcrProperty
  private Integer type;
  @JcrProperty
  private String description;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;
  @JcrProperty
  private Boolean deleted;

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}
