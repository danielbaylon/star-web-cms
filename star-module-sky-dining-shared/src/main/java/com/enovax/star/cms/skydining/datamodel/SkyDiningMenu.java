package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 21/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-menu")
public class SkyDiningMenu implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-menus", propertyName = "menu-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String name;
  @JcrProperty
  private String description;
  @JcrProperty
  private Date earlyBirdEndDate;
  @JcrProperty
  private Date startDate;
  @JcrProperty
  private Date endDate;
  @JcrProperty
  private Integer salesCutOffDays;
  @JcrProperty
  private Integer menuType;
  @JcrProperty
  private Integer price;
  @JcrProperty
  private Integer serviceCharge;
  @JcrProperty
  private Integer gst;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;

  @JcrLink(linkedWorkspace = "sky-dining-admin", linkedNodeType = "mgnl:sky-dining-main-course", linkedPath = "main-courses")
  private List<SkyDiningMainCourse> mainCourses = new ArrayList<>(
    0);

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getEarlyBirdEndDate() {
    return earlyBirdEndDate;
  }

  public void setEarlyBirdEndDate(Date earlyBirdEndDate) {
    this.earlyBirdEndDate = earlyBirdEndDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Integer getSalesCutOffDays() {
    return salesCutOffDays;
  }

  public void setSalesCutOffDays(Integer salesCutOffDays) {
    this.salesCutOffDays = salesCutOffDays;
  }

  public Integer getMenuType() {
    return menuType;
  }

  public void setMenuType(Integer menuType) {
    this.menuType = menuType;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getServiceCharge() {
    return serviceCharge;
  }

  public void setServiceCharge(Integer serviceCharge) {
    this.serviceCharge = serviceCharge;
  }

  public Integer getGst() {
    return gst;
  }

  public void setGst(Integer gst) {
    this.gst = gst;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public List<SkyDiningMainCourse> getMainCourses() {
    return mainCourses;
  }

  public void setMainCourses(List<SkyDiningMainCourse> mainCourses) {
    this.mainCourses = mainCourses;
  }
}
