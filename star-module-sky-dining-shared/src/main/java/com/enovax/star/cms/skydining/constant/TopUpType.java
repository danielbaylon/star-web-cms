package com.enovax.star.cms.skydining.constant;

public enum TopUpType {
  Other(0), Cake(1), Flowers(2), Wine(3);

  public static TopUpType valueOf(int i) {
    switch (i) {
      case 0:
        return Other;
      case 1:
        return Cake;
      case 2:
        return Flowers;
      case 3:
        return Wine;
      default:
        return null;
    }
  }

  public final int value;

  TopUpType(int value) {
    this.value = value;
  }
}
