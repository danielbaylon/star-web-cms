package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;
import com.enovax.star.cms.skydining.datamodel.SkyDiningSession;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
@Repository
public class DefaultSkyDiningSessionRepository extends BaseJcrRepository<SkyDiningSession> implements ISkyDiningSessionRepository {

    public static final String JCR_WORKSPACE = "sky-dining-admin";
    public static final String JCR_PATH = "sessions";
    public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-session";

    @Override
    protected String getJcrWorkspace() {
        return JCR_WORKSPACE;
    }

    @Override
    protected String getJcrPath() {
        return JCR_PATH;
    }

    @Override
    protected SkyDiningSession getNewInstance() {
        return new SkyDiningSession();
    }

    @Override
    protected String getPrimaryNodeType() {
        return PRIMARY_NODE_TYPE;
    }

    @Override
    public List<SkyDiningSession> findAvailable(Date dateOfBooking, Date dateOfVisit, Integer packageId) {
        List<SkyDiningSession> sessions = findByProperty("active", true);
        if (dateOfBooking != null) {
            Iterator<SkyDiningSession> sessionIterator = sessions.iterator();
            while (sessionIterator.hasNext()) {
                SkyDiningSession session = sessionIterator.next();
                if (!NvxDateUtils.inDateRange(session.getSalesStartDate(), session.getSalesEndDate(), dateOfBooking, 0, true, true)) {
                    sessionIterator.remove();
                }
            }
        }
        if (dateOfVisit != null) {
            Iterator<SkyDiningSession> sessionIterator = sessions.iterator();
            while (sessionIterator.hasNext()) {
                SkyDiningSession session = sessionIterator.next();
                if (!NvxDateUtils.inDateRange(session.getStartDate(), session.getEndDate(), dateOfVisit, 0, true, true)) {
                    sessionIterator.remove();
                }
            }
        }
        if (packageId != null) {
            Iterator<SkyDiningSession> sessionIterator = sessions.iterator();
            while (sessionIterator.hasNext()) {
                SkyDiningSession session = sessionIterator.next();
                boolean hasPackage = false;
                for (SkyDiningPackage p : session.getPackages()) {
                    if (p.getId() == packageId) {
                        hasPackage = true;
                        break;
                    }
                }
                if (!hasPackage) {
                    sessionIterator.remove();
                }
            }
        }
        return sessions;
    }

    @Override
    public List<SkyDiningSession> findPaged(int start, int pageSize, String orderBy, boolean asc) {
        return find(null, null, orderBy, asc, Long.valueOf(pageSize), Long.valueOf(start));
    }

}
