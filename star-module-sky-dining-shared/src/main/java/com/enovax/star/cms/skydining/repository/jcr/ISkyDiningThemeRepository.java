package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTheme;
import com.enovax.util.jcr.repository.JcrRepository;

/**
 * Created by jace on 17/8/16.
 */
public interface ISkyDiningThemeRepository extends JcrRepository<SkyDiningTheme> {
}
