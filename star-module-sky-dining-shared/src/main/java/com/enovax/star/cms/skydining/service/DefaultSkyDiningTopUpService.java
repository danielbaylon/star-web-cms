package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;
import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.star.cms.skydining.model.TopUpModel;
import com.enovax.star.cms.skydining.model.TopUpRecord;
import com.enovax.star.cms.skydining.util.JcrFileUtil;
import com.enovax.util.jcr.exception.JcrException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
@Service
public class DefaultSkyDiningTopUpService extends BaseSkyDiningAdminService implements ISkyDiningTopUpService {
  @Override
  public int countTopUpRecords(String name, Boolean active) {
    return topUpRepo.count(name, active, null);
  }

  @Override
  public List<TopUpRecord> getTopUpRecords(String name, Boolean active,
                                           int start, int pageSize, String orderBy, boolean asc) {
    final List<TopUpRecord> topUpRecords = new ArrayList<TopUpRecord>();
    final List<SkyDiningTopUp> topUps = topUpRepo.findPaged(name, active,
      null, start, pageSize, orderBy, asc);

    for (final SkyDiningTopUp skyDiningTopUp : topUps) {
      final TopUpRecord topUpRecord = new TopUpRecord(skyDiningTopUp);
      topUpRecords.add(topUpRecord);
    }

    return topUpRecords;
  }

  @Override
  public TopUpModel getToUp(int id) {
    final SkyDiningTopUp topUp = topUpRepo.find(id);
    final TopUpModel topUpModel = new TopUpModel(topUp);

    final String oldImageHash = topUpModel.getImageHash();
    topUpModel.setImageHash(StringUtils.isEmpty(oldImageHash) ? ""
      : getImagePath()
      + oldImageHash);

    return topUpModel;
  }

  @Override
  public SaveResult saveDisplayImage(int id, File image, String fileName,
                                     String userId, String timestampText) {
    final SkyDiningTopUp entity = topUpRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining topUp.");
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final String savePath = "sky-dining-admin/top-ups";
    String imageHash;
    try {
      imageHash = JcrFileUtil.copyFile(image, savePath, fileName);
    } catch (final Exception e) {
      log.error("Error uploading sky dining topUp image file.", e);
      return new SaveResult(SaveResult.Result.Failed);
    }

    timestamp = new Date();

    entity.setImageHash(imageHash);
    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      topUpRepo.save(entity);
    } catch (JcrException e) {
      log.error("Unable to save top-up.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save top-up.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp, imageHash);
  }

  @Override
  public SaveResult saveTopUp(int id, String newName, Integer newType,
                              String newDescription, Integer newDisplayOrder,
                              String newEarlyBirdEndDate, String newStartDate, String newEndDate,
                              Integer newSalesCutOffDays, Integer newPrice, String newImageHash,
                              String userId, String timestampText) {

    SkyDiningTopUp entity = topUpRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      timestamp = new Date();
      entity = new SkyDiningTopUp();
      entity.setId(id);
      entity.setImageHash("");
      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
      timestamp = new Date();
    }

    if (newName != null) {
      entity.setName(newName);
    }
    if (newType != null) {
      entity.setType(newType);
    }
    if (newDescription != null) {
      entity.setDescription(newDescription);
    }
    if (newDisplayOrder != null) {
      entity.setDisplayOrder(newDisplayOrder);
    }
    if (newEarlyBirdEndDate != null) {
      entity.setEarlyBirdEndDate(parseDate(newEarlyBirdEndDate));
    }
    if (newStartDate != null) {
      entity.setStartDate(parseDate(newStartDate));
    }
    if (newEndDate != null) {
      entity.setEndDate(parseDate(newEndDate));
    }
    if (newSalesCutOffDays != null) {
      entity.setSalesCutOffDays(newSalesCutOffDays);
    }
    if (newPrice != null) {
      entity.setPrice(newPrice);
    }
    if (newImageHash != null) {
      entity.setImageHash(newImageHash);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      if (topUpRepo.save(entity)) {
        return new SaveResult(SaveResult.Result.Success, timestamp, entity.getId());
      }
    } catch (JcrException e) {
      log.error("Unable to save top-up.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save top-up.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }
}
