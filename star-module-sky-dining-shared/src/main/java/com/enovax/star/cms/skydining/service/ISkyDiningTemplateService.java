package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;

/**
 * Created by jace on 31/10/16.
 */
public interface ISkyDiningTemplateService {

    String generateReceiptHtml(Object model) throws Exception;

    boolean generateReceiptPdf(Object model, String path) throws Exception;

    SysEmailTemplateVM getSystemParamByKey(String appKey, String paramKey);

    boolean hasEmailTemplateByKey(String appKey, String handlebarsApproverAlert);
}
