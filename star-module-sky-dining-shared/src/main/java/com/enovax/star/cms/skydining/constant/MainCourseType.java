package com.enovax.star.cms.skydining.constant;

public enum MainCourseType {
  Standard(0), Kids(1), Vegetarian(2);

  public static MainCourseType valueOf(int i) {
    switch (i) {
      case 0:
        return Standard;
      case 1:
        return Kids;
      case 2:
        return Vegetarian;
      default:
        return null;
    }
  }

  public final int value;

  MainCourseType(int value) {
    this.value = value;
  }
}
