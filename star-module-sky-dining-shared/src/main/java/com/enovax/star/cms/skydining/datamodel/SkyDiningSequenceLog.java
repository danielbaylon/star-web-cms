package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "SkyDiningSequenceLog", schema = "dbo", catalog = "STAR_DB")
public class SkyDiningSequenceLog implements Serializable {

  private String seqId;
  private Date modifiedDate;

  @Id
  @Column(name = "seqId")
  public String getSeqId() {
    return seqId;
  }

  public void setSeqId(String seqId) {
    this.seqId = seqId;
  }

  @Column(name = "modifiedDate", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

}
