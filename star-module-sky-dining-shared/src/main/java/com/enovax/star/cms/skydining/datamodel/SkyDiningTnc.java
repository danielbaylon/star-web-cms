package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.Date;

/**
 * Created by jace on 21/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-tnc")
public class SkyDiningTnc implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-packages", propertyName = "tnc-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String title;
  @JcrProperty
  private String tncContent;
  @JcrProperty
  private Boolean selected;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getTncContent() {
    return tncContent;
  }

  public void setTncContent(String tncContent) {
    this.tncContent = tncContent;
  }

  public Boolean getSelected() {
    return selected;
  }

  public void setSelected(Boolean selected) {
    this.selected = selected;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }
}
