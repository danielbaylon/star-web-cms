package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 21/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-discount")
public class SkyDiningDiscount implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-packages", propertyName = "discount-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String name;
  @JcrProperty
  private Boolean active;
  @JcrProperty
  private String type;
  @JcrProperty
  private String merchantId;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;
  @JcrProperty
  private Integer priceType;
  @JcrProperty
  private Integer discountPrice;

  @JcrProperty
  private Date validFrom;
  @JcrProperty
  private Date validTo;
  @JcrProperty
  private String tnc;

  @JcrLink(linkedPath = "menus", linkedNodeType = "mgnl:sky-dining-menu", linkedWorkspace = "sky-dining-admin")
  private List<SkyDiningMenu> menus = new ArrayList<>(0);
  @JcrContent(subNodeType = "mgnl:sky-dining-promo-code")
  private List<SkyDiningPromoCode> promoCodes = new ArrayList<>(
    0);

  @JcrLink(linkedPath = "packages", linkedNodeType = "mgnl:sky-dining-package", linkedWorkspace = "sky-dining-admin")
  private SkyDiningPackage sdPackage;

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getMerchantId() {
    return merchantId;
  }

  public void setMerchantId(String merchantId) {
    this.merchantId = merchantId;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Integer getPriceType() {
    return priceType;
  }

  public void setPriceType(Integer priceType) {
    this.priceType = priceType;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public String getTnc() {
    return tnc;
  }

  public void setTnc(String tnc) {
    this.tnc = tnc;
  }

  public List<SkyDiningMenu> getMenus() {
    return menus;
  }

  public void setMenus(List<SkyDiningMenu> menus) {
    this.menus = menus;
  }

  public List<SkyDiningPromoCode> getPromoCodes() {
    return promoCodes;
  }

  public void setPromoCodes(List<SkyDiningPromoCode> promoCodes) {
    this.promoCodes = promoCodes;
  }

  public SkyDiningPackage getSdPackage() {
    return sdPackage;
  }

  public void setSdPackage(SkyDiningPackage sdPackage) {
    this.sdPackage = sdPackage;
  }
}
