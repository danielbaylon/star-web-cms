package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 30/7/16.
 */
public class SkyDiningMenuActionParam extends BaseSkyDiningActionParam {
  protected String newName;
  protected String newDescription;
  protected String newEarlyBirdEndDate;
  protected String newStartDate;
  protected String newEndDate;
  protected Integer newSalesCutOffDays;
  protected Integer newMenuType;
  protected Integer newPrice;
  protected Integer newServiceCharge;
  protected Integer newGst;
  protected String newCode;

  protected Integer newType;
  protected String nameFilter;

  protected Integer activeFilter;

  public String getNewName() {
    return newName;
  }

  public void setNewName(String newName) {
    this.newName = newName;
  }

  public String getNewDescription() {
    return newDescription;
  }

  public void setNewDescription(String newDescription) {
    this.newDescription = newDescription;
  }

  public String getNewEarlyBirdEndDate() {
    return newEarlyBirdEndDate;
  }

  public void setNewEarlyBirdEndDate(String newEarlyBirdEndDate) {
    this.newEarlyBirdEndDate = newEarlyBirdEndDate;
  }

  public String getNewStartDate() {
    return newStartDate;
  }

  public void setNewStartDate(String newStartDate) {
    this.newStartDate = newStartDate;
  }

  public String getNewEndDate() {
    return newEndDate;
  }

  public void setNewEndDate(String newEndDate) {
    this.newEndDate = newEndDate;
  }

  public Integer getNewSalesCutOffDays() {
    return newSalesCutOffDays;
  }

  public void setNewSalesCutOffDays(Integer newSalesCutOffDays) {
    this.newSalesCutOffDays = newSalesCutOffDays;
  }

  public Integer getNewMenuType() {
    return newMenuType;
  }

  public void setNewMenuType(Integer newMenuType) {
    this.newMenuType = newMenuType;
  }

  public Integer getNewPrice() {
    return newPrice;
  }

  public void setNewPrice(Integer newPrice) {
    this.newPrice = newPrice;
  }

  public Integer getNewServiceCharge() {
    return newServiceCharge;
  }

  public void setNewServiceCharge(Integer newServiceCharge) {
    this.newServiceCharge = newServiceCharge;
  }

  public Integer getNewGst() {
    return newGst;
  }

  public void setNewGst(Integer newGst) {
    this.newGst = newGst;
  }

  public String getNewCode() {
    return newCode;
  }

  public void setNewCode(String newCode) {
    this.newCode = newCode;
  }

  public Integer getNewType() {
    return newType;
  }

  public void setNewType(Integer newType) {
    this.newType = newType;
  }

  public String getNameFilter() {
    return nameFilter;
  }

  public void setNameFilter(String nameFilter) {
    this.nameFilter = nameFilter;
  }

  public Integer getActiveFilter() {
    return activeFilter;
  }

  public void setActiveFilter(Integer activeFilter) {
    this.activeFilter = activeFilter;
  }
}
