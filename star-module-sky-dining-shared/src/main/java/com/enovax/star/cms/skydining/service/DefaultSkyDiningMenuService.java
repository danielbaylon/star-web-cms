package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMainCourse;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMenu;
import com.enovax.star.cms.skydining.model.MainCourseModel;
import com.enovax.star.cms.skydining.model.MenuModel;
import com.enovax.star.cms.skydining.model.MenuRecord;
import com.enovax.star.cms.skydining.model.SaveResult;
import com.enovax.util.jcr.exception.JcrException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
@Service
public class DefaultSkyDiningMenuService extends BaseSkyDiningAdminService implements ISkyDiningMenuService {
  @Override
  public SaveResult addMainCoursesToMenu(int parentId, List<Integer> ids,
                                         String userId, String timestampText) {
    final SkyDiningMenu menu = menuRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (menu == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining menu.");
    } else {
      if (!isTimestampValid(menu, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    for (final int id : ids) {
      final SkyDiningMainCourse mainCourse = mainCourseRepo.find(id);
      if (mainCourse == null) {
        return new SaveResult(SaveResult.Result.Failed,
          "Unable to find sky dining main course(s).");
      } else {
        menu.getMainCourses().add(mainCourse);
      }
    }

    timestamp = new Date();
    menu.setModifiedBy(userId);
    menu.setModifiedDate(timestamp);
    try {
      menuRepo.save(menu);
    } catch (JcrException e) {
      log.error("Failed to save menu.", e);
      return new SaveResult(SaveResult.Result.Failed, "Failed to save menu.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public int countMenuRecords(String name, Boolean active) {
    return menuRepo.count(name, active, null, null, null);
  }

  @Override
  public int countSelectMainCourseRecords(Integer parentId) {
    return mainCourseRepo.count(parentId, false);
  }

  @Override
  public SaveResult deleteMainCourse(int id, String userId,
                                     String timestampText) {
    final SkyDiningMainCourse entity = mainCourseRepo.find(id);
    final Date timestamp = new Date();

    if (entity == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining main course.");
    }

    if (BooleanUtils.isNotTrue(entity.getDeleted())) {
      entity.setDeleted(true);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);
    try {
      mainCourseRepo.save(entity);
    } catch (JcrException e) {
      log.error("Failed to save main course.", e);
      return new SaveResult(SaveResult.Result.Failed, "Failed to save main course.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public List<MainCourseModel> getMainCourses(int parentId) {
    final List<MainCourseModel> mainCourses = new ArrayList<MainCourseModel>();
    final SkyDiningMenu menu = menuRepo.find(parentId);
    if (menu != null) {
      final List<SkyDiningMainCourse> dataModels = menu.getMainCourses();
      for (final SkyDiningMainCourse dataModel : dataModels) {
        final MainCourseModel mainCourse = new MainCourseModel(
          dataModel);
        mainCourses.add(mainCourse);
      }
    }
    return mainCourses;
  }

  @Override
  public MenuModel getMenu(int id) {
    return new MenuModel(menuRepo.find(id));
  }

  @Override
  public List<MenuRecord> getMenuRecords(String name, Boolean active,
                                         int start, int pageSize, String orderBy, boolean asc) {
    final List<MenuRecord> menuRecords = new ArrayList<MenuRecord>();
    final List<SkyDiningMenu> menus = menuRepo.findPaged(name, active, null,
      null, null, start, pageSize, orderBy, asc);

    for (final SkyDiningMenu menu : menus) {
      final MenuRecord menuRecord = new MenuRecord(menu);
      menuRecords.add(menuRecord);
    }

    return menuRecords;
  }

  @Override
  public List<MainCourseModel> getSelectMainCourseRecords(Integer parentId,
                                                          int start, int pageSize, String orderBy, boolean asc) {
    final List<MainCourseModel> mainCourseModels = new ArrayList<MainCourseModel>();

    if ((parentId == null) || (parentId < 1)) {
      return mainCourseModels;
    }

    final List<SkyDiningMainCourse> mainCourses = mainCourseRepo.findPaged(
      parentId, false, start, pageSize, orderBy, asc);

    for (final SkyDiningMainCourse mainCourse : mainCourses) {
      final MainCourseModel mainCourseModel = new MainCourseModel(
        mainCourse);
      mainCourseModels.add(mainCourseModel);
    }

    return mainCourseModels;
  }

  @Override
  public SaveResult removeMainCourses(int parentId, List<Integer> ids,
                                      String userId, String timestampText) {
    final SkyDiningMenu menu = menuRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (menu == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining menu.");
    } else {
      if (!isTimestampValid(menu, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningMainCourse> iterator = menu.getMainCourses()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningMainCourse mainCourse = iterator.next();

      for (final int id : ids) {
        if (mainCourse.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    menu.setModifiedBy(userId);
    menu.setModifiedDate(timestamp);
    try {
      menuRepo.save(menu);
    } catch (JcrException e) {
      log.error("Failed to save menu.", e);
      return new SaveResult(SaveResult.Result.Failed, "Failed to save menu.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult saveMainCourse(int parentId, int id, String newCode,
                                   Integer newType, String newDescription, String userId,
                                   String timestampText) {
    SkyDiningMainCourse entity = mainCourseRepo.find(id);
    final Date timestamp = new Date();

    if (entity == null) {
      entity = new SkyDiningMainCourse();
      entity.setId(0);
      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
    }

    if (newDescription != null) {
      entity.setDescription(newDescription);
    }
    if (newCode != null) {
      entity.setCode(newCode);
    }
    if (newType != null) {
      entity.setType(newType);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      if (mainCourseRepo.save(entity)) {
        return new SaveResult(SaveResult.Result.Success, timestamp,
          entity.getId());
      }
    } catch (JcrException e) {
      log.error("Failed to save main course.", e);
      return new SaveResult(SaveResult.Result.Failed, "Failed to save main course.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult saveMenu(int id, String newName, String newDescription,
                             String newEarlyBirdEndDate, String newStartDate, String newEndDate,
                             Integer newSalesCutOffDays, Integer newMenuType, Integer newPrice,
                             Integer newServiceCharge, Integer newGst, String userId,
                             String timestampText) {
    SkyDiningMenu entity = menuRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      timestamp = new Date();
      entity = new SkyDiningMenu();
      entity.setId(id);
      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
      timestamp = new Date();
    }

    if (newName != null) {
      entity.setName(newName);
    }
    if (newDescription != null) {
      entity.setDescription(newDescription);
    }
    if (newEarlyBirdEndDate != null) {
      entity.setEarlyBirdEndDate(parseDate(newEarlyBirdEndDate));
    }
    if (newStartDate != null) {
      entity.setStartDate(parseDate(newStartDate));
    }
    if (newEndDate != null) {
      entity.setEndDate(parseDate(newEndDate));
    }
    if (newSalesCutOffDays != null) {
      entity.setSalesCutOffDays(newSalesCutOffDays);
    }
    if (newMenuType != null) {
      entity.setMenuType(newMenuType);
    }
    if (newPrice != null) {
      entity.setPrice(newPrice);
    }
    if (newServiceCharge != null) {
      entity.setServiceCharge(newServiceCharge);
    }
    if (newGst != null) {
      entity.setGst(newGst);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      if (menuRepo.save(entity)) {
        return new SaveResult(SaveResult.Result.Success, timestamp,
          entity.getId());
      }
    } catch (JcrException e) {
      log.error("Failed to save menu.", e);
      return new SaveResult(SaveResult.Result.Failed, "Failed to save menu.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }
}
