package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.skydining.datamodel.SkyDiningJewelCard;

import java.io.InputStream;
import java.util.List;

/**
 * Created by jace on 28/9/16.
 */
public interface ISharedConfigService {
    int getJCMemberRecordCount(String searchMemberNumber, String searchJcFrom, String searchJcTo);

    List<SkyDiningJewelCard> getJCMemberRecords(String searchMemberNumber, String searchJcFrom, String searchJcTo,
                                                int start, int pageSize, String orderBy, boolean asc);

    void updateJCMembers(List<SkyDiningJewelCard> jcs, String userName) throws Exception;

    void updateJCMember(SkyDiningJewelCard jc, String userName) throws Exception;

    void deleteJCMember(List<Integer> ids) throws Exception;

    boolean jcMemberExist(SkyDiningJewelCard jc);

    void deleteJcFiltered(String searhMemberCard, String searchJcFrom, String searchJcTo);

    List<SkyDiningJewelCard> processCardXLS(InputStream inp) throws Exception;

    List<SkyDiningJewelCard> processCardXLSX(InputStream inp) throws Exception;

    String getSkyDiningReceiptRootDir();
}
