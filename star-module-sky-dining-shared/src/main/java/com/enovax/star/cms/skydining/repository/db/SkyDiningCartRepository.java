package com.enovax.star.cms.skydining.repository.db;

import com.enovax.star.cms.skydining.datamodel.SkyDiningCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jace on 26/8/16.
 */
@Repository
public interface SkyDiningCartRepository extends JpaRepository<SkyDiningCart, Integer> {

  SkyDiningCart findOneByWebSessionId(String webSessionId);

  Long deleteByWebSessionId(String webSessionId);
}
