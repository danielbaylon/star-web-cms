package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.jcrrepository.system.IEmailTemplateRepository;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.StringTemplateSource;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.github.jknack.handlebars.io.TemplateSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by jace on 31/10/16.
 */
@Service
public class DefaultSkyDiningTemplateService implements ISkyDiningTemplateService {


    private Logger logger = LoggerFactory.getLogger(DefaultSkyDiningTemplateService.class);

    public static final String CHANNEL = "b2c-mflg";
    public static final String TEMPLATE_SKY_DINING_RECEIPT = "sky-dining-receipt-handlebars";

    @Autowired
    private IEmailTemplateRepository mailRepo;

    private Handlebars hbars = null;
    private Template skyDiningReceiptTemplate = null;

    @PostConstruct
    public void init() throws IOException {
        final TemplateLoader loader = new TemplateLoader() {
            public TemplateSource sourceAt(String s) throws IOException {
                SysEmailTemplateVM vm = getSystemParamByKey(s);
                final String fileName = s;
                final String content = vm.getBody();
                return new StringTemplateSource(fileName, content);
            }

            public String resolve(String s) {
                return "";
            }

            public String getPrefix() {
                return "";
            }

            public String getSuffix() {
                return "";
            }

            public void setPrefix(String s) {
            }

            public void setSuffix(String s) {
            }
        };
        this.hbars = new Handlebars(loader);
        initTemplates();
    }

    private void initTemplates() {
        this.skyDiningReceiptTemplate = initTemplateByEmailId(TEMPLATE_SKY_DINING_RECEIPT);
    }

    private Template initTemplateByEmailId(String key) {
        if (key != null && key.trim().length() > 0 && hasEmailTemplateByKey(key)) {
            try {
                return hbars.compile(key);
            } catch (Exception ex) {
                logger.error("[Critical] - init email template failed for " + key + ", error message : " + ex.getMessage(), ex);
            }
        } else {
            logger.error("[Critical] - No email template found in JCR for " + key);
        }
        return null;
    }

    @Override
    public String generateReceiptHtml(Object model) throws Exception {
        return generateHtml(skyDiningReceiptTemplate, model);
    }

    @Override
    public boolean generateReceiptPdf(Object model, String path) throws Exception {
        return generatePdf(skyDiningReceiptTemplate, model, path);
    }

    public boolean hasEmailTemplateByKey(String template) {
        return mailRepo.hasSystemParamByKey(CHANNEL, template);
    }

    public SysEmailTemplateVM getSystemParamByKey(String template) {
        return mailRepo.getSystemParamByKey(CHANNEL, template);
    }

    private String generateHtml(Template template, Object model) throws Exception {
        final Context ctx = Context.newContext(model);
        String htmlStr = template.apply(ctx);
        return htmlStr;
    }

    private boolean generatePdf(Template template, Object viewModel,
                                String outputFilePath) throws Exception {
        final ITextRenderer renderer = new ITextRenderer();
        String html = generateHtml(template, viewModel);
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(new FileOutputStream(new File(outputFilePath)), true);
        return true;
    }

    public boolean generatePdfByHtml(String key, String html, String outputFilePath) {
        final ITextRenderer renderer = new ITextRenderer();
        File file = null;

        FileOutputStream fos = null;
        try {
            renderer.setDocumentFromString(html);
            renderer.layout();
            file = new File(outputFilePath);
            fos = new FileOutputStream(file);
            renderer.createPDF(fos, true);
            fos.flush();
        } catch (Exception ex) {
            logger.error("generatePdfByHtml - Exception during generate pdf for " + key + " is failed, error message : " + ex.getMessage(), ex);
        } catch (Throwable tb) {
            logger.error("generatePdfByHtml - Throwable during generate pdf for " + key + " is failed, error message : " + tb.getMessage(), tb);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception ex) {
                    logger.error("generatePdfByHtml - close FileOutputStream during generate pdf for " + key + " is failed");
                } catch (Throwable tb2) {
                    logger.error("generatePdfByHtml - close FileOutputStream during generate pdf for " + key + " is failed");
                }
            }
        }
        //System.out.println(html);
        if (file != null && file.isFile() && file.exists() && file.canRead()) {
            return true;
        }
        logger.error("close FileOutputStream during generate pdf for " + key + " is failed");
        return false;

    }

    public String processEmailParams(String content, Map<String, String> paramMap) {
        if (content == null || content.trim().length() == 0 || paramMap == null || paramMap.size() == 0) {
            return content;
        }
        Iterator<Map.Entry<String, String>> iter = paramMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = iter.next();
            content = content.replace(getNonEmptyStringKey(entry.getKey()), getNonEmptyStringValue(entry.getValue()));
        }
        return content;
    }

    private String getNonEmptyStringKey(String key) {
        if (key != null && key.trim().length() > 0) {
            return "##" + (key.trim()) + "##";
        }
        return "####";
    }

    private String getNonEmptyStringValue(String val) {
        if (val != null && val.trim().length() > 0) {
            return val.trim();
        }
        return "";
    }

    @Override
    public SysEmailTemplateVM getSystemParamByKey(String appKey, String paramKey) {
        return mailRepo.getSystemParamByKey(appKey, paramKey);
    }

    @Override
    public boolean hasEmailTemplateByKey(String appKey, String paramKey) {
        return mailRepo.hasSystemParamByKey(appKey, paramKey);
    }
}
