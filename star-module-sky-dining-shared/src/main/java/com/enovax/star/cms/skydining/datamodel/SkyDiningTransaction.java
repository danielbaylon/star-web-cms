package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(catalog = "STAR_DB", name = "SkyDiningTransaction", schema = "dbo")
public class SkyDiningTransaction implements HasTimestamp {

  private int id;
  private String receiptNum;
  private String custName;
  private String custSalutation;
  private String custCompany;
  private String custEmail;
  private String custIdType;
  private String custIdNo;
  private String custMobile;
  private Boolean custSubscribe;
  private String custJewelCard;
  private String paymentType;
  private BigDecimal totalAmount;
  private String currency;
  private String bookFeeType;
  private Integer bookFeeCents;
  private Integer bookFeeItemCode;
  private Boolean bookFeeWaived;
  private String bookFeePromoCode;
  private String tmMerchantId;
  private Date statusDate;
  private String errorCode;
  private String stage;
  private String sactResId;
  private String sactPin;
  private String tmStatus;
  private String tmErrorMessage;
  private String tmApprovalCode;
  private String createdBy;
  private Date createdDate;
  private String modifiedBy;
  private Date modifiedDate;

  private SkyDiningReservation reservation;

  @Column(name = "bookFeeCents", nullable = true)
  public Integer getBookFeeCents() {
    return bookFeeCents;
  }

  @Column(name = "bookFeeItemCode", nullable = true)
  public Integer getBookFeeItemCode() {
    return bookFeeItemCode;
  }

  @Column(name = "bookFeePromoCode", nullable = true, length = 50)
  public String getBookFeePromoCode() {
    return bookFeePromoCode;
  }

  @Column(name = "bookFeeType", nullable = false, length = 20)
  public String getBookFeeType() {
    return bookFeeType;
  }

  @Column(name = "bookFeeWaived", nullable = true)
  public Boolean getBookFeeWaived() {
    return bookFeeWaived;
  }

  @Override
  @Column(name = "createdBy", nullable = false, length = 100)
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  @Column(name = "createdDate", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  public Date getCreatedDate() {
    return createdDate;
  }

  @Column(name = "currency", nullable = false, length = 3)
  public String getCurrency() {
    return currency;
  }

  @Column(name = "custCompany", nullable = true, length = 500)
  public String getCustCompany() {
    return custCompany;
  }

  @Column(name = "custEmail", nullable = false, length = 500)
  public String getCustEmail() {
    return custEmail;
  }

  @Column(name = "custIdNo", nullable = false, length = 100)
  public String getCustIdNo() {
    return custIdNo;
  }

  @Column(name = "custIdType", nullable = false, length = 50)
  public String getCustIdType() {
    return custIdType;
  }

  @Column(name = "custJewelCard", nullable = true, length = 50)
  public String getCustJewelCard() {
    return custJewelCard;
  }

  @Column(name = "custMobile", nullable = true, length = 100)
  public String getCustMobile() {
    return custMobile;
  }

  @Column(name = "custName", nullable = false, length = 500)
  public String getCustName() {
    return custName;
  }

  @Column(name = "custSalutation", nullable = true, length = 20)
  public String getCustSalutation() {
    return custSalutation;
  }

  @Column(name = "custSubscribe", nullable = false)
  public Boolean getCustSubscribe() {
    return custSubscribe;
  }

  @Column(name = "errorCode", nullable = true, length = 50)
  public String getErrorCode() {
    return errorCode;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Override
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  @Override
  @Column(name = "modifiedBy", nullable = false, length = 100)
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  @Column(name = "modifiedDate", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Column(name = "paymentType", nullable = false, length = 50)
  public String getPaymentType() {
    return paymentType;
  }

  @Column(name = "receiptNum", nullable = false, length = 50)
  public String getReceiptNum() {
    return receiptNum;
  }

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "reservationId", referencedColumnName = "id", nullable = false)
  public SkyDiningReservation getReservation() {
    return reservation;
  }

  @Column(name = "sactPin", nullable = true, length = 50)
  public String getSactPin() {
    return sactPin;
  }

  @Column(name = "sactResId", nullable = true, length = 50)
  public String getSactResId() {
    return sactResId;
  }

  @Column(name = "stage", nullable = false, length = 50)
  public String getStage() {
    return stage;
  }

  @Column(name = "statusDate", nullable = true)
  public Date getStatusDate() {
    return statusDate;
  }

  @Column(name = "tmApprovalCode", nullable = true, length = 50)
  public String getTmApprovalCode() {
    return tmApprovalCode;
  }

  @Column(name = "tmErrorMessage", nullable = true, length = 1000)
  public String getTmErrorMessage() {
    return tmErrorMessage;
  }

  @Column(name = "tmMerchantId", nullable = false, length = 50)
  public String getTmMerchantId() {
    return tmMerchantId;
  }

  @Column(name = "tmStatus", nullable = true, length = 50)
  public String getTmStatus() {
    return tmStatus;
  }

  @Column(name = "totalAmount", nullable = false)
  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setBookFeeCents(Integer bookFeeCents) {
    this.bookFeeCents = bookFeeCents;
  }

  public void setBookFeeItemCode(Integer bookFeeItemCode) {
    this.bookFeeItemCode = bookFeeItemCode;
  }

  public void setBookFeePromoCode(String bookFeePromoCode) {
    this.bookFeePromoCode = bookFeePromoCode;
  }

  public void setBookFeeType(String bookFeeType) {
    this.bookFeeType = bookFeeType;
  }

  public void setBookFeeWaived(Boolean bookFeeWaived) {
    this.bookFeeWaived = bookFeeWaived;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public void setCustCompany(String custCompany) {
    this.custCompany = custCompany;
  }

  public void setCustEmail(String custEmail) {
    this.custEmail = custEmail;
  }

  public void setCustIdNo(String custIdNo) {
    this.custIdNo = custIdNo;
  }

  public void setCustIdType(String custIdType) {
    this.custIdType = custIdType;
  }

  public void setCustJewelCard(String custJewelCard) {
    this.custJewelCard = custJewelCard;
  }

  public void setCustMobile(String custMobile) {
    this.custMobile = custMobile;
  }

  public void setCustName(String custName) {
    this.custName = custName;
  }

  public void setCustSalutation(String custSalutation) {
    this.custSalutation = custSalutation;
  }

  public void setCustSubscribe(Boolean custSubscribe) {
    this.custSubscribe = custSubscribe;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public void setPaymentType(String paymentType) {
    this.paymentType = paymentType;
  }

  public void setReceiptNum(String receiptNum) {
    this.receiptNum = receiptNum;
  }

  public void setReservation(SkyDiningReservation reservation) {
    this.reservation = reservation;
  }

  public void setSactPin(String sactPin) {
    this.sactPin = sactPin;
  }

  public void setSactResId(String sactResId) {
    this.sactResId = sactResId;
  }

  public void setStage(String stage) {
    this.stage = stage;
  }

  public void setStatusDate(Date statusDate) {
    this.statusDate = statusDate;
  }

  public void setTmApprovalCode(String tmApprovalCode) {
    this.tmApprovalCode = tmApprovalCode;
  }

  public void setTmErrorMessage(String tmErrorMessage) {
    this.tmErrorMessage = tmErrorMessage;
  }

  public void setTmMerchantId(String tmMerchantId) {
    this.tmMerchantId = tmMerchantId;
  }

  public void setTmStatus(String tmStatus) {
    this.tmStatus = tmStatus;
  }

  public void setTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
  }

}
