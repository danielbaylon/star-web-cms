package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(catalog = "STAR_DB", name = "SkyDiningCartMainCourse", schema = "dbo")
public class SkyDiningCartMainCourse implements Serializable {

  private int id;
  private int quantity;

  private Integer mainCourseId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  @Column(name = "quantity", nullable = false)
  public int getQuantity() {
    return quantity;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Column(name = "mainCourseId", nullable = false)
  public Integer getMainCourseId() {
    return mainCourseId;
  }

  public void setMainCourseId(Integer mainCourseId) {
    this.mainCourseId = mainCourseId;
  }

}
