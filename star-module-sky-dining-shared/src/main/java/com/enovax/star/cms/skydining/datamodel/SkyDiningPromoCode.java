package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.Date;

/**
 * Created by jace on 21/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-promo-code")
public class SkyDiningPromoCode implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-packages", propertyName = "promo-code-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String code;
  @JcrProperty
  private Boolean hasLimit;
  @JcrProperty
  private Integer limit;
  @JcrProperty
  private Integer timesUsed;
  @JcrProperty
  private Boolean active;
  @JcrProperty
  private String type;
  @JcrProperty
  private Date startDate;
  @JcrProperty
  private Date endDate;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;

  @JcrLink(linkedPath = "", linkedNodeType = "mgnl:sky-dining-discount", linkedWorkspace = "sky-dining-admin")
  private SkyDiningDiscount discount;

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Boolean getHasLimit() {
    return hasLimit;
  }

  public void setHasLimit(Boolean hasLimit) {
    this.hasLimit = hasLimit;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public Integer getTimesUsed() {
    return timesUsed;
  }

  public void setTimesUsed(Integer timesUsed) {
    this.timesUsed = timesUsed;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public SkyDiningDiscount getDiscount() {
    return discount;
  }

  public void setDiscount(SkyDiningDiscount discount) {
    this.discount = discount;
  }
}
