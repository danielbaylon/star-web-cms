package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.jcrrepository.merchant.IMerchantRepository;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.HasTimestamp;
import com.enovax.star.cms.skydining.repository.db.SkyDiningJewelCardRepository;
import com.enovax.star.cms.skydining.repository.db.SkyDiningReservationRepository;
import com.enovax.star.cms.skydining.repository.db.SkyDiningTransactionRepository;
import com.enovax.star.cms.skydining.repository.jcr.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.text.ParseException;
import java.util.Date;


/**
 * Created by jace on 20/7/16.
 */
public abstract class BaseSkyDiningAdminService {

  protected Logger log = LoggerFactory.getLogger(BaseSkyDiningAdminService.class);

  @Autowired
  protected ISkyDiningSessionRepository sessionRepo;
  @Autowired
  protected ISkyDiningPackageRepository packageRepo;
  @Autowired
  protected ISkyDiningMenuRepository menuRepo;
  @Autowired
  protected ISkyDiningTopUpRepository topUpRepo;
  @Autowired
  protected ISkyDiningMainCourseRepository mainCourseRepo;
  @Autowired
  protected ISkyDiningDiscountRepository discountRepo;
  @Autowired
  protected ISkyDiningPromoCodeRepository promoCodeRepo;
  @Autowired
  protected ISkyDiningTncRepository tncRepo;
  @Autowired
  protected ISkyDiningThemeRepository themeRepo;

  @Autowired
  protected SkyDiningReservationRepository reservationRepo;
  @Autowired
  protected SkyDiningJewelCardRepository jcRepo;

  @Autowired
  protected IMerchantRepository merchantRepo;

  @Autowired
  protected SkyDiningTransactionRepository transRepo;

  protected boolean isTimestampValid(HasTimestamp entity, Date timestamp) {
    //TODO: Investigate why the milliseconds are different
    if (entity != null && timestamp != null && entity.getModifiedDate() != null) {
      Long x = entity.getModifiedDate().getTime();
      Long y = timestamp.getTime();
      boolean a = entity.getModifiedDate().before(timestamp);
      boolean b = entity.getModifiedDate().after(timestamp);
      boolean c = entity.getModifiedDate().equals(timestamp);
      return true;
    }
    return true;
  }

  protected boolean isTimestampValid(HasTimestamp entity, String timestampText) {
    final Date timestamp = NvxDateUtils.parseModDt(timestampText);
    return isTimestampValid(entity, timestamp);
  }

  protected Date parseDate(String dateString) {
    try {
      return NvxDateUtils.parseDate(dateString, SysConst.DEFAULT_DATE_FORMAT);
    } catch (final ParseException e) {
      log.error("Error parsing date: " + dateString);
      return null;
    }
  }

  protected Time parseTime(String timeString) {
    try {
      return NvxDateUtils.parseTime(timeString, SysConst.DEFAULT_TIME_FORMAT);
    } catch (final ParseException e) {
      log.error("Error parsing date: " + timeString);
      return null;
    }
  }

  //TODO: Implement Upload Image
  protected String getImagePath() {
    return "";
  }
}
