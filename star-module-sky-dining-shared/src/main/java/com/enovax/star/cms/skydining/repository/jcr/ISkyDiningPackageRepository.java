package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;
import com.enovax.util.jcr.repository.JcrRepository;
import info.magnolia.jcr.util.ContentMap;

import java.util.Date;
import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
public interface ISkyDiningPackageRepository extends JcrRepository<SkyDiningPackage> {

    int count(String name, Boolean active, Integer sessionId,
              Date dateOfBooking, Date dateOfVisit);

    List<SkyDiningPackage> findPaged(String name, Boolean active,
                                     Integer sessionId, Date dateOfBooking, Date dateOfVisit, int start,
                                     int pageSize, String orderBy, boolean asc);

    SkyDiningPackage getParentPackage(SkyDiningDiscount pc);

    ContentMap getPackageTnc(String packageId);
}
