package com.enovax.star.cms.skydining.constant;

public enum ImageFileTypes {
  PNG("image/png"),
  JPG("image/jpeg"),
  GIF("image/gif");

  public final String contentType;

  ImageFileTypes(String contentType) {
    this.contentType = contentType;
  }

  @Override
  public String toString() {
    return contentType;
  }

  public static boolean isImage(String testType) {
    return PNG.contentType.equals(testType)
      || JPG.contentType.equals(testType)
      || GIF.contentType.equals(testType);
  }
}
