package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 10/11/16.
 */
public class SkyDiningReportActionParam extends BaseSkyDiningActionParam {

    protected String startDate;
    protected String endDate;

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }
}
