package com.enovax.star.cms.skydining.model.booking;

import com.enovax.star.cms.commons.model.booking.TelemoneyProcessingData;
import com.enovax.star.cms.commons.model.booking.skydining.SkyDiningFunCart;
import com.enovax.star.cms.commons.model.booking.skydining.SkyDiningStoreTransaction;
import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTransaction;

public class TelemoneyProcessingDataSkyDining extends TelemoneyProcessingData {

    private SkyDiningReservation sdRes;
    private SkyDiningTransaction sdTxn;
    private SkyDiningStoreTransaction txn;
    private SkyDiningFunCart cart;

    public SkyDiningReservation getSdRes() {
        return sdRes;
    }

    public void setSdRes(SkyDiningReservation sdRes) {
        this.sdRes = sdRes;
    }

    public SkyDiningTransaction getSdTxn() {
        return sdTxn;
    }

    public void setSdTxn(SkyDiningTransaction sdTxn) {
        this.sdTxn = sdTxn;
    }

    public SkyDiningStoreTransaction getTxn() {
        return txn;
    }

    public void setTxn(SkyDiningStoreTransaction txn) {
        this.txn = txn;
    }

    public SkyDiningFunCart getCart() {
        return cart;
    }

    public void setCart(SkyDiningFunCart cart) {
        this.cart = cart;
    }
}
