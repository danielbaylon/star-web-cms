package com.enovax.star.cms.skydining.model.result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jace on 4/10/16.
 */
public class SkyDiningCalendarResult {

    private Integer minBookingDays = 1;
    private List<String> blackoutDates = new ArrayList<>();
    private List<Integer[]> blackoutDatesArr = new ArrayList<>();
    private String minDate = "";
    private Integer[] minDateArr = new Integer[]{0, 0, 0};
    private String maxDate = "";
    private Integer[] maxDateArr = new Integer[]{0, 0, 0};

    public Integer getMinBookingDays() {
        return minBookingDays;
    }

    public void setMinBookingDays(Integer minBookingDays) {
        this.minBookingDays = minBookingDays;
    }

    public List<String> getBlackoutDates() {
        return blackoutDates;
    }

    public void setBlackoutDates(List<String> blackoutDates) {
        this.blackoutDates = blackoutDates;
    }

    public List<Integer[]> getBlackoutDatesArr() {
        return blackoutDatesArr;
    }

    public void setBlackoutDatesArr(List<Integer[]> blackoutDatesArr) {
        this.blackoutDatesArr = blackoutDatesArr;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public Integer[] getMinDateArr() {
        return minDateArr;
    }

    public void setMinDateArr(Integer[] minDateArr) {
        this.minDateArr = minDateArr;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public Integer[] getMaxDateArr() {
        return maxDateArr;
    }

    public void setMaxDateArr(Integer[] maxDateArr) {
        this.maxDateArr = maxDateArr;
    }

}
