package com.enovax.star.cms.skydining.constant;

public enum ReservationStatus {
  Cancelled(0), PaidInsideSystem(1), Unpaid(2), PaidOutsideSystem(3),;

  public static ReservationStatus valueOf(int i) {
    switch (i) {
      case 0:
        return Cancelled;
      case 1:
        return PaidInsideSystem;
      case 2:
        return Unpaid;
      case 3:
        return PaidOutsideSystem;
      default:
        return null;
    }
  }

  public final int value;

  ReservationStatus(int value) {
    this.value = value;
  }

}
