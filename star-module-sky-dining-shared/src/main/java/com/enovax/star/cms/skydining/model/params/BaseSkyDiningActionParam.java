package com.enovax.star.cms.skydining.model.params;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jace on 25/7/16.
 */
public abstract class BaseSkyDiningActionParam implements Serializable {

  protected Integer pageSize;
  protected Integer id;
  protected String modelData;
  protected Integer newId;
  protected String result;
  protected String timestamp;
  protected String idList;
  protected Integer parentId;
  protected String imageHash;

  private String adminUn;

  protected Integer take;
  protected Integer skip;
  protected Integer page;
  protected List<SortModel> sort;
  protected String orderBy;
  protected Boolean asc;

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getModelData() {
    return modelData;
  }

  public void setModelData(String modelData) {
    this.modelData = modelData;
  }

  public Integer getNewId() {
    return newId;
  }

  public void setNewId(Integer newId) {
    this.newId = newId;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getIdList() {
    return idList;
  }

  public void setIdList(String idList) {
    this.idList = idList;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public String getImageHash() {
    return imageHash;
  }

  public void setImageHash(String imageHash) {
    this.imageHash = imageHash;
  }

  public String getAdminUn() {
    return adminUn;
  }

  public void setAdminUn(String adminUn) {
    this.adminUn = adminUn;
  }

  public Integer getTake() {
    return take;
  }

  public void setTake(Integer take) {
    this.take = take;
  }

  public Integer getSkip() {
    return skip;
  }

  public void setSkip(Integer skip) {
    this.skip = skip;
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public List<SortModel> getSort() {
    return sort;
  }

  public void setSort(List<SortModel> sort) {
    this.sort = sort;
  }

  public String getOrderBy() {
    return orderBy;
  }

  public void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }

  public Boolean getAsc() {
    return asc;
  }

  public void setAsc(Boolean asc) {
    this.asc = asc;
  }
}
