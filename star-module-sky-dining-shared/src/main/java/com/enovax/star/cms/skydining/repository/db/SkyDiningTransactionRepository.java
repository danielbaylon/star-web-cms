package com.enovax.star.cms.skydining.repository.db;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jace on 31/8/16.
 */
@Repository
public interface SkyDiningTransactionRepository extends JpaRepository<SkyDiningTransaction, Integer> {

    SkyDiningTransaction findOneByReceiptNum(String receiptNum);

}
