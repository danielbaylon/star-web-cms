package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.skydining.model.*;

import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
public interface ISkyDiningSessionService {
  SaveResult addPackagesToSession(int parentId, List<Integer> ids,
                                  String userId, String timestampText);

  int countScheduleRecords();

  int countSelectPackageRecords(Integer sessionId, String name, Boolean active);

  List<BlockOutDateModel> getBlockOutDates(int parentId);

  List<PackageRecord> getPackageRecords(int parentId);

  List<PackageRecord> getSelectPackageRecords(Integer sessionId, String name,
                                              Boolean active, int start, int pageSize, String orderBy, boolean asc);

  SessionModel getSession(int id);

  List<SessionRecord> getSessionRecords(int start, int pageSize,
                                        String orderBy, boolean asc);

  SaveResult removeBlockOutDates(int parentId, List<Integer> ids,
                                 String userId, String timestampText);

  SaveResult removePackagesFromSession(int parentId, List<Integer> ids,
                                       String userId, String timestampText);

  SaveResult removeSessions(List<Integer> ids);

  SaveResult saveBlockOutDate(int parentId, int id, String newStartDate,
                              String newEndDate, String userId, String timestampText);

  SaveResult saveSession(int id, String newName, Integer newSessionNumber,
                         Integer newSessionType, String newEffectiveDays,
                         String newStartDate, String newEndDate, String newTime,
                         String newSalesStartDate, String newSalesEndDate,
                         Integer newCapacity, Boolean newActive, String userId,
                         String timestampText);

  SaveResult toggleStatus(int id, String userId, String timestampText);
}
