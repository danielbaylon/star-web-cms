package com.enovax.star.cms.skydining.model.params;

import com.enovax.star.cms.skydining.model.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jace on 25/8/16.
 */
public class SkyDiningReservationActionParam extends BaseSkyDiningActionParam {

  private Integer packageId;
  private Integer menuId;
  private String date;
  private String time;

  private List<TransactionStatus> tranStatuses;
  private List<PackageRecord> packageData;
  private List<SessionRecord> sessionData;
  private List<ThemeModel> themeData;
  private List<ResTopUpModel> topUpData;
  private List<ResMainCourseModel> mainCourseData;
  private List<MenuRecord> menuData;
  private List<ReservationRecord> reservationData;

  protected String newGuestName;
  protected String newContactNumber;
  protected String newEmailAddress;
  protected String newDate;
  protected String newTime;
  protected Integer newPax;
  protected Integer newPackageId;
  protected Integer newSessionId;
  protected Integer newThemeId;
  protected Integer newMenuId;
  protected String newSpecialRequest;
  protected Integer newType;
  protected Integer newStatus;
  protected Integer newDiscountId;
  protected String newJewelCard;
  protected String newJewelCardExpiry;
  protected String newPromoCode;
  protected String newMerchantId;
  protected String newReceiptNum;
  protected String newReceivedBy;
  protected String newPaymentDate;
  protected Integer newPaymentMode;
  protected BigDecimal newPaidAmount;

  protected String newTopUpIds;
  protected String newMainCourseIds;
  protected String newTopUpQuantities;
  protected String newMainCourseQuantities;

  private String transactionStartDate;
  private String transactionEndDate;
  private String transactionStatus;
  private String receiptNumber;
  private String packageName;
  private String dateOfVisit;
  private Integer reservationType;
  private String guestName;

  private Integer reservationStatus;

  public Integer getPackageId() {
    return packageId;
  }

  public void setPackageId(Integer packageId) {
    this.packageId = packageId;
  }

  public Integer getMenuId() {
    return menuId;
  }

  public void setMenuId(Integer menuId) {
    this.menuId = menuId;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public List<TransactionStatus> getTranStatuses() {
    return tranStatuses;
  }

  public void setTranStatuses(List<TransactionStatus> tranStatuses) {
    this.tranStatuses = tranStatuses;
  }

  public List<PackageRecord> getPackageData() {
    return packageData;
  }

  public void setPackageData(List<PackageRecord> packageData) {
    this.packageData = packageData;
  }

  public List<SessionRecord> getSessionData() {
    return sessionData;
  }

  public void setSessionData(List<SessionRecord> sessionData) {
    this.sessionData = sessionData;
  }

  public List<ThemeModel> getThemeData() {
    return themeData;
  }

  public void setThemeData(List<ThemeModel> themeData) {
    this.themeData = themeData;
  }

  public List<ResTopUpModel> getTopUpData() {
    return topUpData;
  }

  public void setTopUpData(List<ResTopUpModel> topUpData) {
    this.topUpData = topUpData;
  }

  public List<ResMainCourseModel> getMainCourseData() {
    return mainCourseData;
  }

  public void setMainCourseData(List<ResMainCourseModel> mainCourseData) {
    this.mainCourseData = mainCourseData;
  }

  public List<MenuRecord> getMenuData() {
    return menuData;
  }

  public void setMenuData(List<MenuRecord> menuData) {
    this.menuData = menuData;
  }

  public List<ReservationRecord> getReservationData() {
    return reservationData;
  }

  public void setReservationData(List<ReservationRecord> reservationData) {
    this.reservationData = reservationData;
  }

  public String getNewGuestName() {
    return newGuestName;
  }

  public void setNewGuestName(String newGuestName) {
    this.newGuestName = newGuestName;
  }

  public String getNewContactNumber() {
    return newContactNumber;
  }

  public void setNewContactNumber(String newContactNumber) {
    this.newContactNumber = newContactNumber;
  }

  public String getNewEmailAddress() {
    return newEmailAddress;
  }

  public void setNewEmailAddress(String newEmailAddress) {
    this.newEmailAddress = newEmailAddress;
  }

  public String getNewDate() {
    return newDate;
  }

  public void setNewDate(String newDate) {
    this.newDate = newDate;
  }

  public String getNewTime() {
    return newTime;
  }

  public void setNewTime(String newTime) {
    this.newTime = newTime;
  }

  public Integer getNewPax() {
    return newPax;
  }

  public void setNewPax(Integer newPax) {
    this.newPax = newPax;
  }

  public Integer getNewPackageId() {
    return newPackageId;
  }

  public void setNewPackageId(Integer newPackageId) {
    this.newPackageId = newPackageId;
  }

  public Integer getNewSessionId() {
    return newSessionId;
  }

  public void setNewSessionId(Integer newSessionId) {
    this.newSessionId = newSessionId;
  }

  public Integer getNewThemeId() {
    return newThemeId;
  }

  public void setNewThemeId(Integer newThemeId) {
    this.newThemeId = newThemeId;
  }

  public Integer getNewMenuId() {
    return newMenuId;
  }

  public void setNewMenuId(Integer newMenuId) {
    this.newMenuId = newMenuId;
  }

  public String getNewSpecialRequest() {
    return newSpecialRequest;
  }

  public void setNewSpecialRequest(String newSpecialRequest) {
    this.newSpecialRequest = newSpecialRequest;
  }

  public Integer getNewType() {
    return newType;
  }

  public void setNewType(Integer newType) {
    this.newType = newType;
  }

  public Integer getNewStatus() {
    return newStatus;
  }

  public void setNewStatus(Integer newStatus) {
    this.newStatus = newStatus;
  }

  public Integer getNewDiscountId() {
    return newDiscountId;
  }

  public void setNewDiscountId(Integer newDiscountId) {
    this.newDiscountId = newDiscountId;
  }

  public String getNewJewelCard() {
    return newJewelCard;
  }

  public void setNewJewelCard(String newJewelCard) {
    this.newJewelCard = newJewelCard;
  }

  public String getNewJewelCardExpiry() {
    return newJewelCardExpiry;
  }

  public void setNewJewelCardExpiry(String newJewelCardExpiry) {
    this.newJewelCardExpiry = newJewelCardExpiry;
  }

  public String getNewPromoCode() {
    return newPromoCode;
  }

  public void setNewPromoCode(String newPromoCode) {
    this.newPromoCode = newPromoCode;
  }

  public String getNewMerchantId() {
    return newMerchantId;
  }

  public void setNewMerchantId(String newMerchantId) {
    this.newMerchantId = newMerchantId;
  }

  public String getNewReceiptNum() {
    return newReceiptNum;
  }

  public void setNewReceiptNum(String newReceiptNum) {
    this.newReceiptNum = newReceiptNum;
  }

  public String getNewReceivedBy() {
    return newReceivedBy;
  }

  public void setNewReceivedBy(String newReceivedBy) {
    this.newReceivedBy = newReceivedBy;
  }

  public String getNewPaymentDate() {
    return newPaymentDate;
  }

  public void setNewPaymentDate(String newPaymentDate) {
    this.newPaymentDate = newPaymentDate;
  }

  public Integer getNewPaymentMode() {
    return newPaymentMode;
  }

  public void setNewPaymentMode(Integer newPaymentMode) {
    this.newPaymentMode = newPaymentMode;
  }

  public BigDecimal getNewPaidAmount() {
    return newPaidAmount;
  }

  public void setNewPaidAmount(BigDecimal newPaidAmount) {
    this.newPaidAmount = newPaidAmount;
  }

  public String getNewTopUpIds() {
    return newTopUpIds;
  }

  public void setNewTopUpIds(String newTopUpIds) {
    this.newTopUpIds = newTopUpIds;
  }

  public String getNewMainCourseIds() {
    return newMainCourseIds;
  }

  public void setNewMainCourseIds(String newMainCourseIds) {
    this.newMainCourseIds = newMainCourseIds;
  }

  public String getNewTopUpQuantities() {
    return newTopUpQuantities;
  }

  public void setNewTopUpQuantities(String newTopUpQuantities) {
    this.newTopUpQuantities = newTopUpQuantities;
  }

  public String getNewMainCourseQuantities() {
    return newMainCourseQuantities;
  }

  public void setNewMainCourseQuantities(String newMainCourseQuantities) {
    this.newMainCourseQuantities = newMainCourseQuantities;
  }

  public String getTransactionStartDate() {
    return transactionStartDate;
  }

  public void setTransactionStartDate(String transactionStartDate) {
    this.transactionStartDate = transactionStartDate;
  }

  public String getTransactionEndDate() {
    return transactionEndDate;
  }

  public void setTransactionEndDate(String transactionEndDate) {
    this.transactionEndDate = transactionEndDate;
  }

  public String getTransactionStatus() {
    return transactionStatus;
  }

  public void setTransactionStatus(String transactionStatus) {
    this.transactionStatus = transactionStatus;
  }

  public String getReceiptNumber() {
    return receiptNumber;
  }

  public void setReceiptNumber(String receiptNumber) {
    this.receiptNumber = receiptNumber;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public String getDateOfVisit() {
    return dateOfVisit;
  }

  public void setDateOfVisit(String dateOfVisit) {
    this.dateOfVisit = dateOfVisit;
  }

  public Integer getReservationType() {
    return reservationType;
  }

  public void setReservationType(Integer reservationType) {
    this.reservationType = reservationType;
  }

  public String getGuestName() {
    return guestName;
  }

  public void setGuestName(String guestName) {
    this.guestName = guestName;
  }

  public Integer getReservationStatus() {
    return reservationStatus;
  }

  public void setReservationStatus(Integer reservationStatus) {
    this.reservationStatus = reservationStatus;
  }
}
