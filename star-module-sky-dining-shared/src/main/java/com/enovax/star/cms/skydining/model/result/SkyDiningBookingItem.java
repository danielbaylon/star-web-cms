package com.enovax.star.cms.skydining.model.result;

import java.util.ArrayList;
import java.util.List;

public class SkyDiningBookingItem extends BookingItem {

    private String subName;
    private boolean hasMainCourses;
    private String displayImagePath;
    private Integer gst;
    private Integer serviceCharge;
    private String usualPrice;
    private boolean discountItem;
    private Integer discountId;

    private List<SkyDiningBookingMainCourse> sdMainCourses = new ArrayList<SkyDiningBookingMainCourse>(
        0);

    @Override
    public Integer getDiscountId() {
        return discountId;
    }

    public boolean getDiscountItem() {
        return discountItem;
    }

    public String getDisplayImagePath() {
        return displayImagePath;
    }

    public Integer getGst() {
        return gst;
    }

    public boolean getHasMainCourses() {
        return hasMainCourses;
    }

    public List<SkyDiningBookingMainCourse> getSdMainCourses() {
        return sdMainCourses;
    }

    public Integer getServiceCharge() {
        return serviceCharge;
    }

    public String getSubName() {
        return subName;
    }

    public String getUsualPrice() {
        return usualPrice;
    }

    @Override
    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public void setDiscountItem(boolean discountItem) {
        this.discountItem = discountItem;
    }

    public void setDisplayImagePath(String itemImagePath) {
        displayImagePath = itemImagePath;
    }

    public void setGst(Integer gst) {
        this.gst = gst;
    }

    public void setHasMainCourses(boolean hasMainCourses) {
        this.hasMainCourses = hasMainCourses;
    }

    public void setSdMainCourses(List<SkyDiningBookingMainCourse> sdMainCourses) {
        this.sdMainCourses = sdMainCourses;
    }

    public void setServiceCharge(Integer serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public void setUsualPrice(String usualPrice) {
        this.usualPrice = usualPrice;
    }

}
