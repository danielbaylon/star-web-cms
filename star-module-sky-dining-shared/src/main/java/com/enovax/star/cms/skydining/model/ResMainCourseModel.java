package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.skydining.datamodel.SkyDiningMainCourse;
import com.enovax.star.cms.skydining.datamodel.SkyDiningResMainCourse;

/**
 * Created by jace on 29/7/16.
 */
public class ResMainCourseModel extends SkyDiningResMainCourse {

  private Integer mainCourseId;
  private String mainCourseDescription;

  public ResMainCourseModel(SkyDiningMainCourse mainCourse) {
    initMainCourseValues(mainCourse);

    setId(0);
    setQuantity(0);
  }

  public ResMainCourseModel(SkyDiningResMainCourse dataModel) {
    initDataModelValues(dataModel);
  }

  public ResMainCourseModel(SkyDiningResMainCourse dataModel,
                            SkyDiningMainCourse mainCourse) {
    initDataModelValues(dataModel);
    initMainCourseValues(mainCourse);
  }

  public String getMainCourseDescription() {
    return mainCourseDescription;
  }

  public Integer getMainCourseId() {
    return mainCourseId;
  }

  private void initDataModelValues(SkyDiningResMainCourse dataModel) {
    setId(dataModel.getId());
    setQuantity(dataModel.getQuantity());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());
  }

  private void initMainCourseValues(SkyDiningMainCourse mainCourse) {
    mainCourseId = mainCourse.getId();
    mainCourseDescription = mainCourse.getDescription();
  }

  public void setMainCourseDescription(String mainCourseDescription) {
    this.mainCourseDescription = mainCourseDescription;
  }

  public void setMainCourseId(Integer mainCourseId) {
    this.mainCourseId = mainCourseId;
  }

}
