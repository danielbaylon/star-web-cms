package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTransaction;
import com.enovax.star.cms.skydining.model.*;

import java.util.List;

public interface ISkyDiningReservationService {

    int countReservationRecords(ReservationFilterCriteria criteria);

    List<MenuRecord> getMenus(int packageId);

    List<PackageRecord> getPackages();

    List<PackageRecord> getPackages(String date);

    ReservationModel getReservation(int id);

    List<ReservationRecord> getReservationRecords(
        ReservationFilterCriteria criteria);

    List<ResMainCourseModel> getResMainCourses(int parentId);

    List<ResMainCourseModel> getResMainCourses(int parentId, int menuId,
                                               boolean includeUnselected);

    List<ResTopUpModel> getResTopUps(int parentId);

    List<ResTopUpModel> getResTopUps(int parentId, int packageId,
                                     boolean includeUnselected);

    List<SessionRecord> getSessions(int packageId, String date, String time);

    List<ThemeModel> getThemes(int packageId);

    SaveResult saveReservation(ReservationModel reservation, String userId,
                               String timestampText);

    void sendReceiptEmail(String receiptNumber);

    void sendReceiptEmail(SkyDiningTransaction txn, SkyDiningReservation res);

}

