package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningJewelCard;
import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.format.CellDateFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.Node;
import javax.jcr.Session;
import java.io.InputStream;
import java.util.*;

/**
 * Created by jace on 28/9/16.
 */
@Service
public class DefaultSharedConfigService extends BaseSkyDiningAdminService implements ISharedConfigService {
    public static final String DATE_SEPARATOR = "/";

    @Override
    @Transactional(readOnly = true)
    public int getJCMemberRecordCount(String searchMemberNumber, String searchJcFrom, String searchJcTo) {
        if (StringUtils.isNotBlank(searchMemberNumber)) {
            searchMemberNumber = "%" + searchMemberNumber + "%";
        } else {
            searchMemberNumber = null;
        }

        String[] jcFrom = searchJcFrom.split(DATE_SEPARATOR);
        String[] jcTo = searchJcTo.split(DATE_SEPARATOR);
        if (jcFrom.length == 2 && jcTo.length == 2) {
            Date startDate = getCalendarStart(Integer.parseInt(jcFrom[0]), Integer.parseInt(jcFrom[1]));
            Date endDate = getCalendarEnd(Integer.parseInt(jcTo[0]), Integer.parseInt(jcTo[1]));

            return Math.toIntExact(jcRepo.countBySearchCriteria(searchMemberNumber, startDate, endDate));

        } else {
            return Math.toIntExact(jcRepo.countBySearchCriteria(searchMemberNumber, null, null));
        }
    }

    private Date getCalendarStart(Integer month, Integer year) {
        Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.MONTH, month - 1);
        startDate.set(Calendar.YEAR, year);
        startDate.set(Calendar.DAY_OF_MONTH, startDate.getActualMinimum(Calendar.DAY_OF_MONTH));
        return startDate.getTime();
    }

    private Date getCalendarEnd(Integer month, Integer year) {
        Calendar endDate = Calendar.getInstance();
        endDate.set(Calendar.MONTH, month - 1);
        endDate.set(Calendar.YEAR, year);
        endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
        return endDate.getTime();
    }

    @Override
    @Transactional
    public void deleteJcFiltered(String searhMemberCard, String searchJcFrom, String searchJcTo) {
        List<SkyDiningJewelCard> forDeletion;
        if (StringUtils.isNotBlank(searhMemberCard)) {
            searhMemberCard = "%" + searhMemberCard + "%";
        } else {
            searhMemberCard = null;
        }
        String[] jcFrom = searchJcFrom.split(DATE_SEPARATOR);
        String[] jcTo = searchJcTo.split(DATE_SEPARATOR);
        if (jcFrom.length == 2 && jcTo.length == 2) {
            Date startDate = getCalendarStart(Integer.parseInt(jcFrom[0]), Integer.parseInt(jcFrom[1]));
            Date endDate = getCalendarEnd(Integer.parseInt(jcTo[0]), Integer.parseInt(jcTo[1]));
            forDeletion = jcRepo.findForDeletion(searhMemberCard, startDate, endDate);
        } else {
            forDeletion = jcRepo.findForDeletion(searhMemberCard, null, null);
        }
        if (forDeletion != null) {
            for (SkyDiningJewelCard jc : forDeletion) {
                jcRepo.delete(jc);
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<SkyDiningJewelCard> getJCMemberRecords(String searchMemberNumber, String searchJcFrom, String searchJcTo,
                                                       int start, int pageSize, String orderBy, boolean asc) {
        if (StringUtils.isNotBlank(searchMemberNumber)) {
            searchMemberNumber = "%" + searchMemberNumber + "%";
        } else {
            searchMemberNumber = null;
        }

        String[] jcFrom = searchJcFrom.split(DATE_SEPARATOR);
        String[] jcTo = searchJcTo.split(DATE_SEPARATOR);
        if (jcFrom.length == 2 && jcTo.length == 2) {
            Date startDate = getCalendarStart(Integer.parseInt(jcFrom[0]), Integer.parseInt(jcFrom[1]));
            Date endDate = getCalendarEnd(Integer.parseInt(jcTo[0]), Integer.parseInt(jcTo[1]));

            Page<SkyDiningJewelCard> result = jcRepo.findBySearchCriteria(searchMemberNumber, startDate, endDate,
                new PageRequest(start / pageSize, pageSize, new Sort(asc ? Sort.Direction.ASC : Sort.Direction.DESC, orderBy)));

            if (result != null) {
                return result.getContent();
            }

        } else {
            Page<SkyDiningJewelCard> result = jcRepo.findBySearchCriteria(searchMemberNumber, null, null,
                new PageRequest(start / pageSize, pageSize, new Sort(asc ? Sort.Direction.ASC : Sort.Direction.DESC, orderBy)));

            if (result != null) {
                return result.getContent();
            }
        }

        return Collections.emptyList();
    }

    @Override
    @Transactional
    public void updateJCMembers(List<SkyDiningJewelCard> jcs, String userName) throws Exception {
        SkyDiningJewelCard jc;
        for (SkyDiningJewelCard jc2 : jcs) {
            List<SkyDiningJewelCard> findJCs = jcRepo.findByCardNumber(jc2.getCardNumber());
            if (findJCs != null && findJCs.size() > 0) {
                jc = findJCs.get(0);
            } else {
                jc = new SkyDiningJewelCard();
                jc.setCreatedBy(userName);
                jc.setCreatedDate(new Date());
            }
            String magnoliaUser = MgnlContext.getInstance().getUser().getName();

            jc.setCardNumber(jc2.getCardNumber());
            jc.setExpiryDate(jc2.getExpiryDate());
            jc.setModifiedDate(new Date());
            jc.setModifiedBy(magnoliaUser);
            jcRepo.save(jc);
        }
    }

    @Override
    @Transactional
    public void updateJCMember(SkyDiningJewelCard jc, String userName) throws Exception {
        SkyDiningJewelCard findJC = jcRepo.findOne(jc.getId());
        if (findJC == null) {
            throw new Exception("Faber Licence member does not exist!");
        }
        String magnoliaUser = MgnlContext.getInstance().getUser().getName();

        findJC.setCardNumber(jc.getCardNumber());
        findJC.setExpiryDate(jc.getExpiryDate());
        findJC.setModifiedDate(new Date());
        findJC.setModifiedBy(magnoliaUser);
        jcRepo.save(findJC);
    }

    @Override
    @Transactional
    public void deleteJCMember(List<Integer> ids) throws Exception {
        SkyDiningJewelCard jc;
        for (int id : ids) {
            if (id > 0) {
                jc = jcRepo.findOne(id);
                if (jc == null) {
                    throw new Exception("Faber Licence member does not exist!");
                }
                jcRepo.delete(jc);
            } else {
                throw new Exception("Faber Licence member is invalid!");
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public boolean jcMemberExist(SkyDiningJewelCard jc) {
        if (jc == null) {
            return false;
        }
        return jcRepo.countByIdAndCardNumber(jc.getId(), jc.getCardNumber()) > 0;
    }

    @Override
    @Transactional
    public List<SkyDiningJewelCard> processCardXLS(InputStream inp) throws Exception {
        List<SkyDiningJewelCard> result = new ArrayList<>();

        HSSFWorkbook wb = new HSSFWorkbook(inp);
        HSSFSheet sheet = wb.getSheetAt(0);
        int rows = sheet.getPhysicalNumberOfRows();

        for (int i = 1; i < rows; i++) {
            HSSFRow row = sheet.getRow(i);
            int cellNumber = row.getLastCellNum();
            if (cellNumber < 2) {
                continue;
            }
            HSSFCell cell = row.getCell(0);
            String value = "";
            if (cell != null) {
                switch (cell.getCellType()) {
                    case XSSFCell.CELL_TYPE_FORMULA:
                        value = cell.getCellFormula();
                        break;

                    case XSSFCell.CELL_TYPE_NUMERIC:
                        Double d = cell.getNumericCellValue();
                        value = Integer.toString(d.intValue());
                        break;

                    case XSSFCell.CELL_TYPE_STRING:
                        value = cell.getStringCellValue();
                        break;

                    default:
                }
            }
            String numString = value;

            cell = row.getCell(1);
            value = "";
            if (cell != null) {
                switch (cell.getCellType()) {
                    case XSSFCell.CELL_TYPE_FORMULA:
                        value = cell.getCellFormula();
                        break;

                    case XSSFCell.CELL_TYPE_NUMERIC:
                        value = Double.toString(cell.getNumericCellValue());
                        break;

                    case XSSFCell.CELL_TYPE_STRING:
                        value = cell.getStringCellValue();
                        break;

                    default:
                }
            }
            String expiryString = value;
            if (numString != null && !numString.trim().equals("")
                && expiryString != null && !expiryString.trim().equals("")) {
                numString = numString.trim();
                expiryString = expiryString.trim();

                if (numString.length() != 5) {
                    continue;
                }

                try {
                    Integer.parseInt(numString);
                } catch (NumberFormatException e) {
                    continue;
                }

                Date expiryDate = null;
                try {
                    expiryDate = NvxDateUtils.parseDate("01/" + expiryString, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    //expiryDate = sdf.parse("01/" + expiryString);
                } catch (Exception e) {
                    continue;
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(expiryDate);
                cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));

                Date lastDayOfMonth = cal.getTime();
                SkyDiningJewelCard jc = new SkyDiningJewelCard();
                jc.setCardNumber(numString.trim().toUpperCase());
                jc.setExpiryDate(lastDayOfMonth);
                result.add(jc);
            }
        }
        return result;
    }

    public List<SkyDiningJewelCard> processCardXLSX(InputStream inp) throws Exception {
        List<SkyDiningJewelCard> result = new ArrayList<>();

        XSSFWorkbook wb = new XSSFWorkbook(inp);
        XSSFSheet sheet = wb.getSheetAt(0);
        int rows = sheet.getPhysicalNumberOfRows();

        for (int i = 1; i < rows; i++) {
            XSSFRow row = sheet.getRow(i);
            int cellNumber = row.getLastCellNum();
            if (cellNumber < 2) {
                continue;
            }
            XSSFCell cell = row.getCell(0);
            String value = "";
            if (cell != null) {
                switch (cell.getCellType()) {
                    case XSSFCell.CELL_TYPE_FORMULA:
                        value = cell.getCellFormula();
                        break;

                    case XSSFCell.CELL_TYPE_NUMERIC:
                        Double d = cell.getNumericCellValue();
                        value = Integer.toString(d.intValue());
                        break;

                    case XSSFCell.CELL_TYPE_STRING:
                        value = cell.getStringCellValue();
                        break;

                    default:
                }
            }
            String numString = value;
            cell = row.getCell(1);
            value = "";
            if (cell != null) {
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    Date date = HSSFDateUtil.getJavaDate(cell.getNumericCellValue());
                    String fmt = cell.getCellStyle().getDataFormatString();

                    value = new CellDateFormatter(fmt).format(date);
                } else {
                    switch (cell.getCellType()) {
                        case XSSFCell.CELL_TYPE_FORMULA:
                            value = cell.getCellFormula();
                            break;

                        case XSSFCell.CELL_TYPE_NUMERIC:
                            value = Double.toString(cell.getNumericCellValue());
                            break;

                        case XSSFCell.CELL_TYPE_STRING:
                            value = cell.getStringCellValue();
                            break;

                        default:
                    }
                }
            }
            String expiryString = value;
            if (numString != null && !numString.trim().equals("")
                && expiryString != null && !expiryString.trim().equals("")) {
                numString = numString.trim();
                expiryString = expiryString.trim();

                if (numString.length() != 11) {
                    continue;
                }

                Date expiryDate = null;
                try {
                    expiryDate = NvxDateUtils.parseDate("01/" + expiryString, NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
                    //expiryDate = sdf.parse("01/" + expiryString);
                } catch (Exception e) {
                    continue;
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(expiryDate);
                cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));

                Date lastDayOfMonth = cal.getTime();
                SkyDiningJewelCard jc = new SkyDiningJewelCard();
                jc.setCardNumber(numString.trim().toUpperCase());
                jc.setExpiryDate(lastDayOfMonth);
                result.add(jc);
            }
        }
        return result;
    }

    public static final String WORKSPACE_APP_CONFIG = "app-config";
    public static final String NODE_SKY_DINING = "sky-dining";
    public static final String NODE_RECEIPT_ROOT_DIR = "receipt-root-dir";
    public static final String PROPERTY_VALUE = "value";

    @Override
    public String getSkyDiningReceiptRootDir() {
        try {
            Session session = MgnlContext.getJCRSession(WORKSPACE_APP_CONFIG);
            Node sdNode;
            if (session.getRootNode().hasNode(NODE_SKY_DINING)) {
                sdNode = session.getRootNode().getNode(NODE_SKY_DINING);
            } else {
                sdNode = session.getRootNode().addNode(NODE_SKY_DINING, "mgnl:remote-channel-folder");
            }
            Node rrdNode;
            if (sdNode.hasNode(NODE_RECEIPT_ROOT_DIR)) {
                rrdNode = sdNode.getNode(NODE_RECEIPT_ROOT_DIR);
            } else {
                rrdNode = sdNode.addNode(NODE_RECEIPT_ROOT_DIR, "mgnl:app-config");
            }
            if (!rrdNode.hasProperty(PROPERTY_VALUE)) {
                PropertyUtil.setProperty(rrdNode, PROPERTY_VALUE, "");
            }
            return PropertyUtil.getString(rrdNode, PROPERTY_VALUE, "");
        } catch (Exception e) {
            return "";
        }
    }
}
