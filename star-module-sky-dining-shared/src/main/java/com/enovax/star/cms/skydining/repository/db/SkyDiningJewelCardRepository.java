package com.enovax.star.cms.skydining.repository.db;

import com.enovax.star.cms.skydining.datamodel.SkyDiningJewelCard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by jace on 31/8/16.
 */
@Repository
public interface SkyDiningJewelCardRepository extends JpaRepository<SkyDiningJewelCard, Integer> {

    List<SkyDiningJewelCard> findByCardNumber(String cardNumber);

    SkyDiningJewelCard findUniqueByCardNumber(String cardNumber);

    @Query(value = "select jc from SkyDiningJewelCard jc where " +
        "(jc.cardNumber like :cardNumber or :cardNumber is null) and " +
        "(jc.expiryDate >= :startDate or :startDate is null) and " +
        "(jc.expiryDate <= :endDate or :endDate is null)")
    List<SkyDiningJewelCard> findForDeletion(@Param("cardNumber") String cardNumber,
                                             @Param("startDate") Date start,
                                             @Param("endDate") Date end);

    @Query(value = "select jc from SkyDiningJewelCard jc where " +
        "(jc.cardNumber like :cardNumber or :cardNumber is null) and " +
        "(jc.expiryDate >= :startDate or :startDate is null) and " +
        "(jc.expiryDate <= :endDate or :endDate is null)")
    Page<SkyDiningJewelCard> findBySearchCriteria(@Param("cardNumber") String cardNumber,
                                                  @Param("startDate") Date start,
                                                  @Param("endDate") Date end,
                                                  Pageable pageRequest);

    @Query(value = "select count (*) from SkyDiningJewelCard jc where " +
        "(jc.cardNumber like :cardNumber or :cardNumber is null) and " +
        "(jc.expiryDate >= :startDate or :startDate is null) and " +
        "(jc.expiryDate <= :endDate or :endDate is null)")
    Long countBySearchCriteria(@Param("cardNumber") String cardNumber,
                               @Param("startDate") Date start,
                               @Param("endDate") Date end);

    Long countByIdAndCardNumber(Integer id, String cardNumber);
}
