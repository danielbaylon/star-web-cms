package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTnc;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jace on 29/7/16.
 */
@Repository
public class DefaultSkyDiningTncRepository extends BaseJcrRepository<SkyDiningTnc> implements ISkyDiningTncRepository {

  public static final String JCR_WORKSPACE = "sky-dining-admin";
  public static final String JCR_PATH = "";
  public static final String PRIMARY_NODE_TYPE = "";

  @Override
  protected String getJcrWorkspace() {
    return JCR_WORKSPACE;
  }

  @Override
  protected String getJcrPath() {
    return null;
  }

  @Override
  protected SkyDiningTnc getNewInstance() {
    return new SkyDiningTnc();
  }

  @Override
  protected String getPrimaryNodeType() {
    return "mgnl:sky-dining-tnc";
  }
}
