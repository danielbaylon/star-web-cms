package com.enovax.star.cms.skydining.service;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by jace on 8/11/16.
 */
public interface ISkyDiningReportService {

    Workbook generateDailyBookingExcel(String startDate, String endDate);

    Workbook generateDailyOrderExcel(String startDate, String endDate);

    Workbook generateRevenueExcel(String startDate, String endDate);

}
