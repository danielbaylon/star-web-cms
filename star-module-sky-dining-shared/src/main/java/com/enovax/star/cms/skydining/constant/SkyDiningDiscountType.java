package com.enovax.star.cms.skydining.constant;

/**
 * Created by jace on 21/7/16.
 */
public enum SkyDiningDiscountType {
  EarlyBird("Early Bird"), CreditCard("Credit Card"), PromoCode("Promo Code"), JewelCard(
    "Faber Licence");

  public final String name;

  SkyDiningDiscountType(String name) {
    this.name = name;
  }
}

