package com.enovax.star.cms.skydining.util;

import com.enovax.util.jcr.publishing.PublishingUtil;
import com.google.common.io.Files;
import info.magnolia.context.MgnlContext;
import info.magnolia.dam.api.AssetProviderRegistry;
import info.magnolia.dam.jcr.AssetNodeTypes;
import info.magnolia.dam.jcr.DamConstants;
import info.magnolia.dam.jcr.JcrAssetProvider;
import info.magnolia.dam.jcr.JcrFolder;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.objectfactory.Components;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.shiro.crypto.hash.Md5Hash;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.jcr.Node;
import javax.jcr.Session;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * TODO Merge some of the functionality with the FileUtil common packages
 * TODO Check if the copyFile function is shareable or if it's MFLG-specific, then refactor accordingly.
 */
public class JcrFileUtil {

  public static final String IMG_EXT_PNG = ".png";
  public static final String IMG_EXT_JPG = ".jpg";
  public static final String IMG_EXT_GIF = ".gif";

  private static int hashIterations = 1;

  public static boolean isImgExt(String file) {
    if (StringUtils.isEmpty(file) || file.length() < 5) {
      return false;
    }

    return file.endsWith(IMG_EXT_JPG) || file.endsWith(IMG_EXT_PNG) || file.endsWith(IMG_EXT_GIF);

  }

  public static boolean checkImgDimensions(File imgFile, int height, int width) throws IOException {
    final BufferedImage img = ImageIO.read(imgFile);
    return !(img.getHeight() > height || img.getWidth() > width);
  }

  public static String hashFile(File file) throws IOException {
    return hashFile(Files.toByteArray(file), "");
  }

  public static String hashFile(File file, String salt) throws IOException {
    return hashFile(Files.toByteArray(file), salt);
  }

  public static String hashFile(byte[] file) throws IOException {
    return hashFile(file, "");
  }

  public static String hashFile(byte[] file, String salt) throws IOException {
    Md5Hash hasher = StringUtils.isEmpty(salt) ? new Md5Hash(file) : new Md5Hash(file, salt);
    hasher.setIterations(hashIterations);
    return hasher.toString();
  }

  public static String copyFile(File imageFile, String basePath, String filename) throws Exception {
    final String path;
    if (StringUtils.isNotBlank(basePath)) {
      path = basePath.endsWith(File.separator) ? basePath.substring(0, basePath.length() - 2) : basePath;
    } else {
      path = "";
    }

    Session session = MgnlContext.getJCRSession(DamConstants.WORKSPACE);
    if (!session.getRootNode().hasNode(basePath)) {
      NodeUtil.createPath(session.getRootNode(), basePath, NodeTypes.Folder.NAME, true);
    }

    // "Navigate" to the assets folder node
    AssetProviderRegistry assetProviderRegistry = Components.getComponent(AssetProviderRegistry.class);
    JcrAssetProvider jcrAssetProvider = (JcrAssetProvider) assetProviderRegistry.getProviderById(DamConstants.DEFAULT_JCR_PROVIDER_ID);
    JcrFolder assetFolder = (JcrFolder) jcrAssetProvider.getFolder(path);
    Node assetFolderNode = assetFolder.getNode();

    // Create asset node
    Node assetNode = JcrUtils.getOrAddNode(assetFolderNode, filename, AssetNodeTypes.Asset.NAME);
    assetNode.setProperty(AssetNodeTypes.Asset.ASSET_NAME, filename);

    // String get file name extension
    String extension = FilenameUtils.getExtension(filename);

    // Create BufferedImage to get height and width
    BufferedImage image = ImageIO.read(imageFile);

    // Create asset resource node
    Node assetResourceNode = JcrUtils.getOrAddNode(assetNode, AssetNodeTypes.AssetResource.RESOURCE_NAME, AssetNodeTypes.AssetResource.NAME);
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.DATA, session.getValueFactory().createBinary(new FileInputStream(imageFile)));
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.FILENAME, filename);
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.EXTENSION, extension);
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.SIZE, Long.toString(imageFile.length()));
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.MIMETYPE, new MimetypesFileTypeMap().getContentType(imageFile));
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.WIDTH, Integer.toString(image.getWidth()));
    assetResourceNode.setProperty(AssetNodeTypes.AssetResource.HEIGHT, Integer.toString(image.getHeight()));

    session.save();
    PublishingUtil.publishNodes(assetNode.getIdentifier(), DamConstants.WORKSPACE);

    String resourceLink = "/dam/jcr:" + assetNode.getIdentifier() + "/" + assetNode.getName();
    return resourceLink;
  }
}
