package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPromoCode;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
@Repository
public class DefaultSkyDiningPromoCodeRepository extends BaseJcrRepository<SkyDiningPromoCode> implements ISkyDiningPromoCodeRepository {

    public static final String JCR_WORKSPACE = "sky-dining-admin";
    public static final String JCR_PATH = null;
    public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-promo-code";

    @Override
    protected String getJcrWorkspace() {
        return JCR_WORKSPACE;
    }

    @Override
    protected String getJcrPath() {
        return JCR_PATH;
    }

    @Override
    protected SkyDiningPromoCode getNewInstance() {
        return new SkyDiningPromoCode();
    }

    @Override
    protected String getPrimaryNodeType() {
        return PRIMARY_NODE_TYPE;
    }

    @Override
    public SkyDiningPromoCode checkPromoCode(String promoCode, int packageId) {
        final List<SkyDiningPromoCode> pcs = findByProperty("code", promoCode);
        if (pcs != null) {
            for (SkyDiningPromoCode pc : pcs) {
                if (isActive(pc, true)) {
                    return pc;
                }
            }
        }
        return null;
    }

    @Override
    public List<SkyDiningPromoCode> findPaged(Integer discountId, Integer start, Integer pageSize) {
        final List<SkyDiningPromoCode> promoCodes = new ArrayList<>();
        try {
            Node node = findByPrimaryNodeType(DefaultSkyDiningDiscountRepository.JCR_WORKSPACE, Integer.toString(discountId), DefaultSkyDiningDiscountRepository.PRIMARY_NODE_TYPE);
            if (node.hasNode("promoCodes")) {
                NodeIterator i = node.getNode("promoCodes").getNodes();
                while (i.hasNext()) {
                    Node pcNode = i.nextNode();
                    SkyDiningPromoCode pc = getNewInstance();
                    fillBeanFromNode(pc, pcNode);
                    promoCodes.add(pc);
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving promo codes.");
        }

        return promoCodes;
    }


    @Override
    public SkyDiningPromoCode updatePromoCodeUseCount(String codeStr, int packageId, int qtyToUpdate, boolean isAdd) {
        final List<SkyDiningPromoCode> pcs = findByProperty("code", codeStr);
        try {
            if (pcs != null) {
                for (SkyDiningPromoCode pc : pcs) {
                    if (isActive(pc, true)) {
                        pc.setTimesUsed(isAdd ? pc.getTimesUsed() + qtyToUpdate : pc.getTimesUsed() - qtyToUpdate);
                        save(pc);
                        return pc;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Unable to update use count for promo code: " + codeStr);
        }
        return null;
    }

    public static boolean isActive(SkyDiningPromoCode pc, boolean checkCapacity) {
        if (pc != null && BooleanUtils.isTrue(pc.getActive()) && NvxDateUtils.inDateRangeFlexible(pc.getStartDate(), pc.getEndDate(), new Date(), true, true)) {
            if (checkCapacity && BooleanUtils.isTrue(pc.getHasLimit())) {
                return pc.getTimesUsed() < pc.getLimit();
            } else {
                return true;
            }
        }
        return false;
    }
}
