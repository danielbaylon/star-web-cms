package com.enovax.star.cms.skydining.constant;

public enum SkyDiningCartResult {
  Success, Failed, ValidationFailed, FullSessionCapacity, FullThemeCapacity, MenuAfterCutOff, TopUpAfterCutOff, PackageAfterCutOff, CartNotEmpty
}
