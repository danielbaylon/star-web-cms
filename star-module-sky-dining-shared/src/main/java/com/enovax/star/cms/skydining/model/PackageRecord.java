package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;

import java.io.Serializable;
import java.util.Date;

public class PackageRecord implements Serializable {

  private int id;
  private String name;
  private String status;
  private Integer displayOrder;

  public PackageRecord(SkyDiningPackage skyDiningPackage) {
    id = skyDiningPackage.getId();
    name = skyDiningPackage.getName();

    if ((skyDiningPackage.getStartDate() != null)
      && (skyDiningPackage.getEndDate() != null)) {
      final Date now = new Date();
      if (now.after(skyDiningPackage.getStartDate())
        && now.before(skyDiningPackage.getEndDate())) {
        status = "Active";
      } else {
        status = "Inactive";
      }
    } else {
      status = "Inactive";
    }

    displayOrder = skyDiningPackage.getDisplayOrder();
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getStatus() {
    return status;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Integer getDisplayOrder() {
    return displayOrder;
  }

  public void setDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
  }

}
