package com.enovax.star.cms.skydining.constant;

public enum PaymentMode {
    None(0), Cash(1), eNETS(2), VISA(3), MasterCard(4), ChinaUnionPay(5), OtherCard(
        6);

    public static PaymentMode valueOf(int i) {
        switch (i) {
            case 0:
                return None;
            case 1:
                return Cash;
            case 2:
                return eNETS;
            case 3:
                return VISA;
            case 4:
                return MasterCard;
            case 5:
                return ChinaUnionPay;
            case 6:
                return OtherCard;
            default:
                return null;
        }
    }

    public final int value;

    PaymentMode(int value) {
        this.value = value;
    }

    public static PaymentMode fromTitle(String t) {
        switch (t) {
            case "VISA":
                return VISA;
            case "Mastercard":
                return MasterCard;
            case "China UnionPay":
                return ChinaUnionPay;
            default:
                return OtherCard;
        }
    }
}
