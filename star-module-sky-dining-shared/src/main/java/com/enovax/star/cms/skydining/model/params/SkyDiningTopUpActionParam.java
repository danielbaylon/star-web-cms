package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 30/7/16.
 */
public class SkyDiningTopUpActionParam extends BaseSkyDiningActionParam {

  protected String newName;
  protected Integer newType;
  protected String newDescription;
  protected String newEarlyBirdEndDate;
  protected String newStartDate;
  protected String newEndDate;
  protected Integer newSalesCutOffDays;
  protected Integer newPrice;
  protected String newImageHash;

  protected Integer newDisplayOrder;

  protected String nameFilter;
  protected Integer activeFilter;

  public String getNewName() {
    return newName;
  }

  public void setNewName(String newName) {
    this.newName = newName;
  }

  public Integer getNewType() {
    return newType;
  }

  public void setNewType(Integer newType) {
    this.newType = newType;
  }

  public String getNewDescription() {
    return newDescription;
  }

  public void setNewDescription(String newDescription) {
    this.newDescription = newDescription;
  }

  public String getNewEarlyBirdEndDate() {
    return newEarlyBirdEndDate;
  }

  public void setNewEarlyBirdEndDate(String newEarlyBirdEndDate) {
    this.newEarlyBirdEndDate = newEarlyBirdEndDate;
  }

  public String getNewStartDate() {
    return newStartDate;
  }

  public void setNewStartDate(String newStartDate) {
    this.newStartDate = newStartDate;
  }

  public String getNewEndDate() {
    return newEndDate;
  }

  public void setNewEndDate(String newEndDate) {
    this.newEndDate = newEndDate;
  }

  public Integer getNewSalesCutOffDays() {
    return newSalesCutOffDays;
  }

  public void setNewSalesCutOffDays(Integer newSalesCutOffDays) {
    this.newSalesCutOffDays = newSalesCutOffDays;
  }

  public Integer getNewPrice() {
    return newPrice;
  }

  public void setNewPrice(Integer newPrice) {
    this.newPrice = newPrice;
  }

  public String getNewImageHash() {
    return newImageHash;
  }

  public void setNewImageHash(String newImageHash) {
    this.newImageHash = newImageHash;
  }

  public Integer getNewDisplayOrder() {
    return newDisplayOrder;
  }

  public void setNewDisplayOrder(Integer newDisplayOrder) {
    this.newDisplayOrder = newDisplayOrder;
  }

  public String getNameFilter() {
    return nameFilter;
  }

  public void setNameFilter(String nameFilter) {
    this.nameFilter = nameFilter;
  }

  public Integer getActiveFilter() {
    return activeFilter;
  }

  public void setActiveFilter(Integer activeFilter) {
    this.activeFilter = activeFilter;
  }
}
