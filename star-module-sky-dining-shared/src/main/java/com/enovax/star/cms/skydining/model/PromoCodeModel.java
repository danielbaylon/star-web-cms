package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPromoCode;

public class PromoCodeModel extends SkyDiningPromoCode {

  private String startDateText;
  private String endDateText;
  private String timestamp;

  public PromoCodeModel(SkyDiningPromoCode dataModel) {
    setId(dataModel.getId());
    setCode(dataModel.getCode());
    setHasLimit(dataModel.getHasLimit());
    setLimit(dataModel.getLimit());
    setTimesUsed(dataModel.getTimesUsed());
    setActive(dataModel.getActive());
    setType(dataModel.getType());
    setStartDate(dataModel.getStartDate());
    setEndDate(dataModel.getEndDate());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());

    if (getStartDate() != null) {
      startDateText = NvxDateUtils.formatDate(getStartDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEndDate() != null) {
      endDateText = NvxDateUtils.formatDate(getEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getModifiedDate() != null) {
      timestamp = NvxDateUtils.formatModDt(getModifiedDate());
    }
  }

  public String getEndDateText() {
    return endDateText;
  }

  public String getStartDateText() {
    return startDateText;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setEndDateText(String endDateText) {
    this.endDateText = endDateText;
  }

  public void setStartDateText(String startDateText) {
    this.startDateText = startDateText;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

}
