package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningBlockOutDate;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;
import com.enovax.star.cms.skydining.datamodel.SkyDiningSession;
import com.enovax.star.cms.skydining.model.*;
import com.enovax.util.jcr.exception.JcrException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
@Service
public class DefaultSkyDiningSessionService extends BaseSkyDiningAdminService implements ISkyDiningSessionService {
  @Override
  public SaveResult addPackagesToSession(int parentId, List<Integer> ids,
                                         String userId, String timestampText) {
    final SkyDiningSession session = sessionRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (session == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(session, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    for (final int id : ids) {
      final SkyDiningPackage skyDiningPackage = packageRepo.find(id);
      if (skyDiningPackage == null) {
        return new SaveResult(SaveResult.Result.Failed,
          "Unable to find sky dining package(s).");
      } else {
        session.getPackages().add(skyDiningPackage);
      }
    }

    timestamp = new Date();
    session.setModifiedBy(userId);
    session.setModifiedDate(timestamp);
    try {
      sessionRepo.save(session);
    } catch (JcrException e) {
      e.printStackTrace();
      return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public int countScheduleRecords() {
    return sessionRepo.countAll();
  }

  @Override
  public int countSelectPackageRecords(Integer sessionId, String name,
                                       Boolean active) {
    return packageRepo.count(name, active, sessionId, null, null);
  }

  @Override
  public List<BlockOutDateModel> getBlockOutDates(int parentId) {
    final List<BlockOutDateModel> blockOutDates = new ArrayList<BlockOutDateModel>();
    final SkyDiningSession session = sessionRepo.find(parentId);
    if (session != null) {
      final List<SkyDiningBlockOutDate> dataModels = session
        .getBlockOutDates();
      for (final SkyDiningBlockOutDate dataModel : dataModels) {
        final BlockOutDateModel blockOutDate = new BlockOutDateModel(
          dataModel);
        blockOutDates.add(blockOutDate);
      }
    }
    return blockOutDates;
  }

  @Override
  public List<PackageRecord> getPackageRecords(int parentId) {
    final List<PackageRecord> packageRecords = new ArrayList<PackageRecord>();
    final SkyDiningSession session = sessionRepo.find(parentId);
    if (session != null) {
      final List<SkyDiningPackage> dataModels = session.getPackages();
      for (final SkyDiningPackage dataModel : dataModels) {
        final PackageRecord packageRecord = new PackageRecord(dataModel);
        packageRecords.add(packageRecord);
      }
    }
    return packageRecords;
  }

  @Override
  public List<PackageRecord> getSelectPackageRecords(Integer sessionId,
                                                     String name, Boolean active, int start, int pageSize,
                                                     String orderBy, boolean asc) {
    final List<PackageRecord> packageRecords = new ArrayList<PackageRecord>();
    final List<SkyDiningPackage> packages = packageRepo.findPaged(name,
      active, sessionId, null, null, start, pageSize, orderBy, asc);

    for (final SkyDiningPackage skyDiningPackage : packages) {
      final PackageRecord packageRecord = new PackageRecord(
        skyDiningPackage);
      packageRecords.add(packageRecord);
    }

    return packageRecords;
  }

  @Override
  public SessionModel getSession(int id) {
    return new SessionModel(sessionRepo.find(id));
  }

  @Override
  public List<SessionRecord> getSessionRecords(int start, int pageSize,
                                               String orderBy, boolean asc) {
    final List<SkyDiningSession> sessions = sessionRepo.findPaged(start,
      pageSize, orderBy, asc);
    final List<SessionRecord> records = new ArrayList<SessionRecord>();

    for (final SkyDiningSession session : sessions) {
      final SessionRecord record = new SessionRecord(session);
      records.add(record);
    }

    return records;
  }

  @Override
  public SaveResult removeBlockOutDates(int parentId, List<Integer> ids,
                                        String userId, String timestampText) {
    final SkyDiningSession session = sessionRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (session == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(session, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningBlockOutDate> iterator = session
      .getBlockOutDates().iterator();
    while (iterator.hasNext()) {
      final SkyDiningBlockOutDate blockOutDate = iterator.next();

      for (final int id : ids) {
        if (blockOutDate.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    session.setModifiedBy(userId);
    session.setModifiedDate(timestamp);
    try {
      sessionRepo.save(session);
    } catch (JcrException e) {
      e.printStackTrace();
      return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removePackagesFromSession(int parentId,
                                              List<Integer> ids, String userId, String timestampText) {
    final SkyDiningSession session = sessionRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (session == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(session, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningPackage> iterator = session.getPackages()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningPackage sdPackage = iterator.next();

      for (final int id : ids) {
        if (sdPackage.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    session.setModifiedBy(userId);
    session.setModifiedDate(timestamp);
    try {
      sessionRepo.save(session);
    } catch (JcrException e) {
      e.printStackTrace();
      return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removeSessions(List<Integer> ids) {
    try {
      sessionRepo.remove(ArrayUtils.toPrimitive(ids.toArray(new Integer[ids.size()])));
      return new SaveResult(SaveResult.Result.Success);
    } catch (JcrException e) {
      return new SaveResult(SaveResult.Result.Failed);
    }
  }

  @Override
  public SaveResult saveBlockOutDate(int parentId, int id,
                                     String newStartDate, String newEndDate, String userId,
                                     String timestampText) {

    final SkyDiningSession parent = sessionRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (parent == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(parent, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    timestamp = new Date();
    if (id < 1) {
      final SkyDiningBlockOutDate entity = new SkyDiningBlockOutDate();
      entity.setId(0);
      if (newStartDate != null) {
        entity.setStartDate(parseDate(newStartDate));
      }
      if (newEndDate != null) {
        entity.setEndDate(parseDate(newEndDate));
      }

      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
      entity.setModifiedBy(userId);
      entity.setModifiedDate(timestamp);

      parent.getBlockOutDates().add(entity);
      parent.setModifiedBy(userId);
      parent.setModifiedDate(timestamp);
      try {
        sessionRepo.save(parent);
      } catch (JcrException e) {
        e.printStackTrace();
        return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
      }

      return new SaveResult(SaveResult.Result.Success, timestamp, entity.getId());
    }

    final List<SkyDiningBlockOutDate> entities = parent.getBlockOutDates();
    for (final SkyDiningBlockOutDate entity : entities) {
      if (id == entity.getId()) {
        if (newStartDate != null) {
          entity.setStartDate(parseDate(newStartDate));
        }
        if (newEndDate != null) {
          entity.setEndDate(parseDate(newEndDate));
        }

        entity.setModifiedBy(userId);
        entity.setModifiedDate(timestamp);
        parent.setModifiedBy(userId);
        parent.setModifiedDate(timestamp);
        try {
          sessionRepo.save(parent);
        } catch (JcrException e) {
          e.printStackTrace();
          return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
        }

        return new SaveResult(SaveResult.Result.Success, timestamp);
      }
    }

    return new SaveResult(SaveResult.Result.Failed,
      "Unable to find sky dining block out date.");
  }

  @Override
  public SaveResult saveSession(int id, String newName,
                                Integer newSessionNumber, Integer newSessionType,
                                String newEffectiveDays, String newStartDate, String newEndDate,
                                String newTime, String newSalesStartDate, String newSalesEndDate,
                                Integer newCapacity, Boolean newActive, String userId,
                                String timestampText) {

    SkyDiningSession entity = sessionRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      timestamp = new Date();
      entity = new SkyDiningSession();
      entity.setId(id);
      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
      timestamp = new Date();
    }

    if (newName != null) {
      entity.setName(newName);
    }
    if (newSessionNumber != null) {
      entity.setSessionNumber(newSessionNumber);
    }
    if (newSessionType != null) {
      entity.setSessionType(newSessionType);
    }
    if (newEffectiveDays != null) {
      entity.setEffectiveDays(newEffectiveDays);
    }
    if (newStartDate != null) {
      entity.setStartDate(parseDate(newStartDate));
    }
    if (newEndDate != null) {
      entity.setEndDate(parseDate(newEndDate));
    }
    if (newTime != null) {
      entity.setTime(newTime);
    }
    if (newSalesStartDate != null) {
      entity.setSalesStartDate(parseDate(newSalesStartDate));
    }
    if (newSalesEndDate != null) {
      entity.setSalesEndDate(parseDate(newSalesEndDate));
    }
    if (newCapacity != null) {
      entity.setCapacity(newCapacity);
    }
    if (newActive != null) {
      entity.setActive(newActive);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      if (sessionRepo.save(entity)) {
        return new SaveResult(SaveResult.Result.Success, timestamp,
          entity.getId());
      }
    } catch (JcrException e) {
      e.printStackTrace();
      return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult toggleStatus(int id, String userId, String timestampText) {
    final SkyDiningSession entity = sessionRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    timestamp = new Date();
    if (BooleanUtils.isNotTrue(entity.getActive())) {
      entity.setActive(true);
    } else {
      entity.setActive(false);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      sessionRepo.save(entity);
    } catch (JcrException e) {
      e.printStackTrace();
      return new SaveResult(SaveResult.Result.Failed, e.getLocalizedMessage());
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }
}
