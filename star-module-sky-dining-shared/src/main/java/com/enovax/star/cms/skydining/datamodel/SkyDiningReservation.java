package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(catalog = "STAR_DB", name = "SkyDiningReservation", schema = "dbo")
public class SkyDiningReservation implements HasTimestamp {

    private int id;
    private String guestName;
    private String contactNumber;
    private String emailAddress;
    private Date date;
    private Time time;
    private Integer pax;
    private String specialRequest;
    private Integer type;
    private Integer status;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private Integer discountId;
    private String jewelCard;
    private String jewelCardExpiry;
    private String promoCode;
    private String merchantId;
    private String receiptNum;
    private String receivedBy;
    private Date paymentDate;
    private Integer paymentMode;
    private BigDecimal paidAmount;
    private Integer priceType;
    private Integer discountPrice;
    private Integer originalPrice;

    private SkyDiningTransaction transaction;

    private List<SkyDiningResMainCourse> mainCourses = new ArrayList<>(
        0);
    private List<SkyDiningResTopUp> topUps = new ArrayList<>(0);

    private Integer packageId;
    private Integer sessionId;
    private Integer menuId;
    private Integer themeId;

    private String packageName;

    @Column(name = "contactNumber", nullable = false, length = 100)
    public String getContactNumber() {
        return contactNumber;
    }

    @Override
    @Column(name = "createdBy", nullable = false, length = 100)
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    @Column(name = "createdDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() {
        return createdDate;
    }

    @Column(name = "date", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    @Column(name = "discountId", nullable = true)
    public Integer getDiscountId() {
        return discountId;
    }

    @Column(name = "discountPrice", nullable = true)
    public Integer getDiscountPrice() {
        return discountPrice;
    }

    @Column(name = "emailAddress", nullable = false, length = 200)
    public String getEmailAddress() {
        return emailAddress;
    }

    @Column(name = "guestName", nullable = false, length = 300)
    public String getGuestName() {
        return guestName;
    }

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, updatable = false)
    public int getId() {
        return id;
    }

    @Column(name = "jewelCard", nullable = true, length = 100)
    public String getJewelCard() {
        return jewelCard;
    }

    @Column(name = "jewelCardExpiry", nullable = true, length = 50)
    public String getJewelCardExpiry() {
        return jewelCardExpiry;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "reservationId", referencedColumnName = "id", nullable = false, updatable = false)
    public List<SkyDiningResMainCourse> getMainCourses() {
        return mainCourses;
    }

    @Column(name = "merchantId", nullable = true)
    public String getMerchantId() {
        return merchantId;
    }

    @Override
    @Column(name = "modifiedBy", nullable = false, length = 100)
    public String getModifiedBy() {
        return modifiedBy;
    }

    @Override
    @Column(name = "modifiedDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    @Column(name = "originalPrice", nullable = true)
    public Integer getOriginalPrice() {
        return originalPrice;
    }

    @Column(name = "paidAmount", nullable = true)
    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    @Column(name = "pax", nullable = false)
    public Integer getPax() {
        return pax;
    }

    @Column(name = "paymentDate", nullable = true)
    public Date getPaymentDate() {
        return paymentDate;
    }

    @Column(name = "paymentMode", nullable = true)
    public Integer getPaymentMode() {
        return paymentMode;
    }

    @Column(name = "priceType", nullable = true)
    public Integer getPriceType() {
        return priceType;
    }

    @Column(name = "promoCode", nullable = true, length = 100)
    public String getPromoCode() {
        return promoCode;
    }

    @Column(name = "receiptNum", nullable = true, length = 50)
    public String getReceiptNum() {
        return receiptNum;
    }

    @Column(name = "receivedBy", nullable = true, length = 100)
    public String getReceivedBy() {
        return receivedBy;
    }

    @Column(name = "specialRequest", nullable = false, length = 4000)
    public String getSpecialRequest() {
        return specialRequest;
    }

    @Column(name = "status", nullable = false)
    public Integer getStatus() {
        return status;
    }

    @Column(name = "time", nullable = false, length = 50)
    public Time getTime() {
        return time;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "reservationId", referencedColumnName = "id", nullable = false, updatable = false)
    public List<SkyDiningResTopUp> getTopUps() {
        return topUps;
    }

    @OneToOne(mappedBy = "reservation")
    public SkyDiningTransaction getTransaction() {
        return transaction;
    }

    @Column(name = "type", nullable = false)
    public Integer getType() {
        return type;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public void setDiscountPrice(Integer discountPrice) {
        this.discountPrice = discountPrice;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJewelCard(String jewelCard) {
        this.jewelCard = jewelCard;
    }

    public void setJewelCardExpiry(String jewelCardExpiry) {
        this.jewelCardExpiry = jewelCardExpiry;
    }

    public void setMainCourses(List<SkyDiningResMainCourse> mainCourses) {
        this.mainCourses = mainCourses;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setPaymentMode(Integer paymentMode) {
        this.paymentMode = paymentMode;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }

    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public void setTopUps(List<SkyDiningResTopUp> topUps) {
        this.topUps = topUps;
    }

    public void setTransaction(SkyDiningTransaction transaction) {
        this.transaction = transaction;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "packageId", nullable = false)
    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    @Column(name = "sessionId", nullable = false)
    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "menuId", nullable = false)
    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @Column(name = "themeId", nullable = true)
    public Integer getThemeId() {
        return themeId;
    }

    public void setThemeId(Integer themeId) {
        this.themeId = themeId;
    }

    @Column(name = "packageName", nullable = true, length = 100)
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
