package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningMainCourse;
import com.enovax.star.cms.skydining.util.dao.RepoUtil;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.query.Query;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
@Repository
public class DefaultSkyDiningMainCourseRepository extends BaseJcrRepository<SkyDiningMainCourse> implements ISkyDiningMainCourseRepository {

  public static final String JCR_WORKSPACE = "sky-dining-admin";
  public static final String JCR_PATH = "main-courses";
  public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-main-course";

  @Override
  protected String getJcrWorkspace() {
    return JCR_WORKSPACE;
  }

  @Override
  protected String getJcrPath() {
    return JCR_PATH;
  }

  @Override
  protected SkyDiningMainCourse getNewInstance() {
    return new SkyDiningMainCourse();
  }

  @Override
  protected String getPrimaryNodeType() {
    return PRIMARY_NODE_TYPE;
  }

  protected static final String SQL_FIND_PAGED = "select * from [mgnl:sky-dining-main-course] as mc "
    + RepoUtil.FILTER_CLAUSE + RepoUtil.ORDER_CLAUSE;

  protected String getSQLFindPaged(Integer parentId, Boolean deleted,
                                   String orderBy, boolean asc) {
    String query = SQL_FIND_PAGED;
    String filterClause = "";
    String orderClause = "";

//    if (parentId != null) {
//      if (parentId > 0) {
//        filterClause += "and mc.id not in (select mcm.mainCourseId from SkyDiningMainCourseMapping mcm where mcm.menuId = :parentId) ";
//      }
//    }

    if (deleted != null) {
      if (deleted) {
        filterClause += "and mc.deleted = 1 ";
      } else {
        filterClause += "and (mc.deleted is null or mc.deleted = 0) ";
      }
    }

    if (filterClause.startsWith("and")) {
      filterClause = filterClause.substring(3);
    }
    if (StringUtils.isNotBlank(filterClause)) {
      filterClause = "where" + filterClause;
    }

    if (StringUtils.isNotEmpty(orderBy)) {
      orderClause += "order by mc." + orderBy + " ";
      if (asc) {
        orderClause += "asc";
      } else {
        orderClause += "desc";
      }
    }

    query = query.replace(RepoUtil.FILTER_CLAUSE, filterClause);
    query = query.replace(RepoUtil.ORDER_CLAUSE, orderClause);

    return query;
  }

  @Override
  public int count(Integer parentId, Boolean deleted) {
    final String query = getSQLFindPaged(parentId, deleted, null, false);
    List<SkyDiningMainCourse> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, null, null, null);
    if ((parentId != null) && (parentId > 0)) {
      result = filterNotLinkedTo("sky-dining-admin", "menus", String.valueOf(parentId), "mgnl:sky-dining-menu", "mainCourses", result);
    }
    return result.size();
  }

  @Override
  public List<SkyDiningMainCourse> findPaged(Integer parentId, Boolean deleted, int start, int pageSize, String orderBy, boolean asc) {
    final String query = getSQLFindPaged(parentId, deleted, null, false);
    List<SkyDiningMainCourse> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, null, pageSize, start);
    if ((parentId != null) && (parentId > 0)) {
      result = filterNotLinkedTo("sky-dining-admin", "menus", String.valueOf(parentId), "mgnl:sky-dining-menu", "mainCourses", result);
    }
    return result;
  }
}
