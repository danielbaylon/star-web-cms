package com.enovax.star.cms.skydining.datamodel;

import java.io.Serializable;
import java.util.Date;

public interface HasTimestamp extends Serializable {

  String getCreatedBy();

  Date getCreatedDate();

  int getId();

  String getModifiedBy();

  Date getModifiedDate();

  void setCreatedBy(String createdBy);

  void setCreatedDate(Date createdDate);

  void setModifiedBy(String modifiedBy);

  void setModifiedDate(Date modifiedDate);

}
