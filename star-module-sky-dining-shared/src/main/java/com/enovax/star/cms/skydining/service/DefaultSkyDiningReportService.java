package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.*;
import com.enovax.star.cms.skydining.datamodel.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jace on 8/11/16.
 */
@Service
public class DefaultSkyDiningReportService extends BaseSkyDiningAdminService implements ISkyDiningReportService {

    public class MainCourseColumns {
        private final List<String> stdMcs = new ArrayList<String>();
        private final List<String> kidsMcs = new ArrayList<String>();
        private final List<String> vegMcs = new ArrayList<String>();
        private final List<String> allMcs = new ArrayList<String>();

        public MainCourseColumns(List<SkyDiningReservation> reservations) {
            for (final SkyDiningReservation res : reservations) {
                final List<SkyDiningResMainCourse> resMcs = res
                    .getMainCourses();
                for (final SkyDiningResMainCourse resMc : resMcs) {
                    final SkyDiningMainCourse mc = mainCourseRepo.find(resMc.getMainCourseId());
                    if (mc != null) {
                        final String code = mc.getCode();
                        switch (MainCourseType.valueOf(mc.getType())) {
                            case Kids:
                                if (!kidsMcs.contains(code)) {
                                    kidsMcs.add(code);
                                }
                                break;
                            case Standard:
                                if (!stdMcs.contains(code)) {
                                    stdMcs.add(code);
                                }
                                break;
                            case Vegetarian:
                                if (!vegMcs.contains(code)) {
                                    vegMcs.add(code);
                                }
                                break;
                            default:
                                log.error("Invalid main course type: "
                                    + mc.getType());
                                break;
                        }
                    }
                }
            }
            Collections.sort(kidsMcs);
            Collections.sort(stdMcs);
            Collections.sort(vegMcs);
            allMcs.addAll(stdMcs);
            allMcs.addAll(kidsMcs);
            allMcs.addAll(vegMcs);
        }

        public List<String> getAllMcs() {
            return allMcs;
        }

        public int getColNum(String mcCode, int mcType) {
            int index;
            switch (MainCourseType.valueOf(mcType)) {
                case Kids:
                    index = kidsMcs.indexOf(mcCode);
                    if (index == -1) {
                        return -1;
                    } else {
                        return stdMcs.size() + index;
                    }
                case Standard:
                    index = stdMcs.indexOf(mcCode);
                    if (index == -1) {
                        return -1;
                    } else {
                        return index;
                    }
                case Vegetarian:
                    index = vegMcs.indexOf(mcCode);
                    if (index == -1) {
                        return -1;
                    } else {
                        return stdMcs.size() + kidsMcs.size() + index;
                    }
                default:
                    log.error("Invalid main course type: " + mcType);
                    return -1;
            }
        }

        public List<String> getKidsMcs() {
            return kidsMcs;
        }

        public List<String> getStdMcs() {
            return stdMcs;
        }

        public List<String> getVegMcs() {
            return vegMcs;
        }
    }

    public class MainCourseQuantities {
        private final Map<Integer, Map<String, Integer>> map = new HashMap<Integer, Map<String, Integer>>();

        public MainCourseQuantities(SkyDiningReservation res) {
            final List<SkyDiningResMainCourse> resMcs = res.getMainCourses();
            for (final SkyDiningResMainCourse resMc : resMcs) {
                final SkyDiningMainCourse mc = mainCourseRepo.find(resMc.getMainCourseId());
                if ((resMc.getQuantity() > 0) && (mc != null)) {
                    final String code = mc.getCode();
                    final Integer type = mc.getType();
                    if (map.containsKey(type)) {
                        final Map<String, Integer> codeMap = map.get(type);
                        if (codeMap.containsKey(code)) {
                            final int qty = codeMap.get(code);
                            codeMap.remove(code);
                            codeMap.put(code, qty + resMc.getQuantity());
                        } else {
                            codeMap.put(code, resMc.getQuantity());
                        }
                    } else {
                        final Map<String, Integer> codeMap = new HashMap<String, Integer>();
                        codeMap.put(code, resMc.getQuantity());
                        map.put(type, codeMap);
                    }
                }
            }
        }

        public int getQty(String code, int type) {
            if (map.containsKey(type)) {
                final Map<String, Integer> codeMap = map.get(type);
                if (codeMap.containsKey(code)) {
                    return codeMap.get(code);
                }
            }
            return 0;
        }

        public boolean isEmpty() {
            return map.isEmpty();
        }
    }

    public class MenuQuantities {
        private final Map<Integer, Map<Integer, Integer>> map = new HashMap<Integer, Map<Integer, Integer>>();

        public MenuQuantities(List<SkyDiningReservation> reservations) {
            for (final SkyDiningReservation res : reservations) {
                final SkyDiningMenu menu = menuRepo.find(res.getMenuId());
                if (menu != null) {
                    final Calendar dateOfVisit = Calendar.getInstance();
                    dateOfVisit.setTime(res.getDate());
                    final Integer day = dateOfVisit.get(Calendar.DATE);
                    if (map.containsKey(day)) {
                        final Map<Integer, Integer> menuMap = map.get(day);
                        if (menuMap.containsKey(menu.getId())) {
                            final Integer qty = menuMap.get(menu.getId());
                            menuMap.remove(menu.getId());
                            menuMap.put(menu.getId(), qty + 1);
                        } else {
                            menuMap.put(menu.getId(), 1);
                        }
                    } else {
                        final Map<Integer, Integer> menuMap = new HashMap<Integer, Integer>();
                        menuMap.put(menu.getId(), 1);
                        map.put(day, menuMap);
                    }
                }
            }
        }

        public int getQty(int day, int menuId) {
            if (map.containsKey(day)) {
                final Map<Integer, Integer> menuMap = map.get(day);
                if (menuMap.containsKey(menuId)) {
                    return menuMap.get(menuId);
                }
            }
            return 0;
        }
    }

    public class MenuRows {
        private final List<SkyDiningMenu> menus = new ArrayList<SkyDiningMenu>();

        public MenuRows(List<SkyDiningReservation> reservations) {
            for (final SkyDiningReservation res : reservations) {
                final SkyDiningMenu menu = menuRepo.find(res.getMenuId());
                if (menu != null) {
                    if (canInsert(menu)) {
                        menus.add(menu);
                    }
                }
            }
            Collections.sort(menus, new Comparator<SkyDiningMenu>() {
                @Override
                public int compare(SkyDiningMenu o1, SkyDiningMenu o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
        }

        private boolean canInsert(SkyDiningMenu newMenu) {
            for (final SkyDiningMenu menu : menus) {
                if (menu.getId() == newMenu.getId()) {
                    return false;
                }
            }
            return true;
        }

        public int getMenuId(int row) {
            return menus.get(row).getId();
        }

        public String getMenuName(int row) {
            return menus.get(row).getName();
        }

        public List<String> getMenuNames() {
            final List<String> menuNames = new ArrayList<String>();
            for (final SkyDiningMenu menu : menus) {
                menuNames.add(menu.getName());
            }
            return menuNames;
        }

        public int size() {
            return menus.size();
        }
    }

    private void addDailyBookingRecords(Sheet s,
                                        List<SkyDiningReservation> reservations, MainCourseColumns mcColumns) {
        final DateFormatSymbols dfs = new DateFormatSymbols();
        final String[] dayLabels = dfs.getShortWeekdays();

        final DataFormat df = s.getWorkbook().createDataFormat();

        final Font recordFont = s.getWorkbook().createFont();
        recordFont.setFontName("Arial");
        recordFont.setFontHeightInPoints((short) 10);

        final CellStyle textCenter = s.getWorkbook().createCellStyle();
        textCenter.setFont(recordFont);
        textCenter.setAlignment(CellStyle.ALIGN_CENTER);
        textCenter.setBorderBottom(CellStyle.BORDER_THIN);
        textCenter.setBorderLeft(CellStyle.BORDER_THIN);
        textCenter.setBorderRight(CellStyle.BORDER_THIN);
        textCenter.setWrapText(false);

        final CellStyle textLeft = s.getWorkbook().createCellStyle();
        textLeft.setFont(recordFont);
        textLeft.setAlignment(CellStyle.ALIGN_LEFT);
        textLeft.setBorderBottom(CellStyle.BORDER_THIN);
        textLeft.setBorderLeft(CellStyle.BORDER_THIN);
        textLeft.setBorderRight(CellStyle.BORDER_THIN);
        textLeft.setWrapText(false);

        final CellStyle textRight = s.getWorkbook().createCellStyle();
        textRight.setFont(recordFont);
        textRight.setAlignment(CellStyle.ALIGN_RIGHT);
        textRight.setBorderBottom(CellStyle.BORDER_THIN);
        textRight.setBorderLeft(CellStyle.BORDER_THIN);
        textRight.setBorderRight(CellStyle.BORDER_THIN);
        textRight.setWrapText(false);

        final CellStyle shortDate = s.getWorkbook().createCellStyle();
        shortDate.setFont(recordFont);
        shortDate.setAlignment(CellStyle.ALIGN_CENTER);
        shortDate.setBorderBottom(CellStyle.BORDER_THIN);
        shortDate.setBorderLeft(CellStyle.BORDER_THIN);
        shortDate.setBorderRight(CellStyle.BORDER_THIN);
        shortDate.setDataFormat(df.getFormat("d-mmm"));
        shortDate.setWrapText(false);

        final CellStyle longDate = s.getWorkbook().createCellStyle();
        longDate.setFont(recordFont);
        longDate.setAlignment(CellStyle.ALIGN_CENTER);
        longDate.setBorderBottom(CellStyle.BORDER_THIN);
        longDate.setBorderLeft(CellStyle.BORDER_THIN);
        longDate.setBorderRight(CellStyle.BORDER_THIN);
        longDate.setDataFormat(df.getFormat("d/m/yy"));
        longDate.setWrapText(false);

        final CellStyle money = s.getWorkbook().createCellStyle();
        money.setFont(recordFont);
        money.setAlignment(CellStyle.ALIGN_CENTER);
        money.setBorderBottom(CellStyle.BORDER_THIN);
        money.setBorderLeft(CellStyle.BORDER_THIN);
        money.setBorderRight(CellStyle.BORDER_THIN);
        money.setDataFormat(df.getFormat("#,##0.00"));
        money.setWrapText(false);

        Row r;
        Cell c;

        for (int i = 0; i < reservations.size(); i++) {
            final SkyDiningReservation reservation = reservations.get(i);

            final Calendar dateOfVisit = Calendar.getInstance();
            dateOfVisit.setTime(reservation.getDate());

            final Calendar dateOfBooking = Calendar.getInstance();
            dateOfBooking.setTime(reservation.getCreatedDate());
            dateOfBooking.set(Calendar.HOUR_OF_DAY, 0);
            dateOfBooking.set(Calendar.MINUTE, 0);
            dateOfBooking.set(Calendar.SECOND, 0);
            dateOfBooking.set(Calendar.MILLISECOND, 0);

            r = s.createRow(i + 1);

            c = r.createCell(0);
            c.setCellStyle(textRight);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(1);
            c.setCellStyle(shortDate);
            c.setCellValue(dateOfBooking);

            c = r.createCell(2);
            c.setCellStyle(shortDate);
            c.setCellValue(dateOfVisit);

            c = r.createCell(3);
            c.setCellStyle(textCenter);
            c.setCellValue(dayLabels[dateOfVisit.get(Calendar.DAY_OF_WEEK)]);

            c = r.createCell(4);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(Days.daysBetween(new DateTime(dateOfBooking),
                new DateTime(dateOfVisit)).getDays());

            c = r.createCell(5);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(6);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(7);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(reservation.getGuestName());

            c = r.createCell(8);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(reservation.getContactNumber());

            c = r.createCell(9);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            switch (ReservationStatus.valueOf(reservation.getStatus())) {
                case Cancelled:
                    c.setCellValue("Cancelled");
                    break;
                case PaidInsideSystem:
                    c.setCellValue("Paid Online");
                    break;
                case PaidOutsideSystem:
                    c.setCellValue("Paid Offline");
                    break;
                default:
                    c.setCellValue("Not Paid");
                    break;
            }

            c = r.createCell(10);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(reservation.getPax());

            final MainCourseQuantities mcQuantities = new MainCourseQuantities(
                reservation);

            int offset = 11;
            if (!mcQuantities.isEmpty()) {
                List<String> mcCodes;

                mcCodes = mcColumns.getStdMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(mcQuantities.getQty(mcCodes.get(j), 0));
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getKidsMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(mcQuantities.getQty(mcCodes.get(j), 1));
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getVegMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(mcQuantities.getQty(mcCodes.get(j), 2));
                }
                offset += mcCodes.size();
            } else {
                List<String> mcCodes;

                mcCodes = mcColumns.getStdMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(0);
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getKidsMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(0);
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getVegMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(0);
                }
                offset += mcCodes.size();
            }

            c = r.createCell(offset + 0);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            switch (ReservationStatus.valueOf(reservation.getStatus())) {
                case Cancelled:
                    c.setCellValue("Cancelled");
                    break;
                case PaidInsideSystem:
                    c.setCellValue("Paid Online");
                    break;
                case PaidOutsideSystem:
                    c.setCellValue("Paid Offline");
                    break;
                case Unpaid:
                    c.setCellValue("Cancelled");
                    break;
                default:
                    c.setCellValue("Not Paid");
                    break;
            }

            c = r.createCell(offset + 1);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(reservation.getReceiptNum());

            c = r.createCell(offset + 2);
            c.setCellStyle(longDate);
            if (reservation.getPaymentDate() != null) {
                c.setCellValue(reservation.getPaymentDate());
            }

            c = r.createCell(offset + 3);
            c.setCellStyle(money);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            if (reservation.getPaidAmount() != null) {
                c.setCellValue(reservation.getPaidAmount().doubleValue());
            }

            c = r.createCell(offset + 4);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            if (reservation.getPaymentMode() != null) {
                switch (PaymentMode.valueOf(reservation.getPaymentMode())) {
                    case Cash:
                        c.setCellValue("Cash");
                        break;
                    case eNETS:
                        c.setCellValue("eNett");
                        break;
                    case VISA:
                        c.setCellValue("VISA");
                        break;
                    case MasterCard:
                        c.setCellValue("MasterCard");
                        break;
                    case ChinaUnionPay:
                        c.setCellValue("China UnionPay");
                        break;
                    case OtherCard:
                        c.setCellValue("Other Card");
                        break;
                    default:
                        c.setCellValue("");
                        break;
                }
            }

            c = r.createCell(offset + 5);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(reservation.getReceivedBy());

            c = r.createCell(offset + 6);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(offset + 7);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(getTopUpText(reservation, TopUpType.Cake));

            c = r.createCell(offset + 8);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(getTopUpText(reservation, TopUpType.Flowers));

            c = r.createCell(offset + 9);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(getTopUpText(reservation, TopUpType.Wine));

            c = r.createCell(offset + 10);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(reservation.getSpecialRequest());

            c = r.createCell(offset + 11);
            c.setCellStyle(money);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);

            c = r.createCell(offset + 12);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(offset + 13);
            c.setCellStyle(longDate);

            c = r.createCell(offset + 14);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(offset + 15);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(offset + 16);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(reservation.getEmailAddress());

            c = r.createCell(offset + 17);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

        }
    }

    private void addDailyOrderAdditional(Sheet s,
                                         List<SkyDiningReservation> reservations, MainCourseColumns mcColumns) {
        final DataFormat df = s.getWorkbook().createDataFormat();

        final Font titleFont = s.getWorkbook().createFont();
        titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        titleFont.setItalic(true);
        titleFont.setFontName("Arial");
        titleFont.setFontHeightInPoints((short) 18);

        final Font headerFont = s.getWorkbook().createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        headerFont.setItalic(true);
        headerFont.setFontName("Arial");
        headerFont.setFontHeightInPoints((short) 14);

        final Font recordFont = s.getWorkbook().createFont();
        recordFont.setFontName("Arial");
        recordFont.setFontHeightInPoints((short) 14);

        final CellStyle title = s.getWorkbook().createCellStyle();
        title.setAlignment(CellStyle.ALIGN_LEFT);
        title.setFont(titleFont);
        title.setWrapText(false);

        final CellStyle headerLeft = s.getWorkbook().createCellStyle();
        headerLeft.setFont(headerFont);
        headerLeft.setAlignment(CellStyle.ALIGN_CENTER);
        headerLeft.setBorderTop(CellStyle.BORDER_MEDIUM);
        headerLeft.setBorderBottom(CellStyle.BORDER_MEDIUM);
        headerLeft.setBorderLeft(CellStyle.BORDER_MEDIUM);
        headerLeft.setBorderRight(CellStyle.BORDER_THIN);
        headerLeft.setWrapText(true);

        final CellStyle headerMiddle = s.getWorkbook().createCellStyle();
        headerMiddle.setFont(headerFont);
        headerMiddle.setAlignment(CellStyle.ALIGN_CENTER);
        headerMiddle.setBorderTop(CellStyle.BORDER_MEDIUM);
        headerMiddle.setBorderBottom(CellStyle.BORDER_MEDIUM);
        headerMiddle.setBorderLeft(CellStyle.BORDER_THIN);
        headerMiddle.setBorderRight(CellStyle.BORDER_THIN);
        headerMiddle.setWrapText(true);

        final CellStyle headerRight = s.getWorkbook().createCellStyle();
        headerRight.setFont(headerFont);
        headerRight.setAlignment(CellStyle.ALIGN_CENTER);
        headerRight.setBorderTop(CellStyle.BORDER_MEDIUM);
        headerRight.setBorderBottom(CellStyle.BORDER_MEDIUM);
        headerRight.setBorderLeft(CellStyle.BORDER_THIN);
        headerRight.setBorderRight(CellStyle.BORDER_MEDIUM);
        headerRight.setWrapText(true);

        final CellStyle recordLeft = s.getWorkbook().createCellStyle();
        recordLeft.setFont(recordFont);
        recordLeft.setAlignment(CellStyle.ALIGN_CENTER);
        recordLeft.setBorderTop(CellStyle.BORDER_THIN);
        recordLeft.setBorderBottom(CellStyle.BORDER_THIN);
        recordLeft.setBorderLeft(CellStyle.BORDER_MEDIUM);
        recordLeft.setBorderRight(CellStyle.BORDER_THIN);
        recordLeft.setWrapText(false);

        final CellStyle recordMiddleAlignLeft = s.getWorkbook()
            .createCellStyle();
        recordMiddleAlignLeft.setFont(recordFont);
        recordMiddleAlignLeft.setAlignment(CellStyle.ALIGN_LEFT);
        recordMiddleAlignLeft.setBorderTop(CellStyle.BORDER_THIN);
        recordMiddleAlignLeft.setBorderBottom(CellStyle.BORDER_THIN);
        recordMiddleAlignLeft.setBorderLeft(CellStyle.BORDER_THIN);
        recordMiddleAlignLeft.setBorderRight(CellStyle.BORDER_THIN);
        recordMiddleAlignLeft.setWrapText(false);

        final CellStyle recordMiddleAlignCenter = s.getWorkbook()
            .createCellStyle();
        recordMiddleAlignCenter.setFont(recordFont);
        recordMiddleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
        recordMiddleAlignCenter.setBorderTop(CellStyle.BORDER_THIN);
        recordMiddleAlignCenter.setBorderBottom(CellStyle.BORDER_THIN);
        recordMiddleAlignCenter.setBorderLeft(CellStyle.BORDER_THIN);
        recordMiddleAlignCenter.setBorderRight(CellStyle.BORDER_THIN);
        recordMiddleAlignCenter.setWrapText(false);

        final CellStyle recordRight = s.getWorkbook().createCellStyle();
        recordRight.setFont(recordFont);
        recordRight.setAlignment(CellStyle.ALIGN_CENTER);
        recordRight.setBorderTop(CellStyle.BORDER_THIN);
        recordRight.setBorderBottom(CellStyle.BORDER_THIN);
        recordRight.setBorderLeft(CellStyle.BORDER_THIN);
        recordRight.setBorderRight(CellStyle.BORDER_MEDIUM);
        recordRight.setWrapText(false);
        recordRight.setDataFormat(df.getFormat("d/m/yy"));

        final CellStyle footerLeft = s.getWorkbook().createCellStyle();
        footerLeft.setFont(recordFont);
        footerLeft.setAlignment(CellStyle.ALIGN_CENTER);
        footerLeft.setBorderTop(CellStyle.BORDER_THIN);
        footerLeft.setBorderBottom(CellStyle.BORDER_MEDIUM);
        footerLeft.setBorderLeft(CellStyle.BORDER_MEDIUM);
        footerLeft.setBorderRight(CellStyle.BORDER_THIN);
        footerLeft.setWrapText(false);

        final CellStyle footerMiddleAlignLeft = s.getWorkbook()
            .createCellStyle();
        footerMiddleAlignLeft.setFont(recordFont);
        footerMiddleAlignLeft.setAlignment(CellStyle.ALIGN_LEFT);
        footerMiddleAlignLeft.setBorderTop(CellStyle.BORDER_THIN);
        footerMiddleAlignLeft.setBorderBottom(CellStyle.BORDER_MEDIUM);
        footerMiddleAlignLeft.setBorderLeft(CellStyle.BORDER_THIN);
        footerMiddleAlignLeft.setBorderRight(CellStyle.BORDER_THIN);
        footerMiddleAlignLeft.setWrapText(false);

        final CellStyle footerMiddleAlignCenter = s.getWorkbook()
            .createCellStyle();
        footerMiddleAlignCenter.setFont(recordFont);
        footerMiddleAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
        footerMiddleAlignCenter.setBorderTop(CellStyle.BORDER_THIN);
        footerMiddleAlignCenter.setBorderBottom(CellStyle.BORDER_MEDIUM);
        footerMiddleAlignCenter.setBorderLeft(CellStyle.BORDER_THIN);
        footerMiddleAlignCenter.setBorderRight(CellStyle.BORDER_THIN);
        footerMiddleAlignCenter.setWrapText(false);

        final CellStyle footerRight = s.getWorkbook().createCellStyle();
        footerRight.setFont(recordFont);
        footerRight.setAlignment(CellStyle.ALIGN_CENTER);
        footerRight.setBorderTop(CellStyle.BORDER_THIN);
        footerRight.setBorderBottom(CellStyle.BORDER_MEDIUM);
        footerRight.setBorderLeft(CellStyle.BORDER_THIN);
        footerRight.setBorderRight(CellStyle.BORDER_MEDIUM);
        footerRight.setWrapText(false);
        footerRight.setDataFormat(df.getFormat("d/m/yy"));

        final int mcQuantitites = mcColumns.getAllMcs().size();
        final int startRow = 9 + reservations.size();

        Row r;
        Cell c;

        r = s.createRow(startRow);
        c = r.createCell(0);
        c.setCellStyle(title);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Additional Order");

        r = s.createRow(startRow + 1);
        r.setHeightInPoints(35);

        c = r.createCell(2);
        c.setCellStyle(headerLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Cabin");

        c = r.createCell(3);
        c.setCellStyle(headerMiddle);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Cake");

        c = r.createCell(4);
        c.setCellStyle(headerMiddle);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Flower");

        c = r.createCell(5);
        c.setCellStyle(headerMiddle);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Wine");

        c = r.createCell(6);
        c.setCellStyle(headerMiddle);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Remarks");

        for (int j = 1; j <= mcQuantitites; j++) {
            c = r.createCell(6 + j);
            c.setCellStyle(headerMiddle);
            c.setCellType(Cell.CELL_TYPE_STRING);
        }

        s.addMergedRegion(new CellRangeAddress(startRow + 1, startRow + 1, 6,
            6 + mcQuantitites));

        c = r.createCell(7 + mcQuantitites);
        c.setCellStyle(headerMiddle);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Other Payment");

        c = r.createCell(8 + mcQuantitites);
        c.setCellStyle(headerMiddle);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Receipt No.");

        c = r.createCell(9 + mcQuantitites);
        c.setCellStyle(headerRight);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Date of Payment");

        int topUpRows = 0;
        for (int i = 0; i < reservations.size(); i++) {
            final SkyDiningReservation res = reservations.get(i);
            topUpRows += 1;

            r = s.createRow(startRow + 1 + topUpRows);

            c = r.createCell(2);
            c.setCellStyle(recordLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(3);
            c.setCellStyle(recordMiddleAlignLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(getTopUpText(res, TopUpType.Cake));

            c = r.createCell(4);
            c.setCellStyle(recordMiddleAlignLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(getTopUpText(res, TopUpType.Flowers));

            c = r.createCell(5);
            c.setCellStyle(recordMiddleAlignLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(getTopUpText(res, TopUpType.Wine));

            c = r.createCell(6);
            c.setCellStyle(recordMiddleAlignLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(res.getSpecialRequest());

            for (int j = 1; j <= mcQuantitites; j++) {
                c = r.createCell(6 + j);
                c.setCellStyle(recordMiddleAlignLeft);
                c.setCellType(Cell.CELL_TYPE_STRING);
            }

            s.addMergedRegion(new CellRangeAddress(startRow + 1 + topUpRows,
                startRow + 1 + topUpRows, 6, 6 + mcQuantitites));

            c = r.createCell(7 + mcQuantitites);
            c.setCellStyle(recordMiddleAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(8 + mcQuantitites);
            c.setCellStyle(recordMiddleAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(9 + mcQuantitites);
            c.setCellStyle(recordRight);
            c.setCellType(Cell.CELL_TYPE_STRING);

        }

        r = s.createRow(startRow + 2 + topUpRows);

        c = r.createCell(2);
        c.setCellStyle(footerLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(3);
        c.setCellStyle(footerMiddleAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(4);
        c.setCellStyle(footerMiddleAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(5);
        c.setCellStyle(footerMiddleAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(6);
        c.setCellStyle(footerMiddleAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);

        for (int j = 1; j <= mcQuantitites; j++) {
            c = r.createCell(6 + j);
            c.setCellStyle(footerMiddleAlignLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
        }

        s.addMergedRegion(new CellRangeAddress(startRow + 2 + topUpRows,
            startRow + 2 + topUpRows, 6, 6 + mcQuantitites));

        c = r.createCell(7 + mcQuantitites);
        c.setCellStyle(footerMiddleAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(8 + mcQuantitites);
        c.setCellStyle(footerMiddleAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(9 + mcQuantitites);
        c.setCellStyle(footerRight);
        c.setCellType(Cell.CELL_TYPE_STRING);

    }

    private void addDailyOrderRecords(Sheet s,
                                      List<SkyDiningReservation> reservations, MainCourseColumns mcColumns) {
        final DataFormat df = s.getWorkbook().createDataFormat();

        final Font recordFont = s.getWorkbook().createFont();
        recordFont.setFontName("Arial");
        recordFont.setFontHeightInPoints((short) 14);

        final CellStyle textCenter = s.getWorkbook().createCellStyle();
        textCenter.setFont(recordFont);
        textCenter.setAlignment(CellStyle.ALIGN_CENTER);
        textCenter.setBorderBottom(CellStyle.BORDER_THIN);
        textCenter.setBorderLeft(CellStyle.BORDER_THIN);
        textCenter.setBorderRight(CellStyle.BORDER_THIN);
        textCenter.setWrapText(false);

        final CellStyle textLeft = s.getWorkbook().createCellStyle();
        textLeft.setFont(recordFont);
        textLeft.setAlignment(CellStyle.ALIGN_LEFT);
        textLeft.setBorderBottom(CellStyle.BORDER_THIN);
        textLeft.setBorderLeft(CellStyle.BORDER_THIN);
        textLeft.setBorderRight(CellStyle.BORDER_THIN);
        textLeft.setWrapText(false);

        final CellStyle date = s.getWorkbook().createCellStyle();
        date.setFont(recordFont);
        date.setAlignment(CellStyle.ALIGN_CENTER);
        date.setBorderBottom(CellStyle.BORDER_THIN);
        date.setBorderLeft(CellStyle.BORDER_THIN);
        date.setBorderRight(CellStyle.BORDER_THIN);
        date.setDataFormat(df.getFormat("d/m/yy"));
        date.setWrapText(false);

        final CellStyle money = s.getWorkbook().createCellStyle();
        money.setFont(recordFont);
        money.setAlignment(CellStyle.ALIGN_CENTER);
        money.setBorderBottom(CellStyle.BORDER_THIN);
        money.setBorderLeft(CellStyle.BORDER_THIN);
        money.setBorderRight(CellStyle.BORDER_THIN);
        money.setDataFormat(df.getFormat("#,##0.00"));
        money.setWrapText(false);

        Row r;
        Cell c;

        for (int i = 0; i < reservations.size(); i++) {
            final SkyDiningReservation res = reservations.get(i);
            r = s.createRow(6 + i);

            c = r.createCell(0);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(1);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(2);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);

            c = r.createCell(3);
            c.setCellStyle(textLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(res.getGuestName());

            c = r.createCell(4);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(res.getContactNumber());

            c = r.createCell(5);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            switch (ReservationStatus.valueOf(res.getStatus())) {
                case Cancelled:
                    c.setCellValue("Cancelled");
                    break;
                case PaidInsideSystem:
                case PaidOutsideSystem:
                    c.setCellValue("Paid");
                    break;
                default:
                    c.setCellValue("Not Paid");
                    break;
            }

            c = r.createCell(6);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(res.getPax());

            final MainCourseQuantities mcQuantities = new MainCourseQuantities(
                res);

            int offset = 7;
            if (!mcQuantities.isEmpty()) {
                List<String> mcCodes;

                mcCodes = mcColumns.getStdMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(mcQuantities.getQty(mcCodes.get(j), 0));
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getKidsMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(mcQuantities.getQty(mcCodes.get(j), 1));
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getVegMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(mcQuantities.getQty(mcCodes.get(j), 2));
                }
                offset += mcCodes.size();
            } else {
                List<String> mcCodes;

                mcCodes = mcColumns.getStdMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(0);
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getKidsMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(0);
                }
                offset += mcCodes.size();

                mcCodes = mcColumns.getVegMcs();
                for (int j = 0; j < mcCodes.size(); j++) {
                    c = r.createCell(offset + j);
                    c.setCellStyle(textCenter);
                    c.setCellType(Cell.CELL_TYPE_NUMERIC);
                    c.setCellValue(0);
                }
                offset += mcCodes.size();
            }

            c = r.createCell(offset);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            switch (ReservationStatus.valueOf(res.getStatus())) {
                case Cancelled:
                    c.setCellValue("Cancelled");
                    break;
                case PaidInsideSystem:
                case PaidOutsideSystem:
                    c.setCellValue("Paid");
                    break;
                default:
                    c.setCellValue("Not Paid");
                    break;
            }

            c = r.createCell(offset + 1);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(res.getReceiptNum());

            c = r.createCell(offset + 2);
            c.setCellStyle(date);
            if (res.getPaymentDate() != null) {
                c.setCellValue(res.getPaymentDate());
            }

            c = r.createCell(offset + 3);
            c.setCellStyle(money);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            if (res.getPaidAmount() != null) {
                c.setCellValue(res.getPaidAmount().doubleValue());
            }

            c = r.createCell(offset + 4);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            if (res.getPaymentMode() != null) {
                switch (PaymentMode.valueOf(res.getPaymentMode())) {
                    case Cash:
                        c.setCellValue("Cash");
                        break;
                    case eNETS:
                        c.setCellValue("eNett");
                        break;
                    case VISA:
                        c.setCellValue("VISA");
                        break;
                    case MasterCard:
                        c.setCellValue("MasterCard");
                        break;
                    case ChinaUnionPay:
                        c.setCellValue("China UnionPay");
                        break;
                    case OtherCard:
                        c.setCellValue("Other Card");
                        break;
                    default:
                        c.setCellValue("");
                        break;
                }
            }

            c = r.createCell(offset + 5);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(res.getReceivedBy());
        }

        r = s.createRow(6 + reservations.size());

        c = r.createCell(0);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(1);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(2);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(3);
        c.setCellStyle(textLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(4);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(5);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(6);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_NUMERIC);

        for (int i = 0; i < mcColumns.getAllMcs().size(); i++) {
            c = r.createCell(6 + i);
            c.setCellStyle(textCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        final int offset = 7 + mcColumns.getAllMcs().size();

        c = r.createCell(offset);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(offset + 1);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(offset + 2);
        c.setCellStyle(date);

        c = r.createCell(offset + 3);
        c.setCellStyle(money);
        c.setCellType(Cell.CELL_TYPE_NUMERIC);

        c = r.createCell(offset + 4);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(offset + 5);
        c.setCellStyle(textCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
    }

    private Sheet createDailyBookingSheet(Workbook wb, String sheetName,
                                          MainCourseColumns mcColumns) {
        final Font headerFont = wb.createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        headerFont.setFontName("Arial");
        headerFont.setFontHeightInPoints((short) 10);

        final CellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFont(headerFont);
        headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
        headerStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
        headerStyle.setBorderLeft(CellStyle.BORDER_THIN);
        headerStyle.setBorderRight(CellStyle.BORDER_THIN);
        headerStyle.setWrapText(true);

        final String[] beforeMcHeaders = {"No", "Date", "Date of Dining",
            "Day", "Days Interval", "No.", "R/No.", "Name", "Contact",
            "Payment Status", "Pax"};
        final String[] afterMcHeaders = {"Payment Need to Collect",
            "Receipt No", "Date of Payment", "Payment Received",
            "Mode of Payment", "Cashier", "No", "Cake", "Flower", "Wine",
            "Remark", "Other Payment", "Receipt No", "Date of Payment",
            "Recieved By", "Source", "E-mail", "E-mail 2"};
        final short[] beforeMcWidths = {9, 8, 8, 8, 8, 6, 10, 33, 16, 12, 5};
        final short[] afterMcWidths = {13, 14, 11, 11, 11, 11, 11, 28, 16, 17,
            127, 11, 15, 11, 11, 11, 30, 30};

        Sheet s;
        Row r;
        Cell c;

        s = wb.createSheet(sheetName);
        r = s.createRow(0);
        r.setHeightInPoints(25);

        int offset = 0;
        for (int i = 0; i < beforeMcHeaders.length; i++) {
            final String header = beforeMcHeaders[i];
            final short width = beforeMcWidths[i];

            c = r.createCell(i, Cell.CELL_TYPE_STRING);
            c.setCellStyle(headerStyle);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(header);

            s.setColumnWidth(i, (width * 256) + 200);
        }
        offset = beforeMcHeaders.length;

        final List<String> mcCodes = mcColumns.getAllMcs();
        for (int i = 0; i < mcCodes.size(); i++) {
            final String header = mcCodes.get(i);
            final short width = 5;

            c = r.createCell(offset + i, Cell.CELL_TYPE_STRING);
            c.setCellStyle(headerStyle);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(header);

            s.setColumnWidth(offset + i, (width * 256) + 200);
        }
        offset += mcCodes.size();

        for (int i = 0; i < afterMcHeaders.length; i++) {
            final String header = afterMcHeaders[i];
            final short width = afterMcWidths[i];

            c = r.createCell(offset + i, Cell.CELL_TYPE_STRING);
            c.setCellStyle(headerStyle);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(header);

            s.setColumnWidth(offset + i, (width * 256) + 200);
        }
        offset += afterMcHeaders.length;

        final String endColumn = CellReference
            .convertNumToColString(offset - 1);
        s.setAutoFilter(CellRangeAddress.valueOf("A1:" + endColumn + "1"));

        return s;
    }

    private Sheet createDailyOrderSheet(Workbook wb, String sheetName,
                                        Date reportDate, List<SkyDiningReservation> reservations,
                                        MainCourseColumns mcColumns) {

        final String[] beforeMcHeaders = {"Cabin", "No.", "S./No", "Name",
            "Contact", "Status", "Pax"};
        final String[] afterMcHeaders = {"Payment to Collect", "Receipt No.",
            "Date of Payment", "Total Pay", "Mode of Payment", "Cashier"};
        final short[] beforeMcWidths = {8, 8, 11, 51, 17, 23, 5};
        final short[] afterMcWidths = {25, 16, 16, 16, 16, 16};

        final DataFormat df = wb.createDataFormat();

        final Font arial18Bold = wb.createFont();
        arial18Bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial18Bold.setFontName("Arial");
        arial18Bold.setFontHeightInPoints((short) 18);

        final Font arial16Bold = wb.createFont();
        arial16Bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial16Bold.setFontName("Arial");
        arial16Bold.setFontHeightInPoints((short) 16);

        final Font arial16RedBoldUnderline = wb.createFont();
        arial16RedBoldUnderline.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial16RedBoldUnderline.setUnderline(Font.U_SINGLE);
        arial16RedBoldUnderline.setFontName("Arial");
        arial16RedBoldUnderline.setFontHeightInPoints((short) 16);
        arial16RedBoldUnderline.setColor(IndexedColors.RED.getIndex());

        final Font arial14Bold = wb.createFont();
        arial14Bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial14Bold.setFontName("Arial");
        arial14Bold.setFontHeightInPoints((short) 14);

        final Font arial14BoldItalic = wb.createFont();
        arial14BoldItalic.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial14BoldItalic.setItalic(true);
        arial14BoldItalic.setFontName("Arial");
        arial14BoldItalic.setFontHeightInPoints((short) 14);

        final Font arial14BoldUnderline = wb.createFont();
        arial14BoldUnderline.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial14BoldUnderline.setUnderline(Font.U_SINGLE);
        arial14BoldUnderline.setFontName("Arial");
        arial14BoldUnderline.setFontHeightInPoints((short) 14);

        final Font arial14 = wb.createFont();
        arial14.setFontName("Arial");
        arial14.setFontHeightInPoints((short) 14);

        final Font arial12Bold = wb.createFont();
        arial12Bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        arial12Bold.setFontName("Arial");
        arial12Bold.setFontHeightInPoints((short) 12);

        final Font arial12Italic = wb.createFont();
        arial12Italic.setItalic(true);
        arial12Italic.setFontName("Arial");
        arial12Italic.setFontHeightInPoints((short) 12);

        final Font arial12 = wb.createFont();
        arial12.setFontName("Arial");
        arial12.setFontHeightInPoints((short) 12);

        final CellStyle toText = wb.createCellStyle();
        toText.setFont(arial18Bold);
        toText.setAlignment(CellStyle.ALIGN_LEFT);
        toText.setWrapText(false);

        final CellStyle toSubText = wb.createCellStyle();
        toSubText.setFont(arial12Italic);
        toSubText.setAlignment(CellStyle.ALIGN_LEFT);
        toSubText.setWrapText(false);

        final CellStyle fromText = wb.createCellStyle();
        fromText.setFont(arial12Bold);
        fromText.setAlignment(CellStyle.ALIGN_LEFT);
        fromText.setWrapText(false);

        final CellStyle contactNumber = wb.createCellStyle();
        contactNumber.setFont(arial12);
        contactNumber.setAlignment(CellStyle.ALIGN_LEFT);
        contactNumber.setWrapText(false);

        final CellStyle generationDate = wb.createCellStyle();
        generationDate.setFont(arial12);
        generationDate.setAlignment(CellStyle.ALIGN_CENTER);
        generationDate.setWrapText(false);
        generationDate.setDataFormat(df.getFormat("d-mmm-yy"));

        final CellStyle generationTime = wb.createCellStyle();
        generationTime.setFont(arial12);
        generationTime.setAlignment(CellStyle.ALIGN_CENTER);
        generationTime.setWrapText(false);
        generationTime.setDataFormat(df.getFormat("h:mmAM/PM"));

        final CellStyle subjectText = wb.createCellStyle();
        subjectText.setFont(arial16Bold);
        subjectText.setAlignment(CellStyle.ALIGN_LEFT);
        subjectText.setWrapText(false);

        final CellStyle headerText = wb.createCellStyle();
        headerText.setFont(arial14Bold);
        headerText.setAlignment(CellStyle.ALIGN_CENTER);
        headerText.setBorderTop(CellStyle.BORDER_THIN);
        headerText.setBorderBottom(CellStyle.BORDER_THIN);
        headerText.setBorderLeft(CellStyle.BORDER_THIN);
        headerText.setBorderRight(CellStyle.BORDER_THIN);
        headerText.setWrapText(true);

        final CellStyle contentText = wb.createCellStyle();
        contentText.setFont(arial14);
        contentText.setAlignment(CellStyle.ALIGN_CENTER);
        contentText.setBorderTop(CellStyle.BORDER_THIN);
        contentText.setBorderBottom(CellStyle.BORDER_THIN);
        contentText.setBorderLeft(CellStyle.BORDER_THIN);
        contentText.setBorderRight(CellStyle.BORDER_THIN);
        contentText.setWrapText(false);

        final CellStyle totalText = wb.createCellStyle();
        totalText.setFont(arial14BoldUnderline);
        totalText.setAlignment(CellStyle.ALIGN_RIGHT);
        totalText.setBorderTop(CellStyle.BORDER_THIN);
        totalText.setBorderBottom(CellStyle.BORDER_THIN);
        totalText.setBorderLeft(CellStyle.BORDER_THIN);
        totalText.setBorderRight(CellStyle.BORDER_THIN);
        totalText.setWrapText(false);

        final CellStyle totalNumber = wb.createCellStyle();
        totalNumber.setFont(arial14Bold);
        totalNumber.setAlignment(CellStyle.ALIGN_CENTER);
        totalNumber.setBorderTop(CellStyle.BORDER_THIN);
        totalNumber.setBorderBottom(CellStyle.BORDER_THIN);
        totalNumber.setBorderLeft(CellStyle.BORDER_THIN);
        totalNumber.setBorderRight(CellStyle.BORDER_THIN);
        totalNumber.setWrapText(false);

        final CellStyle totalMoney = wb.createCellStyle();
        totalMoney.setFont(arial14Bold);
        totalMoney.setAlignment(CellStyle.ALIGN_CENTER);
        totalMoney.setBorderTop(CellStyle.BORDER_THIN);
        totalMoney.setBorderBottom(CellStyle.BORDER_THIN);
        totalMoney.setBorderLeft(CellStyle.BORDER_THIN);
        totalMoney.setBorderRight(CellStyle.BORDER_THIN);
        totalMoney.setDataFormat(df.getFormat("* #,##0.00"));
        totalMoney.setWrapText(false);

        final CellStyle preparedByText = wb.createCellStyle();
        preparedByText.setFont(arial14BoldItalic);
        preparedByText.setAlignment(CellStyle.ALIGN_CENTER);
        preparedByText.setBorderTop(CellStyle.BORDER_THIN);
        preparedByText.setBorderBottom(CellStyle.BORDER_THIN);
        preparedByText.setBorderLeft(CellStyle.BORDER_THIN);
        preparedByText.setBorderRight(CellStyle.BORDER_THIN);
        preparedByText.setWrapText(false);

        final int mcColumnCount = mcColumns.getAllMcs().size();

        Sheet s;
        Row r;
        Cell c;

        s = wb.createSheet(sheetName);

        r = s.createRow(0);

        c = r.createCell(0);
        c.setCellStyle(toText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("To: Guest Relation & Kitchen");

        c = r.createCell(mcColumnCount + 8);
        c.setCellStyle(fromText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("From: Mount Faber Leisure Group Pte Ltd");

        r = s.createRow(1);

        c = r.createCell(0);
        c.setCellStyle(toSubText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Sky Dining Crew");

        c = r.createCell(mcColumnCount + 8);
        c.setCellStyle(contactNumber);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Tel: 6377-9633 & Fax: 6273 4639");

        final Date now = new Date();

        c = r.createCell(mcColumnCount + 11);
        c.setCellStyle(generationDate);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue(now);

        c = r.createCell(mcColumnCount + 12);
        c.setCellStyle(generationTime);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue(now);

        r = s.createRow(2);

        final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd EEEE");
        final String subjectString = "SKY DINING DAILY CONFIRMATION ORDER on "
            + sdf.format(reportDate);

        c = r.createCell(1);
        c.setCellStyle(subjectText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue(subjectString);
        final RichTextString rts = c.getRichStringCellValue();
        rts.applyFont(39, subjectString.length(), arial16RedBoldUnderline);
        c.setCellValue(rts);

        c = r.createCell(mcColumnCount + 8);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("For Cashier Use Only:");

        c = r.createCell(mcColumnCount + 9);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 10);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 11);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 12);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        String startCol = CellReference
            .convertNumToColString(mcColumnCount + 8);
        String endCol = CellReference.convertNumToColString(mcColumnCount + 12);
        s.addMergedRegion(CellRangeAddress.valueOf(startCol + 3 + ":" + endCol
            + 3));

        r = s.createRow(3);

        c = r.createCell(mcColumnCount + 8);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 9);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 10);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 11);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(mcColumnCount + 12);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        startCol = CellReference.convertNumToColString(mcColumnCount + 8);
        endCol = CellReference.convertNumToColString(mcColumnCount + 12);
        s.addMergedRegion(CellRangeAddress.valueOf(startCol + 4 + ":" + endCol
            + 4));

        r = s.createRow(4);

        c = r.createCell(6);
        c.setCellStyle(contentText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Set");

        final int mcStdLen = mcColumns.getStdMcs().size();
        final int mcKidsLen = mcColumns.getKidsMcs().size();
        final int mcVegLen = mcColumns.getVegMcs().size();

        int offset = 7;

        for (int i = offset; i < (offset + mcColumnCount); i++) {
            c = r.createCell(i);
            c.setCellStyle(headerText);
            c.setCellType(Cell.CELL_TYPE_STRING);
        }

        if (mcStdLen > 0) {
            c = r.createCell(offset);
            c.setCellStyle(contentText);
            c.setCellType(Cell.CELL_TYPE_STRING);
            startCol = CellReference.convertNumToColString(offset);
            endCol = CellReference
                .convertNumToColString((offset + mcStdLen) - 1);
            s.addMergedRegion(CellRangeAddress.valueOf(startCol + 5 + ":"
                + endCol + 5));
            offset += mcStdLen;
        }
        if (mcKidsLen > 0) {
            c = r.createCell(offset);
            c.setCellStyle(contentText);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue("Kid");
            startCol = CellReference.convertNumToColString(offset);
            endCol = CellReference
                .convertNumToColString((offset + mcKidsLen) - 1);
            s.addMergedRegion(CellRangeAddress.valueOf(startCol + 5 + ":"
                + endCol + 5));
            offset += mcKidsLen;
        }
        if (mcVegLen > 0) {
            c = r.createCell(offset);
            c.setCellStyle(contentText);
            c.setCellType(Cell.CELL_TYPE_STRING);
            startCol = CellReference.convertNumToColString(offset);
            endCol = CellReference
                .convertNumToColString((offset + mcVegLen) - 1);
            s.addMergedRegion(CellRangeAddress.valueOf(startCol + 5 + ":"
                + endCol + 5));
            offset += mcVegLen;
        }

        c = r.createCell(offset);
        c.setCellStyle(contentText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(offset + 1);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Receipt No.");

        c = r.createCell(offset + 2);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Date of Payment");

        c = r.createCell(offset + 3);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Total Pay");

        c = r.createCell(offset + 4);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Mode of Payment");

        c = r.createCell(offset + 5);
        c.setCellStyle(headerText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Cashier");

        r = s.createRow(5);

        offset = 0;
        for (int i = 0; i < beforeMcHeaders.length; i++) {
            final String header = beforeMcHeaders[i];
            final short width = beforeMcWidths[i];

            c = r.createCell(i, Cell.CELL_TYPE_STRING);
            c.setCellStyle(headerText);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(header);

            s.setColumnWidth(i, (width * 256) + 200);
        }
        offset = beforeMcHeaders.length;

        final List<String> mcCodes = mcColumns.getAllMcs();
        for (int i = 0; i < mcCodes.size(); i++) {
            final String header = mcCodes.get(i);
            final short width = 5;

            c = r.createCell(offset + i, Cell.CELL_TYPE_STRING);
            c.setCellStyle(headerText);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(header);

            s.setColumnWidth(offset + i, (width * 256) + 200);
        }
        offset += mcCodes.size();

        for (int i = 0; i < afterMcHeaders.length; i++) {
            final String header = afterMcHeaders[i];
            final short width = afterMcWidths[i];

            c = r.createCell(offset + i, Cell.CELL_TYPE_STRING);
            c.setCellStyle(headerText);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(header);

            s.setColumnWidth(offset + i, (width * 256) + 200);
        }

        s.addMergedRegion(new CellRangeAddress(4, 5, offset + 1, offset + 1));
        s.addMergedRegion(new CellRangeAddress(4, 5, offset + 2, offset + 2));
        s.addMergedRegion(new CellRangeAddress(4, 5, offset + 3, offset + 3));
        s.addMergedRegion(new CellRangeAddress(4, 5, offset + 4, offset + 4));
        s.addMergedRegion(new CellRangeAddress(4, 5, offset + 5, offset + 5));

        r = s.createRow(7 + reservations.size());

        c = r.createCell(5);
        c.setCellStyle(totalText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Total");

        offset = 6;
        for (int i = 0; i < (mcColumnCount + 1); i++) {
            final String column = CellReference.convertNumToColString(offset
                + i);
            final StringBuilder formula = new StringBuilder("SUM(");
            formula.append(column);
            formula.append(7);
            formula.append(":");
            formula.append(column);
            formula.append(7 + reservations.size());
            formula.append(")");

            c = r.createCell(offset + i);
            c.setCellStyle(totalNumber);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(formula.toString());
        }

        c = r.createCell(7 + mcColumnCount);
        c.setCellStyle(preparedByText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Prepared By: system");

        c = r.createCell(8 + mcColumnCount);
        c.setCellStyle(preparedByText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        s.addMergedRegion(new CellRangeAddress(7 + reservations.size(),
            7 + reservations.size(), 7 + mcColumnCount, 8 + mcColumnCount));

        c = r.createCell(9 + mcColumnCount);
        c.setCellStyle(totalText);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Total");

        final String column = CellReference
            .convertNumToColString(10 + mcColumnCount);
        final StringBuilder formula = new StringBuilder("SUM(");
        formula.append(column);
        formula.append(7);
        formula.append(":");
        formula.append(column);
        formula.append(7 + reservations.size());
        formula.append(")");

        c = r.createCell(10 + mcColumnCount);
        c.setCellStyle(totalMoney);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(formula.toString());

        c = r.createCell(11 + mcColumnCount);
        c.setCellStyle(contentText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        c = r.createCell(12 + mcColumnCount);
        c.setCellStyle(contentText);
        c.setCellType(Cell.CELL_TYPE_STRING);

        return s;
    }

    private Sheet createRevenueSheet(Workbook wb, String sheetName,
                                     Calendar startDate, Calendar endDate) {
        final DataFormat df = wb.createDataFormat();

        final Font fontBold = wb.createFont();
        fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);
        fontBold.setFontName("Arial Narrow");
        fontBold.setFontHeightInPoints((short) 11);

        final Font fontNormal = wb.createFont();
        fontNormal.setFontName("Arial Narrow");
        fontNormal.setFontHeightInPoints((short) 11);

        final CellStyle notes = wb.createCellStyle();
        notes.setFont(fontNormal);
        notes.setAlignment(CellStyle.ALIGN_LEFT);
        notes.setWrapText(false);

        final CellStyle headerNoBorderAlignLeft = wb.createCellStyle();
        headerNoBorderAlignLeft.setFont(fontBold);
        headerNoBorderAlignLeft.setAlignment(CellStyle.ALIGN_LEFT);
        headerNoBorderAlignLeft.setWrapText(false);

        final CellStyle headerNoBorderAlignCenter = wb.createCellStyle();
        headerNoBorderAlignCenter.setFont(fontBold);
        headerNoBorderAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
        headerNoBorderAlignCenter.setWrapText(false);

        final CellStyle headerWithBorderAlignCenter = wb.createCellStyle();
        headerWithBorderAlignCenter.setFont(fontBold);
        headerWithBorderAlignCenter.setAlignment(CellStyle.ALIGN_CENTER);
        headerWithBorderAlignCenter.setBorderTop(CellStyle.BORDER_THIN);
        headerWithBorderAlignCenter.setBorderBottom(CellStyle.BORDER_THIN);
        headerWithBorderAlignCenter.setBorderLeft(CellStyle.BORDER_THIN);
        headerWithBorderAlignCenter.setBorderRight(CellStyle.BORDER_THIN);
        headerWithBorderAlignCenter.setWrapText(true);

        final CellStyle headerWithBorderAlignLeft = wb.createCellStyle();
        headerWithBorderAlignLeft.setFont(fontBold);
        headerWithBorderAlignLeft.setAlignment(CellStyle.ALIGN_LEFT);
        headerWithBorderAlignLeft.setBorderTop(CellStyle.BORDER_THIN);
        headerWithBorderAlignLeft.setBorderBottom(CellStyle.BORDER_THIN);
        headerWithBorderAlignLeft.setBorderLeft(CellStyle.BORDER_THIN);
        headerWithBorderAlignLeft.setBorderRight(CellStyle.BORDER_THIN);
        headerWithBorderAlignLeft.setWrapText(true);

        final CellStyle contentTextAlignLeft = wb.createCellStyle();
        contentTextAlignLeft.setFont(fontNormal);
        contentTextAlignLeft.setAlignment(CellStyle.ALIGN_LEFT);
        contentTextAlignLeft.setBorderTop(CellStyle.BORDER_THIN);
        contentTextAlignLeft.setBorderBottom(CellStyle.BORDER_THIN);
        contentTextAlignLeft.setBorderLeft(CellStyle.BORDER_THIN);
        contentTextAlignLeft.setBorderRight(CellStyle.BORDER_THIN);
        contentTextAlignLeft.setWrapText(false);

        final CellStyle contentTextAlignRight = wb.createCellStyle();
        contentTextAlignRight.setFont(fontNormal);
        contentTextAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
        contentTextAlignRight.setBorderTop(CellStyle.BORDER_THIN);
        contentTextAlignRight.setBorderBottom(CellStyle.BORDER_THIN);
        contentTextAlignRight.setBorderLeft(CellStyle.BORDER_THIN);
        contentTextAlignRight.setBorderRight(CellStyle.BORDER_THIN);
        contentTextAlignRight.setWrapText(false);

        final CellStyle contentTextBoldAlignRight = wb.createCellStyle();
        contentTextBoldAlignRight.setFont(fontBold);
        contentTextBoldAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);
        contentTextBoldAlignRight.setBorderTop(CellStyle.BORDER_THIN);
        contentTextBoldAlignRight.setBorderBottom(CellStyle.BORDER_THIN);
        contentTextBoldAlignRight.setBorderLeft(CellStyle.BORDER_THIN);
        contentTextBoldAlignRight.setBorderRight(CellStyle.BORDER_THIN);
        contentTextBoldAlignRight.setWrapText(false);

        final CellStyle contentMoneyNormal = wb.createCellStyle();
        contentMoneyNormal.setFont(fontNormal);
        contentMoneyNormal.setAlignment(CellStyle.ALIGN_RIGHT);
        contentMoneyNormal.setBorderTop(CellStyle.BORDER_THIN);
        contentMoneyNormal.setBorderBottom(CellStyle.BORDER_THIN);
        contentMoneyNormal.setBorderLeft(CellStyle.BORDER_THIN);
        contentMoneyNormal.setBorderRight(CellStyle.BORDER_THIN);
        contentMoneyNormal.setWrapText(false);
        contentMoneyNormal.setDataFormat(df.getFormat("* #,###"));

        final CellStyle contentMoneyBold = wb.createCellStyle();
        contentMoneyBold.setFont(fontBold);
        contentMoneyBold.setAlignment(CellStyle.ALIGN_RIGHT);
        contentMoneyBold.setBorderTop(CellStyle.BORDER_THIN);
        contentMoneyBold.setBorderBottom(CellStyle.BORDER_THIN);
        contentMoneyBold.setBorderLeft(CellStyle.BORDER_THIN);
        contentMoneyBold.setBorderRight(CellStyle.BORDER_THIN);
        contentMoneyBold.setWrapText(false);
        contentMoneyBold.setDataFormat(df.getFormat("* #,###"));

        final CellStyle contentMoneyBoldWithSymbol = wb.createCellStyle();
        contentMoneyBoldWithSymbol.setFont(fontBold);
        contentMoneyBoldWithSymbol.setAlignment(CellStyle.ALIGN_RIGHT);
        contentMoneyBoldWithSymbol.setBorderTop(CellStyle.BORDER_THIN);
        contentMoneyBoldWithSymbol.setBorderBottom(CellStyle.BORDER_THIN);
        contentMoneyBoldWithSymbol.setBorderLeft(CellStyle.BORDER_THIN);
        contentMoneyBoldWithSymbol.setBorderRight(CellStyle.BORDER_THIN);
        contentMoneyBoldWithSymbol.setWrapText(false);
        contentMoneyBoldWithSymbol.setDataFormat(df.getFormat("#,##0.00"));

        final CellStyle contentPercentBold = wb.createCellStyle();
        contentPercentBold.setFont(fontBold);
        contentPercentBold.setAlignment(CellStyle.ALIGN_RIGHT);
        contentPercentBold.setBorderTop(CellStyle.BORDER_THIN);
        contentPercentBold.setBorderBottom(CellStyle.BORDER_THIN);
        contentPercentBold.setBorderLeft(CellStyle.BORDER_THIN);
        contentPercentBold.setBorderRight(CellStyle.BORDER_THIN);
        contentPercentBold.setWrapText(false);
        contentPercentBold.setDataFormat(df.getFormat("#,##0%"));

        final Sheet s = wb.createSheet(sheetName);
        Row r;
        Cell c;

//        final ReservationFilterCriteria criteria = new ReservationFilterCriteria();
//        criteria.setPageSize(-1);
//        criteria.setStart(-1);
//        criteria.setOrderBy("createdDate");
//        criteria.setAsc(true);
//        criteria.setVisitStartDate(NvxDateUtils.formatDate(startDate.getTime(),
//            SysConst.DEFAULT_DATE_FORMAT));
//        criteria.setVisitEndDate(NvxDateUtils.formatDate(endDate.getTime(),
//            SysConst.DEFAULT_DATE_FORMAT));
//        criteria.setIgnoreCancelled(true);
//        criteria.setIgnoreUnpaid(true);

        final List<SkyDiningReservation> reservations = reservationRepo.findForReports(
            startDate.getTime(),
            endDate.getTime(),
            true,
            true
        );

        final int startDay = startDate.get(Calendar.DATE);
        final int endDay = endDate.get(Calendar.DATE);
        final int daysCovered = (endDay - startDay) + 1;

        r = s.createRow(0);

        c = r.createCell(0);
        c.setCellStyle(headerNoBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("SKY DINING");
        s.setColumnWidth(0, (28 * 256) + 200);

        r = s.createRow(1);

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerNoBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(day);

            s.setColumnWidth(1 + (day - startDay), (7 * 256) + 200);
        }

        c = r.createCell(daysCovered + 1);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("MTD Rev");
        s.addMergedRegion(new CellRangeAddress(1, 2, daysCovered + 1,
            daysCovered + 1));
        s.setColumnWidth(daysCovered + 1, (10 * 256) + 200);

        c = r.createCell(daysCovered + 2);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Daily Target");
        s.addMergedRegion(new CellRangeAddress(1, 2, daysCovered + 2,
            daysCovered + 2));
        s.setColumnWidth(daysCovered + 2, (10 * 256) + 200);

        c = r.createCell(daysCovered + 3);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("MTD Cume Target");
        s.addMergedRegion(new CellRangeAddress(1, 2, daysCovered + 3,
            daysCovered + 3));
        s.setColumnWidth(daysCovered + 3, (10 * 256) + 200);

        c = r.createCell(daysCovered + 4);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("MTD % Position");
        s.addMergedRegion(new CellRangeAddress(1, 2, daysCovered + 4,
            daysCovered + 4));
        s.setColumnWidth(daysCovered + 4, (10 * 256) + 200);

        c = r.createCell(daysCovered + 5);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Overall Target");
        s.addMergedRegion(new CellRangeAddress(1, 2, daysCovered + 5,
            daysCovered + 5));
        s.setColumnWidth(daysCovered + 5, (10 * 256) + 200);

        c = r.createCell(daysCovered + 6);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Overall % Position");
        s.addMergedRegion(new CellRangeAddress(1, 2, daysCovered + 6,
            daysCovered + 6));
        s.setColumnWidth(daysCovered + 6, (10 * 256) + 200);

        r = s.createRow(2);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("REVENUE");

        final DateFormatSymbols dfs = new DateFormatSymbols();
        final String[] daysOfWeekText = dfs.getShortWeekdays();
        for (int day = startDay; day <= endDay; day++) {
            final Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(startDate.getTime());
            currentDate.set(Calendar.DATE, day);

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerWithBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(daysOfWeekText[currentDate.get(Calendar.DAY_OF_WEEK)]);
        }

        for (int i = 1; i <= 6; i++) {
            c = r.createCell(daysCovered + i);
            c.setCellStyle(headerWithBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
        }

        r = s.createRow(3);

        c = r.createCell(0);
        c.setCellStyle(contentTextAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Nett Revenue (incl svc)");

        for (int day = startDay; day <= endDay; day++) {
            final Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(startDate.getTime());
            currentDate.set(Calendar.DATE, day);

            BigDecimal totalRevenue = new BigDecimal(0);
            for (final SkyDiningReservation res : reservations) {
                if ((res.getPaidAmount() != null)
                    && DateUtils.isSameDay(currentDate.getTime(),
                    res.getDate())) {
                    totalRevenue = totalRevenue.add(res.getPaidAmount());
                }
            }

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(contentMoneyNormal);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(totalRevenue.doubleValue());
        }

        for (int i = 1; i <= 6; i++) {
            c = r.createCell(daysCovered + i);
            c.setCellStyle(contentMoneyNormal);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        r = s.createRow(4);

        c = r.createCell(0);
        c.setCellStyle(contentTextAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Less Cable Car");

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(contentMoneyNormal);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        for (int i = 1; i <= 6; i++) {
            c = r.createCell(daysCovered + i);
            c.setCellStyle(contentMoneyNormal);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        r = s.createRow(5);

        c = r.createCell(0);
        c.setCellStyle(contentTextAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Less Other Costs");

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(contentMoneyNormal);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        for (int i = 1; i <= 6; i++) {
            c = r.createCell(daysCovered + i);
            c.setCellStyle(contentMoneyNormal);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        r = s.createRow(6);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Day Result");

        for (int day = startDay; day <= endDay; day++) {
            final int columnNumber = 1 + (day - startDay);
            final String columnString = CellReference
                .convertNumToColString(columnNumber);

            final StringBuilder formula = new StringBuilder();
            formula.append(columnString);
            formula.append("4-");
            formula.append(columnString);
            formula.append("5-");
            formula.append(columnString);
            formula.append("6");

            c = r.createCell(columnNumber);
            c.setCellStyle(contentMoneyBold);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(formula.toString());
        }

        final StringBuilder mtdRev = new StringBuilder("SUM(");
        mtdRev.append(CellReference.convertNumToColString(1));
        mtdRev.append("7:");
        mtdRev.append(CellReference.convertNumToColString(daysCovered));
        mtdRev.append("7)");

        c = r.createCell(daysCovered + 1);
        c.setCellStyle(contentMoneyNormal);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(mtdRev.toString());

        final StringBuilder dailyTarget = new StringBuilder("");
        dailyTarget
            .append(CellReference.convertNumToColString(daysCovered + 5));
        dailyTarget.append("7/");
        dailyTarget.append(daysCovered);

        c = r.createCell(daysCovered + 2);
        c.setCellStyle(contentMoneyNormal);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(dailyTarget.toString());

        c = r.createCell(daysCovered + 3);
        c.setCellStyle(contentMoneyNormal);
        c.setCellType(Cell.CELL_TYPE_NUMERIC);

        final StringBuilder mtdPercent = new StringBuilder("(");
        mtdPercent.append(CellReference.convertNumToColString(daysCovered + 1));
        mtdPercent.append("7-");
        mtdPercent.append(CellReference.convertNumToColString(daysCovered + 3));
        mtdPercent.append("7)/");
        mtdPercent.append(CellReference.convertNumToColString(daysCovered + 3));
        mtdPercent.append("7");

        c = r.createCell(daysCovered + 4);
        c.setCellStyle(contentPercentBold);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(mtdPercent.toString());

        c = r.createCell(daysCovered + 5);
        c.setCellStyle(contentMoneyNormal);
        c.setCellType(Cell.CELL_TYPE_NUMERIC);

        final StringBuilder overallPercent = new StringBuilder("(");
        overallPercent.append(CellReference
            .convertNumToColString(daysCovered + 1));
        overallPercent.append("7-");
        overallPercent.append(CellReference
            .convertNumToColString(daysCovered + 5));
        overallPercent.append("7)/");
        overallPercent.append(CellReference
            .convertNumToColString(daysCovered + 5));
        overallPercent.append("7");

        c = r.createCell(daysCovered + 6);
        c.setCellStyle(contentPercentBold);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(overallPercent.toString());

        r = s.createRow(7);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Daily Target");

        for (int day = startDay; day <= endDay; day++) {
            final int columnNumber = 1 + (day - startDay);
            c = r.createCell(columnNumber);
            c.setCellStyle(contentMoneyBold);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(dailyTarget.toString());
        }

        r = s.createRow(8);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Daily Position %");

        for (int day = startDay; day <= endDay; day++) {
            final int columnNumber = 1 + (day - startDay);
            final String columnString = CellReference
                .convertNumToColString(columnNumber);

            final StringBuilder formula = new StringBuilder("(");
            formula.append(columnString);
            formula.append("7-");
            formula.append(columnString);
            formula.append("8)/");
            formula.append(columnString);
            formula.append("8");

            c = r.createCell(columnNumber);
            c.setCellStyle(contentPercentBold);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(formula.toString());
        }

        r = s.createRow(9);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Ahead/Behind by Day");

        for (int day = startDay; day <= endDay; day++) {
            final int columnNumber = 1 + (day - startDay);
            final String columnString = CellReference
                .convertNumToColString(columnNumber);

            final StringBuilder formula = new StringBuilder("");
            formula.append(columnString);
            formula.append("7-");
            formula.append(columnString);
            formula.append("8");

            c = r.createCell(columnNumber);
            c.setCellStyle(contentMoneyBold);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(formula.toString());
        }

        r = s.createRow(10);

        c = r.createCell(0);
        c.setCellStyle(notes);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Note: revenue = (nett+svc-ent-cctix)");

        r = s.createRow(12);

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerNoBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(day);
        }

        r = s.createRow(13);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("COVERS");

        for (int day = startDay; day <= endDay; day++) {
            final Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(startDate.getTime());
            currentDate.set(Calendar.DATE, day);

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerWithBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(daysOfWeekText[currentDate.get(Calendar.DAY_OF_WEEK)]);
        }

        c = r.createCell(daysCovered + 1);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Total");

        r = s.createRow(14);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Sub Totals");

        for (int day = startDay; day <= endDay; day++) {
            final Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(startDate.getTime());
            currentDate.set(Calendar.DATE, day);

            int total = 0;
            for (final SkyDiningReservation res : reservations) {
                if (DateUtils.isSameDay(currentDate.getTime(), res.getDate())) {
                    total++;
                }
            }

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(contentTextBoldAlignRight);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(total);
        }

        final StringBuilder coverTotal = new StringBuilder("SUM(");
        coverTotal.append(CellReference.convertNumToColString(1));
        coverTotal.append("15:");
        coverTotal.append(CellReference.convertNumToColString(daysCovered));
        coverTotal.append("15)");

        c = r.createCell(daysCovered + 1);
        c.setCellStyle(contentTextBoldAlignRight);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(coverTotal.toString());

        r = s.createRow(17);

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerNoBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(day);
        }

        r = s.createRow(18);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("AVG CHEQUES");

        for (int day = startDay; day <= endDay; day++) {
            final Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(startDate.getTime());
            currentDate.set(Calendar.DATE, day);

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerWithBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(daysOfWeekText[currentDate.get(Calendar.DAY_OF_WEEK)]);
        }

        c = r.createCell(daysCovered + 1);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("AVG Cheq");

        r = s.createRow(19);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Sub Totals");

        for (int day = startDay; day <= endDay; day++) {
            final int columnNumber = 1 + (day - startDay);
            final String columnString = CellReference
                .convertNumToColString(columnNumber);

            final StringBuilder formula = new StringBuilder("");
            formula.append(columnString);
            formula.append("7/");
            formula.append(columnString);
            formula.append("15");

            c = r.createCell(columnNumber);
            c.setCellStyle(contentMoneyBold);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(formula.toString());
        }

        final StringBuilder avgCheq = new StringBuilder("");
        avgCheq.append(CellReference.convertNumToColString(daysCovered + 1));
        avgCheq.append("7/");
        avgCheq.append(CellReference.convertNumToColString(daysCovered + 1));
        avgCheq.append("15");

        c = r.createCell(daysCovered + 1);
        c.setCellStyle(contentMoneyBoldWithSymbol);
        c.setCellType(Cell.CELL_TYPE_FORMULA);
        c.setCellFormula(avgCheq.toString());

        r = s.createRow(22);

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerNoBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
            c.setCellValue(day);
        }

        r = s.createRow(23);

        c = r.createCell(0);
        c.setCellStyle(headerWithBorderAlignCenter);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("CURRENT BOOKINGS (covers)");

        for (int day = startDay; day <= endDay; day++) {
            final Calendar currentDate = Calendar.getInstance();
            currentDate.setTime(startDate.getTime());
            currentDate.set(Calendar.DATE, day);

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(headerWithBorderAlignCenter);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(daysOfWeekText[currentDate.get(Calendar.DAY_OF_WEEK)]);
        }

        final MenuRows menuRows = new MenuRows(reservations);
        final MenuQuantities menuQuantities = new MenuQuantities(reservations);
        int offset = 24;
        for (int i = 0; i < menuRows.size(); i++) {
            r = s.createRow(offset + i);

            c = r.createCell(0);
            c.setCellStyle(headerWithBorderAlignLeft);
            c.setCellType(Cell.CELL_TYPE_STRING);
            c.setCellValue(menuRows.getMenuName(i));

            final int menuId = menuRows.getMenuId(i);
            for (int day = startDay; day <= endDay; day++) {
                c = r.createCell((1 + (day - startDay)));
                c.setCellStyle(contentTextAlignRight);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);
                c.setCellValue(menuQuantities.getQty(day, menuId));
            }
        }
        offset += menuRows.size();

        r = s.createRow(offset);

        c = r.createCell(0);
        c.setCellStyle(contentTextAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Final extra bookings on the day ");

        for (int day = startDay; day <= endDay; day++) {
            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(contentTextAlignRight);
            c.setCellType(Cell.CELL_TYPE_NUMERIC);
        }

        r = s.createRow(offset + 1);

        c = r.createCell(0);
        c.setCellStyle(contentTextAlignLeft);
        c.setCellType(Cell.CELL_TYPE_STRING);
        c.setCellValue("Total");

        for (int day = startDay; day <= endDay; day++) {
            final int columnNumber = 1 + (day - startDay);
            final String columnString = CellReference
                .convertNumToColString(columnNumber);

            final StringBuilder formula = new StringBuilder("SUM(");
            formula.append(columnString);
            formula.append("25:");
            formula.append(columnString);
            formula.append(25 + menuRows.size());
            formula.append(")");

            c = r.createCell((1 + (day - startDay)));
            c.setCellStyle(contentTextAlignRight);
            c.setCellType(Cell.CELL_TYPE_FORMULA);
            c.setCellFormula(formula.toString());
        }

        return s;
    }

    @Override
    @Transactional(readOnly = true)
    public Workbook generateDailyBookingExcel(String startDate, String endDate) {
        final Workbook wb = new XSSFWorkbook();

        final DateFormatSymbols dfs = new DateFormatSymbols();
        final String[] monthLabels = dfs.getShortMonths();

        final Calendar reportStart = Calendar.getInstance();
        if (StringUtils.isNotBlank(startDate)) {
            try {
                reportStart.setTime(NvxDateUtils.parseDate(startDate,
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Unable to parse date: " + startDate);
            }
        } else {
            reportStart.setTime(new Date());
            reportStart.set(Calendar.DAY_OF_YEAR, 1);
            reportStart.set(Calendar.HOUR_OF_DAY, 0);
            reportStart.set(Calendar.MINUTE, 0);
            reportStart.set(Calendar.SECOND, 0);
            reportStart.set(Calendar.MILLISECOND, 0);
        }

        final Calendar reportEnd = Calendar.getInstance();
        if (StringUtils.isNotBlank(endDate)) {
            try {
                reportEnd.setTime(NvxDateUtils.parseDate(endDate,
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Unable to parse date: " + endDate);
            }
        } else {
            reportEnd.setTime(new Date());
            reportEnd.set(Calendar.DAY_OF_YEAR,
                reportEnd.getActualMaximum(Calendar.DAY_OF_YEAR));
            reportEnd.set(Calendar.HOUR_OF_DAY, 0);
            reportEnd.set(Calendar.MINUTE, 0);
            reportEnd.set(Calendar.SECOND, 0);
            reportEnd.set(Calendar.MILLISECOND, 0);
        }

        final int yearsCovered = (reportEnd.get(Calendar.YEAR) - reportStart
            .get(Calendar.YEAR)) + 1;

        for (int year = reportStart.get(Calendar.YEAR); year <= reportEnd
            .get(Calendar.YEAR); year++) {
            for (int month = (year == reportStart.get(Calendar.YEAR) ? reportStart
                .get(Calendar.MONTH) : 0); month <= (year == reportEnd
                .get(Calendar.YEAR) ? reportEnd.get(Calendar.MONTH) : 11); month++) {

                final Calendar sheetStart = Calendar.getInstance();
                sheetStart.set(Calendar.YEAR, year);
                sheetStart.set(Calendar.MONTH, month);
                if ((month == reportStart.get(Calendar.MONTH))
                    && (year == reportStart.get(Calendar.YEAR))) {
                    sheetStart.set(Calendar.DATE,
                        reportStart.get(Calendar.DATE));
                } else {
                    sheetStart.set(Calendar.DATE, 1);
                }

                final Calendar sheetEnd = Calendar.getInstance();
                sheetEnd.set(Calendar.YEAR, year);
                sheetEnd.set(Calendar.MONTH, month);
                if ((month == reportEnd.get(Calendar.MONTH))
                    && (year == reportEnd.get(Calendar.YEAR))) {
                    sheetEnd.set(Calendar.DATE, reportEnd.get(Calendar.DATE));
                } else {
                    sheetEnd.set(Calendar.DATE,
                        sheetEnd.getActualMaximum(Calendar.DATE));
                }

                String sheetName = null;
                if (yearsCovered == 1) {
                    sheetName = monthLabels[month];
                } else {
                    sheetName = monthLabels[month] + " " + year;
                }

//                final ReservationFilterCriteria criteria = new ReservationFilterCriteria();
//                criteria.setPageSize(-1);
//                criteria.setStart(-1);
//                criteria.setOrderBy("createdDate");
//                criteria.setAsc(true);
//                criteria.setVisitStartDate(NvxDateUtils.formatDate(
//                    sheetStart.getTime(), SysConst.DEFAULT_DATE_FORMAT));
//                criteria.setVisitEndDate(NvxDateUtils.formatDate(sheetEnd.getTime(),
//                    SysConst.DEFAULT_DATE_FORMAT));

                final List<SkyDiningReservation> reservations = reservationRepo.findForReports(
                    sheetStart.getTime(),
                    sheetEnd.getTime(),
                    false,
                    false
                );

                final MainCourseColumns mcColumns = new MainCourseColumns(
                    reservations);

                final Sheet s = createDailyBookingSheet(wb, sheetName,
                    mcColumns);

                addDailyBookingRecords(s, reservations, mcColumns);

            }
        }

        return wb;
    }

    @Override
    @Transactional(readOnly = true)
    public Workbook generateDailyOrderExcel(String startDate, String endDate) {
        final Workbook wb = new XSSFWorkbook();

        final DateFormatSymbols dfs = new DateFormatSymbols();
        final String[] monthLabels = dfs.getShortMonths();

        final Calendar reportStart = Calendar.getInstance();
        if (StringUtils.isNotBlank(startDate)) {
            try {
                reportStart.setTime(NvxDateUtils.parseDate(startDate,
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Unable to parse date: " + startDate);
            }
        } else {
            reportStart.setTime(new Date());
            reportStart.set(Calendar.DAY_OF_MONTH, 1);
            reportStart.set(Calendar.HOUR_OF_DAY, 0);
            reportStart.set(Calendar.MINUTE, 0);
            reportStart.set(Calendar.SECOND, 0);
            reportStart.set(Calendar.MILLISECOND, 0);
        }

        final Calendar reportEnd = Calendar.getInstance();
        if (StringUtils.isNotBlank(endDate)) {
            try {
                reportEnd.setTime(NvxDateUtils.parseDate(endDate,
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Unable to parse date: " + endDate);
            }
        } else {
            reportEnd.setTime(new Date());
            reportEnd.set(Calendar.DAY_OF_MONTH,
                reportEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
            reportEnd.set(Calendar.HOUR_OF_DAY, 0);
            reportEnd.set(Calendar.MINUTE, 0);
            reportEnd.set(Calendar.SECOND, 0);
            reportEnd.set(Calendar.MILLISECOND, 0);
        }

        final int yearsCovered = (reportEnd.get(Calendar.YEAR) - reportStart
            .get(Calendar.YEAR)) + 1;
        final int monthsCovered = Months.monthsBetween(
            new DateTime(reportStart), new DateTime(reportEnd)).getMonths() + 1;

        for (int year = reportStart.get(Calendar.YEAR); year <= reportEnd
            .get(Calendar.YEAR); year++) {
            for (int month = (year == reportStart.get(Calendar.YEAR) ? reportStart
                .get(Calendar.MONTH) : 0); month <= (year == reportEnd
                .get(Calendar.YEAR) ? reportEnd.get(Calendar.MONTH) : 11); month++) {

                int dayStart;
                final int dayEnd;

                if ((year == reportStart.get(Calendar.YEAR))
                    && (month == reportStart.get(Calendar.MONTH))) {
                    dayStart = reportStart.get(Calendar.DATE);
                } else {
                    dayStart = 1;
                }
                if ((year == reportEnd.get(Calendar.YEAR))
                    && (month == reportEnd.get(Calendar.MONTH))) {
                    dayEnd = reportEnd.get(Calendar.DATE);
                } else {
                    final Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    dayEnd = calendar.getMaximum(Calendar.DATE);
                }

                for (int day = dayStart; day <= dayEnd; day++) {

                    final Calendar sheetStart = Calendar.getInstance();
                    sheetStart.set(Calendar.YEAR, year);
                    sheetStart.set(Calendar.MONTH, month);
                    sheetStart.set(Calendar.DATE, day);

                    final Calendar sheetEnd = Calendar.getInstance();
                    sheetEnd.set(Calendar.YEAR, year);
                    sheetEnd.set(Calendar.MONTH, month);
                    sheetEnd.set(Calendar.DATE, day);

                    String sheetName = null;
                    if ((yearsCovered == 1) && (monthsCovered == 1)) {
                        sheetName = Integer.toString(day);
                    } else if (yearsCovered == 1) {
                        sheetName = day + " " + monthLabels[month];
                    } else {
                        sheetName = day + " " + monthLabels[month] + " " + year;
                    }

//                    final ReservationFilterCriteria criteria = new ReservationFilterCriteria();
//                    criteria.setPageSize(-1);
//                    criteria.setStart(-1);
//                    criteria.setOrderBy("createdDate");
//                    criteria.setAsc(true);
//                    criteria.setVisitStartDate(NvxDateUtils.formatDate(
//                        sheetStart.getTime(), SysConst.DEFAULT_DATE_FORMAT));
//                    criteria.setVisitEndDate(NvxDateUtils.formatDate(
//                        sheetEnd.getTime(), SysConst.DEFAULT_DATE_FORMAT));
//                    criteria.setIgnoreCancelled(true);

                    final List<SkyDiningReservation> reservations = reservationRepo.findForReports(
                        sheetStart.getTime(),
                        sheetEnd.getTime(),
                        false,
                        false
                    );

                    final MainCourseColumns mcColumns = new MainCourseColumns(
                        reservations);

                    final Sheet s = createDailyOrderSheet(wb, sheetName,
                        sheetStart.getTime(), reservations, mcColumns);
                    addDailyOrderRecords(s, reservations, mcColumns);
                    addDailyOrderAdditional(s, reservations, mcColumns);
                }
            }
        }

        return wb;
    }

    @Override
    @Transactional(readOnly = true)
    public Workbook generateRevenueExcel(String startDate, String endDate) {
        final Workbook wb = new XSSFWorkbook();

        final DateFormatSymbols dfs = new DateFormatSymbols();
        final String[] monthLabels = dfs.getShortMonths();

        final Calendar reportStart = Calendar.getInstance();
        if (StringUtils.isNotBlank(startDate)) {
            try {
                reportStart.setTime(NvxDateUtils.parseDate(startDate,
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Unable to parse date: " + startDate);
            }
        } else {
            reportStart.setTime(new Date());
            reportStart.set(Calendar.DAY_OF_YEAR, 1);
            reportStart.set(Calendar.HOUR_OF_DAY, 0);
            reportStart.set(Calendar.MINUTE, 0);
            reportStart.set(Calendar.SECOND, 0);
            reportStart.set(Calendar.MILLISECOND, 0);
        }

        final Calendar reportEnd = Calendar.getInstance();
        if (StringUtils.isNotBlank(endDate)) {
            try {
                reportEnd.setTime(NvxDateUtils.parseDate(endDate,
                    SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
                log.error("Unable to parse date: " + endDate);
            }
        } else {
            reportEnd.setTime(new Date());
            reportEnd.set(Calendar.DAY_OF_YEAR,
                reportEnd.getActualMaximum(Calendar.DAY_OF_YEAR));
            reportEnd.set(Calendar.HOUR_OF_DAY, 0);
            reportEnd.set(Calendar.MINUTE, 0);
            reportEnd.set(Calendar.SECOND, 0);
            reportEnd.set(Calendar.MILLISECOND, 0);
        }

        final int yearsCovered = (reportEnd.get(Calendar.YEAR) - reportStart
            .get(Calendar.YEAR)) + 1;

        for (int year = reportStart.get(Calendar.YEAR); year <= reportEnd
            .get(Calendar.YEAR); year++) {
            for (int month = (year == reportStart.get(Calendar.YEAR) ? reportStart
                .get(Calendar.MONTH) : 0); month <= (year == reportEnd
                .get(Calendar.YEAR) ? reportEnd.get(Calendar.MONTH) : 11); month++) {

                final Calendar sheetStart = Calendar.getInstance();
                sheetStart.set(Calendar.YEAR, year);
                sheetStart.set(Calendar.MONTH, month);
                if ((month == reportStart.get(Calendar.MONTH))
                    && (year == reportStart.get(Calendar.YEAR))) {
                    sheetStart.set(Calendar.DATE,
                        reportStart.get(Calendar.DATE));
                } else {
                    sheetStart.set(Calendar.DATE, 1);
                }

                final Calendar sheetEnd = Calendar.getInstance();
                sheetEnd.set(Calendar.YEAR, year);
                sheetEnd.set(Calendar.MONTH, month);
                if ((month == reportEnd.get(Calendar.MONTH))
                    && (year == reportEnd.get(Calendar.YEAR))) {
                    sheetEnd.set(Calendar.DATE, reportEnd.get(Calendar.DATE));
                } else {
                    sheetEnd.set(Calendar.DATE,
                        sheetEnd.getActualMaximum(Calendar.DATE));
                }

                String sheetName = null;
                if (yearsCovered == 1) {
                    sheetName = monthLabels[month];
                } else {
                    sheetName = monthLabels[month] + " " + year;
                }

                createRevenueSheet(wb, sheetName, sheetStart, sheetEnd);
            }
        }

        return wb;
    }

    private String getTopUpText(SkyDiningReservation reservation, TopUpType type) {
        final List<SkyDiningResTopUp> restTopUps = reservation.getTopUps();
        final StringBuilder sb = new StringBuilder();
        for (final SkyDiningResTopUp resTopUp : restTopUps) {
            final SkyDiningTopUp topUp = topUpRepo.find(resTopUp.getTopUpId());
            if ((resTopUp.getQuantity() > 0) && (topUp != null)
                && (type != null)
                && (type == TopUpType.valueOf(topUp.getType()))) {
                sb.append(topUp.getName());
                sb.append(" x");
                sb.append(resTopUp.getQuantity());
                sb.append(", ");
            }
        }
        if (sb.length() > 2) {
            sb.delete(sb.length() - 2, sb.length() - 1);
        }
        return sb.toString();
    }

}
