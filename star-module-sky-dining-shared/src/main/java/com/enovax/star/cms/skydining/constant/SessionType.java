package com.enovax.star.cms.skydining.constant;

public enum SessionType {
  Date(0), Normal(1);

  public final int value;

  SessionType(int value) {
    this.value = value;
  }

}
