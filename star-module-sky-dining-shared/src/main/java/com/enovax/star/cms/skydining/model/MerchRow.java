package com.enovax.star.cms.skydining.model;

/**
 * Created by jace on 3/8/16.
 */
public class MerchRow {
  private String id;
  private String name;
  private boolean active;
  private String bank;
  private String mid;

  public MerchRow() {

  }

  public MerchRow(String id, String name, boolean active, String bank, String mid) {
    this.id = id;
    this.name = name;
    this.active = active;
    this.bank = bank;
    this.mid = mid;
  }

  public String getBank() {
    return bank;
  }

  public void setBank(String bank) {
    this.bank = bank;
  }

  public String getMid() {
    return mid;
  }

  public void setMid(String mid) {
    this.mid = mid;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
