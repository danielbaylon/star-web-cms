package com.enovax.star.cms.skydining.model;

import java.io.Serializable;

public class ReservationFilterCriteria implements Serializable {

	private String transactionStartDate;
	private String transactionEndDate;
	private String transactionStatus;
	private String receiptNumber;
	private String packageName;
	private String dateOfVisit;
	private Integer reservationType;
	private String guestName;
	private Integer reservationStatus;

	private Integer start;
	private Integer pageSize;
	private String orderBy;
	private Boolean asc;

	private String visitStartDate;
	private String visitEndDate;

	private Boolean ignoreCancelled;
	private Boolean ignoreUnpaid;

	public Boolean getAsc() {
		return asc;
	}

	public String getDateOfVisit() {
		return dateOfVisit;
	}

	public String getGuestName() {
		return guestName;
	}

	public Boolean getIgnoreCancelled() {
		return ignoreCancelled;
	}

	public Boolean getIgnoreUnpaid() {
		return ignoreUnpaid;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public String getPackageName() {
		return packageName;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public Integer getReservationStatus() {
		return reservationStatus;
	}

	public Integer getReservationType() {
		return reservationType;
	}

	public Integer getStart() {
		return start;
	}

	public String getTransactionEndDate() {
		return transactionEndDate;
	}

	public String getTransactionStartDate() {
		return transactionStartDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public String getVisitEndDate() {
		return visitEndDate;
	}

	public String getVisitStartDate() {
		return visitStartDate;
	}

	public void setAsc(Boolean asc) {
		this.asc = asc;
	}

	public void setDateOfVisit(String dateOfVisit) {
		this.dateOfVisit = dateOfVisit;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public void setIgnoreCancelled(Boolean ignoreCancelled) {
		this.ignoreCancelled = ignoreCancelled;
	}

	public void setIgnoreUnpaid(Boolean ignoreUnpaid) {
		this.ignoreUnpaid = ignoreUnpaid;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public void setReservationStatus(Integer reservationStatus) {
		this.reservationStatus = reservationStatus;
	}

	public void setReservationType(Integer reservationType) {
		this.reservationType = reservationType;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public void setTransactionEndDate(String transactionEndDate) {
		this.transactionEndDate = transactionEndDate;
	}

	public void setTransactionStartDate(String transactionStartDate) {
		this.transactionStartDate = transactionStartDate;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public void setVisitEndDate(String visitEndDate) {
		this.visitEndDate = visitEndDate;
	}

	public void setVisitStartDate(String visitStartDate) {
		this.visitStartDate = visitStartDate;
	}

}
