package com.enovax.star.cms.skydining.model.result;


import com.enovax.star.cms.skydining.datamodel.SkyDiningTransaction;

public class SkyDiningCheckoutResult {
  public enum Result {
    Success, Failed, CustomerDetailsFailed, FailedGetBookingFee, FailedReceiptSequence, FailedReserveTicket, FailedInsufficientTickets
  }

  public final SkyDiningTransaction transaction;
  public final Result result;

  public SkyDiningCheckoutResult(Result result) {
    this(result, null);
  }

  public SkyDiningCheckoutResult(Result result, SkyDiningTransaction transaction) {
    this.result = result;
    this.transaction = transaction;
  }
}
