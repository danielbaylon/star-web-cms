package com.enovax.star.cms.skydining.model;


import java.util.Date;

public class SaveResult {

  public enum Result {
    Success, Failed, StaleData, ForeignKey
  }

  private final Result result;
  private Date timestamp;
  private int newId;
  private String message;
  private String imageHash;
  private String timestampText;

  public SaveResult(Result result) {
    this.result = result;
  }

  public SaveResult(Result result, Date timestamp) {
    this.result = result;
    this.timestamp = timestamp;
  }

  public SaveResult(Result result, Date timestamp, int newId) {
    this.result = result;
    this.timestamp = timestamp;
    this.newId = newId;
  }

  public SaveResult(Result result, Date timestamp, int newId, String imageHash) {
    this.result = result;
    this.timestamp = timestamp;
    this.newId = newId;
    this.imageHash = imageHash;
  }

  public SaveResult(Result result, Date timestamp, String imageHash) {
    this.result = result;
    this.timestamp = timestamp;
    this.imageHash = imageHash;
  }

  public SaveResult(Result result, String message) {
    this.result = result;
    this.message = message;
  }

  public String getImageHash() {
    return imageHash;
  }

  public String getMessage() {
    return message;
  }

  public int getNewId() {
    return newId;
  }

  public Result getResult() {
    return result;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setImageHash(String imageHash) {
    this.imageHash = imageHash;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setNewId(int newId) {
    this.newId = newId;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public String getTimestampText() {
    return timestampText;
  }

  public void setTimestampText(String timestampText) {
    this.timestampText = timestampText;
  }
}

