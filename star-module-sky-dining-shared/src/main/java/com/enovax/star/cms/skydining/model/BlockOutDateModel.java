package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningBlockOutDate;

import java.util.Date;

public class BlockOutDateModel extends SkyDiningBlockOutDate {

  private String status;
  private String timestamp;
  private String startDateText;
  private String endDateText;
  private String name;

  public BlockOutDateModel(SkyDiningBlockOutDate dataModel) {
    setId(dataModel.getId());
    setStartDate(dataModel.getStartDate());
    setEndDate(dataModel.getEndDate());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());

    final Date currentDate = new Date();
    if (currentDate.after(getStartDate())
      && currentDate.before(getEndDate())) {
      status = "Active";
    } else {
      status = "Inactive";
    }

    startDateText = NvxDateUtils.formatDate(getStartDate(),
      SysConst.DEFAULT_DATE_FORMAT);
    endDateText = NvxDateUtils.formatDate(getEndDate(),
      SysConst.DEFAULT_DATE_FORMAT);
    timestamp = NvxDateUtils.formatModDt(getModifiedDate());
    name = startDateText + " to " + endDateText;
  }

  public String getEndDateText() {
    return endDateText;
  }

  public String getName() {
    return name;
  }

  public String getStartDateText() {
    return startDateText;
  }

  public String getStatus() {
    return status;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setEndDateText(String endDateText) {
    this.endDateText = endDateText;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStartDateText(String startDateText) {
    this.startDateText = startDateText;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

}