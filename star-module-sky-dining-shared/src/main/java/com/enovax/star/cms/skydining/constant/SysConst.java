package com.enovax.star.cms.skydining.constant;

public class SysConst {

  public static final String SYSTEM = "system";

  public static final String DEFAULT_MERCHANT_NAME = "Default";

  public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";

  public static final String DEFAULT_TIME_FORMAT = "hh:mm aa";

  public static final String DEFAULT_DATE_TIME_FORMAT = DEFAULT_DATE_FORMAT + " " + DEFAULT_TIME_FORMAT;

  public static final String DEFAULT_SCHEDULE_DISPLAY_FORMAT = "dd MMMM, yyyy";

  public static final String HTTP_PRE = "http://";
  public static final String HTTPS_PRE = "https://";

  public static final String ACT_HOME = "home";
  public static final String ACT_ANY = "any";
  public static final String ACT_RENEW_PW = "renewPassword";
  public static final String ACT_LOGIN_REDIR = "loginRedirect";

  public static final String RENEW_REQUIRED = "renewRequired";

  public static final String KEY_FILE_AD_ABS = "ad.absoluteDir";
  public static final String KEY_FILE_AD_WEB = "ad.webDir";

  public static final String KEY_FILE_NEWS_ABS = "news.absoluteDir";
  public static final String KEY_FILE_NEWS_WEB = "news.webDir";

  public static final String KEY_FILE_PROD_ABS = "prod.absoluteDir";
  public static final String KEY_FILE_PROD_WEB = "prod.webDir";
  public static final String KEY_FILE_PROD_DEFAULT = "prod.defaultImg";

  public static final String KEY_FILE_PDF_ABS = "pdf.absoluteDir";

  public static final String KEY_FILE_BARCODE_ABS = "barcode.absoluteDir";
  public static final String KEY_FILE_BARCODE_WEB = "barcode.webDir";

  public static final String KEY_CAT_AD_ABS = "category.absoluteDir";
  public static final String KEY_CAT_AD_WEB = "category.webDir";

  public static final String KEY_FILE_TRANS_TIMEOUT_REPORT = "transTimeout.absoluteDir";

  public static final String KEY_MSG_ERR_GENERAL = "general.err.GeneralFailure";
  public static final String KEY_MSG_ERR_INVALID_IMG = "general.err.InvalidImage";
  public static final String KEY_MSG_ERR_MAX_IMG_DIMENSIONS = "general.err.MaxImageDimensions";
  public static final String KEY_MSG_ERR_STALE_DATA = "general.err.StaleData";
  public static final String KEY_MSG_ERR_FOREIGN_KEY = "general.err.ForeignKey";

  public static final String TM_STATUS_SUCCESS = "YES";
  public static final String TM_STATUS_NOTFOUND = "NA";
  public static final String TM_STATUS_RDR = "RDR";

  public static final String TM_TRN_TYPE_VOID = "void";
  public static final String TM_TRN_TYPE_SALE = "sale";

  public static final String KEY_SYS_TRANS_TO_LOWER_BOUND_MINS = "sys.transTimeoutLowerBoundMins";
  public static final String KEY_SYS_TRANS_TO_UPPER_BOUND_MINS = "sys.transTimeoutUpperBoundMins";

  public static final String KEY_SYS_TRANS_EMPTY_TO_LOWER_BOUND_MINS = "sys.transEmptyLowerBoundMins";
  public static final String KEY_SYS_TRANS_EMPTY_TO_UPPER_BOUND_MINS = "sys.transEmptyUpperBoundMins";

  public static final String KEY_SYS_RETRY_INTERVAL_MILLIS = "sys.retryIntervalMillis";
  public static final String KEY_SYS_RETRY_COUNT = "sys.retryCount";
  public static final String KEY_TM_QUERY_URL = "tm.queryUrl";

  public static final String KEY_SERVER_NAME = "serverName";
  public static final String KEY_SERVER_URL = "serverUrl";

  public static final String KEY_SACT_URL = "sact.ws.url";
  public static final String KEY_SACT_AUTHKEY = "sact.ws.authKey";
  public static final String KEY_SACT_TIMEOUT_MILLIS = "sact.ws.timeoutMillis";
  public static final String KEY_SACT_RETRY_MILLIS = "sact.ws.retryMillis";
  public static final String KEY_SACT_TRY_COUNT = "sact.ws.tryCount";
}

