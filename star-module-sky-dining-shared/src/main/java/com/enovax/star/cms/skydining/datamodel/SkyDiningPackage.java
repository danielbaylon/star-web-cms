package com.enovax.star.cms.skydining.datamodel;

import com.enovax.util.jcr.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
@JcrContent(nodeType = "mgnl:sky-dining-package")
public class SkyDiningPackage implements HasTimestamp {
  @JcrName
  @JcrSequence(workspace = "cms-config", path = "sequences/sky-dining-packages", propertyName = "package-id", nodeType = "mgnl:cms-config")
  private int id;
  @JcrProperty
  private String name;
  @JcrProperty
  private Date earlyBirdEndDate;
  @JcrProperty
  private Date startDate;
  @JcrProperty
  private Date endDate;
  @JcrProperty
  private Integer salesCutOffDays;
  @JcrProperty
  private Integer capacity;
  @JcrProperty
  private String description;
  @JcrProperty
  private String link;
  @JcrProperty
  private Integer minPax;
  @JcrProperty
  private Integer maxPax;
  @JcrProperty
  private String imageHash;
  @JcrProperty
  private String notes;
  @JcrCreatedBy
  private String createdBy;
  @JcrCreated
  private Date createdDate;
  @JcrLastModifiedBy
  private String modifiedBy;
  @JcrLastModified
  private Date modifiedDate;

  @JcrProperty
  private Integer displayOrder;

  @JcrContent(subNodeType = "mgnl:sky-dining-tnc")
  private List<SkyDiningTnc> tncs = new ArrayList<>(0);
  @JcrContent(subNodeType = "mgnl:sky-dining-theme")
  private List<SkyDiningTheme> themes = new ArrayList<>(0);
  @JcrLink(linkedPath = "menus", linkedNodeType = "mgnl:sky-dining-menu", linkedWorkspace = "sky-dining-admin")
  private List<SkyDiningMenu> menus = new ArrayList<>(0);
  @JcrLink(linkedPath = "top-ups", linkedNodeType = "mgnl:sky-dining-top-up", linkedWorkspace = "sky-dining-admin")
  private List<SkyDiningTopUp> topUps = new ArrayList<>(0);
  @JcrLink(linkedPath = "sessions", linkedNodeType = "mgnl:sky-dining-session", linkedWorkspace = "sky-dining-admin")
  private List<SkyDiningSession> sessions = new ArrayList<>(0);
  @JcrContent(subNodeType = "mgnl:sky-dining-discount")
  private List<SkyDiningDiscount> discounts = new ArrayList<>(
    0);

  @JcrProperty
  private Integer selectedTncId;

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getEarlyBirdEndDate() {
    return earlyBirdEndDate;
  }

  public void setEarlyBirdEndDate(Date earlyBirdEndDate) {
    this.earlyBirdEndDate = earlyBirdEndDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Integer getSalesCutOffDays() {
    return salesCutOffDays;
  }

  public void setSalesCutOffDays(Integer salesCutOffDays) {
    this.salesCutOffDays = salesCutOffDays;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public Integer getMinPax() {
    return minPax;
  }

  public void setMinPax(Integer minPax) {
    this.minPax = minPax;
  }

  public Integer getMaxPax() {
    return maxPax;
  }

  public void setMaxPax(Integer maxPax) {
    this.maxPax = maxPax;
  }

  public String getImageHash() {
    return imageHash;
  }

  public void setImageHash(String imageHash) {
    this.imageHash = imageHash;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Integer getDisplayOrder() {
    return displayOrder;
  }

  public void setDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
  }

  public List<SkyDiningTnc> getTncs() {
    return tncs;
  }

  public void setTncs(List<SkyDiningTnc> tncs) {
    this.tncs = tncs;
  }

  public List<SkyDiningTheme> getThemes() {
    return themes;
  }

  public void setThemes(List<SkyDiningTheme> themes) {
    this.themes = themes;
  }

  public List<SkyDiningMenu> getMenus() {
    return menus;
  }

  public void setMenus(List<SkyDiningMenu> menus) {
    this.menus = menus;
  }

  public List<SkyDiningTopUp> getTopUps() {
    return topUps;
  }

  public void setTopUps(List<SkyDiningTopUp> topUps) {
    this.topUps = topUps;
  }

  public List<SkyDiningSession> getSessions() {
    return sessions;
  }

  public void setSessions(List<SkyDiningSession> sessions) {
    this.sessions = sessions;
  }

  public List<SkyDiningDiscount> getDiscounts() {
    return discounts;
  }

  public void setDiscounts(List<SkyDiningDiscount> discounts) {
    this.discounts = discounts;
  }

  public Integer getSelectedTncId() {
    return selectedTncId;
  }

  public void setSelectedTncId(Integer selectedTncId) {
    this.selectedTncId = selectedTncId;
  }
}
