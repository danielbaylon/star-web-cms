package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;

public class PackageModel extends SkyDiningPackage {

  private String earlyBirdEndDateText;
  private String startDateText;
  private String endDateText;
  private String timestamp;
  private Integer extraPax;
  private String tncContent;

  public PackageModel(SkyDiningPackage dataModel) {
    setId(dataModel.getId());
    setName(dataModel.getName());
    setEarlyBirdEndDate(dataModel.getEarlyBirdEndDate());
    setStartDate(dataModel.getStartDate());
    setEndDate(dataModel.getEndDate());
    setSalesCutOffDays(dataModel.getSalesCutOffDays());
    setCapacity(dataModel.getCapacity());
    setDescription(dataModel.getDescription());
    setLink(dataModel.getLink());
    setMinPax(dataModel.getMinPax());
    setMaxPax(dataModel.getMaxPax());
    setImageHash(dataModel.getImageHash());
    setNotes(dataModel.getNotes());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());

    if (getStartDate() != null) {
      startDateText = NvxDateUtils.formatDate(getStartDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEndDate() != null) {
      endDateText = NvxDateUtils.formatDate(getEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEarlyBirdEndDate() != null) {
      earlyBirdEndDateText = NvxDateUtils.formatDate(getEarlyBirdEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getModifiedDate() != null) {
      timestamp = NvxDateUtils.formatModDt(getModifiedDate());
    }
    if (getMinPax() != null) {
      if (getMaxPax() == null) {
        extraPax = 0;
      } else {
        extraPax = getMaxPax() - getMinPax();
      }
    }

    setDisplayOrder(dataModel.getDisplayOrder());
  }

  public String getEarlyBirdEndDateText() {
    return earlyBirdEndDateText;
  }

  public String getEndDateText() {
    return endDateText;
  }

  public Integer getExtraPax() {
    return extraPax;
  }

  public String getStartDateText() {
    return startDateText;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getTncContent() {
    return tncContent;
  }

  public void setEarlyBirdEndDateText(String earlyBirdEndDateText) {
    this.earlyBirdEndDateText = earlyBirdEndDateText;
  }

  public void setEndDateText(String endDateText) {
    this.endDateText = endDateText;
  }

  public void setExtraPax(Integer extraPax) {
    this.extraPax = extraPax;
  }

  public void setStartDateText(String startDateText) {
    this.startDateText = startDateText;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public void setTncContent(String tncContent) {
    this.tncContent = tncContent;
  }

}
