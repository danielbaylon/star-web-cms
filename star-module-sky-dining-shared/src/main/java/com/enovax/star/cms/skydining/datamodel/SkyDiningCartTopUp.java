package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(catalog = "STAR_DB", name = "SkyDiningCartTopUp", schema = "dbo")
public class SkyDiningCartTopUp implements Serializable {

  private int id;
  private int quantity;

  private Integer topUpId;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false)
  public int getId() {
    return id;
  }

  @Column(name = "quantity", nullable = false)
  public int getQuantity() {
    return quantity;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Column(name = "topUpId", nullable = false)
  public Integer getTopUpId() {
    return topUpId;
  }

  public void setTopUpId(Integer topUpId) {
    this.topUpId = topUpId;
  }
}
