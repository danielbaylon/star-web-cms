package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPromoCode;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;

/**
 * Created by jace on 29/7/16.
 */
@Repository
public class DefaultSkyDiningDiscountRepository extends BaseJcrRepository<SkyDiningDiscount> implements ISkyDiningDiscountRepository {

    public static final String JCR_WORKSPACE = "sky-dining-admin";
    public static final String JCR_PATH = null;
    public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-discount";

    @Override
    protected String getJcrWorkspace() {
        return JCR_WORKSPACE;
    }

    @Override
    protected String getJcrPath() {
        return JCR_PATH;
    }

    @Override
    protected SkyDiningDiscount getNewInstance() {
        return new SkyDiningDiscount();
    }

    @Override
    protected String getPrimaryNodeType() {
        return PRIMARY_NODE_TYPE;
    }

    @Override
    public SkyDiningDiscount getParentDiscount(SkyDiningPromoCode pc) {
        try {
            Node pcNode = findByPrimaryNodeType(getJcrWorkspace(), Integer.toString(pc.getId()), "mgnl:sky-dining-promo-code");
            Node dcNode = pcNode.getParent().getParent();
            SkyDiningDiscount dc = getNewInstance();
            fillBeanFromNode(dc, dcNode);
            return dc;
        } catch (Exception e) {
            log.error("Error retrieving promo code parent node.", e);
        }
        return null;
    }
}
