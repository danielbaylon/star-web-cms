package com.enovax.star.cms.skydining.model;

import java.io.Serializable;

/**
 * Created by jace on 25/8/16.
 */
public class TransactionStatus implements Serializable {
  public TransactionStatus(String name, String code) {
    this.name = name;
    this.code = code;
  }

  public TransactionStatus() {
  }

  private String name;
  private String code;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

}