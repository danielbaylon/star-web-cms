package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPackage;
import com.enovax.star.cms.skydining.util.dao.RepoUtil;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.query.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jace on 20/7/16.
 */
@Repository
public class DefaultSkyDiningPackageRepository extends BaseJcrRepository<SkyDiningPackage> implements ISkyDiningPackageRepository {

    public static final String JCR_WORKSPACE = "sky-dining-admin";
    public static final String JCR_PATH = "packages";
    public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-package";

    @Override
    protected String getJcrWorkspace() {
        return JCR_WORKSPACE;
    }

    @Override
    protected String getJcrPath() {
        return JCR_PATH;
    }

    @Override
    protected SkyDiningPackage getNewInstance() {
        return new SkyDiningPackage();
    }

    @Override
    protected String getPrimaryNodeType() {
        return PRIMARY_NODE_TYPE;
    }

    protected static final String SQL_FIND_PAGED = "select * from [mgnl:sky-dining-package] as p "
        + RepoUtil.FILTER_CLAUSE + RepoUtil.ORDER_CLAUSE;

    @Override
    public int count(String name, Boolean active, Integer sessionId, Date dateOfBooking, Date dateOfVisit) {
        final String query = getSQLFindPaged(name, active, sessionId, dateOfBooking, dateOfVisit, null, false);
        final Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(name)) {
            params.put("name", "%" + name + "%");
        }
        if (active != null) {
            params.put("currentDate", new Date());
        }
        List<SkyDiningPackage> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, params, null, null);
        if ((sessionId != null) && (sessionId > 0)) {
            result = filterNotLinkedTo("sky-dining-admin", "sessions", String.valueOf(sessionId), "mgnl:sky-dining-session", "packages", result);
        }

        return result.size();
    }

    @Override
    public List<SkyDiningPackage> findPaged(String name, Boolean active, Integer sessionId, Date dateOfBooking, Date dateOfVisit, int start, int pageSize, String orderBy, boolean asc) {
        final String query = getSQLFindPaged(name, active, sessionId, dateOfBooking, dateOfVisit, orderBy, asc);
        final Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(name)) {
            params.put("name", "%" + name + "%");
        }
        if (active != null) {
            params.put("currentDate", new Date());
        }
        List<SkyDiningPackage> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, params, pageSize, start);
        if ((sessionId != null) && (sessionId > 0)) {
            result = filterNotLinkedTo("sky-dining-admin", "sessions", String.valueOf(sessionId), "mgnl:sky-dining-session", "packages", result);
        }

        return result;
    }

    protected String getSQLFindPaged(String name, Boolean active,
                                     Integer sessionId, Date dateOfBooking, Date dateOfVisit,
                                     String orderBy, boolean asc) {
        String query = SQL_FIND_PAGED;
        String filterClause = "";
        String orderClause = "";

        if (StringUtils.isNotEmpty(name)) {
            filterClause += "and p.name like $name ";
        }

        if (active != null) {
            if (active) {
                filterClause += "and p.endDate >= $currentDate and p.startDate <= $currentDate ";
            } else {
                filterClause += "and (p.endDate < $currentDate or p.startDate > $currentDate) ";
            }
        }

        if (filterClause.startsWith("and")) {
            filterClause = filterClause.substring(3);
        }
        if (StringUtils.isNotBlank(filterClause)) {
            filterClause = "where" + filterClause;
        }

//    if (sessionId != null) {
//      if (sessionId > 0) {
//        filterClause += "and p.id not in (select pm.packageId from SkyDiningPackageMapping pm where pm.sessionId = :sessionId) ";
//      }
//    }
//
//    if (dateOfVisit != null) {
//      filterClause += "and p.id in ("
//        + getSQLFindAvailableSessions(dateOfBooking, dateOfVisit)
//        + ") ";
//    }

        if (StringUtils.isNotEmpty(orderBy)) {
            orderClause += "order by p." + orderBy + " ";
            if (asc) {
                orderClause += "asc";
            } else {
                orderClause += "desc";
            }
        }

        query = query.replace(RepoUtil.FILTER_CLAUSE, filterClause);
        query = query.replace(RepoUtil.ORDER_CLAUSE, orderClause);

        return query;
    }

    @Override
    public SkyDiningPackage getParentPackage(SkyDiningDiscount dc) {
        try {
            Node dcNode = findByPrimaryNodeType(getJcrWorkspace(), Integer.toString(dc.getId()), "mgnl:sky-dining-discount");
            Node pNode = dcNode.getParent().getParent();
            SkyDiningPackage p = getNewInstance();
            fillBeanFromNode(p, pNode);
            return p;
        } catch (Exception e) {
            log.error("Error retrieving promo code parent node.", e);
        }
        return null;
    }

    @Override
    public ContentMap getPackageTnc(String packageId) {
        try {
            Node packageNode = findByPrimaryNodeType(getJcrWorkspace(), packageId, getPrimaryNodeType());
            Long selectedTncId = PropertyUtil.getLong(packageNode, "selectedTncId", 0L);
            if (selectedTncId > 0L && packageNode.hasNode("tncs")) {
                Node tncsNode = packageNode.getNode("tncs");
                if (tncsNode.hasNode(selectedTncId.toString())) {
                    return new ContentMap(tncsNode.getNode(selectedTncId.toString()));
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving package tnc.", e);
        }

        return null;
    }
}
