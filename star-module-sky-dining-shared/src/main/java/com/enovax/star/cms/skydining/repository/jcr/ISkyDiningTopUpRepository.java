package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;
import com.enovax.util.jcr.repository.JcrRepository;

import java.util.List;

/**
 * Created by jace on 28/7/16.
 */
public interface ISkyDiningTopUpRepository extends JcrRepository<SkyDiningTopUp> {

  int count(String name, Boolean active, Integer packageId);

  List<SkyDiningTopUp> findPaged(String name, Boolean active,
                                 Integer packageId, int start, int pageSize, String orderBy,
                                 boolean asc);
}
