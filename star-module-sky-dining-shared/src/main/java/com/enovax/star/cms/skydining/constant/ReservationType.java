package com.enovax.star.cms.skydining.constant;

public enum ReservationType {
  Online(0), Admin(1);

  public final int value;

  ReservationType(int value) {
    this.value = value;
  }
}
