package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.skydining.model.*;

import java.io.File;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningPackageService {
  SaveResult addMenusToDiscount(int parentId, List<Integer> ids,
                                String userId, String timestampText);

  SaveResult addMenusToPackage(int parentId, List<Integer> ids,
                               String userId, String timestampText);

  SaveResult addTopUpsToPackage(int parentId, List<Integer> ids,
                                String userId, String timestampText);

  int countPackageRecords(String name, Boolean active);

  int countSelectDiscountMenuRecords(Integer discountId, String name,
                                     Boolean active, List<Integer> menuTypes);

  int countSelectMenuRecords(Integer packageId, String name, Boolean active,
                             List<Integer> menuTypes);

  int countSelectTopUpRecords(Integer packageId, String name, Boolean active);

  List<MenuRecord> getDiscountMenuRecords(int parentId);

  List<DiscountModel> getDiscounts(int parentId);

  List<MenuRecord> getMenuRecords(int parentId);

  PackageModel getPackage(int id);

  List<PackageRecord> getPackageRecords(String name, Boolean active,
                                        int start, int pageSize, String orderBy, boolean asc);

  List<PromoCodeModel> getPromoCodes(int parentId);

  List<MenuRecord> getSelectDiscountMenuRecords(Integer discountId,
                                                String name, Boolean active, List<Integer> menuTypes, int start,
                                                int pageSize, String orderBy, boolean asc);

  List<MenuRecord> getSelectMenuRecords(Integer packageId, String name,
                                        Boolean active, List<Integer> menuTypes, int start, int pageSize,
                                        String orderBy, boolean asc);

  List<TopUpRecord> getSelectTopUpRecords(Integer packageId, String name,
                                          Boolean active, int start, int pageSize, String orderBy, boolean asc);

  List<ThemeModel> getThemes(int parentId);

  List<TncModel> getTncs(int parentId);

  List<TopUpRecord> getTopUpRecords(int parentId);

  SaveResult removeDiscounts(int parentId, List<Integer> ids, String userId,
                             String timestampText);

  SaveResult removeMenusFromDiscount(int parentId, List<Integer> ids,
                                     String userId, String timestampText);

  SaveResult removeMenusFromPackage(int parentId, List<Integer> ids,
                                    String userId, String timestampText);

  SaveResult removePromoCodes(int parentId, List<Integer> ids, String userId,
                              String timestampText);

  SaveResult removeThemes(int parentId, List<Integer> ids, String userId,
                          String timestampText);

  SaveResult removeTncs(int parentId, List<Integer> ids, String userId,
                        String timestampText);

  SaveResult removeTopUpsFromPackage(int parentId, List<Integer> ids,
                                     String userId, String timestampText);

  SaveResult saveDiscount(int parentId, int id, String newName,
                          Boolean newActive, String newType, String newMerchantId,
                          String userId, String timestampText, Integer priceType,
                          Integer discountPrice, String validFrom, String validTo, String tnc);

  SaveResult saveDisplayImage(int id, File image, String fileName,
                              String userId, String timestampText);

  SaveResult savePackage(int id, String newName, String newEarlyBirdEndDate,
                         String newStartDate, String newEndDate, Integer newSalesCutOffDays,
                         Integer newCapacity, String newDescription, String newLink,
                         Integer newDisplayOrder, Integer newMinPax, Integer newMaxPax,
                         String newImageHash, String newNotes, Integer newSelectedTncId,
                         String userId, String timestampText);

  SaveResult savePromoCode(int parentId, int id, String newCode,
                           Boolean newHasLimit, Integer newLimit, Integer newTimesUsed,
                           Boolean newActive, String newType, String newStartDate,
                           String newEndDate, String userId, String timestampText);

  SaveResult saveTheme(int parentId, int id, File image, String fileName,
                       String newDescription, Integer newCapacity, String userId,
                       String timestampText);

  SaveResult saveTnc(int parentId, int id, String newTitle,
                     String newTncContent, Boolean newSelected, String userId,
                     String timestampText);

  List<MerchRow> doGetMerchants(boolean activeOnly);
}
