package com.enovax.star.cms.skydining.model.api;

import com.enovax.star.cms.skydining.model.result.SkyDiningBookingItem;

import java.util.List;

public class SkyDiningCheckPromoResult {
    public enum Result {
        Success, InvalidOrMaxed, OutOfVisitRange, OutOfBookingRange, Failed, InvalidJewelCard, Expired, NoRelatedItems, MoreThanOnePromo
    }

    public final Result result;
    public final List<SkyDiningBookingItem> tickets;
    public String redirectUrl;

    public SkyDiningCheckPromoResult(Result result,
                                     List<SkyDiningBookingItem> tickets) {
        this.result = result;
        this.tickets = tickets;
    }

    public Result getResult() {
        return result;
    }

    public List<SkyDiningBookingItem> getTickets() {
        return tickets;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
