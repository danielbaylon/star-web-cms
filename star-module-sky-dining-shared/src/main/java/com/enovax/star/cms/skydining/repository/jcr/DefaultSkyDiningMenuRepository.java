package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMenu;
import com.enovax.star.cms.skydining.util.dao.RepoUtil;
import com.enovax.util.jcr.exception.JcrException;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import info.magnolia.jcr.util.ContentMap;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jace on 28/7/16.
 */
@Repository
public class DefaultSkyDiningMenuRepository extends BaseJcrRepository<SkyDiningMenu> implements ISkyDiningMenuRepository {

    public static final String JCR_WORKSPACE = "sky-dining-admin";
    public static final String JCR_PATH = "menus";
    public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-menu";

    @Override
    protected String getJcrWorkspace() {
        return JCR_WORKSPACE;
    }

    @Override
    protected String getJcrPath() {
        return JCR_PATH;
    }

    @Override
    protected SkyDiningMenu getNewInstance() {
        return new SkyDiningMenu();
    }

    @Override
    protected String getPrimaryNodeType() {
        return PRIMARY_NODE_TYPE;
    }

    protected static final String SQL_FIND_PAGED = "select * from [mgnl:sky-dining-menu] as m "
        + RepoUtil.FILTER_CLAUSE + RepoUtil.ORDER_CLAUSE;

    @Override
    public int count(String name, Boolean active, Integer packageId, Integer discountId, List<Integer> menuTypes) {
        final String query = getSQLFindPaged(name, active, packageId,
            discountId, menuTypes, null, false);
        final Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(name)) {
            params.put("name", "%" + name + "%");
        }
        if (active != null) {
            params.put("currentDate", new Date());
        }

        List<SkyDiningMenu> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, params, null, null);

        if ((packageId != null) && (packageId > 0)) {
            result = filterNotLinkedTo("sky-dining-admin", "packages", String.valueOf(packageId), "mgnl:sky-dining-package", "menus", result);
        }
        if ((discountId != null) && (discountId > 0)) {
            result = filterNotLinkedTo("sky-dining-admin", null, String.valueOf(discountId), "mgnl:sky-dining-discount", "menus", result);
        }
        return result.size();
    }

    @Override
    public List<SkyDiningMenu> findPaged(String name, Boolean active, Integer packageId, Integer discountId, List<Integer> menuTypes, int start, int pageSize, String orderBy, boolean asc) {
        final String query = getSQLFindPaged(name, active, packageId,
            discountId, menuTypes, orderBy, asc);
        final Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(name)) {
            params.put("name", "%" + name + "%");
        }
        if (active != null) {
            params.put("currentDate", new Date());
        }

        List<SkyDiningMenu> result = runQuery(getJcrWorkspace(), query, Query.JCR_SQL2, params, pageSize, start);

        if ((packageId != null) && (packageId > 0)) {
            result = filterNotLinkedTo("sky-dining-admin", "packages", String.valueOf(packageId), "mgnl:sky-dining-package", "menus", result);
        }
        if ((discountId != null) && (discountId > 0)) {
            result = filterNotLinkedTo("sky-dining-admin", null, String.valueOf(discountId), "mgnl:sky-dining-discount", "menus", result);
        }
        return result;
    }

    protected String getSQLFindPaged(String name, Boolean active,
                                     Integer packageId, Integer discountId, List<Integer> menuTypes,
                                     String orderBy, boolean asc) {
        String query = SQL_FIND_PAGED;
        String filterClause = "";
        String orderClause = "";

        if (StringUtils.isNotEmpty(name)) {
            filterClause += "and m.name like $name ";
        }

        if (active != null) {
            if (active) {
                filterClause += "and m.endDate >= $currentDate and m.startDate <= $currentDate ";
            } else {
                filterClause += "and (m.endDate < $currentDate or m.startDate > $currentDate) ";
            }
        }

//    if (packageId != null) {
//      if (packageId > 0) {
//        filterClause += "and m.id not in (select mm.menuId from SkyDiningMenuMapping mm where mm.packageId = :packageId) ";
//      }
//    }
//
//    if (discountId != null) {
//      if (discountId > 0) {
//        filterClause += "and m.id not in (select mm.menuId from SkyDiningDiscountMapping mm where mm.discountId = :discountId) ";
//      }
//    }

//    Menu Type is Deprecated
//    if ((menuTypes != null) && !menuTypes.isEmpty()) {
//      filterClause += "and (";
//      StringBuilder sb = new StringBuilder();
//      for (Integer menuType : menuTypes) {
//        if (sb.length() > 0) {
//          sb.append("or ");
//        }
//        sb.append("m.menuType = ");
//        sb.append(menuType);
//      }
//      filterClause += sb.toString();
//      filterClause += ")";
//    }

        if (filterClause.startsWith("and")) {
            filterClause = filterClause.substring(3);
        }
        if (StringUtils.isNotBlank(filterClause)) {
            filterClause = "where" + filterClause;
        }

        if (StringUtils.isNotEmpty(orderBy)) {
            orderClause += "order by m." + orderBy + " ";
            if (asc) {
                orderClause += "asc";
            } else {
                orderClause += "desc";
            }
        }

        query = query.replace(RepoUtil.FILTER_CLAUSE, filterClause);
        query = query.replace(RepoUtil.ORDER_CLAUSE, orderClause);

        return query;
    }

    @Override
    public ContentMap findDiscountedContent(SkyDiningDiscount discount, int menuId) {
        try {
            Node menu = findByPrimaryNodeType(getJcrWorkspace(), Integer.toString(menuId), getPrimaryNodeType());
            if (StringUtils.isNotBlank(discount.getMerchantId())) {
                PropertyUtil.setProperty(menu, "merchantId", discount.getMerchantId());
            }
            Long price = PropertyUtil.getLong(menu, "price", new Long(0));
            PropertyUtil.setProperty(menu, "originalPrice", Long.toString(price));
            PropertyUtil.setProperty(menu, "usualPrice", NvxNumberUtils.formatToCurrency(NvxNumberUtils.centsToMoney(price)));
            PropertyUtil.setProperty(menu, "discountId", Integer.toString(discount.getId()));
            PropertyUtil.setProperty(menu, "discountItem", "true");
            PropertyUtil.setProperty(menu, "priceType", Integer.toString(discount.getPriceType()));
            PropertyUtil.setProperty(menu, "discountPrice", Integer.toString(discount.getDiscountPrice()));
            PropertyUtil.setProperty(menu, "subName", discount.getName());

            Long newPrice = getDiscountUnitPriceInCents(discount, price);
            PropertyUtil.setProperty(menu, "price", newPrice);

            return new ContentMap(menu);
        } catch (JcrException | RepositoryException e) {
            log.error("Unable to find discounted menu content.", e);
        }
        return null;
    }

    private Long getDiscountUnitPriceInCents(SkyDiningDiscount discount,
                                             Long unitPrice) {
        if ((discount.getPriceType() != null)
            && (discount.getDiscountPrice() != null)) {
            switch (discount.getPriceType()) {
                case 1:
                    unitPrice = NvxNumberUtils.percentOffInCents(unitPrice,
                        discount.getDiscountPrice());
                    break;
                case 2:
                    unitPrice = unitPrice - discount.getDiscountPrice();
                    break;
                case 3:
                    unitPrice = new Long(discount.getDiscountPrice());
                    break;
                default:
                    break;
            }
        }
        return unitPrice;
    }

}
