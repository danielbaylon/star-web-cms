package com.enovax.star.cms.skydining.repository.db;

import com.enovax.star.cms.skydining.datamodel.SkyDiningSequenceLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.stereotype.Repository;

import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by jace on 30/8/16.
 */
@Repository
public interface SkyDiningSequenceLogRepository extends JpaRepository<SkyDiningSequenceLog, Integer> {

  Long countByModifiedDate(@Temporal(TemporalType.DATE) Date modifiedDate);
}
