package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 25/7/16.
 */
public class SortModel {
  private String field; //orderBy
  private String dir; //direction

  public SortModel() {
  }

  public SortModel(String field, String dir) {
    this.field = field;
    this.dir = dir;
  }

  public String getDir() {
    return dir;
  }

  public void setDir(String dir) {
    this.dir = dir;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }
}

