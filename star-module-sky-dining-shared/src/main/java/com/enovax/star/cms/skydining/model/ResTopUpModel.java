package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningResTopUp;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;

/**
 * Created by jace on 29/7/16.
 */
public class ResTopUpModel extends SkyDiningResTopUp {

  private Integer topUpId;
  private String topUpName;
  private String topUpDescription;
  private String topUpPrice;
  private String topUpImageHash;
  private Integer topUpPriceInCents;

  public ResTopUpModel(SkyDiningResTopUp dataModel) {
    initDataModelValues(dataModel);
  }

  public ResTopUpModel(SkyDiningResTopUp dataModel, SkyDiningTopUp topUp) {
    initDataModelValues(dataModel);
    initTopUpValues(topUp);
  }

  public ResTopUpModel(SkyDiningTopUp topUp) {
    initTopUpValues(topUp);

    setId(0);
    setQuantity(0);
  }

  public String getTopUpDescription() {
    return topUpDescription;
  }

  public Integer getTopUpId() {
    return topUpId;
  }

  public String getTopUpImageHash() {
    return topUpImageHash;
  }

  public String getTopUpName() {
    return topUpName;
  }

  public String getTopUpPrice() {
    return topUpPrice;
  }

  public Integer getTopUpPriceInCents() {
    return topUpPriceInCents;
  }

  private void initDataModelValues(SkyDiningResTopUp dataModel) {
    setId(dataModel.getId());
    setQuantity(dataModel.getQuantity());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());
  }

  private void initTopUpValues(SkyDiningTopUp topUp) {
    topUpId = topUp.getId();
    topUpName = topUp.getName();
    topUpDescription = topUp.getDescription();

    if (topUp.getPrice() != null) {
      topUpPrice = NvxNumberUtils.formatToCurrency(
        NvxNumberUtils.centsToMoney(topUp.getPrice()), "$");
    }

    topUpImageHash = topUp.getImageHash();
    topUpPriceInCents = topUp.getPrice();
  }

  public void setTopUpDescription(String topUpDescription) {
    this.topUpDescription = topUpDescription;
  }

  public void setTopUpId(Integer topUpId) {
    this.topUpId = topUpId;
  }

  public void setTopUpImageHash(String topUpImageHash) {
    this.topUpImageHash = topUpImageHash;
  }

  public void setTopUpName(String topUpName) {
    this.topUpName = topUpName;
  }

  public void setTopUpPrice(String topUpPrice) {
    this.topUpPrice = topUpPrice;
  }

  public void setTopUpPriceInCents(Integer topUpPriceInCents) {
    this.topUpPriceInCents = topUpPriceInCents;
  }

}