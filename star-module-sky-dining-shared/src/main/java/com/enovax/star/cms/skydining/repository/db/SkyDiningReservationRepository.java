package com.enovax.star.cms.skydining.repository.db;

import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by jace on 24/8/16.
 */
@Repository
public interface SkyDiningReservationRepository extends JpaRepository<SkyDiningReservation, Integer> {

  Long countBySessionIdAndDate(Integer sessionId, Date date);

  Long countByThemeIdAndSessionIdAndDate(Integer themeId, Integer sessionId, Date date);

    @Query(value = "select count(*) from SkyDiningReservation r " +
        "left join r.transaction t where " +
        "(r.guestName like :guestName or :guestName is null) and " +
        "(r.date = :dateOfVisit or :dateOfVisit is null) and " +
        "(r.status = :reservationStatus or :reservationStatus is null) and " +
        "(r.type = :reservationType or :reservationType is null) and " +
        "(r.packageName like :packageName or :packageName is null) and " +
        "(r.createdDate >= :transactionStartDate or :transactionStartDate is null) and " +
        "(r.createdDate <= :transactionEndDate or :transactionEndDate is null) and " +
        "(t.tmStatus = :transactionStatus or :transactionStatus is null) and " +
        "(t.receiptNum like :receiptNumber or :receiptNumber is null)")
    Long countBySearchCriteria(@Param("guestName") String guestName,
                               @Param("dateOfVisit") Date dateOfVisit,
                               @Param("reservationStatus") Integer reservationStatus,
                               @Param("reservationType") Integer reservationType,
                               @Param("packageName") String packageName,
                               @Param("transactionStartDate") Date transactionStartDate,
                               @Param("transactionEndDate") Date transactionEndDate,
                               @Param("transactionStatus") String transactionStatus,
                               @Param("receiptNumber") String receiptNumber);

    @Query(value = "select r from SkyDiningReservation r " +
        "left join r.transaction t where " +
        "(r.guestName like :guestName or :guestName is null) and " +
        "(r.date = :dateOfVisit or :dateOfVisit is null) and " +
        "(r.status = :reservationStatus or :reservationStatus is null) and " +
        "(r.type = :reservationType or :reservationType is null) and " +
        "(r.packageName like :packageName or :packageName is null) and " +
        "(r.createdDate >= :transactionStartDate or :transactionStartDate is null) and " +
        "(r.createdDate <= :transactionEndDate or :transactionEndDate is null) and " +
        "(t.tmStatus = :transactionStatus or :transactionStatus is null) and " +
        "(t.receiptNum like :receiptNumber or :receiptNumber is null)")
    Page<SkyDiningReservation> findBySearchCriteria(@Param("guestName") String guestName,
                                                    @Param("dateOfVisit") Date dateOfVisit,
                                                    @Param("reservationStatus") Integer reservationStatus,
                                                    @Param("reservationType") Integer reservationType,
                                                    @Param("packageName") String packageName,
                                                    @Param("transactionStartDate") Date transactionStartDate,
                                                    @Param("transactionEndDate") Date transactionEndDate,
                                                    @Param("transactionStatus") String transactionStatus,
                                                    @Param("receiptNumber") String receiptNumber,
                                                    Pageable pageRequest);

    @Query(value = "select r from SkyDiningReservation r where " +
        "(r.date >= :visitStartDate or :visitStartDate is null) and " +
        "(r.date <= :visitEndDate or :visitEndDate is null) and " +
        "(r.status != 0 or :ignoreCancelled = false or :ignoreCancelled is null) and " +
        "(r.status != 2 or :ignoreUnpaid = false or :ignoreUnpaid is null)" +
        "order by r.createdDate asc")
    List<SkyDiningReservation> findForReports(@Param("visitStartDate") Date visitStartDate,
                                              @Param("visitEndDate") Date visitEndDate,
                                              @Param("ignoreCancelled") Boolean ignoreCancelled,
                                              @Param("ignoreUnpaid") Boolean ignoreUnpaid);
}
