package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(catalog = "STAR_DB", name = "SkyDiningCart", schema = "dbo")
public class SkyDiningCart implements Serializable {

    private int id;
    private String guestName;
    private String contactNumber;
    private String emailAddress;
    private Date date;
    private Time time;
    private Integer pax;
    private String specialRequest;
    private Integer type;
    private Integer status;
    private String webSessionId;
    private Date createdDate;
    private Date modifiedDate;
    private Integer discountId;
    private String jewelCard;
    private String jewelCardExpiry;
    private String promoCode;
    private String merchantId;
    private Integer priceType;
    private Integer discountPrice;
    private Integer originalPrice;

    private List<SkyDiningCartMainCourse> mainCourses = new ArrayList<>(0);
    private List<SkyDiningCartTopUp> topUps = new ArrayList<>(0);

    private Integer packageId;
    private Integer sessionId;
    private Integer menuId;
    private Integer themeId;

    @Column(name = "contactNumber", nullable = true, length = 100)
    public String getContactNumber() {
        return contactNumber;
    }

    @Column(name = "createdDate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() {
        return createdDate;
    }

    @Column(name = "date", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    @Column(name = "discountId", nullable = true)
    public Integer getDiscountId() {
        return discountId;
    }

    @Column(name = "discountPrice", nullable = true)
    public Integer getDiscountPrice() {
        return discountPrice;
    }

    @Column(name = "emailAddress", nullable = true, length = 200)
    public String getEmailAddress() {
        return emailAddress;
    }

    @Column(name = "guestName", nullable = true, length = 300)
    public String getGuestName() {
        return guestName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    @Column(name = "jewelCard", nullable = true, length = 100)
    public String getJewelCard() {
        return jewelCard;
    }

    @Column(name = "jewelCardExpiry", nullable = true, length = 50)
    public String getJewelCardExpiry() {
        return jewelCardExpiry;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cartId", referencedColumnName = "id", nullable = false, updatable = false)
    public List<SkyDiningCartMainCourse> getMainCourses() {
        return mainCourses;
    }

    @Column(name = "merchantId", nullable = true)
    public String getMerchantId() {
        return merchantId;
    }

    @Column(name = "modifiedDate", nullable = false)
    public Date getModifiedDate() {
        return modifiedDate;
    }

    @Column(name = "originalPrice", nullable = true)
    public Integer getOriginalPrice() {
        return originalPrice;
    }

    @Column(name = "pax", nullable = true)
    public Integer getPax() {
        return pax;
    }

    @Column(name = "priceType", nullable = true)
    public Integer getPriceType() {
        return priceType;
    }

    @Column(name = "promoCode", nullable = true, length = 100)
    public String getPromoCode() {
        return promoCode;
    }

    @Column(name = "specialRequest", nullable = true, length = 4000)
    public String getSpecialRequest() {
        return specialRequest;
    }

    @Column(name = "status", nullable = false)
    public Integer getStatus() {
        return status;
    }

    @Column(name = "time", nullable = true)
    public Time getTime() {
        return time;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cartId", referencedColumnName = "id", nullable = false, updatable = false)
    public List<SkyDiningCartTopUp> getTopUps() {
        return topUps;
    }

    @Column(name = "type", nullable = false)
    public Integer getType() {
        return type;
    }

    @Column(name = "webSessionId", nullable = false, length = 100)
    public String getWebSessionId() {
        return webSessionId;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public void setDiscountPrice(Integer discountPrice) {
        this.discountPrice = discountPrice;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setJewelCard(String jewelCard) {
        this.jewelCard = jewelCard;
    }

    public void setJewelCardExpiry(String jewelCardExpiry) {
        this.jewelCardExpiry = jewelCardExpiry;
    }

    public void setMainCourses(List<SkyDiningCartMainCourse> mainCourses) {
        this.mainCourses = mainCourses;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public void setTopUps(List<SkyDiningCartTopUp> topUps) {
        this.topUps = topUps;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setWebSessionId(String webSessionId) {
        this.webSessionId = webSessionId;
    }

    @Column(name = "packageId", nullable = false)
    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    @Column(name = "sessionId", nullable = false)
    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "menuId", nullable = false)
    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @Column(name = "themeId", nullable = true)
    public Integer getThemeId() {
        return themeId;
    }

    public void setThemeId(Integer themeId) {
        this.themeId = themeId;
    }
}
