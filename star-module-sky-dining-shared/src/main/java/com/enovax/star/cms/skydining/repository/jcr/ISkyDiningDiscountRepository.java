package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;
import com.enovax.star.cms.skydining.datamodel.SkyDiningPromoCode;
import com.enovax.util.jcr.repository.JcrRepository;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningDiscountRepository extends JcrRepository<SkyDiningDiscount> {

  SkyDiningDiscount getParentDiscount(SkyDiningPromoCode pc);
}
