package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningPromoCode;
import com.enovax.util.jcr.repository.JcrRepository;

import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningPromoCodeRepository extends JcrRepository<SkyDiningPromoCode> {
  SkyDiningPromoCode checkPromoCode(String promoCode, int packageId);

  List<SkyDiningPromoCode> findPaged(Integer discountId, Integer start,
                                     Integer pageSize);

  SkyDiningPromoCode updatePromoCodeUseCount(String codeStr, int packageId,
                                             int qtyToUpdate, boolean isAdd);
}
