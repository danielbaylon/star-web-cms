package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;

import java.io.Serializable;
import java.util.Date;

public class TopUpRecord implements Serializable {

  private int id;
  private String imageHash;
  private String name;
  private String description;
  private String price;
  private String status;
  private Integer priceInCents;
  private Integer displayOrder;

  public TopUpRecord(SkyDiningTopUp skyDiningTopUp) {
    id = skyDiningTopUp.getId();
    imageHash = skyDiningTopUp.getImageHash();
    name = skyDiningTopUp.getName();
    description = skyDiningTopUp.getDescription();
    priceInCents = skyDiningTopUp.getPrice();

    if (skyDiningTopUp.getPrice() != null) {
      price = NvxNumberUtils.formatToCurrency(
        NvxNumberUtils.centsToMoney(skyDiningTopUp.getPrice()), "$");
    }

    if ((skyDiningTopUp.getStartDate() != null)
      && (skyDiningTopUp.getEndDate() != null)) {
      final Date now = new Date();
      if (now.after(skyDiningTopUp.getStartDate())
        && now.before(skyDiningTopUp.getEndDate())) {
        status = "Active";
      } else {
        status = "Inactive";
      }
    } else {
      status = "Inactive";
    }

    displayOrder = skyDiningTopUp.getDisplayOrder();
  }

  public String getDescription() {
    return description;
  }

  public int getId() {
    return id;
  }

  public String getImageHash() {
    return imageHash;
  }

  public String getName() {
    return name;
  }

  public String getPrice() {
    return price;
  }

  public Integer getPriceInCents() {
    return priceInCents;
  }

  public String getStatus() {
    return status;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setImageHash(String imageHash) {
    this.imageHash = imageHash;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public void setPriceInCents(Integer priceInCents) {
    this.priceInCents = priceInCents;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Integer getDisplayOrder() {
    return displayOrder;
  }

  public void setDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
  }

}
