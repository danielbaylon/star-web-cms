package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMenu;

public class MenuModel extends SkyDiningMenu {

  private String earlyBirdEndDateText;
  private String startDateText;
  private String endDateText;
  private String timestamp;

  public MenuModel(SkyDiningMenu dataModel) {
    setId(dataModel.getId());
    setName(dataModel.getName());
    setDescription(dataModel.getDescription());
    setEarlyBirdEndDate(dataModel.getEarlyBirdEndDate());
    setStartDate(dataModel.getStartDate());
    setEndDate(dataModel.getEndDate());
    setSalesCutOffDays(dataModel.getSalesCutOffDays());
    setMenuType(dataModel.getMenuType());
    setPrice(dataModel.getPrice());
    setServiceCharge(dataModel.getServiceCharge());
    setGst(dataModel.getGst());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());

    if (getStartDate() != null) {
      startDateText = NvxDateUtils.formatDate(getStartDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEndDate() != null) {
      endDateText = NvxDateUtils.formatDate(getEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEarlyBirdEndDate() != null) {
      earlyBirdEndDateText = NvxDateUtils.formatDate(getEarlyBirdEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getModifiedDate() != null) {
      timestamp = NvxDateUtils.formatModDt(getModifiedDate());
    }
  }

  public String getEarlyBirdEndDateText() {
    return earlyBirdEndDateText;
  }

  public String getEndDateText() {
    return endDateText;
  }

  public String getStartDateText() {
    return startDateText;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setEarlyBirdEndDateText(String earlyBirdEndDateText) {
    this.earlyBirdEndDateText = earlyBirdEndDateText;
  }

  public void setEndDateText(String endDateText) {
    this.endDateText = endDateText;
  }

  public void setStartDateText(String startDateText) {
    this.startDateText = startDateText;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

}
