package com.enovax.star.cms.skydining.constant;

public enum PromoCodeType {
    VisitBased,
    BookingBased,
    EventBased
}
