package com.enovax.star.cms.skydining.constant;

public enum Stage {
  Reserved,
  RedirectedToTm,
  TmStatusReceived,
  SuccessConfirmedSale,
  SuccessFinalized,
  TmVoidSent,
  TmQuerySent,
  Released,
  Incomplete,
  Redeemed,
  Invalidated
}
