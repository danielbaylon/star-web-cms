package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SkyDiningDiscountType;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;

/**
 * Created by jace on 21/7/16.
 */
public class DiscountModel extends SkyDiningDiscount {

  private String typeText;
  private String activeText;
  private String timestamp;

  private String validFromText;
  private String validToText;

  public DiscountModel(SkyDiningDiscount dataModel) {
    setId(dataModel.getId());
    setName(dataModel.getName());
    setActive(dataModel.getActive());
    setType(dataModel.getType());
    setMerchantId(dataModel.getMerchantId());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());
    setPriceType(dataModel.getPriceType());
    setDiscountPrice(dataModel.getDiscountPrice());
    setValidFrom(dataModel.getValidFrom());
    setValidTo(dataModel.getValidTo());
    setTnc(dataModel.getTnc());

    if (getModifiedDate() != null) {
      timestamp = NvxDateUtils.formatModDt(getModifiedDate());
    }
    if (getType() != null) {
      setTypeText(SkyDiningDiscountType.valueOf(getType()).name);
    }
    if (getActive() != null) {
      if (getActive()) {
        setActiveText("Active");
      } else {
        setActiveText("Inactive");
      }
    }

    if (getValidFrom() != null) {
      validFromText = NvxDateUtils.formatDate(getValidFrom(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getValidTo() != null) {
      validToText = NvxDateUtils.formatDate(getValidTo(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
  }

  public String getActiveText() {
    return activeText;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getTypeText() {
    return typeText;
  }

  public void setActiveText(String activeText) {
    this.activeText = activeText;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public void setTypeText(String typeText) {
    this.typeText = typeText;
  }

  public String getValidFromText() {
    return validFromText;
  }

  public void setValidFromText(String validFromText) {
    this.validFromText = validFromText;
  }

  public String getValidToText() {
    return validToText;
  }

  public void setValidToText(String validToText) {
    this.validToText = validToText;
  }

}
