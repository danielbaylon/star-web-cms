package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningReservation;

import java.util.List;


public class ReservationModel extends SkyDiningReservation {

    private String dateText;
    private String timeText;
    private String paymentDateText;
    private String timestamp;

    private List<Integer> mainCourseIds;
    private List<Integer> mainCourseQuantities;

    private List<Integer> topUpIds;
    private List<Integer> topUpQuantities;

    private String sessionName;
    private String sessionTime;
    private String menuName;
    private Integer menuPrice;
    private Integer menuGst;
    private Integer menuServiceCharge;
    private String themeDescription;

    private String discountName;

    public ReservationModel() {
    }

    public ReservationModel(SkyDiningReservation dataModel) {
        setId(dataModel.getId());
        setGuestName(dataModel.getGuestName());
        setContactNumber(dataModel.getContactNumber());
        setEmailAddress(dataModel.getEmailAddress());
        setDate(dataModel.getDate());
        setTime(dataModel.getTime());
        setPax(dataModel.getPax());
        setSpecialRequest(dataModel.getSpecialRequest());
        setType(dataModel.getType());
        setStatus(dataModel.getStatus());
        setCreatedBy(dataModel.getCreatedBy());
        setCreatedDate(dataModel.getCreatedDate());
        setModifiedBy(dataModel.getModifiedBy());
        setModifiedDate(dataModel.getModifiedDate());
        setDiscountId(dataModel.getDiscountId());
        setJewelCard(dataModel.getJewelCard());
        setJewelCardExpiry(dataModel.getJewelCardExpiry());
        setPromoCode(dataModel.getPromoCode());
        setMerchantId(dataModel.getMerchantId());
        setReceiptNum(dataModel.getReceiptNum());
        setReceivedBy(dataModel.getReceivedBy());
        setPaymentDate(dataModel.getPaymentDate());
        setPaymentMode(dataModel.getPaymentMode());
        setPaidAmount(dataModel.getPaidAmount());
        setPriceType(dataModel.getPriceType());
        setDiscountPrice(dataModel.getDiscountPrice());
        setOriginalPrice(dataModel.getOriginalPrice());

        setPackageId(dataModel.getPackageId());
        setSessionId(dataModel.getSessionId());
        setMenuId(dataModel.getMenuId());
        setThemeId(dataModel.getThemeId());
        setPackageName(dataModel.getPackageName());

        if (getDate() != null) {
            dateText = NvxDateUtils.formatDate(getDate(),
                SysConst.DEFAULT_DATE_FORMAT);
        }
        if (getTime() != null) {
            timeText = NvxDateUtils.formatTime(getTime(),
                SysConst.DEFAULT_TIME_FORMAT);
        }
        if (getPaymentDate() != null) {
            paymentDateText = NvxDateUtils.formatDate(getPaymentDate(),
                SysConst.DEFAULT_DATE_FORMAT);
        }
        if (getModifiedDate() != null) {
            timestamp = NvxDateUtils.formatModDt(getModifiedDate());
        }
    }

    public String getDateText() {
        return dateText;
    }

    public String getDiscountName() {
        return discountName;
    }

    public List<Integer> getMainCourseIds() {
        return mainCourseIds;
    }

    public List<Integer> getMainCourseQuantities() {
        return mainCourseQuantities;
    }

    public Integer getMenuGst() {
        return menuGst;
    }

    public String getMenuName() {
        return menuName;
    }

    public Integer getMenuPrice() {
        return menuPrice;
    }

    public Integer getMenuServiceCharge() {
        return menuServiceCharge;
    }

    public String getPaymentDateText() {
        return paymentDateText;
    }

    public String getSessionName() {
        return sessionName;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public String getThemeDescription() {
        return themeDescription;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTimeText() {
        return timeText;
    }

    public List<Integer> getTopUpIds() {
        return topUpIds;
    }

    public List<Integer> getTopUpQuantities() {
        return topUpQuantities;
    }

    public void setDateText(String dateText) {
        this.dateText = dateText;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public void setMainCourseIds(List<Integer> mainCourseIds) {
        this.mainCourseIds = mainCourseIds;
    }

    public void setMainCourseQuantities(List<Integer> mainCourseQuantities) {
        this.mainCourseQuantities = mainCourseQuantities;
    }

    public void setMenuGst(Integer menuGst) {
        this.menuGst = menuGst;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public void setMenuPrice(Integer menuPrice) {
        this.menuPrice = menuPrice;
    }

    public void setMenuServiceCharge(Integer menuServiceCharge) {
        this.menuServiceCharge = menuServiceCharge;
    }

    public void setPaymentDateText(String paymentDateText) {
        this.paymentDateText = paymentDateText;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public void setThemeDescription(String themeDescription) {
        this.themeDescription = themeDescription;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setTimeText(String timeText) {
        this.timeText = timeText;
    }

    public void setTopUpIds(List<Integer> topUpIds) {
        this.topUpIds = topUpIds;
    }

    public void setTopUpQuantities(List<Integer> topUpQuantities) {
        this.topUpQuantities = topUpQuantities;
    }

}
