package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.constant.TNCType;
import com.enovax.star.cms.commons.model.booking.CustomerDetails;
import com.enovax.star.cms.commons.model.booking.skydining.*;
import com.enovax.star.cms.commons.model.system.SysEmailTemplateVM;
import com.enovax.star.cms.commons.model.tnc.TncVM;
import com.enovax.star.cms.commons.service.email.IMailService;
import com.enovax.star.cms.commons.service.email.MailCallback;
import com.enovax.star.cms.commons.service.email.MailProperties;
import com.enovax.star.cms.commons.util.FileUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.*;
import com.enovax.star.cms.skydining.model.*;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jace on 24/8/16.
 */
@Service
public class DefaultSkyDiningReservationService extends BaseSkyDiningAdminService implements ISkyDiningReservationService {

    @Autowired
    private ISharedConfigService configService;
    @Autowired
    private ISkyDiningTemplateService templateService;
    @Autowired
    private IMailService mailSrv;
//    @Autowired
//    private IB2CSysParamService paramService;

    @Override
    @Transactional(readOnly = true)
    public int countReservationRecords(ReservationFilterCriteria criteria) {
        String guestName = null;
        Date dateOfVisit = null;
        String packageName = null;
        Date transactionStartDate = null;
        Date transactionEndDate = null;
        String transactionStatus = null;
        String receiptNumber = null;

        if (StringUtils.isNotBlank(criteria.getGuestName())) {
            guestName = formatForLike(criteria.getGuestName());
        }
        if (StringUtils.isNotBlank(criteria.getDateOfVisit())) {
            try {
                dateOfVisit = NvxDateUtils.parseDate(criteria.getDateOfVisit(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                log.error("Error parsing date: " + criteria.getDateOfVisit(), e);
            }
        }
        if (StringUtils.isNotBlank(criteria.getPackageName())) {
            packageName = formatForLike(criteria.getPackageName());
        }
        if (StringUtils.isNotBlank(criteria.getTransactionStartDate())) {
            try {
                transactionStartDate = NvxDateUtils.parseDate(criteria.getTransactionStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                log.error("Error parsing date: " + criteria.getTransactionStartDate(), e);
            }
        }
        if (StringUtils.isNotBlank(criteria.getTransactionEndDate())) {
            try {
                transactionEndDate = NvxDateUtils.parseDate(criteria.getTransactionEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                log.error("Error parsing date: " + criteria.getTransactionEndDate(), e);
            }
        }
        if (StringUtils.isNotBlank(criteria.getTransactionStatus())) {
            transactionStatus = criteria.getTransactionStatus();
        }
        if (StringUtils.isNotBlank(criteria.getReceiptNumber())) {
            receiptNumber = formatForLike(criteria.getReceiptNumber());
        }

//      return (int)reservationRepo.count();
        return reservationRepo.countBySearchCriteria(
            guestName,
            dateOfVisit,
            criteria.getReservationStatus(),
            criteria.getReservationType(),
            packageName,
            transactionStartDate,
            transactionEndDate,
            transactionStatus,
            receiptNumber
        ).intValue();
    }

    private String formatForLike(String s) {
        return "%" + s + "%";
    }

    @Override
    @Transactional(readOnly = true)
    public List<MenuRecord> getMenus(int packageId) {
        final List<MenuRecord> records = new ArrayList<MenuRecord>();
        final SkyDiningPackage skyDiningPackage = packageRepo.find(packageId);
        final List<SkyDiningMenu> menus = skyDiningPackage.getMenus();

        for (final SkyDiningMenu skyDiningMenu : menus) {
            final MenuRecord record = new MenuRecord(skyDiningMenu);
            records.add(record);
        }
        return records;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PackageRecord> getPackages() {
        final List<PackageRecord> records = new ArrayList<PackageRecord>();
        final List<SkyDiningPackage> packages = packageRepo.findPaged(null,
            null, null, null, null, -1, -1, "name", true);
        for (final SkyDiningPackage skyDiningPackage : packages) {
            final PackageRecord record = new PackageRecord(skyDiningPackage);
            records.add(record);
        }
        return records;
    }

    @Override
    @Transactional(readOnly = true)
    public List<PackageRecord> getPackages(String date) {
        if (StringUtils.isBlank(date)) {
            return getPackages();
        } else {
            final List<PackageRecord> records = new ArrayList<PackageRecord>();
            try {
                final List<SkyDiningPackage> packages = packageRepo.findPaged(
                    null, null, null, new Date(),
                    NvxDateUtils.parseDate(date, SysConst.DEFAULT_DATE_FORMAT),
                    -1, -1, "name", true);
                for (final SkyDiningPackage skyDiningPackage : packages) {
                    final PackageRecord record = new PackageRecord(
                        skyDiningPackage);
                    records.add(record);
                }
            } catch (final ParseException e) {
                log.error("Unable to parse date.", e);
            }
            return records;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public ReservationModel getReservation(int id) {
        final SkyDiningReservation reservation = reservationRepo.findOne(id);
        final ReservationModel model = new ReservationModel(reservation);

        final SkyDiningPackage selectedPackage = packageRepo.find(reservation.getPackageId());
        if (StringUtils.isBlank(model.getPackageName())) {
            if (selectedPackage != null) {
                model.setPackageName(selectedPackage.getName());
            } else {
                model.setPackageName("");
            }
        }

        final SkyDiningSession selectedSession = sessionRepo.find(reservation.getSessionId());
        if (selectedSession != null) {
            if (selectedSession.getSessionNumber() > 1) {
                model.setSessionName(selectedSession.getName() + " - Session "
                    + selectedSession.getSessionNumber());
            } else {
                model.setSessionName(selectedSession.getName());
            }

            if (selectedSession.getTime() != null) {
                model.setSessionTime(selectedSession.getTime());
            }
        }

        SkyDiningTheme selectedTheme;
        if (reservation.getThemeId() != null && reservation.getThemeId() > 0) {
            selectedTheme = themeRepo.find(reservation.getThemeId());
            if (selectedTheme != null) {
                model.setThemeDescription(selectedTheme.getDescription());
            } else {
                model.setThemeDescription("");
            }
        }

        final SkyDiningMenu selectedMenu = menuRepo.find((reservation.getMenuId()));
        if (selectedMenu != null) {
            model.setMenuName(selectedMenu.getName());
            model.setMenuPrice(selectedMenu.getPrice());
            model.setMenuGst(selectedMenu.getGst());
            model.setMenuServiceCharge(selectedMenu.getServiceCharge());
        } else {
            model.setMenuName("");
        }

        if (reservation.getDiscountId() != null) {
            final SkyDiningDiscount discount = discountRepo.find(reservation
                .getDiscountId());
            if (discount != null) {
                model.setDiscountName(discount.getName());
            }
        }

        return model;
    }

    private String getSortColumnName(String orderBy) {
        switch (orderBy) {
            case "dateOfVisit":
                return "date";
            case "transactionDate":
                return "createdDate";
            case "reservationStatus":
                return "status";
            case "reservationType":
                return "type";
            default:
                return orderBy;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReservationRecord> getReservationRecords(ReservationFilterCriteria criteria) {
        final List<ReservationRecord> records = new ArrayList<>();
        final PageRequest pageRequest = new PageRequest(criteria.getStart() / criteria.getPageSize(), criteria.getPageSize(),
            new Sort(BooleanUtils.isFalse(criteria.getAsc()) ? Sort.Direction.DESC : Sort.Direction.ASC, getSortColumnName(criteria.getOrderBy())));

        String guestName = null;
        Date dateOfVisit = null;
        String packageName = null;
        Date transactionStartDate = null;
        Date transactionEndDate = null;
        String transactionStatus = null;
        String receiptNumber = null;

        if (StringUtils.isNotBlank(criteria.getGuestName())) {
            guestName = formatForLike(criteria.getGuestName());
        }
        if (StringUtils.isNotBlank(criteria.getDateOfVisit())) {
            try {
                dateOfVisit = NvxDateUtils.parseDate(criteria.getDateOfVisit(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY);
            } catch (ParseException e) {
                log.error("Error parsing date: " + criteria.getDateOfVisit(), e);
            }
        }
        if (StringUtils.isNotBlank(criteria.getPackageName())) {
            packageName = formatForLike(criteria.getPackageName());
        }
        if (StringUtils.isNotBlank(criteria.getTransactionStartDate())) {
            try {
                transactionStartDate = NvxDateUtils.populateQueryFromDate(
                    NvxDateUtils.parseDate(criteria.getTransactionStartDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY), true);
            } catch (ParseException e) {
                log.error("Error parsing date: " + criteria.getTransactionStartDate(), e);
            }
        }
        if (StringUtils.isNotBlank(criteria.getTransactionEndDate())) {
            try {
                transactionEndDate = NvxDateUtils.populateQueryToDate(
                    NvxDateUtils.parseDate(criteria.getTransactionEndDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY), true);
            } catch (ParseException e) {
                log.error("Error parsing date: " + criteria.getTransactionEndDate(), e);
            }
        }
        if (StringUtils.isNotBlank(criteria.getTransactionStatus())) {
            transactionStatus = criteria.getTransactionStatus();
        }
        if (StringUtils.isNotBlank(criteria.getReceiptNumber())) {
            receiptNumber = formatForLike(criteria.getReceiptNumber());
        }

//    final List<SkyDiningReservation> reservations = reservationRepo.findAll(pageRequest).getContent();
        final List<SkyDiningReservation> reservations = reservationRepo.findBySearchCriteria(
            guestName,
            dateOfVisit,
            criteria.getReservationStatus(),
            criteria.getReservationType(),
            packageName,
            transactionStartDate,
            transactionEndDate,
            transactionStatus,
            receiptNumber,
            pageRequest).getContent();
        for (final SkyDiningReservation reservation : reservations) {
            final ReservationRecord record = new ReservationRecord(reservation);
            final SkyDiningTransaction transaction = reservation.getTransaction();
            if (transaction != null) {
                record.setReceiptNumber(transaction.getReceiptNum());
            }
            records.add(record);
        }
        return records;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResMainCourseModel> getResMainCourses(int parentId) {
        final SkyDiningReservation reservation = reservationRepo.findOne(parentId);
        final List<SkyDiningResMainCourse> mainCourses = reservation
            .getMainCourses();
        final List<ResMainCourseModel> models = new ArrayList<ResMainCourseModel>();

        for (final SkyDiningResMainCourse mainCourse : mainCourses) {
            final SkyDiningMainCourse selected = mainCourseRepo.find(mainCourse.getMainCourseId());
            final ResMainCourseModel model = new ResMainCourseModel(mainCourse,
                selected);
            models.add(model);
        }
        return models;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResMainCourseModel> getResMainCourses(int parentId, int menuId,
                                                      boolean includeUnselected) {
        final List<ResMainCourseModel> models = new ArrayList<ResMainCourseModel>();
        final SkyDiningReservation reservation = reservationRepo.findOne(parentId);

        SkyDiningMenu menu = null;
        if (menuId > 0) {
            menu = menuRepo.find(menuId);
        } else if (reservation != null) {
            menu = menuRepo.find(reservation.getMenuId());
        }

        if (menu != null) {
            final List<SkyDiningMainCourse> mainCourses = menu.getMainCourses();
            if (reservation != null) {
                final List<SkyDiningResMainCourse> resMainCourses = reservation
                    .getMainCourses();
                for (final SkyDiningMainCourse mainCourse : mainCourses) {
                    boolean isSelected = false;
                    for (final SkyDiningResMainCourse resMainCourse : resMainCourses) {
                        if (mainCourse.getId() == resMainCourse.getMainCourseId()) {
                            final ResMainCourseModel model = new ResMainCourseModel(
                                resMainCourse, mainCourse);
                            models.add(model);
                            isSelected = true;
                            break;
                        }
                    }
                    if (!isSelected && includeUnselected) {
                        final ResMainCourseModel model = new ResMainCourseModel(
                            mainCourse);
                        models.add(model);
                    }
                }
            } else {
                for (final SkyDiningMainCourse mainCourse : mainCourses) {
                    final ResMainCourseModel model = new ResMainCourseModel(
                        mainCourse);
                    models.add(model);
                }
            }
        }

        return models;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResTopUpModel> getResTopUps(int parentId) {
        final SkyDiningReservation reservation = reservationRepo.findOne(parentId);
        final List<SkyDiningResTopUp> topUps = reservation.getTopUps();
        final List<ResTopUpModel> models = new ArrayList<ResTopUpModel>();

        for (final SkyDiningResTopUp topUp : topUps) {
            final SkyDiningTopUp selected = topUpRepo.find(topUp.getTopUpId());
            final ResTopUpModel model = new ResTopUpModel(topUp, selected);
            models.add(model);
        }
        return models;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResTopUpModel> getResTopUps(int parentId, int packageId,
                                            boolean includeUnselected) {
        final List<ResTopUpModel> models = new ArrayList<ResTopUpModel>();
        final SkyDiningReservation reservation = reservationRepo.findOne(parentId);

        SkyDiningPackage sdPackage = null;
        if (packageId > 0) {
            sdPackage = packageRepo.find(packageId);
        } else if (reservation != null) {
            sdPackage = packageRepo.find(reservation.getPackageId());
        }

        if (sdPackage != null) {
            final List<SkyDiningTopUp> topUps = sdPackage.getTopUps();
            if (reservation != null) {
                final List<SkyDiningResTopUp> resTopUps = reservation
                    .getTopUps();
                for (final SkyDiningTopUp topUp : topUps) {
                    boolean isSelected = false;
                    for (final SkyDiningResTopUp resTopUp : resTopUps) {
                        if (topUp.getId() == resTopUp.getTopUpId()) {
                            final ResTopUpModel model = new ResTopUpModel(
                                resTopUp, topUp);
                            models.add(model);
                            isSelected = true;
                            break;
                        }
                    }
                    if (!isSelected && includeUnselected) {
                        final ResTopUpModel model = new ResTopUpModel(topUp);
                        models.add(model);
                    }
                }
            } else {
                for (final SkyDiningTopUp topUp : topUps) {
                    final ResTopUpModel model = new ResTopUpModel(topUp);
                    models.add(model);
                }
            }
        }

        return models;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SessionRecord> getSessions(int packageId, String date,
                                           String time) {
        final List<SessionRecord> records = new ArrayList<>();
        try {
            List<SkyDiningSession> sessions = null;
            if (StringUtils.isNotBlank(date)) {
                sessions = sessionRepo.findAvailable(new Date(),
                    NvxDateUtils.parseDate(date, SysConst.DEFAULT_DATE_FORMAT),
                    packageId);
            } else {
                sessions = sessionRepo
                    .findAvailable(new Date(), null, packageId);
            }
            for (final SkyDiningSession session : sessions) {
                final SessionRecord record = new SessionRecord(session);
                records.add(record);
            }
        } catch (final ParseException e) {
            log.error("Unable to parse date.", e);
        }

        return records;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ThemeModel> getThemes(int packageId) {
        final List<ThemeModel> models = new ArrayList<ThemeModel>();
        final SkyDiningPackage skyDiningPackage = packageRepo.find(packageId);
        final List<SkyDiningTheme> themes = skyDiningPackage.getThemes();

        for (final SkyDiningTheme skyDiningTheme : themes) {
            final ThemeModel model = new ThemeModel(skyDiningTheme);
            models.add(model);
        }
        return models;
    }

    @Override
    @Transactional
    public SaveResult saveReservation(ReservationModel model, String userId,
                                      String timestampText) {
        SkyDiningReservation entity = reservationRepo.findOne(model.getId());

        Date timestamp = null;
        if (StringUtils.isNotEmpty(timestampText)) {
            timestamp = NvxDateUtils.parseModDt(timestampText);
        }

        if (entity == null) {
            timestamp = new Date();
            entity = new SkyDiningReservation();
            entity.setId(model.getId());
            entity.setCreatedBy(userId);
            entity.setCreatedDate(timestamp);
        } else {
            if (!isTimestampValid(entity, timestamp)) {
                return new SaveResult(SaveResult.Result.StaleData);
            }
            timestamp = new Date();
        }

        if (model.getGuestName() != null) {
            entity.setGuestName(model.getGuestName());
        }

        if (model.getContactNumber() != null) {
            entity.setContactNumber(model.getContactNumber());
        }

        if (model.getEmailAddress() != null) {
            entity.setEmailAddress(model.getEmailAddress());
        }

        if (model.getDate() != null) {
            entity.setDate(model.getDate());
        }

        if (model.getTime() != null) {
            entity.setTime(model.getTime());
        }

        if (model.getPax() != null) {
            entity.setPax(model.getPax());
        }

        if (model.getSpecialRequest() != null) {
            entity.setSpecialRequest(model.getSpecialRequest());
        }

        if (model.getType() != null) {
            entity.setType(model.getType());
        }

        if (model.getStatus() != null) {
            entity.setStatus(model.getStatus());
        }

        if (model.getPackageId() != null) {
            entity.setPackageId(model.getPackageId());
        }

        if (model.getSessionId() != null) {
            entity.setSessionId(model.getSessionId());
        }

        if (model.getThemeId() != null) {
            if (model.getThemeId() > 0) {
                entity.setThemeId(model.getThemeId());
            } else {
                entity.setThemeId(null);
            }
        }

        if (model.getMenuId() != null) {
            entity.setMenuId(model.getMenuId());
        }

        if ((model.getMainCourseIds() != null)
            && (model.getMainCourseQuantities() != null)) {
            final List<SkyDiningResMainCourse> mainCourses = entity
                .getMainCourses();
            final List<Integer> newIds = model.getMainCourseIds();
            final List<Integer> newQuantities = model.getMainCourseQuantities();

            final Iterator<SkyDiningResMainCourse> iterator = mainCourses
                .iterator();
            while (iterator.hasNext()) {
                final SkyDiningResMainCourse mainCourse = iterator.next();
                boolean removeFlag = true;
                for (final Integer newId : newIds) {
                    if (newId == mainCourse.getMainCourseId()) {
                        removeFlag = false;
                        break;
                    }
                }
                if (removeFlag) {
                    // Remove missing
                    iterator.remove();
                }
            }

            for (int i = 0; i < newIds.size(); i++) {
                final Integer newId = newIds.get(i);
                final Integer newQuantity = newQuantities.get(i);
                boolean newFlag = true;

                for (final SkyDiningResMainCourse mainCourse : mainCourses) {
                    if (newId == mainCourse.getMainCourseId()) {
                        // Update existing
                        mainCourse.setQuantity(newQuantity);
                        mainCourse.setModifiedBy(userId);
                        mainCourse.setModifiedDate(timestamp);
                        newFlag = false;
                        break;
                    }
                }

                if (newFlag) {
                    final SkyDiningMainCourse mainCourse = mainCourseRepo
                        .find(newId);
                    if (mainCourse != null) {
                        final SkyDiningResMainCourse resMainCourse = new SkyDiningResMainCourse();
                        resMainCourse.setId(0);
                        resMainCourse.setMainCourseId(mainCourse.getId());
                        resMainCourse.setQuantity(newQuantity);
                        resMainCourse.setCreatedBy(userId);
                        resMainCourse.setCreatedDate(timestamp);
                        resMainCourse.setModifiedBy(userId);
                        resMainCourse.setModifiedDate(timestamp);

                        // Add new
                        entity.getMainCourses().add(resMainCourse);
                    }
                }

            }
        }

        if ((model.getTopUpIds() != null)
            && (model.getTopUpQuantities() != null)) {
            final List<SkyDiningResTopUp> topUps = entity.getTopUps();
            final List<Integer> newIds = model.getTopUpIds();
            final List<Integer> newQuantities = model.getTopUpQuantities();

            final Iterator<SkyDiningResTopUp> iterator = topUps.iterator();
            while (iterator.hasNext()) {
                final SkyDiningResTopUp topUp = iterator.next();
                boolean removeFlag = true;
                for (final Integer newId : newIds) {
                    if (newId == topUp.getTopUpId()) {
                        removeFlag = false;
                        break;
                    }
                }
                if (removeFlag) {
                    // Remove missing
                    iterator.remove();
                }
            }

            for (int i = 0; i < newIds.size(); i++) {
                final Integer newId = newIds.get(i);
                final Integer newQuantity = newQuantities.get(i);
                boolean newFlag = true;

                for (final SkyDiningResTopUp topUp : topUps) {
                    if (newId == topUp.getTopUpId()) {
                        // Update existing
                        topUp.setQuantity(newQuantity);
                        topUp.setModifiedBy(userId);
                        topUp.setModifiedDate(timestamp);
                        newFlag = false;
                        break;
                    }
                }

                if (newFlag) {
                    final SkyDiningTopUp topUp = topUpRepo.find(newId);
                    if (topUp != null) {
                        final SkyDiningResTopUp resTopUp = new SkyDiningResTopUp();
                        resTopUp.setId(0);
                        resTopUp.setTopUpId(topUp.getId());
                        resTopUp.setQuantity(newQuantity);
                        resTopUp.setCreatedBy(userId);
                        resTopUp.setCreatedDate(timestamp);
                        resTopUp.setModifiedBy(userId);
                        resTopUp.setModifiedDate(timestamp);

                        // Add new
                        entity.getTopUps().add(resTopUp);
                    }
                }

            }
        }

        if (model.getDiscountId() != null) {
            entity.setDiscountId(model.getDiscountId());
        }
        if (model.getJewelCard() != null) {
            entity.setJewelCard(model.getJewelCard());
        }
        if (model.getJewelCardExpiry() != null) {
            entity.setJewelCardExpiry(model.getJewelCardExpiry());
        }
        if (model.getPromoCode() != null) {
            entity.setPromoCode(model.getPromoCode());
        }
        if (model.getMerchantId() != null) {
            entity.setMerchantId(model.getMerchantId());
        }

        if (model.getReceivedBy() != null) {
            entity.setReceivedBy(model.getReceivedBy());
        }
        if (model.getReceiptNum() != null) {
            entity.setReceiptNum(model.getReceiptNum());
        }
        if (model.getPaymentDate() != null) {
            entity.setPaymentDate(model.getPaymentDate());
        }
        if (model.getPaymentMode() != null) {
            entity.setPaymentMode(model.getPaymentMode());
        }
        if (model.getPaidAmount() != null) {
            entity.setPaidAmount(model.getPaidAmount());
        }

        if (model.getPackageId() != null) {
            SkyDiningPackage sdPackage = packageRepo.find(model.getPackageId());
            if (sdPackage != null) {
                entity.setPackageName(sdPackage.getName());
            }
        }

        entity.setModifiedBy(userId);
        entity.setModifiedDate(timestamp);

        if (entity.getId() > 0) {
            reservationRepo.save(entity);
            return new SaveResult(SaveResult.Result.Success, timestamp,
                entity.getId());
        } else {
            reservationRepo.save(entity);
            return new SaveResult(SaveResult.Result.Success, timestamp);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public void sendReceiptEmail(String receiptNumber) {
        SkyDiningTransaction txn = transRepo.findOneByReceiptNum(receiptNumber);
        SkyDiningReservation res = txn.getReservation();

        sendReceiptEmail(txn, res);
    }

    @Override
    @Transactional(readOnly = true)
    public void sendReceiptEmail(SkyDiningTransaction txn, SkyDiningReservation res) {
        List<String> emails = new ArrayList<>();
        emails.add(res.getEmailAddress());

        SkyDiningReceiptDisplay display = getReceiptForDisplayByReceiptNum(txn, res);

        Long uuid = System.currentTimeMillis();

        boolean receiptPDF = false;
        String receiptName = null;
        String receiptAttchmentName = null;
        String receiptPath = null;
        String receiptRootDir = configService.getSkyDiningReceiptRootDir();
        if (StringUtils.isNotBlank(receiptRootDir)) {
            receiptAttchmentName = txn.getReceiptNum() + FileUtil.FILE_EXT_PDF;
            receiptName = txn.getReceiptNum() + "-" + (System.currentTimeMillis()) + FileUtil.FILE_EXT_PDF;
            receiptName = receiptName.toUpperCase();
            receiptPath = receiptRootDir + "/" + receiptName;
            try {
                receiptPDF = templateService.generateReceiptPdf(display, receiptPath);
            } catch (Exception e) {
                log.error("[" + uuid + "] Sky dining receipt not generated due to exception : " + e.getMessage(), e);
            } catch (Throwable e) {
                log.error("[" + uuid + "] Sky dining receipt not generated due to exception : " + e.getMessage(), e);
            }
        } else {
            log.error("[" + uuid + "] Sky dining receipt not generated due to sky dining receipt directory not configured.");
        }

        String html = null;
        String uniqueTrackingId = "[" + uuid + "] DepositTopUp-" + (txn.getReceiptNum() != null && txn.getReceiptNum().trim().length() > 0 ? txn.getReceiptNum().trim() : "");
        try {
            html = templateService.generateReceiptHtml(display);
        } catch (Exception e) {
            log.error("[" + uuid + "] Sky dining receipt email content not generated due to exception : " + e.getMessage(), e);
        } catch (Throwable e) {
            log.error("[" + uuid + "] Sky dining receipt  email content not generated due to exception : " + e.getMessage(), e);
        }

        boolean hasEmailTemplate = templateService.hasEmailTemplateByKey("b2c-mflg", DefaultSkyDiningTemplateService.TEMPLATE_SKY_DINING_RECEIPT);

        if (html != null && html.trim().length() > 0 && hasEmailTemplate) {

            SysEmailTemplateVM mailTemplateVM = templateService.getSystemParamByKey("b2c-mflg", DefaultSkyDiningTemplateService.TEMPLATE_SKY_DINING_RECEIPT);

            MailProperties props = new MailProperties();
            props.setSubject(mailTemplateVM.getSubject());
            props.setToUsers(emails.toArray(new String[emails.size()]));
            props.setBody(html);
            props.setBodyHtml(true);
            props.setTrackingId(uniqueTrackingId);
            props.setSender(mailSrv.getDefaultSender());
            if (receiptPDF) {
                props.setAttachFilePaths(new String[]{receiptPath});
                props.setAttachNames(new String[]{receiptAttchmentName});
                props.setHasAttach(true);
            } else {
                props.setHasAttach(false);
            }
            try {
                log.debug("Sky dining service -- sending email for " + uniqueTrackingId);
                mailSrv.sendEmail(props, new MailCallback() {
                    @Override
                    public void complete(String trackingId, boolean success) {
                        log.info("Finished sending email for {} with result {}", trackingId, success ? "SUCCESS" : "FAILED");
                    }
                });
            } catch (Exception e) {
                log.error("[" + uuid + "] Sending deposit topup email " + uniqueTrackingId + " failed due to exception : " + e.getMessage(), e);
            } catch (Throwable e) {
                log.error("[" + uuid + "] Sending deposit topup email " + uniqueTrackingId + " failed due to exception : " + e.getMessage(), e);
            }
        }
    }

    public SkyDiningReceiptDisplay getReceiptForDisplayByReceiptNum(SkyDiningTransaction txn, SkyDiningReservation res) {
        final SkyDiningFunCartDisplay cartVm = constructCartDisplayFromReservation(res);
        SkyDiningReceiptDisplay receiptDisplay = constructReceiptDisplay(cartVm, txn);
        return receiptDisplay;
    }

    @Transactional(readOnly = true)
    protected SkyDiningFunCartDisplay constructCartDisplayFromReservation(SkyDiningReservation res) {
        SkyDiningMenu selectedMenu = menuRepo.find(res.getMenuId());
        SkyDiningPackage selectedPackage = packageRepo.find(res.getPackageId());
        SkyDiningSession selectedSession = sessionRepo.find(res.getSessionId());
        SkyDiningTheme selectedTheme = null;
        if (res.getThemeId() != null) {
            selectedTheme = themeRepo.find(res.getThemeId());
        }

        final SkyDiningFunCartDisplay cd = new SkyDiningFunCartDisplay();
        cd.setCartId(Integer.toString(res.getId()));

        Integer totalMainQty = 0;
        Integer totalQty = 0;

        List<SkyDiningFunCartDisplayProduct> products = new ArrayList<>();
        SkyDiningFunCartDisplayItem displayItem = new SkyDiningFunCartDisplayItem();
        int realQty = 0;

        StringBuilder mainCourseText = new StringBuilder();

        for (SkyDiningResMainCourse mc : res.getMainCourses()) {
            Integer mainCourseId = mc.getMainCourseId();
            final SkyDiningMainCourse selectedMainCourse = mainCourseRepo.find(mainCourseId);

            final SkyDiningFunCartDisplayMainCourse displayMc = new SkyDiningFunCartDisplayMainCourse();
            displayMc.setMenuId(Integer.toString(selectedMainCourse.getId()));
            displayMc.setMenuName(selectedMainCourse.getDescription());
            displayMc.setQty(mc.getQuantity());

            displayItem.getMainCourses().add(displayMc);
            if (mainCourseText.length() > 0) {
                mainCourseText.append("<br/>");
            }
            mainCourseText.append(mc.getQuantity());
            mainCourseText.append("x ");
            mainCourseText.append(selectedMainCourse.getDescription());

            realQty += mc.getQuantity();
            totalMainQty += mc.getQuantity();
            totalQty += mc.getQuantity();
        }
        displayItem.setQty(realQty);
        if (StringUtils.isNotBlank(mainCourseText.toString())) {
            displayItem.setHasMainCourse(true);
        }
        displayItem.setMainCourseText(mainCourseText.toString());

        displayItem.setName(selectedMenu.getName());
        displayItem.setCartItemId(Integer.toString(selectedPackage.getId()));

        final List<TncVM> tncs = new ArrayList<>();
        if (selectedPackage.getTncs() != null && !selectedPackage.getTncs().isEmpty() && selectedPackage.getSelectedTncId() != null) {
            for (SkyDiningTnc tnc : selectedPackage.getTncs()) {
                if (selectedPackage.getSelectedTncId().equals(tnc.getId())) {
                    final TncVM tncVM = new TncVM();
                    tncVM.setContent(tnc.getTncContent());
                    tncVM.setTitle(tnc.getTitle());
                    tncVM.setType(TNCType.Product.name());
                    tncs.add(tncVM);
                }
            }
        }

        //Default discount values
        displayItem.setHasDiscount(false);
        displayItem.setDiscountName("");
        displayItem.setDiscountLabel("");
        displayItem.setDiscountTotal(new BigDecimal(0));
        displayItem.setDiscountTotalText("");

        if (res.getDiscountId() != null) {
            SkyDiningDiscount discount = discountRepo.find(res.getDiscountId());
            if (discount != null) {
                displayItem.setHasDiscount(true);
                displayItem.setDiscountName(discount.getName());
                displayItem.setPrice(getDiscountUnitPrice(discount, selectedMenu));
                displayItem.setDiscountTotal(NvxNumberUtils.centsToMoney(selectedMenu.getPrice()).subtract(displayItem.getPrice()).multiply(new BigDecimal(realQty)));
                displayItem.setDiscountTotalText(NvxNumberUtils.formatToCurrency(displayItem.getDiscountTotal()));
                if (StringUtils.isNotBlank(discount.getTnc())) {
                    final TncVM tncVM = new TncVM();
                    tncVM.setContent(discount.getTnc());
                    tncVM.setTitle(discount.getName());
                    tncVM.setType(TNCType.Product.name());
                    tncs.add(tncVM);
                }
            }
        } else {
            displayItem.setPrice(NvxNumberUtils.centsToMoney(selectedMenu.getPrice()));
        }
        displayItem.setPriceText(NvxNumberUtils.formatToCurrency(displayItem.getPrice()));

        displayItem.setTotal(displayItem.getPrice().multiply(new BigDecimal(realQty)));
        displayItem.setTotalText(NvxNumberUtils.formatToCurrency(displayItem.getTotal()));


        if (StringUtils.isNotBlank(res.getSpecialRequest())) {
            displayItem.setHasRemarks(true);
        }
        displayItem.setRemarks(res.getSpecialRequest());
        if (selectedTheme != null) {
            displayItem.setThemeId(Integer.toString(selectedTheme.getId()));
            displayItem.setThemeName(selectedTheme.getDescription());
        }

        StringBuilder description = new StringBuilder();
        description.append("Date of Visit: " + NvxDateUtils.formatDate(res.getDate(), SysConst.DEFAULT_DATE_FORMAT));
        description.append("<br/>");
        description.append("Session Time: " + selectedSession.getTime());
        if (selectedTheme != null) {
            description.append("<br/>");
            description.append("Theme: ");
            description.append(selectedTheme.getDescription());
        }

        displayItem.setDescription(description.toString());

        SkyDiningFunCartDisplayProduct product = new SkyDiningFunCartDisplayProduct();
        product.setId(Integer.toString(selectedPackage.getId()));
        product.setName(selectedPackage.getName());

        product.getSkyDiningItems().add(displayItem);

        BigDecimal subTotal = displayItem.getTotal();
        BigDecimal topUpPrice = BigDecimal.ZERO;

        if (res.getTopUps() != null) {
            for (SkyDiningResTopUp ctu : res.getTopUps()) {
                SkyDiningTopUp tu = topUpRepo.find(ctu.getTopUpId());
                if (tu != null) {
                    SkyDiningFunCartDisplayItem di = new SkyDiningFunCartDisplayItem();
                    di.setIsTopup(true);
                    di.setName(tu.getName());
                    di.setPrice(NvxNumberUtils.centsToMoney(tu.getPrice()));
                    di.setPriceText(NvxNumberUtils.formatToCurrency(di.getPrice()));
                    di.setCartItemId(Integer.toString(tu.getId()));
                    di.setQty(ctu.getQuantity());
                    di.setTotal(NvxNumberUtils.centsToMoney(tu.getPrice()).multiply(new BigDecimal(ctu.getQuantity())));
                    di.setTotalText(NvxNumberUtils.formatToCurrency(di.getTotal()));

                    product.getSkyDiningItems().add(di);
                    // subTotal.add(di.getTotal());
                    topUpPrice = topUpPrice.add(di.getTotal());
                    totalQty += ctu.getQuantity();
                }
            }
        }

        product.setSubtotal(subTotal);
        product.setSubtotalText(NvxNumberUtils.formatToCurrency(subTotal, ""));

        product.setTopUpPrice(topUpPrice);
        product.setTopUpPriceText(NvxNumberUtils.formatToCurrency(topUpPrice, ""));

        product.setServiceCharge(selectedMenu.getServiceCharge());
        product.setServiceChargeTotal(subTotal.multiply(new BigDecimal(selectedMenu.getServiceCharge())).divide(new BigDecimal(100)));
        product.setServiceChargeTotalText(NvxNumberUtils.formatToCurrency(product.getServiceChargeTotal(), ""));

        product.setGst(selectedMenu.getGst());
        product.setGstTotal(subTotal.add(product.getServiceChargeTotal()).multiply(new BigDecimal(selectedMenu.getGst())).divide(new BigDecimal(100)));
        product.setGstTotalText(NvxNumberUtils.formatToCurrency(product.getGstTotal(), ""));

        products.add(product);

        BigDecimal total = product.getSubtotal().add(product.getServiceChargeTotal()).add(product.getGstTotal()).add(product.getTopUpPrice());

        cd.setSkyDiningProducts(products);
        cd.setTncs(tncs);
        cd.setHasTnc(tncs != null && !tncs.isEmpty());
        cd.setTotalMainQty(totalMainQty);
        cd.setTotalQty(totalQty);
        cd.setTotal(total);
        cd.setTotalText(NvxNumberUtils.formatToCurrency(total, "S$ "));

        return cd;
    }

    protected BigDecimal getDiscountUnitPrice(SkyDiningDiscount discount,
                                              SkyDiningMenu menu) {
        final int priceInCents = menu.getPrice();
        BigDecimal unitPrice = NvxNumberUtils.centsToMoney(priceInCents);
        if ((discount.getPriceType() != null)
            && (discount.getDiscountPrice() != null)) {
            switch (discount.getPriceType()) {
                case 1:
                    unitPrice = NvxNumberUtils.percentOff(menu.getPrice(),
                        discount.getDiscountPrice());
                    break;
                case 2:
                    unitPrice = NvxNumberUtils.centsToMoney(menu.getPrice()
                        - discount.getDiscountPrice());
                    break;
                case 3:
                    unitPrice = NvxNumberUtils.centsToMoney(discount.getDiscountPrice());
                    break;
                default:
                    break;
            }
        }
        return unitPrice;
    }

    protected SkyDiningReceiptDisplay constructReceiptDisplay(SkyDiningFunCartDisplay cartVm, SkyDiningTransaction trans) {
        final SkyDiningReceiptDisplay receipt = new SkyDiningReceiptDisplay();

        receipt.setCartId(cartVm.getCartId());
        receipt.setTotalQty(cartVm.getTotalQty());
        receipt.setTotalMainQty(cartVm.getTotalMainQty());
        receipt.setCmsProducts(cartVm.getCmsProducts());
        receipt.setPayMethod(cartVm.getPayMethod());
        receipt.setPaymentTypeLabel("");
        if (StringUtils.isNotEmpty(receipt.getPayMethod())) {
            receipt.setMessage("");
        }

        BigDecimal grandTotal = cartVm.getTotal();

        receipt.setTotal(grandTotal);
        receipt.setTotalText(NvxNumberUtils.formatToCurrency(grandTotal, "S$ "));

        receipt.getSkyDiningProducts().addAll(cartVm.getSkyDiningProducts());


        if (trans != null) {
            receipt.setTransStatus(trans.getStage());

            CustomerDetails details = new CustomerDetails();
            details.setCompanyName(trans.getCustCompany());
            details.setPaymentType(trans.getPaymentType());
            details.setEmail(trans.getCustEmail());
            details.setIdNo(trans.getCustIdNo());
            details.setIdType(trans.getCustIdType());
            details.setMerchantId(trans.getTmMerchantId());
            details.setMobile(trans.getCustMobile());
            details.setName(trans.getCustName());
            details.setSalutation(trans.getCustSalutation());
            details.setSubscribed(trans.getCustSubscribe());

            receipt.setCustomer(details);

            receipt.setDateOfPurchase(NvxDateUtils.formatDateForDisplay(trans.getCreatedDate(), false));
            receipt.setReceiptNumber(trans.getReceiptNum());
            receipt.setPaymentType(trans.getPaymentType());
            receipt.setPaymentTypeLabel(trans.getPaymentType());
            receipt.setError(StringUtils.isNotBlank(trans.getTmErrorMessage()));
            receipt.setErrorCode(trans.getErrorCode());
            receipt.setErrorMessage(trans.getTmErrorMessage());
            receipt.setMessage("");
        }

        receipt.setHasTnc(cartVm.getHasTnc());
        receipt.setTncs(cartVm.getTncs());

//        final String appContextRoot = paramService.getSystemParamValueByKey("b2c-mflg", DefaultB2CSysParamService.SYS_PARAM_B2C_APP_CONTEXT_ROOT);
//        receipt.setBaseUrl(appContextRoot);

        return receipt;
    }
}
