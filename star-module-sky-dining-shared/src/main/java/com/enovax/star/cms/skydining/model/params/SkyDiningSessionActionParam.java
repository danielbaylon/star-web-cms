package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 25/7/16.
 */
public class SkyDiningSessionActionParam extends BaseSkyDiningActionParam {

  protected String newName;
  protected Integer newSessionNumber;
  protected Integer newSessionType;
  protected String newStartDate;
  protected String newEndDate;
  protected String newTime;
  protected String newSalesStartDate;
  protected String newSalesEndDate;
  protected Integer newCapacity;
  protected Boolean newActive;
  protected String newEffectiveDays;

  private String nameFilter;
  private Integer activeFilter;

  public String getNewEffectiveDays() {
    return newEffectiveDays;
  }

  public void setNewEffectiveDays(String newEffectiveDays) {
    this.newEffectiveDays = newEffectiveDays;
  }

  public String getNewName() {
    return newName;
  }

  public void setNewName(String newName) {
    this.newName = newName;
  }

  public Integer getNewSessionNumber() {
    return newSessionNumber;
  }

  public void setNewSessionNumber(Integer newSessionNumber) {
    this.newSessionNumber = newSessionNumber;
  }

  public Integer getNewSessionType() {
    return newSessionType;
  }

  public void setNewSessionType(Integer newSessionType) {
    this.newSessionType = newSessionType;
  }

  public String getNewStartDate() {
    return newStartDate;
  }

  public void setNewStartDate(String newStartDate) {
    this.newStartDate = newStartDate;
  }

  public String getNewEndDate() {
    return newEndDate;
  }

  public void setNewEndDate(String newEndDate) {
    this.newEndDate = newEndDate;
  }

  public String getNewTime() {
    return newTime;
  }

  public void setNewTime(String newTime) {
    this.newTime = newTime;
  }

  public String getNewSalesStartDate() {
    return newSalesStartDate;
  }

  public void setNewSalesStartDate(String newSalesStartDate) {
    this.newSalesStartDate = newSalesStartDate;
  }

  public String getNewSalesEndDate() {
    return newSalesEndDate;
  }

  public void setNewSalesEndDate(String newSalesEndDate) {
    this.newSalesEndDate = newSalesEndDate;
  }

  public Integer getNewCapacity() {
    return newCapacity;
  }

  public void setNewCapacity(Integer newCapacity) {
    this.newCapacity = newCapacity;
  }

  public Boolean getNewActive() {
    return newActive;
  }

  public void setNewActive(Boolean newActive) {
    this.newActive = newActive;
  }

  public String getNameFilter() {
    return nameFilter;
  }

  public void setNameFilter(String nameFilter) {
    this.nameFilter = nameFilter;
  }

  public Integer getActiveFilter() {
    return activeFilter;
  }

  public void setActiveFilter(Integer activeFilter) {
    this.activeFilter = activeFilter;
  }
}
