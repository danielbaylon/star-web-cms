package com.enovax.star.cms.skydining.constant;

public enum SkyDiningSysParamConst {
  //General
  FailedLoginAttempts,
  DaysPasswordExpires,
  NumPasswordHistory,

  BookingMonthsAhead,

  HasBookingFeeWaiver,
  BookingFeeWaiverItemCode,
  BookingFeeCached,

  SiteMode,
  MaxCartItems,
  BookingFeeToggle,
  BookingFeeMode,

  BookingQtyPerItem,
  RecordsPerPage,

  SlidesMax,

  //Mail
  MailHost,
  MailPort,
  MailAuthReq,
  MailSslReq,
  MailUsername,
  MailPassword,
  MailSender,

  //AdminMail
  FnbMailTo,
  NsactMailTo,
  AdminMailTo,
  AdminMailCc,
  AdminMailBcc,

  //GeneralCatalog
  GenCataRetryCount,
  GenCataRetryMillis,
  GenCataRetrieveMode,
  GenCataFtpUrl,
  GenCataFtpPort,
  GenCataFtpUser,
  GenCataFtpPass,
  GenCataFile,
  GenCataDirPath,

  //EventCatalog
  EventCataRetryCount,
  EventCataRetryMillis,
  EventCataRetrieveMode,
  EventCataFtpUrl,
  EventCataFtpPort,
  EventCataFtpUser,
  EventCataFtpPass,
  EventCataFile,
  EventCataDirPath,

  SyncFtpUrl,
  SyncFtpPort,
  SyncFtpUser,
  SyncFtpPwd,
  SyncFtpPartner,

  SyncFtpBarcodeDir,

  //VoidRecon
  VoidFtpUrl,
  VoidFtpPort,
  VoidFtpUser,
  VoidFtpPass,
  VoidFtpRetryCount,
  VoidFtpRetryMillis,
  VoidFtpDir,
  VoidFtpTempDir,
  VoidFtpMinsPast,
  Void,

  MailSubjectVoidSuccess,
  MailSubjectVoidFailed,
  MailContentVoidSuccess,
  MailContentVoidFailed,

  //News
  NewsMaxWidth,
  NewsMaxHeight,
  CustomerServiceEmail,
  SkyDiningReceiptBcc,

  ShowJcCateg;

  public enum YesNoVals {Y, N}

  public enum SiteModeVals {L, UM}

  public enum BookFeeToggleVals {ON, OFF}

  public enum BookFeeModeVals {None, Trans, Ticket}

  public enum CatalogRetrieveMode {FTP, DIR, SFTP, test}
}
