package com.enovax.star.cms.skydining.datamodel;
// Generated Apr 19, 2013 3:21:53 PM by Hibernate Tools 3.4.0.CR1


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SkyDiningJewelCard", schema = "dbo", catalog = "STAR_DB")
public class SkyDiningJewelCard implements HasTimestamp {

    private int id;
    private String cardNumber;
    private Date expiryDate;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "cardNumber", nullable = false, length = 50)
    public String getCardNumber() {
        return this.cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Column(name = "expiryDate", nullable = false, length = 23)
    @Temporal(TemporalType.DATE)
    public Date getExpiryDate() {
        return this.expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    @Column(name = "createdBy", nullable = false)
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    @Column(name = "createdDate", nullable = false, length = 23)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedDate() {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    @Column(name = "modifiedBy", nullable = false)
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    @Column(name = "modifiedDate", nullable = false, length = 23)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getModifiedDate() {
        return this.modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}


