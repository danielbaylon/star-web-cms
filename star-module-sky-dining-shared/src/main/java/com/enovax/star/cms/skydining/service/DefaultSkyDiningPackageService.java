package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.commons.model.merchant.Merchant;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SkyDiningDiscountType;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.*;
import com.enovax.star.cms.skydining.model.*;
import com.enovax.star.cms.skydining.util.JcrFileUtil;
import com.enovax.util.jcr.exception.JcrException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
@Service
public class DefaultSkyDiningPackageService extends BaseSkyDiningAdminService implements ISkyDiningPackageService {
  @Override
  public SaveResult addMenusToDiscount(int parentId, List<Integer> ids,
                                       String userId, String timestampText) {
    final SkyDiningDiscount discount = discountRepo.find(parentId);

    if (discount == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining promotion.");
    }
    for (final int id : ids) {
      final SkyDiningMenu menu = menuRepo.find(id);
      if (menu == null) {
        return new SaveResult(SaveResult.Result.Failed,
          "Unable to find sky dining menu(s).");
      } else {
        discount.getMenus().add(menu);
      }
    }

    final Date timestamp = new Date();
    discount.setModifiedBy(userId);
    discount.setModifiedDate(timestamp);
    try {
      discountRepo.save(discount);
    } catch (JcrException e) {
      log.error("Unable to save discount.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save discount.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult addMenusToPackage(int parentId, List<Integer> ids,
                                      String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    for (final int id : ids) {
      final SkyDiningMenu menu = menuRepo.find(id);
      if (menu == null) {
        return new SaveResult(SaveResult.Result.Failed,
          "Unable to find sky dining menu(s).");
      } else {
        sdPackage.getMenus().add(menu);
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult addTopUpsToPackage(int parentId, List<Integer> ids,
                                       String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    for (final int id : ids) {
      final SkyDiningTopUp menu = topUpRepo.find(id);
      if (menu == null) {
        return new SaveResult(SaveResult.Result.Failed,
          "Unable to find sky dining top up(s).");
      } else {
        sdPackage.getTopUps().add(menu);
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public int countPackageRecords(String name, Boolean active) {
    return packageRepo.count(name, active, null, null, null);
  }

  @Override
  public int countSelectDiscountMenuRecords(Integer discountId, String name,
                                            Boolean active, List<Integer> menuTypes) {
    if ((discountId == null) || (discountId < 1)) {
      return 0;
    }

    return menuRepo.count(name, active, null, discountId, menuTypes);
  }

  @Override
  public int countSelectMenuRecords(Integer packageId, String name,
                                    Boolean active, List<Integer> menuTypes) {
    if ((packageId == null) || (packageId < 1)) {
      return 0;
    }

    return menuRepo.count(name, active, packageId, null, menuTypes);
  }

  @Override
  public int countSelectTopUpRecords(Integer packageId, String name,
                                     Boolean active) {

    if ((packageId == null) || (packageId < 1)) {
      return 0;
    }

    return topUpRepo.count(name, active, packageId);
  }

  @Override
  public List<MenuRecord> getDiscountMenuRecords(int parentId) {
    final List<MenuRecord> menuRecords = new ArrayList<MenuRecord>();
    final SkyDiningDiscount discount = discountRepo.find(parentId);
    if (discount != null) {
      final List<SkyDiningMenu> dataModels = discount.getMenus();
      for (final SkyDiningMenu datamodel : dataModels) {
        final MenuRecord menuRecord = new MenuRecord(datamodel);
        menuRecords.add(menuRecord);
      }
    }
    return menuRecords;
  }

  @Override
  public List<DiscountModel> getDiscounts(int parentId) {
    final List<DiscountModel> discountModels = new ArrayList<DiscountModel>();
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);
    if (sdPackage != null) {
      final List<SkyDiningDiscount> dataModels = sdPackage.getDiscounts();
      for (final SkyDiningDiscount datamodel : dataModels) {
        final DiscountModel discountModel = new DiscountModel(datamodel);
        discountModels.add(discountModel);
      }
    }
    return discountModels;
  }

  @Override
  public List<MenuRecord> getMenuRecords(int parentId) {
    final List<MenuRecord> menuRecords = new ArrayList<MenuRecord>();
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);
    if (sdPackage != null) {
      final List<SkyDiningMenu> dataModels = sdPackage.getMenus();
      for (final SkyDiningMenu datamodel : dataModels) {
        final MenuRecord menuRecord = new MenuRecord(datamodel);
        menuRecords.add(menuRecord);
      }
    }
    return menuRecords;
  }

  @Override
  public PackageModel getPackage(int id) {
    final SkyDiningPackage sdPackage = packageRepo.find(id);
    final PackageModel packageModel = new PackageModel(sdPackage);

    final String oldImageHash = packageModel.getImageHash();
    packageModel.setImageHash(StringUtils.isEmpty(oldImageHash) ? ""
      : getImagePath()
      + oldImageHash);

    if (sdPackage.getSelectedTncId() != null && sdPackage.getSelectedTncId() != 0) {
      final SkyDiningTnc selectedTnc = tncRepo.find(sdPackage.getSelectedTncId());
      if (selectedTnc == null) {
        packageModel.setSelectedTncId(0);
        packageModel.setTncContent("");
      } else {
        packageModel.setSelectedTncId(selectedTnc.getId());
        packageModel.setTncContent(selectedTnc.getTncContent());
      }
    } else {
      packageModel.setSelectedTncId(0);
      packageModel.setTncContent("");
    }


    return packageModel;
  }

  @Override
  public List<PackageRecord> getPackageRecords(String name, Boolean active,
                                               int start, int pageSize, String orderBy, boolean asc) {
    final List<PackageRecord> packageRecords = new ArrayList<PackageRecord>();
    final List<SkyDiningPackage> packages = packageRepo.findPaged(name,
      active, null, null, null, start, pageSize, orderBy, asc);

    for (final SkyDiningPackage skyDiningPackage : packages) {
      final PackageRecord packageRecord = new PackageRecord(
        skyDiningPackage);
      packageRecords.add(packageRecord);
    }

    return packageRecords;
  }

  @Override
  public List<PromoCodeModel> getPromoCodes(int parentId) {
    final List<PromoCodeModel> codeModels = new ArrayList<PromoCodeModel>();
    final SkyDiningDiscount discount = discountRepo.find(parentId);
    if (discount != null) {
      final List<SkyDiningPromoCode> dataModels = discount
        .getPromoCodes();
      for (final SkyDiningPromoCode datamodel : dataModels) {
        final PromoCodeModel codeModel = new PromoCodeModel(datamodel);
        codeModels.add(codeModel);
      }
    }
    return codeModels;
  }

  @Override
  public List<MenuRecord> getSelectDiscountMenuRecords(Integer discountId,
                                                       String name, Boolean active, List<Integer> menuTypes, int start,
                                                       int pageSize, String orderBy, boolean asc) {
    final List<MenuRecord> menuRecords = new ArrayList<MenuRecord>();

    if ((discountId == null) || (discountId < 1)) {
      return menuRecords;
    }

    final List<SkyDiningMenu> menus = menuRepo.findPaged(name, active, null,
      discountId, null, start, pageSize, orderBy, asc);

    for (final SkyDiningMenu menu : menus) {
      final MenuRecord menuRecord = new MenuRecord(menu);
      menuRecords.add(menuRecord);
    }

    return menuRecords;
  }

  @Override
  public List<MenuRecord> getSelectMenuRecords(Integer packageId,
                                               String name, Boolean active, List<Integer> menuTypes, int start,
                                               int pageSize, String orderBy, boolean asc) {
    final List<MenuRecord> menuRecords = new ArrayList<MenuRecord>();

    if ((packageId == null) || (packageId < 1)) {
      return menuRecords;
    }

    final List<SkyDiningMenu> menus = menuRepo.findPaged(name, active,
      packageId, null, menuTypes, start, pageSize, orderBy, asc);

    for (final SkyDiningMenu menu : menus) {
      final MenuRecord menuRecord = new MenuRecord(menu);
      menuRecords.add(menuRecord);
    }

    return menuRecords;
  }

  @Override
  public List<TopUpRecord> getSelectTopUpRecords(Integer packageId,
                                                 String name, Boolean active, int start, int pageSize,
                                                 String orderBy, boolean asc) {
    final List<TopUpRecord> topUpRecords = new ArrayList<TopUpRecord>();

    if ((packageId == null) || (packageId < 1)) {
      return topUpRecords;
    }

    final List<SkyDiningTopUp> topUps = topUpRepo.findPaged(name, active,
      packageId, start, pageSize, orderBy, asc);

    for (final SkyDiningTopUp topUp : topUps) {
      final TopUpRecord topUpRecord = new TopUpRecord(topUp);
      topUpRecords.add(topUpRecord);
    }

    return topUpRecords;
  }

  @Override
  public List<ThemeModel> getThemes(int parentId) {
    final List<ThemeModel> themes = new ArrayList<ThemeModel>();
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);
    if (sdPackage != null) {
      final List<SkyDiningTheme> dataModels = sdPackage.getThemes();
      for (final SkyDiningTheme dataModel : dataModels) {
        final ThemeModel theme = new ThemeModel(dataModel);

        final String oldImageHash = theme.getImageHash();
        theme.setImageHash(StringUtils.isEmpty(oldImageHash) ? ""
          : getImagePath()
          + oldImageHash);

        themes.add(theme);
      }
    }
    return themes;
  }

  @Override
  public List<TncModel> getTncs(int parentId) {
    final List<TncModel> tncs = new ArrayList<TncModel>();
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);
    if (sdPackage != null) {
      final List<SkyDiningTnc> dataModels = sdPackage.getTncs();
      for (final SkyDiningTnc dataModel : dataModels) {
        final TncModel tnc = new TncModel(dataModel);
        tncs.add(tnc);
      }
    }
    return tncs;
  }

  @Override
  public List<TopUpRecord> getTopUpRecords(int parentId) {
    final List<TopUpRecord> topUpRecords = new ArrayList<TopUpRecord>();
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);
    if (sdPackage != null) {
      final List<SkyDiningTopUp> dataModels = sdPackage.getTopUps();
      for (final SkyDiningTopUp datamodel : dataModels) {
        final TopUpRecord topUpRecord = new TopUpRecord(datamodel);

        final String oldImageHash = datamodel.getImageHash();
        topUpRecord.setImageHash(StringUtils.isEmpty(oldImageHash) ? ""
          : getImagePath()
          + oldImageHash);

        topUpRecords.add(topUpRecord);
      }
    }
    return topUpRecords;
  }

  @Override
  public SaveResult removeDiscounts(int parentId, List<Integer> ids,
                                    String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningDiscount> iterator = sdPackage.getDiscounts()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningDiscount tnc = iterator.next();

      for (final int id : ids) {
        if (tnc.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removeMenusFromDiscount(int parentId, List<Integer> ids,
                                            String userId, String timestampText) {
    final SkyDiningDiscount discount = discountRepo.find(parentId);

    final Iterator<SkyDiningMenu> iterator = discount.getMenus().iterator();
    while (iterator.hasNext()) {
      final SkyDiningMenu menu = iterator.next();

      for (final int id : ids) {
        if (menu.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    final Date timestamp = new Date();
    discount.setModifiedBy(userId);
    discount.setModifiedDate(timestamp);
    try {
      discountRepo.save(discount);
    } catch (JcrException e) {
      log.error("Unable to save discount.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save discount.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removeMenusFromPackage(int parentId, List<Integer> ids,
                                           String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningMenu> iterator = sdPackage.getMenus()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningMenu menu = iterator.next();

      for (final int id : ids) {
        if (menu.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removePromoCodes(int parentId, List<Integer> ids,
                                     String userId, String timestampText) {
    final SkyDiningDiscount discount = discountRepo.find(parentId);

    final Iterator<SkyDiningPromoCode> iterator = discount.getPromoCodes()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningPromoCode promoCode = iterator.next();

      for (final int id : ids) {
        if (promoCode.getId() == id) {
          try {
            promoCodeRepo.remove(promoCode);
          } catch (JcrException e) {
            log.error("Unable to remove promo code.", e);
            return new SaveResult(SaveResult.Result.Failed, "Unable to remove promo code.");
          }
          break;
        }
      }
    }

    final Date timestamp = new Date();
    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removeThemes(int parentId, List<Integer> ids,
                                 String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningTheme> iterator = sdPackage.getThemes()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningTheme theme = iterator.next();

      for (final int id : ids) {
        if (theme.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removeTncs(int parentId, List<Integer> ids,
                               String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining session.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningTnc> iterator = sdPackage.getTncs().iterator();
    while (iterator.hasNext()) {
      final SkyDiningTnc tnc = iterator.next();

      for (final int id : ids) {
        if (tnc.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult removeTopUpsFromPackage(int parentId, List<Integer> ids,
                                            String userId, String timestampText) {
    final SkyDiningPackage sdPackage = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (sdPackage == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(sdPackage, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final Iterator<SkyDiningTopUp> iterator = sdPackage.getTopUps()
      .iterator();
    while (iterator.hasNext()) {
      final SkyDiningTopUp menu = iterator.next();

      for (final int id : ids) {
        if (menu.getId() == id) {
          iterator.remove();
          break;
        }
      }
    }

    timestamp = new Date();
    sdPackage.setModifiedBy(userId);
    sdPackage.setModifiedDate(timestamp);
    try {
      packageRepo.save(sdPackage);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult saveDiscount(int parentId, int id, String newName,
                                 Boolean newActive, String newType, String newMerchantId,
                                 String userId, String timestampText, Integer newPriceType,
                                 Integer newDiscountPrice, String validFrom, String validTo,
                                 String tnc) {
    final SkyDiningPackage parent = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (parent == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(parent, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    timestamp = new Date();
    if (id < 1) {
      final SkyDiningDiscount entity = new SkyDiningDiscount();
      entity.setId(0);
      entity.setSdPackage(parent);
      if (newName != null) {
        entity.setName(newName);
      }
      if (newActive != null) {
        entity.setActive(newActive);
      }
      if (newType != null) {
        entity.setType(newType);
      }
      if (newMerchantId != null) {
        entity.setMerchantId(newMerchantId);
      }
      if (newPriceType != null) {
        entity.setPriceType(newPriceType);
      }
      if (newDiscountPrice != null) {
        entity.setDiscountPrice(newDiscountPrice);
      }

      if (SkyDiningDiscountType.valueOf(entity.getType()) != SkyDiningDiscountType.CreditCard) {
        entity.setMerchantId("");
      }

      if (SkyDiningDiscountType.valueOf(entity.getType()) == SkyDiningDiscountType.PromoCode) {
        entity.setValidFrom(null);
        entity.setValidTo(null);
      } else {
        if (StringUtils.isNotBlank(validFrom)) {
          try {
            entity.setValidFrom(NvxDateUtils.parseDate(validFrom,
              SysConst.DEFAULT_DATE_FORMAT));
          } catch (final ParseException e) {
            entity.setValidFrom(null);
            log.error("Unable to parse date: " + validFrom, e);
          }
        } else {
          entity.setValidFrom(null);
        }
        if (StringUtils.isNotBlank(validTo)) {
          try {
            entity.setValidTo(NvxDateUtils.parseDate(validTo,
              SysConst.DEFAULT_DATE_FORMAT));
          } catch (final ParseException e) {
            entity.setValidTo(null);
            log.error("Unable to parse date: " + validTo, e);
          }
        } else {
          entity.setValidTo(null);
        }
      }
      if (tnc != null) {
        entity.setTnc(tnc);
      }

      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
      entity.setModifiedBy(userId);
      entity.setModifiedDate(timestamp);

      parent.getDiscounts().add(entity);
      parent.setModifiedBy(userId);
      parent.setModifiedDate(timestamp);

      try {
        packageRepo.save(parent);
      } catch (JcrException e) {
        log.error("Unable to save package.", e);
        return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
      }

      final int newId = entity.getId();

      return new SaveResult(SaveResult.Result.Success, timestamp, newId);
    }

    final List<SkyDiningDiscount> entities = parent.getDiscounts();
    for (final SkyDiningDiscount entity : entities) {
      if (id == entity.getId()) {
        if (newName != null) {
          entity.setName(newName);
        }
        if (newActive != null) {
          entity.setActive(newActive);
        }
        if (newType != null) {
          entity.setType(newType);
        }
        if (newMerchantId != null) {
          entity.setMerchantId(newMerchantId);
        }
        if (newPriceType != null) {
          entity.setPriceType(newPriceType);
        }
        if (newDiscountPrice != null) {
          entity.setDiscountPrice(newDiscountPrice);
        }

        if (SkyDiningDiscountType.valueOf(entity.getType()) != SkyDiningDiscountType.CreditCard) {
          entity.setMerchantId("");
        }

        if (SkyDiningDiscountType.valueOf(entity.getType()) == SkyDiningDiscountType.PromoCode) {
          entity.setValidFrom(null);
          entity.setValidTo(null);
        } else {
          if (StringUtils.isNotBlank(validFrom)) {
            try {
              entity.setValidFrom(NvxDateUtils.parseDate(validFrom,
                SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
              entity.setValidFrom(null);
              log.error("Unable to parse date: " + validFrom, e);
            }
          } else {
            entity.setValidFrom(null);
          }
          if (StringUtils.isNotBlank(validTo)) {
            try {
              entity.setValidTo(NvxDateUtils.parseDate(validTo,
                SysConst.DEFAULT_DATE_FORMAT));
            } catch (final ParseException e) {
              entity.setValidTo(null);
              log.error("Unable to parse date: " + validTo, e);
            }
          } else {
            entity.setValidTo(null);
          }
        }
        if (tnc != null) {
          entity.setTnc(tnc);
        }

        entity.setModifiedBy(userId);
        entity.setModifiedDate(timestamp);
        parent.setModifiedBy(userId);
        parent.setModifiedDate(timestamp);
        try {
          packageRepo.save(parent);
        } catch (JcrException e) {
          log.error("Unable to save package.", e);
          return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
        }

        return new SaveResult(SaveResult.Result.Success, timestamp);
      }
    }

    return new SaveResult(SaveResult.Result.Failed,
      "Unable to find sky dining discount.");
  }

  @Override
  public SaveResult saveDisplayImage(int id, File image, String fileName,
                                     String userId, String timestampText) {
    final SkyDiningPackage entity = packageRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final String savePath = "sky-dining-admin/packages";
    String imageHash;
    try {
      imageHash = JcrFileUtil.copyFile(image, savePath, fileName);
    } catch (final Exception e) {
      log.error("Error uploading sky dining package image file.", e);
      return new SaveResult(SaveResult.Result.Failed);
    }

    timestamp = new Date();

    entity.setImageHash(imageHash);
    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      packageRepo.save(entity);
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp, imageHash);
  }

  @Override
  public SaveResult savePackage(int id, String newName,
                                String newEarlyBirdEndDate, String newStartDate, String newEndDate,
                                Integer newSalesCutOffDays, Integer newCapacity,
                                String newDescription, String newLink, Integer newDisplayOrder,
                                Integer newMinPax, Integer newMaxPax, String newImageHash,
                                String newNotes, Integer newSelectedTncId, String userId,
                                String timestampText) {

    SkyDiningPackage entity = packageRepo.find(id);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (entity == null) {
      timestamp = new Date();
      entity = new SkyDiningPackage();
      entity.setId(id);
      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
    } else {
      if (!isTimestampValid(entity, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
      timestamp = new Date();
    }

    if (newName != null) {
      entity.setName(newName);
    }
    if (newEarlyBirdEndDate != null) {
      entity.setEarlyBirdEndDate(parseDate(newEarlyBirdEndDate));
    }
    if (newStartDate != null) {
      entity.setStartDate(parseDate(newStartDate));
    }
    if (newEndDate != null) {
      entity.setEndDate(parseDate(newEndDate));
    }
    if (newSalesCutOffDays != null) {
      entity.setSalesCutOffDays(newSalesCutOffDays);
    }
    if (newCapacity != null) {
      entity.setCapacity(newCapacity);
    }
    if (newDescription != null) {
      entity.setDescription(newDescription);
    }
    if (newLink != null) {
      entity.setLink(newLink);
    }
    if (newDisplayOrder != null) {
      entity.setDisplayOrder(newDisplayOrder);
    }
    if (newMinPax != null) {
      entity.setMinPax(newMinPax);
    }
    if (newMaxPax != null) {
      entity.setMaxPax(newMaxPax);
    }
    if (newImageHash != null) {
      entity.setImageHash(newImageHash);
    }
    if (newNotes != null) {
      entity.setNotes(newNotes);
    }
    if (newSelectedTncId != null) {
      entity.setSelectedTncId(newSelectedTncId);
    }

    entity.setModifiedBy(userId);
    entity.setModifiedDate(timestamp);

    try {
      if (packageRepo.save(entity)) {
        return new SaveResult(SaveResult.Result.Success, timestamp, entity.getId());
      }
    } catch (JcrException e) {
      log.error("Unable to save package.", e);
      return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
    }

    return new SaveResult(SaveResult.Result.Success, timestamp);
  }

  @Override
  public SaveResult savePromoCode(int parentId, int id, String newCode,
                                  Boolean newHasLimit, Integer newLimit, Integer newTimesUsed,
                                  Boolean newActive, String newType, String newStartDate,
                                  String newEndDate, String userId, String timestampText) {
    final SkyDiningDiscount parent = discountRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (parent == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining discount.");
    } else {
      if (!isTimestampValid(parent, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    final List<SkyDiningPromoCode> promoCodes = promoCodeRepo
      .findByProperty("code", newCode);
    for (final Iterator<SkyDiningPromoCode> pcIterator = promoCodes
      .iterator(); pcIterator.hasNext(); ) {
      final SkyDiningPromoCode iteratedPc = pcIterator.next();
      if (BooleanUtils.isFalse(iteratedPc.getActive())
        || (iteratedPc.getId() == id)) {
        pcIterator.remove();
      }
    }
    if (!promoCodes.isEmpty()) {
      final StringBuilder otherInfo = new StringBuilder();
      otherInfo.append("Duplicate promo code detected.");

//      final SkyDiningPromoCode promoCode = promoCodes.get(0);
//      final SkyDiningDiscount discount = promoCode.getDiscount();
//      final SkyDiningPackage sdPackage = discount.getSdPackage();
//
//      otherInfo.append(" (");
//      otherInfo.append(sdPackage.getName());
//      otherInfo.append(" -> ");
//      otherInfo.append(discount.getName());
//      otherInfo.append(")");

      otherInfo.append(" Please enter a different promo code.");
      return new SaveResult(SaveResult.Result.Failed, otherInfo.toString());
    }

    timestamp = new Date();


    // If New Promo Code
    if (id < 1) {
      final SkyDiningPromoCode entity = new SkyDiningPromoCode();
      entity.setId(0);
      if (newCode != null) {
        entity.setCode(newCode);
      }
      if (newHasLimit != null) {
        entity.setHasLimit(newHasLimit);
      }
      if (newLimit != null) {
        entity.setLimit(newLimit);
      }
      if (newTimesUsed != null) {
        entity.setTimesUsed(newTimesUsed);
      }
      if (newActive != null) {
        entity.setActive(newActive);
      }
      if (newType != null) {
        entity.setType(newType);
      }
      if (StringUtils.isNotEmpty(newStartDate)) {
        try {
          entity.setStartDate(NvxDateUtils.parseDate(newStartDate,
            SysConst.DEFAULT_DATE_FORMAT));
        } catch (final ParseException e) {
          log.error("Unable to parse date: " + newStartDate, e);
        }
      }
      if (StringUtils.isNotEmpty(newEndDate)) {
        try {
          entity.setEndDate(NvxDateUtils.parseDate(newEndDate,
            SysConst.DEFAULT_DATE_FORMAT));
        } catch (final ParseException e) {
          log.error("Unable to parse date: " + newEndDate, e);
        }
      }

      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
      entity.setModifiedBy(userId);
      entity.setModifiedDate(timestamp);

      entity.setDiscount(parent);

      parent.getPromoCodes().add(entity);
      parent.setModifiedBy(userId);
      parent.setModifiedDate(timestamp);

      try {
        discountRepo.save(parent);
      } catch (JcrException e) {
        log.error("Unable to save promo code.", e);
        return new SaveResult(SaveResult.Result.Failed, "Unable to save promo code.");
      }

      return new SaveResult(SaveResult.Result.Success, timestamp, entity.getId());
    }

    // If Not New
    final List<SkyDiningPromoCode> entities = parent.getPromoCodes();
    for (final SkyDiningPromoCode entity : entities) {
      if (id == entity.getId()) {
        if (newCode != null) {
          entity.setCode(newCode);
        }
        if (newHasLimit != null) {
          entity.setHasLimit(newHasLimit);
        }
        if (newLimit != null) {
          entity.setLimit(newLimit);
        }
        if (newTimesUsed != null) {
          entity.setTimesUsed(newTimesUsed);
        }
        if (newActive != null) {
          entity.setActive(newActive);
        }
        if (newType != null) {
          entity.setType(newType);
        }
        if (StringUtils.isNotEmpty(newStartDate)) {
          try {
            entity.setStartDate(NvxDateUtils.parseDate(newStartDate,
              SysConst.DEFAULT_DATE_FORMAT));
          } catch (final ParseException e) {
            log.error("Unable to parse date: " + newStartDate, e);
          }
        }
        if (StringUtils.isNotEmpty(newEndDate)) {
          try {
            entity.setEndDate(NvxDateUtils.parseDate(newEndDate,
              SysConst.DEFAULT_DATE_FORMAT));
          } catch (final ParseException e) {
            log.error("Unable to parse date: " + newEndDate, e);
          }
        }

        entity.setModifiedBy(userId);
        entity.setModifiedDate(timestamp);
        parent.setModifiedBy(userId);
        parent.setModifiedDate(timestamp);
        try {
          discountRepo.save(parent);
        } catch (JcrException e) {
          log.error("Unable to save promo code.", e);
          return new SaveResult(SaveResult.Result.Failed, "Unable to save promo code.");
        }

        return new SaveResult(SaveResult.Result.Success, timestamp);
      }
    }

    return new SaveResult(SaveResult.Result.Failed, "Unable to save promo code.");
  }

  @Override
  public SaveResult saveTheme(int parentId, int id, File image,
                              String filename, String newDescription, Integer newCapacity,
                              String userId, String timestampText) {

    final SkyDiningPackage parent = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (parent == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(parent, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    timestamp = new Date();
    if (id < 1) {
      final SkyDiningTheme entity = new SkyDiningTheme();
      entity.setId(0);

      if (StringUtils.isNotEmpty(filename) && (image != null)) {
        final String savePath = "sky-dining-admin/themes";
        String imageHash;
        try {
          imageHash = JcrFileUtil.copyFile(image, savePath, filename);
        } catch (final Exception e) {
          log.error("Error uploading sky dining theme image file.", e);
          return new SaveResult(SaveResult.Result.Failed);
        }
        entity.setImageHash(imageHash);
      }

      if (newDescription != null) {
        entity.setDescription(newDescription);
      }
      if (newCapacity != null) {
        entity.setCapacity(newCapacity);
      }

      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
      entity.setModifiedBy(userId);
      entity.setModifiedDate(timestamp);

      parent.getThemes().add(entity);
      parent.setModifiedBy(userId);
      parent.setModifiedDate(timestamp);
      try {
        packageRepo.save(parent);
      } catch (JcrException e) {
        log.error("Unable to save package.", e);
        return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
      }

      return new SaveResult(SaveResult.Result.Success, timestamp, entity.getId(),
        entity.getImageHash());
    }

    final List<SkyDiningTheme> entities = parent.getThemes();
    for (final SkyDiningTheme entity : entities) {
      if (id == entity.getId()) {
        if (StringUtils.isNotEmpty(filename) && (image != null)) {
          final String savePath = "sky-dining-admin/themes";
          String imageHash;
          try {
            imageHash = JcrFileUtil.copyFile(image, savePath, filename);
          } catch (final Exception e) {
            log.error(
              "Error uploading sky dining theme image file.",
              e);
            return new SaveResult(SaveResult.Result.Failed);
          }
          entity.setImageHash(imageHash);
        }

        if (newDescription != null) {
          entity.setDescription(newDescription);
        }
        if (newCapacity != null) {
          entity.setCapacity(newCapacity);
        }

        entity.setModifiedBy(userId);
        entity.setModifiedDate(timestamp);
        parent.setModifiedBy(userId);
        parent.setModifiedDate(timestamp);
        try {
          packageRepo.save(parent);
        } catch (JcrException e) {
          log.error("Unable to save package.", e);
          return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
        }

        return new SaveResult(SaveResult.Result.Success, timestamp, entity.getImageHash());
      }
    }

    return new SaveResult(SaveResult.Result.Failed,
      "Unable to find sky dining terms and conditions.");

  }

  @Override
  public SaveResult saveTnc(int parentId, int id, String newTitle,
                            String newTncContent, Boolean newSelected, String userId,
                            String timestampText) {

    final SkyDiningPackage parent = packageRepo.find(parentId);

    Date timestamp = null;
    if (StringUtils.isNotEmpty(timestampText)) {
      timestamp = NvxDateUtils.parseModDt(timestampText);
    }

    if (parent == null) {
      return new SaveResult(SaveResult.Result.Failed,
        "Unable to find sky dining package.");
    } else {
      if (!isTimestampValid(parent, timestamp)) {
        return new SaveResult(SaveResult.Result.StaleData);
      }
    }

    timestamp = new Date();
    if (id < 1) {
      final SkyDiningTnc entity = new SkyDiningTnc();
      entity.setId(0);
      if (newTitle != null) {
        entity.setTitle(newTitle);
      }
      if (newTncContent != null) {
        entity.setTncContent(newTncContent);
      }
      if (newSelected != null) {
        entity.setSelected(newSelected);
      }

      entity.setCreatedBy(userId);
      entity.setCreatedDate(timestamp);
      entity.setModifiedBy(userId);
      entity.setModifiedDate(timestamp);

      parent.getTncs().add(entity);
      parent.setModifiedBy(userId);
      parent.setModifiedDate(timestamp);
      try {
        packageRepo.save(parent);
      } catch (JcrException e) {
        log.error("Unable to save package.", e);
        return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
      }

      return new SaveResult(SaveResult.Result.Success, timestamp, entity.getId());
    }

    final List<SkyDiningTnc> entities = parent.getTncs();
    for (final SkyDiningTnc entity : entities) {
      if (id == entity.getId()) {
        if (newTitle != null) {
          entity.setTitle(newTitle);
        }
        if (newTncContent != null) {
          entity.setTncContent(newTncContent);
        }
        if (newSelected != null) {
          entity.setSelected(newSelected);
        }

        entity.setModifiedBy(userId);
        entity.setModifiedDate(timestamp);
        parent.setModifiedBy(userId);
        parent.setModifiedDate(timestamp);
        try {
          packageRepo.save(parent);
        } catch (JcrException e) {
          log.error("Unable to save package.", e);
          return new SaveResult(SaveResult.Result.Failed, "Unable to save package.");
        }

        return new SaveResult(SaveResult.Result.Success, timestamp);
      }
    }

    return new SaveResult(SaveResult.Result.Failed,
      "Unable to find sky dining terms and conditions.");
  }

  @Override
  public List<MerchRow> doGetMerchants(boolean activeOnly) {
    final List<Merchant> ms = merchantRepo.getAll("b2c-mflg", activeOnly, null);
    final List<MerchRow> rows = new ArrayList<>();
    for (Merchant m : ms) {
      final MerchRow row = new MerchRow(m.getId(), m.getName(),
        m.isActive(), m.getName(), m.getMerchantId());
      rows.add(row);
    }
    return rows;
  }
}
