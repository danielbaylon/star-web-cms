package com.enovax.star.cms.skydining.model.api;

import com.enovax.star.cms.skydining.constant.ApiErrorCodes;
import com.enovax.star.cms.commons.util.ProjectUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by jennylynsze on 5/12/16.
 */
public class ApiResult<T> {
  private boolean success = false;
  private String errorCode = "";
  private String message = "";
  private int total = 0;
  private T data = null;

  public ApiResult() {

  }

  public ApiResult(boolean success, String errorCode, String message, T data) {
    this.success = success;
    this.errorCode = errorCode;
    this.message = message;
    this.data = data;
  }

  public ApiResult(boolean success, String errorCode, String message, int total, T data) {
    this(success, errorCode, message, data);
    this.total = total;
  }

  public ApiResult(ApiErrorCodes err) {
    this(err, "", "");
  }

  public ApiResult(ApiErrorCodes err, String additionalMessage) {
    this(err, additionalMessage, "");
  }

  public ApiResult(ApiErrorCodes err, String additionalMessage, String logId) {
    this.success = false;
    this.errorCode = err.code;

    final String msg = err.message + (StringUtils.isEmpty(additionalMessage) ? "" : " " + additionalMessage);
    this.message = StringUtils.isEmpty(logId) ? msg : ProjectUtils.appendLogId(msg, logId);
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }
}
