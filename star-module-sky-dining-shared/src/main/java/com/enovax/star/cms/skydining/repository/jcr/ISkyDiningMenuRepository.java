package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningDiscount;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMenu;
import com.enovax.util.jcr.repository.JcrRepository;
import info.magnolia.jcr.util.ContentMap;

import java.util.List;

/**
 * Created by jace on 28/7/16.
 */
public interface ISkyDiningMenuRepository extends JcrRepository<SkyDiningMenu> {

    int count(String name, Boolean active, Integer packageId,
              Integer discountId, List<Integer> menuTypes);

    List<SkyDiningMenu> findPaged(String name, Boolean active,
                                  Integer packageId, Integer discountId, List<Integer> menuTypes,
                                  int start, int pageSize, String orderBy, boolean asc);

    ContentMap findDiscountedContent(SkyDiningDiscount discount, int menuId);
}
