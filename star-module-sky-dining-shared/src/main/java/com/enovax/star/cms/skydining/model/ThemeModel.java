package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.skydining.datamodel.SkyDiningTheme;

public class ThemeModel extends SkyDiningTheme {

  public ThemeModel(SkyDiningTheme dataModel) {
    setId(dataModel.getId());
    setDescription(dataModel.getDescription());
    setImageHash(dataModel.getImageHash());
    setCapacity(dataModel.getCapacity());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());
  }

}
