package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningTopUp;

public class TopUpModel extends SkyDiningTopUp {

  private String earlyBirdEndDateText;
  private String startDateText;
  private String endDateText;
  private String timestamp;

  public TopUpModel(SkyDiningTopUp dataModel) {
    setId(dataModel.getId());
    setName(dataModel.getName());
    setType(dataModel.getType());
    setDescription(dataModel.getDescription());
    setEarlyBirdEndDate(dataModel.getEarlyBirdEndDate());
    setStartDate(dataModel.getStartDate());
    setEndDate(dataModel.getEndDate());
    setSalesCutOffDays(dataModel.getSalesCutOffDays());
    setPrice(dataModel.getPrice());
    setImageHash(dataModel.getImageHash());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());

    if (getStartDate() != null) {
      startDateText = NvxDateUtils.formatDate(getStartDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEndDate() != null) {
      endDateText = NvxDateUtils.formatDate(getEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getEarlyBirdEndDate() != null) {
      earlyBirdEndDateText = NvxDateUtils.formatDate(getEarlyBirdEndDate(),
        SysConst.DEFAULT_DATE_FORMAT);
    }
    if (getModifiedDate() != null) {
      timestamp = NvxDateUtils.formatModDt(getModifiedDate());
    }

    setDisplayOrder(dataModel.getDisplayOrder());
  }

  public String getEarlyBirdEndDateText() {
    return earlyBirdEndDateText;
  }

  public String getEndDateText() {
    return endDateText;
  }

  public String getStartDateText() {
    return startDateText;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setEarlyBirdEndDateText(String earlyBirdEndDateText) {
    this.earlyBirdEndDateText = earlyBirdEndDateText;
  }

  public void setEndDateText(String endDateText) {
    this.endDateText = endDateText;
  }

  public void setStartDateText(String startDateText) {
    this.startDateText = startDateText;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

}
