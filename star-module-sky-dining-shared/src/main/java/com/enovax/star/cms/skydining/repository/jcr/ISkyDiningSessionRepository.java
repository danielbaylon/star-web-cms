package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningSession;
import com.enovax.util.jcr.repository.JcrRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by jace on 20/7/16.
 */
public interface ISkyDiningSessionRepository extends JcrRepository<SkyDiningSession> {

  List<SkyDiningSession> findAvailable(Date dateOfBooking, Date dateOfVisit,
                                       Integer packageId);

  List<SkyDiningSession> findPaged(int start, int pageSize, String orderBy,
                                   boolean asc);

}
