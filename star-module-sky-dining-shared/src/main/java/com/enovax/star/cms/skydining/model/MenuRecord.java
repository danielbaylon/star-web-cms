package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxNumberUtils;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMenu;

import java.io.Serializable;
import java.util.Date;

public class MenuRecord implements Serializable {

  private int id;
  private String name;
  private String price;
  private String status;

  private Integer priceInCents;
  private Integer gst;
  private Integer serviceCharge;

  public MenuRecord(SkyDiningMenu skyDiningMenu) {
    id = skyDiningMenu.getId();
    name = skyDiningMenu.getName();

    if (skyDiningMenu.getPrice() != null) {
      price = NvxNumberUtils.formatToCurrency(
        NvxNumberUtils.centsToMoney(skyDiningMenu.getPrice()), "$");
    }

    priceInCents = skyDiningMenu.getPrice();
    gst = skyDiningMenu.getGst();
    serviceCharge = skyDiningMenu.getServiceCharge();

    if ((skyDiningMenu.getStartDate() != null)
      && (skyDiningMenu.getEndDate() != null)) {
      final Date now = new Date();
      if (now.after(skyDiningMenu.getStartDate())
        && now.before(skyDiningMenu.getEndDate())) {
        status = "Active";
      } else {
        status = "Inactive";
      }
    } else {
      status = "Inactive";
    }
  }

  public Integer getGst() {
    return gst;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getPrice() {
    return price;
  }

  public Integer getPriceInCents() {
    return priceInCents;
  }

  public Integer getServiceCharge() {
    return serviceCharge;
  }

  public String getStatus() {
    return status;
  }

  public void setGst(Integer gst) {
    this.gst = gst;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public void setPriceInCents(Integer priceInCents) {
    this.priceInCents = priceInCents;
  }

  public void setServiceCharge(Integer serviceCharge) {
    this.serviceCharge = serviceCharge;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}
