package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningTheme;
import com.enovax.util.jcr.repository.BaseJcrRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jace on 17/8/16.
 */
@Repository
public class DefaultSkyDiningThemeRepository extends BaseJcrRepository<SkyDiningTheme> implements ISkyDiningThemeRepository {
  public static final String JCR_WORKSPACE = "sky-dining-admin";
  public static final String JCR_PATH = null;
  public static final String PRIMARY_NODE_TYPE = "mgnl:sky-dining-theme";

  @Override
  protected String getJcrWorkspace() {
    return JCR_WORKSPACE;
  }

  @Override
  protected String getJcrPath() {
    return JCR_PATH;
  }

  @Override
  protected SkyDiningTheme getNewInstance() {
    return new SkyDiningTheme();
  }

  @Override
  protected String getPrimaryNodeType() {
    return PRIMARY_NODE_TYPE;
  }
}
