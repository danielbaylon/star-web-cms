package com.enovax.star.cms.skydining.service;

import com.enovax.star.cms.skydining.model.MainCourseModel;
import com.enovax.star.cms.skydining.model.MenuModel;
import com.enovax.star.cms.skydining.model.MenuRecord;
import com.enovax.star.cms.skydining.model.SaveResult;

import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningMenuService {
  SaveResult addMainCoursesToMenu(int parentId, List<Integer> ids,
                                  String userId, String timestampText);

  int countMenuRecords(String name, Boolean active);

  int countSelectMainCourseRecords(Integer parentId);

  SaveResult deleteMainCourse(int id, String userId, String timestampText);

  List<MainCourseModel> getMainCourses(int parentId);

  MenuModel getMenu(int id);

  List<MenuRecord> getMenuRecords(String name, Boolean active, int start,
                                  int pageSize, String orderBy, boolean asc);

  List<MainCourseModel> getSelectMainCourseRecords(Integer parentId,
                                                   int start, int pageSize, String orderBy, boolean asc);

  SaveResult removeMainCourses(int parentId, List<Integer> ids,
                               String userId, String timestampText);

  SaveResult saveMainCourse(int parentId, int id, String newCode,
                            Integer newType, String newDescription, String userId,
                            String timestampText);

  SaveResult saveMenu(int id, String newName, String newDescription,
                      String newEarlyBirdEndDate, String newStartDate, String newEndDate,
                      Integer newSalesCutOffDays, Integer newPrice,
                      Integer newServiceCharge, Integer newMenuType, Integer newGst,
                      String userId, String timestampText);
}
