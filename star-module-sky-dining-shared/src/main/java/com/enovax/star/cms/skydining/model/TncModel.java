package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.skydining.datamodel.SkyDiningTnc;

public class TncModel extends SkyDiningTnc {

  public TncModel(SkyDiningTnc dataModel) {
    setId(dataModel.getId());
    setTitle(dataModel.getTitle());
    setTncContent(dataModel.getTncContent());
    setSelected(dataModel.getSelected());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());
  }

}
