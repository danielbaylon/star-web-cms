package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.skydining.constant.MainCourseType;
import com.enovax.star.cms.skydining.datamodel.SkyDiningMainCourse;

public class MainCourseModel extends SkyDiningMainCourse {

  private String typeText;

  public MainCourseModel(SkyDiningMainCourse dataModel) {
    setId(dataModel.getId());
    setCode(dataModel.getCode());
    setType(dataModel.getType());
    setDescription(dataModel.getDescription());
    setCreatedBy(dataModel.getCreatedBy());
    setCreatedDate(dataModel.getCreatedDate());
    setModifiedBy(dataModel.getModifiedBy());
    setModifiedDate(dataModel.getModifiedDate());

    if (dataModel.getType() == null) {
      setTypeText("-");
    } else if (dataModel.getType().equals(MainCourseType.Kids.value)) {
      setTypeText("Kids");
    } else if (dataModel.getType().equals(MainCourseType.Vegetarian.value)) {
      setTypeText("Vegetarian");
    } else {
      setTypeText("Standard");
    }
  }

  public String getTypeText() {
    return typeText;
  }

  public void setTypeText(String typeText) {
    this.typeText = typeText;
  }

}
