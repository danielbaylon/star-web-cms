package com.enovax.star.cms.skydining.model.params;

/**
 * Created by jace on 7/10/16.
 */
public class SharedConfigurationActionParam extends BaseSkyDiningActionParam {

    private String searchMemberCard;
    private Integer jcId;
    private String jcCardNumber;
    private String jcExpiry;
    private String deleteJcIds;

    private String searchJcFrom;
    private String searchJcTo;

    public String getSearchMemberCard() {
        return searchMemberCard;
    }

    public void setSearchMemberCard(String searchMemberCard) {
        this.searchMemberCard = searchMemberCard;
    }

    public Integer getJcId() {
        return jcId;
    }

    public void setJcId(Integer jcId) {
        this.jcId = jcId;
    }

    public String getJcCardNumber() {
        return jcCardNumber;
    }

    public void setJcCardNumber(String jcCardNumber) {
        this.jcCardNumber = jcCardNumber;
    }

    public String getJcExpiry() {
        return jcExpiry;
    }

    public void setJcExpiry(String jcExpiry) {
        this.jcExpiry = jcExpiry;
    }

    public String getDeleteJcIds() {
        return deleteJcIds;
    }

    public void setDeleteJcIds(String deleteJcIds) {
        this.deleteJcIds = deleteJcIds;
    }

    public String getSearchJcFrom() {
        return searchJcFrom;
    }

    public void setSearchJcFrom(String searchJcFrom) {
        this.searchJcFrom = searchJcFrom;
    }

    public String getSearchJcTo() {
        return searchJcTo;
    }

    public void setSearchJcTo(String searchJcTo) {
        this.searchJcTo = searchJcTo;
    }
}
