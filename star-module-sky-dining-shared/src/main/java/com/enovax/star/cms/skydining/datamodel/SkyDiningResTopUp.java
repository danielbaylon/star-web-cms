package com.enovax.star.cms.skydining.datamodel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(catalog = "STAR_DB", name = "SkyDiningResTopUp", schema = "dbo")
public class SkyDiningResTopUp implements HasTimestamp {

  private int id;
  private int quantity;
  private String createdBy;
  private Date createdDate;
  private String modifiedBy;
  private Date modifiedDate;

  private Integer topUpId;

  @Override
  @Column(name = "createdBy", nullable = false, length = 100)
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  @Column(name = "createdDate", nullable = false)
  public Date getCreatedDate() {
    return createdDate;
  }

  @Override
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  public int getId() {
    return id;
  }

  @Override
  @Column(name = "modifiedBy", nullable = false, length = 100)
  public String getModifiedBy() {
    return modifiedBy;
  }

  @Override
  @Column(name = "modifiedDate", nullable = false)
  public Date getModifiedDate() {
    return modifiedDate;
  }

  @Column(name = "quantity", nullable = false)
  public int getQuantity() {
    return quantity;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  @Override
  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Column(name = "topUpId", nullable = false)
  public Integer getTopUpId() {
    return topUpId;
  }

  public void setTopUpId(Integer topUpId) {
    this.topUpId = topUpId;
  }
}
