package com.enovax.star.cms.skydining.model;

import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningSession;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;


public class SessionRecord implements Serializable {

  private int id;
  private String name;
  private String sessionNumber;
  private String nameWithSessionNumber;
  private String schedule;
  private String time;
  private String status;
  private String timestamp;

  public SessionRecord(SkyDiningSession session) {
    id = session.getId();
    name = session.getName();
    if (session.getSessionNumber() == 0) {
      sessionNumber = "Default";
      nameWithSessionNumber = name;
    } else {
      sessionNumber = "Session " + session.getSessionNumber();
      nameWithSessionNumber = name + " - " + sessionNumber;
    }

    schedule = generateScheduleText(session);

    time = session.getTime();

    if (session.getActive()) {
      status = "Active";
    } else {
      status = "Inactive";
    }

    if (session.getModifiedDate() != null) {
      timestamp = NvxDateUtils.formatModDt(session.getModifiedDate());
    }
  }

  private String generateScheduleText(SkyDiningSession session) {
    final StringBuilder sb = new StringBuilder();
    switch (session.getSessionType()) {
      case (0):
        sb.append(NvxDateUtils.formatDate(session.getStartDate(),
          SysConst.DEFAULT_DATE_FORMAT));
        if (!session.getStartDate().equals(session.getEndDate())) {
          sb.append(" to ");
          sb.append(NvxDateUtils.formatDate(session.getEndDate(),
            SysConst.DEFAULT_DATE_FORMAT));
        }
        break;
      case (1):
        if (StringUtils.isNotEmpty(session.getEffectiveDays())) {
          if (session.getEffectiveDays().indexOf('1') > -1) {
            sb.append("Mon");
          }
          if (session.getEffectiveDays().indexOf('2') > -1) {
            if (sb.length() > 0) {
              sb.append(", ");
            }
            sb.append("Tues");
          }
          if (session.getEffectiveDays().indexOf('3') > -1) {
            if (sb.length() > 0) {
              sb.append(", ");
            }
            sb.append("Wed");
          }
          if (session.getEffectiveDays().indexOf('4') > -1) {
            if (sb.length() > 0) {
              sb.append(", ");
            }
            sb.append("Thurs");
          }
          if (session.getEffectiveDays().indexOf('5') > -1) {
            if (sb.length() > 0) {
              sb.append(", ");
            }
            sb.append("Fri");
          }
          if (session.getEffectiveDays().indexOf('6') > -1) {
            if (sb.length() > 0) {
              sb.append(", ");
            }
            sb.append("Sat");
          }
          if (session.getEffectiveDays().indexOf('7') > -1) {
            if (sb.length() > 0) {
              sb.append(", ");
            }
            sb.append("Sun");
          }
        } else {
          sb.append("-");
        }
        break;
      default:
        sb.append("-");
        break;
    }
    return sb.toString();
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getNameWithSessionNumber() {
    return nameWithSessionNumber;
  }

  public String getSchedule() {
    return schedule;
  }

  public String getSessionNumber() {
    return sessionNumber;
  }

  public String getStatus() {
    return status;
  }

  public String getTime() {
    return time;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setNameWithSessionNumber(String nameWithSessionNumber) {
    this.nameWithSessionNumber = nameWithSessionNumber;
  }

  public void setSchedule(String schedule) {
    this.schedule = schedule;
  }

  public void setSessionNumber(String session) {
    sessionNumber = session;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

}
