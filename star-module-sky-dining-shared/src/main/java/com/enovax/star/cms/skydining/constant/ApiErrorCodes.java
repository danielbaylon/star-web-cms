package com.enovax.star.cms.skydining.constant;

public enum ApiErrorCodes {

  //General
  UnexpectedSystemException("UnexpectedSystemException", "Unexpected system error encountered. "),
  General("GeneralError", "A general system error occurred."),

  //Partner Account
  LoginAuthenticationFailed("LoginAuthenticationFailed", "You have entered the wrong password. Please try again."),
  LoginUnknownAccount("LoginUnknownAccount", "The account does not exist in the system."),
  LoginInactiveAccount("LoginInactiveAccount", "Your account is inactive."),
  LoginLockedAccount("LoginLockedAccount", "Your account is locked."),
  LoginExcessConcurrentLogins("LoginExcessConcurrentLogins", "Account exceeded the maximum amount of concurrent logins."),

  NotLoggedIn("NotLoggedIn", "You are not logged in."),;

  public final String code;
  public final String message;

  ApiErrorCodes(String code, String message) {
    this.code = code;
    this.message = message;
  }
}
