package com.enovax.star.cms.skydining.repository.jcr;

import com.enovax.star.cms.skydining.datamodel.SkyDiningMainCourse;
import com.enovax.util.jcr.repository.JcrRepository;

import java.util.List;

/**
 * Created by jace on 29/7/16.
 */
public interface ISkyDiningMainCourseRepository extends JcrRepository<SkyDiningMainCourse> {
  int count(Integer parentId, Boolean deleted);

  List<SkyDiningMainCourse> findPaged(Integer parentId, Boolean deleted,
                                      int start, int pageSize, String orderBy, boolean asc);
}
