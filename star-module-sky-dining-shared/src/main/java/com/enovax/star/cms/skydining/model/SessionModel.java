package com.enovax.star.cms.skydining.model;


import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.skydining.constant.SysConst;
import com.enovax.star.cms.skydining.datamodel.SkyDiningSession;

public class SessionModel extends SkyDiningSession {

  private String startDateText;
  private String endDateText;
  private String timeText;
  private String salesStartDateText;
  private String salesEndDateText;
  private String timestamp;

  public SessionModel(SkyDiningSession dataModel) {
    if (dataModel != null) {
      setId(dataModel.getId());
      setName(dataModel.getName());
      setSessionNumber(dataModel.getSessionNumber());
      setSessionType(dataModel.getSessionType());
      setStartDate(dataModel.getStartDate());
      setEndDate(dataModel.getEndDate());
      setTime(dataModel.getTime());
      setSalesStartDate(dataModel.getSalesStartDate());
      setSalesEndDate(dataModel.getSalesEndDate());
      setCapacity(dataModel.getCapacity());
      setActive(dataModel.getActive());
      setCreatedBy(dataModel.getCreatedBy());
      setCreatedDate(dataModel.getCreatedDate());
      setModifiedBy(dataModel.getModifiedBy());
      setModifiedDate(dataModel.getModifiedDate());
      setEffectiveDays(dataModel.getEffectiveDays());

      if (getStartDate() != null) {
        startDateText = NvxDateUtils.formatDate(getStartDate(),
          SysConst.DEFAULT_DATE_FORMAT);
      }
      if (getEndDate() != null) {
        endDateText = NvxDateUtils.formatDate(getEndDate(),
          SysConst.DEFAULT_DATE_FORMAT);
      }
      if (getTime() != null) {
        timeText = getTime();
      }
      if (getSalesStartDate() != null) {
        salesStartDateText = NvxDateUtils.formatDate(getSalesStartDate(),
          SysConst.DEFAULT_DATE_FORMAT);
      }
      if (getSalesEndDate() != null) {
        salesEndDateText = NvxDateUtils.formatDate(getSalesEndDate(),
          SysConst.DEFAULT_DATE_FORMAT);
      }
      if (getModifiedDate() != null) {
        timestamp = NvxDateUtils.formatModDt(getModifiedDate());
      }
    }
  }

  public String getEndDateText() {
    return endDateText;
  }

  public String getSalesEndDateText() {
    return salesEndDateText;
  }

  public String getSalesStartDateText() {
    return salesStartDateText;
  }

  public String getStartDateText() {
    return startDateText;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getTimeText() {
    return timeText;
  }

  public void setEndDateText(String endDateText) {
    this.endDateText = endDateText;
  }

  public void setSalesEndDateText(String salesEndDateText) {
    this.salesEndDateText = salesEndDateText;
  }

  public void setSalesStartDateText(String salesStartDateText) {
    this.salesStartDateText = salesStartDateText;
  }

  public void setStartDateText(String startDateText) {
    this.startDateText = startDateText;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public void setTimeText(String timeText) {
    this.timeText = timeText;
  }

}
