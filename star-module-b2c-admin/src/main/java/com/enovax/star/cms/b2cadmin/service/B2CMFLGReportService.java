package com.enovax.star.cms.b2cadmin.service;

import com.enovax.star.cms.b2cadmin.model.TransactionReportRequest;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionDetailsReportRow;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionSummaryReportRow;
import com.enovax.star.cms.b2cadmin.repository.B2CMFLGReportDao;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 4/11/16.
 */
@Service
public class B2CMFLGReportService extends BaseB2CReportService {

    @Autowired
    private B2CMFLGReportDao reportDao;

    public ApiResult<List> doTransactionDetailsReportSearch(TransactionReportRequest gridRequest, TransactionReportFilter filter) throws Exception {
        initGridOrdering(gridRequest, "createdDate", true);

        int total = reportDao.getTransactionRecordCount(filter);
        if (total > 0) {
            List<TransactionDetailsReportRow> txnRptList = reportDao.getTransactionRecords(filter, gridRequest.getSkip(), gridRequest.getTake(),
                gridRequest.getOrderBy(), gridRequest.isAsc());
            return new ApiResult<>(true, "", "", total, txnRptList);
        }

        return new ApiResult<>(true, "", "", total, new ArrayList<TransactionDetailsReportRow>());
    }

    public Workbook doTransactionDetailsReportExportExcel(TransactionReportFilter filter) throws Exception {
        Workbook wb = new XSSFWorkbook();
        Sheet s = wb.createSheet("Transaction Detail Report");
        Row r = null;
        Cell c = null;

        //center
        CellStyle cs = wb.createCellStyle();
        cs.setAlignment((short) 2);

        //right
        CellStyle cs2 = wb.createCellStyle();
        cs2.setAlignment((short) 3);

        //middle
        CellStyle cs3 = wb.createCellStyle();
        cs3.setVerticalAlignment((short) 1);

        //middle right
        CellStyle cs4 = wb.createCellStyle();
        cs4.setVerticalAlignment((short) 1);
        cs4.setAlignment((short) 3);

        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        CellStyle cs2f = wb.createCellStyle();
        cs2f.setAlignment((short) 3);
        cs2f.setFont(font);

        CellStyle csf = wb.createCellStyle();
        csf.setFont(font);

        final DataFormat df = s.getWorkbook().createDataFormat();
        CellStyle dateTimeCell = wb.createCellStyle();
        dateTimeCell.setDataFormat(df.getFormat("dd/mm/yy hh:mm:ss"));

        CellStyle dateCell = wb.createCellStyle();
        dateCell.setDataFormat(df.getFormat("dd/mm/yy"));

        CellStyle moneyCell = wb.createCellStyle();
        moneyCell.setDataFormat(df.getFormat("#,##0.00"));

        r = s.createRow(0);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Transaction Start Date: " + NvxDateUtils.formatDateForDisplay(filter.getFromDate(), false));
        c.setCellStyle(cs3);

        r = s.createRow(1);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Transaction End Date: " + NvxDateUtils.formatDateForDisplay(filter.getToDate(), false));
        c.setCellStyle(cs3);

        r = s.createRow(2);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Status: " + (filter.getStatus().equals("")? "ALL" : filter.getStatus()));
        c.setCellStyle(cs3);

        r = s.createRow(3);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Receipt Number: " + filter.getRecNum());
        c.setCellStyle(cs3);

        r = s.createRow(4);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Product Name: " + filter.getProName());
        c.setCellStyle(cs3);

        r = s.createRow(5);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Date of Visit: " + (filter.getDov() == null ? "" : NvxDateUtils.formatDateForDisplay(filter.getDov(), false)));
        c.setCellStyle(cs3);

        r = s.createRow(6);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Promo Code: " + filter.getProCode());
        c.setCellStyle(cs3);

        r = s.createRow(7);
        r.setHeight((short) 300);

        c=r.createCell(0);
        c.setCellValue("Receipt Number");
        c=r.createCell(1);
        s.setColumnWidth(1, (14 * 256) + 200);
        c.setCellValue("Transaction Date");
        c=r.createCell(2);
        c.setCellValue("Payment Type");
        c=r.createCell(3);
        c.setCellValue("Pin Code");
        c=r.createCell(4);
        c.setCellValue("Item Name");
        c=r.createCell(5);
        c.setCellValue("Item Code");
        c=r.createCell(6);
        c.setCellValue("Quantity");
        c=r.createCell(7);
        c.setCellStyle(cs2);
        c.setCellValue("Subtotal (S$)");
        c=r.createCell(8);
        c.setCellStyle(cs2);
        c.setCellValue("Status");
        c=r.createCell(9);
        c.setCellValue("Promo Code");
        c=r.createCell(10);
        c.setCellValue("Date Of Visit");
        c=r.createCell(11);
        c.setCellValue("Customer Name");
        c=r.createCell(12);
        c.setCellValue("Email");
        c=r.createCell(13);
        c.setCellValue("Contact No.");

        int total = reportDao.getTransactionRecordCount(filter);
        if (total > 0) {
            List<TransactionDetailsReportRow> txnRptList = reportDao.getTransactionRecords(filter, 0, 9999,
                "createdDate", true);
            int i = 1;
            for (TransactionDetailsReportRow trr : txnRptList) {
                r = s.createRow(i + 7);
                r.setHeight((short) 300);

                c = r.createCell(0);
                c.setCellValue(trr.getReceiptNum());

                c = r.createCell(1);
                c.setCellStyle(dateTimeCell);
                c.setCellValue(trr.getRawCreatedDate());

                c = r.createCell(2);
                c.setCellValue(trr.getPaymentType());

                c = r.createCell(3);
                c.setCellValue(trr.getSactPin());

                c = r.createCell(4);
                c.setCellValue(trr.getDisplayName());

                c = r.createCell(5);
                c.setCellValue(trr.getItemCode());

                c = r.createCell(6);
                c.setCellValue(trr.getQty());
                c.setCellType(Cell.CELL_TYPE_NUMERIC);

                c = r.createCell(7);
                c.setCellStyle(moneyCell);
                c.setCellValue(trr.getSubTotal());
                c.setCellType(Cell.CELL_TYPE_NUMERIC);

                c = r.createCell(8);
                c.setCellValue(trr.getTmStatus());

                c = r.createCell(9);
                c.setCellValue(trr.getPromoCode());

                c = r.createCell(10);
                c.setCellStyle(dateCell);
                if (trr.getRawDateOfVisit() != null) {
                    c.setCellValue(trr.getRawDateOfVisit());
                }

                c = r.createCell(11);
                c.setCellValue(trr.getCustName());

                c = r.createCell(12);
                c.setCellValue(trr.getCustEmail());

                c = r.createCell(13);
                c.setCellValue(trr.getCustMobile());

                i++;
            }
        }

        return wb;
    }

    public ApiResult<List> doTransactionSummaryReportSearch(TransactionReportRequest gridRequest, TransactionReportFilter filter) throws Exception {
        initGridOrdering(gridRequest, "createdDate", true);

        int total = reportDao.getSummaryRecordCount(filter);
        if (total > 0) {
            List<TransactionSummaryReportRow> txnRptList = reportDao.getSummaryRecords(filter, gridRequest.getSkip(), gridRequest.getTake(),
                gridRequest.getOrderBy(), gridRequest.isAsc());
            return new ApiResult<>(true, "", "", total, txnRptList);
        }

        return new ApiResult<>(true, "", "", total, new ArrayList<TransactionSummaryReportRow>());
    }

    public Workbook doTransactionSummaryReportExportExcel(TransactionReportFilter filter) throws Exception {
        Workbook wb = new XSSFWorkbook();
        Sheet s = wb.createSheet("Summary Report");
        Row r = null;
        Cell c = null;

        //center
        CellStyle cs = wb.createCellStyle();
        cs.setAlignment((short) 2);

        //right
        CellStyle cs2 = wb.createCellStyle();
        cs2.setAlignment((short) 3);

        //middle
        CellStyle cs3 = wb.createCellStyle();
        cs3.setVerticalAlignment((short) 1);

        //middle right
        CellStyle cs4 = wb.createCellStyle();
        cs4.setVerticalAlignment((short) 1);
        cs4.setAlignment((short) 3);

        Font font = wb.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);

        CellStyle cs2f = wb.createCellStyle();
        cs2f.setAlignment((short) 3);
        cs2f.setFont(font);

        CellStyle csf = wb.createCellStyle();
        csf.setFont(font);

        final DataFormat df = s.getWorkbook().createDataFormat();
        CellStyle dateCell = wb.createCellStyle();
        dateCell.setDataFormat(df.getFormat("dd/mm/yy hh:mm:ss"));

        CellStyle moneyCell = wb.createCellStyle();
        moneyCell.setDataFormat(df.getFormat("#,##0.00"));

        r = s.createRow(0);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Transaction Start Date: " + NvxDateUtils.formatDateForDisplay(filter.getFromDate(), false));
        c.setCellStyle(cs3);

        r = s.createRow(1);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Transaction End Date: " + NvxDateUtils.formatDateForDisplay(filter.getToDate(), false));
        c.setCellStyle(cs3);

        r = s.createRow(2);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Status: " + (filter.getStatus().equals("")? "ALL" : filter.getStatus()));
        c.setCellStyle(cs3);

        r = s.createRow(3);
        r.setHeight((short) 400);
        c = r.createCell(0);
        c.setCellValue("Product Name: " + filter.getProName());
        c.setCellStyle(cs3);

        r = s.createRow(4);
        r.setHeight((short) 300);

        c=r.createCell(0);
        s.setColumnWidth(0, (14 * 256) + 200);
        c.setCellValue("Transaction Date");
        c=r.createCell(1);
        c.setCellValue("Receipt Number");
        c=r.createCell(2);
        c.setCellValue("Pin Code");
        c=r.createCell(3);
        c.setCellValue("Card Last 4 Digit");
        c=r.createCell(4);
        c.setCellValue("Status");
        c=r.createCell(5);
        c.setCellValue("Total Amount (S$)");
        c=r.createCell(6);
        c.setCellValue("Approval Code");
        c=r.createCell(7);
        c.setCellStyle(cs2);
        c.setCellValue("Payment Type");
        c=r.createCell(8);
        c.setCellValue("Customer Name");
        c=r.createCell(9);
        c.setCellValue("NRIC/Passport");
        c=r.createCell(10);
        c.setCellValue("Email");
        c=r.createCell(11);
        c.setCellValue("Contact Number");
        c=r.createCell(12);
        c.setCellValue("Promo Code");

        int total = reportDao.getSummaryRecordCount(filter);
        if (total > 0) {
            List<TransactionSummaryReportRow> txnRptList = reportDao.getSummaryRecords(filter, 0, 9999, "createdDate", true);
            int i = 1;
            for (TransactionSummaryReportRow trr : txnRptList) {
                r = s.createRow(i + 4);
                r.setHeight((short) 300);

                c = r.createCell(0);
                c.setCellValue(trr.getRawCreatedDate());
                c.setCellStyle(dateCell);

                c = r.createCell(1);
                c.setCellValue(trr.getReceiptNum());

                c = r.createCell(2);
                c.setCellValue(trr.getSactPin());

                c = r.createCell(3);
                c.setCellValue(trr.getTmCcLast4Digits());

                c = r.createCell(4);
                c.setCellValue(trr.getTmStatus());

                c = r.createCell(5);
                c.setCellValue(trr.getTotalAmount());
                c.setCellStyle(moneyCell);
                c.setCellType(Cell.CELL_TYPE_NUMERIC);

                c = r.createCell(6);
                c.setCellValue(trr.getTmApprovalCode());

                c = r.createCell(7);
                c.setCellValue(trr.getPaymentType());

                c = r.createCell(8);
                c.setCellValue(trr.getCustName());

                c = r.createCell(9);
                c.setCellValue(trr.getCustIdNo());

                c = r.createCell(10);
                c.setCellValue(trr.getCustEmail());

                c = r.createCell(11);
                c.setCellValue(trr.getCustMobile());

                c = r.createCell(12);
                c.setCellValue(trr.getBookFeePromoCode());

                i++;
            }
        }

        return wb;
    }

}
