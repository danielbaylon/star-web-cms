package com.enovax.star.cms.b2cadmin.model;

/**
 * Created by jonathan on 2/11/16.
 */
public class TransactionReportRequest {

    private Integer take;
    private Integer skip;
    private Integer page;
    private Integer pageSize;
    private String orderBy;
    private boolean asc;
    private String theJson;

    public TransactionReportRequest() {
        // empty
    }

    public TransactionReportRequest(Integer take, Integer skip, Integer page, Integer pageSize, String orderBy, boolean asc) {
        this.take = take;
        this.skip = skip;
        this.page = page;
        this.pageSize = pageSize;
        this.orderBy = orderBy;
        this.asc = asc;
    }

    public Integer getTake() {
        return take;
    }

    public void setTake(Integer take) {
        this.take = take;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    public String getTheJson() {
        return theJson;
    }

    public void setTheJson(String theJson) {
        this.theJson = theJson;
    }
}
