package com.enovax.star.cms.b2cadmin.generator;

import com.enovax.star.cms.b2cadmin.model.slm.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionSummaryReportRow;
import org.apache.poi.ss.usermodel.CellStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by jonathan on 7/11/16.
 */
public class TransactionSummaryExcelGenerator extends BaseTransactionExcelGenerator {

    private TransactionReportFilter filter;

    public TransactionSummaryExcelGenerator(TransactionReportFilter filter){
        this.filter = filter;
    }
    private static final int[] txnDtlRptColWidth = new int[]{20, 43, 13, 10, 13};
    private static final String[] txnDtlRptNames = new String[]{
        "Item Code",  /* 0 */
        "Item Name",  /* 1 */
        "Unit Price",  /* 2 */
        "Qty",  /* 3 */
        "Total Amount"  /* 4 */
    };

    public void createReport(String name) {
        super.createReport(name);
        createReportParamSection();
        createReportDataHeader();
    }

    private void createReportParamSection() {
        CellStyle style = getReportParameterStyle(wb);
        addReportParamSection(style, "Transaction Date (From)", filter != null ? filter.getDateFrom() : null, false, 0);
        addReportParamSection(style, "Transaction Date (To)", filter != null ? filter.getDateTo() : null, false, 0);
        style = null;
    }

    private void createReportDataHeader() {
        reset();
        row = sh.createRow(ri++);
        row.setHeightInPoints(20);

        CellStyle style = getRptHeaderStyle(wb);
        reset();
        row = sh.createRow(ri++);
        for(int i = 0 ; i < txnDtlRptNames.length ;i++){
            String cellName = txnDtlRptNames[i];
            cell = row.createCell(ci++);
            cell.setCellStyle(style);
            cell.setCellValue(cellName);
            sh.setColumnWidth(ci - 1, txnDtlRptColWidth[i] * 256);
        }
    }

    public void writeData(List<TransactionSummaryReportRow> lst) {
        if(lst == null || lst.size() == 0){
            return;
        }
        CellStyle styleC = getRptColumnValueStyle(wb, true);
        CellStyle styleL = getRptColumnValueStyle(wb, false);
        CellStyle styleR = getRptColumnValueRightStyle(wb);
        CellStyle styleRM = getReportMoneyValueStyleWithFullBorderAndAlignRight(wb);

        for(TransactionSummaryReportRow t : lst){

            reset();
            row = sh.createRow(ri++);
            // "Item Code",  /* 0 */
            writeFieldData(styleC, t.getTxnItemCode());
            // "Item Name",  /* 1 */
            writeFieldData(styleL, t.getTxnItemName());
            // "Unit Price",  /* 2 */
            writeFieldData(styleRM, t.getTxnItemUnitPrice());
            // "Qty",  /* 3 */
            writeFieldData(styleR, t.getTxnItemQty());
            // "Total Amount"  /* 4 */
            writeFieldData(styleRM, t.getTotalAmount());

            populateGrandTotal(t.getTxnItemQty(), t.getTotalAmount());

        }

        lst = null;
        styleL = null;
        styleC = null;
        styleR = null;
        styleRM = null;
    }


    public void buildReportFooter(){
        CellStyle styleR = getRptColumnValueRightStyle(wb, true);
        CellStyle styleRM = getReportMoneyValueStyleWithFullBorderAndAlignRight(wb, true);

        reset();
        row = sh.createRow(ri++);
        writeFieldData(styleR, "");
        writeFieldData(styleR, "");
        writeFieldData(styleR, "Grand Total");
        writeFieldData(styleR, grandTotalQty.intValue());
        writeFieldData(styleRM, grandTotalAmount);

        styleR = null;
        styleRM = null;
    }

    private void populateGrandTotal(Integer txnItemQty, BigDecimal totalAmount) {
        grandTotalQty += (txnItemQty == null ? 0 : txnItemQty.intValue());
        if(totalAmount != null){
            grandTotalAmount += totalAmount.doubleValue();
        }
    }

    private Long grandTotalQty = 0l;
    private Double grandTotalAmount = 0.0;

}
