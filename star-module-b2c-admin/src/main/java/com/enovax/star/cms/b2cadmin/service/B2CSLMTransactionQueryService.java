package com.enovax.star.cms.b2cadmin.service;

import com.enovax.star.cms.b2cadmin.model.slm.QueryRecord;
import com.enovax.star.cms.b2cadmin.repository.B2CSLMQueryTransDao;
import com.enovax.star.cms.commons.repository.b2cslm.B2CSLMStoreTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jennylynsze on 11/9/16.
 */
@Service
public class B2CSLMTransactionQueryService {

    @Autowired
    private B2CSLMQueryTransDao dao;

    @Autowired
    B2CSLMStoreTransactionRepository transRepo;

    @Transactional(readOnly=true)
    public List<QueryRecord> getQueryRecords(String recNum, String cusName,
                                             String nric, String email, String mobile, String pin, int start,
                                             int pageSize, String orderBy, boolean asc) {
        return dao.getQueryRecords(recNum, cusName, nric, email, mobile, pin, start, pageSize, orderBy, asc);
    }

    @Transactional(readOnly=true)
    public int getQueryRecordCount(String recNum, String cusName, String nric,
                                   String email, String mobile, String pin) {
        return dao.getQueryRecordCount(recNum, cusName, nric, email, mobile, pin);
    }

}
