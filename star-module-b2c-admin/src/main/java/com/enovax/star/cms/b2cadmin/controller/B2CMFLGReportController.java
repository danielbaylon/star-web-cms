package com.enovax.star.cms.b2cadmin.controller;

import com.enovax.star.cms.b2cadmin.model.TransactionReportRequest;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.service.B2CMFLGReportService;
import com.enovax.star.cms.commons.model.api.ApiResult;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by jonathan on 2/11/16.
 */
@Controller
@RequestMapping( "/b2cmflgadmin/report/")
public class B2CMFLGReportController extends BaseB2CAdminController {

    @Autowired
    private B2CMFLGReportService reportService;

    @RequestMapping(value = "transaction-details/search", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List>> apiDoTransactionDetailsReportPregeneration(@RequestParam(name = "startDate", required = false) String startDate,
                                                                                      @RequestParam(name = "endDate", required = false) String endDate,
                                                                                      @RequestParam(name = "proName", required = false) String proName,
                                                                                      @RequestParam(name = "status", required = false) String status,
                                                                                      @RequestParam(name = "recNum", required = false) String recNum,
                                                                                      @RequestParam(name = "dov", required = false) String dov,
                                                                                      @RequestParam(name = "proCode", required = false) String proCode,
                                                                                      @RequestParam("take") int take, @RequestParam("skip") int skip,
                                                                                      @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
                                                                                      @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                                      @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(take, skip, page, pageSize, sortField, "asc".equals(sortDirection) ? true : false);
            TransactionReportFilter filter = new TransactionReportFilter(startDate, endDate, proName, status, recNum, dov, proCode);
            return new ResponseEntity<>(reportService.doTransactionDetailsReportSearch(gridRequest, filter), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionDetailsReportPregeneration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-details/exportExcel", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiDoTransactionDetailsReportExportExcel(@RequestParam(name = "startDate", required = false) String startDate,
                                                                                      @RequestParam(name = "endDate", required = false) String endDate,
                                                                                      @RequestParam(name = "proName", required = false) String proName,
                                                                                      @RequestParam(name = "status", required = false) String status,
                                                                                      @RequestParam(name = "recNum", required = false) String recNum,
                                                                                      @RequestParam(name = "dov", required = false) String dov,
                                                                                      @RequestParam(name = "proCode", required = false) String proCode,
                                                                                      HttpServletResponse response) {
        try {
            TransactionReportFilter filter = new TransactionReportFilter(startDate, endDate, proName, status, recNum, dov, proCode);
            Workbook wb = reportService.doTransactionDetailsReportExportExcel(filter);

            final String fileName = "transaction-details-report.xls";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionDetailsReportExportExcel", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-summary/search", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List>> apiDoTransactionSummaryReportPregeneration(@RequestParam(name = "startDate", required = false) String startDate,
                                                                                      @RequestParam(name = "endDate", required = false) String endDate,
                                                                                      @RequestParam(name = "proName", required = false) String proName,
                                                                                      @RequestParam(name = "status", required = false) String status,
                                                                                      @RequestParam("take") int take, @RequestParam("skip") int skip,
                                                                                      @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
                                                                                      @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                                      @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(take, skip, page, pageSize, sortField, "asc".equals(sortDirection) ? true : false);
            TransactionReportFilter filter = new TransactionReportFilter(startDate, endDate, proName, status);
            return new ResponseEntity<>(reportService.doTransactionSummaryReportSearch(gridRequest, filter), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionSummaryReportPregeneration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-summary/exportExcel", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiDoTransactionSummaryReportExportExcel(@RequestParam(name = "startDate", required = false) String startDate,
                                                                                      @RequestParam(name = "endDate", required = false) String endDate,
                                                                                      @RequestParam(name = "proName", required = false) String proName,
                                                                                      @RequestParam(name = "status", required = false) String status,
                                                                                      HttpServletResponse response) {
        try {
            TransactionReportFilter filter = new TransactionReportFilter(startDate, endDate, proName, status);
            Workbook wb = reportService.doTransactionSummaryReportExportExcel(filter);

            final String fileName = "\"transaction-summary-report.xlsx\"";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionSummaryReportExportExcel", e, String.class), HttpStatus.OK);
        }
    }

}
