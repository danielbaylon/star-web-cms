package com.enovax.star.cms.b2cadmin.model;

import java.util.Map;

/**
 * Created by jonathan on 3/11/16.
 */
public class TransactionQueryHolder {

    private String listQueryScript;

    private String countQueryScripts;

    private String criteriaQueryScripts;

    private String groupingQueryScripts;

    private Map<String, Object> params;

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getListQueryScript() {
        return listQueryScript;
    }

    public void setListQueryScript(String listQueryScript) {
        this.listQueryScript = listQueryScript;
    }

    public String getCountQueryScripts() {
        return countQueryScripts;
    }

    public void setCountQueryScripts(String countQueryScripts) {
        this.countQueryScripts = countQueryScripts;
    }

    public String getCriteriaQueryScripts() {
        return criteriaQueryScripts;
    }

    public void setCriteriaQueryScripts(String criteriaQueryScripts) {
        this.criteriaQueryScripts = criteriaQueryScripts;
    }

    public String getGroupingQueryScripts() {
        return groupingQueryScripts;
    }

    public void setGroupingQueryScripts(String groupingQueryScripts) {
        this.groupingQueryScripts = groupingQueryScripts;
    }

}
