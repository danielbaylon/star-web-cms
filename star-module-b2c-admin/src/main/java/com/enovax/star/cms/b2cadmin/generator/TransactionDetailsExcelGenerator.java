package com.enovax.star.cms.b2cadmin.generator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.enovax.star.cms.b2cadmin.model.slm.TransactionDetailsReportRow;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionReportFilter;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 * Created by jonathan on 7/11/16.
 */
public class TransactionDetailsExcelGenerator extends BaseTransactionExcelGenerator {

    private TransactionReportFilter filter;

    private Map<Integer, String> rptFieldMap = new HashMap<Integer, String>();

    public TransactionDetailsExcelGenerator(TransactionReportFilter filter){
        initRptFieldMap();
        this.filter = filter;
    }
    private static final int[] txnDtlRptColWidth = new int[]{18, 18, 10, 20, /*22,*/ 10, 12, 28, 12, 12, 12, 20, 12, 15, 15, 15, /*20,*/ 16, 28, 6, 50, 10, 12, 12};
    private static final String[] txnDtlRptNames = new String[]{
        "Transaction Date",  /* 1 */
        "Receipt No.",  /* 2 */
        "Status",  /* 3 */
        "Name",  /* 4 */
//        "Recurring PIN",  /* 5 */
        "Language",  /* 6 */
        "PIN",  /* 7 */
        "Email",  /* 9 */
        "Mobile",  /* 10 */
        "ID",  /* 11 */
        "Date of Birth",  /* 12 */
        "Referral Source",  /* 13 */
        "Nationality",  /* 14 */
        "Payment Type",  /* 15 */
        "Booking Fee (Cents)",  /* 16 */
        "Credit Card",  /* 17 */
//        "SACT Reservation ID",  /* 18 */
        "Bank Approval Code",  /* 19 */
        "Traffic Source",  /* 20 */
        "Qty",  /* 21 */
        "Item name",  /* 22 */
        "Unit Price",  /* 23 */
        "Total Amount", /* 24 */
        "Receipt Amount"  /* 8 */
    };

    private void initRptFieldMap(){
        rptFieldMap.put(21, "amt");
        rptFieldMap.put(6, "email");
        rptFieldMap.put(7, "contact");
        rptFieldMap.put(8, "id");
        rptFieldMap.put(9, "bday");
        rptFieldMap.put(10, "referral");
        rptFieldMap.put(11, "natl");
        rptFieldMap.put(12, "paytype");
        rptFieldMap.put(13, "bookfee");
        rptFieldMap.put(14, "cclast4");
//        rptFieldMap.put(17, "sactresid");
        rptFieldMap.put(15, "bankapprovalcode");
        rptFieldMap.put(16, "trafficsource");
    }

    private boolean isFieldVisible(int idx){
        String field = rptFieldMap.get(idx);
        if(field != null && field.trim().length() > 0){
            List<String> cols = filter.getColumnFilters();
            if(cols != null && cols.size() > 0 && cols.contains(field)){
                return true;
            }else{
                return false;
            }
        }
        return true;
    }

    public void createReport(String name) {
        super.createReport(name);
        createReportParamSection();
        createReportDataHeader();
    }

    private void createReportDataHeader() {
        reset();
        row = sh.createRow(ri++);
        row.setHeightInPoints(20);

        CellStyle style = getRptHeaderStyle(wb);
        reset();
        row = sh.createRow(ri++);
        for(int i = 0 ; i < txnDtlRptNames.length ;i++){
            String cellName = txnDtlRptNames[i];
            if(isFieldVisible(i)){
                cell = row.createCell(ci++);
                cell.setCellStyle(style);
                cell.setCellValue(cellName);
                sh.setColumnWidth(ci - 1, txnDtlRptColWidth[i] * 256);
            }
        }
    }

    private void createReportParamSection() {
        CellStyle style = getReportParameterStyle(wb);
        addReportParamSection(style, "Transaction Date (From)", filter != null ? filter.getDateFrom() : null);
        addReportParamSection(style, "Transaction Date (To)", filter != null ? filter.getDateTo() : null);
        addReportParamSection(style, "Transaction Status", filter != null ? translateTxnStatus(filter.getStatus()) : null);
        addReportParamSection(style, "Receipt No.", filter != null ? filter.getReceipt() : null);
        addReportParamSection(style, "Customer Name", filter != null ? filter.getName() : null);
        style = null;
    }

    private String translateTxnStatus(String status) {
        return status != null && status.trim().length() > 0 ? status.trim() : "All";
    }

    public void writeData(List<TransactionDetailsReportRow> lst) {
        if(lst == null || lst.size() == 0){
            return;
        }
        CellStyle styleL = getRptColumnValueStyle(wb, false);
        CellStyle styleC = getRptColumnValueStyle(wb, true);
        CellStyle styleR = getRptColumnValueRightStyle(wb);
        CellStyle styleRM = getReportMoneyValueStyleWithFullBorderAndAlignRight(wb);

        for(TransactionDetailsReportRow t : lst){

            reset();
            row = sh.createRow(ri++);
            // "Transaction Date",  /* 1 */
            writeFieldData(styleL, t.getTransDateText());
            // "Receipt No.",  /* 2 */
            writeFieldData(styleL, t.getReceiptNum());
            // "Status",  /* 3 */
            writeFieldData(styleL, t.getStatus());
            // "Name",  /* 4 */
            writeFieldData(styleL, t.getCustName());
//            // "Recurring PIN",  /* 5 */
//            writeFieldData(styleL, t.getRecurPin());
            // "Language",  /* 6 */
            writeFieldData(styleC, t.getLanCode() != null ? t.getLanCode().trim().toUpperCase() : "");
            // "PIN",  /* 7 */
            writeFieldData(styleL, t.getPin());

            // "Email",  /* 9 */
            if(isFieldVisible(6)){
                writeFieldData(styleL, t.getCustEmail());
            }
            // "Mobile",  /* 10 */
            if(isFieldVisible(7)){
                writeFieldData(styleL, t.getCustMobile());
            }
            // "ID",  /* 11 */
            if(isFieldVisible(8)){
                writeFieldData(styleL, t.getCustIdNo());
            }
            // "Date of Birth",  /* 12 */
            if(isFieldVisible(9)){
                writeFieldData(styleL, t.getCustDob());
            }
            // "Referral Source",  /* 13 */
            if(isFieldVisible(10)){
                writeFieldData(styleL, t.getCustReferSource());
            }
            // "Nationality",  /* 14 */
            if(isFieldVisible(11)){
                writeFieldData(styleL, t.getCustNationality());
            }
            // "Payment Type",  /* 15 */
            if(isFieldVisible(12)){
                writeFieldData(styleL, t.getPaymentType());
            }
            // "Booking Fee (Cents)",  /* 16 */
            if(isFieldVisible(13)){
                writeFieldData(styleRM, t.getBookFeeText());
            }
            // "Card Last 4 Digits",  /* 17 */
            if(isFieldVisible(14)){
                writeFieldData(styleL, t.getCcLast4Digits());
            }
//            // "SACT Reservation ID",  /* 18 */
//            if(isFieldVisible(17)){
//                writeFieldData(styleL, t.getSactResId());
//            }
            // "Bank Approval Code",  /* 19 */
            if(isFieldVisible(15)){
                writeFieldData(styleL, t.getBankApprovalCode());
            }
            // "Traffic Source",  /* 20 */
            if(isFieldVisible(16)){
                writeFieldData(styleL, t.getTrafficSource());
            }
            // "Qty",  /* 21 */
            writeFieldData(styleR, t.getValue1());
            // "Item name",  /* 22 */
            writeFieldData(styleL, t.getValue2());
            // "Unit Price"  /* 23 */
            writeFieldData(styleRM, t.getValue3());
            // "Unit Price"  /* 24 */
            writeFieldData(styleRM, t.getValue4());
            // "Total Amount",  /* 8 */
            if(isFieldVisible(21)){
                writeFieldData(styleRM, t.getTotalAmountText());
            }
        }

        lst = null;
        styleL = null;
        styleC = null;
        styleR = null;
        styleRM = null;
    }

    public void buildReportFooter() {}

}
