package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.*;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionDetailsReportRow;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionPromotionReportRow;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionSummaryReportRow;

import java.util.List;
import java.util.Map;

/**
 * Created by jonathan on 3/11/16.
 */
public interface B2CSLMReportDao {

    TransactionQueryHolder preparePagedDetailTransReportQuery(TransactionReportFilter filter, Map<String, Object> params);

    List<TransactionDetailsReportRow> getPagedDetailTransReport(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc);

    int getPagedDetailTransReportCount(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc);

    TransactionQueryHolder preparePagedSummaryTransReportQuery(TransactionReportFilter filter, Map<String, Object> params);

    List<TransactionSummaryReportRow> getPagedSummaryTransReport(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc);

    int getPagedSummaryTransReportCount(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc);

    TransactionQueryHolder preparePagedPromotionItemTransReportQuery(TransactionReportFilter filter, Map<String, Object> params);

    List<TransactionPromotionReportRow> getPagedPromotionItemTransReport(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc);

    int getPagedPromotionItemTransReportCount(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc);

}
