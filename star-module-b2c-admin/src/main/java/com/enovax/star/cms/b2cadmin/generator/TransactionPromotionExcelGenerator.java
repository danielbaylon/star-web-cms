package com.enovax.star.cms.b2cadmin.generator;

import com.enovax.star.cms.b2cadmin.model.slm.TransactionPromotionReportRow;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionReportFilter;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.poi.ss.usermodel.CellStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by jonathan on 7/11/16.
 */
public class TransactionPromotionExcelGenerator extends BaseTransactionExcelGenerator {

    private TransactionReportFilter filter;

    public TransactionPromotionExcelGenerator(TransactionReportFilter filter){
        this.filter = filter;
    }
    private static final int[] txnDtlRptColWidth = new int[]{22, 22, 22, 22, 50, 15, 15, 15};
    private static final String[] txnDtlRptNames = new String[]{
        "Promo Code",  /* 0 */
        "Promo Code (Valid from)",  /* 1 */
        "Promo Code (Valid to)",  /* 2 */
        "Item Code",  /* 3 */
        "Item Name",  /* 4 */
        "Unit Price",  /* 5 */
        "Qty",  /* 6 */
        "Total Amount"  /* 7 */
    };

    public void createReport(String name) {
        super.createReport(name);
        createReportParamSection();
        createReportDataHeader();
    }

    private void createReportParamSection() {
        CellStyle style = getReportParameterStyle(wb);
        addReportParamSection(style, "Transaction Date (From)", filter != null ? filter.getDateFrom() : null, false, 0);
        addReportParamSection(style, "Transaction Date (To)", filter != null ? filter.getDateTo() : null, false, 0);
        addReportParamSection(style, "Promotion Code", filter != null ? translatePromotionCode(filter.getTxnPromoCode()) : null, false, 0);
        style = null;
    }

    private String translatePromotionCode(String promoCode) {
        if(promoCode != null && promoCode.trim().length() > 0){
            return promoCode.trim();
        }
        return "All";
    }

    private void createReportDataHeader() {
        reset();
        row = sh.createRow(ri++);
        row.setHeightInPoints(20);

        CellStyle style = getRptHeaderStyle(wb);
        reset();
        row = sh.createRow(ri++);
        for(int i = 0 ; i < txnDtlRptNames.length ;i++){
            String cellName = txnDtlRptNames[i];
            cell = row.createCell(ci++);
            cell.setCellStyle(style);
            cell.setCellValue(cellName);
            sh.setColumnWidth(ci - 1, txnDtlRptColWidth[i] * 256);
        }
    }

    public void writeData(List<TransactionPromotionReportRow> lst) {
        if(lst == null || lst.size() == 0){
            return;
        }
        CellStyle styleC = getRptColumnValueStyle(wb, true);
        CellStyle styleL = getRptColumnValueStyle(wb, false);
        CellStyle styleR = getRptColumnValueRightStyle(wb);
        CellStyle styleRM = getReportMoneyValueStyleWithFullBorderAndAlignRight(wb);

        for(TransactionPromotionReportRow t : lst){

            reset();
            row = sh.createRow(ri++);
            // "Promo Code",  /* 0 */
            writeFieldData(styleL, t.getPromoCode());

            // "Promo Code (Valid from)",  /* 1 */
            writeFieldData(styleL, getDateString(t.getValidFrom()));

            // "Promo Code (Valid to)",  /* 2 */
            writeFieldData(styleL, getDateString(t.getValidTo()));

            // "Item Code",  /* 3 */
            writeFieldData(styleL, t.getTxnItemCode());

            // "Item Name",  /* 4 */
            writeFieldData(styleL, t.getTxnItemName());

            // "Unit Price",  /* 5 */
            writeFieldData(styleRM, t.getTxnItemUnitPrice());

            // "Qty",  /* 6 */
            writeFieldData(styleR, t.getTxnItemQty());

            // "Total Amount"  /* 7 */
            writeFieldData(styleRM, t.getTotalAmount());

            populateGrandTotal(t.getTxnItemQty(), t.getTotalAmount());
        }

        lst = null;
        styleL = null;
        styleC = null;
        styleR = null;
        styleRM = null;
    }


    private String getDateString(Date validFrom) {
        return validFrom != null ? NvxDateUtils.formatDateForDisplay(validFrom, false) : "";
    }

    public void buildReportFooter(){
        CellStyle styleR = getRptColumnValueRightStyle(wb, true);
        CellStyle styleRM = getReportMoneyValueStyleWithFullBorderAndAlignRight(wb, true);

        reset();
        row = sh.createRow(ri++);
        writeFieldData(styleR, "");
        writeFieldData(styleR, "");
        writeFieldData(styleR, "");
        writeFieldData(styleR, "");
        writeFieldData(styleR, "");
        writeFieldData(styleR, "Grand Total");
        writeFieldData(styleR, grandTotalQty.intValue());
        writeFieldData(styleRM, grandTotalAmount);

        styleR = null;
        styleRM = null;
    }

    private void populateGrandTotal(Integer txnItemQty, BigDecimal totalAmount) {
        grandTotalQty += (txnItemQty == null ? 0 : txnItemQty.intValue());
        if(totalAmount != null){
            grandTotalAmount += totalAmount.doubleValue();
        }
    }

    private Long grandTotalQty = 0l;
    private Double grandTotalAmount = 0.0;

}
