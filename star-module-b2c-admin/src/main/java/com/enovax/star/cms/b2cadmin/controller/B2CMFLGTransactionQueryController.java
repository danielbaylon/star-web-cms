package com.enovax.star.cms.b2cadmin.controller;


import com.enovax.star.cms.b2cadmin.model.mflg.QueryRecord;
import com.enovax.star.cms.b2cadmin.model.mflg.ResendEmailModel;
import com.enovax.star.cms.b2cadmin.service.B2CMFLGTransactionQueryService;
import com.enovax.star.cms.b2cshared.service.IB2CCartAndDisplayService;
import com.enovax.star.cms.b2cshared.service.IB2CMailService;
import com.enovax.star.cms.commons.constant.api.StoreApiChannels;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.model.booking.ReceiptDisplay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jennylynsze on 11/10/16.
 */
@Controller
@RequestMapping( "/b2cmflgadmin/transaction-query/")
public class B2CMFLGTransactionQueryController extends BaseB2CAdminController {

    @Autowired
    B2CMFLGTransactionQueryService reportService;
    @Autowired
    IB2CCartAndDisplayService cartAndDisplayService;
    @Autowired
    IB2CMailService mailService;

    @RequestMapping(value = "search", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<List>> apiDoGetQueryRecords(
            @RequestParam(value="recNum", required = false) String recNum,
            @RequestParam(name = "custName", required = false) String custName,
            @RequestParam(name = "nric", required = false) String nric,
            @RequestParam(name = "email", required = false) String email,
            @RequestParam(name = "mobile", required = false) String mobile,
            @RequestParam(name = "pin", required = false) String pin,
            @RequestParam("take") int take, @RequestParam("skip") int skip,
            @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "sort[0][field]", required = false) String sortField,
            @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {

        log.info("Entered apiDoGetQueryRecords...");
        try {

            List<QueryRecord> queryRecords = reportService.getQueryRecords(recNum, custName, nric, email, mobile, pin, skip, pageSize, sortField,
                    "ASC".equalsIgnoreCase(sortDirection));
            int totalRecords = reportService.getQueryRecordCount(recNum, custName, nric, email, mobile, pin);
            ApiResult<List> result = new ApiResult<>(true, "", "", queryRecords);
            result.setTotal(totalRecords);

            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoGetQueryRecords", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "show-details/{receiptNumber}", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<ReceiptDisplay>> apiDoShowDetails(@PathVariable("receiptNumber") String receiptNumber) {
        log.info("Entered apiDoShowDetails...");
        try {

            final ReceiptDisplay display = cartAndDisplayService.getReceiptForDisplayByReceipt(StoreApiChannels.B2C_MFLG, receiptNumber, true);
            //todo tnc
            return new ResponseEntity<>(new ApiResult<>(true, "", "", display), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoShowDetails", e, ReceiptDisplay.class), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "send-email", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<String>> apiDoSendEmail(@RequestBody ResendEmailModel resendEmailModel){
        log.info("Entered apiDoSendEmail...");
        try {
            mailService.sendAndUpdateEmailForB2CBookingReceipt(StoreApiChannels.B2C_MFLG, resendEmailModel.getReceiptNumber(), resendEmailModel.isUpdateEmail(), resendEmailModel.getNewEmail());
            return new ResponseEntity<>(new ApiResult<>(true, "", "", ""), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoSendEmail", e, String.class), HttpStatus.OK);
        }
    }

}
