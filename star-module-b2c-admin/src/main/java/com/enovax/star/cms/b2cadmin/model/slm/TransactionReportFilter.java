package com.enovax.star.cms.b2cadmin.model.slm;

import java.util.List;

/**
 * Created by jonathan on 2/11/16.
 */
public class TransactionReportFilter {

    private String dateFrom;
    private String dateTo;
    private String name;
    private String receipt;
    private String status;
    private String prodType;

    private List<Integer> packageCodes;

    private List<Integer> promoCampaignCodes;

    private List<String> columnFilters;

    private String txnPromoCode;

    public TransactionReportFilter() {

    }

    public TransactionReportFilter(String dateFrom, String dateTo, String name, String receipt, String status, String prodType,
                                   List<Integer> packageCodes, List<Integer> promoCampaignCodes, List<String> columnFilters) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.name = name;
        this.receipt = receipt;
        this.status = status;
        this.packageCodes = packageCodes;
        this.promoCampaignCodes = promoCampaignCodes;
        this.columnFilters = columnFilters;
        this.prodType = prodType;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Integer> getPackageCodes() {
        return packageCodes;
    }

    public void setPackageCodes(List<Integer> packageCodes) {
        this.packageCodes = packageCodes;
    }

    public List<Integer> getPromoCampaignCodes() {
        return promoCampaignCodes;
    }

    public void setPromoCampaignCodes(List<Integer> promoCampaignCodes) {
        this.promoCampaignCodes = promoCampaignCodes;
    }

    public List<String> getColumnFilters() {
        return columnFilters;
    }

    public void setColumnFilters(List<String> columnFilters) {
        this.columnFilters = columnFilters;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public String getTxnPromoCode() {
        return txnPromoCode;
    }

    public void setTxnPromoCode(String txnPromoCode) {
        this.txnPromoCode = txnPromoCode;
    }

}
