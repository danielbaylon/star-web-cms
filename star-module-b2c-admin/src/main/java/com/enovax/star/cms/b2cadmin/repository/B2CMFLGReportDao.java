package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.mflg.TransactionDetailsReportRow;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionSummaryReportRow;

import java.util.Date;
import java.util.List;

/**
 * Created by jonathan on 4/11/16.
 */
public interface B2CMFLGReportDao {

    List<TransactionSummaryReportRow> getSummaryRecords(TransactionReportFilter filter, int start, int pageSize, String orderBy, boolean asc);

    int getSummaryRecordCount(TransactionReportFilter filter);

    List<TransactionDetailsReportRow> getTransactionRecords(TransactionReportFilter filter, int start, int pageSize, String orderBy, boolean asc);

    int getTransactionRecordCount(TransactionReportFilter filter);

}
