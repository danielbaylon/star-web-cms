package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.TransactionQueryHolder;
import com.enovax.star.cms.b2cadmin.model.slm.*;
import com.enovax.star.cms.commons.jcrrepository.JcrRepository;
import com.enovax.star.cms.commons.mgnl.definition.JcrWorkspace;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import com.enovax.star.cms.commons.util.NvxNumberUtils;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.jcr.Node;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jonathan on 3/11/16.
 */
@Repository
public class B2CSLMReportDaoImpl implements B2CSLMReportDao {

    @Autowired
    private SessionFactory sessionFactory;

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private static final Map<String, String> sortMap = new HashMap<>();
    private static final Map<String, String> summaryRptSortMap = new HashMap<>();
    private static final Map<String, String> promoCodeRptSortMap = new HashMap<>();

    private static Map<String, Promotion> promotionCacheMap = null;
    private static Calendar promotionCacheDate = null;

    private static void initSortMap(){
        if(sortMap.size() == 0){
            sortMap.put("transDateText", "t.createdDate");
            sortMap.put("receiptNum", "t.receiptNum");
            sortMap.put("status", "t.tmStatus");
            sortMap.put("totalAmount", "t.totalAmount");
            sortMap.put("custName", "t.custName");
            sortMap.put("custEmail", "t.custEmail");
            sortMap.put("custMobile", "t.custMobile");
            sortMap.put("custIdNo", "t.custIdNo");
            sortMap.put("custDob", "t.custDob");
            sortMap.put("custReferSource", "t.custReferSource");
            sortMap.put("custNationality", "t.custNationality");
            sortMap.put("paymentType", "t.paymentType");
            sortMap.put("bookFeeCents", "t.bookFeeCents");
            sortMap.put("ccLast4Digits", "tm.tmCcNum");
            sortMap.put("bankApprovalCode", "tm.tmApprovalCode");
            sortMap.put("value1", "ti.qty");
            sortMap.put("value2", "ti.displayName");
            sortMap.put("value3", "ti.actualUnitPrice");
            sortMap.put("lanCode", "t.langCode");
            sortMap.put("pin", "t.pinCode");
            sortMap.put("trafficSource", "t.trafficSource");
            sortMap.put("value4", "ti.subTotal");
        }
    }

    private static void initSummaryRptSortMap(){
        if(summaryRptSortMap.size() == 0){
            summaryRptSortMap.put("txnItemCode", "ti.itemProductCode");
            summaryRptSortMap.put("txnItemName", "ti.displayName");
            summaryRptSortMap.put("txnItemUnitPrice", "ti.actualUnitPrice");
        }
    }

    private static void initPromoSortMap(){
        if(promoCodeRptSortMap.size() == 0){
            promoCodeRptSortMap.put("promoCode", "ti.promoCodes");
            promoCodeRptSortMap.put("txnItemCode", "ti.itemProductCode");
            promoCodeRptSortMap.put("txnItemName", "ti.displayName");
            promoCodeRptSortMap.put("txnItemUnitPrice", "ti.actualUnitPrice");
            promoCodeRptSortMap.put("promoCodeDiscountId", "ti.promoCodeDiscountId");
        }
    }

    private SQLQuery prepareSqlQuery(String query, Map<String, Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        for (String key : parameters.keySet()) {
            Object para = parameters.get(key);
            if (para instanceof List<?>) {
                queryObject.setParameterList(key, (List<?>)para);
            } else {
                queryObject.setParameter(key, para);
            }
        }
        return queryObject;
    }

    private List<TransactionDetailsReportRow> populateDetailTxnRptList(List<Object[]> results) {
        List<TransactionDetailsReportRow> list = new ArrayList<>();
        if(results != null && results.size() > 0){
            for(Object[] o : results){
                TransactionDetailsReportRow t = new TransactionDetailsReportRow();
                list.add(t);
                t.setTransDate((Date) o[0]);
                t.setTransDateText(NvxDateUtils.formatDate(t.getTransDate(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
                t.setReceiptNum((String)o[1]);
                t.setStatus((String)o[2]);
                t.setTotalAmount((BigDecimal)o[3]);
                t.setTotalAmountText(NvxNumberUtils.formatToCurrency(t.getTotalAmount()));
                t.setCustName((String)o[4]);
                t.setCustEmail((String)o[5]);
                t.setCustMobile((String)o[6]);
                t.setCustIdNo((String)o[7]);
                t.setCustDob((String)o[8]);
                t.setCustReferSource((String)o[9]);
                t.setCustNationality((String)o[10]);
                t.setPaymentType((String)o[11]);
                t.setBookFeeCents((BigDecimal)o[12]);
                t.setBookFeeText(NvxNumberUtils.formatToCurrency(t.getBookFeeCents()));
                t.setCcLast4Digits((String)o[13]);
                t.setBankApprovalCode((String)o[14]);
                t.setValue1((String)o[15]);
                t.setValue2((String)o[16]);
                t.setValue3(NvxNumberUtils.formatToCurrency((BigDecimal)o[17]));
                t.setLanCode((String)o[18]);
                t.setPin((String)o[19]);
                t.setTrafficSource((String)o[20]);
                t.setValue4(NvxNumberUtils.formatToCurrency((BigDecimal)o[21]));
            }
        }
        return list;
    }

    private List<TransactionSummaryReportRow> populateSummaryTxnRptList(List<Object[]> results) {
        List<TransactionSummaryReportRow> list = new ArrayList<>();
        if(results != null && results.size() > 0){
            for(Object[] o : results){
                BigDecimal bdqty   = null;
                BigDecimal bdprice = null;

                TransactionSummaryReportRow t = new TransactionSummaryReportRow();
                list.add(t);

                t.setTxnItemCode((String)o[0]);
                t.setTxnItemName((String)o[1]);

                Integer qty = (Integer)o[3];
                BigDecimal price = (BigDecimal)o[2];

                if(price == null || price.doubleValue() < 0){
                    bdprice = new BigDecimal(0);
                }else{
                    bdprice = price;
                }

                if(qty == null || qty.intValue() < 0){
                    bdqty = new BigDecimal(0);
                }else{
                    bdqty = new BigDecimal(qty);
                }

                t.setTxnItemUnitPrice(bdprice);
                t.setTxnItemQty(qty);
                t.setTotalAmount((BigDecimal)o[4]);
            }
        }
        return list;
    }

    private Map<String, Promotion> cachePromotionValidty() {
        Map<String, Promotion> promoCacheMap = new HashMap<>();
        try {
            Node mainNode = JcrRepository.getParentNode(JcrWorkspace.Promotions.getWorkspaceName(), "/b2c-slm");
            Iterable<Node> promotionNodes = NodeUtil.getNodes(mainNode, JcrWorkspace.Promotions.getNodeType());
            if (promotionNodes != null) {
                for (Node promotionNode: promotionNodes) {
                    String discountId = promotionNode.getName();
                    Iterable<Node> childNodes = NodeUtil.getNodes(promotionNode, "mgnl:contentNode");
                    for (Node childNode: childNodes) {
                        if ("discountCode".equals(childNode.getName())) {
                            Iterable<Node> discountNodes = NodeUtil.getNodes(childNode, JcrWorkspace.DiscountCode.getNodeType());
                            if (discountNodes != null) {
                                for (Node discountNode: discountNodes) {
                                    Promotion promotion = new Promotion();
                                    promotion.setDiscountId(discountId);
                                    promotion.setPromoCode(PropertyUtil.getString(discountNode, "code"));
                                    promotion.setValidFrom(PropertyUtil.getDate(discountNode, "validFrom"));
                                    promotion.setValidTo(PropertyUtil.getDate(discountNode, "validTo"));
                                    promoCacheMap.put(promotion.getDiscountId() + "||" + promotion.getPromoCode(), promotion);
                                }
                            }
                            break;
                        }
                    }
                    // include parent as backup
                    Promotion promotion = new Promotion();
                    promotion.setDiscountId(discountId);
                    promotion.setPromoCode("");
                    promotion.setValidFrom(PropertyUtil.getDate(promotionNode, "validFrom"));
                    promotion.setValidTo(PropertyUtil.getDate(promotionNode, "validTo"));
                    promoCacheMap.put(promotion.getDiscountId() + "||" + promotion.getPromoCode(), promotion);
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving promotion values.", e);
        }
        return promoCacheMap;
    }

    private Map<String, Promotion> getPromotionCache() {
        if (promotionCacheDate == null || promotionCacheMap == null) {
            promotionCacheDate = Calendar.getInstance();
            promotionCacheMap = cachePromotionValidty();
            return promotionCacheMap;
        }
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, -5);
        if (now.compareTo(promotionCacheDate) >= 0) {
            promotionCacheDate = Calendar.getInstance();
            promotionCacheMap = cachePromotionValidty();
            return promotionCacheMap;
        }
        return promotionCacheMap;
    }

    private void getPromotionValidity(String discountId, String promoCode, TransactionPromotionReportRow t, Map<String, Promotion> map) {
        Promotion promo = map.get(discountId + "||" + promoCode);
        if (promo == null) {
            promo = map.get(discountId + "||");
        }
        if (promo == null) {
            t.setValidFrom(null);
            t.setValidTo(null);
        } else {
            t.setValidFrom(promo.getValidFrom().getTime());
            t.setValidTo(promo.getValidTo().getTime());
            t.setValidFromText(NvxDateUtils.formatDate(t.getValidFrom(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
            t.setValidToText(NvxDateUtils.formatDate(t.getValidTo(), NvxDateUtils.DEFAULT_DATE_FORMAT_DISPLAY_TIME));
        }
    }

    private List<TransactionPromotionReportRow> populatePromotionTxnRptList(List<Object[]> results) {
        List<TransactionPromotionReportRow> list = new ArrayList<>();
        if(results != null && results.size() > 0){
            Map<String, Promotion> map = getPromotionCache();

            for(Object[] o : results){
                BigDecimal bdqty   = null;
                BigDecimal bdprice = null;

                String promoCode = (String)o[0];
                String promoCodeDiscountId = (String)o[5];
                TransactionPromotionReportRow t = new TransactionPromotionReportRow();
                list.add(t);
                t.setPromoCode(promoCode);

                if (!StringUtils.isEmpty(promoCodeDiscountId)) {
                    getPromotionValidity(promoCodeDiscountId, promoCode, t, map);
                }

                t.setTxnItemCode((String)o[1]);
                t.setTxnItemName((String)o[2]);

                BigDecimal price = (BigDecimal)o[3];
                Integer qty = (Integer)o[4];

                if(price == null || price.doubleValue() < 0){
                    bdprice = new BigDecimal(0);
                }else{
                    bdprice = price;
                }

                if(qty == null || qty.intValue() < 0){
                    bdqty = new BigDecimal(0);
                }else{
                    bdqty = new BigDecimal(qty);
                }
                t.setTxnItemUnitPrice(bdprice);
                t.setTxnItemQty(qty);
                t.setTotalAmount((BigDecimal)o[6]);
            }
        }
        return list;
    }

    @Override
    public TransactionQueryHolder preparePagedDetailTransReportQuery(TransactionReportFilter filter, Map<String, Object> params) {
        String fromDate     = null;
        String toDate       = null;
        String receiptNo    = null;
        String custName     = null;
        String tmStatus     = null;
        if(filter != null){
            fromDate     = filter.getDateFrom();
            toDate       = filter.getDateTo();
            receiptNo    = filter.getReceipt();
            custName     = filter.getName();
            tmStatus     = filter.getStatus();
        }

        StringBuilder main = new StringBuilder();
        main
            .append(" select   ")
            .append(" t.createdDate as createdDate,  ")
            .append(" t.receiptNum as receiptNum,  ")
            .append(" t.tmStatus as tmStatus,  ")
            .append(" t.totalAmount as totalAmount,  ")
            .append(" t.custName as custName,  ")
            .append(" t.custEmail as custEmail,  ")
            .append(" t.custMobile as custMobile,  ")
            .append(" t.custIdNo as custIdNo,  ")
            .append(" t.custDob as custDob,  ")
            .append(" t.custReferSource as custReferSource,  ")
            .append(" t.custNationality as custNationality,  ")
            .append(" t.paymentType as paymentType,  ")
            .append(" 0 as bookFeeCents,  ") // TODO t.bookFeeCents
            .append(" tm.tmCcNum as tmCcLast4Digits,  ")
            .append(" tm.tmApprovalCode as tmApprovalCode,  ")
            .append(" ti.qty as qty,  ")
            .append(" ti.displayName as displayName,  ")
            .append(" ti.actualUnitPrice as actualUnitPrice,  ")
            .append(" t.langCode as langCode,  ")
            .append(" t.pinCode as pinCode,  ")
            .append(" t.trafficSource as trafficSource,  ")
            .append(" ti.subTotal as subTotal  ");

        StringBuilder sql = new StringBuilder();
        sql
            .append(" from  ")
            .append(" B2CSLMStoreTransaction t  ")
            .append(" left join B2CSLMTelemoneyLog tm on tm.tmRefNo = t.receiptNum,  ")
            .append(" B2CSLMStoreTransactionItem ti  ")
            .append(" where ti.transactionId = t.id ");
        if(fromDate != null && fromDate.trim().length() > 0){
            sql.append(" and t.createdDate >= :txnStartDate ");
            params.put("txnStartDate", NvxDateUtils.populateQueryFromDate(NvxDateUtils.parseDateFromDisplay(fromDate.trim(), false), true));
        }
        if(toDate != null && toDate.trim().length() > 0){
            sql.append(" and t.createdDate <= :txnEndDate ");
            params.put("txnEndDate", NvxDateUtils.populateQueryToDate(NvxDateUtils.parseDateFromDisplay(toDate.trim(), false), true));
        }
        if(tmStatus != null && tmStatus.trim().length() > 0){
            sql.append(" and t.tmStatus = :tmStatus  ");
            params.put("tmStatus", tmStatus.trim());
        }
        if(receiptNo != null && receiptNo.trim().length() > 0){
            sql.append(" and t.receiptNum like :receiptNum ");
            params.put("receiptNum", "%"+ receiptNo.trim() +"%");
        }
        if(custName != null && custName.trim().length() > 0){
            sql.append(" and t.custName like :custName ");
            params.put("custName", "%"+ custName.trim() +"%");
        }

        TransactionQueryHolder qh = new TransactionQueryHolder();
        qh.setListQueryScript(main.toString());
        qh.setCriteriaQueryScripts(sql.toString());
        qh.setCountQueryScripts("select count(ti.id)");
        qh.setParams(params);

        return qh;
    }

    @Transactional(readOnly=true)
    @Override
    public List<TransactionDetailsReportRow> getPagedDetailTransReport(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc) {
        initSortMap();

        StringBuilder sql = new StringBuilder();
        sql.append(qh.getListQueryScript()).append(" ").append(qh.getCriteriaQueryScripts());
        String orderCol = "t.createdDate";
        if(orderBy != null && orderBy.trim().length() > 0){
            orderCol = sortMap.get(orderBy);
            if(!(orderCol != null && orderCol.trim().length() > 0)){
                orderCol = "t.createdDate";
            }
        }
        sql.append(" ").append(" order by ").append(orderCol).append(" ").append(asc ? "asc" : "desc");

        SQLQuery query = prepareSqlQuery(sql.toString(), qh.getParams());

        if(skip != -1){
            query.setFirstResult(skip);
            query.setMaxResults(take);
            query.setFetchSize(take);
        }

        query
            .addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
            .addScalar("receiptNum", StandardBasicTypes.STRING)
            .addScalar("tmStatus", StandardBasicTypes.STRING)
            .addScalar("totalAmount", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("custName", StandardBasicTypes.STRING)
            .addScalar("custEmail", StandardBasicTypes.STRING)
            .addScalar("custMobile", StandardBasicTypes.STRING)
            .addScalar("custIdNo", StandardBasicTypes.STRING)
            .addScalar("custDob", StandardBasicTypes.STRING)
            .addScalar("custReferSource", StandardBasicTypes.STRING)
            .addScalar("custNationality", StandardBasicTypes.STRING)
            .addScalar("paymentType", StandardBasicTypes.STRING)
            .addScalar("bookFeeCents", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("tmCcLast4Digits", StandardBasicTypes.STRING)
            .addScalar("tmApprovalCode", StandardBasicTypes.STRING)
            .addScalar("qty", StandardBasicTypes.STRING)
            .addScalar("displayName", StandardBasicTypes.STRING)
            .addScalar("actualUnitPrice", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("langCode", StandardBasicTypes.STRING)
            .addScalar("pinCode", StandardBasicTypes.STRING)
            .addScalar("trafficSource", StandardBasicTypes.STRING)
            .addScalar("subTotal", StandardBasicTypes.BIG_DECIMAL);

        List<Object[]> results = query.list();
        List<TransactionDetailsReportRow> list = populateDetailTxnRptList(results);
        return list;
    }

    @Transactional(readOnly=true)
    @Override
    public int getPagedDetailTransReportCount(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc) {
        StringBuilder sql = new StringBuilder();
        sql.append(qh.getCountQueryScripts()).append(" ").append(qh.getCriteriaQueryScripts());
        SQLQuery query = prepareSqlQuery(sql.toString(), qh.getParams());
        Integer results = (Integer) query.uniqueResult();
        return results;
    }

    @Override
    public TransactionQueryHolder preparePagedSummaryTransReportQuery(TransactionReportFilter filter, Map<String, Object> params) {
        String fromDate     = null;
        String toDate       = null;
        if(filter != null){
            fromDate     = filter.getDateFrom();
            toDate       = filter.getDateTo();
        }

        StringBuilder sql = new StringBuilder();
        sql
            .append("select ")
            .append("ti.itemProductCode as itemProductCode, ")
            .append("ti.displayName as displayName, ")
            .append("ti.actualUnitPrice as actualUnitPrice, ")
            .append("sum(ti.qty) as qty, ")
            .append("sum(ti.subTotal) as subTotal ");

        StringBuilder criteria = new StringBuilder();
        criteria
            .append("from  ")
            .append("B2CSLMStoreTransaction t, ")
            .append("B2CSLMStoreTransactionItem ti ")
            .append("where ")
            .append("t.id = ti.transactionId ")
            .append("and t.tmStatus = 'SUCCESS' ");
        if(fromDate != null && fromDate.trim().length() > 0){
            criteria.append(" and t.createdDate >= :txnStartDate ");
            params.put("txnStartDate", NvxDateUtils.populateQueryFromDate(NvxDateUtils.parseDateFromDisplay(fromDate.trim(), false), true));
        }
        if(toDate != null && toDate.trim().length() > 0){
            criteria.append(" and t.createdDate <= :txnEndDate ");
            params.put("txnEndDate", NvxDateUtils.populateQueryToDate(NvxDateUtils.parseDateFromDisplay(toDate.trim(), false), true));
        }

        StringBuilder groupScript = new StringBuilder();
        groupScript
            .append("group by ti.itemProductCode,  ")
            .append("ti.displayName,  ")
            .append("ti.actualUnitPrice ");

        TransactionQueryHolder qh = new TransactionQueryHolder();
        qh.setListQueryScript(sql.toString());
        qh.setCriteriaQueryScripts(criteria.toString());
        qh.setCountQueryScripts("select count(temp.itemProductCode)");
        qh.setGroupingQueryScripts(groupScript.toString());
        qh.setParams(params);

        return qh;
    }

    @Transactional(readOnly=true)
    @Override
    public List<TransactionSummaryReportRow> getPagedSummaryTransReport(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc) {
        initSummaryRptSortMap();

        StringBuilder sql = new StringBuilder();

        sql.append(qh.getListQueryScript()).append(" ").append(qh.getCriteriaQueryScripts()).append(" ").append(qh.getGroupingQueryScripts());
        String orderCol = "ti.itemProductCode";
        if(orderBy != null && orderBy.trim().length() > 0){
            orderCol = summaryRptSortMap.get(orderBy);
            if(!(orderCol != null && orderCol.trim().length() > 0)){
                orderCol = "ti.itemProductCode";
            }
        }
        sql.append(" ").append(" order by ").append(orderCol).append(" ").append(asc ? "asc" : "desc");

        SQLQuery query = prepareSqlQuery(sql.toString(), qh.getParams());

        if(skip != -1){
            query.setFirstResult(skip);
            query.setMaxResults(take);
            query.setFetchSize(take);
        }
        query
            .addScalar("itemProductCode", StandardBasicTypes.STRING)
            .addScalar("displayName", StandardBasicTypes.STRING)
            .addScalar("actualUnitPrice", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("qty", StandardBasicTypes.INTEGER)
            .addScalar("subTotal", StandardBasicTypes.BIG_DECIMAL);

        List<Object[]> results = query.list();
        List<TransactionSummaryReportRow> list = populateSummaryTxnRptList(results);
        return list;
    }

    @Transactional(readOnly=true)
    @Override
    public int getPagedSummaryTransReportCount(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc) {
        StringBuilder sql = new StringBuilder();
        sql
            .append(qh.getCountQueryScripts())
            .append(" from ( ")
            .append(qh.getListQueryScript()).append(" ").append(qh.getCriteriaQueryScripts()).append(" ").append(qh.getGroupingQueryScripts())
            .append(" )temp ") ;
        SQLQuery query = prepareSqlQuery(sql.toString(), qh.getParams());
        Integer results = (Integer) query.uniqueResult();
        return results;
    }

    @Override
    public TransactionQueryHolder preparePagedPromotionItemTransReportQuery(TransactionReportFilter filter, Map<String, Object> params) {
        String fromDate     = null;
        String toDate       = null;
        String promoCd      = null;
        if(filter != null){
            fromDate     = filter.getDateFrom();
            toDate       = filter.getDateTo();
            promoCd      = filter.getTxnPromoCode();
        }

        StringBuilder sql = new StringBuilder();
        sql
            .append("select ")
            .append("ti.promoCodes as promoCodes, ")
            .append("ti.itemProductCode as itemProductCode, ")
            .append("ti.displayName as displayName, ")
            .append("ti.actualUnitPrice as actualUnitPrice, ")
            .append("sum(ti.qty) as qty, ")
            .append("ti.promoCodeDiscountId as promoCodeDiscountId, ")
            .append("sum(ti.subTotal) as subTotal ");

        StringBuilder criteria = new StringBuilder();
        criteria
            .append("from  ")
            .append("B2CSLMStoreTransaction t, ")
            .append("B2CSLMStoreTransactionItem ti ")
            .append("where ")
            .append("t.id = ti.transactionId ")
            .append("and t.tmStatus = 'SUCCESS' ")
            .append("and ti.promoCodes is not null ")
            .append("and ti.promoCodes <> '' ");
        if(fromDate != null && fromDate.trim().length() > 0){
            criteria.append(" and t.createdDate >= :txnStartDate ");
            params.put("txnStartDate", NvxDateUtils.populateQueryFromDate(NvxDateUtils.parseDateFromDisplay(fromDate.trim(), false), true));
        }
        if(toDate != null && toDate.trim().length() > 0){
            criteria.append(" and t.createdDate <= :txnEndDate ");
            params.put("txnEndDate", NvxDateUtils.populateQueryToDate(NvxDateUtils.parseDateFromDisplay(toDate.trim(), false), true));
        }
        if(promoCd != null && promoCd.trim().length() > 0){
            criteria.append(" and ti.promoCodes like :txnPromoCode ");
            params.put("txnPromoCode", "%" + promoCd + "%");
        }

        StringBuilder groupScript = new StringBuilder();
        groupScript
            .append("group by ti.promoCodes,  ")
            .append("ti.itemProductCode,  ")
            .append("ti.displayName,  ")
            .append("ti.actualUnitPrice, ")
            .append("ti.promoCodeDiscountId ");

        TransactionQueryHolder qh = new TransactionQueryHolder();
        qh.setListQueryScript(sql.toString());
        qh.setCriteriaQueryScripts(criteria.toString());
        qh.setCountQueryScripts("select count(temp.promoCodes)");
        qh.setGroupingQueryScripts(groupScript.toString());
        qh.setParams(params);

        return qh;
    }

    @Transactional(readOnly=true)
    @Override
    public List<TransactionPromotionReportRow> getPagedPromotionItemTransReport(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc) {
        initPromoSortMap();

        StringBuilder sql = new StringBuilder();

        sql.append(qh.getListQueryScript()).append(" ").append(qh.getCriteriaQueryScripts()).append(" ").append(qh.getGroupingQueryScripts());
        String orderCol = "ti.promoCodes";
        if(orderBy != null && orderBy.trim().length() > 0){
            orderCol = summaryRptSortMap.get(orderBy);
            if(!(orderCol != null && orderCol.trim().length() > 0)){
                orderCol = "ti.promoCodes";
            }
        }
        sql.append(" ").append(" order by ").append(orderCol).append(" ").append(asc ? "asc" : "desc");

        SQLQuery query = prepareSqlQuery(sql.toString(), qh.getParams());

        if(skip != -1){
            query.setFirstResult(skip);
            query.setMaxResults(take);
            query.setFetchSize(take);
        }
        query
            .addScalar("promoCodes", StandardBasicTypes.STRING)
            .addScalar("itemProductCode", StandardBasicTypes.STRING)
            .addScalar("displayName", StandardBasicTypes.STRING)
            .addScalar("actualUnitPrice", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("qty", StandardBasicTypes.INTEGER)
            .addScalar("promoCodeDiscountId", StandardBasicTypes.STRING)
            .addScalar("subTotal", StandardBasicTypes.BIG_DECIMAL);

        List<Object[]> results = query.list();
        List<TransactionPromotionReportRow> list = populatePromotionTxnRptList(results);
        return list;
    }

    @Transactional(readOnly=true)
    @Override
    public int getPagedPromotionItemTransReportCount(TransactionQueryHolder qh, Integer skip, Integer take, TransactionReportFilter filter, String orderBy, boolean asc) {
        StringBuilder sql = new StringBuilder();
        sql
            .append(qh.getCountQueryScripts())
            .append(" from ( ")
            .append(qh.getListQueryScript()).append(" ").append(qh.getCriteriaQueryScripts()).append(" ").append(qh.getGroupingQueryScripts())
            .append(" )temp ") ;
        SQLQuery query = prepareSqlQuery(sql.toString(), qh.getParams());
        Integer results = (Integer) query.uniqueResult();
        return results;
    }

}
