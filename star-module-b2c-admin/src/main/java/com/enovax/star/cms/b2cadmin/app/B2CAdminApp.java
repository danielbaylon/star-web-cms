package com.enovax.star.cms.b2cadmin.app;

import info.magnolia.ui.api.app.AppContext;
import info.magnolia.ui.api.app.AppView;
import info.magnolia.ui.api.app.SubAppDescriptor;
import info.magnolia.ui.api.location.DefaultLocation;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.framework.app.BaseApp;

import javax.inject.Inject;
import java.util.Iterator;

/**
 * Created by jonathan on 1/11/16.
 */
public class B2CAdminApp extends BaseApp {

    @Inject
    public B2CAdminApp(AppContext appContext, AppView view) {
        super(appContext, view);
    }

    public void start(Location location) {
        super.start(location);
        SubAppDescriptor first = null;
        Iterator browserSubAppDescriptor = this.appContext.getAppDescriptor().getSubApps().values().iterator();
        String appName = this.appContext.getAppDescriptor().getName();

        while (browserSubAppDescriptor.hasNext()) {
            SubAppDescriptor descriptor = (SubAppDescriptor) browserSubAppDescriptor.next();
            if (first == null) {
                first = descriptor;
            } else {
                this.getAppContext().openSubApp(new DefaultLocation("app", appName, descriptor.getName()));
            }
        }

        this.getAppContext().openSubApp(new DefaultLocation("app", appName, first.getName()));
    }

}
