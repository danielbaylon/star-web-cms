package com.enovax.star.cms.b2cadmin.model.slm;

import java.util.Calendar;

/**
 * Created by jonathan on 28/11/16.
 */
public class Promotion {

    private String discountId = null;
    private String promoCode = null;
    private Calendar validFrom = null;
    private Calendar validTo = null;

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Calendar getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Calendar validFrom) {
        this.validFrom = validFrom;
    }

    public Calendar getValidTo() {
        return validTo;
    }

    public void setValidTo(Calendar validTo) {
        this.validTo = validTo;
    }
}
