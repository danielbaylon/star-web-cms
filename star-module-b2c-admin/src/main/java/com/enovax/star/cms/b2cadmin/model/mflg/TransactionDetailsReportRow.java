package com.enovax.star.cms.b2cadmin.model.mflg;

import java.util.Date;

/**
 * Created by jonathan on 4/11/16.
 */
public class TransactionDetailsReportRow {

    private String receiptNum;
    private String createdDate;
    private Date rawCreatedDate;
    private String paymentType;
    private String sactPin;
    private String tmStatus;
    private String displayName;
    private String itemCode;
    private int qty;
    private double subTotal;
    private String displayTitle;
    private String ticketType;
    private String promoCode;
    private String jewelCard;
    private String dateOfVisit;
    private Date rawDateOfVisit;
    private String productType;

    private String custName;
    private String custEmail;
    private String custMobile;

    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSactPin() {
        return sactPin;
    }
    public void setSactPin(String sactPin) {
        this.sactPin = sactPin;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public String getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public String getItemCode() {
        return itemCode;
    }
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getSubTotal() {
        return subTotal;
    }
    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }
    public String getDisplayTitle() {
        return displayTitle;
    }
    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }
    public String getTicketType() {
        return ticketType;
    }
    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }
    public String getPromoCode() {
        return promoCode;
    }
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
    public String getTmStatus() {
        return tmStatus;
    }
    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }
    public String getJewelCard() {
        return jewelCard;
    }
    public void setJewelCard(String jewelCard) {
        this.jewelCard = jewelCard;
    }
    public String getDateOfVisit() {
        return dateOfVisit;
    }
    public void setDateOfVisit(String dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }
    public Date getRawCreatedDate() {
        return rawCreatedDate;
    }
    public void setRawCreatedDate(Date rawCreatedDate) {
        this.rawCreatedDate = rawCreatedDate;
    }
    public Date getRawDateOfVisit() {
        return rawDateOfVisit;
    }
    public void setRawDateOfVisit(Date rawDateOfVisit) {
        this.rawDateOfVisit = rawDateOfVisit;
    }
    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }
    public String getCustName() {
        return custName;
    }
    public void setCustName(String custName) {
        this.custName = custName;
    }
    public String getCustEmail() {
        return custEmail;
    }
    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }
    public String getCustMobile() {
        return custMobile;
    }
    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

}
