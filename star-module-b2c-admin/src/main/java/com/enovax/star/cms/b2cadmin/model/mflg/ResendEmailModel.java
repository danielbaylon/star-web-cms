package com.enovax.star.cms.b2cadmin.model.mflg;

/**
 * Created by jennylynsze on 11/10/16.
 */
public class ResendEmailModel {

    private String receiptNumber;
    private String newEmail;
    private boolean updateEmail;

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public boolean isUpdateEmail() {
        return updateEmail;
    }

    public void setUpdateEmail(boolean updateEmail) {
        this.updateEmail = updateEmail;
    }

}

