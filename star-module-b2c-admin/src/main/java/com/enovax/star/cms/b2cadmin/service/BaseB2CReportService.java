package com.enovax.star.cms.b2cadmin.service;

import com.enovax.star.cms.b2cadmin.model.TransactionReportRequest;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jonathan on 4/11/16.
 */
public class BaseB2CReportService {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected void initGridOrdering(TransactionReportRequest gridRequest, String defaultSortCol, boolean defaultAsc) {
        if (StringUtils.isEmpty(gridRequest.getOrderBy())) {
            log.info("No ordering specified. Going with default ordering.");
            gridRequest.setOrderBy(defaultSortCol);
            gridRequest.setAsc(defaultAsc);
        } else {
            log.info("Ordering specified.");
            if (gridRequest.getOrderBy().endsWith("Text")) {
                gridRequest.setOrderBy(gridRequest.getOrderBy().replace("Text", ""));
            }
        }
    }

}
