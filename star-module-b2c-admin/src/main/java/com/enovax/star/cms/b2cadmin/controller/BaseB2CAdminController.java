package com.enovax.star.cms.b2cadmin.controller;

import com.enovax.star.cms.commons.exception.BizValidationException;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.util.ProjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jonathan on 2/11/16.
 */
public abstract class BaseB2CAdminController {

    public static final String LOG_MSG_SYSTEM_EXCEPTION = "Unhandled system exception occurred. Log ID: ";

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected <K> ApiResult<K> handleUncaughtException(Throwable t, Class<K> resultClass, String extraMessage) {
        final String logId = ProjectUtils.generateUniqueLogId();
        log.error(LOG_MSG_SYSTEM_EXCEPTION + logId, t);
        return new ApiResult<>(com.enovax.star.cms.commons.constant.api.ApiErrorCodes.UnexpectedSystemException,
            StringUtils.isEmpty(extraMessage) ? "" : extraMessage, logId);
    }

    protected <K> ApiResult<K> handleException(String methodName, Throwable t, Class<K> resultClass) {
        if(t != null){
            log.error("!!! System exception encountered ["+methodName+"] !!!", t);
        }else{
            log.error("!!! System exception encountered ["+methodName+"] !!!");
        }
        if(t instanceof BizValidationException){
            BizValidationException be = (BizValidationException) t;
            if(be.getErrorMsg() != null && be.getErrorMsg().trim().length() > 0){
                return new ApiResult<>(false, "", be.getErrorMsg(), null);
            }
        }
        return handleUncaughtException(t, resultClass, null);
    }

}
