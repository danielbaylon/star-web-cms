package com.enovax.star.cms.b2cadmin.model.slm;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jonathan on 3/11/16.
 */
public class TransactionPromotionReportRow {

    private String promoCode;
    private Date validFrom;
    private Date validTo;
    private String txnItemCode;
    private String  txnItemName;
    private BigDecimal txnItemUnitPrice;
    private Integer txnItemQty;
    protected BigDecimal totalAmount;
    protected String validFromText;
    protected String validToText;

    public String getPromoCode() {
        return promoCode;
    }
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
    public Date getValidFrom() {
        return validFrom;
    }
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }
    public Date getValidTo() {
        return validTo;
    }
    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
    public String getTxnItemCode() {
        return txnItemCode;
    }
    public void setTxnItemCode(String txnItemCode) {
        this.txnItemCode = txnItemCode;
    }
    public String getTxnItemName() {
        return txnItemName;
    }
    public void setTxnItemName(String txnItemName) {
        this.txnItemName = txnItemName;
    }
    public BigDecimal getTxnItemUnitPrice() {
        return txnItemUnitPrice;
    }
    public void setTxnItemUnitPrice(BigDecimal txnItemUnitPrice) {
        this.txnItemUnitPrice = txnItemUnitPrice;
    }
    public Integer getTxnItemQty() {
        return txnItemQty;
    }
    public void setTxnItemQty(Integer txnItemQty) {
        this.txnItemQty = txnItemQty;
    }
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
    public String getValidFromText() {
        return validFromText;
    }
    public void setValidFromText(String validFromText) {
        this.validFromText = validFromText;
    }
    public String getValidToText() {
        return validToText;
    }
    public void setValidToText(String validToText) {
        this.validToText = validToText;
    }
}
