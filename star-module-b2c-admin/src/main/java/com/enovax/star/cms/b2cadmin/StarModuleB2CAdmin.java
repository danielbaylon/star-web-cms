package com.enovax.star.cms.b2cadmin;

import com.enovax.star.cms.b2cadmin.config.StarModuleB2CAdminAppConfig;
import com.enovax.star.cms.b2cadmin.config.StarModuleB2CAdminWebConfig;
import com.enovax.star.cms.b2cadmin.filter.SimpleCorsFilter;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.objectfactory.Components;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * This class is optional and represents the configuration for the star-module-b2c-admin module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/star-module-b2c-admin</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 */
public class StarModuleB2CAdmin implements ModuleLifecycle {

    private String urlMappings = "/.b2c-admin/*";

    private DispatcherServlet dispatcherServlet;

    public String getUrlMappings() {
        return urlMappings;
    }

    public void setUrlMappings(String urlMappings) {
        this.urlMappings = urlMappings;
    }

    protected ServletContext getServletContext() {
        return Components.getComponent(ServletContext.class);
    }

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
            ServletContext servletContext = getServletContext();

            AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
            webCtx.register(StarModuleB2CAdminAppConfig.class, StarModuleB2CAdminWebConfig.class);

            dispatcherServlet = new DispatcherServlet(webCtx);

            final ServletRegistration.Dynamic dispatcherRegistration = servletContext.addServlet("b2c-admin-dispatcher", dispatcherServlet);
            dispatcherRegistration.setLoadOnStartup(1);
            dispatcherRegistration.setInitParameter("dispatchOptionsRequest", "true");
            dispatcherRegistration.setAsyncSupported(true);

            final String[] urlMappingArray = urlMappings.split(",");
            dispatcherRegistration.addMapping(urlMappingArray);

            final FilterRegistration.Dynamic corsRegistration = servletContext.addFilter("b2cAdminCorsFilter", SimpleCorsFilter.class);
            corsRegistration.addMappingForServletNames(null, false, "b2c-admin-dispatcher");
            corsRegistration.setInitParameter("dispatchOptionsRequest", "true");
            corsRegistration.setAsyncSupported(true);
        }
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
            if (dispatcherServlet != null) {
                dispatcherServlet.destroy();
            }
        }
    }

}
