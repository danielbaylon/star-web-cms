package com.enovax.star.cms.b2cadmin.generator;

import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * Created by jonathan on 7/11/16.
 */
public class BaseTransactionExcelGenerator {

    protected static final int workbook_window_size = 3;

    protected int ri = 0;
    protected int ci = 0;
    protected int sn = 0;

    protected float total = 0.0f;
    protected float gst = 0.0f;

    protected Row row;
    protected Cell cell;

    protected SXSSFWorkbook wb;
    protected Sheet sh;
    protected CellRangeAddress region;

    protected void reset(){
        ci = 0;
        row = null;
        cell = null;
    }

    public void createReport(String name) {
        wb = new SXSSFWorkbook(workbook_window_size);
        sh = wb.createSheet();
        wb.setSheetName(0, name);
    }

    protected void writeFieldData(CellStyle style, String v) {
        cell = row.createCell(ci++);
        cell.setCellStyle(style);
        cell.setCellValue(getNotNullString(v));
    }

    protected void writeFieldData(CellStyle style, Integer v) {
        cell = row.createCell(ci++);
        cell.setCellStyle(style);
        cell.setCellValue(v);
    }

    protected void writeFieldData(CellStyle style, BigDecimal v) {
        cell = row.createCell(ci++);
        cell.setCellStyle(style);
        cell.setCellValue(v.doubleValue());
    }

    protected void writeFieldData(CellStyle style, Double v) {
        cell = row.createCell(ci++);
        cell.setCellStyle(style);
        cell.setCellValue(v.doubleValue());
    }


    protected void addReportParamSection(CellStyle style, String name, String value, boolean merge, int labelColumns) {
        reset();
        row = sh.createRow(ri++);

        cell = row.createCell(ci++);
        cell.setCellStyle(style);
        cell.setCellValue(name + " : ");

        if(merge && labelColumns > 0){
            for(int i = 0 ; i  < labelColumns ; i++){
                cell = row.createCell(ci++);
                cell.setCellStyle(style);
            }
        }

        cell = row.createCell(ci++);
        cell.setCellStyle(style);
        cell.setCellValue(getNotNullString(value));

        if(merge && labelColumns > 0){
            mergeWorksheetCellsByParams(sh, ri - 1, ri - 1, 0, 1);
        }
    }

    protected void addReportParamSection(CellStyle style, String name, String value) {
        addReportParamSection(style, name, value, true, 1);
    }

    protected String getNotNullString(String s) {
        return s != null ? s.trim() : "";
    }

    protected CellStyle getRptHeaderStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setWrapText(true);
        Font font = wb.createFont();
        font.setBold(true);
        style.setFont(font);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        return style;
    }
    protected CellStyle getRptColumnStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setWrapText(true);
        Font font = wb.createFont();
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        return style;
    }
    protected CellStyle getRptColumnValueStyle(SXSSFWorkbook wb, boolean center) {
        CellStyle style = wb.createCellStyle();
        style.setWrapText(true);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        if(center){
            style.setAlignment(CellStyle.ALIGN_CENTER);
        }
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        return style;
    }

    protected CellStyle getRptColumnValueStyle(SXSSFWorkbook wb) {
        return getRptColumnValueStyle(wb,false);
    }

    protected CellStyle getRptColumnValueRightStyle(SXSSFWorkbook wb) {
        CellStyle cell = getRptColumnValueStyle(wb,false);
        cell.setAlignment(CellStyle.ALIGN_RIGHT);
        return cell;
    }

    protected CellStyle getRptColumnValueRightStyle(SXSSFWorkbook wb, boolean bold) {
        CellStyle cell = getRptColumnValueRightStyle(wb);
        if(bold){
            Font f = wb.createFont();
            f.setBold(true);
            cell.setFont(f);
        }
        return cell;
    }

    protected CellStyle getRptCellRightAligmentStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        return style;
    }
    protected CellStyle getReportParameterStyle(SXSSFWorkbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setWrapText(true);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    protected CellStyle getReportMoneyValueStyleWithFullBorder(SXSSFWorkbook wb, boolean center) {
        CellStyle style = wb.createCellStyle();
        style.setWrapText(true);
        if(center){
            style.setAlignment(CellStyle.ALIGN_CENTER);
        }
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);

        DataFormat fmt = wb.createDataFormat();

        style.setDataFormat( fmt.getFormat("#,###,##0.00") );

        return style;
    }

    protected CellStyle getReportMoneyValueStyleWithFullBorderAndAlignRight(SXSSFWorkbook wb) {
        CellStyle style = getReportMoneyValueStyleWithFullBorder(wb, false);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        return style;
    }


    protected CellStyle getReportMoneyValueStyleWithFullBorderAndAlignRight(SXSSFWorkbook wb, boolean bold) {
        CellStyle cell = getReportMoneyValueStyleWithFullBorderAndAlignRight(wb);
        if(bold){
            Font f = wb.createFont();
            f.setBold(true);
            cell.setFont(f);
        }
        return cell;
    }


    protected CellStyle getReportMoneyValueStyleWithoutBorder(SXSSFWorkbook wb, boolean center) {
        CellStyle style = wb.createCellStyle();
        style.setWrapText(true);
        if(center){
            style.setAlignment(CellStyle.ALIGN_CENTER);
        }
        DataFormat fmt = wb.createDataFormat();

        style.setDataFormat( fmt.getFormat("#,###,##0.00") );

        return style;
    }

    protected void mergeWorksheetCellsByParams(Sheet sh, int row1, int row2, int cell1, int cell2) {
        region = new CellRangeAddress(row1, row2, cell1, cell2);
        sh.addMergedRegion(region);
        region = null;
    }

    public Workbook getReport() {
        return wb;
    }

}
