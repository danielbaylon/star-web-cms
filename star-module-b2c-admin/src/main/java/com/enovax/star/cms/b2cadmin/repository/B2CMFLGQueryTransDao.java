package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.mflg.QueryRecord;

import java.util.List;

/**
 * Created by jennylynsze on 11/10/16.
 */
public interface B2CMFLGQueryTransDao {

    List<QueryRecord> getQueryRecords(String recNum, String cusName,
                                      String nric, String email, String mobile, String pin,
                                      int start, int pageSize, String orderBy,
                                      boolean asc);

    int getQueryRecordCount(String recNum, String cusName, String nric,
                            String email, String mobile, String pin);
}
