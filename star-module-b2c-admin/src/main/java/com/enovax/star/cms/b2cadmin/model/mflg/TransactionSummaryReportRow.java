package com.enovax.star.cms.b2cadmin.model.mflg;

import java.util.Date;

/**
 * Created by jonathan on 4/11/16.
 */
public class TransactionSummaryReportRow {

    private String createdDate;
    private String receiptNum;
    private String sactPin;
    private String tmCcLast4Digits;
    private String tmStatus;
    private double totalAmount;
    private String tmApprovalCode;
    private String paymentType;
    private String sactResId;
    private String custName;
    private String custIdNo;
    private String custEmail;
    private String custMobile;
    private String bookFeePromoCode;
    private Date rawCreatedDate;
    private String productType;

    public String getCustEmail() {
        return custEmail;
    }
    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }
    public String getCustIdNo() {
        return custIdNo;
    }
    public void setCustIdNo(String custIdNo) {
        this.custIdNo = custIdNo;
    }
    public String getCustName() {
        return custName;
    }
    public void setCustName(String custName) {
        this.custName = custName;
    }
    public String getSactResId() {
        return sactResId;
    }
    public void setSactResId(String sactResId) {
        this.sactResId = sactResId;
    }
    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    public String getTmApprovalCode() {
        return tmApprovalCode;
    }
    public void setTmApprovalCode(String tmApprovalCode) {
        this.tmApprovalCode = tmApprovalCode;
    }
    public double getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
    public String getTmStatus() {
        return tmStatus;
    }
    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }
    public String getTmCcLast4Digits() {
        return tmCcLast4Digits;
    }
    public void setTmCcLast4Digits(String tmCcLast4Digits) {
        this.tmCcLast4Digits = tmCcLast4Digits;
    }
    public String getSactPin() {
        return sactPin;
    }
    public void setSactPin(String sactPin) {
        this.sactPin = sactPin;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public String getCustMobile() {
        return custMobile;
    }
    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }
    public String getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    public String getBookFeePromoCode() {
        return bookFeePromoCode;
    }
    public void setBookFeePromoCode(String bookFeePromoCode) {
        this.bookFeePromoCode = bookFeePromoCode;
    }
    public Date getRawCreatedDate() {
        return rawCreatedDate;
    }
    public void setRawCreatedDate(Date rawCreateDate) {
        this.rawCreatedDate = rawCreateDate;
    }
    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }

}
