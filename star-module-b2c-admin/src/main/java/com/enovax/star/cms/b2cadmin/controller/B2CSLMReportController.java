package com.enovax.star.cms.b2cadmin.controller;

import com.enovax.star.cms.b2cadmin.model.TransactionReportRequest;
import com.enovax.star.cms.b2cadmin.service.B2CSLMReportService;
import com.enovax.star.cms.commons.model.api.ApiResult;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by jonathan on 2/11/16.
 */
@Controller
@RequestMapping( "/b2cslmadmin/report/")
public class B2CSLMReportController extends BaseB2CAdminController {

    @Autowired
    private B2CSLMReportService reportService;

    @RequestMapping(value = "transaction-details/pre", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<List>> apiDoTransactionDetailsReportPregeneration(@RequestParam(name = "theJson", required = false) String theJson,
                                                                                      @RequestParam("take") int take, @RequestParam("skip") int skip,
                                                                                      @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
                                                                                      @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                                      @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(take, skip, page, pageSize, sortField, "asc".equals(sortDirection) ? true : false);
            gridRequest.setTheJson(theJson);
            return new ResponseEntity<>(reportService.doTransactionDetailsReportPregeneration(gridRequest), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionDetailsReportPregeneration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-details/export", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiDoTransactionDetailsReportExport(@RequestParam(name = "theJson", required = false) String theJson,
                                                                                 HttpServletResponse response) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(9999, 0, 0, 9999, "transDate", true);
            gridRequest.setTheJson(theJson);
            Workbook wb = reportService.doTransactionDetailsReportExport(gridRequest);

            final String fileName = "\"details-transaction-report.xlsx\"";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionDetailsReportExport", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-summary/pre", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<List>> apiDoTransactionSummaryReportPregeneration(@RequestParam(name = "theJson", required = false) String theJson,
                                                                                      @RequestParam("take") int take, @RequestParam("skip") int skip,
                                                                                      @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
                                                                                      @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                                      @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(take, skip, page, pageSize, sortField, "asc".equals(sortDirection) ? true : false);
            gridRequest.setTheJson(theJson);
            return new ResponseEntity<>(reportService.doTransactionSummaryReportPregeneration(gridRequest), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionSummaryReportPregeneration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-summary/export", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiDoTransactionSummaryReportExport(@RequestParam(name = "theJson", required = false) String theJson,
                                                                                 HttpServletResponse response) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(9999, 0, 0, 9999, "transDate", true);
            gridRequest.setTheJson(theJson);
            Workbook wb = reportService.doTransactionSummaryReportExport(gridRequest);

            final String fileName = "\"sales-summary-transaction.xlsx\"";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionSummaryReportExport", e, String.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-promotion/pre", method = {RequestMethod.POST})
    public ResponseEntity<ApiResult<List>> apiDoTransactionPromotionReportPregeneration(@RequestParam(name = "theJson", required = false) String theJson,
                                                                                        @RequestParam("take") int take, @RequestParam("skip") int skip,
                                                                                        @RequestParam("page") int page, @RequestParam("pageSize") int pageSize,
                                                                                        @RequestParam(value = "sort[0][field]", required = false) String sortField,
                                                                                        @RequestParam(value = "sort[0][dir]", required = false) String sortDirection) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(take, skip, page, pageSize, sortField, "asc".equals(sortDirection) ? true : false);
            gridRequest.setTheJson(theJson);
            return new ResponseEntity<>(reportService.doTransactionPromotionReportPregeneration(gridRequest), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionPromotionReportPregeneration", e, List.class), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "transaction-promotion/export", method = {RequestMethod.GET})
    public ResponseEntity<ApiResult<String>> apiDoTransactionPromotionReportExport(@RequestParam(name = "theJson", required = false) String theJson,
                                                                                   HttpServletResponse response) {
        try {
            TransactionReportRequest gridRequest = new TransactionReportRequest(9999, 0, 0, 9999, "transDate", true);
            gridRequest.setTheJson(theJson);
            Workbook wb = reportService.doTransactionPromotionReportExport(gridRequest);

            final String fileName = "\"promotion-transaction-report.xlsx\"";
            response.setContentType("application/octet-stream");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "No-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

            OutputStream outStream = response.getOutputStream();
            try {
                wb.write(outStream);
                outStream.flush();
            } finally {
                outStream.close();
            }

            return null;
        } catch (Exception e) {
            return new ResponseEntity<>(handleException("apiDoTransactionPromotionReportExport", e, String.class), HttpStatus.OK);
        }
    }

}
