package com.enovax.star.cms.b2cadmin.model.mflg;

import com.enovax.star.cms.commons.util.NvxDateUtils;

import java.util.Date;

/**
 * Created by jonathan on 4/11/16.
 */
public class TransactionReportFilter {

    private Date fromDate;
    private Date toDate;
    private String status;
    private int categoryId;
    private String proName;
    private String recNum;
    private Date dov;
    private String itemType;
    private String proCode;
    private String ticketType;
    private String productType;

    public TransactionReportFilter(String fromDate, String toDate, String proName, String status,
                                   String recNum, String dov, String proCode) throws Exception {
        if(fromDate != null && !"".equals(fromDate))
        {
            this.fromDate = NvxDateUtils.parseDateFromDisplay(fromDate, false);
        }
        if(toDate != null && !"".equals(toDate))
        {
            this.toDate = NvxDateUtils.parseDateFromDisplay(toDate, false);
        }
        if(dov != null && !"".equals(dov))
        {
            this.dov = NvxDateUtils.parseDateFromDisplay(dov, false);
        }
        this.status = status == null ? "" : status;
        this.proName = proName == null ? "" : proName;
        this.recNum = recNum == null ? "" : recNum;
        this.proCode = proCode == null ? "" : proCode;
    }

    public TransactionReportFilter(String fromDate, String toDate, String proName, String status) throws Exception {
        if(fromDate != null && !"".equals(fromDate)){
            this.fromDate = NvxDateUtils.parseDateFromDisplay(fromDate, false);
        }
        if(toDate != null && !"".equals(toDate)){
            this.toDate = NvxDateUtils.parseDateFromDisplay(toDate, false);
        }
        this.status = status == null ? "" : status;
        this.proName = proName == null ? "" : proName;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getRecNum() {
        return recNum;
    }

    public void setRecNum(String recNum) {
        this.recNum = recNum;
    }

    public Date getDov() {
        return dov;
    }

    public void setDov(Date dov) {
        this.dov = dov;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }
}
