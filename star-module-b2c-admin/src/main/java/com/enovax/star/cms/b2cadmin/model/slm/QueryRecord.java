package com.enovax.star.cms.b2cadmin.model.slm;

import java.io.Serializable;

/**
 * Created by jennylynsze on 11/9/16.
 */
public class QueryRecord implements Serializable {
    private String receiptNum;
    private String tmStatus;
    private String createdDate;
    private String custName;
    private String custEmail;
    private String custMobile;
    private String pinCode;
    private String langCode;

    public String getCustEmail() {
        return custEmail;
    }
    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }
    public String getCustName() {
        return custName;
    }
    public void setCustName(String custName) {
        this.custName = custName;
    }
    public String getTmStatus() {
        return tmStatus;
    }
    public void setTmStatus(String tmStatus) {
        this.tmStatus = tmStatus;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public String getCustMobile() {
        return custMobile;
    }
    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}
