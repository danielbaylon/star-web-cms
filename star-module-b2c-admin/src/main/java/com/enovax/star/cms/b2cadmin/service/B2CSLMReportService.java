package com.enovax.star.cms.b2cadmin.service;

import com.enovax.star.cms.b2cadmin.generator.TransactionPromotionExcelGenerator;
import com.enovax.star.cms.b2cadmin.generator.TransactionDetailsExcelGenerator;
import com.enovax.star.cms.b2cadmin.generator.TransactionSummaryExcelGenerator;
import com.enovax.star.cms.b2cadmin.model.*;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionDetailsReportRow;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionPromotionReportRow;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.model.slm.TransactionSummaryReportRow;
import com.enovax.star.cms.b2cadmin.repository.B2CSLMReportDao;
import com.enovax.star.cms.commons.model.api.ApiResult;
import com.enovax.star.cms.commons.util.JsonUtil;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by jonathan on 2/11/16.
 */
@Service
public class B2CSLMReportService extends BaseB2CReportService {

    @Autowired
    private B2CSLMReportDao reportDao;

    private void validateFilter(TransactionReportFilter filter) throws Exception {
        if (filter == null) {
            throw new Exception("Filter is NULL");
        }
        if (filter.getDateFrom() == null || filter.getDateFrom().trim().length() == 0) {
            throw new Exception("Invalid From Date [" + filter.getDateFrom() + "]");
        }
        if (filter.getDateTo() == null || filter.getDateTo().trim().length() == 0) {
            throw new Exception("Invalid To Date [" + filter.getDateTo() + "]");
        }

        NvxDateUtils.populateQueryFromDate(NvxDateUtils.parseDateFromDisplay(filter.getDateFrom(), false), true);
        NvxDateUtils.populateQueryFromDate(NvxDateUtils.parseDateFromDisplay(filter.getDateTo(), false), true);
    }

    public ApiResult<List> doTransactionDetailsReportPregeneration(TransactionReportRequest gridRequest) throws Exception {
        initGridOrdering(gridRequest, "transDate", true);
        String theJson = gridRequest.getTheJson();
        if (theJson != null && theJson.trim().length() > 0) {
            TransactionReportFilter filter = JsonUtil.fromJson(theJson, TransactionReportFilter.class);

            Map<String, Object> params = new HashMap<>();
            TransactionQueryHolder qh = reportDao.preparePagedDetailTransReportQuery(filter,  params);
            List<TransactionDetailsReportRow> txnRptList = reportDao.getPagedDetailTransReport(qh, gridRequest.getSkip(), gridRequest.getTake(),
                filter, gridRequest.getOrderBy(), gridRequest.isAsc());
            int total = reportDao.getPagedDetailTransReportCount(qh, gridRequest.getSkip(), gridRequest.getTake(), filter,
                gridRequest.getOrderBy(), gridRequest.isAsc());

            return new ApiResult<>(true, "", "", total, txnRptList);
        } else {
            return new ApiResult<>(true, "", "", 0, new ArrayList<TransactionDetailsReportRow>());
        }
    }

    public Workbook doTransactionDetailsReportExport(TransactionReportRequest gridRequest) throws Exception {
        initGridOrdering(gridRequest, "transDate", true);
        String theJson = gridRequest.getTheJson();
        if (theJson != null && theJson.trim().length() > 0) {
            TransactionReportFilter filter = JsonUtil.fromJson(theJson, TransactionReportFilter.class);

            Map<String, Object> params = new HashMap<>();
            TransactionQueryHolder qh = reportDao.preparePagedDetailTransReportQuery(filter,  params);
            List<TransactionDetailsReportRow> txnRptList = reportDao.getPagedDetailTransReport(qh, gridRequest.getSkip(), gridRequest.getTake(),
                filter, gridRequest.getOrderBy(), gridRequest.isAsc());

            TransactionDetailsExcelGenerator rptGen = new TransactionDetailsExcelGenerator(filter);
            rptGen.createReport("Details Transaction Report");
            rptGen.writeData(txnRptList);
            rptGen.buildReportFooter();
            Workbook wb = rptGen.getReport();

            return wb;
        } else {
            return null;
        }
    }

    public ApiResult<List> doTransactionSummaryReportPregeneration(TransactionReportRequest gridRequest) throws Exception {
        initGridOrdering(gridRequest, "transDate", true);
        String theJson = gridRequest.getTheJson();
        if (theJson != null && theJson.trim().length() > 0) {
            TransactionReportFilter filter = JsonUtil.fromJson(theJson, TransactionReportFilter.class);

            Map<String, Object> params = new HashMap<>();
            TransactionQueryHolder qh = reportDao.preparePagedSummaryTransReportQuery(filter, params);
            List<TransactionSummaryReportRow> txnRptList = reportDao.getPagedSummaryTransReport(qh, gridRequest.getSkip(), gridRequest.getTake(),
                filter, gridRequest.getOrderBy(), gridRequest.isAsc());
            int total = reportDao.getPagedSummaryTransReportCount(qh, gridRequest.getSkip(), gridRequest.getTake(), filter,
                gridRequest.getOrderBy(), gridRequest.isAsc());

            return new ApiResult<>(true, "", "", total, txnRptList);
        } else {
            return new ApiResult<>(true, "", "", 0, new ArrayList<TransactionSummaryReportRow>());
        }
    }

    public Workbook doTransactionSummaryReportExport(TransactionReportRequest gridRequest) throws Exception {
        initGridOrdering(gridRequest, "transDate", true);
        String theJson = gridRequest.getTheJson();
        if (theJson != null && theJson.trim().length() > 0) {
            TransactionReportFilter filter = JsonUtil.fromJson(theJson, TransactionReportFilter.class);

            Map<String, Object> params = new HashMap<>();
            TransactionQueryHolder qh = reportDao.preparePagedSummaryTransReportQuery(filter,  params);
            List<TransactionSummaryReportRow> txnRptList = reportDao.getPagedSummaryTransReport(qh, gridRequest.getSkip(), gridRequest.getTake(),
                filter, gridRequest.getOrderBy(), gridRequest.isAsc());

            TransactionSummaryExcelGenerator rptGen = new TransactionSummaryExcelGenerator(filter);
            rptGen.createReport("Sales Summary Transaction");
            rptGen.writeData(txnRptList);
            rptGen.buildReportFooter();
            Workbook wb = rptGen.getReport();

            return wb;
        } else {
            return null;
        }
    }

    public ApiResult<List> doTransactionPromotionReportPregeneration(TransactionReportRequest gridRequest) throws Exception {
        initGridOrdering(gridRequest, "transDate", true);
        String theJson = gridRequest.getTheJson();
        if (theJson != null && theJson.trim().length() > 0) {
            TransactionReportFilter filter = JsonUtil.fromJson(theJson, TransactionReportFilter.class);

            boolean validationPass = false;
            try {
                validateFilter(filter);
                validationPass = true;
            } catch (Exception e) {
                validationPass = false;
                log.error("Validating promotion code report search error : " + e.getMessage());
            }

            if (validationPass) {
                Map<String, Object> params = new HashMap<>();
                TransactionQueryHolder qh = reportDao.preparePagedPromotionItemTransReportQuery(filter, params);
                List<TransactionPromotionReportRow> txnRptList = reportDao.getPagedPromotionItemTransReport(qh, gridRequest.getSkip(),
                    gridRequest.getTake(), filter, gridRequest.getOrderBy(), gridRequest.isAsc());
                int total = reportDao.getPagedPromotionItemTransReportCount(qh, gridRequest.getSkip(), gridRequest.getTake(),
                    filter, gridRequest.getOrderBy(), gridRequest.isAsc());

                return new ApiResult<>(true, "", "", total, txnRptList);
            } else {
                return new ApiResult<>(true, "", "", 0, new ArrayList<TransactionPromotionReportRow>());
            }
        } else {
            return new ApiResult<>(true, "", "", 0, new ArrayList<TransactionPromotionReportRow>());
        }
    }

    public Workbook doTransactionPromotionReportExport(TransactionReportRequest gridRequest) throws Exception {
        initGridOrdering(gridRequest, "transDate", true);
        String theJson = gridRequest.getTheJson();
        if (theJson != null && theJson.trim().length() > 0) {
            TransactionReportFilter filter = JsonUtil.fromJson(theJson, TransactionReportFilter.class);

            Map<String, Object> params = new HashMap<>();
            TransactionQueryHolder qh = reportDao.preparePagedPromotionItemTransReportQuery(filter,  params);
            List<TransactionPromotionReportRow> txnRptList = reportDao.getPagedPromotionItemTransReport(qh, gridRequest.getSkip(),
                gridRequest.getTake(), filter, gridRequest.getOrderBy(), gridRequest.isAsc());

            TransactionPromotionExcelGenerator rptGen = new TransactionPromotionExcelGenerator(filter);
            rptGen.createReport("Promotion Transaction Report");
            rptGen.writeData(txnRptList);
            rptGen.buildReportFooter();
            Workbook wb = rptGen.getReport();

            return wb;
        } else {
            return null;
        }
    }

}
