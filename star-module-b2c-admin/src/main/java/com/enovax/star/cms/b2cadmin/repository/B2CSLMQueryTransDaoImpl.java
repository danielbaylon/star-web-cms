package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.slm.QueryRecord;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jennylynsze on 11/9/16.
 */
@Service
public class B2CSLMQueryTransDaoImpl implements  B2CSLMQueryTransDao {

    @Autowired(required = false)
    @Qualifier("sessionFactory")
    SessionFactory sessionFactory;

    @Override
    public int getQueryRecordCount(String recNum, String cusName, String nric, String email, String mobile, String pin) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT COUNT(1) FROM B2CSLMStoreTransaction t WHERE 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        if(StringUtils.isNotEmpty(recNum))
        {
            sb.append("AND t.receiptNum LIKE ? ");
            paras.add("%" + recNum + "%");
        }
        if(StringUtils.isNotEmpty(cusName))
        {
            sb.append("AND t.custName LIKE ? ");
            paras.add("%" + cusName + "%");
        }
        if(StringUtils.isNotEmpty(nric))
        {
            sb.append("AND t.custIdNo LIKE ? ");
            paras.add("%" + nric + "%");
        }
        if(StringUtils.isNotEmpty(email))
        {
            sb.append("AND t.custEmail LIKE ? ");
            paras.add("%" + email + "%");
        }
        if(StringUtils.isNotEmpty(mobile))
        {
            sb.append("AND t.custMobile LIKE ? ");
            paras.add("%" + mobile + "%");
        }
        if(StringUtils.isNotEmpty(pin))
        {
            sb.append("AND t.pinCode LIKE ? ");
            paras.add("%" + pin + "%");
        }

        return (int)executeSqlQueryScalar(sb.toString(), paras);
    }

    @Override
    public List<QueryRecord> getQueryRecords(String recNum, String cusName, String nric, String email, String mobile, String pin, int start, int pageSize, String orderBy, boolean asc) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT t.receiptNum as receiptNum, t.tmStatus as tmStatus, t.createdDate as createdDate, t.custName as custName, t.custEmail as custEmail, t.custMobile as custMobile, t.pinCode as pinCode , t.langCode as langCode FROM B2CSLMStoreTransaction t WHERE 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        if(StringUtils.isNotEmpty(recNum))
        {
            sb.append("AND t.receiptNum LIKE ? ");
            paras.add("%" + recNum + "%");
        }
        if(StringUtils.isNotEmpty(cusName))
        {
            sb.append("AND t.custName LIKE ? ");
            paras.add("%" + cusName + "%");
        }
        if(StringUtils.isNotEmpty(nric))
        {
            sb.append("AND t.custIdNo LIKE ? ");
            paras.add("%" + nric + "%");
        }
        if(StringUtils.isNotEmpty(email))
        {
            sb.append("AND t.custEmail LIKE ? ");
            paras.add("%" + email + "%");
        }
        if(StringUtils.isNotEmpty(mobile))
        {
            sb.append("AND t.custMobile LIKE ? ");
            paras.add("%" + mobile + "%");
        }
        if(StringUtils.isNotEmpty(pin))
        {
            sb.append("AND t.pinCode LIKE ? ");
            paras.add("%" + pin + "%");
        }

        sb.append("ORDER BY " + orderBy);
        if(!asc)
        {
            sb.append(" DESC");
        }

        SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("receiptNum", StandardBasicTypes.STRING)
                .addScalar("tmStatus",  StandardBasicTypes.STRING)
                .addScalar("createdDate",  StandardBasicTypes.TIMESTAMP)
                .addScalar("custName", StandardBasicTypes.STRING)
                .addScalar("custEmail",  StandardBasicTypes.STRING)
                .addScalar("custMobile",  StandardBasicTypes.STRING)
                .addScalar("pinCode",  StandardBasicTypes.STRING)
                .addScalar("langCode", StandardBasicTypes.STRING);

        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        @SuppressWarnings("unchecked")
        List<Object[]> list = qry.list();
        List<QueryRecord> records = new ArrayList<QueryRecord>();
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        for(Object[] o : list)
        {
            QueryRecord s = new QueryRecord();
            s.setReceiptNum((String)o[0]);
            s.setTmStatus((String)o[1]);

            Date date = (Date)o[2];
            String sDate = "";
            if(date != null)
            {
                sDate = fmt.format(date);
            }
            s.setCreatedDate(sDate);

            s.setCustName((String)o[3]);
            s.setCustEmail((String)o[4]);
            s.setCustMobile((String)o[5]);
            s.setPinCode((String)o[6]);
            s.setLangCode((String)o[7]);
            records.add(s);
        }
        return records;
    }

    public SQLQuery prepareSqlQuery(String query, List<Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        int i=0;
        for (Object para : parameters) {
            queryObject.setParameter(i, para);
            i++;
        }
        return queryObject;
    }


    public Object executeSqlQueryScalar(String query, List<Object> params) {
        SQLQuery qry = prepareSqlQuery(query, params);

        return qry.uniqueResult();
    }
}
