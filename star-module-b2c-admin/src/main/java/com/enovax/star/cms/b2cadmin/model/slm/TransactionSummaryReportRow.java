package com.enovax.star.cms.b2cadmin.model.slm;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jonathan on 2/11/16.
 */
public class TransactionSummaryReportRow {

    protected Date transDate;
    protected String transDateText;
    protected String receiptNum;
    protected String pin;
    protected String custName;
    protected String custEmail;
    protected String custMobile;
    protected String custIdNo;
    protected String custDob;
    protected String custNationality;
    protected String custReferSource;
    protected String sactResId;
    protected String bankApprovalCode;
    protected BigDecimal totalAmount;
    protected String totalAmountText;
    protected String ccLast4Digits;
    protected String paymentType;
    protected BigDecimal bookFeeCents;
    protected String bookFeeText;
    protected String status;
    protected String trafficSource;
    protected String recurPin;
    protected String lanCode;

    private String txnItemCode;
    private String  txnItemName;
    protected BigDecimal txnItemUnitPrice;
    protected Integer txnItemQty;

    public TransactionSummaryReportRow() {

    }

    public TransactionSummaryReportRow(Date transDate, String transDateText, String receiptNum, String pin,
                                       String custName, String custEmail, String custMobile, String custIdNo, String custDob,
                                       String custNationality, String custReferSource, String sactResId, String bankApprovalCode,
                                       BigDecimal totalAmount, String totalAmountText, String ccLast4Digits, String paymentType,
                                       BigDecimal bookFeeCents, String bookFeeText, String status) {
        super();
        this.transDate = transDate;
        this.transDateText = transDateText;
        this.receiptNum = receiptNum;
        this.pin = pin;
        this.custName = custName;
        this.custEmail = custEmail;
        this.custMobile = custMobile;
        this.custIdNo = custIdNo;
        this.custDob = custDob;
        this.custNationality = custNationality;
        this.custReferSource = custReferSource;
        this.sactResId = sactResId;
        this.bankApprovalCode = bankApprovalCode;
        this.totalAmount = totalAmount;
        this.totalAmountText = totalAmountText;
        this.ccLast4Digits = ccLast4Digits;
        this.paymentType = paymentType;
        this.bookFeeCents = bookFeeCents;
        this.bookFeeText = bookFeeText;
        this.status = status;
    }
    public Date getTransDate() {
        return transDate;
    }
    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }
    public String getTransDateText() {
        return transDateText;
    }
    public void setTransDateText(String transDateText) {
        this.transDateText = transDateText;
    }
    public String getReceiptNum() {
        return receiptNum;
    }
    public void setReceiptNum(String receiptNum) {
        this.receiptNum = receiptNum;
    }
    public String getPin() {
        return pin;
    }
    public void setPin(String pin) {
        this.pin = pin;
    }
    public String getCustName() {
        return custName;
    }
    public void setCustName(String custName) {
        this.custName = custName;
    }
    public String getCustEmail() {
        return custEmail;
    }
    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }
    public String getCustMobile() {
        return custMobile;
    }
    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }
    public String getCustIdNo() {
        return custIdNo;
    }
    public void setCustIdNo(String custIdNo) {
        this.custIdNo = custIdNo;
    }
    public String getCustDob() {
        return custDob;
    }
    public void setCustDob(String custDob) {
        this.custDob = custDob;
    }
    public String getCustNationality() {
        return custNationality;
    }
    public void setCustNationality(String custNationality) {
        this.custNationality = custNationality;
    }
    public String getCustReferSource() {
        return custReferSource;
    }
    public void setCustReferSource(String custReferSource) {
        this.custReferSource = custReferSource;
    }
    public String getSactResId() {
        return sactResId;
    }
    public void setSactResId(String sactResId) {
        this.sactResId = sactResId;
    }
    public String getBankApprovalCode() {
        return bankApprovalCode;
    }
    public void setBankApprovalCode(String bankApprovalCode) {
        this.bankApprovalCode = bankApprovalCode;
    }
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
    public String getTotalAmountText() {
        return totalAmountText;
    }
    public void setTotalAmountText(String totalAmountText) {
        this.totalAmountText = totalAmountText;
    }
    public String getCcLast4Digits() {
        return ccLast4Digits;
    }
    public void setCcLast4Digits(String ccLast4Digits) {
        this.ccLast4Digits = ccLast4Digits;
    }
    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
    public BigDecimal getBookFeeCents() {
        return bookFeeCents;
    }
    public void setBookFeeCents(BigDecimal bookFeeCents) {
        this.bookFeeCents = bookFeeCents;
    }
    public String getBookFeeText() {
        return bookFeeText;
    }
    public void setBookFeeText(String bookFeeText) {
        this.bookFeeText = bookFeeText;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTrafficSource() {
        return trafficSource;
    }

    public void setTrafficSource(String trafficSource) {
        this.trafficSource = trafficSource;
    }

    public String getTxnItemCode() {
        return txnItemCode;
    }

    public void setTxnItemCode(String txnItemCode) {
        this.txnItemCode = txnItemCode;
    }

    public String getRecurPin() {
        return recurPin;
    }

    public void setRecurPin(String recurPin) {
        this.recurPin = recurPin;
    }

    public String getLanCode() {
        return lanCode;
    }

    public void setLanCode(String lanCode) {
        this.lanCode = lanCode;
    }

    public Integer getTxnItemQty() {
        return txnItemQty;
    }

    public void setTxnItemQty(Integer txnItemQty) {
        this.txnItemQty = txnItemQty;
    }

    public BigDecimal getTxnItemUnitPrice() {
        return txnItemUnitPrice;
    }

    public void setTxnItemUnitPrice(BigDecimal txnItemUnitPrice) {
        this.txnItemUnitPrice = txnItemUnitPrice;
    }

    public String getTxnItemName() {
        return txnItemName;
    }

    public void setTxnItemName(String txnItemName) {
        this.txnItemName = txnItemName;
    }

}
