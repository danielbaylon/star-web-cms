package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.mflg.TransactionDetailsReportRow;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionReportFilter;
import com.enovax.star.cms.b2cadmin.model.mflg.TransactionSummaryReportRow;
import com.enovax.star.cms.commons.util.NvxDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jonathan on 4/11/16.
 */
@Service
public class B2CMFLGReportDaoImpl implements B2CMFLGReportDao {

    @Autowired
    private SessionFactory sessionFactory;

    private SQLQuery prepareSqlQuery(String query, List<Object> parameters) {
        SQLQuery queryObject = sessionFactory.getCurrentSession().createSQLQuery(query);
        int i = 0;
        for (Object para : parameters) {
            queryObject.setParameter(i, para);
            i++;
        }
        return queryObject;
    }

    private Object executeSqlQueryScalar(String query, List<Object> params) {
        SQLQuery qry = prepareSqlQuery(query, params);
        return qry.uniqueResult();
    }

    @Transactional(readOnly=true)
    @Override
    public List<TransactionSummaryReportRow> getSummaryRecords(TransactionReportFilter filter, int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT t.createdDate as createdDate, t.receiptNum as receiptNum, t.pinCode as sactPin, t.tmStatus as tmStatus, t.totalAmount as totalAmount, t.tmApprovalCode as tmApprovalCode, t.paymentType as paymentType, "
            + "t.custName as custName, t.custIdNo as custIdNo, t.custEmail as custEmail, t.custMobile as custMobile, tl.tmCcLast4Digits as tmCcLast4Digits, t.promoCodes as bookFeePromoCode "
            + "FROM B2CMFLGStoreTransaction t LEFT JOIN B2CMFLGTelemoneyLog tl ON t.receiptNum = tl.tmRefNo "
            + "AND ((tl.id IN (SELECT MAX(id) FROM B2CMFLGTelemoneyLog GROUP BY tmRefNo HAVING COUNT(*) > 1 )) OR (tl.id IN (SELECT MAX(id) FROM B2CMFLGTelemoneyLog GROUP BY tmRefNo HAVING COUNT(*) = 1 ))) "
            + "WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();
        if (filter.getFromDate() != null) {
            sb.append("AND convert(date, t.createdDate) >= ? ");
            paras.add(filter.getFromDate());
        }
        if (filter.getToDate() != null) {
            sb.append("AND convert(date, t.createdDate) <= ? ");
            paras.add(filter.getToDate());
        }
        if (!StringUtils.isEmpty(filter.getStatus())) {
            sb.append("AND t.tmStatus = ? ");
            paras.add(filter.getStatus());
        }
        if (!StringUtils.isEmpty(filter.getProName())) {
            sb.append("AND EXISTS(SELECT * FROM B2CMFLGStoreTransactionItem ti WHERE ti.transactionId = t.id AND ti.displayName LIKE ?) ");
            paras.add("%" + filter.getProName() + "%");
        }

//        addSkyDiningToSummaryQuery(fromDate, toDate, status, categoryId,
//            proName, ticketType, start, pageSize, orderBy, asc, sb, paras);

        if ("bookFeePromoCode".equals(orderBy)) {
            orderBy = "promoCodes";
        } else if ("sactPin".equals(orderBy)) {
            orderBy = "pinCode";
        }

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
            .addScalar("receiptNum", StandardBasicTypes.STRING)
            .addScalar("sactPin", StandardBasicTypes.STRING)
            .addScalar("tmStatus", StandardBasicTypes.STRING)
            .addScalar("totalAmount", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("tmApprovalCode", StandardBasicTypes.STRING)
            .addScalar("paymentType", StandardBasicTypes.STRING)
            .addScalar("custName", StandardBasicTypes.STRING)
            .addScalar("custIdNo", StandardBasicTypes.STRING)
            .addScalar("custEmail", StandardBasicTypes.STRING)
            .addScalar("custMobile", StandardBasicTypes.STRING)
            .addScalar("tmCcLast4Digits", StandardBasicTypes.STRING)
            .addScalar("bookFeePromoCode", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<TransactionSummaryReportRow> records = new ArrayList<>();
        for (final Object[] o : list) {
            final TransactionSummaryReportRow s = new TransactionSummaryReportRow();
            final Date date = (Date) o[0];
            s.setRawCreatedDate(date);
            String sDate = "";
            if (date != null) {
                sDate = NvxDateUtils.formatDateForDisplay(date, true);
            }
            s.setCreatedDate(sDate);
            s.setReceiptNum((String) o[1]);
            s.setSactPin((String) o[2]);
            s.setTmStatus((String) o[3]);
            final BigDecimal ttAmt = (BigDecimal) o[4];
            s.setTotalAmount(ttAmt.doubleValue());
            s.setTmApprovalCode((String) o[5]);
            s.setPaymentType((String) o[6]);
            s.setCustName((String) o[7]);
            s.setCustIdNo((String) o[8]);
            s.setCustEmail((String) o[9]);
            s.setCustMobile((String) o[10]);
            final String ccDigits = (String) o[11];
            s.setTmCcLast4Digits(StringUtils.isEmpty(ccDigits) ? ccDigits
                : String.format("%04d", Integer.parseInt(ccDigits)));
            s.setBookFeePromoCode((String) o[12]);
            records.add(s);
        }
        return records;
    }

    @Transactional(readOnly=true)
    @Override
    public int getSummaryRecordCount(TransactionReportFilter filter) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT COUNT(1) FROM B2CMFLGStoreTransaction t WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();
        if (filter.getFromDate() != null) {
            sb.append("AND convert(date, t.createdDate) >= ? ");
            paras.add(filter.getFromDate());
        }
        if (filter.getToDate() != null) {
            sb.append("AND convert(date, t.createdDate) <= ? ");
            paras.add(filter.getToDate());
        }
        if (!StringUtils.isEmpty(filter.getStatus())) {
            sb.append("AND t.tmStatus = ? ");
            paras.add(filter.getStatus());
        }
        if (!StringUtils.isEmpty(filter.getProName())) {
            sb.append("AND EXISTS(SELECT * FROM B2CMFLGStoreTransactionItem ti WHERE ti.transactionId = t.id AND ti.displayName LIKE ?) ");
            paras.add("%" + filter.getProName() + "%");
        }
        int summaryCount = (int) executeSqlQueryScalar(sb.toString(), paras);
        // summaryCount += getSkyDiningSummaryCount(fromDate, toDate, status, categoryId, proName, ticketType);
        return summaryCount;
    }

    @Transactional(readOnly=true)
    @Override
    public List<TransactionDetailsReportRow> getTransactionRecords(TransactionReportFilter filter, int start, int pageSize, String orderBy, boolean asc) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT t.receiptNum as receiptNum, t.createdDate as createdDate, t.paymentType as paymentType, t.pinCode as sactPin, ti.displayName as displayName, ti.itemProductCode as itemCode, ti.qty as qty, ti.subTotal as subTotal, "
            + "t.tmStatus as tmStatus, '' as displayTitle, ti.ticketType as ticketType, ti.promoCodes as promoCode, '' as jewelCard, isnull(ti.dateOfVisit, dov2) as dateOfVisit, 0 as nonTicketed, "
            + "t.custName as custName, t.custEmail as custEmail, t.custMobile as custMobile "
            + "FROM B2CMFLGStoreTransaction t INNER JOIN B2CMFLGStoreTransactionItem ti on ti.transactionId = t.id "
            + "INNER JOIN (select max(ti2.dateOfVisit) as dov2, ti2.transactionId as tid2 "
            + "from B2CMFLGStoreTransactionItem ti2 group by ti2.transactionId) tiwhoa on tiwhoa.tid2 = t.id "
            + "WHERE 1=1 ");

        final List<Object> paras = new ArrayList<>();
        if (filter.getFromDate() != null) {
            sb.append("AND convert(date, t.createdDate) >= ? ");
            paras.add(filter.getFromDate());
        }
        if (filter.getToDate() != null) {
            sb.append("AND convert(date, t.createdDate) <= ? ");
            paras.add(filter.getToDate());
        }
        if (!StringUtils.isEmpty(filter.getStatus())) {
            sb.append("AND t.tmStatus = ? ");
            paras.add(filter.getStatus());
        }
        if (!StringUtils.isEmpty(filter.getProName())) {
            sb.append("AND ti.displayName LIKE ? ");
            paras.add("%" + filter.getProName() + "%");
        }
        if (!StringUtils.isEmpty(filter.getRecNum())) {
            sb.append("AND t.receiptNum LIKE ? ");
            paras.add("%" + filter.getRecNum() + "%");
        }
        if (filter.getDov() != null) {
            sb.append("AND convert(date, ti.dateOfVisit) = ? ");
            paras.add(filter.getDov());
        }
        if (!StringUtils.isEmpty(filter.getProCode())) {
            sb.append("AND ti.promoCodes LIKE ? ");
            paras.add("%" + filter.getProCode() + "%");
        }

//        addSkyDiningToTransQuery(fromDate, toDate, status, categoryId, proName,
//            recNum, dov, itemType, proCode, ticketType, productType, start,
//            pageSize, orderBy, asc, sb, paras);

        if ("promoCode".equals(orderBy)) {
            orderBy = "promoCodes";
        } else if ("sactPin".equals(orderBy)) {
            orderBy = "pinCode";
        } else if ("itemCode".equals(orderBy)) {
            orderBy = "itemProductCode";
        }

        sb.append("ORDER BY " + orderBy);
        if (!asc) {
            sb.append(" DESC");
        }

        final SQLQuery qry = prepareSqlQuery(sb.toString(), paras);
        qry.addScalar("receiptNum", StandardBasicTypes.STRING)
            .addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
            .addScalar("paymentType", StandardBasicTypes.STRING)
            .addScalar("sactPin", StandardBasicTypes.STRING)
            .addScalar("displayName", StandardBasicTypes.STRING)
            .addScalar("itemCode",StandardBasicTypes.STRING)
            .addScalar("qty", StandardBasicTypes.INTEGER)
            .addScalar("subTotal", StandardBasicTypes.BIG_DECIMAL)
            .addScalar("tmStatus", StandardBasicTypes.STRING)
            .addScalar("displayTitle", StandardBasicTypes.STRING)
            .addScalar("ticketType", StandardBasicTypes.STRING)
            .addScalar("promoCode", StandardBasicTypes.STRING)
            .addScalar("jewelCard", StandardBasicTypes.STRING)
            .addScalar("dateOfVisit", StandardBasicTypes.TIMESTAMP)
            .addScalar("nonTicketed", StandardBasicTypes.INTEGER)
            .addScalar("custName", StandardBasicTypes.STRING)
            .addScalar("custEmail", StandardBasicTypes.STRING)
            .addScalar("custMobile", StandardBasicTypes.STRING);
        if (start > -1) {
            qry.setFirstResult(start);
            qry.setMaxResults(pageSize);
            qry.setFetchSize(pageSize);
        }

        List<Object[]> list = qry.list();
        if (list == null) {
            list = new ArrayList<>();
        }

        final List<TransactionDetailsReportRow> records = new ArrayList<>();
        for (final Object[] o : list) {
            final TransactionDetailsReportRow s = new TransactionDetailsReportRow();

            s.setReceiptNum((String) o[0]);
            final Date date = (Date) o[1];
            s.setRawCreatedDate(date);
            String sDate = "";
            if (date != null) {
                sDate = NvxDateUtils.formatDateForDisplay(date, false);
            }
            s.setCreatedDate(sDate);
            s.setPaymentType((String) o[2]);
            s.setSactPin((String) o[3]);
            s.setDisplayName((String) o[4]);
            s.setItemCode((String) o[5]);
            s.setQty((int) o[6]);
            final BigDecimal ttAmt = (BigDecimal) o[7];
            s.setSubTotal(ttAmt.doubleValue());
            s.setTmStatus((String) o[8]);
            s.setPromoCode((String) o[11]);
            final Date dateOfVisit = (Date) o[13];
            s.setRawDateOfVisit(dateOfVisit);
            String dateOfVisitStr = "";
            if (dateOfVisit != null) {
                dateOfVisitStr = NvxDateUtils.formatDateForDisplay(dateOfVisit, true);
            }
            s.setDateOfVisit(dateOfVisitStr);
            s.setCustName((String)o[15]);
            s.setCustEmail((String)o[16]);
            s.setCustMobile((String)o[17]);

            records.add(s);
        }

        return records;
    }

    @Transactional(readOnly=true)
    @Override
    public int getTransactionRecordCount(TransactionReportFilter filter) {
        final StringBuffer sb = new StringBuffer();
        sb.append("SELECT COUNT(1) FROM B2CMFLGStoreTransaction t INNER JOIN B2CMFLGStoreTransactionItem ti on ti.transactionId = t.id WHERE 1=1 ");
        final List<Object> paras = new ArrayList<>();
        if (filter.getFromDate() != null) {
            sb.append("AND convert(date, t.createdDate) >= ? ");
            paras.add(filter.getFromDate());
        }
        if (filter.getToDate() != null) {
            sb.append("AND convert(date, t.createdDate) <= ? ");
            paras.add(filter.getToDate());
        }
        if (!StringUtils.isEmpty(filter.getStatus())) {
            sb.append("AND t.tmStatus = ? ");
            paras.add(filter.getStatus());
        }
        if (!StringUtils.isEmpty(filter.getProName())) {
            sb.append("AND ti.displayName LIKE ? ");
            paras.add("%" + filter.getProName() + "%");
        }
        if (!StringUtils.isEmpty(filter.getRecNum())) {
            sb.append("AND t.receiptNum LIKE ? ");
            paras.add("%" + filter.getRecNum() + "%");
        }
        if (filter.getDov() != null) {
            sb.append("AND convert(date, ti.dateOfVisit) = ? ");
            paras.add(filter.getDov());
        }
        if (!StringUtils.isEmpty(filter.getProCode())) {
            sb.append("AND ti.promoCodes LIKE ? ");
            paras.add("%" + filter.getProCode() + "%");
        }
        int transCount = (int) executeSqlQueryScalar(sb.toString(), paras);
//        transCount += getSkyDiningTransCount(fromDate, toDate, status,
//            categoryId, proName, recNum, dov, itemType, proCode,
//            ticketType, productType);
        return transCount;
    }

}
