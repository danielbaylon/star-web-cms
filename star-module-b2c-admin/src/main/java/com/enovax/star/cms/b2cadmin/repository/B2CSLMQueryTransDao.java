package com.enovax.star.cms.b2cadmin.repository;

import com.enovax.star.cms.b2cadmin.model.slm.QueryRecord;

import java.util.List;

/**
 * Created by jennylynsze on 11/9/16.
 */
public interface B2CSLMQueryTransDao {

    int getQueryRecordCount(String recNum, String cusName, String nric, String email, String mobile, String pin);

    List<QueryRecord> getQueryRecords(String recNum, String cusName, String nric, String email,
                                      String mobile, String pin, int start, int pageSize, String orderBy, boolean asc);
}
